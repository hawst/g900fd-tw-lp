.class public final enum Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SyncedMediaType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

.field public static final enum b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

.field public static final enum c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

.field public static final enum d:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

.field private static final synthetic e:[Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 211
    new-instance v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v1, "IMAGES"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    .line 212
    new-instance v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    .line 213
    new-instance v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v1, "VIDEOS"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    .line 214
    new-instance v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v1, "DOCUMENTS"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->d:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    .line 210
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->d:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->e:[Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 210
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;
    .locals 1

    .prologue
    .line 210
    const-class v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->e:[Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v0}, [Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    return-object v0
.end method
