.class public final Lcom/mfluent/asp/datamodel/w;
.super Lcom/mfluent/asp/datamodel/ap;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/w$a;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ap;-><init>()V

    .line 31
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/w;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 35
    const-string v0, "device"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 40
    const-string v0, "device"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->b(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const-string v0, "DEVICES"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const-string v0, "DEVICES"

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return-object v0
.end method
