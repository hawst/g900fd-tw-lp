.class public final Lcom/mfluent/asp/datamodel/a/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/datamodel/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 18
    invoke-static {p1}, Lcom/samsung/android/sdk/samsung/c;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/c;

    move-result-object v0

    .line 19
    invoke-static {p1}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v1

    .line 20
    if-nez p3, :cond_0

    .line 23
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Value for setting must be provided in the extras"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.Settings.VIDEO_OPTIMIZATION"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 27
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SetSetting.VALUE"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 28
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/samsung/c;->a(Z)V

    .line 51
    :cond_1
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 52
    const-string v1, "method_result"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    :goto_1
    return-object v0

    .line 29
    :cond_2
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.Settings.MARKETING_PUSH"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 30
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SetSetting.VALUE"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 31
    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsung/c;->b()Z

    move-result v3

    .line 32
    if-eq v2, v3, :cond_1

    .line 33
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/samsung/c;->b(Z)V

    .line 34
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/samsung/c;->c(Z)V

    .line 35
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/mfluent/asp/datamodel/a/v$1;

    invoke-direct {v2, p0, v1}, Lcom/mfluent/asp/datamodel/a/v$1;-><init>(Lcom/mfluent/asp/datamodel/a/v;Lcom/mfluent/asp/b/i;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 48
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method
