.class public final Lcom/mfluent/asp/datamodel/filebrowser/d$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/filebrowser/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
            "<*>;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b:I

    .line 38
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a:Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    .line 39
    const/4 v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b:I

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b()V

    return-void
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    monitor-exit p0

    return-void

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/mfluent/asp/datamodel/filebrowser/d$a;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->c()V

    return-void
.end method

.method static synthetic c(Lcom/mfluent/asp/datamodel/filebrowser/d$a;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b:I

    return v0
.end method

.method private declared-synchronized c()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 51
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b:I

    if-le v0, v1, :cond_1

    .line 52
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 53
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b:I

    if-ne v0, v1, :cond_0

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->b:I

    .line 55
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a:Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a:Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    invoke-interface {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->destroy()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a:Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    return-object v0
.end method
