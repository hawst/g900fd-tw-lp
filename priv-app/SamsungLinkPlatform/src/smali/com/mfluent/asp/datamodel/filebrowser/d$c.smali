.class final Lcom/mfluent/asp/datamodel/filebrowser/d$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/filebrowser/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field public final d:Ljava/lang/String;

.field public e:Z

.field private f:I


# direct methods
.method public constructor <init>(JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->f:I

    .line 70
    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->e:Z

    .line 73
    iput-wide p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->a:J

    .line 74
    iput-object p3, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->b:Ljava/lang/String;

    .line 75
    iput-object p4, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->c:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 76
    iput-object p5, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->d:Ljava/lang/String;

    .line 77
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 86
    check-cast p1, Lcom/mfluent/asp/datamodel/filebrowser/d$c;

    .line 88
    iget-wide v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->a:J

    iget-wide v2, p1, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->c:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    iget-object v1, p1, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->c:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->e:Z

    iget-boolean v1, p1, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->e:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 97
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->f:I

    if-nez v0, :cond_0

    .line 98
    new-instance v0, Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    invoke-direct {v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;-><init>()V

    .line 99
    iget-wide v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->a:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(J)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    .line 100
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    .line 101
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->c:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    .line 102
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    .line 104
    iget-boolean v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->e:Z

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(Z)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    .line 105
    invoke-virtual {v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->toHashCode()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->f:I

    .line 108
    :cond_0
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/d$c;->f:I

    return v0
.end method
