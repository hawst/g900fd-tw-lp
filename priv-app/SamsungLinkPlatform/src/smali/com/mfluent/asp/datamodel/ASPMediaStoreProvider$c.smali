.class final Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V
    .locals 4

    .prologue
    .line 5294
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5296
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/util/i;->a()Lcom/mfluent/asp/util/i;

    move-result-object v0

    const-string v1, "NotifySender"

    new-instance v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;B)V

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/util/i;->a(Ljava/lang/String;Landroid/os/Handler$Callback;)Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->b:Landroid/os/Handler;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5299
    return-void

    .line 5297
    :catch_0
    move-exception v0

    .line 5298
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trouble starting handler for NotifySender"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 5284
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->b:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 5303
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->b:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 5304
    return-void
.end method
