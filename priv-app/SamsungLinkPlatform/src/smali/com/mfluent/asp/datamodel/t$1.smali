.class final Lcom/mfluent/asp/datamodel/t$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/t;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/datamodel/t;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/t$1;->a:Lcom/mfluent/asp/datamodel/t;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 91
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->h()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "masterReset"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 94
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/t$1;->a:Lcom/mfluent/asp/datamodel/t;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 95
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 96
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v3

    if-nez v3, :cond_0

    .line 98
    :try_start_0
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->P()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v3

    .line 100
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->h()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "::masterReset: Trouble removing device {}"

    invoke-interface {v4, v5, v0, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 103
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    const-string v4, "latitude IS NULL OR longitude IS NULL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->h()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "::onReceive masterReset where:{}"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 113
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;->getContentUriForDevice(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 117
    :cond_1
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Journal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v0, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 118
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Journal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v0, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 119
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v0, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 120
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Journal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v0, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 122
    invoke-static {v8}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 123
    invoke-static {v8}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Artists;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 124
    invoke-static {v8}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 125
    invoke-static {v8}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Albums;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 126
    return-void
.end method
