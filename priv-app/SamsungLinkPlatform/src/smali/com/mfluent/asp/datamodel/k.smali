.class abstract Lcom/mfluent/asp/datamodel/k;
.super Landroid/database/AbstractWindowedCursor;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/k$1;
    }
.end annotation


# instance fields
.field protected a:Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

.field protected b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

.field protected c:J

.field protected d:Ljava/lang/String;

.field protected e:Ljava/lang/String;

.field protected f:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field protected g:Z

.field protected h:Landroid/content/res/Resources;


# direct methods
.method protected constructor <init>(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0}, Landroid/database/AbstractWindowedCursor;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/k;->g:Z

    .line 79
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/k;->a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Z)V

    .line 80
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;B)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 83
    invoke-direct {p0}, Landroid/database/AbstractWindowedCursor;-><init>()V

    .line 36
    iput-boolean v6, p0, Lcom/mfluent/asp/datamodel/k;->g:Z

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    .line 84
    invoke-direct/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/k;->a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Z)V

    .line 85
    return-void
.end method

.method protected static a(Lcom/mfluent/asp/common/datamodel/ASPFile;)Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;
    .locals 3

    .prologue
    .line 134
    const/4 v0, 0x0

    .line 136
    if-eqz p0, :cond_0

    .line 137
    invoke-interface {p0}, Lcom/mfluent/asp/common/datamodel/ASPFile;->getSpecialDirectoryType()Ljava/lang/String;

    move-result-object v1

    .line 138
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 140
    :try_start_0
    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->valueOf(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 147
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/k;->h:Landroid/content/res/Resources;

    .line 44
    iput-wide p2, p0, Lcom/mfluent/asp/datamodel/k;->c:J

    .line 45
    iput-object p4, p0, Lcom/mfluent/asp/datamodel/k;->d:Ljava/lang/String;

    .line 46
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 47
    invoke-static {p1, v0}, Lcom/mfluent/asp/datamodel/filebrowser/e;->a(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/k;->a:Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

    .line 49
    if-eqz p6, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->a:Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/mfluent/asp/datamodel/filebrowser/b;

    if-ne v0, v1, :cond_0

    .line 50
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->a:Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/b;

    .line 51
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/b;->a()V

    .line 60
    :cond_0
    invoke-static {p5}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DATE_MODIFIED_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    :cond_1
    :goto_0
    iput-object v0, p0, Lcom/mfluent/asp/datamodel/k;->f:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/k;->e:Ljava/lang/String;

    .line 63
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a()Lcom/mfluent/asp/datamodel/filebrowser/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/k;->a:Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/k;->f:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    invoke-virtual {v0, v1, p4, v2, p6}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/common/datamodel/ASPFileProvider;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Z)Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/k;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/k;->g:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 74
    return-void

    .line 60
    :cond_2
    const/16 v0, 0x2c

    invoke-static {p5, v0}, Lorg/apache/commons/lang3/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-le v0, v2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only a single sort field may be specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {p5}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->getSortTypeFromCursorSortOrder(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DATE_MODIFIED_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    throw v0

    .line 72
    :catch_1
    move-exception v0

    throw v0
.end method

.method protected static b(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)Ljava/lang/Long;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 217
    if-eqz p0, :cond_0

    .line 218
    if-nez p1, :cond_1

    .line 234
    :cond_0
    :goto_0
    return-object v0

    .line 222
    :cond_1
    sget-object v1, Lcom/mfluent/asp/datamodel/k$1;->a:[I

    invoke-virtual {p1}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 224
    :pswitch_1
    const-wide/32 v0, 0x7f0200be

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 227
    :pswitch_2
    const-wide/32 v0, 0x7f0200bc

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 229
    :pswitch_3
    const-wide/32 v0, 0x7f02008d

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 222
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected static c(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 160
    const/4 v0, 0x0

    .line 162
    if-eqz p0, :cond_0

    .line 163
    invoke-interface {p0}, Lcom/mfluent/asp/common/datamodel/ASPFile;->isSecure()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 172
    :cond_0
    :goto_0
    return-object v0

    .line 165
    :cond_1
    invoke-interface {p0}, Lcom/mfluent/asp/common/datamodel/ASPFile;->isPersonal()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    const-wide/16 v0, 0x2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 168
    :cond_2
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method protected static e(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 213
    invoke-static {p0}, Lcom/mfluent/asp/datamodel/k;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;)Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/mfluent/asp/datamodel/k;->b(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected static f(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 239
    if-eqz p0, :cond_0

    .line 240
    invoke-interface {p0}, Lcom/mfluent/asp/common/datamodel/ASPFile;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 241
    const-string v0, "vnd.android.document/directory"

    .line 246
    :cond_0
    :goto_0
    return-object v0

    .line 243
    :cond_1
    invoke-interface {p0}, Lcom/mfluent/asp/common/datamodel/ASPFile;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 180
    const/4 v0, 0x0

    .line 182
    if-eqz p1, :cond_0

    .line 183
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->getName()Ljava/lang/String;

    move-result-object v0

    .line 184
    if-eqz p2, :cond_0

    .line 186
    sget-object v1, Lcom/mfluent/asp/datamodel/k$1;->a:[I

    invoke-virtual {p2}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    move v1, v2

    .line 203
    :goto_0
    if-eq v1, v2, :cond_0

    .line 204
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->h:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 209
    :cond_0
    return-object v0

    .line 188
    :pswitch_0
    const v1, 0x7f0a02ae

    .line 189
    goto :goto_0

    .line 191
    :pswitch_1
    const v1, 0x7f0a02ad

    .line 192
    goto :goto_0

    .line 194
    :pswitch_2
    const v1, 0x7f0a0278

    .line 195
    goto :goto_0

    .line 197
    :pswitch_3
    const v1, 0x7f0a0277

    .line 198
    goto :goto_0

    .line 200
    :pswitch_4
    const v1, 0x7f0a0279

    goto :goto_0

    .line 186
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(Ljava/lang/Long;II)Z
    .locals 4

    .prologue
    .line 126
    if-eqz p1, :cond_0

    .line 127
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p2, p3}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v0

    .line 129
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0, p2, p3}, Landroid/database/CursorWindow;->putNull(II)Z

    move-result v0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;II)Z
    .locals 1

    .prologue
    .line 118
    if-eqz p1, :cond_0

    .line 119
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0, p1, p2, p3}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    move-result v0

    .line 121
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0, p2, p3}, Landroid/database/CursorWindow;->putNull(II)Z

    move-result v0

    goto :goto_0
.end method

.method protected final b(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 152
    if-eqz p1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->a:Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

    invoke-interface {v0, p1}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v0

    .line 156
    :cond_0
    return-object v0
.end method

.method protected final d(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    invoke-static {p1}, Lcom/mfluent/asp/datamodel/k;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;)Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/mfluent/asp/datamodel/k;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onDeactivateOrClose()V
    .locals 7

    .prologue
    .line 251
    invoke-super {p0}, Landroid/database/AbstractWindowedCursor;->onDeactivateOrClose()V

    .line 252
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/k;->g:Z

    if-eqz v0, :cond_0

    .line 253
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/k;->g:Z

    .line 255
    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a()Lcom/mfluent/asp/datamodel/filebrowser/d;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/k;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    iget-wide v2, p0, Lcom/mfluent/asp/datamodel/k;->c:J

    iget-object v4, p0, Lcom/mfluent/asp/datamodel/k;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/mfluent/asp/datamodel/k;->f:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    iget-object v6, p0, Lcom/mfluent/asp/datamodel/k;->e:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    .line 261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/k;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    .line 263
    :cond_0
    return-void
.end method

.method public onMove(II)Z
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->mWindow:Landroid/database/CursorWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0}, Landroid/database/CursorWindow;->getStartPosition()I

    move-result v0

    if-lt p2, v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v0}, Landroid/database/CursorWindow;->getStartPosition()I

    move-result v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/k;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v1}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v1

    add-int/2addr v0, v1

    if-lt p2, v0, :cond_1

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/k;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {p0, p2, v0}, Lcom/mfluent/asp/datamodel/k;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 114
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
