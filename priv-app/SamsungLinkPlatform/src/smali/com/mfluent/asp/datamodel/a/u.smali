.class public Lcom/mfluent/asp/datamodel/a/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/datamodel/a/a;


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    const-class v0, Lcom/mfluent/asp/datamodel/a/u;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/a/u;->a:Lorg/slf4j/Logger;

    .line 20
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 22
    sput-object v0, Lcom/mfluent/asp/datamodel/a/u;->b:Landroid/util/SparseArray;

    const/4 v1, 0x2

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 23
    sget-object v0, Lcom/mfluent/asp/datamodel/a/u;->b:Landroid/util/SparseArray;

    const/16 v1, 0xf

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->d:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 24
    sget-object v0, Lcom/mfluent/asp/datamodel/a/u;->b:Landroid/util/SparseArray;

    const/4 v1, 0x1

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 25
    sget-object v0, Lcom/mfluent/asp/datamodel/a/u;->b:Landroid/util/SparseArray;

    const/4 v1, 0x3

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 35
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SetSyncMediaTypePriority.INTENT_ARG_MEDIA_TYPE"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 36
    sget-object v0, Lcom/mfluent/asp/datamodel/a/u;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    .line 37
    if-nez v0, :cond_0

    .line 38
    sget-object v0, Lcom/mfluent/asp/datamodel/a/u;->a:Lorg/slf4j/Logger;

    const-string v2, "Ignoring invalid media type argument: {}"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 44
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 40
    :cond_0
    sget-object v1, Lcom/mfluent/asp/datamodel/a/u;->a:Lorg/slf4j/Logger;

    const-string v2, "Setting high priority media type to {}"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 41
    invoke-static {v0}, Lcom/mfluent/asp/sync/b;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    return v0
.end method
