.class public Lcom/mfluent/asp/datamodel/filebrowser/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPFileProvider;


# instance fields
.field private final a:Lcom/mfluent/asp/datamodel/Device;

.field private final b:Landroid/content/Context;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->c:Z

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->b:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->a:Lcom/mfluent/asp/datamodel/Device;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->c:Z

    .line 35
    return-void
.end method

.method public varargs deleteFiles(Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;[Ljava/lang/String;)I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 76
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/d$b;->a:Lcom/mfluent/asp/datamodel/filebrowser/d;

    .line 77
    invoke-virtual {v0, p0, p1, p2}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/common/datamodel/ASPFileProvider;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;)Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    move-result-object v1

    .line 78
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/datamodel/filebrowser/a;

    .line 79
    array-length v7, p3

    .line 82
    :try_start_0
    new-instance v5, Ljava/util/HashSet;

    array-length v4, p3

    invoke-direct {v5, v4}, Ljava/util/HashSet;-><init>(I)V

    .line 83
    array-length v8, p3

    move v4, v3

    :goto_0
    if-ge v4, v8, :cond_0

    aget-object v9, p3, v4

    .line 84
    invoke-virtual {v5, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 83
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    move v4, v3

    .line 87
    :goto_1
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/a;->getCount()I

    move-result v8

    if-ge v4, v8, :cond_2

    .line 88
    invoke-virtual {v2, v4}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(I)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-result-object v8

    .line 89
    invoke-static {v8}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 90
    const/4 v8, 0x1

    invoke-virtual {v2, v4, v8}, Lcom/mfluent/asp/datamodel/filebrowser/a;->setSelected(IZ)V

    .line 87
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 94
    :cond_2
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/a;->b()Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 95
    if-nez v4, :cond_4

    move v7, v3

    .line 101
    :goto_2
    if-eqz v2, :cond_3

    .line 102
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    int-to-long v2, v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    .line 106
    :cond_3
    :goto_3
    array-length v0, p3

    sub-int/2addr v0, v7

    return v0

    .line 95
    :cond_4
    :try_start_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    move v7, v3

    goto :goto_2

    .line 96
    :catch_0
    move-exception v3

    .line 101
    if-eqz v2, :cond_3

    .line 102
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    int-to-long v2, v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    goto :goto_3

    .line 98
    :catch_1
    move-exception v3

    .line 101
    if-eqz v2, :cond_3

    .line 102
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    int-to-long v2, v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    goto :goto_3

    .line 101
    :catchall_0
    move-exception v3

    move-object v7, v3

    if-eqz v2, :cond_5

    .line 102
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    int-to-long v2, v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    :cond_5
    throw v7
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->b:Landroid/content/Context;

    return-object v0
.end method

.method public getCloudDevice()Lcom/mfluent/asp/common/datamodel/CloudDevice;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->a:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method public getCloudStorageFileBrowser(Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/common/datamodel/ASPFileSortType;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->c:Z

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/c;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->a:Lcom/mfluent/asp/datamodel/Device;

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/c;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/content/Context;)V

    .line 58
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a()V

    .line 59
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-static {v1, p1}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-result-object v1

    .line 60
    invoke-virtual {v0, v1, p2, p3}, Lcom/mfluent/asp/datamodel/filebrowser/c;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    .line 61
    const-string v1, "INFO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ASP20FileBrowser new for id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :goto_0
    return-object v0

    .line 64
    :cond_0
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/a;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->a:Lcom/mfluent/asp/datamodel/Device;

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/a;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/content/Context;)V

    .line 65
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a()V

    .line 66
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/b;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-static {v1, p1}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-result-object v1

    .line 67
    invoke-virtual {v0, v1, p2, p3}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 39
    check-cast p1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-static {p1}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
