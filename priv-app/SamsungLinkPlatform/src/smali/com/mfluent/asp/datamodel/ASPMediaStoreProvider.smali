.class public Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;
.super Landroid/content/ContentProvider;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;,
        Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;,
        Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;,
        Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;
    }
.end annotation


# static fields
.field private static final b:Lorg/slf4j/Logger;

.field private static c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

.field private static final g:Landroid/content/UriMatcher;

.field private static final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/datamodel/ao;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/datamodel/a/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/content/BroadcastReceiver;

.field private final d:Lcom/mfluent/asp/util/a/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mfluent/asp/util/a/a/a",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

.field private f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 122
    const-class v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    .line 266
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 267
    sput-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "device"

    invoke-static {}, Lcom/mfluent/asp/datamodel/w$a;->a()Lcom/mfluent/asp/datamodel/w;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "files"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ac;->a()Lcom/mfluent/asp/datamodel/ac;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "document"

    invoke-static {}, Lcom/mfluent/asp/datamodel/z;->l()Lcom/mfluent/asp/datamodel/z;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "document_cross_device"

    invoke-static {}, Lcom/mfluent/asp/datamodel/r;->a()Lcom/mfluent/asp/datamodel/r;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "image"

    invoke-static {}, Lcom/mfluent/asp/datamodel/aj;->l()Lcom/mfluent/asp/datamodel/aj;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "image_cross_device"

    invoke-static {}, Lcom/mfluent/asp/datamodel/s;->a()Lcom/mfluent/asp/datamodel/s;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "image_burst_shot"

    invoke-static {}, Lcom/mfluent/asp/datamodel/p;->a()Lcom/mfluent/asp/datamodel/p;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "video"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ax;->a()Lcom/mfluent/asp/datamodel/ax;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "audio"

    invoke-static {}, Lcom/mfluent/asp/datamodel/j;->a()Lcom/mfluent/asp/datamodel/j;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "audio_cross_device"

    invoke-static {}, Lcom/mfluent/asp/datamodel/q;->l()Lcom/mfluent/asp/datamodel/q;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "album"

    invoke-static {}, Lcom/mfluent/asp/datamodel/d;->g()Lcom/mfluent/asp/datamodel/d;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "artist"

    invoke-static {}, Lcom/mfluent/asp/datamodel/g;->g()Lcom/mfluent/asp/datamodel/g;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "genre"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ad;->g()Lcom/mfluent/asp/datamodel/ad;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "document_journal"

    invoke-static {}, Lcom/mfluent/asp/datamodel/x;->a()Lcom/mfluent/asp/datamodel/x;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "audio_journal"

    invoke-static {}, Lcom/mfluent/asp/datamodel/h;->a()Lcom/mfluent/asp/datamodel/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "image_journal"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ah;->a()Lcom/mfluent/asp/datamodel/ah;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "video_journal"

    invoke-static {}, Lcom/mfluent/asp/datamodel/av;->a()Lcom/mfluent/asp/datamodel/av;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "genre_members"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ae;->a()Lcom/mfluent/asp/datamodel/ae;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "genre_members_sync"

    invoke-static {}, Lcom/mfluent/asp/datamodel/af;->g()Lcom/mfluent/asp/datamodel/af;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "db_integrity"

    invoke-static {}, Lcom/mfluent/asp/datamodel/u;->a()Lcom/mfluent/asp/datamodel/u;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "all"

    invoke-static {}, Lcom/mfluent/asp/datamodel/e;->a()Lcom/mfluent/asp/datamodel/e;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "fileTransferSessions"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ab$a;->a()Lcom/mfluent/asp/datamodel/ab;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "archivedMedia"

    invoke-static {}, Lcom/mfluent/asp/datamodel/f$a;->a()Lcom/mfluent/asp/datamodel/f;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "keywords"

    invoke-static {}, Lcom/mfluent/asp/datamodel/an$a;->a()Lcom/mfluent/asp/datamodel/an;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "file_keywords"

    invoke-static {}, Lcom/mfluent/asp/datamodel/am;->g()Lcom/mfluent/asp/datamodel/am;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "image_keywords"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ai;->a()Lcom/mfluent/asp/datamodel/ai;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "audio_keywords"

    invoke-static {}, Lcom/mfluent/asp/datamodel/i;->a()Lcom/mfluent/asp/datamodel/i;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "video_keywords"

    invoke-static {}, Lcom/mfluent/asp/datamodel/aw;->a()Lcom/mfluent/asp/datamodel/aw;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "document_keywords"

    invoke-static {}, Lcom/mfluent/asp/datamodel/y;->a()Lcom/mfluent/asp/datamodel/y;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "image_thumbs"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ak;->a()Lcom/mfluent/asp/datamodel/ak;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "video_thumbs"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ay;->a()Lcom/mfluent/asp/datamodel/ay;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "album_art"

    invoke-static {}, Lcom/mfluent/asp/datamodel/b;->a()Lcom/mfluent/asp/datamodel/b;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "device_icon"

    invoke-static {}, Lcom/mfluent/asp/datamodel/v;->a()Lcom/mfluent/asp/datamodel/v;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    const-string v1, "geolocation"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ag;->a()Lcom/mfluent/asp/datamodel/ag;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 304
    sput-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "read_lock"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 305
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "thumb_cache/*"

    const/16 v3, 0xe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 306
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "tmpFiles/entry/*"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 307
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "directory_info/#"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 308
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "directory_info/#/*"

    const/16 v3, 0x13

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 309
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "file_browser/#"

    const/16 v3, 0x11

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 310
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "file_browser/#/*"

    const/16 v3, 0x12

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 313
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "file_browser2/#"

    const/16 v3, 0x17

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 314
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "file_browser2/#/*"

    const/16 v3, 0x18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 316
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "search_suggest/search_suggest_regex_query/*"

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 317
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "search_suggest/search_suggest_regex_query"

    const/16 v3, 0x16

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 321
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "search_suggest/search_suggest_query/*"

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 323
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "search_suggest/search_suggest_tag_query/*"

    const/16 v3, 0x19

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 324
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "search_suggest/search_suggest_tag_query"

    const/16 v3, 0x1a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 326
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "*"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 327
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "*/datetaken_grouping"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 328
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "*/digest_grouping"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 329
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "*/unique_grouping"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 330
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "*/file_id_grouping"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 331
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "*/location_grouping"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 332
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "*/entry/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 333
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "*/device/#"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 334
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "*/cleanup/#"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 335
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "*/general_grouping/*"

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 336
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "*/general_grouping_best_device/*"

    const/16 v3, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 338
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 339
    const-string v1, "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.DeleteFile.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/b;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/b;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetScsCoreConfig.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/k;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/k;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/n;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/n;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetAudioUriInfo.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/f;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/f;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetImageUriInfo.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/i;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/i;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetSetting.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/l;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/l;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SetSetting.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/v;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/v;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetDeviceInfo.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/h;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/h;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.RequestNetworkRefresh.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/s;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/s;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SignIn.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/w;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/w;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetSamsungAccountSignInIntent.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/j;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/j;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SetSyncMediaTypePriority.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/u;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/u;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetCloudStorageAccountInfo.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/g;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/g;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.IsAutoUploadInProgress.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/o;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/o;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.IsFileTransferInProgress.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/p;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/p;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SendUsageStat.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/t;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/t;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.DeregisterDevice.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/c;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/c;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetASP10FileBrowserErrorCode.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/e;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/e;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.MRRControl.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/r;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/r;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.FileTransferSessionInfo.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/d;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/d;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVersatileInformation.NAME"

    new-instance v2, Lcom/mfluent/asp/datamodel/a/m;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/a/m;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->i:Ljava/util/Map;

    .line 361
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 436
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 130
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->e:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    .line 502
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$2;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a:Landroid/content/BroadcastReceiver;

    .line 437
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$1;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    .line 453
    return-void
.end method

.method private a(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 14

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 1123
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 1124
    const-string v0, "method_result"

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1126
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "device_id"

    aput-object v0, v2, v1

    const-string v0, "source_media_id"

    aput-object v0, v2, v3

    const-string v0, "bookmark"

    aput-object v0, v2, v4

    const-string v0, "duration"

    aput-object v0, v2, v5

    .line 1127
    const-string v0, "INTENT_ARG_CONTENT_ID"

    const-wide/16 v4, -0x1

    invoke-virtual {p1, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1131
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->b()V

    .line 1133
    :try_start_0
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1134
    if-eqz v0, :cond_1

    .line 1135
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1136
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1137
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1138
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1139
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 1141
    const-string v1, "INTENT_ARG_BOOKMARK"

    invoke-virtual {p1, v1, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 1142
    const-string v1, "INTENT_ARG_DURATION"

    invoke-virtual {p1, v1, v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 1144
    cmp-long v1, v12, v6

    if-nez v1, :cond_0

    cmp-long v1, v10, v4

    if-eqz v1, :cond_4

    .line 1145
    :cond_0
    new-instance v1, Lcom/mfluent/asp/media/m;

    invoke-direct/range {v1 .. v7}, Lcom/mfluent/asp/media/m;-><init>(ILjava/lang/String;JJ)V

    :goto_0
    move-object v8, v1

    .line 1151
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1154
    :cond_1
    if-eqz v8, :cond_2

    .line 1155
    invoke-virtual {v8}, Lcom/mfluent/asp/media/m;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1158
    :cond_2
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    .line 1161
    return-object v9

    .line 1148
    :cond_3
    :try_start_1
    const-string v1, "method_result"

    const/4 v2, -0x1

    invoke-virtual {v9, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1149
    const-string v1, "method_result_str"

    const-string v2, "Record not found."

    invoke-virtual {v9, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1158
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    throw v0

    :cond_4
    move-object v1, v8

    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->e:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

    return-object v0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1731
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    if-eqz v0, :cond_0

    .line 1732
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a(Landroid/net/Uri;)V

    .line 1736
    :goto_0
    return-void

    .line 1734
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->e:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;Lcom/mfluent/asp/datamodel/ao;Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/mfluent/asp/datamodel/ao;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1712
    if-eqz p1, :cond_0

    .line 1713
    invoke-direct {p0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/net/Uri;)V

    .line 1715
    :cond_0
    if-eqz p2, :cond_3

    .line 1716
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/ao;->h()Landroid/net/Uri;

    move-result-object v0

    .line 1717
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1718
    :cond_1
    invoke-direct {p0, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/net/Uri;)V

    .line 1720
    :cond_2
    if-eqz p3, :cond_3

    .line 1721
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1722
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1723
    invoke-virtual {p2, v2, v3}, Lcom/mfluent/asp/datamodel/ao;->b(J)Landroid/net/Uri;

    move-result-object v0

    .line 1724
    invoke-direct {p0, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 1728
    :cond_3
    return-void
.end method

.method private a(Lcom/mfluent/asp/datamodel/ao;Z)V
    .locals 4

    .prologue
    .line 521
    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/ao;->d_()Z

    move-result v0

    if-nez v0, :cond_1

    .line 522
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 523
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v1, "External application {} is trying to access {}"

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 524
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "External access to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is forbidden"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 527
    :cond_1
    return-void
.end method

.method static synthetic a(Ljava/util/Locale;)V
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/database/sqlite/SQLiteDatabase;->setLocale(Ljava/util/Locale;)V

    return-void
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 537
    const-string v0, "com.samsung.android.sdk.samsunglink.permission.PRIVATE_ACCESS"

    invoke-virtual {p0, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1583
    if-nez p1, :cond_0

    .line 1593
    :goto_0
    return-object p0

    .line 1587
    :cond_0
    if-nez p0, :cond_2

    const/4 v0, 0x1

    :goto_1
    new-array v0, v0, [Ljava/lang/String;

    .line 1588
    if-eqz p0, :cond_1

    .line 1589
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1591
    :cond_1
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1

    move-object p0, v0

    .line 1593
    goto :goto_0

    .line 1587
    :cond_2
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private b(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 13

    .prologue
    const/4 v4, -0x1

    const/4 v6, 0x0

    .line 1165
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 1167
    const-string v0, "INTENT_ARG_CONTENT_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 1169
    const-string v0, "INTENT_ARG_PLAYED_TIMESTAMP"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1170
    const-string v0, "INTENT_ARG_PLAYED_TIMESTAMP"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object v7, v0

    .line 1173
    :goto_0
    if-eqz v7, :cond_2

    .line 1174
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->b()V

    .line 1176
    :try_start_0
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 1177
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v12

    .line 1179
    invoke-static {v10, v11}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "isPlayed"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "device_id"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "source_media_id"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1183
    if-eqz v1, :cond_4

    .line 1184
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1185
    const-string v0, "isPlayed"

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1186
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1187
    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    .line 1188
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    move-object v0, v6

    .line 1191
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1193
    :goto_2
    invoke-virtual {v9}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1194
    invoke-static {v10, v11}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v9, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1195
    const-string v1, "method_result"

    const/4 v2, 0x0

    invoke-virtual {v8, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1196
    if-eqz v0, :cond_0

    .line 1197
    const-string v1, "date_modified"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1198
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1205
    :cond_0
    :goto_3
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    .line 1214
    :goto_4
    return-object v8

    .line 1201
    :cond_1
    :try_start_1
    const-string v0, "method_result"

    const/4 v1, -0x1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1202
    const-string v0, "method_result_str"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Record not found with entry id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 1205
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    throw v0

    .line 1208
    :cond_2
    const-string v0, "method_result"

    invoke-virtual {v8, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1209
    const-string v0, "method_result_str"

    const-string v1, "Invalid arguments. Missing INTENT_ARG_PLAYED_TIMESTAMP."

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_3
    move-object v0, v6

    goto :goto_1

    :cond_4
    move-object v0, v6

    goto :goto_2

    :cond_5
    move-object v7, v6

    goto/16 :goto_0
.end method

.method private c(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 13

    .prologue
    const/4 v4, -0x1

    const/4 v6, 0x0

    .line 1218
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 1220
    const-string v0, "INTENT_ARG_CONTENT_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 1222
    const-string v0, "INTENT_ARG_PLAYED_TIMESTAMP"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1223
    const-string v0, "INTENT_ARG_PLAYED_TIMESTAMP"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object v7, v0

    .line 1226
    :goto_0
    if-eqz v7, :cond_2

    .line 1227
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->b()V

    .line 1229
    :try_start_0
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 1230
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v12

    .line 1232
    invoke-static {v10, v11}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "recently_played"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "most_played"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "device_id"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string v3, "source_media_id"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1237
    if-eqz v1, :cond_4

    .line 1238
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1239
    const-string v0, "most_played"

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1240
    const-string v0, "recently_played"

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1241
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1242
    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    .line 1243
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    move-object v0, v6

    .line 1246
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1248
    :goto_2
    invoke-virtual {v9}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1249
    invoke-static {v10, v11}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v9, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1250
    const-string v1, "method_result"

    const/4 v2, 0x0

    invoke-virtual {v8, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1251
    if-eqz v0, :cond_0

    .line 1252
    const-string v1, "date_modified"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1253
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1260
    :cond_0
    :goto_3
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    .line 1269
    :goto_4
    return-object v8

    .line 1256
    :cond_1
    :try_start_1
    const-string v0, "method_result"

    const/4 v1, -0x1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1257
    const-string v0, "method_result_str"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Record not found with entry id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 1260
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    throw v0

    .line 1263
    :cond_2
    const-string v0, "method_result"

    invoke-virtual {v8, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1264
    const-string v0, "method_result_str"

    const-string v1, "Invalid arguments. Missing INTENT_ARG_PLAYED_TIMESTAMP."

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_3
    move-object v0, v6

    goto :goto_1

    :cond_4
    move-object v0, v6

    goto :goto_2

    :cond_5
    move-object v7, v6

    goto/16 :goto_0
.end method

.method static synthetic f()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 517
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Long;)Z
    .locals 1

    .prologue
    .line 942
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1274
    move v1, v2

    .line 1275
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 1276
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation;->isWriteOperation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1277
    const/4 v0, 0x1

    move v1, v0

    .line 1282
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v5, v0, [Landroid/content/ContentProviderResult;

    .line 1286
    if-eqz v1, :cond_2

    .line 1287
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 1288
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/a/a/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1289
    new-instance v4, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-direct {v4, p0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v4, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    move-object v4, v0

    .line 1295
    :goto_2
    if-eqz v1, :cond_0

    .line 1296
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_0
    move v3, v2

    .line 1300
    :goto_3
    :try_start_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 1301
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    .line 1302
    invoke-virtual {v0, p0, v5, v3}, Landroid/content/ContentProviderOperation;->apply(Landroid/content/ContentProvider;[Landroid/content/ContentProviderResult;I)Landroid/content/ContentProviderResult;

    move-result-object v0

    aput-object v0, v5, v3
    :try_end_1
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1300
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 1275
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1291
    :cond_2
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 1292
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/a/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    move-object v4, v0

    goto :goto_2

    .line 1304
    :cond_3
    if-eqz v1, :cond_4

    .line 1305
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->b()V
    :try_end_2
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1312
    :cond_4
    if-eqz v1, :cond_5

    .line 1313
    :try_start_3
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->c()V

    .line 1314
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1318
    :cond_5
    if-eqz v1, :cond_7

    .line 1319
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    .line 1325
    :goto_4
    return-object v5

    .line 1307
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 1308
    :try_start_4
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    .line 1309
    sget-object v3, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error applying batch. Failed : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1310
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1312
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_6

    .line 1313
    :try_start_5
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->c()V

    .line 1314
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    :cond_6
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1318
    :catchall_1
    move-exception v0

    if-eqz v1, :cond_8

    .line 1319
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    .line 1321
    :goto_5
    throw v0

    :cond_7
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_8
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_9
    move v1, v2

    goto/16 :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 541
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->b()V

    .line 544
    :cond_0
    return-void
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 1330
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    invoke-interface {v0}, Lorg/slf4j/Logger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1331
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1332
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v1, "external bulkInsert for uri {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1338
    :cond_0
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1343
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1340
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1346
    sget-object v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/mfluent/asp/datamodel/ao;

    .line 1347
    const/4 v0, 0x1

    invoke-direct {p0, v6, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Lcom/mfluent/asp/datamodel/ao;Z)V

    .line 1348
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 1352
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1354
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/a/a/a;->c()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/Long;

    .line 1357
    :try_start_0
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-direct {v0, p0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    .line 1358
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1360
    :try_start_1
    new-instance v0, Lcom/mfluent/asp/datamodel/ao$a;

    iget-object v4, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    iget-object v5, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ao$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;Lcom/mfluent/asp/util/a/a/a;)V

    move v2, v8

    move v1, v8

    .line 1362
    :goto_0
    array-length v3, p2

    if-ge v2, v3, :cond_3

    .line 1363
    aget-object v3, p2, v2

    const-string v4, "device_id"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 1364
    aget-object v4, p2, v2

    aget-object v5, p2, v2

    invoke-virtual {v6, v0, v4, v5}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 1370
    const-wide/16 v10, 0x0

    cmp-long v4, v4, v10

    if-lez v4, :cond_2

    .line 1371
    if-eqz v3, :cond_1

    .line 1372
    invoke-virtual {v9, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1374
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 1362
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1378
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1380
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->c()V

    .line 1381
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1384
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    .line 1387
    if-lez v1, :cond_4

    .line 1390
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->e:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/ao;->h()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->a(Landroid/net/Uri;)V

    .line 1391
    invoke-virtual {v9}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1392
    iget-object v3, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->e:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v6, v4, v5}, Lcom/mfluent/asp/datamodel/ao;->b(J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->a(Landroid/net/Uri;)V

    goto :goto_1

    .line 1380
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->c()V

    .line 1381
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1384
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v7}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    throw v0

    .line 1396
    :cond_4
    return v1

    .line 1338
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 547
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 548
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    .line 550
    :cond_0
    return-void
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 948
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    invoke-interface {v0}, Lorg/slf4j/Logger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 949
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 950
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v2, "external call for method {} and arg {}"

    invoke-interface {v0, v2, p1, p2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 954
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->b()V

    .line 956
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/a/a;

    .line 957
    if-eqz v0, :cond_2

    .line 958
    invoke-interface {v0}, Lcom/mfluent/asp/datamodel/a/a;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 959
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v2, "Unauthorized call for method {}"

    invoke-interface {v0, v2, p1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 960
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    .line 1118
    :goto_0
    return-object v1

    .line 962
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Lcom/mfluent/asp/datamodel/a/a;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 1118
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    goto :goto_0

    .line 968
    :cond_2
    :try_start_2
    const-string v0, "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.GetDatabaseIntegrityValue.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 969
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 970
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "files_table_validity"

    invoke-static {v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->getValue(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 973
    const-string v2, "method_result"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1118
    :goto_1
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    move-object v1, v0

    goto :goto_0

    .line 974
    :cond_3
    :try_start_3
    const-string v0, "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.GetSignInStatus.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 975
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 976
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/a;->h()Z

    move-result v1

    .line 977
    const-string v2, "method_result"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1118
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    throw v0

    .line 978
    :cond_4
    :try_start_4
    const-string v0, "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.GetUIAppAvailabilityInSamsungAppStore.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 980
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_5

    .line 981
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Do not call isUIAppAvailableInSamsungAppStore() from Main Thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 984
    :cond_5
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 985
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/hybrid/update/b;->b(Landroid/content/Context;)Z

    move-result v1

    .line 986
    const-string v2, "method_result"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 987
    :cond_6
    const-string v0, "com.sec.samsunglink.api.SamsungLinkMediaStore.CallMethods.Is3boxSupported.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 988
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 989
    const-string v1, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_SOURCE_DEVICE_ID"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 990
    sget-object v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v2, "Call method {} missing required extra \'{}\'"

    const-string v3, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_SOURCE_DEVICE_ID"

    invoke-interface {v1, v2, p1, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 991
    :cond_7
    const-string v1, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_TARGET_DEVICE_ID"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 992
    sget-object v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v2, "Call method {} missing required extra \'{}\'"

    const-string v3, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_TARGET_DEVICE_ID"

    invoke-interface {v1, v2, p1, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 994
    :cond_8
    const-string v1, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_SOURCE_DEVICE_ID"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 995
    const-string v1, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.Is3boxSupported.EXTRA_TARGET_DEVICE_ID"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 997
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    .line 998
    long-to-int v6, v2

    int-to-long v6, v6

    invoke-virtual {v1, v6, v7}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v6

    .line 999
    long-to-int v7, v4

    int-to-long v8, v7

    invoke-virtual {v1, v8, v9}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 1001
    if-nez v6, :cond_9

    .line 1002
    sget-object v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v4, "Device {} does not exist"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v4, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1003
    :cond_9
    if-nez v1, :cond_a

    .line 1004
    sget-object v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v2, "Device {} does not exist"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1007
    :cond_a
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v6, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1008
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1010
    const/4 v1, 0x0

    .line 1024
    :goto_2
    const-string v2, "method_result"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 1013
    :cond_b
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->B()Z

    move-result v1

    goto :goto_2

    .line 1016
    :cond_c
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1018
    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->B()Z

    move-result v1

    goto :goto_2

    .line 1021
    :cond_d
    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->A()Z

    move-result v1

    goto :goto_2

    .line 1027
    :cond_e
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.UpdateMediaBookmark.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1028
    invoke-direct {p0, p3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    goto/16 :goto_1

    .line 1029
    :cond_f
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.UpdateVideoPlayedTimestamp.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1030
    invoke-direct {p0, p3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    goto/16 :goto_1

    .line 1031
    :cond_10
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.UpdateAudioPlayedTimestamp.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1032
    invoke-direct {p0, p3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    goto/16 :goto_1

    .line 1033
    :cond_11
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.IsInitialized.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1034
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1035
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v1

    invoke-virtual {v1}, Lpcloud/net/nat/c;->i()Z

    move-result v1

    .line 1036
    const-string v2, "method_result"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 1037
    :cond_12
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.NetworkLockRequested.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1039
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1040
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    invoke-virtual {v0}, Lpcloud/net/nat/c;->h()V

    move-object v0, v1

    goto/16 :goto_1

    .line 1042
    :cond_13
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.UpdateUserDeviceName.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1043
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1044
    const-string v0, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.UpdateUserDeviceName.EXTRA_DEVICE_NAME"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 1045
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v2, "Call method {} missing required extra \'{}\'"

    const-string v3, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.UpdateUserDeviceName.EXTRA_DEVICE_NAME"

    invoke-interface {v0, v2, p1, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto/16 :goto_1

    .line 1046
    :cond_14
    const-string v0, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.UpdateUserDeviceName.EXTRA_DEVICE_IMEI"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 1047
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v2, "Call method {} missing required extra \'{}\'"

    const-string v3, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.UpdateUserDeviceName.EXTRA_DEVICE_IMEI"

    invoke-interface {v0, v2, p1, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto/16 :goto_1

    .line 1049
    :cond_15
    const-string v0, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.UpdateUserDeviceName.EXTRA_DEVICE_NAME"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1050
    const-string v0, "com.samsung.android.sdk.samsunglink.SamsungLinkMediaStore.CallMethods.UpdateUserDeviceName.EXTRA_DEVICE_IMEI"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1051
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v3

    .line 1052
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1053
    invoke-virtual {v3, v1, v2}, Lcom/mfluent/asp/b/i;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1054
    if-eqz v3, :cond_16

    .line 1055
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v4

    .line 1056
    invoke-virtual {v4, v2}, Lcom/mfluent/asp/datamodel/t;->b(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    .line 1057
    invoke-virtual {v2, v1}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    .line 1059
    :cond_16
    const-string v1, "method_result"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 1062
    :cond_17
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.TurnOnDevice.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1063
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1064
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v1

    .line 1065
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1066
    invoke-virtual {v1, p2}, Lcom/mfluent/asp/b/i;->b(Ljava/lang/String;)Z

    move-result v1

    .line 1067
    const-string v2, "method_result"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 1069
    :cond_18
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SendWakeupPushInBackground.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1070
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1071
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v1

    .line 1072
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1073
    invoke-virtual {v1}, Lcom/mfluent/asp/b/i;->e()V

    goto/16 :goto_1

    .line 1075
    :cond_19
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SetDeviceInfo.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1076
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1077
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v0

    .line 1078
    invoke-virtual {v0}, Lcom/mfluent/asp/b/i;->c()Ljava/lang/String;

    move-object v0, v1

    .line 1079
    goto/16 :goto_1

    .line 1080
    :cond_1a
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SignOutOfStorageService.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1081
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1082
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1083
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    .line 1084
    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 1086
    new-instance v2, Lcom/mfluent/asp/ui/StorageSignInOutHelper;

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/mfluent/asp/ui/StorageSignInOutHelper;-><init>(Landroid/content/Context;)V

    .line 1087
    invoke-virtual {v2, v0}, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->a(Lcom/mfluent/asp/datamodel/Device;)V

    move-object v0, v1

    .line 1088
    goto/16 :goto_1

    .line 1089
    :cond_1b
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetAuthInfo.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1090
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1091
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v1

    .line 1092
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1094
    :try_start_5
    invoke-virtual {v1}, Lcom/mfluent/asp/a;->e()Ljava/lang/String;

    move-result-object v1

    .line 1095
    const-string v2, "method_result"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 1096
    :catch_0
    move-exception v1

    .line 1097
    :try_start_6
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v3, "::getEncryptedAuthInfo: unable to get authinfo."

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 1100
    :cond_1c
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.IsUpgradeAvailable.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1101
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1103
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/util/y;->a(Landroid/content/Context;)Lcom/mfluent/asp/util/y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/util/y;->d()Z

    move-result v1

    .line 1104
    const-string v2, "method_result"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 1105
    :cond_1d
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SetAllowedDMRIP.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1106
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SetAllowedDMRIP.EXTRA_IP"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1107
    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.SetAllowedDMRIP.EXTRA_DEVICE_ID"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 1108
    invoke-static {v2, v0}, Lcom/sec/pcw/util/b;->a(ILjava/lang/String;)V

    move-object v0, v1

    .line 1109
    goto/16 :goto_1

    :cond_1e
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.ClearDMRIP.NAME"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 1110
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.ClearDMRIP.EXTRA_DEVICE_ID"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1111
    invoke-static {v0}, Lcom/sec/pcw/util/b;->a(I)V

    move-object v0, v1

    .line 1112
    goto/16 :goto_1

    .line 1113
    :cond_1f
    invoke-super {p0, p1, p2, p3}, Landroid/content/ContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    goto/16 :goto_1

    :cond_20
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 553
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 1480
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    invoke-interface {v0}, Lorg/slf4j/Logger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1481
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1482
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v1, "external delete for uri {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1489
    :cond_0
    const/4 v2, -0x1

    .line 1490
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1491
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    .line 1492
    packed-switch v4, :pswitch_data_0

    .line 1517
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1494
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move v8, v2

    move v9, v3

    move-object v1, v10

    .line 1520
    :goto_0
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/mfluent/asp/datamodel/ao;

    .line 1521
    invoke-direct {p0, v6, v5}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Lcom/mfluent/asp/datamodel/ao;Z)V

    .line 1522
    if-nez v9, :cond_7

    .line 1525
    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/ao;->k()Ljava/lang/String;

    move-result-object v2

    .line 1526
    if-eqz v2, :cond_7

    .line 1527
    if-eqz v1, :cond_3

    .line 1528
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND media_type=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1533
    :goto_1
    invoke-static {p3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p3

    .line 1537
    :goto_2
    if-nez p2, :cond_4

    move-object p2, v0

    .line 1544
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/a/a/a;->c()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/Long;

    .line 1546
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1548
    new-instance v0, Lcom/mfluent/asp/datamodel/ao$a;

    iget-object v5, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ao$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;ILcom/mfluent/asp/util/a/a/a;)V

    .line 1550
    if-eqz v9, :cond_5

    .line 1551
    invoke-virtual {v6, v0, p2, p3, v8}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;[Ljava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1560
    :goto_4
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v7}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    .line 1569
    if-lez v0, :cond_2

    if-nez v9, :cond_2

    .line 1571
    invoke-direct {p0, v10, v6, v11}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/net/Uri;Lcom/mfluent/asp/datamodel/ao;Ljava/util/Collection;)V

    .line 1573
    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1574
    const-string v1, ""

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1, v10, v10}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/net/Uri;Lcom/mfluent/asp/datamodel/ao;Ljava/util/Collection;)V

    .line 1579
    :cond_2
    return v0

    .line 1497
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1498
    const-string v6, "_id=?"

    .line 1499
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1500
    invoke-static {p3, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p3

    .line 1501
    const-string v7, "device"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1502
    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v8, v2

    move v9, v3

    move-object v1, v6

    goto/16 :goto_0

    .line 1507
    :pswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move v8, v2

    move v9, v5

    move-object v1, v10

    .line 1508
    goto/16 :goto_0

    .line 1510
    :pswitch_3
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1511
    const-string v6, "device_id=?"

    .line 1512
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1513
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1514
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p3, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p3

    move v8, v2

    move v9, v3

    move-object v1, v6

    .line 1515
    goto/16 :goto_0

    .line 1530
    :cond_3
    const-string v0, "media_type=?"

    goto/16 :goto_1

    .line 1539
    :cond_4
    if-eqz v0, :cond_1

    .line 1540
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_3

    .line 1553
    :cond_5
    :try_start_1
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_6

    .line 1554
    invoke-virtual {v6, v0, p2, p3, v11}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;[Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1556
    :cond_6
    invoke-virtual {v6, v0, p2, p3}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto/16 :goto_4

    .line 1560
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v7}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    throw v0

    :cond_7
    move-object v0, v1

    goto/16 :goto_2

    :cond_8
    move v8, v2

    move v9, v3

    move-object v1, v6

    goto/16 :goto_0

    .line 1492
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method protected final e()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 938
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/a/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 838
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    invoke-interface {v0}, Lorg/slf4j/Logger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 839
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 840
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v1, "external getStreamTypes for uri {} and mimeTypeFilter {}"

    invoke-interface {v0, v1, p1, p2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 846
    :cond_0
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_1
    move-object v1, v8

    .line 874
    :goto_0
    if-eqz v1, :cond_4

    .line 875
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 876
    array-length v3, v1

    move v0, v9

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v1, v0

    .line 877
    invoke-static {v4, p2}, Landroid/content/ClipDescription;->compareMimeTypes(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 878
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 876
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 848
    :sswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 849
    sget-object v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/mfluent/asp/datamodel/ao;

    .line 850
    if-eqz v6, :cond_1

    .line 851
    invoke-direct {p0, v6, v9}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Lcom/mfluent/asp/datamodel/ao;Z)V

    .line 852
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 853
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/a/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/Long;

    .line 855
    :try_start_0
    new-instance v0, Lcom/mfluent/asp/datamodel/ao$a;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ao$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;ILcom/mfluent/asp/util/a/a/a;)V

    .line 856
    invoke-virtual {v6, v0}, Lcom/mfluent/asp/datamodel/ao;->b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 858
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v7}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 859
    goto :goto_0

    .line 858
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v7}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    throw v0

    .line 861
    :sswitch_1
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 867
    if-eqz v1, :cond_1

    .line 868
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    aput-object v1, v0, v9

    move-object v1, v0

    goto :goto_0

    .line 881
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 884
    :goto_2
    return-object v0

    :cond_4
    move-object v0, v8

    goto :goto_2

    .line 846
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x6 -> :sswitch_1
        0xe -> :sswitch_1
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 769
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    invoke-interface {v0}, Lorg/slf4j/Logger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 770
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 771
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v1, "external getType for uri {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 778
    :cond_0
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 803
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 787
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move v1, v2

    .line 806
    :goto_0
    sget-object v3, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/ao;

    .line 807
    invoke-direct {p0, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Lcom/mfluent/asp/datamodel/ao;Z)V

    .line 809
    if-eqz v1, :cond_1

    .line 810
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ao;->e_()Ljava/lang/String;

    move-result-object v0

    .line 812
    :goto_1
    return-object v0

    .line 790
    :pswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 791
    const/4 v1, 0x1

    .line 792
    goto :goto_0

    .line 795
    :pswitch_3
    const-string v0, "vnd.android.cursor.item/vnd.android.search.suggest"

    goto :goto_1

    .line 798
    :pswitch_4
    invoke-static {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$TemporaryFiles;->getFileForUri(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    .line 799
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 812
    :cond_1
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ao;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 778
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 12

    .prologue
    const/4 v2, 0x1

    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    .line 1411
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    invoke-interface {v0}, Lorg/slf4j/Logger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1412
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1413
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v1, "external insert for uri {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1419
    :cond_0
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1424
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1421
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1427
    sget-object v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/mfluent/asp/datamodel/ao;

    .line 1428
    invoke-direct {p0, v6, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Lcom/mfluent/asp/datamodel/ao;Z)V

    .line 1429
    const-string v0, "device_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    .line 1432
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/a/a/a;->c()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/Long;

    .line 1435
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1438
    new-instance v0, Lcom/mfluent/asp/datamodel/ao$a;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ao$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;ILcom/mfluent/asp/util/a/a/a;)V

    invoke-virtual {v6, v0, p2, p2}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 1439
    cmp-long v0, v2, v10

    if-gtz v0, :cond_2

    .line 1440
    :try_start_1
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Row ID is <= 0! "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1442
    :catch_0
    move-exception v0

    .line 1443
    :goto_0
    :try_start_2
    sget-object v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Insert failed for uri: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1445
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    .line 1449
    :goto_1
    cmp-long v0, v2, v10

    if-lez v0, :cond_1

    .line 1450
    invoke-virtual {v6, v2, v3}, Lcom/mfluent/asp/datamodel/ao;->a(J)Landroid/net/Uri;

    move-result-object v1

    .line 1451
    if-nez v9, :cond_3

    move-object v0, v8

    .line 1455
    :goto_2
    invoke-direct {p0, v8, v6, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/net/Uri;Lcom/mfluent/asp/datamodel/ao;Ljava/util/Collection;)V

    move-object v8, v1

    .line 1460
    :cond_1
    return-object v8

    .line 1445
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v7}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    throw v0

    .line 1451
    :cond_3
    invoke-virtual {v9}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_2

    .line 1442
    :catch_1
    move-exception v0

    move-wide v2, v10

    goto :goto_0

    .line 1419
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 468
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    if-nez v0, :cond_0

    .line 469
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    .line 471
    :cond_0
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 473
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UPDATE DEVICES SET network_mode=\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\', device_priority=((16777215 & device_priority) | 67108864) WHERE transport_type=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\';"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 493
    invoke-static {p0}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    .line 496
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 497
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 498
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 499
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/high16 v4, 0x10000000

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 889
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    invoke-interface {v0}, Lorg/slf4j/Logger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 890
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 891
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v1, "external openFile for uri {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 895
    :cond_0
    const-string v0, "r"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 898
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Content is read-only."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 901
    :cond_1
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 931
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 903
    :sswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 904
    sget-object v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/mfluent/asp/datamodel/ao;

    .line 905
    if-nez v6, :cond_2

    .line 906
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 909
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 911
    invoke-direct {p0, v6, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Lcom/mfluent/asp/datamodel/ao;Z)V

    .line 912
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 914
    :try_start_0
    new-instance v0, Lcom/mfluent/asp/datamodel/ao$a;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ao$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;ILcom/mfluent/asp/util/a/a/a;)V

    invoke-virtual {v6, v0, v8, v9}, Lcom/mfluent/asp/datamodel/ao;->a_(Lcom/mfluent/asp/datamodel/ao$a;J)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 934
    :goto_0
    return-object v0

    .line 916
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 917
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Interrupted while attempting to openFile for uri "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 922
    :sswitch_1
    invoke-static {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$TemporaryFiles;->getFileForUri(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    .line 923
    invoke-static {v0, v4}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0

    .line 926
    :sswitch_2
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/media/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/media/d;

    move-result-object v0

    .line 927
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lcom/mfluent/asp/media/d;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 928
    invoke-static {v1, v4}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0

    .line 901
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x6 -> :sswitch_1
        0xe -> :sswitch_2
    .end sparse-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 16

    .prologue
    .line 569
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    invoke-interface {v2}, Lorg/slf4j/Logger;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 570
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 571
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v3, "external query for {}, {}, {}, {}, {}"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static/range {p2 .. p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object p3, v4, v5

    const/4 v5, 0x3

    invoke-static/range {p4 .. p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    aput-object p5, v4, v5

    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 577
    :cond_0
    const/4 v5, 0x0

    .line 578
    const/4 v9, 0x0

    .line 580
    const/4 v4, 0x0

    .line 581
    const-string v2, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 582
    const-wide/16 v12, -0x1

    .line 584
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    .line 585
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    .line 586
    packed-switch v6, :pswitch_data_0

    .line 719
    :pswitch_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown URI "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 588
    :pswitch_1
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v10, v4

    move-object v3, v5

    .line 724
    :goto_0
    const/4 v15, 0x0

    .line 726
    if-eqz v2, :cond_5

    .line 727
    sget-object v4, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/mfluent/asp/datamodel/ao;

    .line 728
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Lcom/mfluent/asp/datamodel/ao;Z)V

    .line 729
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v2}, Lcom/mfluent/asp/util/a/a/a;->b()Ljava/lang/Object;

    move-result-object v2

    move-object v14, v2

    check-cast v14, Ljava/lang/Long;

    .line 732
    if-nez p3, :cond_4

    move-object/from16 p3, v3

    .line 738
    :cond_1
    :goto_1
    :try_start_0
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 739
    new-instance v2, Lcom/mfluent/asp/datamodel/ao$a;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    invoke-direct/range {v2 .. v7}, Lcom/mfluent/asp/datamodel/ao$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;ILcom/mfluent/asp/util/a/a/a;)V

    move-object v3, v8

    move-object v4, v2

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    .line 741
    invoke-virtual/range {v3 .. v13}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 745
    if-eqz v2, :cond_2

    .line 746
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v2, v3, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 751
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v3, v14}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    .line 754
    :goto_2
    return-object v2

    .line 591
    :pswitch_2
    const-string v2, "device_priority = MIN(device_priority)"

    move-object v4, v2

    .line 594
    :pswitch_3
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 595
    const/4 v7, 0x2

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v10, v4

    move-object v9, v3

    move-object v3, v5

    .line 597
    goto :goto_0

    .line 599
    :pswitch_4
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 600
    const-string v9, "date_code"

    move-object v10, v4

    move-object v3, v5

    .line 601
    goto :goto_0

    .line 603
    :pswitch_5
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 604
    const-string v9, "_id"

    .line 605
    const-string v4, "keyword_type = MIN(keyword_type)"

    move-object v10, v4

    move-object v3, v5

    .line 606
    goto/16 :goto_0

    .line 608
    :pswitch_6
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 609
    const-string v5, "_id=?"

    .line 610
    const/4 v7, 0x2

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 611
    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 612
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    move-object v10, v4

    move-object v3, v5

    .line 613
    goto/16 :goto_0

    .line 615
    :pswitch_7
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 616
    const-string v5, "device_id=?"

    .line 617
    const/4 v7, 0x2

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    move-object v10, v4

    move-object v3, v5

    .line 618
    goto/16 :goto_0

    .line 620
    :pswitch_8
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 621
    const-string v9, "geo_loc_country,geo_loc_province,geo_loc_locality,geo_loc_sub_locality"

    .line 628
    const-string v4, "date_added=MAX(date_added)"

    move-object v10, v4

    move-object v3, v5

    .line 629
    goto/16 :goto_0

    .line 631
    :pswitch_9
    invoke-static/range {p1 .. p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$TemporaryFiles;->getFileForUri(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v3

    .line 632
    new-instance v2, Lcom/mfluent/asp/datamodel/aa;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/datamodel/aa;-><init>(Ljava/io/File;)V

    goto/16 :goto_2

    .line 637
    :pswitch_a
    const/4 v3, 0x0

    .line 638
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->b()V

    .line 640
    :try_start_2
    new-instance v2, Lcom/mfluent/asp/datamodel/ar;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1, v6}, Lcom/mfluent/asp/datamodel/ar;-><init>(Landroid/net/Uri;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 644
    :goto_3
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    goto/16 :goto_2

    .line 641
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v2, v3

    goto :goto_3

    .line 650
    :pswitch_b
    const/4 v3, 0x0

    .line 651
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->b()V

    .line 653
    :try_start_3
    sget-boolean v2, Lcom/mfluent/asp/ASPApplication;->n:Z

    if-eqz v2, :cond_3

    .line 654
    new-instance v2, Lcom/mfluent/asp/datamodel/as;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1}, Lcom/mfluent/asp/datamodel/as;-><init>(Landroid/net/Uri;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 659
    :goto_4
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c()V

    goto/16 :goto_2

    .line 656
    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    move-object v2, v3

    goto :goto_4

    .line 663
    :pswitch_c
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 666
    :pswitch_d
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getDeviceIdFromUri(Landroid/net/Uri;)J

    move-result-wide v4

    .line 667
    const/4 v2, 0x0

    .line 669
    const/16 v3, 0x12

    if-ne v6, v3, :cond_8

    .line 670
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getDirectoryIdFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 674
    :goto_5
    :try_start_4
    new-instance v2, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v7, p2

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;-><init>(Landroid/content/Context;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_2

    .line 676
    :catch_2
    move-exception v2

    const/4 v2, 0x0

    goto/16 :goto_2

    .line 678
    :catch_3
    move-exception v2

    const/4 v2, 0x0

    goto/16 :goto_2

    .line 685
    :pswitch_e
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList2;->getDeviceIdFromUri(Landroid/net/Uri;)J

    move-result-wide v4

    .line 686
    const/4 v2, 0x0

    .line 688
    const/16 v3, 0x18

    if-ne v6, v3, :cond_7

    .line 689
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList2;->getDirectoryIdFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 692
    :goto_6
    :try_start_5
    new-instance v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v7, p2

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;-><init>(Landroid/content/Context;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_2

    .line 694
    :catch_4
    move-exception v2

    const/4 v2, 0x0

    goto/16 :goto_2

    .line 696
    :catch_5
    move-exception v2

    const/4 v2, 0x0

    goto/16 :goto_2

    .line 701
    :pswitch_f
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$DirectoryInfo;->getDeviceIdFromUri(Landroid/net/Uri;)J

    move-result-wide v4

    .line 702
    const/4 v2, 0x0

    .line 704
    const/16 v3, 0x13

    if-ne v6, v3, :cond_6

    .line 705
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$DirectoryInfo;->getDirectoryIdFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 709
    :goto_7
    :try_start_6
    new-instance v2, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v7, p2

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;-><init>(Landroid/content/Context;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7

    goto/16 :goto_2

    .line 711
    :catch_6
    move-exception v2

    const/4 v2, 0x0

    goto/16 :goto_2

    .line 713
    :catch_7
    move-exception v2

    const/4 v2, 0x0

    goto/16 :goto_2

    .line 717
    :pswitch_10
    new-instance v2, Lcom/mfluent/asp/datamodel/aq;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/mfluent/asp/datamodel/aq;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    goto/16 :goto_2

    .line 734
    :cond_4
    if-eqz v3, :cond_1

    .line 735
    :try_start_7
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "("

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") AND "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object p3

    goto/16 :goto_1

    .line 748
    :catch_8
    move-exception v2

    move-object v3, v2

    move-object v2, v15

    .line 749
    :goto_8
    :try_start_8
    sget-object v4, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error in query for Uri: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 751
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v3, v14}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v3, v14}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    throw v2

    .line 748
    :catch_9
    move-exception v3

    goto :goto_8

    :cond_5
    move-object v2, v15

    goto/16 :goto_2

    :cond_6
    move-object v6, v2

    goto :goto_7

    :cond_7
    move-object v6, v2

    goto/16 :goto_6

    :cond_8
    move-object v6, v2

    goto/16 :goto_5

    .line 586
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_8
        :pswitch_a
        :pswitch_a
        :pswitch_c
        :pswitch_3
        :pswitch_2
        :pswitch_d
        :pswitch_d
        :pswitch_f
        :pswitch_f
        :pswitch_10
        :pswitch_a
        :pswitch_e
        :pswitch_e
        :pswitch_b
        :pswitch_b
    .end packed-switch
.end method

.method public shutdown()V
    .locals 3

    .prologue
    .line 1740
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/a/a/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1742
    :try_start_0
    sget-object v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1744
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    .line 1745
    return-void

    .line 1744
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    throw v1
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 14

    .prologue
    .line 1624
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    invoke-interface {v2}, Lorg/slf4j/Logger;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1625
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1626
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b:Lorg/slf4j/Logger;

    const-string v3, "external update for uri {}"

    invoke-interface {v2, v3, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1631
    :cond_0
    const/4 v3, 0x0

    .line 1632
    const-wide/16 v8, 0x0

    .line 1633
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1634
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->g:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    .line 1635
    packed-switch v6, :pswitch_data_0

    .line 1654
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown URI "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1637
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1657
    :goto_0
    sget-object v4, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->h:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Lcom/mfluent/asp/datamodel/ao;

    .line 1658
    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/ao;->k()Ljava/lang/String;

    move-result-object v4

    .line 1659
    if-eqz v4, :cond_7

    .line 1660
    if-eqz v3, :cond_4

    .line 1661
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND media_type=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1666
    :goto_1
    move-object/from16 v0, p4

    invoke-static {v0, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 1669
    :goto_2
    if-nez p3, :cond_5

    move-object/from16 p3, v2

    .line 1675
    :cond_1
    :goto_3
    const/4 v2, 0x1

    invoke-direct {p0, v11, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Lcom/mfluent/asp/datamodel/ao;Z)V

    .line 1677
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v2}, Lcom/mfluent/asp/util/a/a/a;->c()Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Ljava/lang/Long;

    .line 1680
    :try_start_0
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 1681
    new-instance v2, Lcom/mfluent/asp/datamodel/ao$a;

    iget-object v7, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    move-object v3, p0

    move-object v5, p1

    invoke-direct/range {v2 .. v7}, Lcom/mfluent/asp/datamodel/ao$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;ILcom/mfluent/asp/util/a/a/a;)V

    move-object v3, v11

    move-object v4, v2

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v10, p2

    .line 1682
    invoke-virtual/range {v3 .. v10}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;JLandroid/content/ContentValues;)I

    move-result v3

    .line 1684
    invoke-virtual {v13}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1685
    const-string v4, "device_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1686
    if-eqz v4, :cond_6

    .line 1687
    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1694
    :cond_2
    :goto_4
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v2, v12}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    .line 1703
    if-lez v3, :cond_3

    .line 1704
    invoke-direct {p0, p1, v11, v13}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Landroid/net/Uri;Lcom/mfluent/asp/datamodel/ao;Ljava/util/Collection;)V

    .line 1708
    :cond_3
    return v3

    .line 1640
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1641
    const-string v4, "_id=?"

    .line 1642
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    const/4 v5, 0x2

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 1643
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    move-object v3, v4

    .line 1644
    goto/16 :goto_0

    .line 1646
    :pswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1647
    const-string v4, "device_id=?"

    .line 1649
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    const/4 v5, 0x2

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1650
    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1651
    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    move-object v3, v4

    .line 1652
    goto/16 :goto_0

    .line 1663
    :cond_4
    const-string v2, "media_type=?"

    goto/16 :goto_1

    .line 1671
    :cond_5
    if-eqz v2, :cond_1

    .line 1672
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_3

    .line 1689
    :cond_6
    :try_start_1
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v11, v2, v0, v1, v13}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;[Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_4

    .line 1694
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v3, v12}, Lcom/mfluent/asp/util/a/a/a;->b(Ljava/lang/Object;)Z

    throw v2

    :cond_7
    move-object v2, v3

    goto/16 :goto_2

    .line 1635
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
