.class abstract Lcom/mfluent/asp/datamodel/a;
.super Lcom/mfluent/asp/datamodel/ap;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/mfluent/asp/datamodel/ap;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "local_source_album_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/a;->a:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ap;-><init>()V

    .line 32
    new-instance v0, Landroid/util/LruCache;

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/a;->c()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/a;->b:Landroid/util/LruCache;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;
    .locals 13

    .prologue
    .line 49
    invoke-static/range {p6 .. p6}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 50
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/a;->a()Ljava/lang/String;

    move-result-object v7

    .line 53
    :goto_0
    invoke-static/range {p7 .. p7}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 54
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/a;->b()Ljava/lang/String;

    move-result-object v8

    .line 57
    :goto_1
    if-nez p2, :cond_0

    .line 58
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/datamodel/a;->a(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;

    move-result-object p2

    move-object v3, p2

    :goto_2
    move-object v1, p0

    move-object v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v9, p8

    move-wide/from16 v10, p9

    .line 73
    invoke-super/range {v1 .. v11}, Lcom/mfluent/asp/datamodel/ap;->a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 60
    :cond_0
    const-string v0, "*"

    invoke-static {p2, v0}, Lorg/apache/commons/lang3/ArrayUtils;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 61
    if-ltz v0, :cond_2

    .line 62
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/datamodel/a;->a(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;

    move-result-object v1

    .line 63
    array-length v2, p2

    add-int/lit8 v2, v2, -0x1

    array-length v3, v1

    add-int/2addr v2, v3

    new-array v2, v2, [Ljava/lang/String;

    .line 64
    if-lez v0, :cond_1

    .line 65
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p2, v3, v2, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    :cond_1
    const/4 v3, 0x0

    array-length v4, v1

    invoke-static {v1, v3, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 68
    array-length v3, p2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_2

    .line 69
    add-int/lit8 v3, v0, 0x1

    array-length v1, v1

    add-int/2addr v1, v0

    array-length v4, p2

    sub-int v0, v4, v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p2, v3, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    move-object v3, p2

    goto :goto_2

    :cond_3
    move-object/from16 v8, p7

    goto :goto_1

    :cond_4
    move-object/from16 v7, p6

    goto :goto_0
.end method

.method protected final a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/Object;)Ljava/lang/Integer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/datamodel/ao$a;",
            "TT;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a;->b:Landroid/util/LruCache;

    invoke-virtual {v0, p2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 86
    if-nez v0, :cond_0

    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    if-eqz v1, :cond_0

    .line 87
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v1, p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a(Lcom/mfluent/asp/datamodel/ao;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 88
    if-eqz v1, :cond_0

    .line 89
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 93
    :cond_0
    return-object v0
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method public a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 113
    check-cast p3, Ljava/util/HashMap;

    .line 114
    invoke-virtual {p3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 115
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 116
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/a;->b:Landroid/util/LruCache;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 118
    :cond_0
    return-void
.end method

.method protected final a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/Object;Ljava/lang/Integer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/datamodel/ao$a;",
            "TT;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0, p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a(Lcom/mfluent/asp/datamodel/ao;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 100
    if-nez v0, :cond_0

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 102
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v1, p0, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a(Lcom/mfluent/asp/datamodel/ao;Ljava/lang/Object;)V

    .line 104
    :cond_0
    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a;->b:Landroid/util/LruCache;

    invoke-virtual {v0, p2, p3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a;->b:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    return-void
.end method

.method protected abstract a(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
.end method

.method protected final a_()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/mfluent/asp/datamodel/a;->a:[Ljava/lang/String;

    return-object v0
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract c()I
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a;->b:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->size()I

    move-result v0

    return v0
.end method
