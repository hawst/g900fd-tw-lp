.class public final Lcom/mfluent/asp/datamodel/e;
.super Lcom/mfluent/asp/datamodel/n;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/e$a;
    }
.end annotation


# static fields
.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "local_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "local_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "local_source_media_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "local_source_album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "local_caption_path"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "local_caption_index_path"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/e;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/n;-><init>()V

    .line 40
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/e;-><init>()V

    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/e;
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/mfluent/asp/datamodel/e$a;->a()Lcom/mfluent/asp/datamodel/e;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final a_()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/mfluent/asp/datamodel/e;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const-string v0, "ALL_MEDIA"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d_()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$AllMediaSearch;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$AllMediaSearch;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
