.class final enum Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FieldValues"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum b:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum c:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum d:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum e:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum f:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum g:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum h:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum i:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum j:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum k:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum l:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum m:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum n:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum o:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field public static final enum p:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

.field private static final synthetic q:[Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 54
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "_ID"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->a:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 55
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "MEDIA_ID"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->b:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 56
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "FILE_ID"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->c:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 57
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "LOCAL_DATA"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->d:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 58
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "DISPLAY_NAME"

    invoke-direct {v0, v1, v7}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->e:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 59
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "MIME_TYPE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->f:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 60
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "LAST_MODIFIED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->g:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 61
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "SIZE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->h:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 62
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "HOME_SYNC_FLAGS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->i:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 63
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "ICON_ID"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->j:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 64
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "MEDIA_TYPE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->k:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 65
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "ORIENTATION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->l:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 66
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "WIDTH"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->m:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 67
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "HEIGHT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->n:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 68
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "ALBUM_ID"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->o:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 69
    new-instance v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    const-string v1, "DEVICE_ID"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->p:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    .line 53
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    sget-object v1, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->a:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->b:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->c:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->d:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->e:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->f:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->g:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->h:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->i:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->j:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->k:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->l:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->m:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->n:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->o:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->p:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->q:[Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->q:[Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    invoke-virtual {v0}, [Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$FieldValues;

    return-object v0
.end method
