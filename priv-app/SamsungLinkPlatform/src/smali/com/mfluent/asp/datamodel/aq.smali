.class public final Lcom/mfluent/asp/datamodel/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/database/CrossProcessCursor;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

.field private c:Ljava/lang/Long;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/aq;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/aq;->d:Z

    .line 25
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/aq;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    .line 26
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->e()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/aq;->c:Ljava/lang/Long;

    .line 27
    return-void
.end method

.method private static a()V
    .locals 3

    .prologue
    .line 197
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(II)V

    throw v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/aq;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/aq;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/aq;->c:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Ljava/lang/Long;)Z

    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/aq;->c:Ljava/lang/Long;

    .line 205
    :cond_0
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/aq;->b()V

    .line 224
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/aq;->d:Z

    .line 225
    return-void
.end method

.method public final copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 0

    .prologue
    .line 136
    invoke-static {}, Lcom/mfluent/asp/datamodel/aq;->a()V

    .line 138
    return-void
.end method

.method public final deactivate()V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/aq;->b()V

    .line 194
    return-void
.end method

.method public final fillWindow(ILandroid/database/CursorWindow;)V
    .locals 0

    .prologue
    .line 285
    return-void
.end method

.method public final getBlob(I)[B
    .locals 1

    .prologue
    .line 122
    invoke-static {}, Lcom/mfluent/asp/datamodel/aq;->a()V

    .line 124
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getColumnCount()I
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    return v0
.end method

.method public final getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 91
    const/4 v0, -0x1

    return v0
.end method

.method public final getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 96
    const-string v0, "_id"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    const/4 v0, 0x0

    return v0

    .line 99
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public final getColumnName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    if-nez p1, :cond_0

    .line 105
    const-string v0, "_id"

    return-object v0

    .line 107
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/mfluent/asp/datamodel/aq;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public final getDouble(I)D
    .locals 2

    .prologue
    .line 170
    invoke-static {}, Lcom/mfluent/asp/datamodel/aq;->a()V

    .line 172
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 269
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getFloat(I)F
    .locals 1

    .prologue
    .line 163
    invoke-static {}, Lcom/mfluent/asp/datamodel/aq;->a()V

    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public final getInt(I)I
    .locals 1

    .prologue
    .line 149
    invoke-static {}, Lcom/mfluent/asp/datamodel/aq;->a()V

    .line 151
    const/4 v0, 0x0

    return v0
.end method

.method public final getLong(I)J
    .locals 2

    .prologue
    .line 156
    invoke-static {}, Lcom/mfluent/asp/datamodel/aq;->a()V

    .line 158
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getNotificationUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getPosition()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, -0x1

    return v0
.end method

.method public final getShort(I)S
    .locals 1

    .prologue
    .line 142
    invoke-static {}, Lcom/mfluent/asp/datamodel/aq;->a()V

    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    invoke-static {}, Lcom/mfluent/asp/datamodel/aq;->a()V

    .line 131
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getType(I)I
    .locals 1

    .prologue
    .line 177
    if-nez p1, :cond_0

    .line 178
    const/4 v0, 0x1

    .line 181
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getWantsAllOnMoveCalls()Z
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x0

    return v0
.end method

.method public final getWindow()Landroid/database/CursorWindow;
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x0

    return-object v0
.end method

.method public final isAfterLast()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public final isBeforeFirst()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public final isClosed()Z
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/aq;->d:Z

    return v0
.end method

.method public final isFirst()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public final isLast()Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public final isNull(I)Z
    .locals 1

    .prologue
    .line 186
    invoke-static {}, Lcom/mfluent/asp/datamodel/aq;->a()V

    .line 188
    const/4 v0, 0x1

    return v0
.end method

.method public final move(I)Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public final moveToFirst()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public final moveToLast()Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public final moveToNext()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public final moveToPosition(I)Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public final moveToPrevious()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public final onMove(II)Z
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    return v0
.end method

.method public final registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 245
    return-void
.end method

.method public final requery()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 210
    const/4 v0, 0x0

    .line 211
    iget-boolean v1, p0, Lcom/mfluent/asp/datamodel/aq;->d:Z

    if-nez v1, :cond_0

    .line 212
    const/4 v0, 0x1

    .line 213
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/aq;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/aq;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->e()Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/aq;->c:Ljava/lang/Long;

    .line 218
    :cond_0
    return v0
.end method

.method public final respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    return-object v0
.end method

.method public final setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method public final unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 250
    return-void
.end method
