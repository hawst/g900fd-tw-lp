.class public final Lcom/mfluent/asp/datamodel/ag;
.super Lcom/mfluent/asp/datamodel/ao;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ag$a;
    }
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "addr"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "langagecode"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ag;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ao;-><init>()V

    .line 38
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ag;-><init>()V

    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/ag;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/mfluent/asp/datamodel/ag$a;->a:Lcom/mfluent/asp/datamodel/ag;

    return-object v0
.end method


# virtual methods
.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 42
    const-string v0, "geolocation"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No per-device filtering for GeoLocation."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const-string v0, "GEO_LOCATION"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    const-string v0, "GEO_LOCATION"

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$GeoLocation;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$GeoLocation;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$GeoLocation;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method
