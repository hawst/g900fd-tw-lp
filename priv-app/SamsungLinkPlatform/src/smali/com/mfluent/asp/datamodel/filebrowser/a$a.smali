.class final Lcom/mfluent/asp/datamodel/filebrowser/a$a;
.super Lcom/mfluent/asp/util/WeakReferencingBroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/filebrowser/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/util/WeakReferencingBroadcastReceiver",
        "<",
        "Lcom/mfluent/asp/datamodel/filebrowser/a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/filebrowser/a;)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/mfluent/asp/util/WeakReferencingBroadcastReceiver;-><init>(Ljava/lang/Object;)V

    .line 124
    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 120
    check-cast p2, Lcom/mfluent/asp/datamodel/filebrowser/a;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.mfluent.asp.filetransfer.FileTransferManager.TRANSFER_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.mfluent.asp.filetransfer.FileTransferManager.TRANSFER_COMPLETE_DEVICE_ID"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {p2}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lcom/mfluent/asp/datamodel/filebrowser/a;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-static {p2}, Lcom/mfluent/asp/datamodel/filebrowser/a;->b(Lcom/mfluent/asp/datamodel/filebrowser/a;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/DataSetObserver;

    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    goto :goto_0

    :cond_0
    return-void
.end method
