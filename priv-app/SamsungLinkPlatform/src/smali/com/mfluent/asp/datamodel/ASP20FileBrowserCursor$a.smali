.class final Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field a:J

.field final synthetic b:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;


# direct methods
.method private constructor <init>(Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;)V
    .locals 2

    .prologue
    .line 82
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;->b:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;->a:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;B)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;-><init>(Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;)V

    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 82
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;->a:J

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;->b:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/c;

    :cond_0
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->f()Z

    move-result v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;->a:J

    const-wide/16 v6, 0x2710

    add-long/2addr v4, v6

    cmp-long v4, v2, v4

    if-gtz v4, :cond_1

    if-eqz v1, :cond_3

    :cond_1
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->c()V

    const-string v1, "INFO"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "send onchange for uri="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->getCurrentDirectoryAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->d()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->e()V

    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;->b:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;)Z

    iput-wide v2, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;->a:J

    :goto_0
    return-object v8

    :cond_3
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->b()I

    move-result v1

    const/16 v2, 0x14

    if-le v1, v2, :cond_0

    const-string v1, "ERR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t finish filebrowser2 because of many errors ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/c;->b()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor$a;->b:Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;->a(Lcom/mfluent/asp/datamodel/ASP20FileBrowserCursor;)Z

    goto :goto_0
.end method
