.class public final Lcom/mfluent/asp/datamodel/filebrowser/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
        "<",
        "Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final deselectAll()V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method public final destroy()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public final bridge synthetic getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCurrentDirectoryAbsolutePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const-string v0, ""

    return-object v0
.end method

.method public final bridge synthetic getFile(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic getFileNonBlocking(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic getParentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSelectedFileIndexes()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic init(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    return-void
.end method

.method public final isAllSelected()Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public final isSelected(I)Z
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public final selectAll()V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public final setSelected(IZ)V
    .locals 0

    .prologue
    .line 108
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 96
    return-void
.end method
