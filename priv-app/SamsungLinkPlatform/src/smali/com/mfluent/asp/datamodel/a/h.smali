.class public Lcom/mfluent/asp/datamodel/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/datamodel/a/a;


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/mfluent/asp/datamodel/a/h;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/a/h;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 38
    sget-object v0, Lcom/mfluent/asp/datamodel/a/h;->a:Lorg/slf4j/Logger;

    const-string v2, "::call: entered"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 40
    const-string v0, "INTENT_ARG_DEVICE_ID"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 41
    const-string v0, "INTENT_ARG_DEVICE_CMD"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 42
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 43
    long-to-int v2, v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    .line 47
    :try_start_0
    const-string v2, "shutdown"

    invoke-static {v4, v2}, Lorg/apache/commons/lang3/StringUtils;->startsWith(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 49
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "api/pCloud/device/"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-static {v3, v0, v2}, Lcom/mfluent/asp/nts/b;->b(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 85
    :cond_0
    :goto_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 86
    const-string v3, "method_result_str"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    return-object v2

    .line 50
    :cond_2
    :try_start_1
    const-string v2, "remotewakeup"

    invoke-static {v4, v2}, Lorg/apache/commons/lang3/StringUtils;->startsWith(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 51
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "api/pCloud/device/"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-static {v3, v0, v2}, Lcom/mfluent/asp/nts/b;->b(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 52
    :try_start_2
    const-string v2, "ON"

    invoke-static {v4, v2}, Lorg/apache/commons/lang3/StringUtils;->containsIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v2}, Lcom/mfluent/asp/datamodel/Device;->o(Z)V

    .line 53
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->O()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 81
    :catch_0
    move-exception v2

    .line 82
    :goto_2
    sget-object v3, Lcom/mfluent/asp/datamodel/a/h;->a:Lorg/slf4j/Logger;

    const-string v4, "Error getting device info"

    invoke-interface {v3, v4, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 52
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 55
    :cond_4
    :try_start_3
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEARABLE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v3, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 56
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/a/a;

    move-result-object v0

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/samsung/a/a;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_7

    .line 61
    invoke-static {v0}, Lcom/sec/pcw/service/d/b;->a(Lcom/mfluent/asp/datamodel/Device;)Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_0

    .line 65
    :cond_5
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/t;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 66
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v2

    const-string v5, ":"

    invoke-static {v2, v5}, Lorg/apache/commons/lang3/StringUtils;->remove(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 67
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "api/pCloud/device/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "?_="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&uniqueId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v0

    .line 71
    :try_start_4
    const-string v2, "networkStatus"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 72
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v3, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    .line 73
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->O()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 78
    :cond_6
    :try_start_5
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "api/pCloud/device/"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "?_="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move-result-object v0

    goto/16 :goto_0

    .line 81
    :catch_1
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    goto/16 :goto_2

    :cond_7
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method
