.class public abstract Lcom/mfluent/asp/datamodel/m;
.super Lcom/mfluent/asp/datamodel/ao;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const-class v0, Lcom/mfluent/asp/datamodel/m;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/m;->a:Lorg/slf4j/Logger;

    .line 51
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "orig_journal_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/m;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ao;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;JLandroid/content/ContentValues;)I
    .locals 18

    .prologue
    .line 55
    const-string v2, "media_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 56
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "is_delete"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v16

    .line 57
    const/4 v14, 0x1

    .line 58
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    .line 59
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/m;->g()Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    move-result-object v17

    .line 60
    const/4 v13, 0x0

    .line 62
    invoke-static {v15}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 63
    const/4 v12, 0x0

    .line 64
    const-wide/16 v10, 0x0

    .line 65
    const/4 v2, 0x0

    aput-object v15, v6, v2

    .line 66
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/m;->d()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/mfluent/asp/datamodel/m;->b:[Ljava/lang/String;

    const-string v5, "media_id=?"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 74
    if-eqz v5, :cond_7

    .line 75
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 76
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 77
    const/4 v4, 0x1

    invoke-interface {v5, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 78
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 83
    :goto_0
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 86
    :goto_1
    const-wide/16 v8, 0x0

    cmp-long v5, v2, v8

    if-eqz v5, :cond_0

    .line 87
    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v5

    .line 88
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/m;->d()Ljava/lang/String;

    move-result-object v3

    const-string v5, "_id=?"

    invoke-virtual {v2, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 90
    :cond_0
    if-eqz v16, :cond_5

    if-eqz v4, :cond_5

    .line 91
    invoke-static {}, Lcom/mfluent/asp/datamodel/u;->a()Lcom/mfluent/asp/datamodel/u;

    move-result-object v2

    invoke-interface/range {v17 .. v17}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;->getJournalSyncedKey()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lcom/mfluent/asp/datamodel/u;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 92
    const-wide v2, 0x7fffffffffffffffL

    .line 93
    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 94
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 96
    :cond_1
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-gez v2, :cond_5

    .line 97
    const/4 v2, 0x0

    .line 98
    sget-object v3, Lcom/mfluent/asp/datamodel/m;->a:Lorg/slf4j/Logger;

    const-string v5, "Skipped recording delete in {} for media_id: {}"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6, v15}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 101
    :goto_2
    if-eqz v2, :cond_4

    .line 102
    sget-object v3, Lcom/mfluent/asp/datamodel/m;->a:Lorg/slf4j/Logger;

    const-string v5, "Recording {} in {} for media_id: {}"

    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/Object;

    const/4 v7, 0x0

    if-eqz v16, :cond_3

    const-string v2, "delete"

    :goto_3
    aput-object v2, v6, v7

    const/4 v2, 0x1

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x2

    aput-object v15, v6, v2

    invoke-interface {v3, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    new-instance v2, Landroid/content/ContentValues;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 104
    const-string v3, "orig_journal_id"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 105
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/datamodel/m;->d()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    long-to-int v2, v2

    .line 109
    :goto_4
    return v2

    .line 80
    :cond_2
    const/4 v4, 0x1

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto/16 :goto_0

    .line 102
    :cond_3
    const-string v2, "update"

    goto :goto_3

    :cond_4
    move v2, v13

    goto :goto_4

    :cond_5
    move v2, v14

    goto :goto_2

    :cond_6
    move-wide v2, v10

    move-object v4, v12

    goto/16 :goto_0

    :cond_7
    move-wide v2, v10

    move-object v4, v12

    goto/16 :goto_1
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J
    .locals 4

    .prologue
    .line 47
    sget-object v0, Lcom/mfluent/asp/datamodel/m;->a:Lorg/slf4j/Logger;

    const-string v1, "Recording insert into {}. media_id: {}"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/m;->g()Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "media_id"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 48
    invoke-super {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/m;->h()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No per-device filtering for journal."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract g()Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;
.end method

.method protected final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return-object v0
.end method
