.class final Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1755
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1752
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b:Ljava/util/HashMap;

    .line 1753
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->c:Ljava/util/HashMap;

    .line 1757
    new-array v0, v5, [Ljava/lang/String;

    .line 1759
    const-string v1, "DEVICES"

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "API_DEVICES"

    aput-object v3, v2, v5

    const-string v3, "FILES_DEVICES"

    aput-object v3, v2, v6

    const-string v3, "API_FILES_DEVICES"

    aput-object v3, v2, v7

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1763
    const-string v1, "FILES"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "FILES_DEVICES"

    aput-object v3, v2, v5

    const-string v3, "API_FILES_DEVICES"

    aput-object v3, v2, v6

    const-string v3, "ALBUM_ORPHANS"

    aput-object v3, v2, v7

    const-string v3, "ARTIST_ORPHANS"

    aput-object v3, v2, v8

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1768
    const-string v1, "ALBUMS"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "ALBUM_DETAIL"

    aput-object v3, v2, v5

    const-string v3, "API_ALBUM_DETAIL"

    aput-object v3, v2, v6

    const-string v3, "ALBUM_ORPHANS"

    aput-object v3, v2, v7

    const-string v3, "ALL_MEDIA"

    aput-object v3, v2, v8

    const-string v3, "AUDIO_DETAILS"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "API_AUDIO_DETAILS"

    aput-object v4, v2, v3

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1775
    const-string v1, "ARTISTS"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "ALBUM_DETAIL"

    aput-object v3, v2, v5

    const-string v3, "API_ALBUM_DETAIL"

    aput-object v3, v2, v6

    const-string v3, "ARTISTS_DETAIL"

    aput-object v3, v2, v7

    const-string v3, "API_ARTISTS_DETAIL"

    aput-object v3, v2, v8

    const-string v3, "ARTIST_ORPHANS"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "ALL_MEDIA"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "AUDIO_DETAILS"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "API_AUDIO_DETAILS"

    aput-object v4, v2, v3

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1784
    const-string v1, "GENRES"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "GENRES_AGGREGATE"

    aput-object v3, v2, v5

    const-string v3, "API_GENRES_AGGREGATE"

    aput-object v3, v2, v6

    const-string v3, "GENRE_ORPHANS"

    aput-object v3, v2, v7

    const-string v3, "GENRE_MEMBERS_DETAIL"

    aput-object v3, v2, v8

    const-string v3, "API_GENRE_MEMBERS_DETAIL"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "GENRE_MEMBERS_SYNC"

    aput-object v4, v2, v3

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->f()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1791
    const-string v1, "GENRE_MEMBERS"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "GENRE_MEMBERS_DETAIL"

    aput-object v3, v2, v5

    const-string v3, "API_GENRE_MEMBERS_DETAIL"

    aput-object v3, v2, v6

    const-string v3, "GENRES_AGGREGATE"

    aput-object v3, v2, v7

    const-string v3, "API_GENRES_AGGREGATE"

    aput-object v3, v2, v8

    const-string v3, "GENRE_ORPHANS"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "GENRE_MEMBERS_SYNC"

    aput-object v4, v2, v3

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1798
    const-string v1, "KEYWORDS"

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "KEYWORD_ORPHANS"

    aput-object v3, v2, v5

    const-string v3, "FILE_KEYWORD_DETAIL"

    aput-object v3, v2, v6

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->h()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1802
    const-string v1, "KEYWORD_MAP"

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "FILE_KEYWORD_DETAIL"

    aput-object v3, v2, v5

    const-string v3, "KEYWORD_ORPHANS"

    aput-object v3, v2, v6

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->i()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1807
    const-string v1, "FILE_TRANSFER_SESSIONS"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1808
    const-string v1, "ARCHIVED_MEDIA"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1809
    const-string v1, "IMAGES_JOURNAL"

    const-string v2, "IMAGES_JOURNAL"

    invoke-static {v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1810
    const-string v1, "VIDEOS_JOURNAL"

    const-string v2, "VIDEOS_JOURNAL"

    invoke-static {v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1811
    const-string v1, "AUDIO_JOURNAL"

    const-string v2, "AUDIO_JOURNAL"

    invoke-static {v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1812
    const-string v1, "DOCUMENTS_JOURNAL"

    const-string v2, "DOCUMENTS_JOURNAL"

    invoke-static {v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1813
    const-string v1, "db_integrity"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->l()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1814
    const-string v1, "GEO_LOCATION"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->m()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1816
    const-string v1, "FILES_DEVICES"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "IMAGES"

    aput-object v3, v2, v5

    const-string v3, "AUDIO"

    aput-object v3, v2, v6

    const-string v3, "VIDEOS"

    aput-object v3, v2, v7

    const-string v3, "DOCUMENTS"

    aput-object v3, v2, v8

    const-string v3, "LOCAL_DUPS"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "FILE_KEYWORD_DETAIL"

    aput-object v4, v2, v3

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->n()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1824
    const-string v1, "API_FILES_DEVICES"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "API_IMAGES"

    aput-object v3, v2, v5

    const-string v3, "API_AUDIO"

    aput-object v3, v2, v6

    const-string v3, "API_VIDEOS"

    aput-object v3, v2, v7

    const-string v3, "API_DOCUMENTS"

    aput-object v3, v2, v8

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->o()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1830
    const-string v1, "LOCAL_DUPS"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1832
    const-string v1, "IMAGES"

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "BURST_SHOT_IMAGES"

    aput-object v3, v2, v5

    const-string v3, "CROSS_DEVICE_IMAGES"

    aput-object v3, v2, v6

    const-string v3, "IMAGE_KEYWORD_DETAIL"

    aput-object v3, v2, v7

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->p()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1837
    const-string v1, "API_IMAGES"

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "API_BURST_SHOT_IMAGES"

    aput-object v3, v2, v5

    const-string v3, "API_CROSS_DEVICE_IMAGES"

    aput-object v3, v2, v6

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->q()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1842
    const-string v1, "AUDIO"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "ALBUM_DETAIL"

    aput-object v3, v2, v5

    const-string v3, "ARTISTS_DETAIL"

    aput-object v3, v2, v6

    const-string v3, "AUDIO_DETAILS"

    aput-object v3, v2, v7

    const-string v3, "GENRES_AGGREGATE"

    aput-object v3, v2, v8

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->r()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1848
    const-string v1, "API_AUDIO"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "API_ALBUM_DETAIL"

    aput-object v3, v2, v5

    const-string v3, "API_ARTISTS_DETAIL"

    aput-object v3, v2, v6

    const-string v3, "API_AUDIO_DETAILS"

    aput-object v3, v2, v7

    const-string v3, "API_GENRES_AGGREGATE"

    aput-object v3, v2, v8

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->s()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1854
    const-string v1, "VIDEOS"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "VIDEO_KEYWORD_DETAIL"

    aput-object v3, v2, v5

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->t()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1856
    const-string v1, "API_VIDEOS"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->u()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1858
    const-string v1, "DOCUMENTS"

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "CROSS_DEVICE_DOCUMENTS"

    aput-object v3, v2, v5

    const-string v3, "DOCUMENT_KEYWORD_DETAIL"

    aput-object v3, v2, v6

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->v()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1862
    const-string v1, "API_DOCUMENTS"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "API_CROSS_DEVICE_DOCUMENTS"

    aput-object v3, v2, v5

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->w()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1864
    const-string v1, "AUDIO_DETAILS"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "CROSS_DEVICE_AUDIO"

    aput-object v3, v2, v5

    const-string v3, "GENRE_MEMBERS_DETAIL"

    aput-object v3, v2, v6

    const-string v3, "GENRE_MEMBERS_SYNC"

    aput-object v3, v2, v7

    const-string v3, "AUDIO_KEYWORD_DETAIL"

    aput-object v3, v2, v8

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->x()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1870
    const-string v1, "API_AUDIO_DETAILS"

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "API_CROSS_DEVICE_AUDIO"

    aput-object v3, v2, v5

    const-string v3, "API_GENRE_MEMBERS_DETAIL"

    aput-object v3, v2, v6

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->y()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1875
    const-string v1, "CROSS_DEVICE_IMAGES"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->z()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1876
    const-string v1, "API_CROSS_DEVICE_IMAGES"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->A()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1877
    const-string v1, "BURST_SHOT_IMAGES"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->B()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1878
    const-string v1, "API_BURST_SHOT_IMAGES"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->C()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1880
    const-string v1, "CROSS_DEVICE_AUDIO"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->D()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1881
    const-string v1, "API_CROSS_DEVICE_AUDIO"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->E()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1883
    const-string v1, "CROSS_DEVICE_DOCUMENTS"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->F()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1884
    const-string v1, "API_CROSS_DEVICE_DOCUMENTS"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->G()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1886
    const-string v1, "KEYWORD_ORPHANS"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->H()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1888
    const-string v1, "ALBUM_DETAIL"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->I()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1889
    const-string v1, "API_ALBUM_DETAIL"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->J()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1890
    const-string v1, "ALBUM_ORPHANS"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->K()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1892
    const-string v1, "ARTISTS_DETAIL"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->L()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1893
    const-string v1, "API_ARTISTS_DETAIL"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->M()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1894
    const-string v1, "ARTIST_ORPHANS"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->N()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1896
    const-string v1, "API_DEVICES"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->O()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1898
    const-string v1, "GENRES_AGGREGATE"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->P()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1899
    const-string v1, "API_GENRES_AGGREGATE"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1900
    const-string v1, "GENRE_ORPHANS"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->R()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1902
    const-string v1, "GENRE_MEMBERS_DETAIL"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->S()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1903
    const-string v1, "API_GENRE_MEMBERS_DETAIL"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->T()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1905
    const-string v1, "GENRE_MEMBERS_SYNC"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->U()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1907
    const-string v1, "FILE_KEYWORD_DETAIL"

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ALL_MEDIA"

    aput-object v3, v2, v5

    const-string v3, "FILE_KEYWORD_DETAIL"

    const-string v4, "FILES_DEVICES"

    const-string v5, "_id"

    invoke-static {v3, v4, v5}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1912
    const-string v1, "AUDIO_KEYWORD_DETAIL"

    const-string v2, "AUDIO_KEYWORD_DETAIL"

    const-string v3, "AUDIO_DETAILS"

    const-string v4, "_id"

    invoke-static {v2, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1920
    const-string v1, "DOCUMENT_KEYWORD_DETAIL"

    const-string v2, "DOCUMENT_KEYWORD_DETAIL"

    const-string v3, "DOCUMENTS"

    const-string v4, "_id"

    invoke-static {v2, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1925
    const-string v1, "IMAGE_KEYWORD_DETAIL"

    const-string v2, "IMAGE_KEYWORD_DETAIL"

    const-string v3, "IMAGES"

    const-string v4, "_id"

    invoke-static {v2, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1930
    const-string v1, "VIDEO_KEYWORD_DETAIL"

    const-string v2, "VIDEO_KEYWORD_DETAIL"

    const-string v3, "VIDEOS"

    const-string v4, "_id"

    invoke-static {v2, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1935
    const-string v1, "ALL_MEDIA"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->V()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1937
    return-void
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1940
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1941
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already exists in dependency graph. Check your code."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1943
    :cond_0
    invoke-static {p2, p1}, Lorg/apache/commons/lang3/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1944
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Circular dependancy. Key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot depend on itself. Check your code."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1946
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b:Ljava/util/HashMap;

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p2, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1947
    return-void
.end method

.method private b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1950
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1951
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already exists in dependency graph. Check your code."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1953
    :cond_0
    invoke-static {p2, p1}, Lorg/apache/commons/lang3/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1954
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Circular dependancy. Key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot depend on itself. Check your code."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1956
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->c:Ljava/util/HashMap;

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p2, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1957
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1994
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1996
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1997
    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    goto :goto_0

    .line 2000
    :cond_0
    return-object v1
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 2005
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2006
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2008
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_0

    .line 2010
    :cond_0
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2013
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2014
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2015
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteException;
        }
    .end annotation

    .prologue
    .line 2024
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 2025
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2024
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 2027
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1960
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 1961
    if-eqz v0, :cond_0

    .line 1962
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/String;

    array-length v1, v1

    if-ge v2, v1, :cond_1

    .line 1963
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {p0, v1, p2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 1962
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1966
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Table: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found in table graph."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1962
    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1971
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1972
    if-gez v0, :cond_2

    .line 1973
    const v2, 0x7fffffff

    .line 1975
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 1976
    if-eqz v0, :cond_0

    .line 1977
    const/4 v1, 0x0

    move v3, v2

    move v2, v1

    :goto_0
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/String;

    array-length v1, v1

    if-ge v2, v1, :cond_1

    .line 1978
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {p0, v1, p2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v1

    .line 1979
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1977
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1982
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not find in view graph."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1985
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1987
    invoke-virtual {p2, v0, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1990
    :cond_2
    return v0
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 2018
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2019
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    invoke-static {p1, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_0

    .line 2021
    :cond_0
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteException;
        }
    .end annotation

    .prologue
    .line 2030
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2031
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->c:Ljava/util/HashMap;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2032
    if-eqz v0, :cond_0

    .line 2033
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2030
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2036
    :cond_1
    return-void
.end method
