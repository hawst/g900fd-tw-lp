.class public final Lcom/mfluent/asp/datamodel/h;
.super Lcom/mfluent/asp/datamodel/m;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/h$a;
    }
.end annotation


# static fields
.field private static final a:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;

    invoke-direct {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/h;->a:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/m;-><init>()V

    .line 32
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/h;-><init>()V

    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/h;
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/mfluent/asp/datamodel/h$a;->a()Lcom/mfluent/asp/datamodel/h;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final c_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    const-string v0, "AUDIO_JOURNAL"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    const-string v0, "AUDIO_JOURNAL"

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method protected final g()Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/mfluent/asp/datamodel/h;->a:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
