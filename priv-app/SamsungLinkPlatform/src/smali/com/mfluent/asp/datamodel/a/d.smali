.class public Lcom/mfluent/asp/datamodel/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/datamodel/a/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/a/d$a;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/mfluent/asp/datamodel/a/d;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/a/d;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/a/d;->b:Landroid/content/Context;

    .line 43
    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/FileTransferSession;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 87
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a/d;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferSessions;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "uuid=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 94
    if-nez v1, :cond_0

    .line 105
    :goto_0
    return-object v2

    .line 99
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 100
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 103
    :cond_1
    :try_start_1
    new-instance v2, Lcom/mfluent/asp/datamodel/a/d$a;

    invoke-direct {v2, v1}, Lcom/mfluent/asp/datamodel/a/d$a;-><init>(Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Lcom/mfluent/asp/filetransfer/FileTransferSession;)Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 275
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 280
    :try_start_0
    const-string v0, "currentBytesSent"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->t()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 282
    const-string v0, "totalBytesSent"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->k()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 284
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$SlinkFileTransferSessionInfo$SlinkFileTransferState;->PENDING:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$SlinkFileTransferSessionInfo$SlinkFileTransferState;

    .line 285
    const-string v2, "Chan"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "session.getStatus() : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    if-ne v2, v3, :cond_2

    .line 287
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$SlinkFileTransferSessionInfo$SlinkFileTransferState;->COMPLETE:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$SlinkFileTransferSessionInfo$SlinkFileTransferState;

    .line 292
    :cond_0
    :goto_0
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 293
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$SlinkFileTransferSessionInfo$SlinkFileTransferState;->ERROR:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$SlinkFileTransferSessionInfo$SlinkFileTransferState;

    .line 298
    :cond_1
    :goto_1
    const-string v2, "status"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 299
    const-string v0, "id"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 302
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->h()Ljava/util/Set;

    move-result-object v0

    .line 303
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 304
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v0

    .line 308
    :goto_2
    const-string v2, "controllerName"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 309
    const-string v2, "sourceName"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 310
    const-string v0, "targetName"

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    const-string v0, "Chan"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "json : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    return-object v1

    .line 288
    :cond_2
    :try_start_1
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    if-ne v2, v3, :cond_0

    .line 289
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$SlinkFileTransferSessionInfo$SlinkFileTransferState;->CANCELLED:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$SlinkFileTransferSessionInfo$SlinkFileTransferState;

    goto :goto_0

    .line 294
    :cond_3
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->o()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 295
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$SlinkFileTransferSessionInfo$SlinkFileTransferState;->IN_PROGRESS:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$SlinkFileTransferSessionInfo$SlinkFileTransferState;

    goto :goto_1

    .line 306
    :cond_4
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    const v2, 0x7f0a00c5

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/ASPApplication;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_2

    .line 312
    :catch_0
    move-exception v0

    .line 313
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trouble building json response"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 53
    sget-object v0, Lcom/mfluent/asp/datamodel/a/d;->a:Lorg/slf4j/Logger;

    const-string v2, "::call: entered"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 58
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 59
    invoke-static {p1}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v2

    .line 61
    invoke-interface {v2, p2}, Lcom/mfluent/asp/filetransfer/d;->b(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v0

    .line 62
    if-nez v0, :cond_0

    .line 63
    invoke-interface {v2}, Lcom/mfluent/asp/filetransfer/d;->e()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileTransferSession;

    .line 66
    :cond_0
    if-nez v0, :cond_1

    .line 67
    invoke-direct {p0, p2}, Lcom/mfluent/asp/datamodel/a/d;->a(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/FileTransferSession;

    move-result-object v0

    .line 70
    :cond_1
    if-eqz v0, :cond_3

    .line 71
    invoke-static {v0}, Lcom/mfluent/asp/datamodel/a/d;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession;)Lorg/json/JSONObject;

    move-result-object v0

    .line 78
    :goto_0
    const-string v3, "mfl_FileTransferSessionCallHanlder"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v2, "json is null? : "

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_5

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 81
    const-string v3, "method_result"

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    return-object v2

    .line 73
    :cond_3
    sget-object v0, Lcom/mfluent/asp/datamodel/a/d;->a:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "session is null : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    :cond_4
    move-object v0, v1

    goto :goto_0

    .line 78
    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method
