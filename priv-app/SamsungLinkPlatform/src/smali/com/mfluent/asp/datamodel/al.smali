.class public final Lcom/mfluent/asp/datamodel/al;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J

.field private static final b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 25
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5a

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/datamodel/al;->a:J

    .line 28
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_SYNC:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/datamodel/al;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/al;->c:Ljava/util/HashMap;

    .line 209
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MAX(_id) AS max_journal_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/al;->d:[Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;)Landroid/content/ContentProviderOperation;
    .locals 2

    .prologue
    .line 66
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/mfluent/asp/datamodel/al;->a(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;ZZ)Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;ZZ)Landroid/content/ContentProviderOperation;
    .locals 4

    .prologue
    .line 78
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 79
    const-string v2, "is_delete"

    if-eqz p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 80
    const-string v0, "media_id"

    invoke-virtual {v1, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    if-eqz p3, :cond_0

    .line 82
    const-string v0, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 85
    :cond_0
    if-nez p2, :cond_2

    .line 86
    invoke-interface {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 90
    :goto_1
    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 92
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0

    .line 79
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :cond_2
    invoke-interface {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/content/ContentResolver;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;)V
    .locals 10

    .prologue
    .line 141
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MIN("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;->getDeviceSyncedJournalColumn()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") AS min_journal"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 148
    const-wide v0, 0x7fffffffffffffffL

    .line 149
    if-eqz v2, :cond_1

    .line 151
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 152
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 155
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 159
    :cond_1
    const-string v2, "timestamp<?"

    .line 160
    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sget-wide v8, Lcom/mfluent/asp/datamodel/al;->a:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    .line 161
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v3, v0, v6

    if-eqz v3, :cond_7

    .line 162
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "("

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR _id<?)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 163
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v4, v3}, Lorg/apache/commons/lang3/ArrayUtils;->addAll([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v4, v0

    move-object v0, v2

    .line 166
    :goto_0
    const-wide/16 v6, 0x0

    .line 167
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "MAX(_id) AS max_id"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "is_delete=1 AND "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 173
    if-eqz v2, :cond_6

    .line 175
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 176
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    .line 179
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 183
    :goto_2
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_4

    .line 185
    sget-object v2, Lcom/mfluent/asp/datamodel/al;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 186
    const-string v2, "mfl_JournalHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Purging delete entries in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " <= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :cond_2
    invoke-static {p0, p1}, Lcom/mfluent/asp/datamodel/al;->b(Landroid/content/ContentResolver;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;)J

    move-result-wide v2

    .line 190
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 191
    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 192
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;->getJournalDroppedKey()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, v4, v1

    invoke-static {p0, v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->setValue(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_3
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "_id<=? AND is_delete=1"

    invoke-virtual {p0, v0, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 197
    :cond_4
    return-void

    .line 155
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 179
    :catchall_1
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    move-wide v0, v6

    goto :goto_1

    :cond_6
    move-wide v0, v6

    goto :goto_2

    :cond_7
    move-object v0, v2

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/ContentResolver;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;IJ)V
    .locals 7

    .prologue
    .line 122
    const/4 v0, 0x0

    .line 123
    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-lez v1, :cond_0

    .line 124
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 127
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 128
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;->getDeviceSyncedJournalColumn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 129
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 130
    return-void
.end method

.method public static a(Landroid/content/ContentResolver;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;J)V
    .locals 4

    .prologue
    .line 225
    sget-object v0, Lcom/mfluent/asp/datamodel/al;->c:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;->getJournalSyncedKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->getValue(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/datamodel/al;->c:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    if-nez v0, :cond_2

    const-wide/16 v0, 0x0

    .line 227
    :goto_0
    cmp-long v0, v0, p2

    if-gez v0, :cond_1

    .line 228
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 229
    sget-object v1, Lcom/mfluent/asp/datamodel/al;->c:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;->getJournalSyncedKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->setValue(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_1
    return-void

    .line 225
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static b(Landroid/content/ContentResolver;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;)J
    .locals 4

    .prologue
    .line 200
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;->getJournalDroppedKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->getValue(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 201
    const-wide/16 v0, 0x0

    .line 202
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 203
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 206
    :cond_0
    return-wide v0
.end method

.method public static b(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;)Landroid/content/ContentProviderOperation;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-static {p0, p1, v0, v0}, Lcom/mfluent/asp/datamodel/al;->a(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;ZZ)Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;)Landroid/content/ContentProviderOperation;
    .locals 2

    .prologue
    .line 74
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Lcom/mfluent/asp/datamodel/al;->a(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;ZZ)Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method
