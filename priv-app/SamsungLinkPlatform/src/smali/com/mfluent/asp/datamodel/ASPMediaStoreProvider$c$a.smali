.class final Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

.field private final b:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;)V
    .locals 2

    .prologue
    .line 5306
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5308
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;->b:Landroid/util/LruCache;

    .line 5361
    return-void
.end method

.method synthetic constructor <init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;B)V
    .locals 0

    .prologue
    .line 5306
    invoke-direct {p0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;)V

    return-void
.end method

.method private a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;)V
    .locals 3

    .prologue
    .line 5357
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Notifying change on URI: {}"

    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5358
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

    iget-object v0, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 5359
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 12

    .prologue
    .line 5312
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 5314
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 5353
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 5316
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    .line 5317
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;->b:Landroid/util/LruCache;

    invoke-virtual {v1, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;

    .line 5319
    if-nez v1, :cond_0

    .line 5320
    new-instance v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;B)V

    .line 5321
    iput-object v0, v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;->a:Landroid/net/Uri;

    .line 5322
    iget-object v4, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;->b:Landroid/util/LruCache;

    invoke-virtual {v4, v0, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5325
    :cond_0
    iget-wide v4, v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;->b:J

    sub-long v4, v2, v4

    .line 5326
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v6

    const-string v7, "lastTimeSent {}, timeDiff {}, uri {}"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-wide v10, v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    invoke-interface {v6, v7, v8}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 5328
    const-wide/16 v6, 0x3e8

    cmp-long v6, v4, v6

    if-lez v6, :cond_1

    .line 5329
    iput-wide v2, v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;->b:J

    .line 5330
    invoke-direct {p0, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;)V

    .line 5342
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 5332
    :cond_1
    iget-boolean v2, v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;->c:Z

    if-eqz v2, :cond_2

    .line 5334
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "ignoring noti for {}"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 5336
    :cond_2
    const-wide/16 v2, 0x3e8

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 5337
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "delaying noti for {} by {}"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v0, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 5338
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;->c:Z

    .line 5339
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;)Landroid/os/Handler;

    move-result-object v0

    iget-object v4, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

    invoke-static {v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 5345
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;

    .line 5346
    iput-wide v2, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;->b:J

    .line 5347
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;->c:Z

    .line 5348
    invoke-direct {p0, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c$a$a;)V

    .line 5349
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 5314
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
