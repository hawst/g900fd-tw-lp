.class final enum Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FieldValues"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

.field public static final enum b:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

.field public static final enum c:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

.field public static final enum d:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

.field public static final enum e:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

.field public static final enum f:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

.field public static final enum g:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

.field public static final enum h:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

.field public static final enum i:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

.field public static final enum j:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

.field private static final synthetic k:[Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    const-string v1, "FILE_ID"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->a:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    .line 33
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    const-string v1, "DISPLAY_NAME"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->b:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    .line 34
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    const-string v1, "HOME_SYNC_FLAGS"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->c:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    .line 35
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    const-string v1, "ICON_ID"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->d:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    .line 36
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    const-string v1, "FILE_COUNT"

    invoke-direct {v0, v1, v7}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->e:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    .line 37
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    const-string v1, "PATH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->f:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    .line 38
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    const-string v1, "PARENT_DISPLAY_NAME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->g:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    .line 39
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    const-string v1, "PARENT_FILE_ID"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->h:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    .line 40
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    const-string v1, "PARENT_HOME_SYNC_FLAGS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->i:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    .line 41
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    const-string v1, "PARENT_ICON_ID"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->j:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    .line 31
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->a:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->b:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->c:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->d:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->e:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->f:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->g:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->h:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->i:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->j:Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->k:[Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->k:[Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    invoke-virtual {v0}, [Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/datamodel/ASPFileBrowserDirectoryCrossProcessCursor$FieldValues;

    return-object v0
.end method
