.class public final Lcom/mfluent/asp/datamodel/ar;
.super Landroid/database/MatrixCursor;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 59
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_icon_2"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_text_3"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_group"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "orientation"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ar;->e:[Ljava/lang/String;

    .line 74
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 75
    const-string v1, "android.resource"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 76
    const-string v1, "com.samsung.android.sdk.samsunglink"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 78
    const v1, 0x7f020069

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 79
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/mfluent/asp/datamodel/ar;->a:Ljava/lang/String;

    .line 81
    const v1, 0x7f02006a

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 82
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/mfluent/asp/datamodel/ar;->b:Ljava/lang/String;

    .line 84
    const v1, 0x7f02006b

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 85
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/mfluent/asp/datamodel/ar;->c:Ljava/lang/String;

    .line 87
    const v1, 0x7f020067

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 88
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/ar;->d:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;I)V
    .locals 21

    .prologue
    .line 92
    sget-object v2, Lcom/mfluent/asp/datamodel/ar;->e:[Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 94
    const/4 v9, 0x0

    .line 95
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 97
    const-string v2, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 99
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 100
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 103
    :cond_0
    const-string v2, "CHJ"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "searchUri : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v2, "stime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 106
    const-string v2, "etime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 107
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 108
    :cond_1
    const/4 v2, 0x0

    move-object v3, v2

    move-object v4, v2

    .line 111
    :goto_0
    const-string v2, "location"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 112
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_22

    .line 113
    const/4 v2, 0x0

    move-object v5, v2

    .line 116
    :goto_1
    const-string v2, "device"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 117
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_21

    .line 118
    const/4 v2, 0x0

    move-object v6, v2

    .line 121
    :goto_2
    const/4 v2, 0x0

    .line 123
    const-string v7, "category"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 124
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 125
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    new-array v8, v2, [Ljava/lang/String;

    .line 126
    const/4 v2, 0x0

    move v7, v2

    :goto_3
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    if-ge v7, v2, :cond_2

    .line 127
    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v8, v7

    .line 126
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_3

    :cond_2
    move-object v2, v8

    .line 131
    :cond_3
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 132
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v12

    .line 135
    if-eqz v4, :cond_4

    .line 136
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 137
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 139
    const-wide/16 v18, 0x3e8

    div-long v14, v14, v18

    .line 140
    const-wide/16 v18, 0x3e8

    div-long v16, v16, v18

    .line 141
    const/16 v3, 0x28

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 142
    const-string v3, "date_added>=? AND "

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    const-string v3, "date_added<=?) AND "

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_4
    if-eqz v5, :cond_5

    .line 149
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "geo_loc_sub_locality LIKE \'%\' || \'"

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "\' || \'%\' COLLATE NOCASE OR geo_loc_locality LIKE \'%\' || \'"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "\' || \'%\' COLLATE NOCASE OR geo_loc_province LIKE \'%\' || \'"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "\' || \'%\' COLLATE NOCASE OR geo_loc_country LIKE \'%\' || \'"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'|| \'%\' COLLATE NOCASE"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    const/16 v4, 0x28

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 168
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 169
    const-string v3, ") AND "

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    :cond_5
    if-eqz v2, :cond_c

    .line 173
    const/16 v3, 0x28

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 174
    const/4 v3, 0x0

    :goto_4
    array-length v4, v2

    if-ge v3, v4, :cond_b

    .line 175
    const-string v4, "media_type=?"

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    aget-object v4, v2, v3

    const-string v5, "Images"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 177
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_6
    :goto_5
    array-length v4, v2

    const/4 v5, 0x1

    if-le v4, v5, :cond_7

    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_7

    .line 188
    const-string v4, " OR "

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 178
    :cond_8
    aget-object v4, v2, v3

    const-string v5, "Music"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 179
    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 180
    :cond_9
    aget-object v4, v2, v3

    const-string v5, "Videos"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 181
    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 182
    :cond_a
    aget-object v4, v2, v3

    const-string v5, "Document"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 183
    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 191
    :cond_b
    const-string v2, ") AND "

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    :cond_c
    const/4 v2, 0x0

    .line 195
    sparse-switch p3, :sswitch_data_0

    :goto_6
    move-object v8, v2

    .line 329
    :goto_7
    const-string v2, "CHJ"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sb : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    invoke-static {}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$AllMediaSearch;->getGroupByFileIdUri()Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_17

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object v6, v2

    :goto_8
    const/4 v7, 0x0

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 338
    invoke-virtual/range {p2 .. p2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/media/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/media/d;

    move-result-object v14

    .line 340
    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v15

    .line 342
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    .line 343
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v16, v0

    .line 345
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 346
    :goto_9
    sget-object v2, Lcom/mfluent/asp/datamodel/ar;->e:[Ljava/lang/String;

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/Object;

    move-object/from16 v17, v0

    .line 349
    const-string v2, "_id"

    invoke-static {v13, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    .line 350
    const-string v3, "media_type"

    invoke-static {v13, v3}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v18

    .line 353
    sparse-switch v18, :sswitch_data_1

    .line 369
    :goto_a
    const/4 v2, 0x1

    invoke-virtual/range {p2 .. p2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    invoke-static {v13}, Lcom/mfluent/asp/util/w;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v17, v2

    .line 371
    const/4 v2, 0x2

    invoke-virtual/range {p2 .. p2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v13}, Lcom/mfluent/asp/util/w;->a(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v17, v2

    .line 374
    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v17, v2

    .line 375
    const-string v2, "device_id"

    invoke-static {v13, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v19

    .line 376
    invoke-virtual {v15}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    move/from16 v0, v19

    if-ne v2, v0, :cond_1c

    .line 377
    const-string v2, "source_media_id"

    invoke-static {v13, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v20

    .line 380
    packed-switch v18, :pswitch_data_0

    .line 450
    :cond_d
    :goto_b
    const/4 v2, 0x3

    aget-object v2, v17, v2

    if-nez v2, :cond_1f

    .line 451
    sparse-switch v18, :sswitch_data_2

    :goto_c
    move v2, v9

    .line 467
    :goto_d
    invoke-virtual {v15}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    move/from16 v0, v19

    if-eq v3, v0, :cond_f

    .line 468
    invoke-virtual {v11}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_e

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 469
    :cond_e
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v11, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    :cond_f
    :goto_e
    const-string v3, "_size"

    invoke-static {v13, v3}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    .line 480
    const/4 v3, 0x5

    invoke-virtual/range {p2 .. p2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v6, v7}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v17, v3

    .line 483
    const/4 v3, 0x6

    const-string v5, "mime_type"

    invoke-static {v13, v5}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v17, v3

    .line 486
    const-string v3, "device_id"

    invoke-static {v13, v3}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v3

    int-to-long v6, v3

    invoke-virtual {v12, v6, v7}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    .line 487
    if-eqz v3, :cond_10

    .line 488
    const/4 v5, 0x7

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v17, v5

    .line 492
    :cond_10
    const/16 v3, 0x8

    const-string v5, "_id"

    invoke-static {v13, v5}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v17, v3

    .line 495
    const/16 v3, 0x9

    const-string v5, "orientation"

    invoke-static {v13, v5}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v17, v3

    .line 497
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/ar;->addRow([Ljava/lang/Object;)V

    .line 499
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1e

    .line 503
    :goto_f
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 505
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "SearchSuggest query:{} result:{}"

    new-instance v5, Lcom/mfluent/asp/datamodel/ar$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/mfluent/asp/datamodel/ar$1;-><init>(Lcom/mfluent/asp/datamodel/ar;)V

    invoke-interface {v3, v4, v8, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 517
    if-eqz v2, :cond_1d

    invoke-virtual {v11}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1d

    .line 519
    invoke-virtual {v11}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    .line 520
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 521
    :goto_10
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 522
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 523
    const-class v3, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v3}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 524
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.mfluent.asp.sync.ASPThumbnailPrefetcherService.ACTION_PREFETCH_DEVICE"

    const/4 v7, 0x0

    const-class v8, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;

    invoke-direct {v5, v6, v7, v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 525
    const-string v6, "DEVICE_ID"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 526
    const-string v2, "TRIGGER_BY_SFINDER"

    const/4 v6, 0x1

    invoke-virtual {v5, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 527
    invoke-virtual {v3, v5}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_10

    .line 198
    :sswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 199
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->trimToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 201
    const/16 v2, 0x28

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 202
    const/16 v2, 0xd

    move/from16 v0, p3

    if-ne v0, v2, :cond_14

    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 203
    new-instance v2, Lcom/samsung/android/a/a/a/a;

    invoke-direct {v2}, Lcom/samsung/android/a/a/a/a;-><init>()V

    .line 204
    invoke-virtual {v2, v3}, Lcom/samsung/android/a/a/a/a;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 206
    const/4 v2, 0x0

    :goto_11
    array-length v5, v4

    if-ge v2, v5, :cond_12

    .line 207
    const-string v5, "keyword"

    aget-object v8, v4, v2

    invoke-static {v5, v8}, Lcom/mfluent/asp/util/UiUtils;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    aget-object v5, v4, v2

    invoke-static {v5}, Lcom/mfluent/asp/util/UiUtils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    if-ge v2, v5, :cond_11

    .line 210
    const/16 v5, 0x20

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 211
    add-int/lit8 v5, v2, 0x1

    aget-object v5, v4, v5

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    const/16 v5, 0x20

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 206
    :cond_11
    add-int/lit8 v2, v2, 0x2

    goto :goto_11

    .line 216
    :cond_12
    const-string v2, ") AND ("

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    if-eqz v6, :cond_13

    .line 218
    const-string v2, "device_id==?"

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    :goto_12
    const/16 v2, 0x29

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-object v8, v3

    .line 231
    goto/16 :goto_7

    .line 221
    :cond_13
    const-string v2, "device_id!=?"

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_12

    .line 224
    :cond_14
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 225
    const-string v2, "keyword LIKE %"

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_12

    .line 227
    :cond_15
    const-string v2, "keyword"

    invoke-static {v2, v3}, Lcom/mfluent/asp/util/UiUtils;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    invoke-static {v3}, Lcom/mfluent/asp/util/UiUtils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_12

    .line 233
    :sswitch_1
    const/16 v3, 0x28

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 234
    if-eqz v6, :cond_16

    .line 235
    const-string v3, "device_id==?"

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    :goto_13
    const/16 v3, 0x29

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 238
    :cond_16
    const-string v3, "device_id!=?"

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    invoke-virtual {v12}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 331
    :cond_17
    const/4 v6, 0x0

    goto/16 :goto_8

    .line 355
    :sswitch_2
    const/4 v3, 0x0

    int-to-long v6, v2

    invoke-static {v6, v7}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v17, v3

    goto/16 :goto_a

    .line 358
    :sswitch_3
    const/4 v3, 0x0

    int-to-long v6, v2

    invoke-static {v6, v7}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v17, v3

    goto/16 :goto_a

    .line 361
    :sswitch_4
    const/4 v3, 0x0

    int-to-long v6, v2

    invoke-static {v6, v7}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v17, v3

    goto/16 :goto_a

    .line 364
    :sswitch_5
    const/4 v3, 0x0

    int-to-long v6, v2

    invoke-static {v6, v7}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v17, v3

    goto/16 :goto_a

    .line 383
    :pswitch_0
    const/4 v2, 0x1

    move/from16 v0, v18

    if-ne v0, v2, :cond_1a

    .line 384
    sget-object v2, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v10, v2

    .line 388
    :goto_14
    const/4 v2, 0x0

    const-string v3, "_data"

    aput-object v3, v4, v2

    .line 389
    invoke-virtual/range {p2 .. p2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move/from16 v0, v20

    int-to-long v6, v0

    invoke-static {v10, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const-string v5, "kind=1"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 398
    if-eqz v2, :cond_19

    .line 399
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 400
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 401
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 402
    const/4 v5, 0x3

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v17, v5

    .line 405
    :cond_18
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 406
    :cond_19
    const/4 v2, 0x3

    aget-object v2, v17, v2

    if-nez v2, :cond_d

    .line 409
    invoke-virtual {v10}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 410
    const-string v3, "orig_id"

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 411
    const-string v3, "blocking"

    const-string v5, "1"

    invoke-virtual {v2, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 412
    const/4 v3, 0x3

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v17, v3

    goto/16 :goto_b

    .line 386
    :cond_1a
    sget-object v2, Landroid/provider/MediaStore$Video$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v10, v2

    goto :goto_14

    .line 418
    :pswitch_1
    sget-object v3, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 419
    const/4 v2, 0x0

    const-string v5, "album_art"

    aput-object v5, v4, v2

    .line 420
    const/4 v2, 0x0

    const-string v5, "source_album_id"

    invoke-static {v13, v5}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v16, v2

    .line 421
    invoke-virtual/range {p2 .. p2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "_id=?"

    const/4 v7, 0x0

    move-object/from16 v6, v16

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 425
    if-eqz v2, :cond_d

    .line 426
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 427
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 428
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 429
    const/4 v5, 0x3

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v17, v5

    .line 432
    :cond_1b
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_b

    .line 437
    :cond_1c
    if-eqz v14, :cond_d

    .line 438
    invoke-static {v13}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fromCursor(Landroid/database/Cursor;)Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v2

    .line 439
    invoke-virtual {v14, v2}, Lcom/mfluent/asp/media/d;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Lcom/mfluent/asp/media/c/d;

    move-result-object v2

    .line 441
    if-eqz v2, :cond_d

    .line 444
    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$ThumbnailCache;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    .line 445
    invoke-virtual {v2}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 446
    const/4 v2, 0x3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v17, v2

    goto/16 :goto_b

    .line 453
    :sswitch_6
    const/4 v2, 0x3

    sget-object v3, Lcom/mfluent/asp/datamodel/ar;->b:Ljava/lang/String;

    aput-object v3, v17, v2

    .line 454
    const/4 v2, 0x1

    .line 455
    goto/16 :goto_d

    .line 457
    :sswitch_7
    const/4 v2, 0x3

    sget-object v3, Lcom/mfluent/asp/datamodel/ar;->a:Ljava/lang/String;

    aput-object v3, v17, v2

    move v2, v9

    .line 458
    goto/16 :goto_d

    .line 460
    :sswitch_8
    const/4 v2, 0x3

    sget-object v3, Lcom/mfluent/asp/datamodel/ar;->c:Ljava/lang/String;

    aput-object v3, v17, v2

    .line 461
    const/4 v2, 0x1

    .line 462
    goto/16 :goto_d

    .line 464
    :sswitch_9
    const/4 v2, 0x3

    sget-object v3, Lcom/mfluent/asp/datamodel/ar;->d:Ljava/lang/String;

    aput-object v3, v17, v2

    goto/16 :goto_c

    .line 531
    :cond_1d
    return-void

    :cond_1e
    move v9, v2

    goto/16 :goto_9

    :cond_1f
    move v2, v9

    goto/16 :goto_e

    :cond_20
    move v2, v9

    goto/16 :goto_f

    :cond_21
    move-object v6, v2

    goto/16 :goto_2

    :cond_22
    move-object v5, v2

    goto/16 :goto_1

    :cond_23
    move-object v4, v3

    move-object v3, v2

    goto/16 :goto_0

    .line 195
    nop

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0xd -> :sswitch_0
        0x16 -> :sswitch_1
    .end sparse-switch

    .line 353
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0xf -> :sswitch_5
    .end sparse-switch

    .line 380
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 451
    :sswitch_data_2
    .sparse-switch
        0x1 -> :sswitch_6
        0x2 -> :sswitch_7
        0x3 -> :sswitch_8
        0xf -> :sswitch_9
    .end sparse-switch
.end method
