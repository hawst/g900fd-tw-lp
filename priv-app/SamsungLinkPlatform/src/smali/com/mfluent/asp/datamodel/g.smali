.class public final Lcom/mfluent/asp/datamodel/g;
.super Lcom/mfluent/asp/datamodel/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/datamodel/a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "artist"

    aput-object v1, v0, v4

    const-string v1, "artist_index_char"

    aput-object v1, v0, v5

    const-string v1, "artist_key"

    aput-object v1, v0, v6

    const-string v1, "COUNT(DISTINCT album_id) AS number_of_albums"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "COUNT(*) AS number_of_tracks"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "COUNT(DISTINCT dup_id) AS num_dup_reduced_tracks"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "source_media_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "thumbnail_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "source_album_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "device_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "local_source_album_id"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "local_thumb_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/g;->a:[Ljava/lang/String;

    .line 48
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "artist"

    aput-object v1, v0, v4

    const-string v1, "artist_index_char"

    aput-object v1, v0, v5

    const-string v1, "artist_key"

    aput-object v1, v0, v6

    const-string v1, "COUNT(DISTINCT album_id) AS number_of_albums"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "COUNT(*) AS number_of_tracks"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "COUNT(DISTINCT dup_id) AS num_dup_reduced_tracks"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "device_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "local_source_album_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "local_thumb_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/g;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/a;-><init>()V

    .line 73
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/g;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 210
    const/4 v0, 0x0

    .line 212
    const-string v1, "artist"

    invoke-virtual {p0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 213
    const-string v0, "artist"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "<unknown>"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 216
    :cond_0
    const-string v0, "<unknown>"

    .line 219
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 221
    const-string v2, "artist"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v2, "artist_key"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v2, "artist_index_char"

    invoke-static {v0}, Lcom/sec/pcw/util/LanguageUtil;->a(Ljava/lang/String;)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 226
    :cond_2
    return-object v0
.end method

.method public static g()Lcom/mfluent/asp/datamodel/g;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/mfluent/asp/datamodel/g$a;->a()Lcom/mfluent/asp/datamodel/g;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;JLandroid/content/ContentValues;)I
    .locals 9

    .prologue
    .line 256
    invoke-static {p2}, Lcom/mfluent/asp/datamodel/g;->a(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v3

    .line 258
    if-nez v3, :cond_0

    .line 259
    const/4 v0, 0x0

    .line 262
    :goto_0
    return v0

    :cond_0
    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    move-object/from16 v8, p7

    invoke-super/range {v1 .. v8}, Lcom/mfluent/asp/datamodel/a;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;JLandroid/content/ContentValues;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;[Ljava/lang/String;I)I
    .locals 10

    .prologue
    const/16 v9, 0x29

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 184
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/g;->e()I

    move-result v0

    if-lez v0, :cond_1

    .line 185
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ARTIST_ORPHANS"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "artist_key"

    aput-object v3, v2, v8

    move-object v3, p2

    move-object v4, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 186
    if-eqz v0, :cond_1

    .line 187
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 190
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/datamodel/g;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 192
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 196
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 198
    const-string v1, "_id IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    const-string v1, "SELECT _id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    const-string v1, " FROM ARTIST_ORPHANS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 202
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 203
    const-string v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 206
    :cond_2
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "ARTISTS"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J
    .locals 2

    .prologue
    .line 245
    invoke-static {p2}, Lcom/mfluent/asp/datamodel/g;->a(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v0

    .line 247
    if-nez v0, :cond_0

    .line 248
    const-wide/16 v0, -0x1

    .line 251
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1, v0, p3}, Lcom/mfluent/asp/datamodel/a;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 23
    invoke-super/range {p0 .. p10}, Lcom/mfluent/asp/datamodel/a;->a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 82
    const-string v0, "artist"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 132
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "<unknown>"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    :cond_0
    const-string p2, "<unknown>"

    .line 136
    :cond_1
    invoke-static {p2}, Landroid/provider/MediaStore$Audio;->keyFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 140
    invoke-virtual {p0, p1, v9}, Lcom/mfluent/asp/datamodel/g;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v8

    .line 141
    if-nez v8, :cond_5

    .line 142
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ARTISTS"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v6

    const-string v3, "artist_key=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object v9, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 151
    if-eqz v1, :cond_4

    .line 153
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 154
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 157
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 161
    :goto_1
    if-nez v0, :cond_2

    .line 162
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 163
    const-string v1, "artist"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v1, "artist_key"

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v1, "artist_index_char"

    invoke-static {p2}, Lcom/sec/pcw/util/LanguageUtil;->a(Ljava/lang/String;)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "ARTISTS"

    invoke-virtual {v1, v2, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 168
    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 171
    :cond_2
    invoke-virtual {p0, p1, v9, v0}, Lcom/mfluent/asp/datamodel/g;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/Object;Ljava/lang/Integer;)V

    .line 174
    :goto_2
    return-object v0

    .line 157
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    move-object v0, v8

    goto :goto_0

    :cond_4
    move-object v0, v8

    goto :goto_1

    :cond_5
    move-object v0, v8

    goto :goto_2
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    const-string v0, "_id"

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    invoke-super {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/a;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)V

    return-void
.end method

.method protected final a(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    sget-object v0, Lcom/mfluent/asp/datamodel/g;->b:[Ljava/lang/String;

    .line 239
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/mfluent/asp/datamodel/g;->a:[Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 87
    const-string v0, "artist"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->b(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    const-string v0, "ARTISTS_DETAIL"

    return-object v0
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 77
    const/16 v0, 0x96

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    const-string v0, "ARTISTS"

    return-object v0
.end method

.method public final d_()Z
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x1

    return v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Artists;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Artists;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Artists;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
