.class public Lcom/mfluent/asp/datamodel/a/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/datamodel/a/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/a/f$1;,
        Lcom/mfluent/asp/datamodel/a/f$a;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/mfluent/asp/datamodel/a/f;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/mfluent/asp/datamodel/a/f$a;)Lcom/mfluent/asp/datamodel/a/f$a;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 102
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    .line 105
    iget-object v3, p1, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v3

    if-nez v3, :cond_1

    .line 106
    sget-object v3, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    const-string v4, "::changeToAvailableContents: device is offline. so, try to search available device having same content."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 107
    iget-object v3, p1, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    const-string v4, "dup_id"

    invoke-static {v3, v4}, Lcom/mfluent/asp/common/util/CursorUtils;->getLongOrThrow(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 108
    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string v6, "dup_id"

    aput-object v6, v3, v1

    const-string v6, "device_id"

    aput-object v6, v3, v0

    const-string v6, "full_uri"

    aput-object v6, v3, v8

    const/4 v6, 0x3

    const-string v7, "source_media_id"

    aput-object v7, v3, v6

    const/4 v6, 0x4

    const-string v7, "local_source_media_id"

    aput-object v7, v3, v6

    invoke-static {p0, v4, v5, v8, v3}, Lcom/mfluent/asp/datamodel/a/q;->a(Landroid/content/Context;JI[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 109
    sget-object v6, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "::changeToAvailableContents: dup_id : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 111
    if-eqz v3, :cond_4

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 113
    :cond_0
    invoke-static {v3, v2}, Lcom/mfluent/asp/datamodel/a/q;->a(Landroid/database/Cursor;Lcom/mfluent/asp/datamodel/t;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v6

    .line 114
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 116
    iput-object v3, p1, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    .line 117
    iput-object v6, p1, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 118
    sget-object v1, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    const-string v2, "::changeToAvailableContents: Find presence device."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 119
    sget-object v1, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::changeToAvailableContents: NEW - params.device.getPeerId() :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 120
    sget-object v1, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::changeToAvailableContents: NEW - params.device.getDisplayName() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 122
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    const-string v2, "full_uri"

    invoke-static {v1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/mfluent/asp/datamodel/a/f$a;->c:Ljava/lang/String;

    .line 123
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/a/f$a;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 124
    sget-object v0, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    const-string v1, "::changeToAvailableContents: Empty FULL_URI column. Returning null."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 125
    const/4 p1, 0x0

    .line 136
    :cond_1
    :goto_0
    return-object p1

    .line 127
    :cond_2
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v1

    iput-object v1, p1, Lcom/mfluent/asp/datamodel/a/f$a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 132
    :goto_1
    if-nez v0, :cond_1

    .line 133
    sget-object v0, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::changeToAvailableContents: can\'t search available device having same content with dup id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :cond_3
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 48
    sget-object v0, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    const-string v1, "::call: entered"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 49
    new-instance v7, Lcom/mfluent/asp/datamodel/a/f$a;

    invoke-direct {v7, v2}, Lcom/mfluent/asp/datamodel/a/f$a;-><init>(B)V

    .line 51
    const/4 v0, 0x5

    :try_start_0
    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "dup_id"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-string v1, "device_id"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "full_uri"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    const-string v1, "source_media_id"

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "local_source_media_id"

    aput-object v1, v4, v0

    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetAudioUriInfo.INTENT_ARG_CONTENT_ID"

    const/4 v3, 0x2

    const-string v5, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetAudioUriInfo.NAME"

    move-object v0, p1

    move-object v1, p3

    invoke-static/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/a/q;->a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    .line 52
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 53
    sget-object v0, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    const-string v1, "::call: No cursor. Returning null"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v6

    .line 57
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 58
    if-nez v0, :cond_2

    .line 59
    sget-object v0, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    const-string v1, "::call: DataModel not available. Returning empty bundle"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 60
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 63
    :cond_2
    :try_start_2
    iget-object v1, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    invoke-static {v1, v0}, Lcom/mfluent/asp/datamodel/a/q;->a(Landroid/database/Cursor;Lcom/mfluent/asp/datamodel/t;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 64
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    if-nez v0, :cond_3

    .line 65
    sget-object v0, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    const-string v1, "::call: No device. Returning null"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 66
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 68
    :cond_3
    :try_start_3
    sget-object v0, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ORIGINAL - params.device.getPeerId() :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v7, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 69
    sget-object v0, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ORIGINAL - params.device.getDisplayName() :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v7, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 71
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    const-string v1, "full_uri"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->c:Ljava/lang/String;

    .line 72
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 73
    sget-object v0, Lcom/mfluent/asp/datamodel/a/f;->a:Lorg/slf4j/Logger;

    const-string v1, "::call: Empty FULL_URI column. Returning null."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 74
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 76
    :cond_4
    :try_start_4
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/datamodel/a/f$a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 78
    new-instance v0, Lcom/mfluent/asp/datamodel/a/f$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/mfluent/asp/datamodel/a/f$a;-><init>(B)V

    .line 79
    invoke-static {p1, v7}, Lcom/mfluent/asp/datamodel/a/f;->a(Landroid/content/Context;Lcom/mfluent/asp/datamodel/a/f$a;)Lcom/mfluent/asp/datamodel/a/f$a;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    .line 80
    if-eqz v2, :cond_c

    .line 84
    :goto_1
    :try_start_5
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 85
    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetAudioUriInfo.KEY_RESULT_HTTP_PROXY_URI"

    sget-object v0, Lcom/mfluent/asp/datamodel/a/f$1;->a:[I

    iget-object v4, v2, Lcom/mfluent/asp/datamodel/a/f$a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    move-object v0, v6

    :goto_2
    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 86
    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetAudioUriInfo.KEY_RESULT_SCS_URI"

    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/f$a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v0, v4, :cond_9

    move-object v0, v6

    :goto_3
    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 90
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetAudioUriInfo.KEY_RESULT_SAME_ACCESS_POINT_URI"

    iget-object v3, v2, Lcom/mfluent/asp/datamodel/a/f$a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v3, v4, :cond_5

    iget-object v3, v2, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-static {v3}, Lcom/mfluent/asp/util/r;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v2, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v3

    if-eqz v3, :cond_a

    :cond_5
    :goto_4
    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 91
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetAudioUriInfo.KEY_RESULT_LOCAL_FILE_URI"

    iget-object v3, v2, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    const-string v4, "local_source_media_id"

    invoke-static {v3, v4}, Lcom/mfluent/asp/common/util/CursorUtils;->getLongOrThrow(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    const/4 v3, 0x2

    invoke-static {v3, v4, v5}, Lcom/mfluent/asp/datamodel/a/q;->a(IJ)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 92
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_6

    .line 95
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_6
    move-object v6, v1

    goto/16 :goto_0

    .line 85
    :pswitch_0
    :try_start_6
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/f$a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v0, v4, :cond_7

    move-object v0, v6

    goto :goto_2

    :cond_7
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    const-string v4, "source_media_id"

    invoke-static {v0, v4}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, v2, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    const/4 v5, 0x2

    iget-object v7, v2, Lcom/mfluent/asp/datamodel/a/f$a;->c:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v4, v5, v7, v0, v8}, Lcom/mfluent/asp/datamodel/a/q;->a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    :pswitch_1
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/f$a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v0, v4, :cond_8

    move-object v0, v6

    goto :goto_2

    :cond_8
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v4, v2, Lcom/mfluent/asp/datamodel/a/f$a;->c:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Lcom/mfluent/asp/datamodel/a/q;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    .line 86
    :cond_9
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v4, v2, Lcom/mfluent/asp/datamodel/a/f$a;->c:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Lcom/mfluent/asp/datamodel/a/q;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    .line 90
    :cond_a
    iget-object v3, v2, Lcom/mfluent/asp/datamodel/a/f$a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->l()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lcom/mfluent/asp/datamodel/a/f$a;->c:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/mfluent/asp/datamodel/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v6

    goto :goto_4

    .line 94
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_5
    iget-object v2, v1, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    if-eqz v2, :cond_b

    .line 95
    iget-object v1, v1, Lcom/mfluent/asp/datamodel/a/f$a;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v0

    .line 94
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_5

    :cond_c
    move-object v2, v7

    goto/16 :goto_1

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method
