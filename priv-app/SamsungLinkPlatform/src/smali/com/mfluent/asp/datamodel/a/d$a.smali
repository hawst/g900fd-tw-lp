.class final Lcom/mfluent/asp/datamodel/a/d$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/filetransfer/FileTransferSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/ContentValues;

.field private b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    invoke-static {p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->fromCursor(Landroid/database/Cursor;)Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    .line 118
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->a:Landroid/content/ContentValues;

    .line 119
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->a:Landroid/content/ContentValues;

    invoke-static {p1, v0}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 121
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->a:Landroid/content/ContentValues;

    const-string v1, "uuid"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V
    .locals 1

    .prologue
    .line 268
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    if-ne p1, v0, :cond_0

    .line 269
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    .line 271
    :cond_0
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->a:Landroid/content/ContentValues;

    const-string v1, "createdTime"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    return v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->a:Landroid/content/ContentValues;

    const-string v1, "numFiles"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->a:Landroid/content/ContentValues;

    const-string v1, "numFilesSkipped"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final h()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->a:Landroid/content/ContentValues;

    const-string v1, "sourceDeviceId"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 161
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 162
    if-nez v0, :cond_0

    .line 163
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 165
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Lcom/mfluent/asp/datamodel/Device;
    .locals 4

    .prologue
    .line 171
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->a:Landroid/content/ContentValues;

    const-string v1, "targetDeviceId"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 172
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    return-object v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 177
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->a:Landroid/content/ContentValues;

    const-string v1, "transferSize"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/a/d$a;->k()J

    move-result-wide v0

    return-wide v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->a:Landroid/content/ContentValues;

    const-string v1, "firstFileName"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x1

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x0

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/a/d$a;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    return-object v0
.end method

.method public final t()J
    .locals 2

    .prologue
    .line 227
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final u()J
    .locals 2

    .prologue
    .line 232
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/a/d$a;->b:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    .line 240
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    goto :goto_0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x0

    return-object v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x0

    return v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    return v0
.end method
