.class public Lcom/mfluent/asp/datamodel/aj;
.super Lcom/mfluent/asp/datamodel/l;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/aj$a;
    }
.end annotation


# static fields
.field private static b:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/mfluent/asp/datamodel/aj;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/aj;->b:Lorg/slf4j/Logger;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/l;-><init>()V

    .line 48
    return-void
.end method

.method public static l()Lcom/mfluent/asp/datamodel/aj;
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lcom/mfluent/asp/datamodel/aj$a;->a()Lcom/mfluent/asp/datamodel/aj;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 52
    const-string v0, "image"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 57
    const-string v0, "image"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->b(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const-string v0, "IMAGES"

    return-object v0
.end method

.method protected final d(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)J
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 121
    .line 122
    const-string v0, "group_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    const-string v0, "group_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 141
    :goto_0
    invoke-static {v6}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "0"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 142
    :cond_0
    const-wide/16 p3, 0x0

    .line 158
    :cond_1
    :goto_1
    return-wide p3

    .line 124
    :cond_2
    if-nez p5, :cond_6

    .line 125
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "IMAGES"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "group_id"

    aput-object v3, v2, v8

    const-string v3, "_id=?"

    new-array v4, v9, [Ljava/lang/String;

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v8

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 133
    if-eqz v1, :cond_6

    .line 134
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 135
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 137
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v6, v0

    goto :goto_0

    .line 147
    :cond_3
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "IMAGES"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "burst_id"

    aput-object v3, v2, v8

    const-string v3, "burst_id IS NOT NULL AND group_id=?"

    new-array v4, v9, [Ljava/lang/String;

    aput-object v6, v4, v8

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 151
    if-eqz v0, :cond_1

    .line 152
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 153
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p3

    .line 155
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_5
    move-object v0, v5

    goto :goto_2

    :cond_6
    move-object v6, v5

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    const-string v0, "FILES"

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method protected final g()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/mfluent/asp/datamodel/aj;->b:Lorg/slf4j/Logger;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected final j()I
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    return v0
.end method
