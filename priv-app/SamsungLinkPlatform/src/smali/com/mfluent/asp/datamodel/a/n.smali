.class public Lcom/mfluent/asp/datamodel/a/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/datamodel/a/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/a/n$1;,
        Lcom/mfluent/asp/datamodel/a/n$a;,
        Lcom/mfluent/asp/datamodel/a/n$b;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/mfluent/asp/datamodel/a/n;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method private static a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 315
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    const/4 v0, 0x0

    .line 319
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2, p0, p3, p4}, Lcom/mfluent/asp/datamodel/a/q;->a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 323
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    const/4 v0, 0x0

    .line 334
    :goto_0
    return-object v0

    .line 333
    :cond_0
    if-nez p2, :cond_1

    const-string v0, ""

    .line 334
    :goto_1
    invoke-static {p1, p0, v0}, Lcom/mfluent/asp/datamodel/a/q;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 333
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "proxy."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p2, Lcom/mfluent/asp/datamodel/a/n$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/mfluent/asp/datamodel/a/n$a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 368
    if-nez p0, :cond_0

    .line 369
    const/4 v0, 0x0

    .line 377
    :goto_0
    return-object v0

    .line 371
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 372
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_VIDEO_URI"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 373
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_CAPTION_URI"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 374
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_CAPTION_INDEX_URI"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 375
    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_OPTIMIZED_VIDEO_URI"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method private static a(Lcom/mfluent/asp/datamodel/Device;Z)Lcom/mfluent/asp/datamodel/a/n$a;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 381
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v3

    .line 382
    if-nez v3, :cond_1

    move-object v5, v0

    .line 383
    :goto_0
    if-nez v5, :cond_2

    .line 422
    :cond_0
    :goto_1
    return-object v0

    .line 382
    :cond_1
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    move-object v5, v3

    goto :goto_0

    .line 387
    :cond_2
    new-instance v3, Lcom/mfluent/asp/datamodel/a/n$a;

    invoke-direct {v3, v1}, Lcom/mfluent/asp/datamodel/a/n$a;-><init>(B)V

    .line 390
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->v()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 391
    const-string v0, "HLS"

    .line 392
    const-string v4, "m3u8"

    iput-object v4, v3, Lcom/mfluent/asp/datamodel/a/n$a;->b:Ljava/lang/String;

    move-object v4, v0

    .line 402
    :goto_2
    if-eqz p1, :cond_5

    .line 403
    const-string v1, "720x480"

    .line 404
    const-string v0, "1228"

    .line 415
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "type="

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "&resolution="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&bitrate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/mfluent/asp/datamodel/a/n$a;->a:Ljava/lang/String;

    .line 418
    if-eqz p1, :cond_3

    .line 419
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v3, Lcom/mfluent/asp/datamodel/a/n$a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&sameAP=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/mfluent/asp/datamodel/a/n$a;->a:Ljava/lang/String;

    .line 421
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v3, Lcom/mfluent/asp/datamodel/a/n$a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&peerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/mfluent/asp/datamodel/a/n$a;->a:Ljava/lang/String;

    move-object v0, v3

    .line 422
    goto :goto_1

    .line 393
    :cond_4
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->w()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 394
    const-string v0, "TS"

    .line 395
    const-string v4, "ts"

    iput-object v4, v3, Lcom/mfluent/asp/datamodel/a/n$a;->b:Ljava/lang/String;

    move-object v4, v0

    goto :goto_2

    .line 405
    :cond_5
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v6

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v7

    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_TAB:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_6
    move v0, v2

    :goto_4
    sget-object v8, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-eq v6, v8, :cond_7

    sget-object v8, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_LTE:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne v6, v8, :cond_9

    :cond_7
    if-eqz v0, :cond_8

    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne v7, v0, :cond_9

    :cond_8
    move v1, v2

    :cond_9
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "::isGoodNetworkCondition: result = "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 406
    const-string v1, "320x240"

    .line 412
    const-string v0, "300"

    goto/16 :goto_3

    :cond_a
    move v0, v1

    .line 405
    goto :goto_4
.end method

.method private static a(Landroid/content/Context;Lcom/mfluent/asp/datamodel/a/n$b;)Lcom/mfluent/asp/datamodel/a/n$b;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 131
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    .line 135
    iget-object v3, p1, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v3

    if-nez v3, :cond_1

    .line 136
    sget-object v3, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    const-string v4, "::changeToAvailableContents: device is offline. so, try to search available device having same content."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 137
    iget-object v3, p1, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    const-string v4, "dup_id"

    invoke-static {v3, v4}, Lcom/mfluent/asp/common/util/CursorUtils;->getLongOrThrow(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 138
    sget-object v3, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::changeToAvailableContents: dup_id : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 139
    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/String;

    const-string v6, "dup_id"

    aput-object v6, v3, v1

    const-string v6, "device_id"

    aput-object v6, v3, v0

    const/4 v6, 0x2

    const-string v7, "full_uri"

    aput-object v7, v3, v6

    const-string v6, "source_media_id"

    aput-object v6, v3, v8

    const/4 v6, 0x4

    const-string v7, "caption_uri"

    aput-object v7, v3, v6

    const/4 v6, 0x5

    const-string v7, "caption_index_uri"

    aput-object v7, v3, v6

    const/4 v6, 0x6

    const-string v7, "local_data"

    aput-object v7, v3, v6

    const/4 v6, 0x7

    const-string v7, "local_caption_path"

    aput-object v7, v3, v6

    const/16 v6, 0x8

    const-string v7, "local_caption_index_path"

    aput-object v7, v3, v6

    const/16 v6, 0x9

    const-string v7, "caption_type"

    aput-object v7, v3, v6

    invoke-static {p0, v4, v5, v8, v3}, Lcom/mfluent/asp/datamodel/a/q;->a(Landroid/content/Context;JI[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 141
    if-eqz v3, :cond_4

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 143
    :cond_0
    invoke-static {v3, v2}, Lcom/mfluent/asp/datamodel/a/q;->a(Landroid/database/Cursor;Lcom/mfluent/asp/datamodel/t;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v6

    .line 144
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 146
    iput-object v3, p1, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    .line 147
    iput-object v6, p1, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 148
    sget-object v1, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    const-string v2, "::changeToAvailableContents: Find presence device."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 149
    sget-object v1, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "::changeToAvailableContents: NEW - params.device.getPeerId() :"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p1, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 150
    sget-object v1, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "::changeToAvailableContents: NEW - params.device.getDisplayName() : "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p1, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 152
    const-string v1, "full_uri"

    invoke-static {v3, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/mfluent/asp/datamodel/a/n$b;->c:Ljava/lang/String;

    .line 153
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/a/n$b;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 154
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    const-string v1, "::changeToAvailableContents: Empty FULL_URI column. Returning null."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 155
    const/4 p1, 0x0

    .line 169
    :cond_1
    :goto_0
    return-object p1

    .line 157
    :cond_2
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v1

    iput-object v1, p1, Lcom/mfluent/asp/datamodel/a/n$b;->g:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 158
    const-string v1, "caption_uri"

    invoke-static {v3, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/mfluent/asp/datamodel/a/n$b;->d:Ljava/lang/String;

    .line 159
    const-string v1, "caption_index_uri"

    invoke-static {v3, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/mfluent/asp/datamodel/a/n$b;->e:Ljava/lang/String;

    .line 165
    :goto_1
    if-nez v0, :cond_1

    .line 166
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::changeToAvailableContents: can\'t search available device having same content with dup id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 162
    :cond_3
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 358
    if-eqz p1, :cond_0

    .line 359
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/mfluent/asp/datamodel/a/n$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 363
    :cond_0
    return-object p0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 338
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 344
    :goto_0
    return-object v0

    .line 342
    :cond_0
    if-nez p2, :cond_1

    .line 343
    :goto_1
    invoke-static {p0, p2}, Lcom/mfluent/asp/datamodel/a/n;->a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Ljava/lang/String;

    move-result-object v1

    .line 344
    invoke-static {p1, v1, v0}, Lcom/mfluent/asp/datamodel/a/q;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 342
    :cond_1
    iget-object v0, p2, Lcom/mfluent/asp/datamodel/a/n$a;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 348
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 353
    :goto_0
    return-object v0

    .line 351
    :cond_0
    if-nez p2, :cond_1

    .line 352
    :goto_1
    invoke-static {p0, p2}, Lcom/mfluent/asp/datamodel/a/n;->a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Ljava/lang/String;

    move-result-object v1

    .line 353
    invoke-static {p1, v1, v0}, Lcom/mfluent/asp/datamodel/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 351
    :cond_1
    iget-object v0, p2, Lcom/mfluent/asp/datamodel/a/n$a;->a:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 58
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    const-string v1, "::call: entered"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 59
    new-instance v7, Lcom/mfluent/asp/datamodel/a/n$b;

    invoke-direct {v7, v2}, Lcom/mfluent/asp/datamodel/a/n$b;-><init>(B)V

    .line 62
    const/16 v0, 0xa

    :try_start_0
    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "dup_id"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-string v1, "device_id"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "full_uri"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    const-string v1, "source_media_id"

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "caption_uri"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    const-string v1, "caption_index_uri"

    aput-object v1, v4, v0

    const/4 v0, 0x6

    const-string v1, "local_data"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    const-string v1, "local_caption_path"

    aput-object v1, v4, v0

    const/16 v0, 0x8

    const-string v1, "local_caption_index_path"

    aput-object v1, v4, v0

    const/16 v0, 0x9

    const-string v1, "caption_type"

    aput-object v1, v4, v0

    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.INTENT_ARG_CONTENT_ID"

    const/4 v3, 0x3

    const-string v5, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.NAME"

    move-object v0, p1

    move-object v1, p3

    invoke-static/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/a/q;->a(Landroid/content/Context;Landroid/os/Bundle;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    .line 63
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 64
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    const-string v1, "::call: No cursor. Returning null"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 65
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v6

    .line 68
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 69
    if-nez v0, :cond_2

    .line 70
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    const-string v1, "::call: DataModel not available. Returning empty bundle"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 71
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 123
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 74
    :cond_2
    :try_start_2
    iget-object v1, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    invoke-static {v1, v0}, Lcom/mfluent/asp/datamodel/a/q;->a(Landroid/database/Cursor;Lcom/mfluent/asp/datamodel/t;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 75
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    if-nez v0, :cond_3

    .line 76
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    const-string v1, "::call: No device. Returning null"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 77
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 79
    :cond_3
    :try_start_3
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ORIGINAL - params.device.getPeerId() :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v7, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 80
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ORIGINAL - params.device.getDisplayName() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v7, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 82
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    const-string v1, "full_uri"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->c:Ljava/lang/String;

    .line 83
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 84
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    const-string v1, "::call: Empty FULL_URI column. Returning null."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 85
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 87
    :cond_4
    :try_start_4
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->g:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 88
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    const-string v1, "caption_uri"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->d:Ljava/lang/String;

    .line 89
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    const-string v1, "caption_index_uri"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->e:Ljava/lang/String;

    .line 90
    iget-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    const-string v1, "caption_type"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/mfluent/asp/datamodel/a/n$b;->f:Ljava/lang/String;

    .line 92
    new-instance v0, Lcom/mfluent/asp/datamodel/a/n$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/mfluent/asp/datamodel/a/n$b;-><init>(B)V

    .line 93
    invoke-static {p1, v7}, Lcom/mfluent/asp/datamodel/a/n;->a(Landroid/content/Context;Lcom/mfluent/asp/datamodel/a/n$b;)Lcom/mfluent/asp/datamodel/a/n$b;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v2

    .line 94
    if-eqz v2, :cond_10

    .line 98
    :goto_1
    :try_start_5
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 100
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    const-string v3, "local_source_media_id"

    invoke-static {v0, v3}, Lcom/mfluent/asp/common/util/CursorUtils;->getLongOrThrow(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-gtz v0, :cond_8

    move-object v4, v6

    .line 101
    :goto_2
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->g:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v0, v3, :cond_5

    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-static {v0}, Lcom/mfluent/asp/util/r;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 102
    :cond_5
    :goto_3
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->g:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v0, v3, :cond_c

    move-object v3, v6

    .line 103
    :goto_4
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n$1;->a:[I

    iget-object v5, v2, Lcom/mfluent/asp/datamodel/a/n$b;->g:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    .line 105
    :cond_6
    :goto_5
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_HTTP_PROXY_INFO"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 106
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_SCS_INFO"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 108
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_SAME_ACCESS_POINT_INFO"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 111
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_LOCAL_FILE_INFO"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 116
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    const-string v5, "::call: local : {}"

    const/4 v7, 0x0

    invoke-static {v4, v7}, Lorg/apache/commons/lang3/ObjectUtils;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v5, v4}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 117
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::call: sameAP : {}"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lorg/apache/commons/lang3/ObjectUtils;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 118
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::call: scs : {}"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lorg/apache/commons/lang3/ObjectUtils;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 119
    sget-object v0, Lcom/mfluent/asp/datamodel/a/n;->a:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::call: proxy : {}"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-static {v6, v4}, Lorg/apache/commons/lang3/ObjectUtils;->toString(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 121
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_7

    .line 124
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_7
    move-object v6, v1

    goto/16 :goto_0

    .line 100
    :cond_8
    :try_start_6
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    const-string v3, "local_caption_path"

    invoke-static {v0, v3}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, v2, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    const-string v7, "local_caption_index_path"

    invoke-static {v3, v7}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x3

    invoke-static {v7, v4, v5}, Lcom/mfluent/asp/datamodel/a/q;->a(IJ)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/a/q;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v3}, Lcom/mfluent/asp/datamodel/a/q;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v4, v0, v3, v5}, Lcom/mfluent/asp/datamodel/a/n;->a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v0

    move-object v4, v0

    goto/16 :goto_2

    .line 101
    :cond_9
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    const/4 v3, 0x1

    invoke-static {v0, v3}, Lcom/mfluent/asp/datamodel/a/n;->a(Lcom/mfluent/asp/datamodel/Device;Z)Lcom/mfluent/asp/datamodel/a/n$a;

    move-result-object v0

    iget-object v3, v2, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->l()Ljava/lang/String;

    move-result-object v3

    iget-object v5, v2, Lcom/mfluent/asp/datamodel/a/n$b;->c:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v5, v3, v7}, Lcom/mfluent/asp/datamodel/a/n;->c(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;

    move-result-object v5

    iget-object v7, v2, Lcom/mfluent/asp/datamodel/a/n$b;->d:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v7, v3, v8}, Lcom/mfluent/asp/datamodel/a/n;->c(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;

    move-result-object v7

    iget-object v8, v2, Lcom/mfluent/asp/datamodel/a/n$b;->e:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v8, v3, v9}, Lcom/mfluent/asp/datamodel/a/n;->c(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;

    move-result-object v8

    if-nez v0, :cond_b

    move-object v0, v6

    :goto_6
    invoke-static {v5, v7, v8, v0}, Lcom/mfluent/asp/datamodel/a/n;->a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)Landroid/os/Bundle;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_3

    .line 123
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_7
    iget-object v2, v1, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    if-eqz v2, :cond_a

    .line 124
    iget-object v1, v1, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v0

    .line 101
    :cond_b
    :try_start_7
    iget-object v9, v2, Lcom/mfluent/asp/datamodel/a/n$b;->c:Ljava/lang/String;

    invoke-static {v9, v3, v0}, Lcom/mfluent/asp/datamodel/a/n;->c(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_6

    .line 102
    :cond_c
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v3, v2, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lcom/mfluent/asp/datamodel/a/n;->a(Lcom/mfluent/asp/datamodel/Device;Z)Lcom/mfluent/asp/datamodel/a/n$a;

    move-result-object v3

    iget-object v5, v2, Lcom/mfluent/asp/datamodel/a/n$b;->c:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v5, v0, v7}, Lcom/mfluent/asp/datamodel/a/n;->b(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;

    move-result-object v5

    iget-object v7, v2, Lcom/mfluent/asp/datamodel/a/n$b;->d:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v7, v0, v8}, Lcom/mfluent/asp/datamodel/a/n;->b(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;

    move-result-object v7

    iget-object v8, v2, Lcom/mfluent/asp/datamodel/a/n$b;->e:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v8, v0, v9}, Lcom/mfluent/asp/datamodel/a/n;->b(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;

    move-result-object v8

    if-nez v3, :cond_d

    move-object v0, v6

    :goto_8
    invoke-static {v5, v7, v8, v0}, Lcom/mfluent/asp/datamodel/a/n;->a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_4

    :cond_d
    iget-object v9, v2, Lcom/mfluent/asp/datamodel/a/n$b;->c:Ljava/lang/String;

    invoke-static {v9, v0, v3}, Lcom/mfluent/asp/datamodel/a/n;->b(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_8

    .line 103
    :pswitch_0
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->g:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v0, v5, :cond_e

    move-object v0, v6

    :goto_9
    move-object v6, v0

    goto/16 :goto_5

    :cond_e
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->a:Landroid/database/Cursor;

    const-string v5, "source_media_id"

    invoke-static {v0, v5}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, v2, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v5

    iget-object v6, v2, Lcom/mfluent/asp/datamodel/a/n$b;->c:Ljava/lang/String;

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static {v6, v5, v7, v0, v8}, Lcom/mfluent/asp/datamodel/a/n;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, v2, Lcom/mfluent/asp/datamodel/a/n$b;->d:Ljava/lang/String;

    const/16 v8, 0xd

    iget-object v9, v2, Lcom/mfluent/asp/datamodel/a/n$b;->f:Ljava/lang/String;

    invoke-static {v7, v5, v8, v0, v9}, Lcom/mfluent/asp/datamodel/a/n;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    iget-object v8, v2, Lcom/mfluent/asp/datamodel/a/n$b;->e:Ljava/lang/String;

    const/16 v9, 0xe

    const-string v10, "idx"

    invoke-static {v8, v5, v9, v0, v10}, Lcom/mfluent/asp/datamodel/a/n;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v5, 0x0

    invoke-static {v6, v7, v0, v5}, Lcom/mfluent/asp/datamodel/a/n;->a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_9

    :pswitch_1
    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->g:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v0, v5, :cond_6

    iget-object v0, v2, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v5, v2, Lcom/mfluent/asp/datamodel/a/n$b;->b:Lcom/mfluent/asp/datamodel/Device;

    const/4 v7, 0x0

    invoke-static {v5, v7}, Lcom/mfluent/asp/datamodel/a/n;->a(Lcom/mfluent/asp/datamodel/Device;Z)Lcom/mfluent/asp/datamodel/a/n$a;

    move-result-object v5

    iget-object v7, v2, Lcom/mfluent/asp/datamodel/a/n$b;->c:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v7, v0, v8}, Lcom/mfluent/asp/datamodel/a/n;->a(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;

    move-result-object v7

    iget-object v8, v2, Lcom/mfluent/asp/datamodel/a/n$b;->d:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v8, v0, v9}, Lcom/mfluent/asp/datamodel/a/n;->a(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;

    move-result-object v8

    iget-object v9, v2, Lcom/mfluent/asp/datamodel/a/n$b;->e:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v9, v0, v10}, Lcom/mfluent/asp/datamodel/a/n;->a(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;

    move-result-object v9

    if-nez v5, :cond_f

    :goto_a
    invoke-static {v7, v8, v9, v6}, Lcom/mfluent/asp/datamodel/a/n;->a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v6

    goto/16 :goto_5

    :cond_f
    iget-object v6, v2, Lcom/mfluent/asp/datamodel/a/n$b;->c:Ljava/lang/String;

    invoke-static {v6, v0, v5}, Lcom/mfluent/asp/datamodel/a/n;->a(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/a/n$a;)Landroid/net/Uri;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v6

    goto :goto_a

    .line 123
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto/16 :goto_7

    :cond_10
    move-object v2, v7

    goto/16 :goto_1

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method
