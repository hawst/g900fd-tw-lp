.class public final Lcom/mfluent/asp/datamodel/u;
.super Lcom/mfluent/asp/datamodel/ao;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/u$a;
    }
.end annotation


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 105
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "value"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/u;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ao;-><init>()V

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/u;->a:Ljava/util/HashMap;

    .line 40
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/u;-><init>()V

    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/u;
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcom/mfluent/asp/datamodel/u$a;->a()Lcom/mfluent/asp/datamodel/u;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 122
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0, p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a(Lcom/mfluent/asp/datamodel/ao;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 125
    if-nez v0, :cond_0

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 127
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v1, p0, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a(Lcom/mfluent/asp/datamodel/ao;Ljava/lang/Object;)V

    .line 129
    :cond_0
    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    :cond_1
    :goto_0
    return-void

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/u;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;JLandroid/content/ContentValues;)I
    .locals 3

    .prologue
    .line 138
    invoke-super/range {p0 .. p7}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;JLandroid/content/ContentValues;)I

    move-result v0

    .line 140
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 141
    const-string v1, "name"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 142
    const-string v2, "value"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 144
    invoke-direct {p0, p1, v1, v2}, Lcom/mfluent/asp/datamodel/u;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    :cond_0
    return v0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J
    .locals 4

    .prologue
    .line 109
    invoke-super {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 110
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 111
    const-string v2, "name"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 112
    const-string v3, "value"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 114
    invoke-direct {p0, p1, v2, v3}, Lcom/mfluent/asp/datamodel/u;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    return-wide v0
.end method

.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 165
    .line 167
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    if-eqz v0, :cond_3

    .line 169
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->b:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;

    invoke-virtual {v0, p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a(Lcom/mfluent/asp/datamodel/ao;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 170
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 172
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v8, v0

    move v0, v4

    .line 175
    :goto_0
    if-nez v0, :cond_0

    .line 176
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/u;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/u;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v8, v0

    move v0, v4

    .line 182
    :cond_0
    if-nez v0, :cond_1

    .line 183
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "db_integrity"

    sget-object v2, Lcom/mfluent/asp/datamodel/u;->b:[Ljava/lang/String;

    const-string v3, "name=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v9

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 191
    if-eqz v1, :cond_1

    .line 192
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 193
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 194
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/u;->a:Ljava/util/HashMap;

    invoke-virtual {v2, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v8, v0

    .line 200
    :cond_1
    return-object v8

    :cond_2
    move-object v0, v8

    goto :goto_1

    :cond_3
    move-object v8, v5

    move v0, v9

    goto :goto_0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 152
    invoke-super {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)V

    .line 154
    instance-of v0, p3, Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 156
    check-cast p3, Ljava/util/HashMap;

    .line 157
    invoke-virtual {p3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 158
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 159
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/u;->a:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 162
    :cond_0
    return-void
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const-string v0, "db_integrity"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const-string v0, "db_integrity"

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return-object v0
.end method
