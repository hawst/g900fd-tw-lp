.class public abstract Lcom/mfluent/asp/datamodel/n;
.super Lcom/mfluent/asp/datamodel/ap;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/n$1;
    }
.end annotation


# static fields
.field protected static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "local_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "local_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "local_thumb_data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "local_source_media_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/n;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ap;-><init>()V

    .line 133
    return-void
.end method

.method private b(Lcom/mfluent/asp/datamodel/ao$a;J)Lcom/mfluent/asp/media/b;
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 63
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "device_id"

    aput-object v0, v2, v1

    const-string v0, "source_media_id"

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v1, "media_type"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "full_uri"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 72
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->f:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/a/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/lang/Long;

    .line 74
    :try_start_0
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/n;->c_()Ljava/lang/String;

    move-result-object v1

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 82
    if-eqz v10, :cond_3

    .line 83
    :try_start_1
    const-string v0, "source_media_id"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 84
    const-string v1, "device_id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 85
    const-string v2, "media_type"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 87
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 88
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 89
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 90
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    int-to-long v4, v0

    invoke-virtual {v1, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    .line 91
    if-eqz v4, :cond_3

    .line 92
    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v0, v1, :cond_1

    const-string v0, "_data"

    :goto_0
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 94
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 95
    new-instance v1, Lcom/mfluent/asp/media/b;

    iget-object v7, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-wide v2, p2

    invoke-direct/range {v1 .. v8}, Lcom/mfluent/asp/media/b;-><init>(JLcom/mfluent/asp/datamodel/Device;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 100
    :goto_1
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->f:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0, v9}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    .line 101
    if-eqz v10, :cond_0

    .line 102
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 106
    :cond_0
    return-object v1

    .line 92
    :cond_1
    :try_start_2
    const-string v0, "full_uri"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    :goto_2
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->f:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v9}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    .line 101
    if-eqz v8, :cond_2

    .line 102
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 100
    :catchall_1
    move-exception v0

    move-object v8, v10

    goto :goto_2

    :cond_3
    move-object v1, v8

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J
    .locals 4

    .prologue
    .line 148
    const-string v0, "date_added"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 149
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 150
    const-string v1, "date_modified"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 151
    const-string v0, "date_modified"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 155
    :goto_0
    const-string v1, "date_added"

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 161
    :cond_0
    :goto_1
    const-string v0, "mime_type"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    const-string v0, "_display_name"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    const-string v1, "mime_type"

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/ap;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0

    .line 153
    :cond_2
    const-string v1, "date_modified"

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 156
    :cond_3
    const-string v0, "date_modified"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    const-string v0, "date_added"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 158
    const-string v1, "date_modified"

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1
.end method

.method public final a_(Lcom/mfluent/asp/datamodel/ao$a;J)Landroid/os/ParcelFileDescriptor;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 112
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->b()V

    .line 114
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/n;->b(Lcom/mfluent/asp/datamodel/ao$a;J)Lcom/mfluent/asp/media/b;

    move-result-object v1

    .line 116
    if-nez v1, :cond_0

    .line 117
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Record does not exist for Uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    :catchall_0
    move-exception v0

    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c()V

    throw v0

    .line 119
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Lcom/mfluent/asp/media/b;->b()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    .line 121
    sget-object v3, Lcom/mfluent/asp/datamodel/n$1;->a:[I

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 130
    :goto_0
    if-nez v0, :cond_1

    .line 137
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "No ImageMediaGetter has been implemented."

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :pswitch_0
    new-instance v0, Lcom/mfluent/asp/media/b/d;

    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/media/b/d;-><init>(Lcom/mfluent/asp/media/b;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    goto :goto_0

    .line 126
    :pswitch_1
    new-instance v0, Lcom/mfluent/asp/media/b/a;

    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/media/b/a;-><init>(Lcom/mfluent/asp/media/b;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    goto :goto_0

    .line 129
    :pswitch_2
    new-instance v0, Lcom/mfluent/asp/media/b/c;

    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/media/b/c;-><init>(Lcom/mfluent/asp/media/b;Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)V

    goto :goto_0

    .line 140
    :cond_1
    invoke-virtual {v0}, Lcom/mfluent/asp/media/b/e;->b()Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 142
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->c()V

    return-object v0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a_()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    sget-object v0, Lcom/mfluent/asp/datamodel/n;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 38
    new-array v2, v8, [Ljava/lang/String;

    const-string v0, "mime_type"

    aput-object v0, v2, v7

    .line 43
    :try_start_0
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 44
    if-eqz v1, :cond_3

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 45
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    move-object v2, v0

    .line 48
    :goto_0
    if-eqz v1, :cond_0

    .line 49
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 53
    :cond_0
    if-eqz v2, :cond_2

    .line 54
    new-array v0, v8, [Ljava/lang/String;

    aput-object v2, v0, v7

    .line 56
    :goto_1
    return-object v0

    .line 48
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v6, :cond_1

    .line 49
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    :cond_2
    move-object v0, v6

    .line 56
    goto :goto_1

    .line 48
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_2

    :cond_3
    move-object v2, v6

    goto :goto_0
.end method
