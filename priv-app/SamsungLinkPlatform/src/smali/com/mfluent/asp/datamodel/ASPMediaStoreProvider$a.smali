.class public final Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/database/sqlite/SQLiteTransactionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x14
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/mfluent/asp/datamodel/ao;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 369
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->b:Ljava/util/ArrayList;

    .line 366
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->c:Ljava/util/HashSet;

    .line 370
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->d:Landroid/database/sqlite/SQLiteDatabase;

    .line 371
    return-void
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/datamodel/ao;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 409
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 410
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-ne v2, p1, :cond_0

    .line 411
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 415
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 375
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 433
    return-void
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 419
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 420
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 421
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 422
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    .line 423
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 429
    :goto_1
    return-void

    .line 420
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 428
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 379
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 383
    return-void
.end method

.method public final onBegin()V
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 388
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 389
    return-void
.end method

.method public final onCommit()V
    .locals 5

    .prologue
    .line 393
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 394
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/mfluent/asp/datamodel/ao;

    iget-object v3, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    iget-object v4, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->d:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v3, v4, v0}, Lcom/mfluent/asp/datamodel/ao;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)V

    goto :goto_0

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 397
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-static {v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;)Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$c;->a(Landroid/net/Uri;)V

    goto :goto_1

    .line 399
    :cond_1
    return-void
.end method

.method public final onRollback()V
    .locals 3

    .prologue
    .line 403
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 404
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$a;->d:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto :goto_0

    .line 406
    :cond_0
    return-void
.end method
