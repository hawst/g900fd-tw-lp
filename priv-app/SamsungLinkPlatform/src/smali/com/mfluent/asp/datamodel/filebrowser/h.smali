.class public final Lcom/mfluent/asp/datamodel/filebrowser/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPFileProvider;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/mfluent/asp/datamodel/Device;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/h;->a:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/filebrowser/h;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 24
    return-void
.end method


# virtual methods
.method public final varargs deleteFiles(Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public final getApplicationContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/h;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final getCloudDevice()Lcom/mfluent/asp/common/datamodel/CloudDevice;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/h;->b:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method public final getCloudStorageFileBrowser(Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/common/datamodel/ASPFileSortType;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/g;

    invoke-direct {v0}, Lcom/mfluent/asp/datamodel/filebrowser/g;-><init>()V

    return-object v0
.end method

.method public final getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 38
    const-string v0, ""

    return-object v0
.end method
