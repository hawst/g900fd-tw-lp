.class public Lcom/mfluent/asp/datamodel/Device;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/CloudDevice;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lorg/slf4j/Logger;

.field private static final d:[Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

.field private static final e:[Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field private static final f:[Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;


# instance fields
.field private A:Z

.field private final B:Ljava/lang/Object;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:I

.field private G:Z

.field private H:Z

.field private I:I

.field private J:Ljava/lang/String;

.field private K:I

.field private L:B

.field private M:Z

.field private N:Z

.field private O:I

.field private P:Z

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private X:Ljava/lang/String;

.field private Y:I

.field private final Z:Ljava/util/concurrent/locks/ReadWriteLock;

.field private aa:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

.field private final ab:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ac:Z

.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:Z

.field private final ah:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;",
            ">;"
        }
    .end annotation
.end field

.field private ai:Ljava/lang/String;

.field private aj:Ljava/lang/String;

.field private ak:Z

.field private al:J

.field private am:J

.field private final an:Landroid/content/Context;

.field private ao:Z

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

.field private p:Ljava/lang/String;

.field private q:J

.field private r:J

.field private s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

.field private t:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 69
    const-class v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    .line 71
    new-array v0, v7, [Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEARABLE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mfluent/asp/datamodel/Device;->d:[Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 77
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_TAB:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE_WINDOWS:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->CAMERA:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/Device;->e:[Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 93
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_LTE:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_3G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_2G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v1, v0, v7

    sput-object v0, Lcom/mfluent/asp/datamodel/Device;->f:[Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 219
    const-class v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/Device;->a:Ljava/util/Set;

    .line 220
    const-class v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/datamodel/Device;->b:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->o:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 141
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 143
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->t:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 155
    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/Device;->z:Z

    .line 157
    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/Device;->A:Z

    .line 159
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->B:Ljava/lang/Object;

    .line 173
    iput v3, p0, Lcom/mfluent/asp/datamodel/Device;->I:I

    .line 177
    iput v3, p0, Lcom/mfluent/asp/datamodel/Device;->K:I

    .line 179
    iput-byte v3, p0, Lcom/mfluent/asp/datamodel/Device;->L:B

    .line 182
    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/Device;->M:Z

    .line 184
    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/Device;->N:Z

    .line 186
    const/4 v0, 0x5

    iput v0, p0, Lcom/mfluent/asp/datamodel/Device;->O:I

    .line 190
    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/Device;->Q:Z

    .line 192
    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/Device;->R:Z

    .line 199
    iput-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->U:Ljava/lang/String;

    .line 200
    iput-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->V:Ljava/lang/String;

    .line 201
    iput-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->W:Ljava/lang/String;

    .line 202
    iput-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->X:Ljava/lang/String;

    .line 204
    iput v1, p0, Lcom/mfluent/asp/datamodel/Device;->Y:I

    .line 206
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->Z:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 222
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->ab:Ljava/util/HashMap;

    .line 230
    const-class v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->ah:Ljava/util/Set;

    .line 239
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/datamodel/Device;->am:J

    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 246
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    .line 247
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 250
    invoke-direct {p0, p1}, Lcom/mfluent/asp/datamodel/Device;-><init>(Landroid/content/Context;)V

    .line 251
    const-string v0, "_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    const-string v0, "alias_name"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    const-string v0, "peer_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->c(Ljava/lang/String;)V

    const-string v0, "eimei"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->j:Ljava/lang/String;

    invoke-static {v0, v2}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->j:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->i:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    :cond_0
    invoke-static {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->getDeviceTransportType(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->b(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V

    invoke-static {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->getDevicePhysicalType(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->b(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)V

    const-string v0, "model_name"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->f(Ljava/lang/String;)V

    const-string v0, "model_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->g(Ljava/lang/String;)V

    const-string v0, "web_storage_type"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->i(Ljava/lang/String;)V

    const-string v0, "web_storage_account_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->v:Ljava/lang/String;

    invoke-static {v2, v0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->v:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    :cond_1
    const-string v0, "web_storage_user_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->setWebStorageUserId(Ljava/lang/String;)V

    const-string v0, "web_storage_pw_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->k(Ljava/lang/String;)V

    const-string v0, "web_storage_email_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->y:Ljava/lang/String;

    invoke-static {v2, v0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->y:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    :cond_2
    const-string v0, "web_storage_is_signed_in_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->setWebStorageSignedIn(Z)V

    const-string v0, "web_storage_total_capacity_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/mfluent/asp/datamodel/Device;->setCapacityInBytes(J)V

    const-string v0, "web_storage_used_capacity_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/mfluent/asp/datamodel/Device;->setUsedCapacityInBytes(J)V

    sget-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v2, "sync_key_images"

    invoke-static {p2, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    sget-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v2, "sync_key_audio"

    invoke-static {p2, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    sget-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v2, "sync_key_videos"

    invoke-static {p2, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    sget-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->d:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    const-string v2, "sync_key_documents"

    invoke-static {p2, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    const-string v0, "is_sync_server"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->c(Z)V

    const-string v0, "supports_push_notification"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->f(Z)V

    const-string v0, "is_hls_server"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->d(Z)V

    const-string v0, "is_ts_server"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->e(Z)V

    const-string v0, "server_sort_key"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->c(I)V

    const-string v0, "device_unique_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->l(Ljava/lang/String;)V

    const-string v0, "udn_wifi"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->ai:Ljava/lang/String;

    const-string v0, "udn_wifi_direct"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->aj:Ljava/lang/String;

    const-string v0, "is_remote_wakeup"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->o(Z)V

    invoke-static {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->getNetworkMode(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    const-string v0, "supports_auto_archive"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->i(Z)V

    const-string v0, "registration_date"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/mfluent/asp/datamodel/Device;->b(J)V

    const-string v0, "sync_server_supported_media_types"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-class v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_3

    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->valueOf(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception parsing synced media types for device: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n Original Exception:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Ljava/util/Set;)V

    const-string v0, "supports_threebox_transfer"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->af:Z

    const-string v0, "is_remote_wakeup_support"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->p(Z)V

    const-string v0, "smallest_drive_capacity"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lcom/mfluent/asp/datamodel/Device;->am:J

    const-string v0, "host_peer_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->m(Ljava/lang/String;)V

    const-string v0, "mac_bt"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->n(Ljava/lang/String;)V

    const-string v0, "mac_ble"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->o(Ljava/lang/String;)V

    const-string v0, "mac_wifi"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->p(Ljava/lang/String;)V

    const-string v0, "mac_wifi_direct"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->q(Ljava/lang/String;)V

    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 252
    return-void
.end method

.method private Y()Landroid/content/ContentValues;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1529
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1530
    const-string v0, "alias_name"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->l:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1531
    const-string v0, "peer_id"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->h:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    const-string v0, "eimei"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->j:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->i:Ljava/lang/String;

    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->i:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/mfluent/asp/datamodel/Device;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->j:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->j:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1534
    const-string v0, "transport_type"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1535
    const-string v0, "physical_type"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->t:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536
    const-string v5, "device_priority"

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v0, v2, :cond_b

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v0, v2, :cond_b

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEARABLE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v0, v2, :cond_b

    sget-object v0, Lcom/mfluent/asp/datamodel/Device;->f:[Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->o:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-static {v0, v2}, Lorg/apache/commons/lang3/ArrayUtils;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_1

    sget-object v0, Lcom/mfluent/asp/datamodel/Device;->f:[Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    array-length v0, v0

    :cond_1
    shl-int/lit8 v0, v0, 0x18

    or-int/lit8 v0, v0, 0x0

    :goto_0
    sget-object v2, Lcom/mfluent/asp/datamodel/Device;->d:[Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    iget-object v6, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-static {v2, v6}, Lorg/apache/commons/lang3/ArrayUtils;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_2

    sget-object v2, Lcom/mfluent/asp/datamodel/Device;->d:[Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    array-length v2, v2

    :cond_2
    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v2, v0

    sget-object v0, Lcom/mfluent/asp/datamodel/Device;->e:[Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    iget-object v6, p0, Lcom/mfluent/asp/datamodel/Device;->t:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-static {v0, v6}, Lorg/apache/commons/lang3/ArrayUtils;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_3

    sget-object v0, Lcom/mfluent/asp/datamodel/Device;->e:[Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    array-length v0, v0

    :cond_3
    or-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1537
    const-string v0, "model_name"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->m:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1538
    const-string v0, "model_id"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->n:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1539
    const-string v0, "web_storage_type"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->u:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1540
    const-string v0, "web_storage_account_id"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->v:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1541
    const-string v0, "web_storage_user_id"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getWebStorageUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1542
    const-string v0, "web_storage_pw_id"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getWebStoragePw()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1543
    const-string v0, "web_storage_email_id"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->y:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1544
    const-string v2, "web_storage_is_signed_in_id"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->isWebStorageSignedIn()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1545
    const-string v0, "web_storage_total_capacity_id"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getCapacityInBytes()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1546
    const-string v0, "web_storage_used_capacity_id"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getUsedCapacityInBytes()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1547
    const-string v0, "sync_key_images"

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1548
    const-string v0, "sync_key_audio"

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1549
    const-string v0, "sync_key_videos"

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1550
    const-string v0, "sync_key_documents"

    sget-object v2, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->d:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1551
    const-string v0, "is_sync_server"

    iget-boolean v2, p0, Lcom/mfluent/asp/datamodel/Device;->D:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1552
    const-string v0, "supports_push_notification"

    iget-boolean v2, p0, Lcom/mfluent/asp/datamodel/Device;->ac:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1553
    const-string v0, "is_hls_server"

    iget-boolean v2, p0, Lcom/mfluent/asp/datamodel/Device;->G:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1554
    const-string v0, "is_ts_server"

    iget-boolean v2, p0, Lcom/mfluent/asp/datamodel/Device;->H:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1555
    const-string v0, "server_sort_key"

    iget v2, p0, Lcom/mfluent/asp/datamodel/Device;->I:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1556
    const-string v0, "device_unique_id"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->J:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1557
    const-string v0, "sync_server_supported_media_types"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->ah:Ljava/util/Set;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/datamodel/t;->a(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1558
    const-string v2, "is_remote_wakeup"

    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->Q:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1559
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->o:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->toContentValues(Landroid/content/ContentValues;)V

    .line 1560
    const-string v2, "is_on_local_network"

    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->E:Z

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1561
    const-string v0, "local_ip_address"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->p:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1562
    const-string v2, "is_syncing"

    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ad:Z

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1563
    const-string v2, "supports_auto_archive"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->C()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1564
    const-string v0, "registration_date"

    iget-wide v6, p0, Lcom/mfluent/asp/datamodel/Device;->al:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1565
    const-string v2, "supports_threebox_transfer"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->A()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1566
    const-string v0, "is_remote_wakeup_support"

    iget-boolean v2, p0, Lcom/mfluent/asp/datamodel/Device;->R:Z

    if-nez v2, :cond_a

    :goto_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1567
    const-string v0, "smallest_drive_capacity"

    iget-wide v2, p0, Lcom/mfluent/asp/datamodel/Device;->am:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1568
    const-string v0, "host_peer_id"

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->T:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1570
    const-string v0, "mac_bt"

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->U:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1571
    const-string v0, "mac_ble"

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->V:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1572
    const-string v0, "mac_wifi"

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->W:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1573
    const-string v0, "mac_wifi_direct"

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->X:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1575
    const-string v0, "network_mode_sort"

    iget v1, p0, Lcom/mfluent/asp/datamodel/Device;->Y:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1576
    return-object v4

    :cond_4
    move v0, v3

    .line 1544
    goto/16 :goto_1

    :cond_5
    move v0, v3

    .line 1558
    goto/16 :goto_2

    :cond_6
    move v0, v3

    .line 1560
    goto/16 :goto_3

    :cond_7
    move v0, v3

    .line 1562
    goto/16 :goto_4

    :cond_8
    move v0, v3

    .line 1563
    goto :goto_5

    :cond_9
    move v0, v3

    .line 1565
    goto :goto_6

    :cond_a
    move v1, v3

    .line 1566
    goto :goto_7

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1042
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 1043
    if-eqz v0, :cond_0

    .line 1044
    invoke-virtual {v0, p0}, Lcom/mfluent/asp/datamodel/t;->a(Landroid/content/Intent;)V

    .line 1046
    :cond_0
    return-void
.end method

.method public static j()Z
    .locals 1

    .prologue
    .line 591
    const/4 v0, 0x1

    return v0
.end method

.method private r(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 356
    invoke-static {p1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 367
    :goto_0
    return-object v0

    .line 362
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/pcw/service/account/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 363
    :catch_0
    move-exception v1

    .line 364
    sget-object v2, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v3, "Failed to decrypt text."

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private s(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 371
    invoke-static {p1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 382
    :goto_0
    return-object v0

    .line 377
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/sec/pcw/service/account/a;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 378
    :catch_0
    move-exception v1

    .line 379
    sget-object v2, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v3, "Failed to encrypt text: {}"

    invoke-interface {v2, v3, p1, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final A()Z
    .locals 1

    .prologue
    .line 1193
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1194
    const/4 v0, 0x1

    .line 1197
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->af:Z

    goto :goto_0
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 1205
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1206
    const/4 v0, 0x1

    .line 1208
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ag:Z

    goto :goto_0
.end method

.method public final C()Z
    .locals 1

    .prologue
    .line 1216
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1217
    const/4 v0, 0x0

    .line 1219
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ak:Z

    goto :goto_0
.end method

.method public final D()Z
    .locals 1

    .prologue
    .line 1232
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->E:Z

    .line 1234
    return v0
.end method

.method public final E()Z
    .locals 1

    .prologue
    .line 1253
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->M:Z

    return v0
.end method

.method public final F()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1279
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final G()Z
    .locals 1

    .prologue
    .line 1292
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->N:Z

    return v0
.end method

.method public final H()I
    .locals 1

    .prologue
    .line 1300
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->O:I

    return v0
.end method

.method public final I()V
    .locals 1

    .prologue
    .line 1308
    const/4 v0, 0x5

    iput v0, p0, Lcom/mfluent/asp/datamodel/Device;->O:I

    .line 1309
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->N:Z

    .line 1310
    return-void
.end method

.method public final J()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1396
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->ah:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final K()I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1400
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1401
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1402
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 1403
    if-eqz v0, :cond_1

    .line 1404
    const-string v1, "level"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1405
    const-string v2, "scale"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1406
    if-ltz v1, :cond_0

    if-lez v0, :cond_0

    .line 1407
    mul-int/lit8 v1, v1, 0x64

    div-int v0, v1, v0

    .line 1415
    :goto_0
    return v0

    .line 1409
    :cond_0
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->K:I

    goto :goto_0

    .line 1412
    :cond_1
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->K:I

    goto :goto_0

    .line 1415
    :cond_2
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->K:I

    goto :goto_0
.end method

.method public final L()Z
    .locals 1

    .prologue
    .line 1432
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->S:Z

    return v0
.end method

.method public final M()Ljava/util/concurrent/locks/Lock;
    .locals 1

    .prologue
    .line 1436
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->Z:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    return-object v0
.end method

.method public final N()J
    .locals 2

    .prologue
    .line 1448
    iget-wide v0, p0, Lcom/mfluent/asp/datamodel/Device;->al:J

    return-wide v0
.end method

.method public final O()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 1461
    iget-boolean v1, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    if-nez v1, :cond_1

    .line 1489
    :cond_0
    :goto_0
    return v0

    .line 1465
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1469
    iget-boolean v1, p0, Lcom/mfluent/asp/datamodel/Device;->S:Z

    if-eqz v1, :cond_2

    .line 1470
    sget-object v1, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v2, "Someone tried to update a deleted device"

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1474
    :cond_2
    iget v1, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    if-nez v1, :cond_5

    .line 1475
    sget-object v1, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v2, "Inserting device {}"

    invoke-interface {v1, v2, p0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1476
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/Device;->Y()Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 1477
    if-eqz v1, :cond_4

    .line 1478
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    .line 1479
    iget v1, p0, Lcom/mfluent/asp/datamodel/Device;->I:I

    if-gez v1, :cond_3

    .line 1480
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/datamodel/Device;->c(I)V

    .line 1482
    :cond_3
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 1488
    :cond_4
    :goto_1
    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 1489
    const/4 v0, 0x1

    goto :goto_0

    .line 1485
    :cond_5
    sget-object v1, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v2, "Updating device {}"

    invoke-interface {v1, v2, p0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1486
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->getDeviceEntryUri(J)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/Device;->Y()Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final P()V
    .locals 4

    .prologue
    .line 1493
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1494
    sget-object v0, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v1, "Ignoring remove request for local device"

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1526
    :goto_0
    return-void

    .line 1498
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1499
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot remove the all devices device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1502
    :cond_1
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->S:Z

    if-eqz v0, :cond_2

    .line 1503
    sget-object v0, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v1, "Trying to delete a device that has already been deleted."

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1507
    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1508
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->setWebStorageSignedIn(Z)V

    .line 1511
    :cond_3
    sget-object v0, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v1, "Deleting {}"

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1513
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 1515
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/media/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/media/d;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/media/d;->a(I)V

    .line 1517
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->Z:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1519
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->S:Z

    .line 1520
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->getDeviceEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1522
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->Z:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1525
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/mfluent/asp/datamodel/t;->b(Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_0

    .line 1522
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->Z:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1649
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->T:Ljava/lang/String;

    return-object v0
.end method

.method public final R()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1677
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->U:Ljava/lang/String;

    return-object v0
.end method

.method public final S()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1689
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->V:Ljava/lang/String;

    return-object v0
.end method

.method public final T()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1701
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->W:Ljava/lang/String;

    return-object v0
.end method

.method public final U()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1713
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->X:Ljava/lang/String;

    return-object v0
.end method

.method public final V()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1717
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->B:Ljava/lang/Object;

    monitor-enter v1

    .line 1718
    :try_start_0
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->A:Z

    if-eq v2, v0, :cond_0

    .line 1719
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->A:Z

    .line 1720
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->B:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1722
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final W()Z
    .locals 6

    .prologue
    .line 1727
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->B:Ljava/lang/Object;

    monitor-enter v1

    .line 1728
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x7530

    add-long/2addr v2, v4

    .line 1729
    :goto_0
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->A:Z

    if-nez v0, :cond_0

    .line 1730
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    .line 1731
    cmp-long v0, v4, v2

    if-gtz v0, :cond_0

    .line 1732
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->B:Ljava/lang/Object;

    sub-long v4, v2, v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1737
    :cond_0
    :try_start_2
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->A:Z

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return v0

    .line 1742
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final X()Z
    .locals 6

    .prologue
    .line 1748
    const/4 v0, 0x0

    .line 1751
    :try_start_0
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->W:Ljava/lang/String;

    .line 1752
    const-string v2, "INFO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "checkMACInfo = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1753
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1754
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/pcw/util/c;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1755
    if-eqz v1, :cond_1

    .line 1756
    invoke-static {v1}, Lcom/sec/pcw/util/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1757
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/datamodel/Device;->p(Ljava/lang/String;)V

    .line 1758
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->q(Ljava/lang/String;)V

    .line 1760
    :cond_1
    invoke-static {}, Lcom/sec/pcw/util/c;->f()Ljava/lang/String;

    move-result-object v2

    .line 1761
    if-eqz v2, :cond_2

    .line 1762
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/datamodel/Device;->n(Ljava/lang/String;)V

    .line 1764
    :cond_2
    const-string v3, "INFO"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "strWifiMac="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ",strWifiDirectMac="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",strBTMac="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1769
    :cond_3
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1766
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->l:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->l:Ljava/lang/String;

    .line 417
    :goto_0
    return-object v0

    .line 410
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 411
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->u:Ljava/lang/String;

    goto :goto_0

    .line 412
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->m:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 413
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->m:Ljava/lang/String;

    goto :goto_0

    .line 414
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->n:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 415
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->n:Ljava/lang/String;

    goto :goto_0

    .line 417
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    const v1, 0x7f0a01b0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1088
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->ab:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(B)V
    .locals 0

    .prologue
    .line 1428
    iput-byte p1, p0, Lcom/mfluent/asp/datamodel/Device;->L:B

    .line 1429
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 1075
    sget-object v0, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v1, "::broadcastDeviceRefresh: device: {}, where: {}"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, p0, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1077
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_REFRESH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1078
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->getDeviceEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->CONTENT_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1079
    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1080
    if-lez p1, :cond_0

    .line 1081
    const-string v1, "REFRESH_FROM_KEY"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1084
    :cond_0
    invoke-static {v0}, Lcom/mfluent/asp/datamodel/Device;->a(Landroid/content/Intent;)V

    .line 1085
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 1440
    iput-wide p1, p0, Lcom/mfluent/asp/datamodel/Device;->am:J

    .line 1441
    return-void
.end method

.method public final a(Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;)V
    .locals 0

    .prologue
    .line 1164
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->aa:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    .line 1165
    return-void
.end method

.method public final a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1092
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->ab:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1094
    invoke-static {v0, p2}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1095
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 1097
    :cond_0
    return-void
.end method

.method public final a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 630
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->o:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-eq p1, v0, :cond_4

    .line 631
    sget-object v0, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v1, "::setDeviceNetworkMode: new: {} old: {} for device: {}"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    iget-object v3, p0, Lcom/mfluent/asp/datamodel/Device;->o:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    aput-object v3, v2, v5

    aput-object p0, v2, v6

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 633
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->o:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 634
    iput-boolean v5, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 635
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne p1, v0, :cond_1

    .line 636
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/a/b;->b()V

    .line 639
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne p1, v0, :cond_2

    .line 640
    iput v4, p0, Lcom/mfluent/asp/datamodel/Device;->F:I

    .line 641
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->h(Ljava/lang/String;)V

    .line 642
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/datamodel/Device;->j(Z)V

    .line 644
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->s()V

    .line 646
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-eq p1, v0, :cond_3

    .line 647
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_NOW_ONLINE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 648
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->getDeviceEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->CONTENT_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 649
    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 650
    invoke-static {v0}, Lcom/mfluent/asp/datamodel/Device;->a(Landroid/content/Intent;)V

    .line 653
    :cond_3
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne p1, v0, :cond_5

    .line 654
    iput v4, p0, Lcom/mfluent/asp/datamodel/Device;->Y:I

    .line 663
    :cond_4
    :goto_0
    return-void

    .line 655
    :cond_5
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne p1, v0, :cond_6

    .line 656
    iput v4, p0, Lcom/mfluent/asp/datamodel/Device;->Y:I

    goto :goto_0

    .line 657
    :cond_6
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->BLUETOOTH:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne p1, v0, :cond_7

    .line 658
    iput v5, p0, Lcom/mfluent/asp/datamodel/Device;->Y:I

    goto :goto_0

    .line 660
    :cond_7
    iput v6, p0, Lcom/mfluent/asp/datamodel/Device;->Y:I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->ai:Ljava/lang/String;

    .line 269
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1386
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->ah:Ljava/util/Set;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1393
    :goto_0
    return-void

    .line 1390
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->ah:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1391
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->ah:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1392
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 612
    const-string v0, "ATT"

    invoke-static {}, Lcom/sec/pcw/hybrid/update/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/hybrid/update/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->P:Z

    .line 617
    :goto_0
    return-void

    .line 615
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->P:Z

    goto :goto_0
.end method

.method public final a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z
    .locals 1

    .prologue
    .line 799
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->t:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 1117
    iput p1, p0, Lcom/mfluent/asp/datamodel/Device;->F:I

    .line 1118
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 1452
    iget-wide v0, p0, Lcom/mfluent/asp/datamodel/Device;->al:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 1458
    :goto_0
    return-void

    .line 1456
    :cond_0
    iput-wide p1, p0, Lcom/mfluent/asp/datamodel/Device;->al:J

    .line 1457
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final b(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)V
    .locals 1

    .prologue
    .line 803
    if-nez p1, :cond_0

    .line 804
    sget-object p1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 807
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->t:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 813
    :goto_0
    return-void

    .line 811
    :cond_1
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->t:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    .line 812
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final b(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 526
    if-nez p1, :cond_0

    .line 527
    sget-object p1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/ObjectUtils;->notEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 531
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNREGISTERED:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v0, v2, :cond_5

    move v0, v1

    .line 532
    :goto_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 533
    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 535
    if-nez v0, :cond_1

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNREGISTERED:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne p1, v1, :cond_2

    .line 536
    :cond_1
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    .line 537
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.mfluent.asp.DataModel.DEVICE_LIST_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/t;->a(Landroid/content/Intent;)V

    .line 539
    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {p0, v1, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    .line 540
    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {p0, v1, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    .line 541
    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {p0, v1, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    .line 542
    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->d:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {p0, v1, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    .line 544
    :cond_2
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq p1, v1, :cond_3

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNREGISTERED:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne p1, v1, :cond_4

    .line 545
    :cond_3
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    .line 546
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    .line 547
    new-instance v3, Lcom/mfluent/asp/datamodel/Device$1;

    invoke-direct {v3, p0, v2, v1}, Lcom/mfluent/asp/datamodel/Device$1;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/content/Context;I)V

    .line 555
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v1, v2, :cond_6

    .line 556
    invoke-static {}, Lcom/mfluent/asp/util/d;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 561
    :goto_1
    if-nez v0, :cond_4

    .line 563
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->deleteAllMetaData(Landroid/content/ContentResolver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    :cond_4
    :goto_2
    return-void

    .line 531
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 558
    :cond_6
    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 564
    :catch_0
    move-exception v0

    .line 565
    sget-object v1, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v2, "Failed to deleteAllMetaData."

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->aj:Ljava/lang/String;

    .line 286
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    const-string v1, "slpf_pref_20"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1003
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1004
    const-string v1, "useInternal"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1005
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1006
    return-void
.end method

.method public buildDeviceIntentFilterForAction(Ljava/lang/String;)Landroid/content/IntentFilter;
    .locals 4

    .prologue
    .line 1054
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0, p1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1055
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->getDeviceEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 1056
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1057
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->getPort()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/IntentFilter;->addDataAuthority(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/IntentFilter;->addDataPath(Ljava/lang/String;I)V

    .line 1060
    :try_start_0
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->CONTENT_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1065
    return-object v0

    .line 1061
    :catch_0
    move-exception v0

    .line 1062
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trouble creating intentFilter"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 1151
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->I:I

    if-ne v0, p1, :cond_0

    .line 1157
    :goto_0
    return-void

    .line 1155
    :cond_0
    iput p1, p0, Lcom/mfluent/asp/datamodel/Device;->I:I

    .line 1156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->h:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    :goto_0
    return-void

    .line 314
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 315
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->h:Ljava/lang/String;

    .line 316
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/t$a;->a(Landroid/content/Context;)Lcom/mfluent/asp/datamodel/t$a;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/t$a;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 1104
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->D:Z

    if-ne v0, p1, :cond_0

    .line 1110
    :goto_0
    return-void

    .line 1108
    :cond_0
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->D:Z

    .line 1109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 578
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v1, v2, :cond_1

    .line 582
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->o:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 1304
    iput p1, p0, Lcom/mfluent/asp/datamodel/Device;->O:I

    .line 1305
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->i:Ljava/lang/String;

    invoke-static {p1, v0}, Lorg/apache/commons/lang3/ObjectUtils;->notEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->j:Ljava/lang/String;

    .line 336
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->i:Ljava/lang/String;

    .line 338
    :cond_0
    return-void
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 1125
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->G:Z

    if-ne v0, p1, :cond_0

    .line 1131
    :goto_0
    return-void

    .line 1129
    :cond_0
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->G:Z

    .line 1130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 500
    const/4 v0, 0x0

    return v0
.end method

.method public deleteAllMetaData(Landroid/content/ContentResolver;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1169
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1170
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1171
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1172
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1173
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Albums;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1174
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Artists;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1175
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1176
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1177
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 1420
    iput p1, p0, Lcom/mfluent/asp/datamodel/Device;->K:I

    .line 1421
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->k:Ljava/lang/String;

    .line 398
    return-void
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 1138
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->H:Z

    if-ne v0, p1, :cond_0

    .line 1144
    :goto_0
    return-void

    .line 1142
    :cond_0
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->H:Z

    .line 1143
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    invoke-static {}, Lcom/sec/pcw/util/c;->a()Ljava/lang/String;

    move-result-object v0

    .line 326
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->m:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    :goto_0
    return-void

    .line 474
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->m:Ljava/lang/String;

    .line 475
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 1184
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ac:Z

    if-ne v0, p1, :cond_0

    .line 1190
    :goto_0
    return-void

    .line 1188
    :cond_0
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->ac:Z

    .line 1189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->n:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    :goto_0
    return-void

    .line 487
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->n:Ljava/lang/String;

    .line 488
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final g(Z)V
    .locals 0

    .prologue
    .line 1201
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->af:Z

    .line 1202
    return-void
.end method

.method public getCapacityInBytes()J
    .locals 2

    .prologue
    .line 691
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 692
    invoke-static {}, Lcom/sec/pcw/service/d/a;->f()J

    move-result-wide v0

    .line 694
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/mfluent/asp/datamodel/Device;->q:J

    goto :goto_0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->g:I

    return v0
.end method

.method public getUsedCapacityInBytes()J
    .locals 2

    .prologue
    .line 724
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 725
    invoke-static {}, Lcom/sec/pcw/service/d/a;->e()J

    move-result-wide v0

    .line 727
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/mfluent/asp/datamodel/Device;->r:J

    goto :goto_0
.end method

.method public getWebStoragePw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 903
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->x:Ljava/lang/String;

    return-object v0
.end method

.method public getWebStorageUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->p:Ljava/lang/String;

    invoke-static {p1, v0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 678
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->p:Ljava/lang/String;

    .line 679
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 680
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->s()V

    .line 682
    :cond_0
    return-void
.end method

.method public final h(Z)V
    .locals 0

    .prologue
    .line 1212
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->ag:Z

    .line 1213
    return-void
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 827
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->u:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 833
    :goto_0
    return-void

    .line 831
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->u:Ljava/lang/String;

    .line 832
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final i(Z)V
    .locals 1

    .prologue
    .line 1223
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ak:Z

    if-ne v0, p1, :cond_0

    .line 1229
    :goto_0
    return-void

    .line 1227
    :cond_0
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->ak:Z

    .line 1228
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public isWebStorageSignedIn()Z
    .locals 1

    .prologue
    .line 944
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->z:Z

    return v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 867
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->l:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 874
    :goto_0
    return-void

    .line 871
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->l:Ljava/lang/String;

    .line 872
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 873
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->s()V

    goto :goto_0
.end method

.method public final j(Z)V
    .locals 1

    .prologue
    .line 1238
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->E:Z

    if-eq p1, v0, :cond_0

    .line 1239
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->E:Z

    .line 1240
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 1241
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->s()V

    .line 1243
    :cond_0
    return-void
.end method

.method public final k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;
    .locals 1

    .prologue
    .line 600
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->o:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    return-object v0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->x:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917
    :goto_0
    return-void

    .line 915
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->x:Ljava/lang/String;

    .line 916
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final k(Z)V
    .locals 0

    .prologue
    .line 1249
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->M:Z

    .line 1250
    return-void
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final l(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1283
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->J:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1289
    :goto_0
    return-void

    .line 1287
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->J:Ljava/lang/String;

    .line 1288
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final l(Z)V
    .locals 1

    .prologue
    .line 1261
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ad:Z

    if-ne v0, p1, :cond_0

    .line 1267
    :goto_0
    return-void

    .line 1265
    :cond_0
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->ad:Z

    .line 1266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final m()J
    .locals 4

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getCapacityInBytes()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getUsedCapacityInBytes()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final m(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1659
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->T:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1665
    :goto_0
    return-void

    .line 1663
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 1664
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->T:Ljava/lang/String;

    goto :goto_0
.end method

.method public final m(Z)V
    .locals 0

    .prologue
    .line 1275
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->ae:Z

    .line 1276
    return-void
.end method

.method public final n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;
    .locals 1

    .prologue
    .line 792
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->t:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    return-object v0
.end method

.method public final n(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1669
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->U:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1674
    :cond_0
    :goto_0
    return-void

    .line 1672
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 1673
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->U:Ljava/lang/String;

    goto :goto_0
.end method

.method public final n(Z)V
    .locals 0

    .prologue
    .line 1296
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->N:Z

    .line 1297
    return-void
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 819
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final o(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1681
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->V:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1686
    :cond_0
    :goto_0
    return-void

    .line 1684
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 1685
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->V:Ljava/lang/String;

    goto :goto_0
.end method

.method public final o(Z)V
    .locals 1

    .prologue
    .line 1347
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->Q:Z

    if-ne v0, p1, :cond_0

    .line 1353
    :goto_0
    return-void

    .line 1351
    :cond_0
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->Q:Z

    .line 1352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 859
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final p(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1693
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->W:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1698
    :cond_0
    :goto_0
    return-void

    .line 1696
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 1697
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->W:Ljava/lang/String;

    goto :goto_0
.end method

.method public final p(Z)V
    .locals 1

    .prologue
    .line 1360
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->R:Z

    if-ne v0, p1, :cond_0

    .line 1366
    :goto_0
    return-void

    .line 1364
    :cond_0
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->R:Z

    .line 1365
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final q(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1705
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->X:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1710
    :cond_0
    :goto_0
    return-void

    .line 1708
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 1709
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->X:Ljava/lang/String;

    goto :goto_0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 974
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 975
    invoke-static {}, Lcom/sec/pcw/service/d/a;->g()Z

    move-result v0

    .line 978
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->C:Z

    goto :goto_0
.end method

.method public final r()Z
    .locals 3

    .prologue
    .line 993
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->an:Landroid/content/Context;

    const-string v1, "slpf_pref_20"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 994
    const-string v1, "useInternal"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final s()V
    .locals 4

    .prologue
    .line 1031
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1032
    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1033
    invoke-static {v0}, Lcom/mfluent/asp/datamodel/Device;->a(Landroid/content/Intent;)V

    .line 1035
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE_WITH_DATA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1036
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->getDeviceEntryUri(J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->CONTENT_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1037
    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1038
    invoke-static {v0}, Lcom/mfluent/asp/datamodel/Device;->a(Landroid/content/Intent;)V

    .line 1039
    return-void
.end method

.method public setCapacityInBytes(J)V
    .locals 3

    .prologue
    .line 706
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 715
    :cond_0
    :goto_0
    return-void

    .line 710
    :cond_1
    iget-wide v0, p0, Lcom/mfluent/asp/datamodel/Device;->q:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 711
    iput-wide p1, p0, Lcom/mfluent/asp/datamodel/Device;->q:J

    .line 712
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 713
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->s()V

    goto :goto_0
.end method

.method public setUsedCapacityInBytes(J)V
    .locals 3

    .prologue
    .line 743
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 752
    :cond_0
    :goto_0
    return-void

    .line 747
    :cond_1
    iget-wide v0, p0, Lcom/mfluent/asp/datamodel/Device;->r:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 748
    iput-wide p1, p0, Lcom/mfluent/asp/datamodel/Device;->r:J

    .line 749
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 750
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->s()V

    goto :goto_0
.end method

.method public setWebStorageSignedIn(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 953
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->z:Z

    if-eq v0, p1, :cond_1

    .line 954
    sget-object v0, Lcom/mfluent/asp/datamodel/Device;->c:Lorg/slf4j/Logger;

    const-string v1, "::setWebStorageSignedIn:Changing login state of: {} to login={}"

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/Device;->u:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 955
    if-ne p1, v4, :cond_0

    .line 956
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->M:Z

    .line 959
    :cond_0
    iput-boolean p1, p0, Lcom/mfluent/asp/datamodel/Device;->z:Z

    .line 960
    iput-boolean v4, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    .line 962
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->s()V

    .line 964
    const-class v0, Lcom/mfluent/asp/sync/h;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 965
    const-class v0, Lcom/mfluent/asp/sync/h;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/h;

    invoke-virtual {v0, p0, p1}, Lcom/mfluent/asp/sync/h;->a(Lcom/mfluent/asp/datamodel/Device;Z)V

    .line 968
    :cond_1
    return-void
.end method

.method public setWebStorageUserId(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 890
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->w:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 896
    :goto_0
    return-void

    .line 894
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/Device;->w:Ljava/lang/String;

    .line 895
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ao:Z

    goto :goto_0
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 1049
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/Device;->a(I)V

    .line 1050
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1014
    new-instance v0, Lorg/apache/commons/lang3/builder/ToStringBuilder;

    invoke-direct {v0, p0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(I)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v1

    .line 1015
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->s:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v0, v2, :cond_1

    .line 1016
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->u:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    .line 1017
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ad:Z

    if-eqz v0, :cond_0

    .line 1018
    const-string v0, "+syncing"

    invoke-virtual {v1, v0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    .line 1026
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1021
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->t:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v1, v0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    .line 1022
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ONLINE"

    :goto_1
    invoke-virtual {v1, v0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    .line 1023
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->o:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1, v0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    goto :goto_0

    .line 1022
    :cond_2
    const-string v0, "OFFLINE"

    goto :goto_1
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 1100
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->D:Z

    return v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 1121
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->G:Z

    return v0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 1134
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->H:Z

    return v0
.end method

.method public final x()I
    .locals 1

    .prologue
    .line 1147
    iget v0, p0, Lcom/mfluent/asp/datamodel/Device;->I:I

    return v0
.end method

.method public final y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;
    .locals 1

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/Device;->aa:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    return-object v0
.end method

.method public final z()Z
    .locals 1

    .prologue
    .line 1180
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/Device;->ac:Z

    return v0
.end method
