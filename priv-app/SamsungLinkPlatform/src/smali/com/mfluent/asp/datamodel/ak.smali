.class public final Lcom/mfluent/asp/datamodel/ak;
.super Lcom/mfluent/asp/datamodel/at;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ak$a;
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 10
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "thumb_data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "thumb_width"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "thumb_height"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "source_media_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "full_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "thumbnail_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "device_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "transport_type"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "height"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ak;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/at;-><init>()V

    .line 38
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ak;-><init>()V

    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/ak;
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcom/mfluent/asp/datamodel/ak$a;->a()Lcom/mfluent/asp/datamodel/ak;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/mfluent/asp/datamodel/ak;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public final c_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, "IMAGES"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Thumbnails;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Images$Thumbnails;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method protected final g()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    return v0
.end method
