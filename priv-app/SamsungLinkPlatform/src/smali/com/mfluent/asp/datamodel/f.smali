.class public final Lcom/mfluent/asp/datamodel/f;
.super Lcom/mfluent/asp/datamodel/ao;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/f$a;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ao;-><init>()V

    .line 21
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/f;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 25
    const-string v0, "archivedMedia"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 30
    const-string v0, "archivedMedia"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->b(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    const-string v0, "ARCHIVED_MEDIA"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const-string v0, "ARCHIVED_MEDIA"

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ArchivedMedia;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ArchivedMedia;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ArchivedMedia;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
