.class public Lcom/mfluent/asp/datamodel/ae;
.super Lcom/mfluent/asp/datamodel/ap;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ae$a;
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "local_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "local_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "local_thumb_data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "local_source_media_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "local_source_album_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ae;->a:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ap;-><init>()V

    .line 45
    return-void
.end method

.method public static a()Lcom/mfluent/asp/datamodel/ae;
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/mfluent/asp/datamodel/ae$a;->a()Lcom/mfluent/asp/datamodel/ae;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 111
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 116
    const-string v0, "device_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 117
    const-string v0, "device_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 120
    :goto_0
    const-string v3, "audio_id"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 121
    const-string v0, "audio_id"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 127
    :goto_1
    const-string v3, "genre_id"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 128
    const-string v1, "genre_id"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 133
    :cond_0
    :goto_2
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lez v3, :cond_1

    .line 134
    const-string v3, "genre_id"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 137
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    .line 138
    const-string v1, "audio_id"

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 141
    :cond_2
    const-string v0, "audio_id"

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "genre_id"

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 142
    :cond_3
    const-wide/16 v0, -0x1

    .line 145
    :goto_3
    return-wide v0

    .line 123
    :cond_4
    const-string v3, "source_media_id"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-eqz v0, :cond_7

    .line 124
    iget-object v3, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "source_media_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/mfluent/asp/datamodel/j;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1

    .line 129
    :cond_5
    const-string v3, "name"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 130
    invoke-static {}, Lcom/mfluent/asp/datamodel/ad;->g()Lcom/mfluent/asp/datamodel/ad;

    move-result-object v1

    const-string v3, "name"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p1, v3}, Lcom/mfluent/asp/datamodel/ad;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_2

    .line 145
    :cond_6
    invoke-super {p0, p1, v2, p3}, Lcom/mfluent/asp/datamodel/ap;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto :goto_3

    :cond_7
    move-object v0, v1

    goto :goto_1

    :cond_8
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 49
    const-string v0, "genre_members"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final a_()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/mfluent/asp/datamodel/ae;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 63
    const-string v0, "genre_members"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->b(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return-object v0
.end method

.method protected b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const-string v0, "GENRE_MEMBERS_DETAIL"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const-string v0, "GENRE_MEMBERS"

    return-object v0
.end method

.method public d_()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x1

    return v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres$Members;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres$Members;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres$Members;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
