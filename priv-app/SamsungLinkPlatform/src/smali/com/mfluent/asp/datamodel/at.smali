.class public abstract Lcom/mfluent/asp/datamodel/at;
.super Lcom/mfluent/asp/datamodel/ao;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/at$1;,
        Lcom/mfluent/asp/datamodel/at$a;
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lcom/mfluent/asp/datamodel/at$a;",
            "Lcom/mfluent/asp/media/c/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "width"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "height"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "full_width"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "full_height"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "thumb_width"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "thumb_height"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "orientation"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/at;->a:[Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/at;->b:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ao;-><init>()V

    .line 30
    return-void
.end method

.method private static a(Landroid/net/Uri;Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 310
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_0

    .line 314
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 320
    :cond_0
    :goto_0
    return p2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;Ljava/lang/String;)J
    .locals 3

    .prologue
    .line 296
    const-wide/16 v0, 0x0

    .line 297
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 298
    if-eqz v2, :cond_0

    .line 300
    :try_start_0
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 306
    :cond_0
    :goto_0
    return-wide v0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 286
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 288
    if-eqz v0, :cond_0

    .line 289
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result p2

    .line 292
    :cond_0
    return p2
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;JLandroid/content/ContentValues;)I
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J
    .locals 2

    .prologue
    .line 117
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;
    .locals 11

    .prologue
    .line 134
    iget v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->e:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 135
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Only single entries may be queried. Please use an entry URI."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 137
    :cond_0
    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    const-string v3, "cancel"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/mfluent/asp/datamodel/at;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v2

    .line 138
    if-eqz v2, :cond_2

    .line 139
    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    const-string v3, "group_id"

    invoke-static {v2, v3}, Lcom/mfluent/asp/datamodel/at;->a(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v6

    .line 141
    new-instance v2, Lcom/mfluent/asp/datamodel/at$a;

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/at;->g()I

    move-result v5

    move-wide/from16 v3, p9

    invoke-direct/range {v2 .. v7}, Lcom/mfluent/asp/datamodel/at$a;-><init>(JIJ)V

    .line 142
    sget-object v3, Lcom/mfluent/asp/datamodel/at;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mfluent/asp/media/c/f;

    .line 143
    if-eqz v3, :cond_1

    .line 144
    sget-object v4, Lcom/mfluent/asp/datamodel/at;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    invoke-virtual {v3}, Lcom/mfluent/asp/media/c/f;->f()V

    .line 147
    :cond_1
    const/4 v2, 0x0

    .line 199
    :goto_0
    return-object v2

    .line 151
    :cond_2
    if-eqz p2, :cond_4

    .line 152
    array-length v3, p2

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_5

    aget-object v4, p2, v2

    .line 153
    sget-object v5, Lcom/mfluent/asp/datamodel/at;->a:[Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 154
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Illegal Field: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 152
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 158
    :cond_4
    sget-object p2, Lcom/mfluent/asp/datamodel/at;->a:[Ljava/lang/String;

    .line 161
    :cond_5
    new-instance v3, Landroid/database/MatrixCursor;

    invoke-direct {v3, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 162
    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/media/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/media/d;

    move-result-object v2

    .line 163
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/at;->g()I

    move-result v4

    move-wide/from16 v0, p9

    invoke-virtual {v2, v0, v1, v4}, Lcom/mfluent/asp/media/d;->a(JI)Lcom/mfluent/asp/media/c/d;

    move-result-object v4

    .line 165
    if-eqz v4, :cond_11

    .line 166
    invoke-virtual {v4}, Lcom/mfluent/asp/media/c/d;->a()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v5

    .line 167
    if-eqz v5, :cond_11

    .line 168
    array-length v2, p2

    new-array v6, v2, [Ljava/lang/Object;

    .line 169
    const/4 v2, 0x0

    :goto_2
    array-length v7, p2

    if-ge v2, v7, :cond_10

    .line 170
    const-string v7, "_id"

    aget-object v8, p2, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 171
    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v2

    .line 169
    :cond_6
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 172
    :cond_7
    const-string v7, "_size"

    aget-object v8, p2, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 173
    invoke-virtual {v4}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 174
    invoke-virtual {v4}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v2

    goto :goto_3

    .line 176
    :cond_8
    const-string v7, "width"

    aget-object v8, p2, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 177
    invoke-virtual {v5}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredWidth()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    goto :goto_3

    .line 178
    :cond_9
    const-string v7, "height"

    aget-object v8, p2, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 179
    invoke-virtual {v5}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredHeight()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    goto :goto_3

    .line 180
    :cond_a
    const-string v7, "full_width"

    aget-object v8, p2, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 181
    invoke-virtual {v5}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getFullWidth()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    goto :goto_3

    .line 182
    :cond_b
    const-string v7, "full_height"

    aget-object v8, p2, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 183
    invoke-virtual {v5}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getFullHeight()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    goto :goto_3

    .line 184
    :cond_c
    const-string v7, "orientation"

    aget-object v8, p2, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 185
    invoke-virtual {v5}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getOrientation()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    goto/16 :goto_3

    .line 186
    :cond_d
    const-string v7, "thumb_width"

    aget-object v8, p2, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 187
    invoke-virtual {v5}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbWidth()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    goto/16 :goto_3

    .line 188
    :cond_e
    const-string v7, "thumb_height"

    aget-object v8, p2, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 189
    invoke-virtual {v5}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbHeight()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    goto/16 :goto_3

    .line 190
    :cond_f
    const-string v7, "device_id"

    aget-object v8, p2, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 191
    invoke-virtual {v5}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDeviceId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    goto/16 :goto_3

    .line 195
    :cond_10
    invoke-virtual {v3, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_11
    move-object v2, v3

    .line 199
    goto/16 :goto_0
.end method

.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Lcom/mfluent/asp/datamodel/ao$a;J)Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 206
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->f:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/a/a/a;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/Long;

    .line 208
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/at;->b()[Ljava/lang/String;

    move-result-object v2

    .line 209
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/at;->c_()Ljava/lang/String;

    move-result-object v1

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 218
    if-eqz v1, :cond_1

    .line 219
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    invoke-static {v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fromCursor(Landroid/database/Cursor;)Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v0

    .line 222
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    :goto_1
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->f:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v8}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->f:Lcom/mfluent/asp/util/a/a/a;

    invoke-virtual {v1, v8}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Object;)Z

    throw v0

    :cond_0
    move-object v0, v9

    goto :goto_0

    :cond_1
    move-object v0, v9

    goto :goto_1
.end method

.method public final a_(Lcom/mfluent/asp/datamodel/ao$a;J)Landroid/os/ParcelFileDescriptor;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 232
    invoke-virtual {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/at;->a(Lcom/mfluent/asp/datamodel/ao$a;J)Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v0

    .line 233
    if-nez v0, :cond_0

    .line 234
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Record not found for Uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_0
    const/4 v1, 0x0

    .line 239
    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    const-string v3, "skip_cache_get"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/mfluent/asp/datamodel/at;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v3

    .line 240
    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    const-string v4, "skip_cache_put"

    const/4 v5, 0x1

    invoke-static {v2, v4, v5}, Lcom/mfluent/asp/datamodel/at;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v4

    .line 241
    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    const-string v5, "cache_only"

    const/4 v6, 0x0

    invoke-static {v2, v5, v6}, Lcom/mfluent/asp/datamodel/at;->a(Landroid/net/Uri;Ljava/lang/String;Z)Z

    move-result v5

    .line 242
    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    const-string v6, "group_id"

    invoke-static {v2, v6}, Lcom/mfluent/asp/datamodel/at;->a(Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v6

    .line 243
    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    const-string v8, "width"

    const/16 v9, 0x200

    const/16 v10, 0x180

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v2, v8, v9}, Lcom/mfluent/asp/datamodel/at;->a(Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v2

    .line 247
    iget-object v8, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    const-string v9, "height"

    const/16 v10, 0x200

    const/16 v11, 0x180

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v10

    invoke-static {v8, v9, v10}, Lcom/mfluent/asp/datamodel/at;->a(Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v8

    .line 252
    iget-object v9, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v9}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/mfluent/asp/media/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/media/d;

    move-result-object v9

    .line 254
    invoke-virtual {v0, v2}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setDesiredWidth(I)V

    .line 255
    invoke-virtual {v0, v8}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setDesiredHeight(I)V

    .line 257
    sget-object v2, Lcom/mfluent/asp/datamodel/at$1;->a:[I

    invoke-virtual {v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDeviceTransportType()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->ordinal()I

    move-result v8

    aget v2, v2, v8

    packed-switch v2, :pswitch_data_0

    .line 266
    :goto_0
    if-nez v1, :cond_1

    .line 272
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "No valid device found."

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :pswitch_0
    new-instance v1, Lcom/mfluent/asp/media/c/e;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/media/c/e;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    goto :goto_0

    .line 262
    :pswitch_1
    new-instance v1, Lcom/mfluent/asp/media/c/a;

    invoke-direct {v1, v0, v9}, Lcom/mfluent/asp/media/c/a;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/media/d;)V

    goto :goto_0

    .line 265
    :pswitch_2
    new-instance v1, Lcom/mfluent/asp/media/c/c;

    invoke-direct {v1, v0, v9}, Lcom/mfluent/asp/media/c/c;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/media/d;)V

    goto :goto_0

    .line 275
    :cond_1
    new-instance v8, Lcom/mfluent/asp/datamodel/at$a;

    invoke-direct {v8, v0, v6, v7}, Lcom/mfluent/asp/datamodel/at$a;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;J)V

    .line 276
    sget-object v0, Lcom/mfluent/asp/datamodel/at;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v8, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    :try_start_0
    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    if-nez v4, :cond_3

    const/4 v4, 0x1

    :goto_2
    invoke-virtual/range {v1 .. v7}, Lcom/mfluent/asp/media/c/f;->b(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;ZZZJ)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 280
    sget-object v1, Lcom/mfluent/asp/datamodel/at;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v8}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    .line 278
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    .line 280
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/datamodel/at;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v8}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0

    .line 257
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract b()[Ljava/lang/String;
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 101
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "image/jpeg"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final d_()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract g()I
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return-object v0
.end method
