.class final Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2043
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    .line 2044
    const-string v0, "asp.db"

    const/4 v1, 0x0

    const/16 v2, 0x4e35

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 2045
    return-void
.end method

.method static synthetic A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_CROSS_DEVICE_IMAGES AS SELECT * FROM API_IMAGES GROUP BY burst_dup_id HAVING device_priority = MIN(device_priority);"

    return-object v0
.end method

.method static synthetic B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS BURST_SHOT_IMAGES AS SELECT * FROM IMAGES GROUP BY burst_id HAVING device_priority = MIN(device_priority);"

    return-object v0
.end method

.method static synthetic C()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_BURST_SHOT_IMAGES AS SELECT * FROM API_IMAGES GROUP BY burst_id HAVING device_priority = MIN(device_priority);"

    return-object v0
.end method

.method static synthetic D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS CROSS_DEVICE_AUDIO AS SELECT * FROM AUDIO_DETAILS GROUP BY dup_id HAVING device_priority = MIN(device_priority);"

    return-object v0
.end method

.method static synthetic E()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_CROSS_DEVICE_AUDIO AS SELECT * FROM API_AUDIO_DETAILS GROUP BY dup_id HAVING device_priority = MIN(device_priority);"

    return-object v0
.end method

.method static synthetic F()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS CROSS_DEVICE_DOCUMENTS AS SELECT * FROM DOCUMENTS GROUP BY dup_id HAVING device_priority = MIN(device_priority);"

    return-object v0
.end method

.method static synthetic G()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_CROSS_DEVICE_DOCUMENTS AS SELECT * FROM API_DOCUMENTS GROUP BY dup_id HAVING device_priority = MIN(device_priority);"

    return-object v0
.end method

.method static synthetic H()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS KEYWORD_ORPHANS AS SELECT KEYWORDS.* FROM KEYWORDS LEFT OUTER JOIN KEYWORD_MAP ON KEYWORDS._id=KEYWORD_MAP.keyword_id WHERE KEYWORD_MAP._id IS NULL;"

    return-object v0
.end method

.method static synthetic I()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS ALBUM_DETAIL AS SELECT ALBUMS.*, ALBUMS._id AS album_id,ARTISTS._id AS artist_id,artist,artist_index_char,source_album_id,AUDIO.thumbnail_uri AS thumbnail_uri,thumb_data,thumb_width,thumb_height,AUDIO.year,device_id,dup_id,transport_type,physical_type,device_priority,12 AS media_type FROM (AUDIO LEFT OUTER JOIN ARTISTS ON artist_id=ARTISTS._id) INNER JOIN ALBUMS ON AUDIO.album_id=ALBUMS._id;"

    return-object v0
.end method

.method static synthetic J()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_ALBUM_DETAIL AS SELECT ALBUMS.*, ALBUMS._id AS album_id,thumb_data,ARTISTS._id AS artist_id,artist,artist_index_char,API_AUDIO.year,device_id,dup_id,transport_type,physical_type,device_priority,12 AS media_type FROM (API_AUDIO LEFT OUTER JOIN ARTISTS ON artist_id=ARTISTS._id) INNER JOIN ALBUMS ON API_AUDIO.album_id=ALBUMS._id;"

    return-object v0
.end method

.method static synthetic K()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS ALBUM_ORPHANS AS SELECT ALBUMS.* FROM ALBUMS LEFT OUTER JOIN FILES ON ALBUMS._id=FILES.album_id WHERE FILES._id IS NULL;"

    return-object v0
.end method

.method static synthetic L()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS ARTISTS_DETAIL AS SELECT ARTISTS.*,album_id,thumb_data,thumb_width,thumb_height,source_media_id,thumbnail_uri,source_album_id,device_id,dup_id,13 AS media_type FROM ARTISTS INNER JOIN AUDIO ON AUDIO.artist_id=ARTISTS._id;"

    return-object v0
.end method

.method static synthetic M()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_ARTISTS_DETAIL AS SELECT ARTISTS.*,album_id,thumb_data,thumb_width,thumb_height,device_id,dup_id,13 AS media_type FROM ARTISTS INNER JOIN API_AUDIO ON API_AUDIO.artist_id=ARTISTS._id;"

    return-object v0
.end method

.method static synthetic N()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS ARTIST_ORPHANS AS SELECT ARTISTS.* FROM ARTISTS LEFT OUTER JOIN FILES ON ARTISTS._id=FILES.artist_id WHERE FILES._id IS NULL;"

    return-object v0
.end method

.method static synthetic O()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_DEVICES AS SELECT _id,alias_name,transport_type,physical_type,model_name,model_id,network_mode,is_on_local_network,local_ip_address,is_syncing,registration_date FROM DEVICES"

    return-object v0
.end method

.method static synthetic P()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS GENRES_AGGREGATE AS SELECT GENRES.*,GENRE_MEMBERS.audio_id,source_media_id,album_id,thumb_data,thumb_width,thumb_height,artist_id,thumbnail_uri,source_album_id,device_id,dup_id,title,14 AS media_type FROM (AUDIO INNER JOIN GENRE_MEMBERS ON AUDIO._id=GENRE_MEMBERS.audio_id) INNER JOIN GENRES ON GENRE_MEMBERS.genre_id=GENRES._id"

    return-object v0
.end method

.method static synthetic Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_GENRES_AGGREGATE AS SELECT GENRES.*,GENRE_MEMBERS.audio_id,album_id,thumb_data,thumb_width,thumb_height,artist_id,device_id,dup_id,title,14 AS media_type FROM (API_AUDIO INNER JOIN GENRE_MEMBERS ON API_AUDIO._id=GENRE_MEMBERS.audio_id) INNER JOIN GENRES ON GENRE_MEMBERS.genre_id=GENRES._id"

    return-object v0
.end method

.method static synthetic R()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS GENRE_ORPHANS AS SELECT GENRES.* FROM GENRES LEFT OUTER JOIN GENRE_MEMBERS ON GENRES._id=GENRE_MEMBERS.genre_id WHERE GENRE_MEMBERS._id IS NULL;"

    return-object v0
.end method

.method static synthetic S()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS GENRE_MEMBERS_DETAIL AS SELECT AUDIO_DETAILS.*,audio_id,genre_id,GENRES.name,GENRES.genre_key,GENRE_MEMBERS._id AS genre_map_id FROM (AUDIO_DETAILS INNER JOIN GENRE_MEMBERS ON AUDIO_DETAILS._id=GENRE_MEMBERS.audio_id) INNER JOIN GENRES ON GENRE_MEMBERS.genre_id=GENRES._id"

    return-object v0
.end method

.method static synthetic T()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_GENRE_MEMBERS_DETAIL AS SELECT API_AUDIO_DETAILS.*,audio_id,genre_id,GENRES.name,GENRES.genre_key,GENRE_MEMBERS._id AS genre_map_id FROM (API_AUDIO_DETAILS INNER JOIN GENRE_MEMBERS ON API_AUDIO_DETAILS._id=GENRE_MEMBERS.audio_id) INNER JOIN GENRES ON GENRE_MEMBERS.genre_id=GENRES._id"

    return-object v0
.end method

.method static synthetic U()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS GENRE_MEMBERS_SYNC AS SELECT AUDIO_DETAILS.*,audio_id,genre_id,GENRES.name,GENRES.genre_key,GENRE_MEMBERS._id AS genre_map_id FROM (AUDIO_DETAILS LEFT JOIN GENRE_MEMBERS ON AUDIO_DETAILS._id=GENRE_MEMBERS.audio_id) LEFT JOIN GENRES ON GENRE_MEMBERS.genre_id=GENRES._id"

    return-object v0
.end method

.method static synthetic V()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS ALL_MEDIA AS SELECT FILE_KEYWORD_DETAIL._id AS _id, title, FILE_KEYWORD_DETAIL.device_id AS device_id, media_type, mime_type, _display_name, source_media_id, full_uri, _data, thumbnail_uri, thumb_data, thumb_width, thumb_height, width, height, date_added, date_modified, _size, index_char, description, latitude, longitude, bucket_id, bucket_display_name, group_id, orientation, geo_loc_locale,geo_loc_country,geo_loc_province,geo_loc_sub_province,geo_loc_locality,geo_loc_sub_locality,geo_loc_feature,geo_loc_thoroughfare,geo_loc_sub_thoroughfare,geo_loc_premises,geo_loc_last_timestamp,geo_is_valid_map_coord,datetaken, bookmark, duration, caption_type, caption_uri, caption_index_uri, resolution, isPlayed, ARTISTS.artist AS artist, artist_id, artist_index_char, ALBUMS.album AS album, album_id, album_artist, album_index_char, source_album_id, recently_played, most_played, dup_id, burst_dup_id, burst_id, device_priority, transport_type, physical_type, file_digest, keyword, keyword_type, keyword_id, keyword_map_id, is_personal FROM (FILE_KEYWORD_DETAIL LEFT OUTER JOIN ARTISTS ON FILE_KEYWORD_DETAIL.artist_id=ARTISTS._id) LEFT OUTER JOIN ALBUMS ON FILE_KEYWORD_DETAIL.album_id=ALBUMS._id;"

    return-object v0
.end method

.method private W()Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;
    .locals 2

    .prologue
    .line 2048
    const/4 v0, 0x0

    .line 2049
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 2050
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;

    .line 2052
    :cond_0
    if-nez v0, :cond_1

    .line 2053
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;-><init>(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;)V

    .line 2054
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b:Ljava/lang/ref/WeakReference;

    .line 2057
    :cond_1
    return-object v0
.end method

.method public static a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3130
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE VIEW IF NOT EXISTS LOCAL_DUPS AS SELECT dup_id AS local_dup_id,_id AS local_id,_data AS local_data,source_media_id AS local_source_media_id,source_album_id AS local_source_album_id,caption_uri AS local_caption_path,caption_index_uri AS local_caption_index_path,thumb_data AS local_thumb_data,thumb_width AS local_thumb_width,thumb_height AS local_thumb_height FROM FILES_DEVICES WHERE transport_type=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GROUP BY dup_id;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1748
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE TABLE IF NOT EXISTS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(_id INTEGER PRIMARY KEY AUTOINCREMENT,media_id TEXT,is_delete INTEGER,orig_journal_id INTEGER,timestamp INTEGER);"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1748
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE VIEW IF NOT EXISTS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".*,file_id,keyword_id,keyword_type,keyword,KEYWORD_MAP._id AS keyword_map_id FROM (KEYWORDS INNER JOIN KEYWORD_MAP ON KEYWORDS._id=keyword_id) INNER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "KEYWORD_MAP.file_id;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 2422
    const/4 v9, 0x0

    .line 2423
    const-string v1, "DEVICES"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const-string v3, "transport_type=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2425
    if-eqz v1, :cond_0

    .line 2426
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2427
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2430
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v9, v0

    .line 2433
    :cond_0
    if-nez v9, :cond_2

    .line 2480
    :cond_1
    :goto_1
    return-void

    .line 2437
    :cond_2
    const-string v1, "FILES"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "latitude"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "longitude"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "geo_loc_locale"

    aput-object v3, v2, v0

    const-string v3, "geo_loc_locale IS NOT NULL AND device_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v9, v4, v0

    const-string v5, "geo_loc_locale"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2444
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2445
    if-eqz v0, :cond_5

    .line 2446
    :cond_3
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2447
    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2448
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    .line 2449
    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    .line 2450
    invoke-static {v2, v4, v5, v6, v7}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;DD)Ljava/lang/String;

    move-result-object v3

    .line 2451
    if-eqz v3, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2452
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "Reformatting geo-location locale from:{} to:{}"

    invoke-interface {v4, v5, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2453
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2457
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2460
    :cond_5
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2461
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2463
    :try_start_0
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 2464
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2465
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    .line 2466
    const/4 v1, 0x1

    aput-object v9, v3, v1

    .line 2467
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2468
    const-string v5, "geo_loc_locale"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2469
    const/4 v1, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v3, v1

    .line 2470
    const-string v0, "FILES"

    const-string v1, "geo_loc_locale=? AND device_id=?"

    invoke-virtual {p0, v0, v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 2477
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 2475
    :cond_6
    :try_start_1
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2477
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_1

    :cond_7
    move-object v0, v9

    goto/16 :goto_0
.end method

.method static synthetic a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1748
    invoke-static {p0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2555
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ALTER TABLE "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ADD COLUMN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2556
    return-void
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1748
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE TABLE IF NOT EXISTS DEVICES (_id INTEGER PRIMARY KEY AUTOINCREMENT,peer_id TEXT,eimei BLOB,alias_name TEXT,transport_type TEXT DEFAULT \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\',device_priority INTEGER DEFAULT 2147483647,physical_type TEXT DEFAULT \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\',model_name TEXT,model_id TEXT,web_storage_type TEXT,web_storage_account_id TEXT,web_storage_user_id TEXT,web_storage_pw_id TEXT,web_storage_email_id TEXT,web_storage_is_signed_in_id INTEGER,web_storage_total_capacity_id INTEGER,web_storage_used_capacity_id INTEGER,sync_key_images TEXT,sync_key_audio TEXT,sync_key_videos TEXT,sync_key_documents TEXT,is_sync_server INTEGER,supports_push_notification"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INTEGER,is_hls_server INTEGER,is_ts_server INTEGER,server_sort_key INTEGER,device_unique_id TEXT,last_recv_image_journal_id INTEGER,last_recv_video_journal_id INTEGER,last_recv_music_journal_id INTEGER,last_recv_document_journal_id INTEGER,sync_server_supported_media_types TEXT,udn_wifi TEXT,udn_wifi_direct TEXT,is_remote_wakeup INTEGER,network_mode TEXT,is_on_local_network INTEGER,local_ip_address TEXT,is_syncing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INTEGER,supports_auto_archive INTEGER,registration_date INTEGER,supports_threebox_transfer INTEGER,is_remote_wakeup_support INTEGER,smallest_drive_capacity INTEGER,host_peer_id TEXT,mac_bt TEXT,mac_ble TEXT,mac_wifi TEXT,mac_wifi_direct TEXT,network_mode_sort INTEGER);"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 4821
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE INDEX IF NOT EXISTS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 2484
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Enter limitGeoLocationToDecimalPlaces()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 2486
    const/4 v9, 0x0

    .line 2487
    const-string v1, "DEVICES"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const-string v3, "transport_type=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2489
    if-eqz v1, :cond_0

    .line 2490
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2491
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2494
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v9, v0

    .line 2497
    :cond_0
    if-nez v9, :cond_2

    .line 2551
    :cond_1
    :goto_1
    return-void

    .line 2501
    :cond_2
    const-string v1, "FILES"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "latitude"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "longitude"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "geo_loc_locale"

    aput-object v3, v2, v0

    const-string v3, "geo_loc_locale IS NOT NULL AND device_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v9, v4, v0

    const-string v5, "geo_loc_locale"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2508
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2509
    if-eqz v0, :cond_5

    .line 2510
    :cond_3
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2511
    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2512
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    .line 2513
    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    .line 2514
    invoke-static {v2, v4, v5, v6, v7}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/String;DD)Ljava/lang/String;

    move-result-object v3

    .line 2520
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "::limitGeoLocationToDecimalPlaces() - oldLocaleEncoding:{} newEncodingValue:{}"

    invoke-interface {v4, v5, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2522
    if-eqz v3, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2523
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "Reformatting geo-location locale from:{} to:{}"

    invoke-interface {v4, v5, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2524
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2528
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2531
    :cond_5
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2532
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2534
    :try_start_0
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 2535
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    .line 2536
    const/4 v1, 0x1

    aput-object v9, v2, v1

    .line 2537
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2538
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2539
    const-string v5, "geo_loc_locale"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2540
    const/4 v1, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v1

    .line 2541
    const-string v0, "FILES"

    const-string v1, "geo_loc_locale=? AND device_id=?"

    invoke-virtual {p0, v0, v4, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 2548
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 2546
    :cond_6
    :try_start_1
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2548
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_1

    :cond_7
    move-object v0, v9

    goto/16 :goto_0
.end method

.method static synthetic b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1748
    invoke-static {p0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    return-void
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 4767
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "INSERT INTO db_integrity(name,value) VALUES (\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\',\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\');"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4783
    :goto_0
    return-void

    .line 4780
    :catch_0
    move-exception v0

    .line 4781
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::insertIntoDbIntegrityTable: Failed to insert into DB integrity table."

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE TABLE IF NOT EXISTS FILES (_id INTEGER PRIMARY KEY AUTOINCREMENT,device_id INTEGER,source_media_id TEXT,date_added INTEGER,date_modified INTEGER,_data TEXT,mime_type TEXT,_size INTEGER,media_type INTEGER,full_uri TEXT,thumbnail_uri TEXT,_display_name TEXT,title TEXT,index_char TEXT,dup_id INTEGER,burst_dup_id INTEGER,burst_id INTEGER,thumb_data TEXT,thumb_width INTEGER,thumb_height INTEGER,file_digest TEXT,file_digest_size INTEGER,file_digest_time INTEGER,width INTEGER,height INTEGER,datetaken INTEGER,orientation INTEGER,bucket_id TEXT,bucket_display_name TEXT,latitude DOUBLE,longitude DOUBLE,geo_loc_locale TEXT,geo_loc_country TEXT,geo_loc_province TEXT,geo_loc_sub_province TEXT,geo_loc_locality TEXT,geo_loc_sub_locality TEXT,geo_loc_feature TEXT,geo_loc_thoroughfare TEXT,geo_loc_sub_thoroughfare TEXT,geo_loc_premises TEXT,geo_loc_last_timestamp INTEGER,geo_is_valid_map_coord INTEGER,description TEXT,isprivate INTEGER,picasa_id TEXT,group_id TEXT,album TEXT,artist TEXT,category TEXT,language TEXT,resolution TEXT,tags TEXT,caption_type TEXT,caption_uri TEXT,caption_index_uri TEXT,isPlayed INTEGER,bookmark INTEGER,duration INTEGER,album_id INTEGER,source_album_id TEXT,artist_id INTEGER,composer TEXT,is_alarm INTEGER,is_music INTEGER,is_notification INTEGER,is_podcast INTEGER,is_ringtone INTEGER,title_key TEXT,track INTEGER,year INTEGER,recently_played INTEGER,most_played INTEGER,is_personal INTEGER);"

    return-object v0
.end method

.method private static c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 5214
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Enter ::insertIntoGeoLocationTable()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 5216
    const-string v0, "INSERT INTO GEO_LOCATION(latitude,longitude,geo_loc_locale,geo_loc_country,geo_loc_province,geo_loc_sub_province,geo_loc_locality,geo_loc_sub_locality,geo_loc_feature,geo_loc_thoroughfare,geo_loc_sub_thoroughfare,geo_loc_premises) SELECT latitude,longitude,geo_loc_locale,geo_loc_country,geo_loc_province,geo_loc_sub_province,geo_loc_locality,geo_loc_sub_locality,geo_loc_feature,geo_loc_thoroughfare,geo_loc_sub_thoroughfare,geo_loc_premises FROM FILES WHERE geo_loc_locale IS NOT NULL GROUP BY geo_loc_locale"

    .line 5275
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::insertIntoGeoLocationTable(): insertCmd:{}"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 5276
    invoke-static {p0, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5280
    :goto_0
    return-void

    .line 5277
    :catch_0
    move-exception v0

    .line 5278
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::insertIntoGeoLocationTable: Failed to insert into GEO_LOCATION table."

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1748
    invoke-static {p0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->f(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE TABLE IF NOT EXISTS ALBUMS (_id INTEGER PRIMARY KEY AUTOINCREMENT,album TEXT,album_artist TEXT,album_key TEXT,album_artist_key TEXT,album_index_char TEXT);"

    return-object v0
.end method

.method private static d(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteException;
        }
    .end annotation

    .prologue
    .line 2205
    :try_start_0
    invoke-static {p0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->f(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2209
    :goto_0
    return-void

    .line 2207
    :catch_0
    move-exception v0

    invoke-static {p0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE TABLE IF NOT EXISTS ARTISTS (_id INTEGER PRIMARY KEY AUTOINCREMENT,artist TEXT,artist_key TEXT,artist_index_char TEXT);"

    return-object v0
.end method

.method private static e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2559
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DROP TABLE IF EXISTS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2560
    return-void
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE TABLE IF NOT EXISTS GENRES (_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,genre_key TEXT);"

    return-object v0
.end method

.method private static f(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2563
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DROP VIEW IF EXISTS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2564
    return-void
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE TABLE IF NOT EXISTS GENRE_MEMBERS(_id INTEGER PRIMARY KEY AUTOINCREMENT,audio_id INTEGER,genre_id INTEGER);"

    return-object v0
.end method

.method private static g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2577
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::logAndExecCmd:{}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2578
    invoke-virtual {p0, p1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2579
    return-void
.end method

.method static synthetic h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE TABLE IF NOT EXISTS KEYWORDS (_id INTEGER PRIMARY KEY AUTOINCREMENT,keyword TEXT);"

    return-object v0
.end method

.method static synthetic i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE TABLE IF NOT EXISTS KEYWORD_MAP (_id INTEGER PRIMARY KEY AUTOINCREMENT,file_id INTEGER,keyword_id INTEGER,keyword_type INTEGER DEFAULT 1);"

    return-object v0
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE TABLE IF NOT EXISTS  FILE_TRANSFER_SESSIONS (_id INTEGER PRIMARY KEY AUTOINCREMENT,uuid TEXT,state INTEGER,createdTime INTEGER,endTime INTEGER,sourceDeviceId INTEGER,targetDeviceId INTEGER,transferSize INTEGER,firstFileName TEXT,numFiles INTEGER,transferarchive INTEGER,numFilesSkipped INTEGER,currentTransferSize INTEGER)"

    return-object v0
.end method

.method static synthetic k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE TABLE IF NOT EXISTS ARCHIVED_MEDIA (_id INTEGER PRIMARY KEY AUTOINCREMENT,device_id INTEGER,datetaken INTEGER,_size INTEGER,_display_name TEXT)"

    return-object v0
.end method

.method static synthetic l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE TABLE IF NOT EXISTS db_integrity(name TEXT PRIMARY KEY,value TEXT);"

    return-object v0
.end method

.method static synthetic m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1748
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Enter ::createGeoLocationTable()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS GEO_LOCATION (_id INTEGER PRIMARY KEY AUTOINCREMENT,latitude DOUBLE,longitude DOUBLE,geo_loc_locale TEXT,geo_loc_country TEXT,geo_loc_province TEXT,geo_loc_sub_province TEXT,geo_loc_locality TEXT,geo_loc_sub_locality TEXT,geo_loc_feature TEXT,geo_loc_thoroughfare TEXT,geo_loc_sub_thoroughfare TEXT,geo_loc_premises TEXT);"

    return-object v0
.end method

.method static synthetic n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS FILES_DEVICES AS SELECT FILES.*,device_priority,transport_type,physical_type FROM FILES INNER JOIN DEVICES ON FILES.device_id=DEVICES._id;"

    return-object v0
.end method

.method static synthetic o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_FILES_DEVICES AS SELECT FILES._id,device_id,date_added,date_modified,_data,mime_type,_size,media_type,_display_name,title,index_char,dup_id,burst_dup_id,burst_id,thumb_data,thumb_width,thumb_height,file_digest,width,height,datetaken,orientation,bucket_id,bucket_display_name,latitude,longitude,geo_loc_locale,geo_loc_country,geo_loc_province,geo_loc_sub_province,geo_loc_locality,geo_loc_sub_locality,geo_loc_feature,geo_loc_thoroughfare,geo_loc_sub_thoroughfare,geo_loc_premises,description,isprivate,picasa_id,group_id,album,artist,category,language,resolution,tags,caption_type,isPlayed,bookmark,duration,album_id,artist_id,composer,is_alarm,is_music,is_notification,is_podcast,is_ringtone,title_key,track,year,recently_played,most_played,is_personal,device_priority,transport_type,physical_type FROM FILES INNER JOIN DEVICES ON FILES.device_id=DEVICES._id;"

    return-object v0
.end method

.method static synthetic p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS IMAGES AS SELECT _id,device_id,source_media_id,bucket_display_name,bucket_id,date_added,date_modified,datetaken,description,_display_name,isprivate,latitude,longitude,geo_loc_locale,geo_loc_country,geo_loc_province,geo_loc_sub_province,geo_loc_locality,geo_loc_sub_locality,geo_loc_feature,geo_loc_thoroughfare,geo_loc_sub_thoroughfare,geo_loc_premises,geo_loc_last_timestamp,geo_is_valid_map_coord,mime_type,orientation,picasa_id,_size,title,index_char,width,height,thumbnail_uri,full_uri,_data,thumb_data,thumb_width,thumb_height,media_type,group_id,file_digest,file_digest_size,file_digest_time,dup_id,burst_dup_id,burst_id,device_priority,transport_type,physical_type FROM FILES_DEVICES WHERE media_type=1;"

    return-object v0
.end method

.method static synthetic q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_IMAGES AS SELECT _id,device_id,bucket_display_name,bucket_id,date_added,date_modified,datetaken,description,_display_name,isprivate,latitude,longitude,geo_loc_locale,geo_loc_country,geo_loc_province,geo_loc_sub_province,geo_loc_locality,geo_loc_sub_locality,geo_loc_feature,geo_loc_thoroughfare,geo_loc_sub_thoroughfare,geo_loc_premises,mime_type,orientation,picasa_id,_size,title,index_char,width,height,_data,media_type,group_id,file_digest,dup_id,burst_dup_id,burst_id,thumb_data,thumb_width,thumb_height,device_priority,transport_type,physical_type FROM API_FILES_DEVICES WHERE media_type=1;"

    return-object v0
.end method

.method static synthetic r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS AUDIO AS SELECT _id,device_id,source_media_id,source_album_id,bucket_display_name,album_id,artist_id,bookmark,composer,date_added,date_modified,_display_name,duration,is_music,is_podcast,mime_type,_size,title,title_key,index_char,track,year,width,height,thumbnail_uri,full_uri,_data,recently_played,most_played,media_type,file_digest,file_digest_size,file_digest_time,dup_id,burst_dup_id,burst_id,thumb_data,thumb_width,thumb_height,device_priority,transport_type,physical_type FROM FILES_DEVICES WHERE media_type=2;"

    return-object v0
.end method

.method static synthetic s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_AUDIO AS SELECT _id,device_id,bucket_display_name,album_id,thumb_data,thumb_width,thumb_height,artist_id,bookmark,composer,date_added,date_modified,_display_name,duration,is_music,is_podcast,mime_type,_size,title,title_key,index_char,track,year,width,height,_data,recently_played,most_played,media_type,file_digest,dup_id,burst_dup_id,burst_id,device_priority,transport_type,physical_type FROM API_FILES_DEVICES WHERE media_type=2;"

    return-object v0
.end method

.method static synthetic t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS VIDEOS AS SELECT _id,device_id,source_media_id,album,artist,bookmark,bucket_display_name,bucket_id,category,date_added,date_modified,datetaken,description,_display_name,duration,isprivate,language,latitude,longitude,geo_loc_locale,geo_loc_country,geo_loc_province,geo_loc_sub_province,geo_loc_locality,geo_loc_sub_locality,geo_loc_feature,geo_loc_thoroughfare,geo_loc_sub_thoroughfare,geo_loc_premises,geo_loc_last_timestamp,geo_is_valid_map_coord,mime_type,resolution,_size,tags,title,index_char,width,height,orientation,thumbnail_uri,full_uri,_data,thumb_data,thumb_width,thumb_height,isPlayed,caption_uri,caption_type,caption_index_uri,media_type,file_digest,file_digest_size,file_digest_time,dup_id,burst_dup_id,burst_id,device_priority,transport_type,physical_type FROM FILES_DEVICES WHERE media_type=3;"

    return-object v0
.end method

.method static synthetic u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_VIDEOS AS SELECT _id,device_id,album,artist,bookmark,bucket_display_name,bucket_id,category,date_added,date_modified,datetaken,description,_display_name,duration,isprivate,language,latitude,longitude,geo_loc_locale,geo_loc_country,geo_loc_province,geo_loc_sub_province,geo_loc_locality,geo_loc_sub_locality,geo_loc_feature,geo_loc_thoroughfare,geo_loc_sub_thoroughfare,geo_loc_premises,mime_type,resolution,_size,tags,title,index_char,width,height,orientation,_data,isPlayed,caption_type,media_type,file_digest,dup_id,burst_dup_id,burst_id,thumb_data,thumb_width,thumb_height,device_priority,transport_type,physical_type FROM API_FILES_DEVICES WHERE media_type=3;"

    return-object v0
.end method

.method static synthetic v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS DOCUMENTS AS SELECT _id,device_id,source_media_id,date_added,date_modified,_display_name,bucket_display_name,mime_type,_size,title,index_char,width,height,thumbnail_uri,full_uri,_data,media_type,file_digest,file_digest_size,file_digest_time,dup_id,burst_dup_id,burst_id,thumb_data,thumb_width,thumb_height,device_priority,transport_type,physical_type,is_personal FROM FILES_DEVICES WHERE media_type=15;"

    return-object v0
.end method

.method static synthetic w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_DOCUMENTS AS SELECT _id,device_id,date_added,date_modified,_display_name,bucket_display_name,mime_type,_size,title,index_char,width,height,_data,media_type,file_digest,dup_id,burst_dup_id,burst_id,thumb_data,thumb_width,thumb_height,device_priority,transport_type,physical_type,is_personal FROM API_FILES_DEVICES WHERE media_type=15;"

    return-object v0
.end method

.method static synthetic x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS AUDIO_DETAILS AS SELECT AUDIO.*,artist,artist_key,artist_index_char,album,album_key,album_artist,album_index_char FROM (AUDIO LEFT OUTER JOIN ARTISTS ON AUDIO.artist_id=ARTISTS._id) LEFT OUTER JOIN ALBUMS ON AUDIO.album_id=ALBUMS._id;"

    return-object v0
.end method

.method static synthetic y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS API_AUDIO_DETAILS AS SELECT API_AUDIO.*,artist,artist_key,artist_index_char,album,album_key,album_artist,album_index_char FROM (API_AUDIO LEFT OUTER JOIN ARTISTS ON API_AUDIO.artist_id=ARTISTS._id) LEFT OUTER JOIN ALBUMS ON API_AUDIO.album_id=ALBUMS._id;"

    return-object v0
.end method

.method static synthetic z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748
    const-string v0, "CREATE VIEW IF NOT EXISTS CROSS_DEVICE_IMAGES AS SELECT * FROM IMAGES GROUP BY burst_dup_id HAVING device_priority = MIN(device_priority);"

    return-object v0
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 2063
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->W()Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;

    move-result-object v0

    .line 2065
    invoke-virtual {v0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2067
    const-string v1, "FILES_DEVICE_ID_IDX"

    const-string v2, "FILES"

    const-string v3, "device_id"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2068
    const-string v1, "FILES_SRC_MDA_IDX"

    const-string v2, "FILES"

    const-string v3, "source_media_id"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2069
    const-string v1, "FILES_DATE_ADDED_IDX"

    const-string v2, "FILES"

    const-string v3, "date_added"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2070
    const-string v1, "FILES_DATE_MOD_IDX"

    const-string v2, "FILES"

    const-string v3, "date_modified"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2071
    const-string v1, "FILES_DATE_TAKEN_IDX"

    const-string v2, "FILES"

    const-string v3, "datetaken"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2072
    const-string v1, "FILES_ARTIST_IDX"

    const-string v2, "FILES"

    const-string v3, "artist_id"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2073
    const-string v1, "FILES_ALBUM_IDX"

    const-string v2, "FILES"

    const-string v3, "album_id"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2074
    const-string v1, "FILES_MIME_TYPE_IDX"

    const-string v2, "FILES"

    const-string v3, "mime_type"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2075
    const-string v1, "FILES_SIZE_INDEX"

    const-string v2, "FILES"

    const-string v3, "_size"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2076
    const-string v1, "FILES_DUP_ID_IDX"

    const-string v2, "FILES"

    const-string v3, "dup_id"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2077
    const-string v1, "FILES_BURST_DUP_ID_IDX"

    const-string v2, "FILES"

    const-string v3, "burst_dup_id"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2078
    const-string v1, "FILES_BURST_ID_IDX"

    const-string v2, "FILES"

    const-string v3, "burst_id"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2080
    const-string v1, "CREATE INDEX IF NOT EXISTS ALBUMS_DEVICE_IDX ON ALBUMS(album);"

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2082
    const-string v1, "CREATE INDEX IF NOT EXISTS ARTISTS_DEVICE_INDEX ON ARTISTS(artist);"

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2084
    const-string v1, "CREATE INDEX IF NOT EXISTS GENRES_DEVICE_IDX ON GENRES(name);"

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2086
    const-string v1, "GENRES_MAP_AUDIO_ID_IDX"

    const-string v2, "GENRE_MEMBERS"

    const-string v3, "audio_id"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2092
    const-string v1, "GENRES_MAP_GENRE_ID_IDX"

    const-string v2, "GENRE_MEMBERS"

    const-string v3, "genre_id"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2099
    const-string v1, "KEYWORDS_IDX"

    const-string v2, "KEYWORDS"

    const-string v3, "keyword"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2101
    const-string v1, "KEYWORD_MAP_FILE_ID_IDX"

    const-string v2, "KEYWORD_MAP"

    const-string v3, "file_id"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2104
    const-string v1, "KEYWORD_MAP_KEYWORD_ID_IDX"

    const-string v2, "KEYWORD_MAP"

    const-string v3, "keyword_id"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2108
    const-string v1, "CREATE TRIGGER IF NOT EXISTS on_file_delete AFTER DELETE ON FILES FOR EACH ROW BEGIN DELETE FROM GENRE_MEMBERS WHERE audio_id=OLD._id; DELETE FROM KEYWORD_MAP WHERE file_id=OLD._id; END;"

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2109
    const-string v1, "CREATE TRIGGER IF NOT EXISTS device_delete AFTER DELETE ON DEVICES FOR EACH ROW BEGIN DELETE FROM FILES WHERE device_id=OLD._id; END;"

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2110
    const-string v1, "CREATE TRIGGER IF NOT EXISTS genre_delete AFTER DELETE ON GENRES FOR EACH ROW BEGIN DELETE FROM GENRE_MEMBERS WHERE genre_id=OLD._id; END;"

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2111
    const-string v1, "CREATE TRIGGER IF NOT EXISTS keyword_delete AFTER DELETE ON KEYWORDS FOR EACH ROW BEGIN DELETE FROM KEYWORD_MAP WHERE keyword_id=OLD._id; END;"

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2113
    const-string v1, "GEO_LOCATION_GEO_LOC_LOCALE_IDX"

    const-string v2, "GEO_LOCATION"

    const-string v3, "geo_loc_locale"

    invoke-static {v1, v2, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2117
    const-string v1, "files_table_validity"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2118
    const-string v1, "image_journal_synced"

    const-string v2, "0"

    invoke-static {p1, v1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2119
    const-string v1, "image_journal_dropped"

    const-string v2, "0"

    invoke-static {p1, v1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2121
    const-string v1, "audio_journal_synced"

    const-string v2, "0"

    invoke-static {p1, v1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2122
    const-string v1, "audio_journal_dropped"

    const-string v2, "0"

    invoke-static {p1, v1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2124
    const-string v1, "video_journal_synced"

    const-string v2, "0"

    invoke-static {p1, v1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2125
    const-string v1, "video_journal_dropped"

    const-string v2, "0"

    invoke-static {p1, v1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2127
    const-string v1, "document_journal_synced"

    const-string v2, "0"

    invoke-static {p1, v1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2128
    const-string v1, "document_journal_dropped"

    const-string v2, "0"

    invoke-static {p1, v1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2130
    invoke-static {p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2132
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2134
    invoke-virtual {v0, p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V

    .line 2136
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/media/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/media/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/media/d;->b()V

    .line 2138
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.sdk.samsunglink.VersionChanged"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2139
    return-void
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4

    .prologue
    .line 2180
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Downgrading database from version {} to {}, which will destroy all old data"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2181
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->W()Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;

    move-result-object v0

    .line 2183
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2189
    const-string v2, "IMAGES"

    invoke-static {p1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->d(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2190
    const-string v2, "VIDEOS"

    invoke-static {p1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->d(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2191
    const-string v2, "AUDIO"

    invoke-static {p1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->d(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2192
    const-string v2, "DOCUMENTS"

    invoke-static {p1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->d(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2194
    invoke-virtual {v0, p1, v1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V

    .line 2195
    invoke-virtual {v0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2198
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2200
    const/16 v0, 0x4e35

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->setVersion(I)V

    .line 2201
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 9

    .prologue
    const/16 v8, 0x4e35

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2213
    if-ne p3, v1, :cond_0

    .line 2405
    :goto_0
    return-void

    .line 2217
    :cond_0
    const/16 v1, 0x4e20

    if-ge p2, v1, :cond_1

    .line 2219
    invoke-virtual {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    goto :goto_0

    .line 2227
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->W()Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;

    move-result-object v1

    .line 2228
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2241
    const/16 v3, 0x4e22

    if-ge p2, v3, :cond_2

    .line 2242
    const-string v3, "FILES"

    const-string v4, "geo_loc_last_timestamp INTEGER"

    invoke-static {p1, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2244
    const-string v3, "FILES"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2245
    const-string v3, "ALL_MEDIA"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2246
    const-string v3, "IMAGES"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2247
    const-string v3, "VIDEOS"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2249
    invoke-static {p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2252
    :cond_2
    const/16 v3, 0x4e23

    if-ge p2, v3, :cond_3

    .line 2253
    const-string v3, "ARCHIVED_MEDIA"

    invoke-virtual {v1, p1, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2256
    :cond_3
    const/16 v3, 0x4e24

    if-ge p2, v3, :cond_4

    .line 2257
    const-string v3, "DEVICES"

    const-string v4, "supports_auto_archive INTEGER"

    invoke-static {p1, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2259
    const-string v3, "DEVICES"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2262
    :cond_4
    const/16 v3, 0x4e25

    if-ge p2, v3, :cond_5

    .line 2263
    const-string v3, "DEVICES"

    const-string v4, "registration_date INTEGER"

    invoke-static {p1, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2265
    const-string v3, "DEVICES"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2266
    const-string v3, "API_DEVICES"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2269
    :cond_5
    const/16 v3, 0x4e26

    if-ge p2, v3, :cond_6

    .line 2270
    const-string v3, "DEVICES"

    const-string v4, "supports_threebox_transfer INTEGER"

    invoke-static {p1, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2272
    const-string v3, "DEVICES"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2275
    :cond_6
    const/16 v3, 0x4e27

    if-ge p2, v3, :cond_7

    .line 2276
    const-string v3, "DEVICES"

    const-string v4, "is_remote_wakeup_support INTEGER"

    invoke-static {p1, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2278
    const-string v3, "DEVICES"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2279
    const-string v3, "API_DEVICES"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2282
    :cond_7
    const/16 v3, 0x4e28

    if-ge p2, v3, :cond_8

    .line 2283
    const-string v3, "FILE_TRANSFER_SESSIONS"

    const-string v4, "currentTransferSize INTEGER"

    invoke-static {p1, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2286
    const-string v3, "FILE_TRANSFER_SESSIONS"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2289
    :cond_8
    const/16 v3, 0x4e29

    if-ge p2, v3, :cond_9

    .line 2290
    invoke-static {p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2293
    :cond_9
    const/16 v3, 0x4e2a

    if-ge p2, v3, :cond_a

    .line 2294
    const-string v3, "DEVICES"

    const-string v4, "smallest_drive_capacity INTEGER"

    invoke-static {p1, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2296
    const-string v3, "DEVICES"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2297
    const-string v3, "API_DEVICES"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2300
    :cond_a
    const/16 v3, 0x4e2b

    if-ge p2, v3, :cond_b

    .line 2301
    const-string v3, "DEVICES"

    const-string v4, "host_peer_id TEXT"

    invoke-static {p1, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2303
    const-string v3, "DEVICES"

    invoke-virtual {v1, v3, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2306
    :cond_b
    const/16 v3, 0x4e2c

    if-ge p2, v3, :cond_d

    .line 2307
    const/16 v3, 0x9

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "geo_loc_country"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "geo_loc_province"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "geo_loc_sub_province"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "geo_loc_locality"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "geo_loc_sub_locality"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "geo_loc_thoroughfare"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "geo_loc_sub_thoroughfare"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "geo_loc_premises"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "geo_loc_feature"

    aput-object v5, v3, v4

    .line 2317
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2319
    :try_start_1
    array-length v4, v3

    :goto_1
    if-ge v0, v4, :cond_c

    aget-object v5, v3, v0

    .line 2320
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "UPDATE FILES SET "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=NULL WHERE "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=\'null\';"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2319
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2322
    :cond_c
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2324
    :try_start_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2328
    :cond_d
    const/16 v0, 0x4e2d

    if-ge p2, v0, :cond_e

    .line 2329
    const-string v0, "ALBUM_DETAIL"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2330
    const-string v0, "API_ALBUM_DETAIL"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2333
    :cond_e
    const/16 v0, 0x4e2e

    if-ge p2, v0, :cond_f

    .line 2334
    const-string v0, "GENRES_AGGREGATE"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2337
    :cond_f
    const/16 v0, 0x4e2f

    if-ge p2, v0, :cond_10

    .line 2338
    const-string v0, "API_GENRES_AGGREGATE"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2341
    :cond_10
    const/16 v0, 0x4e30

    if-ge p2, v0, :cond_11

    .line 2342
    const-string v0, "FILES"

    const-string v3, "geo_is_valid_map_coord INTEGER"

    invoke-static {p1, v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2344
    const-string v0, "FILES"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2345
    const-string v0, "ALL_MEDIA"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2346
    const-string v0, "IMAGES"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2347
    const-string v0, "VIDEOS"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Ljava/lang/String;Ljava/util/ArrayList;)I

    .line 2350
    :cond_11
    const/16 v0, 0x4e31

    if-ge p2, v0, :cond_12

    .line 2351
    const-string v0, "GEO_LOCATION"

    invoke-virtual {v1, p1, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2353
    const-string v0, "GEO_LOCATION_GEO_LOC_LOCALE_IDX"

    const-string v3, "GEO_LOCATION"

    const-string v4, "geo_loc_locale"

    invoke-static {v0, v3, v4}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 2360
    invoke-static {p1}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2363
    :cond_12
    const/16 v0, 0x4e32

    if-ge p2, v0, :cond_13

    .line 2364
    const-string v0, "FILES"

    const-string v3, "thumb_data TEXT"

    invoke-static {p1, v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2365
    const-string v0, "FILES"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2369
    :cond_13
    const/16 v0, 0x4e33

    if-ge p2, v0, :cond_14

    .line 2370
    const-string v0, "FILES"

    const-string v3, "thumb_width INTEGER"

    invoke-static {p1, v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2371
    const-string v0, "FILES"

    const-string v3, "thumb_height INTEGER"

    invoke-static {p1, v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2372
    const-string v0, "FILES"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2376
    :cond_14
    const/16 v0, 0x4e34

    if-ge p2, v0, :cond_15

    .line 2377
    const-string v0, "DEVICES"

    const-string v3, "mac_bt TEXT"

    invoke-static {p1, v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2378
    const-string v0, "DEVICES"

    const-string v3, "mac_ble TEXT"

    invoke-static {p1, v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2379
    const-string v0, "DEVICES"

    const-string v3, "mac_wifi TEXT"

    invoke-static {p1, v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2380
    const-string v0, "DEVICES"

    const-string v3, "mac_wifi_direct TEXT"

    invoke-static {p1, v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2381
    const-string v0, "DEVICES"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2385
    :cond_15
    if-ge p2, v8, :cond_16

    .line 2386
    const-string v0, "DEVICES"

    const-string v3, "network_mode_sort INTEGER"

    invoke-static {p1, v0, v3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 2387
    const-string v0, "DEVICES"

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2395
    :cond_16
    invoke-virtual {v1, p1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V

    .line 2396
    invoke-virtual {v1, p1, v2}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b$a;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)V

    .line 2399
    const/16 v0, 0x4e35

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->setVersion(I)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 2400
    :catch_0
    move-exception v0

    .line 2401
    invoke-static {}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->f()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Failed to upgrade database, will perform wipe."

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2402
    invoke-virtual {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$b;->onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    goto/16 :goto_0

    .line 2324
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
.end method
