.class final Lcom/mfluent/asp/datamodel/v$1;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/datamodel/v;->a_(Lcom/mfluent/asp/datamodel/ao$a;J)Landroid/os/ParcelFileDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/io/InputStream;

.field final synthetic b:Ljava/io/OutputStream;

.field final synthetic c:Lcom/mfluent/asp/datamodel/v;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/datamodel/v;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/v$1;->c:Lcom/mfluent/asp/datamodel/v;

    iput-object p2, p0, Lcom/mfluent/asp/datamodel/v$1;->a:Ljava/io/InputStream;

    iput-object p3, p0, Lcom/mfluent/asp/datamodel/v$1;->b:Ljava/io/OutputStream;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 159
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/v$1;->a:Ljava/io/InputStream;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/v$1;->b:Ljava/io/OutputStream;

    invoke-static {v0, v1}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/v$1;->a:Ljava/io/InputStream;

    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 164
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/v$1;->b:Ljava/io/OutputStream;

    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 165
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v0

    .line 161
    :try_start_1
    invoke-static {}, Lcom/mfluent/asp/datamodel/v;->g()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Trouble piping device icon"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/v$1;->a:Ljava/io/InputStream;

    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 164
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/v$1;->b:Ljava/io/OutputStream;

    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/v$1;->a:Ljava/io/InputStream;

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 164
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/v$1;->b:Ljava/io/OutputStream;

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    throw v0
.end method
