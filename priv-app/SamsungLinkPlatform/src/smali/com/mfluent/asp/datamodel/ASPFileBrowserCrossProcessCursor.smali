.class public final Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;
.super Lcom/mfluent/asp/datamodel/k;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$1;,
        Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;
    }
.end annotation


# static fields
.field private static final i:[Ljava/lang/String;

.field private static j:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final k:[Ljava/lang/String;

.field private final l:[Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

.field private final m:Ljava/lang/String;

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "document_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "local_data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "last_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "home_sync_flags"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "icon"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->i:[Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 53
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/k;-><init>(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;B)V

    .line 49
    iput v6, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->n:I

    .line 54
    if-nez p5, :cond_0

    .line 55
    sget-object p5, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->i:[Ljava/lang/String;

    .line 57
    :cond_0
    iput-object p5, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->k:[Ljava/lang/String;

    .line 59
    array-length v0, p5

    new-array v0, v0, [Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    :goto_0
    array-length v1, p5

    if-ge v6, v1, :cond_9

    aget-object v1, p5, v6

    const-string v2, "document_id"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->a:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    const-string v2, "local_data"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->b:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_2
    const-string v2, "_display_name"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->c:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_3
    const-string v2, "home_sync_flags"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->g:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_4
    const-string v2, "icon"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->h:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_5
    const-string v2, "last_modified"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->e:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_6
    const-string v2, "mime_type"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->d:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_7
    const-string v2, "_size"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->f:Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aput-object v1, v0, v6

    goto :goto_1

    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported column: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->l:[Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "file_browser_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->m:Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eqz v0, :cond_a

    .line 63
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->a:Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v1

    invoke-interface {v1}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getFileListUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 68
    :cond_a
    return-void
.end method


# virtual methods
.method public final fillWindow(ILandroid/database/CursorWindow;)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 114
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->m:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->clearOrCreateWindow(Ljava/lang/String;)V

    .line 116
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    iget-object v3, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->l:[Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    array-length v3, v3

    invoke-virtual {v2, v3}, Landroid/database/CursorWindow;->setNumColumns(I)Z

    .line 117
    iget v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->n:I

    invoke-static {p1, v2}, Landroid/database/DatabaseUtils;->cursorPickFillWindowStartPosition(II)I

    move-result v7

    .line 118
    iget v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->n:I

    add-int v3, v7, v2

    .line 119
    const/4 v2, 0x0

    .line 120
    iget-object v4, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eqz v4, :cond_a

    .line 121
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v2

    .line 122
    if-ne v3, v7, :cond_9

    .line 123
    invoke-interface {v2}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCount()I

    move-result v3

    move-object v4, v2

    move v2, v3

    .line 126
    :goto_0
    iget-object v3, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v3, v7}, Landroid/database/CursorWindow;->setStartPosition(I)V

    .line 128
    if-nez v4, :cond_8

    .line 197
    :cond_0
    :goto_1
    return-void

    .line 138
    :cond_1
    invoke-interface {v4, v7}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getFileNonBlocking(I)Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v3

    move v5, v6

    .line 140
    :goto_2
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->l:[Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    array-length v2, v2

    if-ge v5, v2, :cond_7

    .line 143
    if-nez v3, :cond_2

    .line 144
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v2, v7, v5}, Landroid/database/CursorWindow;->putNull(II)Z

    move-result v2

    .line 189
    :goto_3
    if-nez v2, :cond_6

    .line 190
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v2}, Landroid/database/CursorWindow;->freeLastRow()V

    .line 191
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v2}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v2

    iput v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->n:I

    move v2, v7

    .line 132
    :goto_4
    add-int/lit8 v7, v7, 0x1

    move v8, v2

    :goto_5
    if-ge v7, v8, :cond_0

    .line 133
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v2}, Landroid/database/CursorWindow;->allocRow()Z

    move-result v2

    if-nez v2, :cond_1

    .line 134
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v2}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v2

    iput v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->n:I

    goto :goto_1

    .line 148
    :cond_2
    :try_start_0
    invoke-static {v3}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;)Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v2

    .line 149
    sget-object v9, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$1;->a:[I

    iget-object v10, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->l:[Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;

    aget-object v10, v10, v5

    invoke-virtual {v10}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor$FieldValues;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    move v2, v6

    goto :goto_3

    .line 151
    :pswitch_0
    invoke-virtual {p0, v3}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->b(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v7, v5}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_3

    .line 154
    :pswitch_1
    instance-of v2, v3, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    if-eqz v2, :cond_3

    .line 155
    move-object v0, v3

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v7, v5}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_3

    .line 157
    :cond_3
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-virtual {v2, v7, v5}, Landroid/database/CursorWindow;->putNull(II)Z

    move-result v2

    goto :goto_3

    .line 161
    :pswitch_2
    invoke-virtual {p0, v3, v2}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v7, v5}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_3

    .line 164
    :pswitch_3
    iget-object v9, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFile;->isSecure()Z

    move-result v2

    if-eqz v2, :cond_4

    const-wide/16 v10, 0x1

    :goto_6
    invoke-virtual {v9, v10, v11, v7, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto :goto_3

    :cond_4
    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFile;->isPersonal()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x2

    :goto_7
    int-to-long v10, v2

    goto :goto_6

    :cond_5
    move v2, v6

    goto :goto_7

    .line 171
    :pswitch_4
    invoke-static {v3, v2}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->b(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0, v2, v7, v5}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->a(Ljava/lang/Long;II)Z

    move-result v2

    goto/16 :goto_3

    .line 174
    :pswitch_5
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFile;->lastModified()J

    move-result-wide v10

    invoke-virtual {v2, v10, v11, v7, v5}, Landroid/database/CursorWindow;->putLong(JII)Z

    move-result v2

    goto/16 :goto_3

    .line 177
    :pswitch_6
    invoke-static {v3}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->f(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v7, v5}, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->a(Ljava/lang/String;II)Z

    move-result v2

    goto/16 :goto_3

    .line 180
    :pswitch_7
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->mWindow:Landroid/database/CursorWindow;

    invoke-interface {v3}, Lcom/mfluent/asp/common/datamodel/ASPFile;->length()J

    move-result-wide v10

    invoke-virtual {v2, v10, v11, v7, v5}, Landroid/database/CursorWindow;->putLong(JII)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto/16 :goto_3

    .line 183
    :catch_0
    move-exception v2

    .line 184
    new-instance v3, Landroid/database/StaleDataException;

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/database/StaleDataException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 140
    :cond_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_2

    :cond_7
    move v2, v8

    goto/16 :goto_4

    :cond_8
    move v8, v2

    goto/16 :goto_5

    :cond_9
    move-object v4, v2

    move v2, v3

    goto/16 :goto_0

    :cond_a
    move-object v4, v2

    move v2, v3

    goto/16 :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->k:[Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPFileBrowserCrossProcessCursor;->b:Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v0

    invoke-interface {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCount()I

    move-result v0

    .line 108
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic onDeactivateOrClose()V
    .locals 0

    .prologue
    .line 21
    invoke-super {p0}, Lcom/mfluent/asp/datamodel/k;->onDeactivateOrClose()V

    return-void
.end method

.method public final bridge synthetic onMove(II)Z
    .locals 1

    .prologue
    .line 21
    invoke-super {p0, p1, p2}, Lcom/mfluent/asp/datamodel/k;->onMove(II)Z

    move-result v0

    return v0
.end method
