.class public final Lcom/mfluent/asp/datamodel/filebrowser/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SdCardPath"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/filebrowser/i$4;,
        Lcom/mfluent/asp/datamodel/filebrowser/i$b;,
        Lcom/mfluent/asp/datamodel/filebrowser/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
        "<",
        "Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

.field private e:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/io/FileFilter;

.field private final h:Ljava/util/concurrent/locks/Lock;

.field private final i:Ljava/util/concurrent/locks/Condition;

.field private j:Z

.field private k:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

.field private l:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field private m:Landroid/database/DataSetObserver;

.field private n:Landroid/os/FileObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 59
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->b:Ljava/util/Set;

    .line 61
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->c:Ljava/util/Set;

    .line 64
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->b:Ljava/util/Set;

    const-string v1, "/sdcard"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->b:Ljava/util/Set;

    const-string v1, "/acct"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 66
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->b:Ljava/util/Set;

    const-string v1, "/mnt/asec"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 67
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->b:Ljava/util/Set;

    const-string v1, "/mnt/obb"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 68
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->b:Ljava/util/Set;

    const-string v1, "/mnt/secure"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-static {}, Lcom/mfluent/a/a/b;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->b:Ljava/util/Set;

    const-string v1, "/storage/emulated/legacy"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 74
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->b:Ljava/util/Set;

    const-string v1, "/storage/sdcard0"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_0
    :try_start_0
    const-string v0, "com.samsung.android.secretmode.SecretModeManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 79
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/secretmode/SecretModeManager;->getSecretDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 81
    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/i;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :cond_1
    :goto_0
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->c:Ljava/util/Set;

    const-string v1, "/storage/container"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 89
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->c:Ljava/util/Set;

    const-string v1, "/system"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 90
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->c:Ljava/util/Set;

    const-string v1, "/sys"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->c:Ljava/util/Set;

    const-string v1, "/proc"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 92
    return-void

    .line 83
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    .line 100
    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->g:Ljava/io/FileFilter;

    .line 102
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->h:Ljava/util/concurrent/locks/Lock;

    .line 104
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->i:Ljava/util/concurrent/locks/Condition;

    .line 113
    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->m:Landroid/database/DataSetObserver;

    .line 647
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;
    .locals 2

    .prologue
    .line 118
    invoke-static {p0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x0

    .line 129
    :goto_0
    return-object v0

    .line 122
    :cond_0
    const-string v0, "ROOT"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    invoke-static {}, Lcom/mfluent/a/a/b;->c()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 129
    :goto_1
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;-><init>(Ljava/io/File;)V

    move-object v0, v1

    goto :goto_0

    .line 126
    :cond_1
    new-instance v1, Ljava/lang/String;

    const/16 v0, 0xa

    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    .line 127
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a()Ljava/io/File;
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lcom/mfluent/a/a/b;->c()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/datamodel/filebrowser/i;)Ljava/io/FileFilter;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->g:Ljava/io/FileFilter;

    return-object v0
.end method

.method public static a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 135
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/16 v2, 0xa

    invoke-static {v0, v2}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    return-object v1
.end method

.method public static a(Ljava/io/File;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 350
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 351
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->c:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->b:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 362
    :goto_0
    return v0

    .line 354
    :cond_1
    const-class v0, Lcom/mfluent/a/a/b;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/a/a/b;

    invoke-virtual {v0}, Lcom/mfluent/a/a/b;->a()[Landroid/os/storage/StorageVolume;

    move-result-object v4

    array-length v5, v4

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_3

    aget-object v6, v4, v0

    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    invoke-static {v3}, Lcom/sec/pcw/service/d/a;->a(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v2, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    move v0, v1

    .line 356
    goto :goto_0

    .line 354
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    .line 357
    :cond_5
    invoke-static {}, Lcom/mfluent/a/a/b;->c()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 359
    goto :goto_0

    :cond_6
    move v0, v2

    .line 362
    goto :goto_0
.end method

.method static synthetic b(Lcom/mfluent/asp/datamodel/filebrowser/i;)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 261
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 283
    :cond_0
    :goto_0
    return v0

    .line 264
    :cond_1
    invoke-static {}, Lcom/mfluent/a/a/b;->c()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 265
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    .line 266
    invoke-static {}, Lcom/sec/pcw/service/d/a;->a()Ljava/io/File;

    move-result-object v3

    .line 268
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 269
    const-string v1, "LoFileBr"

    const-string v2, "checkNeedFiltering parent path true"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    .line 283
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 272
    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 273
    const-string v1, "LoFileBr"

    const-string v2, "checkNeedFiltering main path true"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    :cond_4
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 277
    const-string v1, "LoFileBr"

    const-string v2, "checkNeedFiltering ext path true"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method static synthetic c(Lcom/mfluent/asp/datamodel/filebrowser/i;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->m:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->m:Landroid/database/DataSetObserver;

    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    :cond_0
    return-void
.end method

.method static synthetic d()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/i;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method


# virtual methods
.method public final a(I)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 436
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lockInterruptibly()V

    .line 438
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->j:Z

    if-nez v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->i:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 445
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 443
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/datamodel/filebrowser/i;->b(I)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 445
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)V

    .line 145
    return-void
.end method

.method public final a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 154
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lockInterruptibly()V

    .line 157
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->j:Z

    .line 159
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 161
    if-nez p1, :cond_0

    .line 162
    new-instance p1, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-static {}, Lcom/mfluent/a/a/b;->g()Ljava/io/File;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;-><init>(Ljava/io/File;)V

    .line 165
    :cond_0
    invoke-static {}, Lcom/mfluent/a/a/b;->c()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    .line 166
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 167
    invoke-static {}, Lcom/sec/pcw/service/d/a;->a()Ljava/io/File;

    move-result-object v3

    .line 168
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v4

    .line 169
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    .line 170
    invoke-virtual {v4, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 171
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->e:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    .line 179
    :goto_0
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->l:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 181
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->g:Ljava/io/FileFilter;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->e:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    if-nez v1, :cond_8

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/sec/pcw/service/d/a;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/pcw/service/d/a;->a()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-static {}, Lcom/sec/pcw/service/d/a;->a()Ljava/io/File;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    :cond_1
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->e:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/i$4;->a:[I

    invoke-virtual {p2}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$a;

    new-instance v2, Lcom/mfluent/asp/datamodel/filebrowser/i$b;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/mfluent/asp/datamodel/filebrowser/i$b;-><init>(Z)V

    invoke-direct {v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/i$a;-><init>(Ljava/util/Comparator;)V

    :goto_1
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-static {v2, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 183
    :cond_2
    new-instance v1, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;-><init>(I)V

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->k:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->k:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->setIsDirectory(I)V

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 172
    :cond_4
    invoke-virtual {v4, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v4, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 173
    :cond_5
    iget-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {v4, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->INTERNAL_STORAGE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    :goto_3
    invoke-virtual {v3, v1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    .line 174
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-direct {v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;-><init>(Ljava/io/File;)V

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->e:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 187
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 173
    :cond_6
    :try_start_1
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->EXTERNAL_STORAGE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    goto :goto_3

    .line 176
    :cond_7
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 177
    new-instance v2, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-direct {v2, v1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;-><init>(Ljava/io/File;)V

    iput-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->e:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    goto/16 :goto_0

    .line 181
    :cond_8
    invoke-static {p3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/datamodel/filebrowser/i$1;-><init>(Lcom/mfluent/asp/datamodel/filebrowser/i;)V

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->g:Ljava/io/FileFilter;

    :goto_4
    if-eqz p4, :cond_9

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/datamodel/filebrowser/i;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_9
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->g:Ljava/io/FileFilter;

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v1

    move-object v2, v1

    :goto_5
    if-eqz v2, :cond_1

    array-length v3, v2

    move v1, v0

    :goto_6
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    iget-object v5, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_a
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$2;

    invoke-direct {v1, p0, p3}, Lcom/mfluent/asp/datamodel/filebrowser/i$2;-><init>(Lcom/mfluent/asp/datamodel/filebrowser/i;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->g:Ljava/io/FileFilter;

    goto :goto_4

    :cond_b
    const-string v1, "LoFileBr"

    const-string v2, "no filter applied"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    move-object v2, v1

    goto :goto_5

    .line 182
    :pswitch_0
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$a;

    new-instance v2, Lorg/apache/commons/io/comparator/CompositeFileComparator;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/util/Comparator;

    const/4 v4, 0x0

    sget-object v5, Lorg/apache/commons/io/comparator/LastModifiedFileComparator;->LASTMODIFIED_COMPARATOR:Ljava/util/Comparator;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lcom/mfluent/asp/datamodel/filebrowser/i$b;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/mfluent/asp/datamodel/filebrowser/i$b;-><init>(Z)V

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Lorg/apache/commons/io/comparator/CompositeFileComparator;-><init>([Ljava/util/Comparator;)V

    invoke-direct {v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/i$a;-><init>(Ljava/util/Comparator;)V

    goto/16 :goto_1

    :pswitch_1
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$a;

    new-instance v2, Lorg/apache/commons/io/comparator/CompositeFileComparator;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/util/Comparator;

    const/4 v4, 0x0

    sget-object v5, Lorg/apache/commons/io/comparator/LastModifiedFileComparator;->LASTMODIFIED_REVERSE:Ljava/util/Comparator;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lcom/mfluent/asp/datamodel/filebrowser/i$b;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/mfluent/asp/datamodel/filebrowser/i$b;-><init>(Z)V

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Lorg/apache/commons/io/comparator/CompositeFileComparator;-><init>([Ljava/util/Comparator;)V

    invoke-direct {v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/i$a;-><init>(Ljava/util/Comparator;)V

    goto/16 :goto_1

    :pswitch_2
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$a;

    new-instance v2, Lorg/apache/commons/io/comparator/CompositeFileComparator;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/util/Comparator;

    const/4 v4, 0x0

    sget-object v5, Lorg/apache/commons/io/comparator/SizeFileComparator;->SIZE_COMPARATOR:Ljava/util/Comparator;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lcom/mfluent/asp/datamodel/filebrowser/i$b;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/mfluent/asp/datamodel/filebrowser/i$b;-><init>(Z)V

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Lorg/apache/commons/io/comparator/CompositeFileComparator;-><init>([Ljava/util/Comparator;)V

    invoke-direct {v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/i$a;-><init>(Ljava/util/Comparator;)V

    goto/16 :goto_1

    :pswitch_3
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$a;

    new-instance v2, Lorg/apache/commons/io/comparator/CompositeFileComparator;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/util/Comparator;

    const/4 v4, 0x0

    sget-object v5, Lorg/apache/commons/io/comparator/SizeFileComparator;->SIZE_REVERSE:Ljava/util/Comparator;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lcom/mfluent/asp/datamodel/filebrowser/i$b;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/mfluent/asp/datamodel/filebrowser/i$b;-><init>(Z)V

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Lorg/apache/commons/io/comparator/CompositeFileComparator;-><init>([Ljava/util/Comparator;)V

    invoke-direct {v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/i$a;-><init>(Ljava/util/Comparator;)V

    goto/16 :goto_1

    :pswitch_4
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$a;

    new-instance v2, Lcom/mfluent/asp/datamodel/filebrowser/i$b;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/mfluent/asp/datamodel/filebrowser/i$b;-><init>(Z)V

    invoke-direct {v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/i$a;-><init>(Ljava/util/Comparator;)V

    goto/16 :goto_1

    :pswitch_5
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$a;

    new-instance v2, Lcom/mfluent/asp/datamodel/filebrowser/i$b;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/mfluent/asp/datamodel/filebrowser/i$b;-><init>(Z)V

    invoke-direct {v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/i$a;-><init>(Ljava/util/Comparator;)V

    goto/16 :goto_1

    :pswitch_6
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$a;

    new-instance v2, Lorg/apache/commons/io/comparator/CompositeFileComparator;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/util/Comparator;

    const/4 v4, 0x0

    sget-object v5, Lorg/apache/commons/io/comparator/ExtensionFileComparator;->EXTENSION_INSENSITIVE_COMPARATOR:Ljava/util/Comparator;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lcom/mfluent/asp/datamodel/filebrowser/i$b;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/mfluent/asp/datamodel/filebrowser/i$b;-><init>(Z)V

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Lorg/apache/commons/io/comparator/CompositeFileComparator;-><init>([Ljava/util/Comparator;)V

    invoke-direct {v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/i$a;-><init>(Ljava/util/Comparator;)V

    goto/16 :goto_1

    :pswitch_7
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$a;

    new-instance v2, Lorg/apache/commons/io/comparator/CompositeFileComparator;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/util/Comparator;

    const/4 v4, 0x0

    sget-object v5, Lorg/apache/commons/io/comparator/ExtensionFileComparator;->EXTENSION_INSENSITIVE_REVERSE:Ljava/util/Comparator;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lcom/mfluent/asp/datamodel/filebrowser/i$b;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/mfluent/asp/datamodel/filebrowser/i$b;-><init>(Z)V

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Lorg/apache/commons/io/comparator/CompositeFileComparator;-><init>([Ljava/util/Comparator;)V

    invoke-direct {v1, v2}, Lcom/mfluent/asp/datamodel/filebrowser/i$a;-><init>(Ljava/util/Comparator;)V

    goto/16 :goto_1

    .line 184
    :cond_c
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->j:Z

    .line 185
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->i:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 188
    return-void

    .line 182
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final b()Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    return-object v0
.end method

.method public final b(I)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;
    .locals 3

    .prologue
    .line 455
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 456
    const/4 v0, 0x0

    .line 469
    :goto_0
    return-object v0

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 459
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-direct {v1, v0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;-><init>(Ljava/io/File;)V

    .line 460
    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->e:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    if-nez v2, :cond_1

    .line 461
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    .line 463
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 464
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->INTERNAL_STORAGE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    :cond_1
    :goto_1
    move-object v0, v1

    .line 469
    goto :goto_0

    .line 465
    :cond_2
    invoke-static {}, Lcom/sec/pcw/service/d/a;->a()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 466
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->EXTERNAL_STORAGE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    goto :goto_1
.end method

.method public final c()Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->e:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    return-object v0
.end method

.method public final deselectAll()V
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->k:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->deselectAll()V

    .line 506
    return-void
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 204
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    return-object v0
.end method

.method public final getCurrentDirectoryAbsolutePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    if-nez v0, :cond_0

    .line 416
    const-string v0, ""

    .line 418
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->d:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic getFile(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(I)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getFileNonBlocking(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/datamodel/filebrowser/i;->b(I)Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getParentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->e:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    return-object v0
.end method

.method public final getSelectedFileIndexes()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 532
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->k:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    if-nez v0, :cond_0

    .line 533
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 534
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 537
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->k:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->getSelectedFileIndexes()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic init(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    check-cast p1, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/filebrowser/i;->a(Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    return-void
.end method

.method public final isAllSelected()Z
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->k:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->isAllSelected()Z

    move-result v0

    return v0
.end method

.method public final isSelected(I)Z
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->k:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->isSelected(I)Z

    move-result v0

    return v0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 2

    .prologue
    .line 551
    if-nez p1, :cond_0

    .line 552
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/filebrowser/i;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 593
    :goto_0
    return-void

    .line 556
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->m:Landroid/database/DataSetObserver;

    .line 558
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->n:Landroid/os/FileObserver;

    if-eqz v0, :cond_1

    .line 559
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->n:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    .line 562
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/i;->getCurrentDirectoryAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 564
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/i$3;

    invoke-direct {v1, p0, v0}, Lcom/mfluent/asp/datamodel/filebrowser/i$3;-><init>(Lcom/mfluent/asp/datamodel/filebrowser/i;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->n:Landroid/os/FileObserver;

    .line 592
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->n:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->startWatching()V

    goto :goto_0
.end method

.method public final selectAll()V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->k:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectAll()V

    .line 497
    return-void
.end method

.method public final setSelected(IZ)V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->k:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->setSelected(IZ)V

    .line 479
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 542
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LocalFileBrowser: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/i;->getCurrentDirectoryAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", root: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->e:Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", files: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/i;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 601
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->n:Landroid/os/FileObserver;

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->n:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    .line 603
    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->n:Landroid/os/FileObserver;

    .line 605
    :cond_0
    iput-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/i;->m:Landroid/database/DataSetObserver;

    .line 606
    return-void
.end method
