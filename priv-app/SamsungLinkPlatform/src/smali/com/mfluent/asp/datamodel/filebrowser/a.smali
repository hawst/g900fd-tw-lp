.class public final Lcom/mfluent/asp/datamodel/filebrowser/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/filebrowser/a$4;,
        Lcom/mfluent/asp/datamodel/filebrowser/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
        "<",
        "Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/mfluent/asp/datamodel/Device;

.field private final f:Landroid/content/Context;

.field private g:I

.field private h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

.field private i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

.field private j:Ljava/lang/String;

.field private final k:Ljava/util/concurrent/locks/Lock;

.field private l:Z

.field private m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

.field private final n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/database/DataSetObserver;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field private p:J

.field private q:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:Ljava/text/Collator;

.field private u:Lcom/mfluent/asp/datamodel/filebrowser/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 70
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/a$1;

    invoke-direct {v0}, Lcom/mfluent/asp/datamodel/filebrowser/a$1;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/a;->b:Ljava/util/Comparator;

    .line 78
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/a$2;

    invoke-direct {v0}, Lcom/mfluent/asp/datamodel/filebrowser/a$2;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/filebrowser/a;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    .line 156
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    .line 162
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->n:Ljava/util/Set;

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->o:Z

    .line 179
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->e:Lcom/mfluent/asp/datamodel/Device;

    .line 180
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->f:Landroid/content/Context;

    .line 181
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 63
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->isDirectory()Z

    move-result v2

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->isDirectory()Z

    move-result v3

    if-eqz v2, :cond_1

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez v2, :cond_2

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->i()Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    move-result-object v3

    if-nez v2, :cond_0

    if-eqz v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a()Ljava/text/CollationKey;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a()Ljava/text/CollationKey;

    move-result-object v3

    if-nez v2, :cond_4

    if-eqz v3, :cond_0

    :cond_4
    if-eqz v2, :cond_0

    if-nez v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {v2, v3}, Ljava/text/CollationKey;->compareTo(Ljava/text/CollationKey;)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/datamodel/filebrowser/a;)Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->e:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method static a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 184
    invoke-static {p1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-object v0

    .line 188
    :cond_1
    const-string v1, "ROOT"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 189
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-eq v1, v2, :cond_0

    .line 192
    const-string v0, "mycomputer"

    .line 196
    :goto_1
    const-string v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 197
    new-instance v2, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct {v2}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    .line 198
    if-ltz v3, :cond_3

    .line 201
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->valueOf(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 205
    :goto_2
    invoke-virtual {v2, v1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V

    .line 206
    add-int/lit8 v1, v3, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(Ljava/lang/String;)V

    :goto_3
    move-object v0, v2

    .line 211
    goto :goto_0

    .line 194
    :cond_2
    new-instance v0, Ljava/lang/String;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_1

    .line 203
    :catch_0
    move-exception v1

    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->a:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    goto :goto_2

    .line 208
    :cond_3
    invoke-virtual {v2, v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private static a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 599
    if-nez p0, :cond_0

    .line 600
    const-string v0, ""

    .line 611
    :goto_0
    return-object v0

    .line 603
    :cond_0
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/a$4;->a:[I

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 611
    const-string v0, ""

    goto :goto_0

    .line 605
    :pswitch_0
    const-string v0, "personal"

    goto :goto_0

    .line 607
    :pswitch_1
    const-string v0, "secured"

    goto :goto_0

    .line 609
    :pswitch_2
    const-string v0, "shared"

    goto :goto_0

    .line 603
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method static a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v0

    .line 216
    if-nez v0, :cond_0

    .line 217
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->a:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    .line 219
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 873
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 947
    :goto_0
    return-object v4

    .line 881
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 882
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->getCount()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    if-le v5, v6, :cond_5

    move v6, v1

    .line 883
    :goto_1
    const-string v7, "status"

    if-eqz v6, :cond_6

    const-string v5, "EXCLUDE"

    :goto_2
    invoke-virtual {v2, v7, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 884
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 885
    invoke-direct {p0, v5, p1, v6}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lorg/json/JSONArray;Ljava/util/ArrayList;Z)V

    .line 886
    const-string v6, "items"

    invoke-virtual {v2, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 888
    const-string v5, "path"

    iget-object v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 889
    const-string v6, "isPersonal"

    iget-object v5, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v5

    sget-object v7, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->d:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    if-ne v5, v7, :cond_7

    move v5, v1

    :goto_3
    invoke-virtual {v2, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 891
    iget-object v5, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->e:Lcom/mfluent/asp/datamodel/Device;

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v5, v6}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 892
    const-string v5, "inRecycleBin"

    const/4 v6, 0x1

    invoke-virtual {v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move-object v6, v2

    move v2, v3

    .line 903
    :goto_4
    if-eqz v6, :cond_f

    .line 905
    :try_start_1
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    iget-object v7, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->e:Lcom/mfluent/asp/datamodel/Device;

    iget-object v5, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v5

    sget-object v8, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->c:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    if-ne v5, v8, :cond_8

    const-string v5, "/api/pCloud/device/secured/delete"

    :goto_5
    invoke-static {v7, v5, v6}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v5

    .line 907
    sget-object v6, Lcom/mfluent/asp/datamodel/filebrowser/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v6

    const/4 v7, 0x2

    if-gt v6, v7, :cond_2

    .line 908
    const-string v6, "mfl_ASP10FileBrowser"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "::sendDeleteRequest: Got json response: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    :cond_2
    const-string v6, "FAIL"

    const-string v7, "result"

    const-string v8, "OK"

    invoke-virtual {v5, v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 913
    const-string v2, "numOfUndeleted"

    const/4 v6, -0x1

    invoke-virtual {v5, v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    :goto_6
    move v5, v0

    .line 923
    :goto_7
    if-eqz v1, :cond_d

    .line 924
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v3

    .line 926
    :goto_8
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    if-ltz v5, :cond_3

    if-ge v1, v5, :cond_9

    .line 927
    :cond_3
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->b(I)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-result-object v0

    .line 928
    if-eqz v0, :cond_4

    .line 929
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 926
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    :cond_5
    move v6, v3

    .line 882
    goto/16 :goto_1

    .line 883
    :cond_6
    :try_start_2
    const-string v5, "INCLUDE"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    :cond_7
    move v5, v3

    .line 889
    goto/16 :goto_3

    .line 896
    :catch_0
    move-exception v2

    move-object v6, v4

    move v2, v1

    .line 897
    goto/16 :goto_4

    .line 905
    :cond_8
    :try_start_3
    const-string v5, "/api/pCloud/device/files/delete"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_5

    .line 915
    :catch_1
    move-exception v2

    .line 916
    const-string v5, "mfl_ASP10FileBrowser"

    const-string v6, "Error deleting files."

    invoke-static {v5, v6, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v5, v0

    .line 918
    goto :goto_7

    :cond_9
    move-object v0, v2

    .line 934
    :goto_9
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_c

    move-object v1, v4

    .line 939
    :goto_a
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/DataSetObserver;

    .line 940
    if-nez v1, :cond_a

    .line 941
    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    goto :goto_b

    .line 943
    :cond_a
    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onInvalidated()V

    goto :goto_b

    :cond_b
    move-object v4, v1

    .line 947
    goto/16 :goto_0

    :cond_c
    move-object v1, v0

    goto :goto_a

    :cond_d
    move-object v0, v4

    goto :goto_9

    :cond_e
    move v1, v2

    goto :goto_6

    :cond_f
    move v5, v0

    move v1, v2

    goto :goto_7
.end method

.method private a(Lorg/json/JSONArray;Ljava/util/ArrayList;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 959
    if-eqz p3, :cond_3

    .line 961
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->getCount()I

    move-result v1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v0

    move v2, v0

    .line 964
    :goto_0
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 965
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v1, v0, :cond_0

    .line 966
    add-int/lit8 v2, v2, 0x1

    .line 964
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 968
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 971
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 972
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 971
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    move-object p2, v3

    .line 977
    :cond_3
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_5

    .line 978
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(I)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-result-object v0

    .line 979
    if-eqz v0, :cond_4

    .line 980
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 977
    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 983
    :cond_5
    return-void
.end method

.method private static a(Lorg/json/JSONObject;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mfluent/asp/common/datamodel/ASPFileException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 582
    const-string v0, "result"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 583
    const-string v1, "resultDetail"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 584
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "-705343"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 585
    invoke-static {v5}, Lcom/mfluent/asp/datamodel/a/e;->a(I)V

    .line 586
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getNextWindow: result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/common/datamodel/ASPFileException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 588
    :cond_0
    const-string v1, "-705342"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "-405343"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "-405344"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 589
    :cond_1
    invoke-static {v4}, Lcom/mfluent/asp/datamodel/a/e;->a(I)V

    .line 590
    new-instance v1, Lcom/mfluent/asp/common/datamodel/ASPFileException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getNextWindow: result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, v4}, Lcom/mfluent/asp/common/datamodel/ASPFileException;-><init>(Ljava/lang/String;I)V

    throw v1

    .line 592
    :cond_2
    return-void
.end method

.method private b(I)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;
    .locals 1

    .prologue
    .line 737
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 738
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 740
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/mfluent/asp/datamodel/filebrowser/a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->n:Ljava/util/Set;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v3, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 283
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 287
    new-array v1, v3, [Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    sget-object v2, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->a:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    aput-object v2, v1, v0

    sget-object v2, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->d:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    aput-object v2, v1, v4

    sget-object v2, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->c:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    aput-object v2, v1, v5

    .line 291
    new-array v2, v3, [Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_SHARED:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    aput-object v3, v2, v0

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_PERSONAL:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    aput-object v3, v2, v4

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_PRIVATE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    aput-object v3, v2, v5

    .line 293
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 294
    new-instance v3, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct {v3}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    .line 295
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b()V

    .line 296
    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V

    .line 297
    aget-object v4, v1, v0

    invoke-static {v4}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 298
    iget-object v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)V

    .line 299
    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    .line 300
    iget-object v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 303
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 304
    array-length v0, v1

    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    .line 305
    return-void
.end method

.method private d()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v14, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 313
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-nez v0, :cond_0

    .line 314
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This file browser was never initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->b:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    if-ne v0, v1, :cond_2

    .line 319
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->c()V

    .line 567
    :cond_1
    :goto_0
    return-void

    .line 323
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->defaultString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 325
    invoke-static {}, Lcom/mfluent/asp/c;->a()Lcom/mfluent/asp/c;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->q:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/c;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSortType;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->defaultString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 326
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->r:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->defaultString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 330
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_8

    move-result-object v0

    .line 335
    :cond_3
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/a$4;->a:[I

    iget-object v7, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->ordinal()I

    move-result v7

    aget v1, v1, v7

    packed-switch v1, :pswitch_data_0

    const-string v1, "/api/pCloud/device/files/"

    :goto_2
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "?_="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "&sort="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v0, ""

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&maxItems=100&isPersonal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v0

    sget-object v4, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->d:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    if-ne v0, v4, :cond_c

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 348
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_23

    .line 349
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    iget-object v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-object v4, v0

    .line 352
    :goto_5
    if-eqz v4, :cond_d

    .line 354
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "&fileName="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "UTF-8"

    invoke-static {v6, v7}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 360
    :goto_6
    :try_start_2
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-static {v1, v0}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 362
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v6, 0x2

    if-gt v0, v6, :cond_4

    .line 363
    const-string v0, "mfl_ASP10FileBrowser"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Got json response: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    :cond_4
    const-string v0, "totalCount"

    const/4 v6, -0x1

    invoke-virtual {v1, v0, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    .line 367
    if-ltz v0, :cond_e

    .line 368
    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    .line 375
    const-string v0, "dirRevision"

    const-wide/16 v6, 0x0

    invoke-virtual {v1, v0, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 376
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->o:Z

    if-eqz v0, :cond_f

    .line 377
    iput-wide v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->p:J

    .line 393
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    if-nez v0, :cond_6

    .line 394
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    iget v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    invoke-direct {v0, v6}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    .line 397
    :cond_6
    const-string v0, "path"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 398
    const-string v6, "mycomputer"

    invoke-static {v6, v0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 399
    const-string v6, ""

    iput-object v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->j:Ljava/lang/String;

    .line 404
    :goto_7
    iget-object v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v6, v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(Ljava/lang/String;)V

    .line 405
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    iget-object v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->j:Ljava/lang/String;

    invoke-static {v6}, Lorg/apache/commons/io/FilenameUtils;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 406
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 408
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    iget-object v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->j:Ljava/lang/String;

    invoke-static {v6}, Lorg/apache/commons/io/FilenameUtils;->getFullPathNoEndSeparator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    iget-object v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "\\"

    invoke-static {v6, v7}, Lorg/apache/commons/lang3/StringUtils;->removeEnd(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 413
    :cond_7
    const-string v0, "special"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 414
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z
    :try_end_2
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    move-result v6

    if-eqz v6, :cond_14

    .line 416
    :try_start_3
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->valueOf(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/InterruptedIOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v0

    .line 421
    :goto_8
    :try_start_4
    iget-object v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v6, v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    .line 423
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, v6, :cond_15

    .line 424
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/a;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "\\"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->j:Ljava/lang/String;

    .line 426
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 427
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/a;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 440
    :cond_8
    :goto_9
    const-string v0, "files"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 441
    if-nez v0, :cond_22

    .line 443
    const-string v0, "file"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    move-object v1, v0

    .line 446
    :goto_a
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    .line 448
    :goto_b
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_1c

    .line 449
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 450
    const-string v0, "name"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 451
    const-string v0, "uri"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 453
    const-string v0, "special"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 454
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z
    :try_end_4
    .catch Ljava/io/InterruptedIOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5

    move-result v10

    if-eqz v10, :cond_17

    .line 456
    :try_start_5
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->valueOf(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/io/InterruptedIOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_5 .. :try_end_5} :catch_3

    move-result-object v0

    .line 463
    :goto_c
    if-nez v2, :cond_18

    :try_start_6
    const-string v10, ".."

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_18

    .line 465
    new-instance v7, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct {v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    iput-object v7, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 466
    iget-object v7, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b()V

    .line 467
    iget-object v7, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7, v9}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(Ljava/lang/String;)V

    .line 468
    iget-object v7, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7, v8}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 469
    iget-object v7, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7, v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    .line 471
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, v7, :cond_9

    .line 473
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 474
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    const-string v7, "mycomputer"

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(Ljava/lang/String;)V

    .line 475
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/a;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 478
    :cond_9
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    iget-object v7, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V

    .line 479
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    .line 480
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    iget v7, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    invoke-direct {v0, v7}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;
    :try_end_6
    .catch Ljava/io/InterruptedIOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    .line 448
    :cond_a
    :goto_d
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_b

    .line 335
    :pswitch_0
    const-string v1, "/api/pCloud/device/secured/"

    goto/16 :goto_2

    :cond_b
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "&search="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_c
    move v0, v2

    goto/16 :goto_4

    :catch_0
    move-exception v0

    :cond_d
    move-object v0, v1

    goto/16 :goto_6

    .line 371
    :cond_e
    :try_start_7
    invoke-static {v1}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lorg/json/JSONObject;)V

    .line 372
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getNextWindow: result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "result"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Ljava/io/InterruptedIOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 547
    :catch_1
    move-exception v0

    .line 548
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 549
    new-instance v1, Ljava/lang/InterruptedException;

    invoke-virtual {v0}, Ljava/io/InterruptedIOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 379
    :cond_f
    :try_start_8
    iget-wide v8, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->p:J

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    .line 380
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v14, :cond_10

    .line 381
    const-string v0, "mfl_ASP10FileBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::getNextWindow:Got json response: dirRevision: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->p:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_10
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->destroy()V

    .line 386
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/DataSetObserver;

    .line 387
    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onInvalidated()V
    :try_end_8
    .catch Ljava/io/InterruptedIOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_e

    .line 550
    :catch_2
    move-exception v0

    .line 551
    invoke-virtual {v0}, Lcom/mfluent/asp/nts/NonOkHttpResponseException;->a()I

    move-result v1

    const/16 v2, 0x190

    if-eq v1, v2, :cond_11

    invoke-virtual {v0}, Lcom/mfluent/asp/nts/NonOkHttpResponseException;->a()I

    move-result v1

    const/16 v2, 0x194

    if-ne v1, v2, :cond_12

    .line 553
    :cond_11
    :try_start_9
    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {v0}, Lcom/mfluent/asp/nts/NonOkHttpResponseException;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 554
    invoke-static {v1}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lorg/json/JSONObject;)V
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_7

    .line 560
    :cond_12
    throw v0

    .line 401
    :cond_13
    :try_start_a
    new-instance v6, Ljava/lang/String;

    const/16 v7, 0x8

    invoke-static {v0, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v7

    const-string v8, "UTF-8"

    invoke-direct {v6, v7, v8}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iput-object v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->j:Ljava/lang/String;
    :try_end_a
    .catch Ljava/io/InterruptedIOException; {:try_start_a .. :try_end_a} :catch_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_a .. :try_end_a} :catch_2
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_7

    .line 561
    :catch_3
    move-exception v0

    throw v0

    :catch_4
    move-exception v0

    :cond_14
    move-object v0, v3

    goto/16 :goto_8

    .line 429
    :cond_15
    :try_start_b
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, v6, :cond_8

    .line 430
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)Ljava/lang/String;

    move-result-object v6

    .line 431
    const-string v0, "/"

    .line 432
    iget-object v7, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->j:Ljava/lang/String;

    invoke-static {v7, v0}, Lorg/apache/commons/lang3/StringUtils;->startsWith(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_16

    .line 433
    const-string v0, ""

    .line 435
    :cond_16
    invoke-static {v6}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 436
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->j:Ljava/lang/String;
    :try_end_b
    .catch Ljava/io/InterruptedIOException; {:try_start_b .. :try_end_b} :catch_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_b .. :try_end_b} :catch_2
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5

    goto/16 :goto_9

    .line 563
    :catch_5
    move-exception v0

    .line 564
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Trouble fetching next ASP10 window"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_6
    move-exception v0

    :cond_17
    move-object v0, v3

    goto/16 :goto_c

    .line 483
    :cond_18
    if-nez v2, :cond_19

    :try_start_c
    iget-object v10, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->r:Ljava/lang/String;

    invoke-static {v10}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_19

    .line 484
    iget-object v10, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-eqz v10, :cond_19

    sget-object v10, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->b:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    iget-object v11, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_19

    .line 487
    iget v10, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    add-int/lit8 v10, v10, -0x1

    iput v10, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    .line 491
    :cond_19
    new-instance v10, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct {v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    .line 492
    iget-object v11, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->f()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V

    .line 493
    const-string v11, "isDirectory"

    const/4 v12, 0x0

    invoke-virtual {v7, v11, v12}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 494
    if-eqz v11, :cond_1a

    .line 495
    invoke-virtual {v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b()V

    .line 498
    :cond_1a
    iget-object v11, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v10, v11}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)V

    .line 499
    const-string v11, "fileSize"

    const-wide/16 v12, 0x0

    invoke-virtual {v7, v11, v12, v13}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(J)V

    .line 500
    invoke-virtual {v10, v8}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/lang/String;)V

    .line 501
    iget-object v11, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->t:Ljava/text/Collator;

    if-eqz v11, :cond_1b

    .line 502
    iget-object v11, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->t:Ljava/text/Collator;

    invoke-virtual {v11, v8}, Ljava/text/Collator;->getCollationKey(Ljava/lang/String;)Ljava/text/CollationKey;

    move-result-object v8

    invoke-virtual {v10, v8}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Ljava/text/CollationKey;)V

    .line 504
    :cond_1b
    invoke-virtual {v10, v9}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(Ljava/lang/String;)V

    .line 505
    const-string v8, "modified"

    const-wide/16 v12, 0x0

    invoke-virtual {v7, v8, v12, v13}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v8

    const-wide/16 v12, 0x3e8

    mul-long/2addr v8, v12

    invoke-virtual {v10, v8, v9}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b(J)V

    .line 506
    invoke-virtual {v10, v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;)V

    .line 508
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 510
    invoke-virtual {v10}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 511
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    iget-object v7, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->setIsDirectory(I)V

    goto/16 :goto_d

    .line 516
    :cond_1c
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, v1, :cond_1d

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    if-nez v0, :cond_1d

    .line 519
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 520
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->b:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V

    .line 522
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b()V

    .line 525
    :cond_1d
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;)V

    .line 527
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v14, :cond_1e

    .line 528
    const-string v1, "mfl_ASP10FileBrowser"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "getNextWindow: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " of "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", dir: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", parent: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", sort: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v4, :cond_20

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, ", from: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    :cond_1e
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_1f

    .line 544
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->s:Z

    if-eqz v0, :cond_1f

    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->f()Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->q:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    if-ne v0, v1, :cond_21

    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/a;->b:Ljava/util/Comparator;

    :goto_10
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_c
    .catch Ljava/io/InterruptedIOException; {:try_start_c .. :try_end_c} :catch_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_c .. :try_end_c} :catch_2
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    .line 566
    :cond_1f
    invoke-static {}, Lcom/mfluent/asp/datamodel/a/e;->b()V

    goto/16 :goto_0

    .line 528
    :cond_20
    :try_start_d
    const-string v0, ""

    goto :goto_f

    .line 544
    :cond_21
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/a;->c:Ljava/util/Comparator;
    :try_end_d
    .catch Ljava/io/InterruptedIOException; {:try_start_d .. :try_end_d} :catch_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_d .. :try_end_d} :catch_2
    .catch Lcom/mfluent/asp/common/datamodel/ASPFileException; {:try_start_d .. :try_end_d} :catch_3
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5

    goto :goto_10

    .line 556
    :catch_7
    move-exception v1

    throw v0

    :catch_8
    move-exception v1

    goto/16 :goto_1

    :cond_22
    move-object v1, v0

    goto/16 :goto_a

    :cond_23
    move-object v4, v3

    goto/16 :goto_5

    .line 335
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 595
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 716
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->q:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->q:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x3

    .line 674
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->getCount()I

    move-result v1

    if-lt p1, v1, :cond_0

    .line 711
    :goto_0
    return-object v0

    .line 678
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    .line 679
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    goto :goto_0

    .line 682
    :cond_1
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lockInterruptibly()V

    .line 685
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x64

    add-int/lit8 v2, v1, 0x1

    .line 687
    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v5, :cond_2

    .line 688
    const-string v1, "mfl_ASP10FileBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getFile: not in cache: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", windows: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    :cond_2
    const/4 v1, 0x0

    .line 693
    :cond_3
    :try_start_0
    iget-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt p1, v3, :cond_6

    .line 694
    iget-boolean v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->l:Z

    if-eqz v3, :cond_4

    .line 695
    new-instance v0, Ljava/lang/InterruptedException;

    const-string v1, "ASP10FileBrowser destroyed"

    invoke-direct {v0, v1}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 711
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 698
    :cond_4
    :try_start_1
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->d()V

    .line 700
    add-int/lit8 v1, v1, 0x1

    .line 701
    if-le v1, v2, :cond_3

    .line 702
    sget-object v1, Lcom/mfluent/asp/datamodel/filebrowser/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v5, :cond_5

    .line 703
    const-string v1, "mfl_ASP10FileBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::getFile: file doesn\'t exist! "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 705
    :cond_5
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    .line 709
    :cond_6
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 711
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->s:Z

    .line 225
    return-void
.end method

.method public final a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lockInterruptibly()V

    .line 237
    if-nez p1, :cond_2

    .line 238
    :try_start_0
    new-instance p1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-direct {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;-><init>()V

    .line 239
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->b()V

    .line 240
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 241
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, v1, :cond_0

    .line 242
    sget-object v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;->b:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File$ASP10FileSystemType;)V

    .line 249
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 250
    iput-object p2, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->q:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 251
    iput-object p3, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->r:Ljava/lang/String;

    .line 253
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 255
    iget-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->s:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 256
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->t:Ljava/text/Collator;

    .line 257
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->t:Ljava/text/Collator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/Collator;->setStrength(I)V

    .line 261
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->j:Ljava/lang/String;

    .line 263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->l:Z

    .line 264
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    .line 265
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->p:J

    .line 268
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->o:Z

    .line 269
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->d()V

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->o:Z

    .line 272
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->u:Lcom/mfluent/asp/datamodel/filebrowser/a$a;

    if-nez v0, :cond_1

    .line 273
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/a$a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/datamodel/filebrowser/a$a;-><init>(Lcom/mfluent/asp/datamodel/filebrowser/a;)V

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->u:Lcom/mfluent/asp/datamodel/filebrowser/a$a;

    .line 274
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.mfluent.asp.filetransfer.FileTransferManager.TRANSFER_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 275
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->f:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->u:Lcom/mfluent/asp/datamodel/filebrowser/a$a;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 279
    return-void

    .line 246
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->d()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 278
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 259
    :cond_3
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->t:Ljava/text/Collator;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 851
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lockInterruptibly()V

    .line 853
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 854
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v0, 0x0

    .line 868
    :goto_0
    return-object v0

    .line 856
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 857
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->getSelectedFileIndexes()Ljava/util/Iterator;

    move-result-object v1

    .line 858
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 859
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 868
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 862
    :cond_1
    :try_start_2
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 864
    invoke-direct {p0, v0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 868
    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method public final deselectAll()V
    .locals 1

    .prologue
    .line 779
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    if-eqz v0, :cond_0

    .line 780
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->deselectAll()V

    .line 782
    :cond_0
    return-void
.end method

.method public final destroy()V
    .locals 2

    .prologue
    .line 621
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->l:Z

    .line 623
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->u:Lcom/mfluent/asp/datamodel/filebrowser/a$a;

    if-eqz v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->u:Lcom/mfluent/asp/datamodel/filebrowser/a$a;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 625
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->u:Lcom/mfluent/asp/datamodel/filebrowser/a$a;

    .line 627
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 665
    iget v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->g:I

    return v0
.end method

.method public final bridge synthetic getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    return-object v0
.end method

.method public final getCurrentDirectoryAbsolutePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic getFile(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(I)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getFileNonBlocking(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/mfluent/asp/datamodel/filebrowser/a;->b(I)Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getParentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    return-object v0
.end method

.method public final getSelectedFileIndexes()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 824
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    if-nez v0, :cond_0

    .line 825
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 826
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 831
    :goto_0
    return-object v0

    .line 829
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->getSelectedFileIndexes()Ljava/util/Iterator;

    move-result-object v1

    .line 831
    new-instance v0, Lcom/mfluent/asp/datamodel/filebrowser/a$3;

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/datamodel/filebrowser/a$3;-><init>(Lcom/mfluent/asp/datamodel/filebrowser/a;Ljava/util/Iterator;)V

    goto :goto_0
.end method

.method public final synthetic init(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    check-cast p1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/filebrowser/a;->a(Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    return-void
.end method

.method public final isAllSelected()Z
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->isAllSelected()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSelected(I)Z
    .locals 1

    .prologue
    .line 759
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->isSelected(I)Z

    move-result v0

    return v0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 799
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 802
    :cond_0
    return-void
.end method

.method public final selectAll()V
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    if-eqz v0, :cond_0

    .line 769
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectAll()V

    .line 771
    :cond_0
    return-void
.end method

.method public final setSelected(IZ)V
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->m:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->setSelected(IZ)V

    .line 751
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 815
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ASP10FileBrowser: curDir: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->h:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", parent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->i:Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", full path: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/filebrowser/a;->getCurrentDirectoryAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 810
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/filebrowser/a;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 811
    return-void
.end method
