.class public abstract Lcom/mfluent/asp/datamodel/o;
.super Lcom/mfluent/asp/datamodel/n;
.source "SourceFile"


# static fields
.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "keyword"

    aput-object v1, v0, v2

    const-string v1, "keyword_map_id"

    aput-object v1, v0, v3

    const-string v1, "keyword_id"

    aput-object v1, v0, v4

    const-string v1, "file_id"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mfluent/asp/datamodel/o;->b:[Ljava/lang/String;

    .line 299
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "COUNT("

    aput-object v1, v0, v2

    const-string v1, "MAX("

    aput-object v1, v0, v3

    const-string v1, "MIN("

    aput-object v1, v0, v4

    const-string v1, "SUM("

    aput-object v1, v0, v5

    const-string v1, "AVG("

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "TOTAL("

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "GROUP_CONCAT("

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/o;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/n;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 2

    .prologue
    .line 338
    const-string v0, "title"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "_display_name"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 339
    :cond_0
    const-string v0, "title"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 340
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 341
    const-string v0, "_display_name"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 344
    :cond_1
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 345
    if-ne p0, p1, :cond_2

    .line 346
    new-instance p0, Landroid/content/ContentValues;

    invoke-direct {p0, p1}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 348
    :cond_2
    const-string v1, "index_char"

    invoke-static {v0}, Lcom/sec/pcw/util/LanguageUtil;->a(Ljava/lang/String;)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    :cond_3
    return-object p0
.end method

.method private static b(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 2

    .prologue
    .line 356
    const-string v0, "file_digest"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 357
    const-string v0, "file_digest"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 358
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 359
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 360
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 361
    if-ne p0, p1, :cond_0

    .line 362
    new-instance p0, Landroid/content/ContentValues;

    invoke-direct {p0, p1}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 364
    :cond_0
    const-string v0, "file_digest"

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    :cond_1
    return-object p0
.end method

.method private e(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const-wide/16 v8, 0x0

    .line 60
    cmp-long v2, p3, v8

    if-nez v2, :cond_1

    if-nez p5, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    invoke-virtual/range {p0 .. p5}, Lcom/mfluent/asp/datamodel/o;->b(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)J

    move-result-wide v4

    .line 69
    cmp-long v2, v4, v8

    if-nez v2, :cond_2

    if-eqz p5, :cond_0

    .line 74
    :cond_2
    invoke-virtual/range {p0 .. p5}, Lcom/mfluent/asp/datamodel/o;->d(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)J

    move-result-wide v2

    .line 76
    cmp-long v6, v2, v8

    if-nez v6, :cond_8

    move-wide v2, v4

    .line 85
    :goto_1
    cmp-long v6, v4, v8

    if-eqz v6, :cond_4

    const-string v6, "dup_id"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    if-eqz v6, :cond_3

    const-string v6, "dup_id"

    invoke-virtual {p2, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-eqz v6, :cond_4

    .line 86
    :cond_3
    const-string v0, "dup_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move v0, v1

    .line 89
    :cond_4
    cmp-long v4, p3, v8

    if-eqz v4, :cond_6

    const-string v4, "burst_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    if-eqz v4, :cond_5

    const-string v4, "burst_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, p3

    if-eqz v4, :cond_6

    .line 90
    :cond_5
    const-string v0, "burst_id"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move v0, v1

    .line 93
    :cond_6
    cmp-long v4, v2, v8

    if-eqz v4, :cond_0

    const-string v4, "burst_dup_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    if-eqz v4, :cond_7

    const-string v4, "burst_dup_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-eqz v4, :cond_0

    .line 95
    :cond_7
    const-string v0, "burst_dup_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p2, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move v0, v1

    .line 96
    goto :goto_0

    :cond_8
    move-wide p3, v2

    .line 80
    goto :goto_1
.end method

.method private f(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 184
    const-string v0, "bucket_display_name"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    const-string v0, "bucket_display_name"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 192
    :goto_0
    const/4 v3, 0x3

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/o;->a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;IJZ)V

    .line 195
    :cond_0
    return-void

    .line 190
    :cond_1
    new-array v2, v3, [Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;JLandroid/content/ContentValues;)I
    .locals 13

    .prologue
    .line 376
    const-wide/16 v2, 0x0

    cmp-long v2, p5, v2

    if-eqz v2, :cond_0

    .line 378
    move-object/from16 v0, p7

    invoke-static {p2, v0}, Lcom/mfluent/asp/datamodel/o;->a(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v2

    .line 379
    move-object/from16 v0, p7

    invoke-static {v2, v0}, Lcom/mfluent/asp/datamodel/o;->b(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v5

    .line 380
    const/4 v8, 0x0

    move-object v3, p0

    move-object v4, p1

    move-wide/from16 v6, p5

    invoke-direct/range {v3 .. v8}, Lcom/mfluent/asp/datamodel/o;->e(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)Z

    .line 382
    const/4 v12, 0x0

    move-object v7, p0

    move-object v8, p1

    move-object/from16 v9, p7

    move-wide/from16 v10, p5

    invoke-direct/range {v7 .. v12}, Lcom/mfluent/asp/datamodel/o;->f(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)V

    .line 383
    const/4 v12, 0x0

    move-object v7, p0

    move-object v8, p1

    move-object/from16 v9, p7

    move-wide/from16 v10, p5

    invoke-virtual/range {v7 .. v12}, Lcom/mfluent/asp/datamodel/o;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)V

    .line 384
    const/4 v12, 0x0

    move-object v7, p0

    move-object v8, p1

    move-object/from16 v9, p7

    move-wide/from16 v10, p5

    invoke-virtual/range {v7 .. v12}, Lcom/mfluent/asp/datamodel/o;->c(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)V

    :goto_0
    move-object v3, p0

    move-object v4, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-wide/from16 v8, p5

    move-object/from16 v10, p7

    .line 387
    invoke-super/range {v3 .. v10}, Lcom/mfluent/asp/datamodel/n;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;JLandroid/content/ContentValues;)I

    move-result v2

    return v2

    :cond_0
    move-object v5, p2

    goto :goto_0
.end method

.method public a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J
    .locals 11

    .prologue
    .line 223
    const-string v0, "media_type"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    const-string v0, "media_type"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/o;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 227
    :cond_0
    invoke-static {p2, p3}, Lcom/mfluent/asp/datamodel/o;->a(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v0

    .line 229
    invoke-static {v0, p3}, Lcom/mfluent/asp/datamodel/o;->b(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v10

    .line 231
    const-wide/16 v8, 0x0

    .line 233
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->d:Landroid/net/Uri;

    const-string v2, "insert_or_update"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 234
    const-string v0, "device_id"

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 235
    const-string v1, "source_media_id"

    invoke-virtual {v10, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 236
    if-eqz v0, :cond_9

    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 237
    const-string v3, "device_id=? AND source_media_id=? AND media_type=?"

    .line 238
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/o;->j()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 240
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/o;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v10, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 241
    if-lez v0, :cond_9

    .line 242
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/o;->d()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v2, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 250
    if-eqz v2, :cond_9

    .line 251
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 252
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 254
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-wide v4, v0

    .line 260
    :goto_1
    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, v10

    invoke-direct/range {v1 .. v6}, Lcom/mfluent/asp/datamodel/o;->e(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)Z

    move-result v0

    .line 261
    const-wide/16 v2, 0x0

    cmp-long v1, v4, v2

    if-nez v1, :cond_1

    .line 262
    invoke-super {p0, p1, v10, p3}, Lcom/mfluent/asp/datamodel/n;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 263
    const/4 v0, 0x1

    .line 265
    :cond_1
    if-eqz v0, :cond_6

    .line 266
    const-string v0, "dup_id"

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 267
    const-string v0, "burst_id"

    invoke-virtual {v10, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 268
    if-nez v1, :cond_2

    .line 269
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 271
    :cond_2
    if-nez v0, :cond_3

    .line 272
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 274
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-nez v2, :cond_6

    .line 275
    :cond_4
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 276
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 277
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_5

    .line 279
    const-string v1, "dup_id"

    invoke-virtual {v3, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object v1, v2

    .line 281
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_7

    .line 282
    const-string v0, "burst_dup_id"

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 283
    const-string v0, "burst_id"

    invoke-virtual {v3, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 288
    :goto_2
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/o;->d()Ljava/lang/String;

    move-result-object v1

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v8

    invoke-virtual {v0, v1, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 292
    :cond_6
    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    invoke-direct/range {v1 .. v6}, Lcom/mfluent/asp/datamodel/o;->f(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)V

    .line 293
    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    invoke-virtual/range {v1 .. v6}, Lcom/mfluent/asp/datamodel/o;->a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)V

    .line 294
    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    invoke-virtual/range {v1 .. v6}, Lcom/mfluent/asp/datamodel/o;->c(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)V

    .line 296
    return-wide v4

    .line 285
    :cond_7
    const-string v1, "burst_dup_id"

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_2

    :cond_8
    move-wide v0, v8

    goto/16 :goto_0

    :cond_9
    move-wide v4, v8

    goto/16 :goto_1
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 313
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/b/c;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/c;

    move-result-object v3

    .line 314
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d()Z

    move-result v0

    .line 316
    if-nez v0, :cond_4

    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/o;->j()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/b/c;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 317
    if-nez p2, :cond_1

    const/4 v0, 0x1

    .line 318
    :goto_0
    if-nez v0, :cond_3

    .line 319
    const/4 v1, 0x1

    .line 320
    array-length v4, p2

    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, p2, v2

    .line 321
    sget-object v6, Lcom/mfluent/asp/datamodel/o;->c:[Ljava/lang/String;

    array-length v7, v6

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v7, :cond_0

    aget-object v8, v6, v1

    .line 322
    invoke-static {v5, v8}, Lorg/apache/commons/lang3/StringUtils;->indexOfIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v8

    if-ltz v8, :cond_2

    .line 323
    const/4 v0, 0x0

    .line 320
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 317
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 321
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 329
    :cond_3
    if-eqz v0, :cond_4

    .line 330
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/o;->j()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Lcom/mfluent/asp/b/c;->a(IZ)V

    .line 334
    :cond_4
    invoke-super/range {p0 .. p10}, Lcom/mfluent/asp/datamodel/n;->a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)V
    .locals 7

    .prologue
    .line 199
    const-string v0, "_display_name"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 202
    const-string v1, "_display_name"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 203
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 205
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 206
    invoke-static {v1}, Lorg/apache/commons/io/FilenameUtils;->removeExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 207
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 208
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 212
    :cond_0
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/o;->a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;IJZ)V

    .line 215
    :cond_1
    return-void
.end method

.method protected final a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;IJZ)V
    .locals 12

    .prologue
    .line 103
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 104
    const-string v0, "file_id"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 105
    const-string v0, "keyword_type"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 106
    invoke-static {}, Lcom/mfluent/asp/datamodel/an$a;->a()Lcom/mfluent/asp/datamodel/an;

    move-result-object v9

    .line 108
    if-eqz p6, :cond_1

    .line 110
    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_8

    aget-object v2, p2, v0

    .line 111
    invoke-virtual {v9, p1, v2}, Lcom/mfluent/asp/datamodel/an;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    .line 112
    if-eqz v3, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/o;->g()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "Inserting keyword: {} file id: {}"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v2, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 114
    const-string v2, "keyword_id"

    invoke-virtual {v8, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 115
    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "KEYWORD_MAP"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 110
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 122
    :cond_1
    invoke-static {p2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 124
    const/4 v0, 0x1

    new-array v10, v0, [Ljava/lang/String;

    .line 126
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FILE_KEYWORD_DETAIL"

    sget-object v2, Lcom/mfluent/asp/datamodel/o;->b:[Ljava/lang/String;

    const-string v3, "file_id=? AND keyword_type=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "keyword"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 130
    if-eqz v5, :cond_8

    .line 131
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    .line 132
    const/4 v0, 0x0

    .line 135
    :goto_1
    array-length v2, p2

    if-lt v0, v2, :cond_2

    if-eqz v1, :cond_7

    .line 136
    :cond_2
    const/4 v3, 0x0

    .line 137
    const/4 v2, 0x0

    .line 138
    if-eqz v1, :cond_4

    .line 139
    const-string v2, "keyword"

    invoke-static {v5, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 140
    const-string v2, "keyword_map_id"

    invoke-static {v5, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    .line 141
    array-length v4, p2

    if-lt v0, v4, :cond_3

    .line 142
    const/4 v4, -0x1

    .line 151
    :goto_2
    if-nez v4, :cond_5

    .line 153
    add-int/lit8 v0, v0, 0x1

    .line 154
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    goto :goto_1

    .line 144
    :cond_3
    aget-object v4, p2, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    goto :goto_2

    .line 148
    :cond_4
    const/4 v4, 0x1

    goto :goto_2

    .line 155
    :cond_5
    if-gez v4, :cond_6

    .line 156
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/o;->g()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v4, "Removing keyword: {} file id: {}"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v1, v4, v3, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 159
    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v1

    .line 160
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "KEYWORD_MAP"

    const-string v3, "_id=?"

    invoke-virtual {v1, v2, v3, v10}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 162
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    goto :goto_1

    .line 165
    :cond_6
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/o;->g()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "Inserting keyword: {} file id: {}"

    aget-object v4, p2, v0

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v2, v3, v4, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 168
    aget-object v2, p2, v0

    invoke-virtual {v9, p1, v2}, Lcom/mfluent/asp/datamodel/an;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 170
    const-string v3, "keyword_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 172
    iget-object v2, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "KEYWORD_MAP"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 174
    add-int/lit8 v0, v0, 0x1

    .line 176
    goto :goto_1

    .line 178
    :cond_7
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 180
    :cond_8
    return-void
.end method

.method protected abstract b(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)J
.end method

.method protected c(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)V
    .locals 0

    .prologue
    .line 219
    return-void
.end method

.method protected d(Lcom/mfluent/asp/datamodel/ao$a;Landroid/content/ContentValues;JZ)J
    .locals 2

    .prologue
    .line 43
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final d_()Z
    .locals 1

    .prologue
    .line 392
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract g()Lorg/slf4j/Logger;
.end method

.method protected abstract j()I
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/o;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
