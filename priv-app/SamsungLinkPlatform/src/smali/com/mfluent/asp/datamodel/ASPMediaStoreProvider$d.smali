.class final Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# static fields
.field private static a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/Thread;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->a:Ljava/util/ArrayList;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->b:Ljava/lang/Thread;

    .line 173
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c:I

    return-void
.end method

.method public static declared-synchronized a()Z
    .locals 3

    .prologue
    .line 141
    const-class v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;

    invoke-direct {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;-><init>()V

    .line 143
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b()V
    .locals 3

    .prologue
    .line 147
    const-class v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;

    invoke-direct {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;-><init>()V

    .line 149
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 150
    if-ltz v2, :cond_0

    .line 151
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;

    iget v2, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :goto_0
    monitor-exit v1

    return-void

    .line 153
    :cond_0
    :try_start_1
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized c()V
    .locals 5

    .prologue
    .line 159
    const-class v1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;

    invoke-direct {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;-><init>()V

    .line 161
    sget-object v2, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 162
    if-ltz v2, :cond_0

    .line 163
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;

    .line 164
    iget v3, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c:I

    const/4 v4, 0x1

    if-gt v3, v4, :cond_1

    .line 165
    sget-object v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 167
    :cond_1
    :try_start_1
    iget v2, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 182
    if-ne p1, p0, :cond_0

    .line 183
    const/4 v0, 0x1

    .line 187
    :goto_0
    return v0

    .line 186
    :cond_0
    check-cast p1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;

    .line 187
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->b:Ljava/lang/Thread;

    iget-object v1, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->b:Ljava/lang/Thread;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider$d;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
