.class public final Lcom/mfluent/asp/datamodel/ad;
.super Lcom/mfluent/asp/datamodel/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/datamodel/ad$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/datamodel/a",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "genre_key"

    aput-object v1, v0, v5

    const-string v1, "COUNT(audio_id) AS _count"

    aput-object v1, v0, v6

    const-string v1, "COUNT(DISTINCT dup_id) AS dup_reduced_count"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "source_media_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "thumbnail_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "source_album_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "device_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "local_source_album_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ad;->a:[Ljava/lang/String;

    .line 45
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "genre_key"

    aput-object v1, v0, v5

    const-string v1, "COUNT(audio_id) AS _count"

    aput-object v1, v0, v6

    const-string v1, "COUNT(DISTINCT dup_id) AS dup_reduced_count"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "device_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "local_source_album_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/datamodel/ad;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/a;-><init>()V

    .line 68
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/mfluent/asp/datamodel/ad;-><init>()V

    return-void
.end method

.method public static g()Lcom/mfluent/asp/datamodel/ad;
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Lcom/mfluent/asp/datamodel/ad$a;->a()Lcom/mfluent/asp/datamodel/ad;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;[Ljava/lang/String;I)I
    .locals 10

    .prologue
    .line 172
    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v0, 0x64

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 174
    const-string v0, "<unknown>"

    invoke-virtual {p0, p1, v0}, Lcom/mfluent/asp/datamodel/ad;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    .line 176
    const-string v0, "INSERT INTO GENRE_MEMBERS"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    const-string v0, " (genre_id,audio_id)"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string v0, " SELECT "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "AUDIO._id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    const-string v0, " FROM AUDIO LEFT OUTER JOIN GENRE_MEMBERS"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    const-string v0, " ON AUDIO._id"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    const/16 v0, 0x3d

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "GENRE_MEMBERS.audio_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    const-string v0, " WHERE genre_id IS NULL AND "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string v0, "AUDIO.device_id="

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 190
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 192
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/ad;->e()I

    move-result v0

    if-lez v0, :cond_2

    .line 193
    invoke-virtual {v9}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Lorg/apache/commons/lang3/ArrayUtils;->add([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 194
    const/4 v0, 0x0

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 195
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const/16 v0, 0x28

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    :cond_0
    const-string v0, "_id!=?"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "GENRE_ORPHANS"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "genre_key"

    aput-object v5, v2, v3

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_2

    .line 209
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 210
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 211
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/datamodel/ad;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 214
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 218
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 219
    const-string v0, "_id IN ("

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    const-string v0, "SELECT GENRES._id"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    const-string v0, " FROM GENRES LEFT OUTER JOIN GENRE_MEMBERS"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    const-string v0, " ON GENRES._id=genre_id"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    const-string v0, " WHERE "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    const-string v0, "genre_id IS NULL)"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    const-string v0, " AND (GENRES._id!="

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 232
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 233
    const-string v0, " AND ("

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 236
    :cond_3
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "GENRES"

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 21
    invoke-super/range {p0 .. p10}, Lcom/mfluent/asp/datamodel/a;->a(Lcom/mfluent/asp/datamodel/ao$a;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 77
    const-string v0, "genre"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 121
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    :goto_0
    return-object v5

    .line 125
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 126
    invoke-static {v9}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->keyFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 128
    invoke-virtual {p0, p1, v10}, Lcom/mfluent/asp/datamodel/ad;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v8

    .line 131
    if-nez v8, :cond_4

    .line 132
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "GENRES"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v6

    const-string v3, "genre_key=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object v10, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 141
    if-eqz v1, :cond_3

    .line 143
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 147
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 151
    :goto_2
    if-nez v0, :cond_1

    .line 152
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 153
    const-string v1, "name"

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v1, "genre_key"

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v1, p1, Lcom/mfluent/asp/datamodel/ao$a;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "GENRES"

    invoke-virtual {v1, v2, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 157
    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 160
    :cond_1
    invoke-virtual {p0, p1, v10, v0}, Lcom/mfluent/asp/datamodel/ad;->a(Lcom/mfluent/asp/datamodel/ao$a;Ljava/lang/Object;Ljava/lang/Integer;)V

    :goto_3
    move-object v5, v0

    .line 163
    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move-object v0, v8

    goto :goto_1

    :cond_3
    move-object v0, v8

    goto :goto_2

    :cond_4
    move-object v0, v8

    goto :goto_3
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    const-string v0, "_id"

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    invoke-super {p0, p1, p2, p3}, Lcom/mfluent/asp/datamodel/a;->a(Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)V

    return-void
.end method

.method protected final a(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/mfluent/asp/datamodel/ao$a;->a:Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/ASPMediaStoreProvider;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    sget-object v0, Lcom/mfluent/asp/datamodel/ad;->b:[Ljava/lang/String;

    .line 254
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/mfluent/asp/datamodel/ad;->a:[Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 82
    const-string v0, "genre"

    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/datamodel/ao;->b(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    const-string v0, "(title COLLATE LOCALIZED) = MIN(title)"

    return-object v0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/ao$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    const-string v0, "GENRES_AGGREGATE"

    return-object v0
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 72
    const/16 v0, 0x32

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    const-string v0, "GENRES"

    return-object v0
.end method

.method public final d_()Z
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x1

    return v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->CONTENT_TYPE:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method
