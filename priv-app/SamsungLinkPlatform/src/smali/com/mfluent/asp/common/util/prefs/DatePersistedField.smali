.class public Lcom/mfluent/asp/common/util/prefs/DatePersistedField;
.super Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField",
        "<",
        "Ljava/util/Date;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;)V

    .line 13
    return-void
.end method


# virtual methods
.method protected bridge synthetic getValue(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    check-cast p3, Ljava/util/Date;

    invoke-virtual {p0, p1, p2, p3}, Lcom/mfluent/asp/common/util/prefs/DatePersistedField;->getValue(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Date;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method protected getValue(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Date;)Ljava/util/Date;
    .locals 4

    .prologue
    .line 17
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-interface {p1, p2, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method protected bridge synthetic putValue(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    check-cast p3, Ljava/util/Date;

    invoke-virtual {p0, p1, p2, p3}, Lcom/mfluent/asp/common/util/prefs/DatePersistedField;->putValue(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/util/Date;)V

    return-void
.end method

.method protected putValue(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/util/Date;)V
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-interface {p1, p2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 23
    return-void
.end method
