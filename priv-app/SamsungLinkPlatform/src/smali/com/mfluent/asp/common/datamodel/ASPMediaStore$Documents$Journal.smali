.class public final Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Journal;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Journal"
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String;

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final ENTRY_CONTENT_TYPE:Ljava/lang/String;

.field public static final PATH:Ljava/lang/String; = "document_journal"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1223
    const-string v0, "document_journal"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$7500(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Journal;->CONTENT_TYPE:Ljava/lang/String;

    .line 1225
    const-string v0, "document_journal"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$7600(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Journal;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    .line 1227
    const-string v0, "document_journal"

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Journal;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1231
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Journal;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public final getDeviceSyncedJournalColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1246
    const-string v0, "last_recv_document_journal_id"

    return-object v0
.end method

.method public final getJournalDroppedKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1236
    const-string v0, "document_journal_dropped"

    return-object v0
.end method

.method public final getJournalSyncedKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1241
    const-string v0, "document_journal_synced"

    return-object v0
.end method
