.class public Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;
.super Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/common/content/ContentResolverContentAdapter",
        "<",
        "Lcom/mfluent/asp/common/content/MultiColumnContentId;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private static final LIMIT_BOUND_PARAMS:I = 0xfa

.field private static final PARCEL_VERSION:I = 0x2


# instance fields
.field private mIdFieldNames:[Ljava/lang/String;

.field private mIdTypes:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter$1;

    invoke-direct {v0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter$1;-><init>()V

    sput-object v0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    const-string v0, "_id"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;-><init>(Ljava/lang/String;I)V

    .line 42
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 71
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;-><init>(Landroid/os/Parcel;)V

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 73
    if-lez v0, :cond_0

    if-le v0, v1, :cond_1

    .line 74
    :cond_0
    new-instance v0, Landroid/os/BadParcelableException;

    const-string v1, "Bad parcel version."

    invoke-direct {v0, v1}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_1
    if-ge v0, v1, :cond_2

    .line 77
    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 79
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    .line 80
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aput v1, v0, v2

    .line 85
    :goto_0
    return-void

    .line 82
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;-><init>()V

    .line 45
    new-array v0, v2, [Ljava/lang/String;

    aput-object p1, v0, v1

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    .line 46
    new-array v0, v2, [I

    aput p2, v0, v1

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    .line 47
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;-><init>()V

    .line 50
    invoke-virtual {p1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    .line 51
    invoke-virtual {p2}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    .line 52
    return-void
.end method


# virtual methods
.method protected bridge synthetic copyId(Lcom/mfluent/asp/common/content/ContentId;)Lcom/mfluent/asp/common/content/ContentId;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->copyId(Lcom/mfluent/asp/common/content/MultiColumnContentId;)Lcom/mfluent/asp/common/content/MultiColumnContentId;

    move-result-object v0

    return-object v0
.end method

.method protected copyId(Lcom/mfluent/asp/common/content/MultiColumnContentId;)Lcom/mfluent/asp/common/content/MultiColumnContentId;
    .locals 1

    .prologue
    .line 224
    if-nez p1, :cond_0

    .line 225
    const/4 v0, 0x0

    .line 227
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    invoke-direct {v0, p1}, Lcom/mfluent/asp/common/content/MultiColumnContentId;-><init>(Lcom/mfluent/asp/common/content/MultiColumnContentId;)V

    goto :goto_0
.end method

.method protected createFilteredSelection()Landroid/util/Pair;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v7, 0x28

    const/16 v11, 0x27

    const/16 v10, 0x29

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 141
    const/4 v0, 0x0

    .line 142
    const/4 v4, 0x0

    .line 144
    iget-boolean v1, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIsEditing:Z

    if-eqz v1, :cond_d

    .line 146
    new-instance v5, Ljava/lang/StringBuffer;

    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    iget-object v1, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    array-length v1, v1

    mul-int/2addr v0, v1

    invoke-direct {v5, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 147
    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    array-length v1, v1

    mul-int/2addr v0, v1

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 148
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    array-length v1, v1

    mul-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v3

    :goto_0
    add-int/2addr v0, v1

    const/16 v1, 0xfa

    if-le v0, v1, :cond_3

    move v1, v2

    .line 151
    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    array-length v0, v0

    if-ne v0, v2, :cond_7

    .line 153
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 154
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIsInverseSelection:Z

    if-eqz v0, :cond_0

    .line 155
    const-string v0, " NOT"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 157
    :cond_0
    const-string v0, " IN ("

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 159
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    .line 160
    if-lez v2, :cond_1

    .line 161
    const/16 v8, 0x2c

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 163
    :cond_1
    if-eqz v1, :cond_5

    .line 164
    iget-object v8, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    aget v8, v8, v3

    const/4 v9, 0x3

    if-ne v8, v9, :cond_4

    .line 165
    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->getMediaId()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v8, "\'"

    const-string v9, "\'\'"

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-virtual {v5, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 167
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 168
    invoke-virtual {v5, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 176
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 177
    goto :goto_2

    .line 148
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    .line 170
    :cond_4
    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->getMediaId()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 173
    :cond_5
    const/16 v8, 0x3f

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 174
    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->getMediaId()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 178
    :cond_6
    invoke-virtual {v5, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 209
    :goto_4
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 210
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    .line 211
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 219
    :goto_5
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 180
    :cond_7
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIsInverseSelection:Z

    if-eqz v0, :cond_8

    .line 181
    const-string v0, "NOT "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 183
    :cond_8
    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 185
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-le v0, v2, :cond_9

    .line 186
    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 188
    :cond_9
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_a
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    .line 189
    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->getSqlFilterSelection(Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 193
    if-nez v1, :cond_a

    .line 194
    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->getSqlFilterSelectionArgs()[Ljava/lang/String;

    move-result-object v8

    .line 195
    if-eqz v8, :cond_a

    move v0, v3

    .line 196
    :goto_6
    array-length v9, v8

    if-ge v0, v9, :cond_a

    .line 197
    aget-object v9, v8, v0

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 202
    :cond_b
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-le v0, v2, :cond_c

    .line 203
    invoke-virtual {v5, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 206
    :cond_c
    invoke-virtual {v5, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 213
    :cond_d
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->getSingleSelectedId()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 214
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->getSingleSelectedId()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    .line 215
    invoke-virtual {v0, v3}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->getSqlFilterSelection(Z)Ljava/lang/String;

    move-result-object v1

    .line 216
    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->getSqlFilterSelectionArgs()[Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_e
    move-object v1, v0

    move-object v0, v4

    goto :goto_5

    :cond_f
    move-object v0, v4

    goto :goto_5
.end method

.method public createMediaSet()Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 4

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->getSelection()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createExcludeSet(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;[J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public getIdFieldNamesCopy()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 58
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIdTypesCopy()[I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic instantiateId()Lcom/mfluent/asp/common/content/ContentId;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->instantiateId()Lcom/mfluent/asp/common/content/MultiColumnContentId;

    move-result-object v0

    return-object v0
.end method

.method public instantiateId()Lcom/mfluent/asp/common/content/MultiColumnContentId;
    .locals 3

    .prologue
    .line 237
    new-instance v0, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    iget-object v1, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    iget-object v2, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/common/content/MultiColumnContentId;-><init>([Ljava/lang/String;[I)V

    return-object v0
.end method

.method protected instantiateNewContentAdapter()Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mfluent/asp/common/content/ContentResolverContentAdapter",
            "<",
            "Lcom/mfluent/asp/common/content/MultiColumnContentId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    new-instance v0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;

    iget-object v1, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    iget-object v2, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;-><init>([Ljava/lang/String;[I)V

    return-object v0
.end method

.method public setIdFields([Ljava/lang/String;[I)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 88
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 89
    :cond_0
    new-instance v0, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    invoke-direct {v0, p1, p2}, Lcom/mfluent/asp/common/content/MultiColumnContentId;-><init>([Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    .line 90
    iget v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mSingleSelectedIndex:I

    if-ltz v0, :cond_1

    .line 91
    iget v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mSingleSelectedIndex:I

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 92
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    check-cast v0, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    iget-object v2, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->setValues(Landroid/database/Cursor;)V

    .line 98
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->isEditing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 99
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->getMultiSelectedRowIndexVector()Lcom/mfluent/asp/common/util/IntVector;

    move-result-object v2

    .line 100
    new-instance v3, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    move v0, v1

    .line 102
    :goto_1
    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 103
    invoke-virtual {v2, v0}, Lcom/mfluent/asp/common/util/IntVector;->elementAt(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 104
    new-instance v1, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    invoke-direct {v1, p1, p2}, Lcom/mfluent/asp/common/content/MultiColumnContentId;-><init>([Ljava/lang/String;[I)V

    .line 105
    iget-object v4, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->setValues(Landroid/database/Cursor;)V

    .line 106
    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 102
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 94
    :cond_3
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mSingleSelectedIndex:I

    goto :goto_0

    .line 109
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 110
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 112
    :cond_5
    iput-object p1, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    .line 113
    iput-object p2, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    .line 115
    const/4 v1, 0x1

    .line 118
    :cond_6
    return v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0, p1, p2}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->writeToParcel(Landroid/os/Parcel;I)V

    .line 129
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdFieldNames:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleMediaTypeContentAdapter;->mIdTypes:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 132
    return-void
.end method
