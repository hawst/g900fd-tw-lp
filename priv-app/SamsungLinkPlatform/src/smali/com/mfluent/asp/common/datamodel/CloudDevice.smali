.class public interface abstract Lcom/mfluent/asp/common/datamodel/CloudDevice;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final BROADCAST_DEVICE_NOW_ONLINE:Ljava/lang/String; = "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_NOW_ONLINE"

.field public static final BROADCAST_DEVICE_REFRESH:Ljava/lang/String; = "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_REFRESH"

.field public static final BROADCAST_DEVICE_STATE_CHANGE:Ljava/lang/String; = "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE"

.field public static final BROADCAST_DEVICE_STATE_CHANGE_WITH_DATA:Ljava/lang/String; = "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE_WITH_DATA"

.field public static final BYTES_PER_MEGABYTE:J = 0x100000L

.field public static final DEVICE_ID_EXTRA_KEY:Ljava/lang/String; = "DEVICE_ID_EXTRA_KEY"

.field public static final REFRESH_FROM_DOCUMENTS:I = 0x9

.field public static final REFRESH_FROM_FILES:I = 0x4

.field public static final REFRESH_FROM_HOME:I = 0x5

.field public static final REFRESH_FROM_KEY:Ljava/lang/String; = "REFRESH_FROM_KEY"

.field public static final REFRESH_FROM_LAUNCH:I = 0x7

.field public static final REFRESH_FROM_MUSIC:I = 0x2

.field public static final REFRESH_FROM_PHOTOS:I = 0x1

.field public static final REFRESH_FROM_RECENT:I = 0x6

.field public static final REFRESH_FROM_UNKNOWN:I = -0x1

.field public static final REFRESH_FROM_VIDEOS:I = 0x3

.field public static final REFRESH_NEW_ACCOUNT:I = 0x8


# virtual methods
.method public abstract buildDeviceIntentFilterForAction(Ljava/lang/String;)Landroid/content/IntentFilter;
.end method

.method public abstract deleteAllMetaData(Landroid/content/ContentResolver;)V
.end method

.method public abstract getCapacityInBytes()J
.end method

.method public abstract getId()I
.end method

.method public abstract getUsedCapacityInBytes()J
.end method

.method public abstract getWebStoragePw()Ljava/lang/String;
.end method

.method public abstract getWebStorageUserId()Ljava/lang/String;
.end method

.method public abstract isWebStorageSignedIn()Z
.end method

.method public abstract setCapacityInBytes(J)V
.end method

.method public abstract setUsedCapacityInBytes(J)V
.end method

.method public abstract setWebStorageSignedIn(Z)V
.end method

.method public abstract setWebStorageUserId(Ljava/lang/String;)V
.end method
