.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$GenresColumns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$GenresColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GenresColumns"
.end annotation


# static fields
.field public static final SOURCE_ALBUM_ID:Ljava/lang/String; = "source_album_id"

.field public static final SOURCE_MEDIA_ID:Ljava/lang/String; = "source_media_id"

.field public static final THUMBNAIL_URI:Ljava/lang/String; = "thumbnail_uri"
