.class public abstract Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final defaultValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final key:Ljava/lang/String;

.field private final lock:Ljava/util/concurrent/locks/ReadWriteLock;

.field private final prefs:Landroid/content/SharedPreferences;

.field private value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 25
    iput-object p1, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->prefs:Landroid/content/SharedPreferences;

    .line 26
    iput-object p2, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->key:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->defaultValue:Ljava/lang/Object;

    .line 29
    invoke-virtual {p0, p1, p2, p3}, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->getValue(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->value:Ljava/lang/Object;

    .line 30
    return-void
.end method

.method private setValueInner(Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/content/SharedPreferences$Editor;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->value:Ljava/lang/Object;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const/4 v0, 0x0

    .line 81
    :goto_0
    return v0

    .line 78
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->value:Ljava/lang/Object;

    .line 79
    iget-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->key:Ljava/lang/String;

    invoke-virtual {p0, p2, v0, p1}, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->putValue(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Object;)V

    .line 81
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getValue()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->value:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    iget-object v1, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method protected abstract getValue(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation
.end method

.method protected abstract putValue(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences$Editor;",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation
.end method

.method public resetToDefault()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->defaultValue:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->setValue(Ljava/lang/Object;)Z

    .line 86
    return-void
.end method

.method public resetToDefault(Landroid/content/SharedPreferences$Editor;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->defaultValue:Ljava/lang/Object;

    invoke-virtual {p0, v0, p1}, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->setValue(Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)Z

    .line 90
    return-void
.end method

.method public setValue(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 45
    .line 46
    iget-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 49
    invoke-direct {p0, p1, v0}, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->setValueInner(Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)Z

    move-result v1

    .line 50
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 55
    return v1

    .line 52
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public setValue(Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/content/SharedPreferences$Editor;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 62
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->setValueInner(Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 64
    iget-object v1, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 67
    return v0

    .line 64
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/common/util/prefs/PrefsPersistedField;->lock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
