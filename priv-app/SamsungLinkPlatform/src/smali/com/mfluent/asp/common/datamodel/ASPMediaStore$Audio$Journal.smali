.class public final Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Journal"
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String;

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final ENTRY_CONTENT_TYPE:Ljava/lang/String;

.field public static final PATH:Ljava/lang/String; = "audio_journal"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1113
    const-string v0, "audio_journal"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$6100(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;->CONTENT_TYPE:Ljava/lang/String;

    .line 1115
    const-string v0, "audio_journal"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$6200(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    .line 1117
    const-string v0, "audio_journal"

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1121
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public final getDeviceSyncedJournalColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1136
    const-string v0, "last_recv_music_journal_id"

    return-object v0
.end method

.method public final getJournalDroppedKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1126
    const-string v0, "audio_journal_dropped"

    return-object v0
.end method

.method public final getJournalSyncedKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1131
    const-string v0, "audio_journal_synced"

    return-object v0
.end method
