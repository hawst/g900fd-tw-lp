.class public Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;
.super Ljava/io/OutputStream;
.source "SourceFile"


# instance fields
.field private final outstream:Ljava/io/OutputStream;

.field private final streamProgressListener:Lcom/mfluent/asp/common/io/util/StreamProgressListener;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;Lcom/mfluent/asp/common/io/util/StreamProgressListener;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;->outstream:Ljava/io/OutputStream;

    .line 21
    if-nez p2, :cond_0

    .line 22
    new-instance p2, Lcom/mfluent/asp/common/io/util/NullStreamProgressListener;

    invoke-direct {p2}, Lcom/mfluent/asp/common/io/util/NullStreamProgressListener;-><init>()V

    .line 24
    :cond_0
    iput-object p2, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;->streamProgressListener:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    .line 25
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;->outstream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 53
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;->outstream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 48
    return-void
.end method

.method public write(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;->outstream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 30
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;->streamProgressListener:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    const-wide/16 v2, 0x1

    invoke-interface {v0, v2, v3}, Lcom/mfluent/asp/common/io/util/StreamProgressListener;->bytesTransferred(J)V

    .line 31
    return-void
.end method

.method public write([B)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;->outstream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 36
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;->streamProgressListener:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    array-length v1, p1

    int-to-long v2, v1

    invoke-interface {v0, v2, v3}, Lcom/mfluent/asp/common/io/util/StreamProgressListener;->bytesTransferred(J)V

    .line 37
    return-void
.end method

.method public write([BII)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;->outstream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 42
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;->streamProgressListener:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    int-to-long v2, p3

    invoke-interface {v0, v2, v3}, Lcom/mfluent/asp/common/io/util/StreamProgressListener;->bytesTransferred(J)V

    .line 43
    return-void
.end method
