.class public final Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$FileColumns;
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$KeywordColumns;
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Keywords"
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String;

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final ENTRY_CONTENT_TYPE:Ljava/lang/String;

.field public static final PATH:Ljava/lang/String; = "file_keywords"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1294
    const-string v0, "file_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$7900(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->CONTENT_TYPE:Ljava/lang/String;

    .line 1296
    const-string v0, "file_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$8000(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    .line 1298
    const-string v0, "file_keywords"

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContentUriForDevice(I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1301
    int-to-long v0, p0

    const-string v2, "file_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$8100(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getEntryUri(I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1305
    int-to-long v0, p0

    const-string v2, "file_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$8200(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getGroupByFileIdUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1309
    const-string v0, "file_keywords"

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGroupByFileIdUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getOrphanCleanUriForDevice(I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1313
    int-to-long v0, p0

    const-string v2, "file_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildOrphanCleanupUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$3300(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final instanceGetContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1323
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public final instanceGetContentUriForDevice(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1328
    const-string v0, "file_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$8400(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final instanceGetEntryUri(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1318
    const-string v0, "file_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$8300(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
