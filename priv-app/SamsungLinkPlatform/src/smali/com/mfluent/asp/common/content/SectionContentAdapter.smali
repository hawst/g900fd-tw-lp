.class public interface abstract Lcom/mfluent/asp/common/content/SectionContentAdapter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/database/Cursor;
.implements Landroid/os/Parcelable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ParcelCreator"
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/mfluent/asp/common/content/ContentId;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/database/Cursor;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final COUNT_FIELD:Ljava/lang/String; = "_count"


# virtual methods
.method public abstract acquireSourceReadLock()V
.end method

.method public abstract cancelLoad(Ljava/lang/Object;)V
.end method

.method public abstract destroy()V
.end method

.method public abstract finishLoad(Ljava/lang/Object;)V
.end method

.method public abstract getNumRowsInSection(I)I
.end method

.method public abstract getSectionEndRow(I)I
.end method

.method public abstract getSectionForRow(I)I
.end method

.method public abstract getSectionStartRow(I)I
.end method

.method public abstract hasLoadToFinish(Ljava/lang/Object;)Z
.end method

.method public abstract initContext(Landroid/content/Context;)V
.end method

.method public abstract isDataLoaded()Z
.end method

.method public abstract isDestroyed()Z
.end method

.method public abstract isSectionCollapsed(I)Z
.end method

.method public abstract loadData(Landroid/database/Cursor;)Ljava/lang/Object;
.end method

.method public abstract releaseSourceReadLock()V
.end method

.method public abstract setCountFieldName(Ljava/lang/String;)Z
.end method

.method public abstract setSectionCollapsed(IZ)Z
.end method
