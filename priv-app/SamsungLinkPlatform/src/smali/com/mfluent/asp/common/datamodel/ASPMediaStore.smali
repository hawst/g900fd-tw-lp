.class public final Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.super Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$GeoLocation;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferSessions;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferSessionColumns;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$AllMediaSearch;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$TemporaryFiles;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$ReadLock;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$ThumbnailCache;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DateTakenGroupingColumns;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$MediaColumns;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrityColumns;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$KeywordColumns;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$GeolocationColumns;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$BaseASPColumns;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DeviceColumns;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$MediaTypes;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$CallMethods;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;
    }
.end annotation


# static fields
.field public static final DEFAULT_THUMBNAIL_HEIGHT:I = 0x180

.field public static final DEFAULT_THUMBNAIL_WIDTH:I = 0x200

.field private static LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mfl_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->TAG:Ljava/lang/String;

    .line 32
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_MEDIA_PROVIDER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;-><init>()V

    .line 1525
    return-void
.end method

.method static synthetic access$000(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2000(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUriBestDevice(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2300(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2400(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2500(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2600(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2700(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3000(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3100(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3200(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3300(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildOrphanCleanupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3400(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3500(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3600(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3700(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3800(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3900(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4000(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4100(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4200(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUriBestDevice(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4300(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4400(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4500(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4600()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method

.method static synthetic access$4700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4800(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4900(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUriBestDevice(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5000(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUriBestDevice(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5100(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5200(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5300(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5400(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5500(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5600(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5700(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5800(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5900(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6000(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6100(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6200(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6300(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6400(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUriBestDevice(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6500(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6600(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6700(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6800(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6900(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7000(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7100(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7200(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7300(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7400(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7500(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7600(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7700(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7800(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7900(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8000(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8100(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8200(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8300(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8400(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8500(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8600(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8700(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8800(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8900(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9000(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9100(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9200(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9300(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9400(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9500(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    invoke-static {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9600(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1, p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected static final buildGroupByFileIdUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://com.samsung.android.sdk.samsunglink.provider.SLinkMedia/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/file_id_grouping"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static final buildOrphanCleanupUri(JLjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://com.samsung.android.sdk.samsunglink.provider.SLinkMedia/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/cleanup/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
