.class public Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/content/BaseContentAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ListenerState"
.end annotation


# instance fields
.field public mLastNotification:J

.field public final mListener:Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;

.field public mPaused:Z

.field public mScheduled:Z

.field public mThottle:J


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mLastNotification:J

    .line 38
    iput-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mScheduled:Z

    .line 39
    iput-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mPaused:Z

    .line 42
    iput-object p1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mListener:Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;

    .line 43
    return-void
.end method
