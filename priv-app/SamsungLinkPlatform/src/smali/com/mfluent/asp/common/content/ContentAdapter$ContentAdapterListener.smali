.class public interface abstract Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/content/ContentAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContentAdapterListener"
.end annotation


# virtual methods
.method public abstract onDataLoaded(Lcom/mfluent/asp/common/content/ContentAdapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/common/content/ContentAdapter",
            "<*>;)V"
        }
    .end annotation
.end method

.method public abstract onDataSetChanged(Lcom/mfluent/asp/common/content/ContentAdapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/common/content/ContentAdapter",
            "<*>;)V"
        }
    .end annotation
.end method

.method public abstract onDataSetInvalidated(Lcom/mfluent/asp/common/content/ContentAdapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/common/content/ContentAdapter",
            "<*>;)V"
        }
    .end annotation
.end method

.method public abstract onLoadFailed(Lcom/mfluent/asp/common/content/ContentAdapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/common/content/ContentAdapter",
            "<*>;)V"
        }
    .end annotation
.end method
