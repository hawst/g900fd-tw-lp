.class public Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x58a2a70210ee585fL


# instance fields
.field private artistId:I

.field private contentId:I

.field private data:Ljava/lang/String;

.field private desiredBitmapHeight:I

.field private desiredBitmapWidth:I

.field private desiredHeight:I

.field private desiredWidth:I

.field private deviceId:I

.field private fileName:Ljava/lang/String;

.field private fullHeight:I

.field private fullWidth:I

.field private mediaType:I

.field private orientation:I

.field private remoteUri:Ljava/lang/String;

.field private sourceAlbumId:Ljava/lang/String;

.field private sourceMediaId:Ljava/lang/String;

.field private sourceThumbHeight:I

.field private sourceThumbWidth:I

.field private thumbData:Ljava/lang/String;

.field private thumbHeight:I

.field private thumbUri:Ljava/lang/String;

.field private thumbWidth:I

.field private thumbnailSize:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

.field private transportType:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->transportType:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 76
    sget-object v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->MINI:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbnailSize:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    .line 145
    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->transportType:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 76
    sget-object v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->MINI:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbnailSize:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    .line 148
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->artistId:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->artistId:I

    .line 149
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->contentId:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->contentId:I

    .line 150
    iget-object v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->data:Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->data:Ljava/lang/String;

    .line 151
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapHeight:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapHeight:I

    .line 152
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapWidth:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapWidth:I

    .line 153
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredHeight:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredHeight:I

    .line 154
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredWidth:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredWidth:I

    .line 155
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullWidth:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullWidth:I

    .line 156
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullHeight:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullHeight:I

    .line 157
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbWidth:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbWidth:I

    .line 158
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbHeight:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbHeight:I

    .line 159
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->deviceId:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->deviceId:I

    .line 160
    iget-object v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fileName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fileName:Ljava/lang/String;

    .line 161
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->mediaType:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->mediaType:I

    .line 162
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->orientation:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->orientation:I

    .line 163
    iget-object v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->remoteUri:Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->remoteUri:Ljava/lang/String;

    .line 164
    iget-object v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceAlbumId:Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceAlbumId:Ljava/lang/String;

    .line 165
    iget-object v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceMediaId:Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceMediaId:Ljava/lang/String;

    .line 166
    iget-object v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbnailSize:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbnailSize:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    .line 167
    iget-object v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->transportType:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->transportType:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 168
    iget-object v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbUri:Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbUri:Ljava/lang/String;

    .line 169
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceThumbWidth:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceThumbWidth:I

    .line 170
    iget v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceThumbHeight:I

    iput v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceThumbHeight:I

    .line 171
    iget-object v0, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbData:Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbData:Ljava/lang/String;

    .line 172
    return-void
.end method

.method public static fromCursor(Landroid/database/Cursor;)Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;
    .locals 2

    .prologue
    .line 91
    new-instance v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;-><init>()V

    .line 93
    const-string v1, "media_type"

    invoke-static {p0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setMediaType(I)V

    .line 94
    invoke-static {v0, p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->initFromCursor(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/database/Cursor;)V

    .line 96
    return-object v0
.end method

.method public static fromCursor(Landroid/database/Cursor;I)Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;-><init>()V

    .line 84
    invoke-virtual {v0, p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setMediaType(I)V

    .line 85
    invoke-static {v0, p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->initFromCursor(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/database/Cursor;)V

    .line 87
    return-object v0
.end method

.method private static initFromCursor(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/16 v4, 0xc

    const/4 v1, 0x1

    .line 100
    const-string v0, "_id"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setContentId(I)V

    .line 101
    const-string v0, "device_id"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setDeviceId(I)V

    .line 102
    const-string v0, "source_media_id"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setSourceMediaId(Ljava/lang/String;)V

    .line 103
    const-string v0, "full_uri"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setRemoteUri(Ljava/lang/String;)V

    .line 104
    const-string v0, "_data"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setData(Ljava/lang/String;)V

    .line 105
    const-string v0, "_display_name"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setFileName(Ljava/lang/String;)V

    .line 106
    const-string v0, "thumb_data"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setThumbData(Ljava/lang/String;)V

    .line 107
    const-string v0, "thumb_width"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setSourceThumbWidth(I)V

    .line 108
    const-string v0, "thumb_height"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setSourceThumbHeight(I)V

    .line 109
    const-string v0, "transport_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 110
    if-ltz v0, :cond_0

    .line 111
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setDeviceTransportType(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V

    .line 114
    :cond_0
    const/4 v0, 0x0

    .line 115
    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getMediaType()I

    move-result v2

    if-ne v2, v4, :cond_3

    .line 116
    const-string v1, "_id"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setContentId(I)V

    .line 117
    const-string v1, "thumbnail_uri"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setRemoteUri(Ljava/lang/String;)V

    .line 118
    const-string v1, "source_album_id"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setSourceAlbumId(Ljava/lang/String;)V

    .line 119
    const-string v1, "artist_id"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setArtistId(I)V

    .line 135
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 136
    const-string v0, "width"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setFullWidth(I)V

    .line 137
    const-string v0, "height"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setFullHeight(I)V

    .line 139
    :cond_2
    return-void

    .line 120
    :cond_3
    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getMediaType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 121
    const-string v1, "album_id"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setContentId(I)V

    .line 122
    invoke-virtual {p0, v4}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setMediaType(I)V

    .line 123
    const-string v1, "thumbnail_uri"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setRemoteUri(Ljava/lang/String;)V

    .line 124
    const-string v1, "source_album_id"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setSourceAlbumId(Ljava/lang/String;)V

    .line 125
    const-string v1, "artist_id"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setArtistId(I)V

    goto :goto_0

    .line 126
    :cond_4
    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getMediaType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    .line 127
    const-string v0, "thumbnail_uri"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setRemoteUri(Ljava/lang/String;)V

    move v0, v1

    .line 128
    goto :goto_0

    .line 129
    :cond_5
    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getMediaType()I

    move-result v2

    if-ne v2, v1, :cond_1

    .line 130
    const-string v0, "thumbnail_uri"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setThumbUri(Ljava/lang/String;)V

    .line 131
    const-string v0, "orientation"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setOrientation(I)V

    move v0, v1

    .line 132
    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 491
    if-nez p1, :cond_0

    .line 492
    const/4 v0, 0x0

    .line 496
    :goto_0
    return v0

    .line 495
    :cond_0
    check-cast p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    .line 496
    new-instance v0, Lorg/apache/commons/lang3/builder/EqualsBuilder;

    invoke-direct {v0}, Lorg/apache/commons/lang3/builder/EqualsBuilder;-><init>()V

    iget v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->contentId:I

    iget v2, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->contentId:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/EqualsBuilder;->append(II)Lorg/apache/commons/lang3/builder/EqualsBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->artistId:I

    iget v2, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->artistId:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/EqualsBuilder;->append(II)Lorg/apache/commons/lang3/builder/EqualsBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->mediaType:I

    iget v2, p1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->mediaType:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/EqualsBuilder;->append(II)Lorg/apache/commons/lang3/builder/EqualsBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/lang3/builder/EqualsBuilder;->isEquals()Z

    move-result v0

    goto :goto_0
.end method

.method public getArtistId()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->artistId:I

    return v0
.end method

.method public getContentId()I
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->contentId:I

    return v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getDesiredBitmapHeight()I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapHeight:I

    if-nez v0, :cond_0

    .line 252
    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredHeight()I

    move-result v0

    .line 254
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapHeight:I

    goto :goto_0
.end method

.method public getDesiredBitmapWidth()I
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapWidth:I

    if-nez v0, :cond_0

    .line 231
    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredWidth()I

    move-result v0

    .line 233
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapWidth:I

    goto :goto_0
.end method

.method public getDesiredHeight()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredHeight:I

    return v0
.end method

.method public getDesiredWidth()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredWidth:I

    return v0
.end method

.method public getDeviceId()I
    .locals 1

    .prologue
    .line 349
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->deviceId:I

    return v0
.end method

.method public getDeviceTransportType()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->transportType:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFullHeight()I
    .locals 1

    .prologue
    .line 311
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullHeight:I

    return v0
.end method

.method public getFullWidth()I
    .locals 1

    .prologue
    .line 307
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullWidth:I

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 272
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->mediaType:I

    return v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 334
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->orientation:I

    return v0
.end method

.method public getRemoteUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->remoteUri:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceAlbumId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceMediaId:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceThumbHeight()I
    .locals 1

    .prologue
    .line 432
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceThumbHeight:I

    return v0
.end method

.method public getSourceThumbWidth()I
    .locals 1

    .prologue
    .line 424
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceThumbWidth:I

    return v0
.end method

.method public getThumbData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbData:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbHeight()I
    .locals 1

    .prologue
    .line 327
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbHeight:I

    return v0
.end method

.method public getThumbUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbUri:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbWidth()I
    .locals 1

    .prologue
    .line 323
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbWidth:I

    return v0
.end method

.method public getThumbnailSize()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbnailSize:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 508
    new-instance v0, Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    invoke-direct {v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;-><init>()V

    iget v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->contentId:I

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(I)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->artistId:I

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(I)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->mediaType:I

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(I)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->toHashCode()I

    move-result v0

    return v0
.end method

.method public setArtistId(I)V
    .locals 0

    .prologue
    .line 223
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->artistId:I

    .line 224
    return-void
.end method

.method public setContentId(I)V
    .locals 0

    .prologue
    .line 295
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->contentId:I

    .line 296
    return-void
.end method

.method public setData(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->data:Ljava/lang/String;

    .line 460
    return-void
.end method

.method public setDesiredBitmapHeight(I)V
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullHeight:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullHeight:I

    if-ge v0, p1, :cond_0

    .line 263
    iget p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullHeight:I

    .line 265
    :cond_0
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapHeight:I

    .line 266
    return-void
.end method

.method public setDesiredBitmapWidth(I)V
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullWidth:I

    if-ge v0, p1, :cond_0

    .line 242
    iget p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullWidth:I

    .line 244
    :cond_0
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapWidth:I

    .line 245
    return-void
.end method

.method public setDesiredHeight(I)V
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullHeight:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullHeight:I

    if-ge v0, p1, :cond_0

    .line 213
    iget p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullHeight:I

    .line 215
    :cond_0
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredHeight:I

    .line 216
    return-void
.end method

.method public setDesiredWidth(I)V
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullWidth:I

    if-ge v0, p1, :cond_0

    .line 187
    iget p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullWidth:I

    .line 189
    :cond_0
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredWidth:I

    .line 190
    return-void
.end method

.method public setDeviceId(I)V
    .locals 0

    .prologue
    .line 357
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->deviceId:I

    .line 358
    return-void
.end method

.method public setDeviceTransportType(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->transportType:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 201
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 482
    iput-object p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fileName:Ljava/lang/String;

    .line 483
    return-void
.end method

.method public setFullHeight(I)V
    .locals 0

    .prologue
    .line 303
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullHeight:I

    .line 304
    return-void
.end method

.method public setFullWidth(I)V
    .locals 0

    .prologue
    .line 299
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fullWidth:I

    .line 300
    return-void
.end method

.method public setMediaType(I)V
    .locals 0

    .prologue
    .line 280
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->mediaType:I

    .line 281
    return-void
.end method

.method public setOrientation(I)V
    .locals 0

    .prologue
    .line 342
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->orientation:I

    .line 343
    return-void
.end method

.method public setRemoteUri(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->remoteUri:Ljava/lang/String;

    .line 388
    return-void
.end method

.method public setSourceAlbumId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 467
    iput-object p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceAlbumId:Ljava/lang/String;

    .line 468
    return-void
.end method

.method public setSourceMediaId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceMediaId:Ljava/lang/String;

    .line 373
    return-void
.end method

.method public setSourceThumbHeight(I)V
    .locals 0

    .prologue
    .line 436
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceThumbHeight:I

    .line 437
    return-void
.end method

.method public setSourceThumbWidth(I)V
    .locals 0

    .prologue
    .line 428
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceThumbWidth:I

    .line 429
    return-void
.end method

.method public setThumbData(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbData:Ljava/lang/String;

    .line 421
    return-void
.end method

.method public setThumbHeight(I)V
    .locals 0

    .prologue
    .line 319
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbHeight:I

    .line 320
    return-void
.end method

.method public setThumbUri(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbUri:Ljava/lang/String;

    .line 445
    return-void
.end method

.method public setThumbWidth(I)V
    .locals 0

    .prologue
    .line 315
    iput p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbWidth:I

    .line 316
    return-void
.end method

.method public setThumbnailSize(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;)V
    .locals 0

    .prologue
    .line 402
    iput-object p1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->thumbnailSize:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    .line 403
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 520
    new-instance v0, Lorg/apache/commons/lang3/builder/ToStringBuilder;

    sget-object v1, Lorg/apache/commons/lang3/builder/ToStringStyle;->SHORT_PREFIX_STYLE:Lorg/apache/commons/lang3/builder/ToStringStyle;

    invoke-direct {v0, p0, v1}, Lorg/apache/commons/lang3/builder/ToStringBuilder;-><init>(Ljava/lang/Object;Lorg/apache/commons/lang3/builder/ToStringStyle;)V

    .line 521
    const-string v1, "type"

    iget v2, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->mediaType:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;I)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v1

    const-string v2, "id"

    iget v3, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->contentId:I

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;I)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v1

    const-string v2, "source"

    iget-object v3, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->sourceMediaId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    .line 523
    iget v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredWidth:I

    if-eqz v1, :cond_0

    .line 524
    const-string v1, "fetch_w"

    iget v2, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredWidth:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;I)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v1

    const-string v2, "fetch_h"

    iget v3, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredHeight:I

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;I)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    .line 526
    :cond_0
    iget v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapWidth:I

    if-eqz v1, :cond_1

    .line 527
    const-string v1, "render_w"

    iget v2, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapWidth:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;I)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v1

    const-string v2, "render_h"

    iget v3, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->desiredBitmapHeight:I

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;I)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    .line 530
    :cond_1
    iget v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->orientation:I

    if-lez v1, :cond_2

    .line 531
    const-string v1, "r"

    iget v2, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->orientation:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;I)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    .line 534
    :cond_2
    iget-object v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->data:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->data:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 535
    const-string v1, "data"

    iget-object v2, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->data:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    .line 540
    :cond_3
    :goto_0
    invoke-virtual {v0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 536
    :cond_4
    iget-object v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fileName:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fileName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 537
    const-string v1, "name"

    iget-object v2, p0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fileName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    goto :goto_0
.end method
