.class public Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;
.super Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Files"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$KeywordList;,
        Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$FileColumns;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1265
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;-><init>()V

    .line 1290
    return-void
.end method

.method public static getGroupByDeviceUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1348
    const-string v0, "files"

    const-string v1, "device_id"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$8700(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public instanceGetContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1339
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public instanceGetContentUriForDevice(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1344
    const-string v0, "files"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$8600(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public instanceGetEntryUri(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1334
    const-string v0, "files"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$8500(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
