.class final Lcom/mfluent/asp/common/util/IntVector$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/util/IntVector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/mfluent/asp/common/util/IntVector;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Lcom/mfluent/asp/common/util/IntVector;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 29
    if-le v0, v1, :cond_0

    .line 30
    new-instance v1, Landroid/os/BadParcelableException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid version in parcel: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 32
    :cond_0
    new-instance v3, Lcom/mfluent/asp/common/util/IntVector;

    const/4 v0, 0x0

    invoke-direct {v3, v0}, Lcom/mfluent/asp/common/util/IntVector;-><init>(Lcom/mfluent/asp/common/util/IntVector$1;)V

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    # setter for: Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z
    invoke-static {v3, v0}, Lcom/mfluent/asp/common/util/IntVector;->access$102(Lcom/mfluent/asp/common/util/IntVector;Z)Z

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    # setter for: Lcom/mfluent/asp/common/util/IntVector;->m_increment:I
    invoke-static {v3, v0}, Lcom/mfluent/asp/common/util/IntVector;->access$202(Lcom/mfluent/asp/common/util/IntVector;I)I

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    # setter for: Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z
    invoke-static {v3, v0}, Lcom/mfluent/asp/common/util/IntVector;->access$302(Lcom/mfluent/asp/common/util/IntVector;Z)Z

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_2
    # setter for: Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z
    invoke-static {v3, v1}, Lcom/mfluent/asp/common/util/IntVector;->access$102(Lcom/mfluent/asp/common/util/IntVector;Z)Z

    .line 39
    invoke-virtual {v3, v4}, Lcom/mfluent/asp/common/util/IntVector;->ensureCapacity(I)V

    .line 40
    :goto_3
    if-ge v2, v4, :cond_4

    .line 41
    # getter for: Lcom/mfluent/asp/common/util/IntVector;->m_array:[I
    invoke-static {v3}, Lcom/mfluent/asp/common/util/IntVector;->access$400(Lcom/mfluent/asp/common/util/IntVector;)[I

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aput v1, v0, v2

    .line 40
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_1
    move v0, v2

    .line 34
    goto :goto_0

    :cond_2
    move v0, v2

    .line 37
    goto :goto_1

    :cond_3
    move v1, v2

    .line 38
    goto :goto_2

    .line 43
    :cond_4
    # setter for: Lcom/mfluent/asp/common/util/IntVector;->m_index:I
    invoke-static {v3, v4}, Lcom/mfluent/asp/common/util/IntVector;->access$502(Lcom/mfluent/asp/common/util/IntVector;I)I

    .line 45
    return-object v3
.end method

.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/util/IntVector$1;->createFromParcel(Landroid/os/Parcel;)Lcom/mfluent/asp/common/util/IntVector;

    move-result-object v0

    return-object v0
.end method

.method public final newArray(I)[Lcom/mfluent/asp/common/util/IntVector;
    .locals 1

    .prologue
    .line 50
    new-array v0, p1, [Lcom/mfluent/asp/common/util/IntVector;

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/util/IntVector$1;->newArray(I)[Lcom/mfluent/asp/common/util/IntVector;

    move-result-object v0

    return-object v0
.end method
