.class public Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;
.super Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DeviceColumns;
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Device"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;-><init>()V

    return-void
.end method


# virtual methods
.method public instanceGetContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 310
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public instanceGetContentUriForDevice(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 315
    const-string v0, "device"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$100(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public instanceGetEntryUri(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 305
    const-string v0, "device"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$000(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
