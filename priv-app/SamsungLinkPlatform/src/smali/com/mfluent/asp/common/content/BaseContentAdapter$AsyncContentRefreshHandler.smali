.class public Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;
.super Landroid/os/Handler;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/content/BaseContentAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "AsyncContentRefreshHandler"
.end annotation


# static fields
.field public static final EVENT_DATA_CHANGED_ALL:I = 0x7d165

.field public static final EVENT_DATA_CHANGED_SINGLE:I = 0x7d166

.field public static final EVENT_DATA_LOADED:I = 0x7d164

.field public static final EVENT_LOAD_FAILED:I = 0x7d167


# instance fields
.field private final mContentAdapter:Lcom/mfluent/asp/common/content/BaseContentAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mfluent/asp/common/content/BaseContentAdapter",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/common/content/BaseContentAdapter;Landroid/os/Looper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/common/content/BaseContentAdapter",
            "<*>;",
            "Landroid/os/Looper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 920
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 921
    iput-object p1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->mContentAdapter:Lcom/mfluent/asp/common/content/BaseContentAdapter;

    .line 922
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 934
    const v0, 0x7d165

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->removeMessages(I)V

    .line 935
    const v0, 0x7d164

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->removeMessages(I)V

    .line 936
    const v0, 0x7d166

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->removeMessages(I)V

    .line 937
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 941
    .line 943
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    move v0, v2

    .line 976
    :goto_1
    if-nez v0, :cond_0

    .line 977
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 979
    :cond_0
    return-void

    .line 946
    :pswitch_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->mContentAdapter:Lcom/mfluent/asp/common/content/BaseContentAdapter;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->hasDataLoadToFinish(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 947
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->mContentAdapter:Lcom/mfluent/asp/common/content/BaseContentAdapter;

    # invokes: Lcom/mfluent/asp/common/content/BaseContentAdapter;->notifyListenerDataSetInvalidated()V
    invoke-static {v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->access$000(Lcom/mfluent/asp/common/content/BaseContentAdapter;)V

    .line 948
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->mContentAdapter:Lcom/mfluent/asp/common/content/BaseContentAdapter;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->finishDataLoad(Ljava/lang/Object;)V

    .line 950
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->mContentAdapter:Lcom/mfluent/asp/common/content/BaseContentAdapter;

    # invokes: Lcom/mfluent/asp/common/content/BaseContentAdapter;->notifyListenersDataLoaded()V
    invoke-static {v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->access$100(Lcom/mfluent/asp/common/content/BaseContentAdapter;)V

    :goto_2
    move v0, v1

    .line 955
    goto :goto_1

    .line 952
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->mContentAdapter:Lcom/mfluent/asp/common/content/BaseContentAdapter;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->cancelLoad(Ljava/lang/Object;)V

    goto :goto_2

    .line 958
    :pswitch_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->mContentAdapter:Lcom/mfluent/asp/common/content/BaseContentAdapter;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->notifyListenersDataSetChanged()V

    move v0, v1

    .line 960
    goto :goto_1

    .line 963
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;

    .line 964
    iget-object v3, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mListener:Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;

    iget-object v4, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->mContentAdapter:Lcom/mfluent/asp/common/content/BaseContentAdapter;

    invoke-interface {v3, v4}, Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;->onDataSetChanged(Lcom/mfluent/asp/common/content/ContentAdapter;)V

    .line 965
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mLastNotification:J

    .line 966
    iput-boolean v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mScheduled:Z

    move v0, v1

    .line 968
    goto :goto_1

    .line 971
    :pswitch_3
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->mContentAdapter:Lcom/mfluent/asp/common/content/BaseContentAdapter;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->notifyListenersLoadFailed()V

    goto :goto_0

    .line 943
    nop

    :pswitch_data_0
    .packed-switch 0x7d164
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public handleMessageOrQueue(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 925
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 926
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->handleMessage(Landroid/os/Message;)V

    .line 927
    invoke-virtual {p1}, Landroid/os/Message;->recycle()V

    .line 931
    :goto_0
    return-void

    .line 929
    :cond_0
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
