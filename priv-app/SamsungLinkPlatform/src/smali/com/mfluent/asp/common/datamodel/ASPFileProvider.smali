.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPFileProvider;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public varargs abstract deleteFiles(Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;[Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getApplicationContext()Landroid/content/Context;
.end method

.method public abstract getCloudDevice()Lcom/mfluent/asp/common/datamodel/CloudDevice;
.end method

.method public abstract getCloudStorageFileBrowser(Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/common/datamodel/ASPFileSortType;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method
