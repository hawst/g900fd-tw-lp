.class public final Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;
.super Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Documents$Media;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$DocumentColumns;
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Media"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1147
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Documents$Media;-><init>()V

    return-void
.end method

.method public static getCrossDeviceGroupingUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1158
    const-string v0, "document_cross_device"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$6500(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getGeneralGroupingUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1150
    const-string v0, "document"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$6300(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getGeneralGroupingUriBestDevice(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1154
    const-string v0, "document"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUriBestDevice(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$6400(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final instanceGetContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1168
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public final instanceGetContentUriForDevice(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1173
    const-string v0, "document"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$6700(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final instanceGetEntryUri(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1163
    const-string v0, "document"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$6600(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
