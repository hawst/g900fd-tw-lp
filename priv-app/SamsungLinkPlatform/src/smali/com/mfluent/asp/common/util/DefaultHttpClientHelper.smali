.class public Lcom/mfluent/asp/common/util/DefaultHttpClientHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/common/util/DefaultHttpClientHelper$GzipDecompressingEntity;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    return-void
.end method

.method public static addGzipCompressionSupport(Lorg/apache/http/impl/client/DefaultHttpClient;)V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/mfluent/asp/common/util/DefaultHttpClientHelper$1;

    invoke-direct {v0}, Lcom/mfluent/asp/common/util/DefaultHttpClientHelper$1;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 33
    new-instance v0, Lcom/mfluent/asp/common/util/DefaultHttpClientHelper$2;

    invoke-direct {v0}, Lcom/mfluent/asp/common/util/DefaultHttpClientHelper$2;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 52
    return-void
.end method
