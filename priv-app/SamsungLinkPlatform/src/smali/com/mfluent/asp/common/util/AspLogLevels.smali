.class public Lcom/mfluent/asp/common/util/AspLogLevels;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    }
.end annotation


# static fields
.field public static ASP_NATIVE_AGENT_LOG_ENABLE:Z

.field public static BUGSENSE_LOGGING:Z

.field public static LOGLEVEL_BROADCAST:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_CACHE:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_DLNA:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_DWS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_HTTPINPUTSTREAM:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_HTTPSERVER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_MEDIAPLAYER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_MEDIA_PROVIDER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_NTS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_SYNC:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_TRANSPORT:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_VERSION:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOGLEVEL_VIEW:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field public static LOG_FILENAME:Ljava/lang/String;

.field public static SAVE_LOG_ON_CRASH:Z

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 17
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mfl_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mfluent/asp/common/util/AspLogLevels;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->TAG:Ljava/lang/String;

    .line 21
    sput-boolean v2, Lcom/mfluent/asp/common/util/AspLogLevels;->BUGSENSE_LOGGING:Z

    sput-boolean v2, Lcom/mfluent/asp/common/util/AspLogLevels;->SAVE_LOG_ON_CRASH:Z

    .line 25
    sput-boolean v2, Lcom/mfluent/asp/common/util/AspLogLevels;->ASP_NATIVE_AGENT_LOG_ENABLE:Z

    .line 27
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(I)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 29
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_VIEW:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 31
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CACHE:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 33
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_TRANSPORT:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 35
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_BROADCAST:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 37
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_DWS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 39
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_MEDIA_PROVIDER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 41
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_SYNC:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 43
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 45
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_MEDIAPLAYER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 47
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_DLNA:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 49
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_HTTPSERVER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 51
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_HTTPINPUTSTREAM:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 53
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_NTS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 55
    new-instance v0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;-><init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_VERSION:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 57
    const-class v0, Lcom/mfluent/asp/common/util/AspLogLevels;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOG_FILENAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    return-void
.end method

.method public static loadLogLevels(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 64
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOG_FILENAME:Ljava/lang/String;

    invoke-virtual {p0, v0, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_0

    const-string v1, "LOGLEVEL_GENERAL"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v6, :cond_3

    .line 68
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOG_FILENAME:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 69
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "../shared_prefs"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 72
    sget-object v3, Lcom/mfluent/asp/common/util/AspLogLevels;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "loadLogLevels: using external: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mfluent/asp/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {v1, v2}, Lorg/apache/commons/io/FileUtils;->copyFileToDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 74
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOG_FILENAME:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 78
    :cond_1
    :goto_0
    if-nez v0, :cond_4

    .line 112
    :cond_2
    :goto_1
    return-void

    .line 82
    :cond_3
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "loadLogLevels: loading: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/mfluent/asp/common/util/AspLogLevels;->LOG_FILENAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mfluent/asp/common/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_4
    const-string v1, "BUGSENSE_LOGGING"

    sget-boolean v2, Lcom/mfluent/asp/common/util/AspLogLevels;->BUGSENSE_LOGGING:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/mfluent/asp/common/util/AspLogLevels;->BUGSENSE_LOGGING:Z

    .line 86
    const-string v1, "SAVE_LOG_ON_CRASH"

    sget-boolean v2, Lcom/mfluent/asp/common/util/AspLogLevels;->SAVE_LOG_ON_CRASH:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/mfluent/asp/common/util/AspLogLevels;->SAVE_LOG_ON_CRASH:Z

    .line 87
    const-string v1, "ASP_NATIVE_AGENT_LOG_ENABLE"

    sget-boolean v2, Lcom/mfluent/asp/common/util/AspLogLevels;->ASP_NATIVE_AGENT_LOG_ENABLE:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/mfluent/asp/common/util/AspLogLevels;->ASP_NATIVE_AGENT_LOG_ENABLE:Z

    .line 89
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_GENERAL"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 91
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_VIEW:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_VIEW"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 92
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CACHE:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_CACHE"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 93
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_TRANSPORT:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_TRANSPORT"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 94
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_BROADCAST:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_BROADCAST"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 95
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_DWS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_DWS"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 96
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_MEDIA_PROVIDER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_MEDIA_PROVIDER"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 97
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_SYNC:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_SYNC"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 98
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_CLOUD"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 99
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_MEDIAPLAYER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_MEDIAPLAYER"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 100
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_DLNA:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_DLNA"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 102
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_HTTPSERVER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_HTTPSERVER"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 103
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_HTTPINPUTSTREAM:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_HTTPINPUTSTREAM"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 104
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_NTS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const-string v2, "LOGLEVEL_NTS"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->setValue(I)V

    .line 106
    const-string v1, "STRICT_MODE"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 107
    if-eqz v0, :cond_2

    .line 109
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectDiskWrites()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 110
    new-instance v0, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->detectAll()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    goto/16 :goto_1

    :catch_0
    move-exception v1

    goto/16 :goto_0
.end method

.method public static saveLogLevels(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 118
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOG_FILENAME:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 119
    if-nez v1, :cond_0

    .line 162
    :goto_0
    return-void

    .line 123
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 125
    const-string v3, "BUGSENSE_LOGGING"

    sget-boolean v4, Lcom/mfluent/asp/common/util/AspLogLevels;->BUGSENSE_LOGGING:Z

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 126
    const-string v3, "SAVE_LOG_ON_CRASH"

    sget-boolean v4, Lcom/mfluent/asp/common/util/AspLogLevels;->SAVE_LOG_ON_CRASH:Z

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 127
    const-string v3, "ASP_NATIVE_AGENT_LOG_ENABLE"

    sget-boolean v4, Lcom/mfluent/asp/common/util/AspLogLevels;->ASP_NATIVE_AGENT_LOG_ENABLE:Z

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 128
    const-string v3, "STRICT_MODE"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 129
    const-string v4, "STRICT_MODE"

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 131
    const-string v3, "LOGLEVEL_GENERAL"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 133
    const-string v3, "LOGLEVEL_VIEW"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_VIEW:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 134
    const-string v3, "LOGLEVEL_CACHE"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CACHE:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 135
    const-string v3, "LOGLEVEL_TRANSPORT"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_TRANSPORT:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 136
    const-string v3, "LOGLEVEL_BROADCAST"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_BROADCAST:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 137
    const-string v3, "LOGLEVEL_DWS"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_DWS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 138
    const-string v3, "LOGLEVEL_MEDIA_PROVIDER"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_MEDIA_PROVIDER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 139
    const-string v3, "LOGLEVEL_SYNC"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_SYNC:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 140
    const-string v3, "LOGLEVEL_CLOUD"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 141
    const-string v3, "LOGLEVEL_MEDIAPLAYER"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_MEDIAPLAYER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 142
    const-string v3, "LOGLEVEL_DLNA"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_DLNA:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 144
    const-string v3, "LOGLEVEL_HTTPSERVER"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_HTTPSERVER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 145
    const-string v3, "LOGLEVEL_HTTPINPUTSTREAM"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_HTTPINPUTSTREAM:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 146
    const-string v3, "LOGLEVEL_NTS"

    sget-object v4, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_NTS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 149
    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "SugarSyncLogLevel.LOGLEVEL"

    aput-object v4, v3, v0

    const/4 v4, 0x1

    const-string v5, "NDriveLogLevel.LOGLEVEL"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "DROP_BOX_LogLevel.LOGLEVEL"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "SKY_DRIVE_LogLevel.LOGLEVEL"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "V_DISK_LogLevel.LOGLEVEL"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "BAIDU_LogLevel.LOGLEVEL"

    aput-object v5, v3, v4

    .line 156
    array-length v4, v3

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 157
    sget-object v6, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v6

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 158
    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 161
    :cond_1
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method
