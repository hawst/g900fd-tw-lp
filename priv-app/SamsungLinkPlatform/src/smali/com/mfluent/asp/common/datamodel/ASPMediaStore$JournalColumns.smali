.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "JournalColumns"
.end annotation


# static fields
.field public static final IS_DELETE:Ljava/lang/String; = "is_delete"

.field public static final MEDIA_ID:Ljava/lang/String; = "media_id"

.field public static final ORIG_JOURNAL_ID:Ljava/lang/String; = "orig_journal_id"

.field public static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field public static final _ID:Ljava/lang/String; = "_id"


# virtual methods
.method public abstract getContentUri()Landroid/net/Uri;
.end method

.method public abstract getDeviceSyncedJournalColumn()Ljava/lang/String;
.end method

.method public abstract getJournalDroppedKey()Ljava/lang/String;
.end method

.method public abstract getJournalSyncedKey()Ljava/lang/String;
.end method
