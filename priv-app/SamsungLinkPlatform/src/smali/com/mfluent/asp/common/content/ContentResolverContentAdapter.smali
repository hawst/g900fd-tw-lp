.class public abstract Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;
.super Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/mfluent/asp/common/content/ContentId;",
        ">",
        "Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final PARCEL_VERSION:I = 0x2


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mNotificationObserver:Landroid/database/ContentObserver;

.field private mNotificationUri:Landroid/net/Uri;

.field private mNotificationUriRegistered:Z

.field private mProjection:[Ljava/lang/String;

.field private mSelection:Ljava/lang/String;

.field private mSelectionArgs:[Ljava/lang/String;

.field private mSortOrder:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    .line 32
    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    .line 33
    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationObserver:Landroid/database/ContentObserver;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUriRegistered:Z

    .line 38
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/BadParcelableException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;-><init>(Landroid/os/Parcel;)V

    .line 24
    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    .line 32
    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    .line 33
    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationObserver:Landroid/database/ContentObserver;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUriRegistered:Z

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 44
    if-lez v0, :cond_0

    if-le v0, v3, :cond_1

    .line 45
    :cond_0
    new-instance v1, Landroid/os/BadParcelableException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid Parcel version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 48
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 49
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 50
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    .line 53
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mProjection:[Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSortOrder:Ljava/lang/String;

    .line 58
    if-lt v0, v3, :cond_3

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 60
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 61
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    .line 64
    :cond_3
    return-void
.end method

.method private registerNotificationUri()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 94
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUriRegistered:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationObserver:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter$1;

    iget-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter$1;-><init>(Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationObserver:Landroid/database/ContentObserver;

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 105
    iput-boolean v3, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUriRegistered:Z

    .line 107
    :cond_1
    return-void
.end method

.method private unregisterNotificationUri()V
    .locals 2

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUriRegistered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUriRegistered:Z

    .line 114
    :cond_0
    return-void
.end method


# virtual methods
.method public createFilteredAdapter(Z)Lcom/mfluent/asp/common/content/ContentAdapter;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/mfluent/asp/common/content/ContentAdapter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 279
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->instantiateNewContentAdapter()Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;

    move-result-object v1

    .line 281
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    iput-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    .line 282
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mProjection:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mProjection:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mProjection:[Ljava/lang/String;

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    iput-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    .line 286
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    .line 289
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSortOrder:Ljava/lang/String;

    iput-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSortOrder:Ljava/lang/String;

    .line 290
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mIsEditing:Z

    if-nez v0, :cond_2

    if-eqz p1, :cond_7

    .line 291
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->createFilteredSelection()Landroid/util/Pair;

    move-result-object v2

    .line 293
    iget-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 294
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    .line 298
    :cond_3
    :goto_0
    iget-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/commons/lang3/ArrayUtils;->isEmpty([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 299
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    .line 306
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    iput-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    .line 308
    return-object v1

    .line 295
    :cond_5
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ") AND ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    goto :goto_0

    .line 300
    :cond_6
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Lorg/apache/commons/lang3/ArrayUtils;->isNotEmpty([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 301
    iget-object v3, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v3, v0}, Lorg/apache/commons/lang3/ArrayUtils;->addAll([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    goto :goto_1

    .line 304
    :cond_7
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->copyId(Lcom/mfluent/asp/common/content/ContentId;)Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    iput-object v0, v1, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    goto :goto_1
.end method

.method protected abstract createFilteredSelection()Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public createMediaSet()Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized destroy()V
    .locals 1

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->destroy()V

    .line 119
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->unregisterNotificationUri()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit p0

    return-void

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getNotificationUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mProjection:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mProjection:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 160
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectionArgs()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 190
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSortOrder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSortOrder:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public initContext(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->initContext(Landroid/content/Context;)V

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    .line 90
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->registerNotificationUri()V

    .line 91
    return-void
.end method

.method protected abstract instantiateNewContentAdapter()Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mfluent/asp/common/content/ContentResolverContentAdapter",
            "<TT;>;"
        }
    .end annotation
.end method

.method public loadData()V
    .locals 3

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    :goto_0
    return-void

    .line 214
    :cond_0
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->loadData()V

    .line 215
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->getAndIncrementLoadToken()I

    move-result v0

    .line 216
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->getExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v2, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter$2;

    invoke-direct {v2, p0, v0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter$2;-><init>(Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;I)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected openCursor()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 227
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_2

    .line 228
    :cond_0
    const/4 v0, 0x0

    .line 237
    :cond_1
    :goto_0
    return-object v0

    .line 231
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mProjection:[Ljava/lang/String;

    iget-object v3, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    iget-object v4, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    iget-object v5, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSortOrder:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 233
    if-eqz v0, :cond_1

    .line 234
    iget-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 0

    .prologue
    .line 243
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 253
    return-void
.end method

.method public setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    invoke-static {p2, v0}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->unregisterNotificationUri()V

    .line 264
    iput-object p2, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    .line 265
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->registerNotificationUri()V

    .line 267
    :cond_0
    return-void
.end method

.method public setProjection([Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mProjection:[Ljava/lang/String;

    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    iput-object p1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mProjection:[Ljava/lang/String;

    .line 149
    const/4 v0, 0x1

    .line 152
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setQueryParams(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->setUri(Landroid/net/Uri;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 125
    invoke-virtual {p0, p2}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->setProjection([Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 126
    invoke-virtual {p0, p3}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->setSelection(Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 127
    invoke-virtual {p0, p4}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->setSelectionArgs([Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 128
    invoke-virtual {p0, p5}, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->setSortOrder(Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 130
    return v0
.end method

.method public setSelection(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iput-object p1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    .line 166
    const/4 v0, 0x1

    .line 169
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSelectionArgs([Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    iput-object p1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    .line 179
    const/4 v0, 0x1

    .line 182
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSortOrder(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSortOrder:Ljava/lang/String;

    invoke-static {p1, v0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    iput-object p1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSortOrder:Ljava/lang/String;

    .line 196
    const/4 v0, 0x1

    .line 199
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setUri(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 134
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 135
    :cond_2
    iput-object p1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    .line 136
    const/4 v0, 0x1

    .line 139
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 0

    .prologue
    .line 248
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 258
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1, p2}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->writeToParcel(Landroid/os/Parcel;I)V

    .line 69
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    const/4 v0, 0x0

    .line 71
    iget-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 72
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mProjection:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelection:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mSortOrder:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 80
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverContentAdapter;->mNotificationUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
