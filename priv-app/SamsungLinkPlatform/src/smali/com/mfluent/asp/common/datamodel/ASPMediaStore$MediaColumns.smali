.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPMediaStore$MediaColumns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$BaseASPColumns;
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$MediaColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MediaColumns"
.end annotation


# static fields
.field public static final FILE_DIGEST_SIZE:Ljava/lang/String; = "file_digest_size"

.field public static final FILE_DIGEST_TIME:Ljava/lang/String; = "file_digest_time"

.field public static final FULL_URI:Ljava/lang/String; = "full_uri"

.field public static final QUERY_STR_INSERT_OR_UPDATE:Ljava/lang/String; = "insert_or_update"

.field public static final THUMBNAIL_URI:Ljava/lang/String; = "thumbnail_uri"
