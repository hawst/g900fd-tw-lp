.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPMediaStore$MediaTypes;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MediaTypes"
.end annotation


# static fields
.field public static final ALBUM:I = 0xc

.field public static final ARTIST:I = 0xd

.field public static final AUDIO:I = 0x2

.field public static final CAPTION:I = 0xd

.field public static final CAPTION_INDEX:I = 0xe

.field public static final DOCUMENT:I = 0xf

.field public static final GENRE:I = 0xe

.field public static final IMAGE:I = 0x1

.field public static final NONE:I = 0x0

.field public static final VIDEO:I = 0x3
