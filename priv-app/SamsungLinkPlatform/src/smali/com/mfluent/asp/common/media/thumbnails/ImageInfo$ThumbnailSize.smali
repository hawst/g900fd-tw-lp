.class public final enum Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ThumbnailSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

.field public static final enum FULL_SCREEN:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

.field public static final enum MICRO:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

.field public static final enum MINI:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    const-string v1, "MICRO"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->MICRO:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    .line 30
    new-instance v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    const-string v1, "MINI"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->MINI:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    .line 31
    new-instance v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    const-string v1, "FULL_SCREEN"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->FULL_SCREEN:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    .line 28
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    sget-object v1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->MICRO:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->MINI:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->FULL_SCREEN:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->$VALUES:[Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->$VALUES:[Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    invoke-virtual {v0}, [Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    return-object v0
.end method
