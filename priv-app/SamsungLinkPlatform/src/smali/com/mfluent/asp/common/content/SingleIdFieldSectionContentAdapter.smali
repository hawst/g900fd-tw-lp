.class public Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;
.super Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter",
        "<",
        "Lcom/mfluent/asp/common/content/MultiColumnContentId;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIdField:Ljava/lang/String;

.field private mIdType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter$1;

    invoke-direct {v0}, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter$1;-><init>()V

    sput-object v0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;-><init>(Landroid/os/Parcel;)V

    .line 24
    const-string v0, "_id"

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->mIdField:Ljava/lang/String;

    .line 25
    const/4 v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->mIdType:I

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->mIdField:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;-><init>()V

    .line 24
    const-string v0, "_id"

    iput-object v0, p0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->mIdField:Ljava/lang/String;

    .line 25
    const/4 v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->mIdType:I

    .line 29
    iput-object p1, p0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->mIdField:Ljava/lang/String;

    .line 30
    iput p2, p0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->mIdType:I

    .line 31
    return-void
.end method


# virtual methods
.method protected bridge synthetic copySectionId(Lcom/mfluent/asp/common/content/ContentId;)Lcom/mfluent/asp/common/content/ContentId;
    .locals 1

    .prologue
    .line 9
    check-cast p1, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->copySectionId(Lcom/mfluent/asp/common/content/MultiColumnContentId;)Lcom/mfluent/asp/common/content/MultiColumnContentId;

    move-result-object v0

    return-object v0
.end method

.method protected copySectionId(Lcom/mfluent/asp/common/content/MultiColumnContentId;)Lcom/mfluent/asp/common/content/MultiColumnContentId;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    invoke-direct {v0, p1}, Lcom/mfluent/asp/common/content/MultiColumnContentId;-><init>(Lcom/mfluent/asp/common/content/MultiColumnContentId;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public getIdField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->mIdField:Ljava/lang/String;

    return-object v0
.end method

.method protected bridge synthetic instantiateSectionId()Lcom/mfluent/asp/common/content/ContentId;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->instantiateSectionId()Lcom/mfluent/asp/common/content/MultiColumnContentId;

    move-result-object v0

    return-object v0
.end method

.method protected instantiateSectionId()Lcom/mfluent/asp/common/content/MultiColumnContentId;
    .locals 3

    .prologue
    .line 57
    new-instance v0, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    iget-object v1, p0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->mIdField:Ljava/lang/String;

    iget v2, p0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->mIdType:I

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/common/content/MultiColumnContentId;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->writeToParcel(Landroid/os/Parcel;I)V

    .line 43
    iget-object v0, p0, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->mIdField:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    return-void
.end method
