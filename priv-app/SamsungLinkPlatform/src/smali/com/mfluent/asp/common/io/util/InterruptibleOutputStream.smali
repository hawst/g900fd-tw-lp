.class public Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;
.super Ljava/io/OutputStream;
.source "SourceFile"


# instance fields
.field private final outstream:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;->outstream:Ljava/io/OutputStream;

    .line 20
    return-void
.end method

.method private checkForInterrupt()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 50
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;->outstream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 55
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;->checkForInterrupt()V

    .line 43
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;->outstream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 44
    return-void
.end method

.method public write(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;->checkForInterrupt()V

    .line 25
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;->outstream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 26
    return-void
.end method

.method public write([B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;->checkForInterrupt()V

    .line 31
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;->outstream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 32
    return-void
.end method

.method public write([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;->checkForInterrupt()V

    .line 37
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;->outstream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 38
    return-void
.end method
