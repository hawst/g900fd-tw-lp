.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DeviceColumns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$DeviceColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DeviceColumns"
.end annotation


# static fields
.field public static final DEVICE_UNIQUE_ID:Ljava/lang/String; = "device_unique_id"

.field public static final ENCRYPTED_IMEI:Ljava/lang/String; = "eimei"

.field public static final HOST_PEER_ID:Ljava/lang/String; = "host_peer_id"

.field public static final IMEI:Ljava/lang/String; = "imei"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final IS_HLS_SERVER:Ljava/lang/String; = "is_hls_server"

.field public static final IS_REMOTE_WAKEUP_AVAILABLE:Ljava/lang/String; = "is_remote_wakeup"

.field public static final IS_REMOTE_WAKEUP_SUPPORT:Ljava/lang/String; = "is_remote_wakeup_support"

.field public static final IS_SYNC_SERVER:Ljava/lang/String; = "is_sync_server"

.field public static final IS_TS_SERVER:Ljava/lang/String; = "is_ts_server"

.field public static final LAST_RECV_DOCUMENT_JOURNAL_ID:Ljava/lang/String; = "last_recv_document_journal_id"

.field public static final LAST_RECV_IMAGE_JOURNAL_ID:Ljava/lang/String; = "last_recv_image_journal_id"

.field public static final LAST_RECV_MUSIC_JOURNAL_ID:Ljava/lang/String; = "last_recv_music_journal_id"

.field public static final LAST_RECV_VIDEO_JOURNAL_ID:Ljava/lang/String; = "last_recv_video_journal_id"

.field public static final PEER_ID:Ljava/lang/String; = "peer_id"

.field public static final SERVER_SORT_KEY:Ljava/lang/String; = "server_sort_key"

.field public static final SMALLEST_DRIVE_CAPACITY:Ljava/lang/String; = "smallest_drive_capacity"

.field public static final SUPPORTS_PUSH:Ljava/lang/String; = "supports_push_notification"

.field public static final SYNC_KEY_AUDIO:Ljava/lang/String; = "sync_key_audio"

.field public static final SYNC_KEY_DOCUMENTS:Ljava/lang/String; = "sync_key_documents"

.field public static final SYNC_KEY_IMAGES:Ljava/lang/String; = "sync_key_images"

.field public static final SYNC_KEY_VIDEOS:Ljava/lang/String; = "sync_key_videos"

.field public static final SYNC_SERVER_SUPPORTED_MEDIA_TYPES:Ljava/lang/String; = "sync_server_supported_media_types"

.field public static final UDN_WIFI:Ljava/lang/String; = "udn_wifi"

.field public static final UDN_WIFI_DIRECT:Ljava/lang/String; = "udn_wifi_direct"

.field public static final VIDEO_REMOTE_PLAY_DEGRADED_DIALOG_DISABLED:Ljava/lang/String; = "video_remote_play_degraded_dialog_disabled"

.field public static final WEB_STORAGE_ACCOUNT_ID:Ljava/lang/String; = "web_storage_account_id"

.field public static final WEB_STORAGE_EMAIL:Ljava/lang/String; = "web_storage_email_id"

.field public static final WEB_STORAGE_IS_SIGNED_IN:Ljava/lang/String; = "web_storage_is_signed_in_id"

.field public static final WEB_STORAGE_PW:Ljava/lang/String; = "web_storage_pw_id"

.field public static final WEB_STORAGE_TOTAL_CAPACITY:Ljava/lang/String; = "web_storage_total_capacity_id"

.field public static final WEB_STORAGE_TYPE:Ljava/lang/String; = "web_storage_type"

.field public static final WEB_STORAGE_USED_CAPACITY:Ljava/lang/String; = "web_storage_used_capacity_id"

.field public static final WEB_STORAGE_USER_ID:Ljava/lang/String; = "web_storage_user_id"
