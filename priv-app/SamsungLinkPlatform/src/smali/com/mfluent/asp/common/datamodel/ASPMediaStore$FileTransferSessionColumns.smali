.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferSessionColumns;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FileTransferSessionColumns"
.end annotation


# static fields
.field public static final CREATED_TIME:Ljava/lang/String; = "createdTime"

.field public static final CURRENT_TRANSFER_SIZE:Ljava/lang/String; = "currentTransferSize"

.field public static final END_TIME:Ljava/lang/String; = "endTime"

.field public static final FIRST_FILE_NAME:Ljava/lang/String; = "firstFileName"

.field public static final NUM_FILES:Ljava/lang/String; = "numFiles"

.field public static final NUM_FILES_SKIPPED:Ljava/lang/String; = "numFilesSkipped"

.field public static final SOURCE_DEVICE_ID:Ljava/lang/String; = "sourceDeviceId"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final TARGET_DEVICE_ID:Ljava/lang/String; = "targetDeviceId"

.field public static final TRANSFER_ARCHIVE:Ljava/lang/String; = "transferarchive"

.field public static final TRANSFER_SIZE:Ljava/lang/String; = "transferSize"

.field public static final UUID:Ljava/lang/String; = "uuid"
