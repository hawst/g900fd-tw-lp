.class public Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/util/AspLogLevels;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LogLevel"
.end annotation


# instance fields
.field private level:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    iput p1, p0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->level:I

    .line 174
    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;)V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iget v0, p1, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->level:I

    iput v0, p0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->level:I

    .line 178
    return-void
.end method


# virtual methods
.method public canLog(I)Z
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->level:I

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setValue(I)V
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x2

    if-lt p1, v0, :cond_0

    const/4 v0, 0x7

    if-gt p1, v0, :cond_0

    .line 190
    iput p1, p0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->level:I

    .line 192
    :cond_0
    return-void
.end method

.method public value()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->level:I

    return v0
.end method
