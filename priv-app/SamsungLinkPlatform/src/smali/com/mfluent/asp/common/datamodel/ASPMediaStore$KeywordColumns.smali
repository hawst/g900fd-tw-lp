.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPMediaStore$KeywordColumns;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "KeywordColumns"
.end annotation


# static fields
.field public static final FILE_ID:Ljava/lang/String; = "file_id"

.field public static final KEYWORD:Ljava/lang/String; = "keyword"

.field public static final KEYWORD_ID:Ljava/lang/String; = "keyword_id"

.field public static final KEYWORD_MAP_ID:Ljava/lang/String; = "keyword_map_id"

.field public static final KEYWORD_TYPE:Ljava/lang/String; = "keyword_type"

.field public static final KEYWORD_TYPE_BUCKET:I = 0x3

.field public static final KEYWORD_TYPE_GENERAL:I = 0x1

.field public static final KEYWORD_TYPE_GEO_LOC:I = 0x2

.field public static final KEYWORD_TYPE_USER:I = 0x4
