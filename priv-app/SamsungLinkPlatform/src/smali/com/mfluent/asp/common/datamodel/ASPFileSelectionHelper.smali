.class public Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private directoryCount:I

.field private final directoryIndexes:[Z

.field private selectedCount:I

.field private final selectedIndexes:[Z

.field private final totalCount:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->totalCount:I

    .line 27
    new-array v0, p1, [Z

    iput-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectedIndexes:[Z

    .line 28
    new-array v0, p1, [Z

    iput-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->directoryIndexes:[Z

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;)I
    .locals 1

    .prologue
    .line 12
    iget v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->totalCount:I

    return v0
.end method


# virtual methods
.method public deselectAll()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 69
    move v0, v1

    :goto_0
    iget v2, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->totalCount:I

    if-ge v0, v2, :cond_1

    .line 70
    iget-object v2, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->directoryIndexes:[Z

    aget-boolean v2, v2, v0

    if-nez v2, :cond_0

    .line 71
    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->setSelected(IZ)V

    .line 69
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :cond_1
    return-void
.end method

.method public getSelectedFileIndexes()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;-><init>(Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;)V

    return-object v0
.end method

.method public isAllSelected()Z
    .locals 2

    .prologue
    .line 59
    iget v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectedCount:I

    iget v1, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->totalCount:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectedCount:I

    iget v1, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->directoryCount:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelected(I)Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectedIndexes:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->directoryIndexes:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public selectAll()V
    .locals 2

    .prologue
    .line 63
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->totalCount:I

    if-ge v0, v1, :cond_0

    .line 64
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->setSelected(IZ)V

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_0
    return-void
.end method

.method public setIsDirectory(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 47
    iget-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->directoryIndexes:[Z

    aget-boolean v0, v0, p1

    .line 48
    if-nez v0, :cond_0

    .line 49
    iget v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->directoryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->directoryCount:I

    .line 50
    iget-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->directoryIndexes:[Z

    aput-boolean v1, v0, p1

    .line 53
    invoke-virtual {p0, p1, v1}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->setSelected(IZ)V

    .line 55
    :cond_0
    return-void
.end method

.method public setSelected(IZ)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectedIndexes:[Z

    aget-boolean v0, v0, p1

    .line 37
    if-eqz p2, :cond_1

    if-nez v0, :cond_1

    .line 38
    iget v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectedCount:I

    .line 43
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectedIndexes:[Z

    aput-boolean p2, v0, p1

    .line 44
    return-void

    .line 39
    :cond_1
    if-nez p2, :cond_0

    if-eqz v0, :cond_0

    .line 40
    iget v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectedCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->selectedCount:I

    goto :goto_0
.end method
