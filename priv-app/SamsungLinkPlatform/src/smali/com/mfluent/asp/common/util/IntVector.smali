.class public final Lcom/mfluent/asp/common/util/IntVector;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mfluent/asp/common/util/IntVector;",
            ">;"
        }
    .end annotation
.end field

.field private static final INT_VECTOR_FILE_VERSION:I = 0x2

.field private static final INT_VECTOR_PARCEL_VERSION:I = 0x1


# instance fields
.field private m_array:[I

.field private m_increment:I

.field private m_index:I

.field private m_sorted:Z

.field private m_unique:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/mfluent/asp/common/util/IntVector$1;

    invoke-direct {v0}, Lcom/mfluent/asp/common/util/IntVector$1;-><init>()V

    sput-object v0, Lcom/mfluent/asp/common/util/IntVector;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    .line 143
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZ)V

    .line 139
    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZZ)V

    .line 94
    return-void
.end method

.method public constructor <init>(IIZZ)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    .line 103
    iput p2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    .line 104
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    .line 105
    iput-boolean p3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    .line 106
    iput-boolean p4, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    .line 107
    return-void
.end method

.method synthetic constructor <init>(Lcom/mfluent/asp/common/util/IntVector$1;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/mfluent/asp/common/util/IntVector;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/common/util/IntVector;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    .line 146
    iget-object v0, p1, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    .line 147
    iget v0, p1, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    .line 148
    iget v0, p1, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 149
    iget-boolean v0, p1, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    iput-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    .line 150
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-lez v0, :cond_0

    .line 151
    iget-object v0, p1, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    iget v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 153
    :cond_0
    return-void
.end method

.method public constructor <init>([II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    .line 119
    if-eqz p1, :cond_0

    .line 120
    iput-object p1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    .line 124
    :goto_0
    iput p2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    .line 125
    iput-boolean v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    .line 126
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    array-length v0, v0

    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 127
    return-void

    .line 122
    :cond_0
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    goto :goto_0
.end method

.method static synthetic access$102(Lcom/mfluent/asp/common/util/IntVector;Z)Z
    .locals 0

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    return p1
.end method

.method static synthetic access$202(Lcom/mfluent/asp/common/util/IntVector;I)I
    .locals 0

    .prologue
    .line 18
    iput p1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    return p1
.end method

.method static synthetic access$302(Lcom/mfluent/asp/common/util/IntVector;Z)Z
    .locals 0

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mfluent/asp/common/util/IntVector;)[I
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    return-object v0
.end method

.method static synthetic access$502(Lcom/mfluent/asp/common/util/IntVector;I)I
    .locals 0

    .prologue
    .line 18
    iput p1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    return p1
.end method

.method private appendRangeInner([III)V
    .locals 2

    .prologue
    .line 378
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    if-eqz v0, :cond_0

    .line 379
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "IntVector: don\'t use append if sorted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 382
    :cond_0
    if-lez p3, :cond_1

    .line 383
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    add-int/2addr v0, p3

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/util/IntVector;->ensureCapacity(I)V

    .line 385
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 386
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 388
    :cond_1
    return-void
.end method

.method public static contains(Lcom/mfluent/asp/common/util/IntVector;I)Z
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    .line 166
    if-eqz p0, :cond_0

    .line 167
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/util/IntVector;->contains(I)Z

    move-result v0

    .line 170
    :cond_0
    return v0
.end method

.method public static fromInputStream(Ljava/io/DataInput;)Lcom/mfluent/asp/common/util/IntVector;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 193
    .line 196
    :try_start_0
    new-instance v0, Lcom/mfluent/asp/common/util/IntVector;

    invoke-direct {v0}, Lcom/mfluent/asp/common/util/IntVector;-><init>()V

    .line 197
    invoke-virtual {v0, p0}, Lcom/mfluent/asp/common/util/IntVector;->readFromInputStream(Ljava/io/DataInput;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 204
    :goto_0
    return-object v0

    .line 199
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 202
    goto :goto_0

    .line 201
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method private grow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 577
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    array-length v0, v0

    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    add-int/2addr v0, v1

    new-array v0, v0, [I

    .line 578
    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    iget-object v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 579
    iput-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    .line 580
    return-void
.end method

.method public static orderedIntersection(Lcom/mfluent/asp/common/util/IntVector;Lcom/mfluent/asp/common/util/IntVector;)Lcom/mfluent/asp/common/util/IntVector;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 288
    new-instance v2, Lcom/mfluent/asp/common/util/IntVector;

    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    iget v3, p1, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    iget-boolean v4, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    iget-boolean v5, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZZ)V

    move v1, v0

    .line 293
    :goto_0
    iget v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge v1, v3, :cond_1

    .line 294
    iget-object v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v3, v3, v1

    invoke-virtual {p1, v3}, Lcom/mfluent/asp/common/util/IntVector;->contains(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 295
    iget-object v3, v2, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    iget-object v4, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v4, v4, v1

    aput v4, v3, v0

    .line 296
    add-int/lit8 v0, v0, 0x1

    .line 293
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 300
    :cond_1
    iput v0, v2, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 302
    return-object v2
.end method

.method private search(I)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 589
    iget-boolean v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    if-eqz v2, :cond_8

    .line 590
    iget v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-nez v2, :cond_1

    move v0, v1

    .line 623
    :cond_0
    :goto_0
    return v0

    .line 594
    :cond_1
    iget v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    add-int/lit8 v2, v2, -0x1

    .line 596
    iget-object v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v3, v3, v2

    if-ne v3, p1, :cond_2

    move v0, v2

    .line 597
    goto :goto_0

    .line 598
    :cond_2
    iget-object v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v3, v3, v2

    if-ge v3, p1, :cond_3

    move v0, v2

    .line 599
    goto :goto_0

    .line 600
    :cond_3
    iget-object v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v3, v3, v0

    if-eq v3, p1, :cond_0

    .line 602
    iget-object v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v3, v3, v0

    if-le v3, p1, :cond_a

    move v0, v1

    .line 603
    goto :goto_0

    .line 609
    :cond_4
    iget-object v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v3, v3, v0

    if-le v3, p1, :cond_5

    move v2, v0

    .line 605
    :goto_1
    sub-int v0, v2, v1

    const/4 v3, 0x1

    if-le v0, v3, :cond_6

    .line 606
    add-int v0, v2, v1

    ushr-int/lit8 v0, v0, 0x1

    .line 607
    iget-object v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v3, v3, v0

    if-ne v3, p1, :cond_4

    goto :goto_0

    :cond_5
    move v1, v0

    .line 612
    goto :goto_1

    :cond_6
    move v0, v1

    .line 615
    goto :goto_0

    .line 617
    :cond_7
    add-int/lit8 v0, v0, 0x1

    :cond_8
    iget v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge v0, v2, :cond_9

    .line 618
    iget-object v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v2, v2, v0

    if-ne v2, p1, :cond_7

    goto :goto_0

    :cond_9
    move v0, v1

    .line 623
    goto :goto_0

    :cond_a
    move v1, v0

    goto :goto_1
.end method

.method public static size(Lcom/mfluent/asp/common/util/IntVector;)I
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    .line 157
    if-eqz p0, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v0

    .line 161
    :cond_0
    return v0
.end method

.method public static sortedIntersection(Lcom/mfluent/asp/common/util/IntVector;Lcom/mfluent/asp/common/util/IntVector;Z)Lcom/mfluent/asp/common/util/IntVector;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 306
    iget-boolean v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p1, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    if-nez v1, :cond_1

    .line 307
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "sortedIntersection requires both vectors to be sorted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310
    :cond_1
    new-instance v3, Lcom/mfluent/asp/common/util/IntVector;

    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    iget v2, p1, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    const/4 v4, 0x1

    invoke-direct {v3, v1, v2, v4, p2}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZZ)V

    move v1, v0

    move v2, v0

    .line 316
    :goto_0
    iget v4, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge v2, v4, :cond_6

    iget v4, p1, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge v1, v4, :cond_6

    .line 317
    iget-object v4, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v4, v4, v2

    iget-object v5, p1, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v5, v5, v1

    if-ne v4, v5, :cond_4

    .line 318
    iget-object v4, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v4, v4, v2

    .line 319
    if-eqz p2, :cond_2

    if-eqz v0, :cond_2

    iget-object v5, v3, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    add-int/lit8 v6, v0, -0x1

    aget v5, v5, v6

    if-ge v5, v4, :cond_3

    .line 320
    :cond_2
    iget-object v5, v3, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aput v4, v5, v0

    .line 321
    add-int/lit8 v0, v0, 0x1

    .line 323
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 324
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 325
    :cond_4
    iget-object v4, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v4, v4, v2

    iget-object v5, p1, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v5, v5, v1

    if-ge v4, v5, :cond_5

    .line 326
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 329
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 333
    :cond_6
    iput v0, v3, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 335
    return-object v3
.end method

.method public static sortedMerge(Lcom/mfluent/asp/common/util/IntVector;Lcom/mfluent/asp/common/util/IntVector;Z)Lcom/mfluent/asp/common/util/IntVector;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 258
    iget-boolean v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p1, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    if-nez v1, :cond_1

    .line 259
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "sortedMerge requires both vectors to be sorted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_1
    new-instance v4, Lcom/mfluent/asp/common/util/IntVector;

    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    iget v2, p1, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    array-length v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    const/4 v3, 0x1

    invoke-direct {v4, v1, v2, v3, p2}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZZ)V

    move v1, v0

    move v2, v0

    .line 268
    :cond_2
    :goto_0
    iget v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-lt v2, v3, :cond_3

    iget v3, p1, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge v1, v3, :cond_7

    .line 269
    :cond_3
    iget v3, p1, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge v1, v3, :cond_4

    iget v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge v2, v3, :cond_6

    iget-object v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v3, v3, v2

    iget-object v5, p1, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v5, v5, v1

    if-ge v3, v5, :cond_6

    .line 270
    :cond_4
    iget-object v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v3, v3, v2

    .line 271
    add-int/lit8 v2, v2, 0x1

    .line 276
    :goto_1
    if-eqz p2, :cond_5

    if-eqz v0, :cond_5

    iget-object v5, v4, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    add-int/lit8 v6, v0, -0x1

    aget v5, v5, v6

    if-ge v5, v3, :cond_2

    .line 277
    :cond_5
    iget-object v5, v4, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aput v3, v5, v0

    .line 278
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 273
    :cond_6
    iget-object v3, p1, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v3, v3, v1

    .line 274
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 282
    :cond_7
    iput v0, v4, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 284
    return-object v4
.end method


# virtual methods
.method public final append(I)V
    .locals 2

    .prologue
    .line 250
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 251
    invoke-direct {p0}, Lcom/mfluent/asp/common/util/IntVector;->grow()V

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    aput p1, v0, v1

    .line 254
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 255
    return-void
.end method

.method public final appendRange(Lcom/mfluent/asp/common/util/IntVector;II)V
    .locals 2

    .prologue
    .line 356
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    if-eqz v0, :cond_0

    .line 357
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "IntVector: don\'t use append if sorted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 360
    :cond_0
    iget v0, p1, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    sub-int/2addr v0, p2

    if-le p3, v0, :cond_1

    .line 361
    iget v0, p1, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    sub-int p3, v0, p2

    .line 363
    :cond_1
    iget-object v0, p1, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    invoke-direct {p0, v0, p2, p3}, Lcom/mfluent/asp/common/util/IntVector;->appendRangeInner([III)V

    .line 364
    return-void
.end method

.method public final appendRange([III)V
    .locals 2

    .prologue
    .line 367
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    if-eqz v0, :cond_0

    .line 368
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "IntVector: don\'t use append if sorted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 371
    :cond_0
    array-length v0, p1

    sub-int/2addr v0, p2

    if-le p3, v0, :cond_1

    .line 372
    array-length v0, p1

    sub-int p3, v0, p2

    .line 374
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/mfluent/asp/common/util/IntVector;->appendRangeInner([III)V

    .line 375
    return-void
.end method

.method public final clone()Lcom/mfluent/asp/common/util/IntVector;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 209
    const/4 v0, 0x0

    .line 210
    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-lez v1, :cond_0

    .line 211
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    new-array v0, v0, [I

    .line 212
    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    iget v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 215
    :cond_0
    new-instance v1, Lcom/mfluent/asp/common/util/IntVector;

    iget v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    invoke-direct {v1, v0, v2}, Lcom/mfluent/asp/common/util/IntVector;-><init>([II)V

    .line 216
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    iput-boolean v0, v1, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    .line 217
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    iput-boolean v0, v1, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    .line 219
    return-object v1
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/mfluent/asp/common/util/IntVector;->clone()Lcom/mfluent/asp/common/util/IntVector;

    move-result-object v0

    return-object v0
.end method

.method public final contains(I)Z
    .locals 2

    .prologue
    .line 452
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/util/IntVector;->search(I)I

    move-result v0

    .line 453
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v0, v1, v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public final elementAt(I)I
    .locals 3

    .prologue
    .line 569
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-lt p1, v0, :cond_1

    .line 570
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t read element at index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 573
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v0, v0, p1

    return v0
.end method

.method public final ensureCapacity(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 339
    .line 340
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    if-eqz v0, :cond_3

    .line 341
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    array-length v0, v0

    .line 343
    :goto_0
    iget-object v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    if-eqz v2, :cond_0

    if-ge v0, p1, :cond_2

    .line 344
    :cond_0
    sub-int v2, p1, v0

    .line 345
    iget v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    div-int/2addr v2, v3

    iget v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    mul-int/2addr v2, v3

    add-int/2addr v0, v2

    iget v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    add-int/2addr v0, v2

    .line 347
    new-array v0, v0, [I

    .line 348
    iget v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    if-eqz v2, :cond_1

    .line 349
    iget-object v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    iget v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    invoke-static {v2, v1, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 351
    :cond_1
    iput-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    .line 353
    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final find(I)I
    .locals 2

    .prologue
    .line 465
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/util/IntVector;->search(I)I

    move-result v0

    .line 467
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 470
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final insert(I)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 414
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    if-eqz v0, :cond_2

    .line 415
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/util/IntVector;->search(I)I

    move-result v0

    .line 416
    iget-boolean v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    if-eqz v2, :cond_1

    if-ltz v0, :cond_1

    iget-object v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v2, v2, v0

    if-ne v2, p1, :cond_1

    move v0, v1

    .line 427
    :cond_0
    :goto_0
    return v0

    .line 419
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 424
    :goto_1
    invoke-virtual {p0, p1, v0}, Lcom/mfluent/asp/common/util/IntVector;->insertElementAt(II)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 427
    goto :goto_0

    .line 421
    :cond_2
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    goto :goto_1
.end method

.method public final insertElementAt(II)Z
    .locals 4

    .prologue
    .line 432
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 433
    invoke-direct {p0}, Lcom/mfluent/asp/common/util/IntVector;->grow()V

    .line 436
    :cond_0
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge p2, v0, :cond_1

    .line 437
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    add-int/lit8 v2, p2, 0x1

    iget v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    sub-int/2addr v3, p2

    invoke-static {v0, p2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 439
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aput p1, v0, p2

    .line 440
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 441
    const/4 v0, 0x1

    return v0
.end method

.method public final isUnique()Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    return v0
.end method

.method public final lastElement()I
    .locals 2

    .prologue
    .line 722
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-lez v0, :cond_0

    .line 723
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0

    .line 726
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method public final readFromInputStream(Ljava/io/DataInput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 174
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 176
    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    .line 177
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid version in stream: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_0
    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 181
    invoke-interface {p1}, Ljava/io/DataInput;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    .line 182
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    .line 183
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 184
    invoke-interface {p1}, Ljava/io/DataInput;->readBoolean()Z

    move-result v2

    iput-boolean v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    .line 185
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/util/IntVector;->ensureCapacity(I)V

    .line 186
    :goto_0
    if-ge v0, v1, :cond_1

    .line 187
    iget-object v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v3

    aput v3, v2, v0

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    :cond_1
    iput v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 190
    return-void
.end method

.method public final remove(Lcom/mfluent/asp/common/util/IntVector;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 503
    .line 505
    if-ne p1, p0, :cond_1

    .line 506
    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 507
    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 516
    :cond_0
    return v1

    :cond_1
    move v1, v0

    .line 509
    :goto_0
    iget v2, p1, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge v0, v2, :cond_0

    .line 510
    iget-object v2, p1, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/mfluent/asp/common/util/IntVector;->remove(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 511
    add-int/lit8 v1, v1, 0x1

    .line 509
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final remove(I)Z
    .locals 2

    .prologue
    .line 494
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/util/IntVector;->search(I)I

    move-result v0

    .line 495
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v1, v1, v0

    if-eq v1, p1, :cond_1

    .line 496
    :cond_0
    const/4 v0, 0x0

    .line 499
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/util/IntVector;->removeElementAt(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final removeAll()V
    .locals 1

    .prologue
    .line 537
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 538
    return-void
.end method

.method public final removeElementAt(I)Z
    .locals 4

    .prologue
    .line 520
    const/4 v0, 0x0

    .line 522
    if-ltz p1, :cond_1

    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge p1, v1, :cond_1

    .line 523
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge p1, v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    add-int/lit8 v1, p1, 0x1

    iget-object v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    iget v3, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    sub-int/2addr v3, p1

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v1, v2, p1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 526
    :cond_0
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 527
    const/4 v0, 0x1

    .line 530
    :cond_1
    return v0
.end method

.method public final removeLastElement()Z
    .locals 1

    .prologue
    .line 480
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-lez v0, :cond_0

    .line 481
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 482
    const/4 v0, 0x1

    .line 484
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final removeRange(II)V
    .locals 3

    .prologue
    .line 391
    if-nez p1, :cond_1

    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-lt p2, v0, :cond_1

    .line 392
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    .line 404
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge p1, v0, :cond_0

    .line 394
    add-int v0, p1, p2

    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-le v0, v1, :cond_2

    .line 395
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    sub-int p2, v0, p1

    .line 398
    :cond_2
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    sub-int/2addr v0, p1

    if-eq p2, v0, :cond_3

    .line 399
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    add-int v1, p1, p2

    iget-object v2, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    invoke-static {v0, v1, v2, p1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 402
    :cond_3
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    sub-int/2addr v0, p2

    iput v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    goto :goto_0
.end method

.method public final replaceElementAt(II)I
    .locals 2

    .prologue
    .line 545
    if-ltz p2, :cond_0

    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-lt p2, v0, :cond_1

    .line 546
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 549
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v0, v0, p2

    .line 550
    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aput p1, v1, p2

    .line 552
    return v0
.end method

.method public final setUnique(Z)V
    .locals 0

    .prologue
    .line 238
    iput-boolean p1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    .line 239
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 559
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    return v0
.end method

.method public final toArray()[I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 707
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    new-array v0, v0, [I

    .line 709
    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-lez v1, :cond_0

    .line 710
    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    array-length v2, v0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 713
    :cond_0
    return-object v0
.end method

.method public final toDelimSeparatedString(C)Ljava/lang/String;
    .locals 3

    .prologue
    .line 696
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 697
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 698
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/util/IntVector;->elementAt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 699
    invoke-virtual {p0}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 700
    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 697
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 703
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toOutputStream(Ljava/io/DataOutput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 224
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeBoolean(Z)V

    .line 225
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 226
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 227
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeBoolean(Z)V

    .line 228
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge v0, v1, :cond_0

    .line 229
    iget-object v1, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v1, v1, v0

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    .line 228
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 231
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 718
    invoke-virtual {p0}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/16 v0, 0x2c

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/util/IntVector;->toDelimSeparatedString(C)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "!!empty!!"

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 62
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 66
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_increment:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 67
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 68
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_unique:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 69
    iget-boolean v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_sorted:Z

    if-eqz v0, :cond_2

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 70
    :goto_3
    iget v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_index:I

    if-ge v2, v0, :cond_3

    .line 71
    iget-object v0, p0, Lcom/mfluent/asp/common/util/IntVector;->m_array:[I

    aget v0, v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_0
    move v0, v2

    .line 64
    goto :goto_0

    :cond_1
    move v0, v2

    .line 68
    goto :goto_1

    :cond_2
    move v1, v2

    .line 69
    goto :goto_2

    .line 73
    :cond_3
    return-void
.end method
