.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/mfluent/asp/common/datamodel/ASPFile;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract deselectAll()V
.end method

.method public abstract destroy()V
.end method

.method public abstract getCount()I
.end method

.method public abstract getCurrentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract getCurrentDirectoryAbsolutePath()Ljava/lang/String;
.end method

.method public abstract getFile(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getFileNonBlocking(I)Lcom/mfluent/asp/common/datamodel/ASPFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation
.end method

.method public abstract getParentDirectory()Lcom/mfluent/asp/common/datamodel/ASPFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract getSelectedFileIndexes()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract init(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/mfluent/asp/common/datamodel/ASPFileSortType;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract isAllSelected()Z
.end method

.method public abstract isSelected(I)Z
.end method

.method public abstract registerDataSetObserver(Landroid/database/DataSetObserver;)V
.end method

.method public abstract selectAll()V
.end method

.method public abstract setSelected(IZ)V
.end method

.method public abstract unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
.end method
