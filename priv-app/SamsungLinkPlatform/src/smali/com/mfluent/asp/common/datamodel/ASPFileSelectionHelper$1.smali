.class Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->getSelectedFileIndexes()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private index:I

.field final synthetic this$0:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;)V
    .locals 1

    .prologue
    .line 77
    iput-object p1, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->this$0:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->getNextSelected(I)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->index:I

    return-void
.end method

.method private getNextSelected(I)I
    .locals 1

    .prologue
    .line 99
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->this$0:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    # getter for: Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->totalCount:I
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->access$000(Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;)I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->this$0:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->isSelected(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    :goto_1
    return p1

    .line 99
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 105
    :cond_1
    const/4 p1, -0x1

    goto :goto_1
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 83
    iget v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->index:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 88
    iget v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->index:I

    .line 89
    iget v1, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->index:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->getNextSelected(I)I

    move-result v1

    iput v1, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->index:I

    .line 90
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->next()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->this$0:Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;

    iget v1, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper$1;->index:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPFileSelectionHelper;->setSelected(IZ)V

    .line 96
    return-void
.end method
