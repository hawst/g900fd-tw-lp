.class public Lcom/mfluent/asp/common/content/MultiColumnContentId;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/content/ContentId;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mfluent/asp/common/content/MultiColumnContentId;",
            ">;"
        }
    .end annotation
.end field

.field private static final PARCEL_VERSION:I = 0x4


# instance fields
.field private final mIdFields:[Ljava/lang/String;

.field private final mIdTypes:[I

.field private final mIdValues:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/mfluent/asp/common/content/MultiColumnContentId$1;

    invoke-direct {v0}, Lcom/mfluent/asp/common/content/MultiColumnContentId$1;-><init>()V

    sput-object v0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    const-string v0, "_id"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/common/content/MultiColumnContentId;-><init>(Ljava/lang/String;I)V

    .line 45
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/BadParcelableException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 72
    const/4 v2, 0x3

    if-lt v0, v2, :cond_0

    if-le v0, v4, :cond_1

    .line 73
    :cond_0
    new-instance v1, Landroid/os/BadParcelableException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Invalid parcel version:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 76
    :cond_1
    if-ge v0, v4, :cond_2

    .line 77
    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    .line 78
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    .line 79
    new-array v0, v3, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    .line 80
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aput v2, v0, v1

    .line 81
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    :goto_0
    move v0, v1

    .line 87
    :goto_1
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-eqz v2, :cond_3

    move v2, v3

    .line 89
    :goto_2
    if-eqz v2, :cond_4

    .line 90
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    aget v2, v2, v0

    packed-switch v2, :pswitch_data_0

    .line 87
    :goto_3
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 83
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 88
    goto :goto_2

    .line 92
    :pswitch_1
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v0

    goto :goto_3

    .line 95
    :pswitch_2
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    goto :goto_3

    .line 99
    :cond_4
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v4, v2, v0

    goto :goto_3

    .line 102
    :cond_5
    return-void

    .line 90
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>(Lcom/mfluent/asp/common/content/MultiColumnContentId;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iget-object v0, p1, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    .line 66
    iget-object v0, p1, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    .line 67
    iget-object v0, p1, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    .line 49
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    .line 50
    new-array v0, v1, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    .line 51
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    aput p2, v0, v2

    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    aput-object p1, v0, v2

    .line 53
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;[I)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    array-length v0, p1

    if-eqz v0, :cond_0

    array-length v0, p2

    if-eqz v0, :cond_0

    array-length v0, p1

    array-length v1, p2

    if-eq v0, v1, :cond_1

    .line 57
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid length for array(s)."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_1
    invoke-virtual {p1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    .line 60
    invoke-virtual {p2}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    .line 61
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    .line 62
    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 226
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 228
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 229
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    move v1, v2

    .line 230
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 231
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    move v0, v2

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 232
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 230
    :cond_0
    :goto_2
    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 231
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 235
    :pswitch_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_2

    .line 238
    :pswitch_2
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 243
    :cond_2
    return-void

    .line 233
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 137
    if-nez p1, :cond_1

    .line 147
    :cond_0
    :goto_0
    return v0

    .line 140
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 141
    goto :goto_0

    .line 143
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 144
    check-cast p1, Lcom/mfluent/asp/common/content/MultiColumnContentId;

    .line 145
    iget-object v2, p1, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    iget-object v3, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    iget-object v3, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public equalsCursorValues(Landroid/database/Cursor;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 153
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 155
    const/4 v2, 0x0

    .line 156
    iget-object v4, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    aget v4, v4, v0

    if-ne v4, v3, :cond_1

    .line 157
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 162
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v4, v4, v0

    invoke-static {v4, v2}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 167
    :goto_2
    return v1

    .line 158
    :cond_1
    iget-object v4, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    aget v4, v4, v0

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 159
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 153
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v1, v3

    .line 167
    goto :goto_2
.end method

.method public getIdFieldName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getIdFieldName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getMediaId()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getMediaId(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getSqlFilterSelection(Z)Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x27

    .line 180
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x5

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 182
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 183
    if-lez v0, :cond_0

    .line 184
    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    :cond_0
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    .line 188
    const-string v2, " IS NULL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    :cond_1
    if-eqz p1, :cond_3

    .line 190
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    aget v2, v2, v0

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 191
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\'"

    const-string v4, "\'\'"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 192
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 196
    :cond_2
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 200
    :cond_3
    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 204
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSqlFilterSelectionArgs()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 208
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 210
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 211
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 212
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 216
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 217
    const/4 v0, 0x0

    .line 219
    :goto_1
    return-object v0

    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 172
    new-instance v1, Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    invoke-direct {v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;-><init>()V

    .line 173
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 174
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 176
    :cond_0
    invoke-virtual {v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->build()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public setMediaId(Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->setMediaId(Ljava/lang/Long;I)V

    .line 257
    return-void
.end method

.method public setMediaId(Ljava/lang/Long;I)V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    aget v0, v0, p2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 261
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Id type is not FIELD_TYPE_INTEGER."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aput-object p1, v0, p2

    .line 264
    return-void
.end method

.method public setMediaId(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->setMediaId(Ljava/lang/String;I)V

    .line 268
    return-void
.end method

.method public setMediaId(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    aget v0, v0, p2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 272
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Id type is not FIELD_TYPE_STRING."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aput-object p1, v0, p2

    .line 275
    return-void
.end method

.method public setValues(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 106
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    aget v1, v1, v0

    packed-switch v1, :pswitch_data_0

    .line 106
    :goto_1
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    :pswitch_1
    iget-object v1, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 113
    :pswitch_2
    iget-object v1, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 117
    :cond_0
    return-void

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 279
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 280
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdTypes:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 282
    if-lez v0, :cond_0

    .line 283
    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 285
    :cond_0
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdFields:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 287
    iget-object v2, p0, Lcom/mfluent/asp/common/content/MultiColumnContentId;->mIdValues:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 281
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 289
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 252
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/content/MultiColumnContentId;->writeToParcel(Landroid/os/Parcel;)V

    .line 253
    return-void
.end method
