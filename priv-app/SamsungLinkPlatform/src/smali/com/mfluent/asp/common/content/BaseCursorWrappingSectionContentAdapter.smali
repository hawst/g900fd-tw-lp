.class public abstract Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;
.super Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/mfluent/asp/common/content/ContentId;",
        ">",
        "Lcom/mfluent/asp/common/content/BaseSectionContentAdapter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private mCursor:Landroid/database/Cursor;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    .line 22
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/BadParcelableException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;-><init>(Landroid/os/Parcel;)V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    .line 26
    return-void
.end method


# virtual methods
.method public cancelLoad(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 306
    instance-of v0, p1, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 307
    check-cast v0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;

    .line 308
    iget-object v1, v0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;->mLoadCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 309
    iget-object v1, v0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;->mLoadCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 310
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;->mLoadCursor:Landroid/database/Cursor;

    .line 313
    :cond_0
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->cancelLoad(Ljava/lang/Object;)V

    .line 314
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 264
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    .line 266
    :cond_0
    return-void
.end method

.method public copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 194
    :cond_0
    return-void
.end method

.method public deactivate()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->close()V

    .line 257
    return-void
.end method

.method public finishLoad(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 323
    instance-of v0, p1, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;

    if-eqz v0, :cond_1

    .line 324
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, p1

    .line 327
    check-cast v0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;

    iget-object v0, v0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;->mLoadCursor:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    .line 329
    :cond_1
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->finishLoad(Ljava/lang/Object;)V

    .line 330
    return-void
.end method

.method public getBlob(I)[B
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 177
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    .line 169
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 135
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 142
    if-gez v0, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Column "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    return v0
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    .line 153
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 161
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 38
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDouble(I)D
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    .line 233
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getFloat(I)F
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    .line 225
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInt(I)I
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 209
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 217
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 46
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getShort(I)S
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    .line 201
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 185
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getType(I)I
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    .line 241
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLoadToFinish(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 318
    instance-of v0, p1, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;

    return v0
.end method

.method public isAfterLast()Z
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    .line 127
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBeforeFirst()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v0

    .line 119
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    .line 274
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isDataLoaded()Z
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFirst()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isFirst()Z

    move-result v0

    .line 103
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLast()Z
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    .line 111
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNull(I)Z
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    .line 249
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected abstract loadCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
.end method

.method protected loadDataImpl(Landroid/database/Cursor;)Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$BaseSectionLoadContext;
    .locals 6

    .prologue
    .line 281
    const/4 v0, 0x0

    .line 282
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->loadCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v2

    .line 284
    if-eqz v2, :cond_2

    .line 285
    new-instance v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;

    invoke-direct {v1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;-><init>()V

    .line 286
    iput-object v2, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;->mLoadCursor:Landroid/database/Cursor;

    .line 287
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;->mLoadSections:Ljava/util/ArrayList;

    .line 288
    const/4 v0, 0x0

    .line 289
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 290
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->getCountFieldName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 292
    :cond_0
    new-instance v4, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;

    invoke-direct {v4}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;-><init>()V

    .line 293
    iput v0, v4, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;->mOffset:I

    .line 294
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v4, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;->mLength:I

    .line 295
    iget-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter$CursorWrappingLoadContext;->mLoadSections:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    iget v4, v4, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;->mLength:I

    add-int/2addr v0, v4

    .line 297
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    :cond_1
    move-object v0, v1

    .line 301
    :cond_2
    return-object v0
.end method

.method public move(I)Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->move(I)Z

    move-result v0

    .line 54
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToFirst()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 70
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToLast()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    .line 79
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToNext()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    .line 87
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToPosition(I)Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    .line 62
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToPrevious()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v0

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
