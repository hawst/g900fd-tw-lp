.class public abstract Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/content/SectionContentAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$BaseSectionLoadContext;,
        Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/mfluent/asp/common/content/ContentId;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/mfluent/asp/common/content/SectionContentAdapter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mCollapsedSections:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mCountFieldName:Ljava/lang/String;

.field private mIsDestroyed:Z

.field private mSections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, "_count"

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCountFieldName:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mIsDestroyed:Z

    .line 33
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCollapsedSections:Ljava/util/HashSet;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v1, "_count"

    iput-object v1, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCountFieldName:Ljava/lang/String;

    .line 32
    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mIsDestroyed:Z

    .line 33
    new-instance v1, Ljava/util/HashSet;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCollapsedSections:Ljava/util/HashSet;

    .line 34
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCountFieldName:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    move v1, v0

    .line 43
    :goto_0
    if-ge v1, v2, :cond_0

    .line 44
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/ContentId;

    .line 46
    iget-object v3, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCollapsedSections:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 43
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 48
    :cond_0
    return-void
.end method

.method private sectionContainsRow(Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;I)I
    .locals 2

    .prologue
    .line 208
    iget v0, p1, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;->mOffset:I

    iget v1, p1, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;->mLength:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    .line 209
    iget v1, p1, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;->mOffset:I

    if-ge p2, v1, :cond_0

    .line 210
    const/4 v0, -0x1

    .line 214
    :goto_0
    return v0

    .line 211
    :cond_0
    if-le p2, v0, :cond_1

    .line 212
    const/4 v0, 0x1

    goto :goto_0

    .line 214
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public acquireSourceReadLock()V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method public cancelLoad(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method protected abstract copySectionId(Lcom/mfluent/asp/common/content/ContentId;)Lcom/mfluent/asp/common/content/ContentId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mIsDestroyed:Z

    .line 110
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 111
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCollapsedSections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 112
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->close()V

    .line 113
    return-void
.end method

.method public finishLoad(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 96
    instance-of v0, p1, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$BaseSectionLoadContext;

    if-eqz v0, :cond_0

    .line 97
    check-cast p1, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$BaseSectionLoadContext;

    iget-object v0, p1, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$BaseSectionLoadContext;->mLoadSections:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    .line 99
    :cond_0
    return-void
.end method

.method protected final getCountFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCountFieldName:Ljava/lang/String;

    return-object v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNotificationUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNumRowsInSection(I)I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;

    .line 104
    iget v0, v0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;->mLength:I

    return v0
.end method

.method public getSectionEndRow(I)I
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;

    .line 227
    iget v1, v0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;->mOffset:I

    iget v0, v0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;->mLength:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getSectionForRow(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 161
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 162
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "There are no sections available."

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    .line 170
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;

    .line 171
    invoke-direct {p0, v0, p1}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->sectionContainsRow(Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;I)I

    move-result v0

    .line 172
    if-nez v0, :cond_1

    move v0, v1

    .line 191
    :goto_0
    return v0

    .line 174
    :cond_1
    if-gez v0, :cond_2

    .line 175
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "Illegal row. Before first section."

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;

    .line 179
    invoke-direct {p0, v0, p1}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->sectionContainsRow(Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;I)I

    move-result v0

    .line 180
    if-nez v0, :cond_3

    move v0, v2

    .line 181
    goto :goto_0

    .line 182
    :cond_3
    if-lez v0, :cond_8

    .line 183
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "Illegal row. Past last section."

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_4
    if-gez v0, :cond_5

    move v2, v1

    .line 186
    :goto_1
    if-ge v3, v2, :cond_7

    .line 187
    sub-int v0, v2, v3

    div-int/lit8 v0, v0, 0x2

    add-int v1, v0, v3

    .line 188
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;

    .line 189
    invoke-direct {p0, v0, p1}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->sectionContainsRow(Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;I)I

    move-result v0

    .line 190
    if-nez v0, :cond_4

    move v0, v1

    .line 191
    goto :goto_0

    .line 196
    :cond_5
    if-ne v3, v1, :cond_6

    move v3, v2

    .line 197
    goto :goto_1

    :cond_6
    move v3, v1

    .line 202
    goto :goto_1

    .line 204
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Row was not found in section list."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move v3, v1

    goto :goto_1
.end method

.method public getSectionStartRow(I)I
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mSections:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;

    .line 221
    iget v0, v0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$SectionInfo;->mOffset:I

    return v0
.end method

.method public getWantsAllOnMoveCalls()Z
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    return v0
.end method

.method public hasLoadToFinish(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 91
    instance-of v0, p1, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$BaseSectionLoadContext;

    return v0
.end method

.method public initContext(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mIsDestroyed:Z

    .line 62
    return-void
.end method

.method protected abstract instantiateSectionId()Lcom/mfluent/asp/common/content/ContentId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mIsDestroyed:Z

    return v0
.end method

.method public isSectionCollapsed(I)Z
    .locals 2

    .prologue
    .line 148
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->instantiateSectionId()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    .line 150
    invoke-interface {v0, p0}, Lcom/mfluent/asp/common/content/ContentId;->setValues(Landroid/database/Cursor;)V

    .line 152
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCollapsedSections:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 155
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadData(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->loadDataImpl(Landroid/database/Cursor;)Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$BaseSectionLoadContext;

    move-result-object v0

    return-object v0
.end method

.method protected abstract loadDataImpl(Landroid/database/Cursor;)Lcom/mfluent/asp/common/content/BaseSectionContentAdapter$BaseSectionLoadContext;
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 250
    return-void
.end method

.method public releaseSourceReadLock()V
    .locals 0

    .prologue
    .line 290
    return-void
.end method

.method public requery()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 234
    const/4 v0, 0x0

    return v0
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    return-object v0
.end method

.method public setCountFieldName(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCountFieldName:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 67
    :goto_0
    if-eqz v0, :cond_0

    .line 68
    iput-object p1, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCountFieldName:Ljava/lang/String;

    .line 70
    :cond_0
    return v0

    .line 66
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 264
    return-void
.end method

.method public setSectionCollapsed(IZ)Z
    .locals 2

    .prologue
    .line 127
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->instantiateSectionId()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    .line 129
    invoke-interface {v0, p0}, Lcom/mfluent/asp/common/content/ContentId;->setValues(Landroid/database/Cursor;)V

    .line 131
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCollapsedSections:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 132
    if-eq v1, p2, :cond_1

    .line 133
    if-eqz p2, :cond_0

    .line 134
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCollapsedSections:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 139
    :goto_0
    const/4 v0, 0x1

    .line 143
    :goto_1
    return v0

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCollapsedSections:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 0

    .prologue
    .line 245
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCountFieldName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCollapsedSections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseSectionContentAdapter;->mCollapsedSections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/ContentId;

    .line 55
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 57
    :cond_0
    return-void
.end method
