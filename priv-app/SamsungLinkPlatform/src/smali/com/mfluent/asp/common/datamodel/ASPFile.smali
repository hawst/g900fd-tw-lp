.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPFile;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getSpecialDirectoryType()Ljava/lang/String;
.end method

.method public abstract isDirectory()Z
.end method

.method public abstract isPersonal()Z
.end method

.method public abstract isSecure()Z
.end method

.method public abstract lastModified()J
.end method

.method public abstract length()J
.end method
