.class public Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/content/BaseContentAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "LoadContext"
.end annotation


# instance fields
.field public mLoadToken:I

.field public mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

.field public mNotFoundMultiSelections:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/mfluent/asp/common/content/ContentId;",
            ">;"
        }
    .end annotation
.end field

.field public mSectionLoadContext:Ljava/lang/Object;

.field public mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

.field public mSingleSelectedIndex:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;->mSingleSelectedIndex:I

    return-void
.end method
