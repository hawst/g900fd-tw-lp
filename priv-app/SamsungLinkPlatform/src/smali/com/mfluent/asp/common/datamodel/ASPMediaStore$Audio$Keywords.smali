.class public final Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Keywords;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$AudioColumns;
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$KeywordColumns;
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Keywords"
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String;

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final ENTRY_CONTENT_TYPE:Ljava/lang/String;

.field public static final PATH:Ljava/lang/String; = "audio_keywords"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1071
    const-string v0, "audio_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$5400(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Keywords;->CONTENT_TYPE:Ljava/lang/String;

    .line 1073
    const-string v0, "audio_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$5500(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Keywords;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    .line 1075
    const-string v0, "audio_keywords"

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Keywords;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContentUriForDevice(I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1078
    int-to-long v0, p0

    const-string v2, "audio_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$5600(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getEntryUri(I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1082
    int-to-long v0, p0

    const-string v2, "audio_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$5700(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getGeneralGroupingUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1090
    const-string v0, "audio_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$5800(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getGroupByFileIdUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1086
    const-string v0, "audio_keywords"

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGroupByFileIdUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final instanceGetContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1100
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Keywords;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public final instanceGetContentUriForDevice(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1105
    const-string v0, "audio_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$6000(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final instanceGetEntryUri(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1095
    const-string v0, "audio_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$5900(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
