.class public Lcom/mfluent/asp/common/util/MimeType;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 8
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 11
    sput-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "EML"

    const-string v2, "message/rfc822"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MP3"

    const-string v2, "audio/mpeg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "M4A"

    const-string v2, "audio/mp4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "WAV"

    const-string v2, "audio/x-wav"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "AMR"

    const-string v2, "audio/amr"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "AWB"

    const-string v2, "audio/amr-wb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "WMA"

    const-string v2, "audio/x-ms-wma"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "OGG"

    const-string v2, "audio/ogg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "OGA"

    const-string v2, "audio/ogg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "AAC"

    const-string v2, "audio/aac"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "3GA"

    const-string v2, "audio/3gpp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "FLAC"

    const-string v2, "audio/flac"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MPGA"

    const-string v2, "audio/mpeg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MP4_A"

    const-string v2, "audio/mp4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "3GP_A"

    const-string v2, "audio/3gpp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "3G2_A"

    const-string v2, "audio/3gpp2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "ASF_A"

    const-string v2, "audio/x-ms-asf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "3GPP_A"

    const-string v2, "audio/3gpp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MID"

    const-string v2, "audio/midi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "XMF"

    const-string v2, "audio/midi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MXMF"

    const-string v2, "audio/midi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "RTTTL"

    const-string v2, "audio/midi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "SMF"

    const-string v2, "audio/sp-midi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "IMY"

    const-string v2, "audio/imelody"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MIDI"

    const-string v2, "audio/midi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "RTX"

    const-string v2, "audio/midi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "OTA"

    const-string v2, "audio/midi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "PYA"

    const-string v2, "audio/vnd.ms-playready.media.pya"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "M4B"

    const-string v2, "audio/mp4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "QCP"

    const-string v2, "audio/qcelp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MPEG"

    const-string v2, "video/mpeg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MPG"

    const-string v2, "video/mpeg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MP4"

    const-string v2, "video/mp4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "M4V"

    const-string v2, "video/mp4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "3GP"

    const-string v2, "video/3gpp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "3GPP"

    const-string v2, "video/3gpp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "3G2"

    const-string v2, "video/3gpp2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "3GPP2"

    const-string v2, "video/3gpp2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "WMV"

    const-string v2, "video/x-ms-wmv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "ASF"

    const-string v2, "video/x-ms-asf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "AVI"

    const-string v2, "video/avi"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "DIVX"

    const-string v2, "video/divx"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "FLV"

    const-string v2, "video/flv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MKV"

    const-string v2, "video/mkv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "SDP"

    const-string v2, "application/sdp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "RM"

    const-string v2, "video/mp4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "RMVB"

    const-string v2, "video/mp4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MOV"

    const-string v2, "video/quicktime"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "PYV"

    const-string v2, "video/vnd.ms-playready.media.pyv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "ISMV"

    const-string v2, "video/ismv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "SKM"

    const-string v2, "video/skm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "K3G"

    const-string v2, "video/k3g"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "AK3G"

    const-string v2, "video/ak3g"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "WEBM"

    const-string v2, "video/webm"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "JPG"

    const-string v2, "image/jpeg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "JPEG"

    const-string v2, "image/jpeg"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MY5"

    const-string v2, "image/vnd.tmo.my5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "GIF"

    const-string v2, "image/gif"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "PNG"

    const-string v2, "image/png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "BMP"

    const-string v2, "image/x-ms-bmp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "WBMP"

    const-string v2, "image/vnd.wap.wbmp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "MPO"

    const-string v2, "image/mpo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "M3U"

    const-string v2, "audio/x-mpegurl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "PLS"

    const-string v2, "audio/x-scpls"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "WPL"

    const-string v2, "application/vnd.ms-wpl"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "PDF"

    const-string v2, "application/pdf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "RTF"

    const-string v2, "application/msword"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "DOC"

    const-string v2, "application/msword"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "DOCX"

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "DOT"

    const-string v2, "application/msword"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "DOTX"

    const-string v2, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "XLS"

    const-string v2, "application/vnd.ms-excel"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "XLSX"

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "XLT"

    const-string v2, "application/vnd.ms-excel"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "XLTX"

    const-string v2, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "PPT"

    const-string v2, "application/vnd.ms-powerpoint"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "PPTX"

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "POT"

    const-string v2, "application/vnd.ms-powerpoint"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "POTX"

    const-string v2, "application/vnd.openxmlformats-officedocument.presentationml.template"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "TXT"

    const-string v2, "text/plain"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "GUL"

    const-string v2, "application/jungumword"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "EPUB"

    const-string v2, "application/epub+zip"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "ACSM"

    const-string v2, "application/vnd.adobe.adept+xml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "SWF"

    const-string v2, "application/x-shockwave-flash"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "SVG"

    const-string v2, "image/svg+xml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "DCF"

    const-string v2, "application/vnd.oma.drm.content"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "ODF"

    const-string v2, "application/vnd.oma.drm.content"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "APK"

    const-string v2, "application/vnd.android.package-archive"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "JAD"

    const-string v2, "text/vnd.sun.j2me.app-descriptor"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "JAR"

    const-string v2, "application/java-archive "

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "VCS"

    const-string v2, "text/x-vCalendar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "ICS"

    const-string v2, "text/x-vCalendar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "VTS"

    const-string v2, "text/x-vtodo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "VCF"

    const-string v2, "text/x-vcard"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "VNT"

    const-string v2, "text/x-vnote"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "HTML"

    const-string v2, "text/html"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "HTM"

    const-string v2, "text/html"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "XML"

    const-string v2, "application/xhtml+xml"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "WGT"

    const-string v2, "application/vnd.samsung.widget"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "HWP"

    const-string v2, "application/x-hwp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "SNB"

    const-string v2, "application/snb"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    const-string v1, "GOLF"

    const-string v2, "image/golf"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
