.class public final enum Lcom/mfluent/asp/common/datamodel/ASPFileSortType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/common/datamodel/ASPFileSortType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field public static final enum DATE_MODIFIED_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field public static final enum DATE_MODIFIED_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field public static final enum DURATION_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field public static final enum DURATION_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field public static final enum NAME_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field public static final enum NAME_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field public static final enum SIZE_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field public static final enum SIZE_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field public static final enum TYPE_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

.field public static final enum TYPE_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;


# instance fields
.field private final cursorSortOrder:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 13
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v1, "DATE_MODIFIED_ASC"

    const-string v2, "last_modified"

    invoke-direct {v0, v1, v4, v2}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DATE_MODIFIED_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 14
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v1, "DATE_MODIFIED_DESC"

    const-string v2, "last_modified DESC"

    invoke-direct {v0, v1, v5, v2}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DATE_MODIFIED_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 15
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v1, "SIZE_ASC"

    const-string v2, "_size"

    invoke-direct {v0, v1, v6, v2}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->SIZE_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 16
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v1, "SIZE_DESC"

    const-string v2, "_size DESC"

    invoke-direct {v0, v1, v7, v2}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->SIZE_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 17
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v1, "NAME_ASC"

    const-string v2, "_display_name"

    invoke-direct {v0, v1, v8, v2}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 18
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v1, "NAME_DESC"

    const/4 v2, 0x5

    const-string v3, "_display_name DESC"

    invoke-direct {v0, v1, v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 19
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v1, "TYPE_ASC"

    const/4 v2, 0x6

    const-string v3, "mime_type"

    invoke-direct {v0, v1, v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->TYPE_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 20
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v1, "TYPE_DESC"

    const/4 v2, 0x7

    const-string v3, "mime_type DESC"

    invoke-direct {v0, v1, v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->TYPE_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 21
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v1, "DURATION_ASC"

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DURATION_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 22
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    const-string v1, "DURATION_DESC"

    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DURATION_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    .line 12
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DATE_MODIFIED_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DATE_MODIFIED_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->SIZE_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->SIZE_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->NAME_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->TYPE_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->TYPE_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DURATION_ASC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DURATION_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->$VALUES:[Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput-object p3, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->cursorSortOrder:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public static getDefaultSortType()Lcom/mfluent/asp/common/datamodel/ASPFileSortType;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->DATE_MODIFIED_DESC:Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    return-object v0
.end method

.method public static getSortTypeFromCursorSortOrder(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSortType;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 35
    if-nez p0, :cond_1

    .line 45
    :cond_0
    :goto_0
    return-object v0

    .line 39
    :cond_1
    invoke-static {}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->values()[Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 40
    iget-object v5, v1, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->cursorSortOrder:Ljava/lang/String;

    invoke-virtual {p0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 41
    goto :goto_0

    .line 39
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSortType;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/common/datamodel/ASPFileSortType;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->$VALUES:[Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    invoke-virtual {v0}, [Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    return-object v0
.end method


# virtual methods
.method public final getCursorSortOrder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->cursorSortOrder:Ljava/lang/String;

    return-object v0
.end method
