.class public final enum Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FileTransferState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

.field public static final enum CANCELLED:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

.field public static final enum COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

.field public static final enum ERROR:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

.field public static final enum IN_PROGRESS:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

.field public static final enum PENDING:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1508
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->PENDING:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    .line 1509
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->IN_PROGRESS:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    .line 1510
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    .line 1511
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ERROR:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    .line 1512
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->CANCELLED:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    .line 1507
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->PENDING:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->IN_PROGRESS:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ERROR:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->CANCELLED:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->$VALUES:[Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1507
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromCursor(Landroid/database/Cursor;)Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;
    .locals 1

    .prologue
    .line 1515
    const-string v0, "state"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 1516
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1517
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->valueOf(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;
    .locals 1

    .prologue
    .line 1507
    const-class v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;
    .locals 1

    .prologue
    .line 1507
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->$VALUES:[Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v0}, [Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    return-object v0
.end method


# virtual methods
.method public final toContentValues(Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 1521
    const-string v0, "state"

    invoke-virtual {p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522
    return-void
.end method
