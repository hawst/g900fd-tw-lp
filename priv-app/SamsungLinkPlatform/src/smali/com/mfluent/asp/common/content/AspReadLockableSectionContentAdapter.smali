.class public Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter;
.super Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mLockCursor:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter$1;

    invoke-direct {v0}, Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter$1;-><init>()V

    sput-object v0, Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;-><init>(Landroid/os/Parcel;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;-><init>(Ljava/lang/String;I)V

    .line 29
    return-void
.end method


# virtual methods
.method public acquireSourceReadLock()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter;->releaseSourceReadLock()V

    .line 38
    iget-object v0, p0, Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$ReadLock;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter;->mLockCursor:Landroid/database/Cursor;

    .line 39
    return-void
.end method

.method public close()V
    .locals 0

    .prologue
    .line 51
    invoke-super {p0}, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->close()V

    .line 52
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter;->releaseSourceReadLock()V

    .line 53
    return-void
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 57
    invoke-super {p0}, Lcom/mfluent/asp/common/content/SingleIdFieldSectionContentAdapter;->destroy()V

    .line 58
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter;->releaseSourceReadLock()V

    .line 59
    return-void
.end method

.method public releaseSourceReadLock()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter;->mLockCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter;->mLockCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/AspReadLockableSectionContentAdapter;->mLockCursor:Landroid/database/Cursor;

    .line 47
    :cond_0
    return-void
.end method
