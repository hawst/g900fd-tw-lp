.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UriProvider"
.end annotation


# virtual methods
.method public abstract instanceGetContentUri()Landroid/net/Uri;
.end method

.method public abstract instanceGetContentUriForDevice(J)Landroid/net/Uri;
.end method

.method public abstract instanceGetEntryUri(J)Landroid/net/Uri;
.end method
