.class public abstract Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;
.super Lcom/mfluent/asp/common/content/BaseContentAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/mfluent/asp/common/content/ContentId;",
        ">",
        "Lcom/mfluent/asp/common/content/BaseContentAdapter",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final logger:Lorg/slf4j/Logger;


# instance fields
.field private mContentObserver:Landroid/database/ContentObserver;

.field protected mCursor:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->logger:Lorg/slf4j/Logger;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    .line 28
    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mContentObserver:Landroid/database/ContentObserver;

    .line 32
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/BadParcelableException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;-><init>(Landroid/os/Parcel;)V

    .line 27
    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    .line 28
    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mContentObserver:Landroid/database/ContentObserver;

    .line 36
    return-void
.end method

.method private findRowOfId(Lcom/mfluent/asp/common/content/ContentId;Landroid/database/Cursor;)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 320
    if-nez p1, :cond_1

    move v0, v1

    .line 337
    :cond_0
    :goto_0
    return v0

    .line 324
    :cond_1
    if-eqz p2, :cond_3

    .line 325
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 326
    const/4 v0, 0x0

    .line 329
    :cond_2
    invoke-interface {p1, p2}, Lcom/mfluent/asp/common/content/ContentId;->equalsCursorValues(Landroid/database/Cursor;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 332
    add-int/lit8 v0, v0, 0x1

    .line 333
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_3
    move v0, v1

    .line 337
    goto :goto_0
.end method


# virtual methods
.method protected cancelLoad(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 382
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->cancelLoad(Ljava/lang/Object;)V

    .line 384
    instance-of v0, p1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;

    if-eqz v0, :cond_0

    .line 385
    check-cast p1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;

    .line 386
    iget-object v0, p1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 388
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mCursor:Landroid/database/Cursor;

    .line 391
    :cond_0
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 281
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->close()V

    .line 282
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->unregisterCursor(Landroid/database/Cursor;)V

    .line 284
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 285
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    .line 287
    :cond_0
    return-void
.end method

.method public copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 214
    :cond_0
    return-void
.end method

.method public deactivate()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->close()V

    .line 277
    return-void
.end method

.method public bridge synthetic deregisterListener(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->deregisterListener(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)V

    return-void
.end method

.method public bridge synthetic destroy()V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->destroy()V

    return-void
.end method

.method protected doRequery(ZI)Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 395
    .line 397
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->getLoadToken()I

    move-result v1

    if-ne p2, v1, :cond_a

    .line 402
    :try_start_0
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    if-eqz v1, :cond_d

    .line 403
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    .line 404
    iget-object v4, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    invoke-interface {v4}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->acquireSourceReadLock()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v4, v1

    .line 408
    :goto_0
    :try_start_1
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->openCursor()Landroid/database/Cursor;

    move-result-object v5

    .line 410
    if-eqz v5, :cond_c

    .line 411
    new-instance v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;

    invoke-direct {v1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 412
    :try_start_2
    iput-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mCursor:Landroid/database/Cursor;

    .line 413
    iput p2, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mLoadToken:I

    .line 415
    iget-object v5, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    if-eqz v5, :cond_0

    .line 416
    iget-object v5, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    iget-object v6, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5, v6}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->loadData(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v5

    iput-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mSectionLoadContext:Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 420
    :cond_0
    :goto_1
    if-eqz v4, :cond_1

    .line 421
    :try_start_3
    invoke-interface {v4}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->releaseSourceReadLock()V

    .line 423
    :cond_1
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mIsLoading:Z

    .line 426
    if-eqz v1, :cond_7

    .line 427
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->getSingleSelectedId()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v4

    iput-object v4, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    .line 428
    iget-object v4, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    if-eqz v4, :cond_9

    .line 430
    :goto_2
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->isEditing()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 431
    new-instance v0, Lcom/mfluent/asp/common/util/IntVector;

    const/16 v4, 0x14

    const/16 v5, 0x14

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZZ)V

    iput-object v0, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    .line 432
    new-instance v0, Ljava/util/HashSet;

    iget-object v4, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-direct {v0, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mNotFoundMultiSelections:Ljava/util/HashSet;

    .line 433
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->instantiateId()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    :cond_2
    move v4, v3

    .line 437
    :goto_3
    if-nez v2, :cond_3

    iget-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mNotFoundMultiSelections:Ljava/util/HashSet;

    if-eqz v5, :cond_7

    iget-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mNotFoundMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    if-lez v5, :cond_7

    :cond_3
    iget-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 439
    if-eqz v2, :cond_4

    iget-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    iget-object v6, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5, v6}, Lcom/mfluent/asp/common/content/ContentId;->equalsCursorValues(Landroid/database/Cursor;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 441
    iput v4, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mSingleSelectedIndex:I

    move v2, v3

    .line 443
    :cond_4
    iget-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mNotFoundMultiSelections:Ljava/util/HashSet;

    if-eqz v5, :cond_5

    iget-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mNotFoundMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    if-lez v5, :cond_5

    .line 444
    iget-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, v5}, Lcom/mfluent/asp/common/content/ContentId;->setValues(Landroid/database/Cursor;)V

    .line 445
    iget-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mNotFoundMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 446
    iget-object v5, v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v5, v4}, Lcom/mfluent/asp/common/util/IntVector;->insert(I)I

    .line 450
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 420
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_4
    if-eqz v4, :cond_6

    .line 421
    invoke-interface {v4}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->releaseSourceReadLock()V

    .line 423
    :cond_6
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mIsLoading:Z

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 453
    :catch_0
    move-exception v0

    .line 454
    :goto_5
    sget-object v2, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->logger:Lorg/slf4j/Logger;

    const-string v3, "::doRequery error loading data."

    invoke-interface {v2, v3, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 464
    :cond_7
    :goto_6
    if-eqz p1, :cond_8

    .line 465
    if-eqz v1, :cond_b

    .line 466
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->postDataLoadedMessage(Ljava/lang/Object;)V

    .line 472
    :cond_8
    :goto_7
    return-object v1

    :cond_9
    move v2, v3

    .line 428
    goto/16 :goto_2

    .line 457
    :cond_a
    sget-object v1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->logger:Lorg/slf4j/Logger;

    const-string v4, "::doRequery Load token ({}) does not match the latest token ({}). Skipping load. {}."

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->getLoadToken()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    invoke-interface {v1, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v1, v0

    goto :goto_6

    .line 468
    :cond_b
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->postLoadFailedMessage()V

    goto :goto_7

    .line 453
    :catch_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_5

    .line 420
    :catchall_1
    move-exception v0

    goto :goto_4

    :cond_c
    move-object v1, v0

    goto/16 :goto_1

    :cond_d
    move-object v4, v0

    goto/16 :goto_0
.end method

.method protected finishDataLoad(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 364
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->finishDataLoad(Ljava/lang/Object;)V

    .line 366
    instance-of v0, p1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;

    if-eqz v0, :cond_1

    .line 367
    check-cast p1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;

    .line 368
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->unregisterCursor(Landroid/database/Cursor;)V

    .line 371
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 373
    :cond_0
    iget-object v0, p1, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;->mCursor:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    .line 374
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 375
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->registerCursor(Landroid/database/Cursor;)V

    .line 378
    :cond_1
    return-void
.end method

.method public bridge synthetic getAllSelectedRows()[I
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getAllSelectedRows()[I

    move-result-object v0

    return-object v0
.end method

.method public getBlob(I)[B
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 197
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    .line 189
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 155
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 160
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 162
    if-gez v0, :cond_0

    .line 163
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Column "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_0
    return v0
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    .line 173
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 181
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 58
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDouble(I)D
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    .line 253
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getFloat(I)F
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    .line 245
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getIdIndexes(Ljava/util/Set;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getIdIndexes(Ljava/util/Set;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method protected getIdOfCurrentRow()Lcom/mfluent/asp/common/content/ContentId;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->instantiateId()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    .line 307
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v1

    if-nez v1, :cond_0

    .line 308
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, v1}, Lcom/mfluent/asp/common/content/ContentId;->setValues(Landroid/database/Cursor;)V

    .line 311
    :cond_0
    return-object v0
.end method

.method public getInt(I)I
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 229
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 237
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getNumRowsMultiSelected()I
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getNumRowsMultiSelected()I

    move-result v0

    return v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 66
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getRowOfId(Lcom/mfluent/asp/common/content/ContentId;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .prologue
    .line 316
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-direct {p0, p1, v0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->findRowOfId(Lcom/mfluent/asp/common/content/ContentId;Landroid/database/Cursor;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic getSectionContentAdapter()Lcom/mfluent/asp/common/content/SectionContentAdapter;
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getSectionContentAdapter()Lcom/mfluent/asp/common/content/SectionContentAdapter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSelectionCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getSelectionCursor()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getShort(I)S
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    .line 221
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getSingleSelectedId()Lcom/mfluent/asp/common/content/ContentId;
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getSingleSelectedId()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSingleSelectedRow()I
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getSingleSelectedRow()I

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 205
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getType(I)I
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    .line 261
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getWantsAllOnMoveCalls()Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getWantsAllOnMoveCalls()Z

    move-result v0

    return v0
.end method

.method public initContext(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->initContext(Landroid/content/Context;)V

    .line 42
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mContentObserver:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$1;

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$1;-><init>(Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mContentObserver:Landroid/database/ContentObserver;

    .line 51
    :cond_0
    return-void
.end method

.method public isAfterLast()Z
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    .line 147
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic isAllSelected()Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isAllSelected()Z

    move-result v0

    return v0
.end method

.method public isBeforeFirst()Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v0

    .line 139
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    .line 295
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isDataLoaded()Z
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic isDataStale()Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isDataStale()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isDestroyed()Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isDestroyed()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isEditing()Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isEditing()Z

    move-result v0

    return v0
.end method

.method public isFirst()Z
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isFirst()Z

    move-result v0

    .line 123
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLast()Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    .line 131
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic isLoading()Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isLoading()Z

    move-result v0

    return v0
.end method

.method public isNull(I)Z
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    .line 269
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic isRowMultiSelected(I)Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isRowMultiSelected(I)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic loadData()V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->loadData()V

    return-void
.end method

.method public loadDataSynchronously()V
    .locals 2

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    :goto_0
    return-void

    .line 356
    :cond_0
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->loadDataSynchronously()V

    .line 358
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->getAndIncrementLoadToken()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->doRequery(ZI)Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter$CursorLoadContext;

    move-result-object v0

    .line 359
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->finishDataLoad(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public move(I)Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->move(I)Z

    move-result v0

    .line 74
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToFirst()Z
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 90
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToLast()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToNext()Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    .line 107
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToPosition(I)Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    .line 82
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveToPrevious()Z
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v0

    .line 115
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract openCursor()Landroid/database/Cursor;
.end method

.method public bridge synthetic pauseDataSetChangedNotifications(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->pauseDataSetChangedNotifications(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)V

    return-void
.end method

.method protected registerCursor(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mContentObserver:Landroid/database/ContentObserver;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 349
    return-void
.end method

.method public bridge synthetic registerListener(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;JJ)Z
    .locals 2

    .prologue
    .line 18
    invoke-super/range {p0 .. p5}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->registerListener(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;JJ)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic requery()Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->requery()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic resumeDataSetChangedNotifications(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->resumeDataSetChangedNotifications(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic setAllRowsMultiSelected(Z)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->setAllRowsMultiSelected(Z)V

    return-void
.end method

.method public bridge synthetic setIsEditing(Z)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->setIsEditing(Z)V

    return-void
.end method

.method public bridge synthetic setRowMultiSelected(IZ)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1, p2}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->setRowMultiSelected(IZ)V

    return-void
.end method

.method public bridge synthetic setSectionContentAdapter(Lcom/mfluent/asp/common/content/SectionContentAdapter;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->setSectionContentAdapter(Lcom/mfluent/asp/common/content/SectionContentAdapter;)V

    return-void
.end method

.method public bridge synthetic setSingleSelectedId(Lcom/mfluent/asp/common/content/ContentId;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->setSingleSelectedId(Lcom/mfluent/asp/common/content/ContentId;)V

    return-void
.end method

.method public bridge synthetic setSingleSelectedRow(I)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->setSingleSelectedRow(I)V

    return-void
.end method

.method protected unregisterCursor(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseCursorWrappingContentAdapter;->mContentObserver:Landroid/database/ContentObserver;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 345
    return-void
.end method

.method public bridge synthetic writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1, p2}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->writeToParcel(Landroid/os/Parcel;I)V

    return-void
.end method
