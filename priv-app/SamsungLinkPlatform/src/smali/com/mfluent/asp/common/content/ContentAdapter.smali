.class public interface abstract Lcom/mfluent/asp/common/content/ContentAdapter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/database/Cursor;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/mfluent/asp/common/content/ContentId;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/database/Cursor;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# virtual methods
.method public abstract createFilteredAdapter(Z)Lcom/mfluent/asp/common/content/ContentAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/mfluent/asp/common/content/ContentAdapter",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract createMediaSet()Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
.end method

.method public abstract deregisterListener(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)V
.end method

.method public abstract destroy()V
.end method

.method public abstract getAllSelectedRows()[I
.end method

.method public abstract getIdIndexes(Ljava/util/Set;)Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<TT;>;)",
            "Ljava/util/HashMap",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getNumRowsMultiSelected()I
.end method

.method public abstract getRowOfId(Lcom/mfluent/asp/common/content/ContentId;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation
.end method

.method public abstract getSectionContentAdapter()Lcom/mfluent/asp/common/content/SectionContentAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mfluent/asp/common/content/SectionContentAdapter",
            "<*>;"
        }
    .end annotation
.end method

.method public abstract getSelectionCursor()Landroid/database/Cursor;
.end method

.method public abstract getSingleSelectedId()Lcom/mfluent/asp/common/content/ContentId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract getSingleSelectedRow()I
.end method

.method public abstract initContext(Landroid/content/Context;)V
.end method

.method public abstract instantiateId()Lcom/mfluent/asp/common/content/ContentId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract isAllSelected()Z
.end method

.method public abstract isDataLoaded()Z
.end method

.method public abstract isDataStale()Z
.end method

.method public abstract isDestroyed()Z
.end method

.method public abstract isEditing()Z
.end method

.method public abstract isLoading()Z
.end method

.method public abstract isRowMultiSelected(I)Z
.end method

.method public abstract loadData()V
.end method

.method public abstract loadDataSynchronously()V
.end method

.method public abstract pauseDataSetChangedNotifications(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)V
.end method

.method public abstract registerListener(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;JJ)Z
.end method

.method public abstract resumeDataSetChangedNotifications(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)Z
.end method

.method public abstract setAllRowsMultiSelected(Z)V
.end method

.method public abstract setIsEditing(Z)V
.end method

.method public abstract setRowMultiSelected(IZ)V
.end method

.method public abstract setSectionContentAdapter(Lcom/mfluent/asp/common/content/SectionContentAdapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/common/content/SectionContentAdapter",
            "<*>;)V"
        }
    .end annotation
.end method

.method public abstract setSingleSelectedId(Lcom/mfluent/asp/common/content/ContentId;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public abstract setSingleSelectedRow(I)V
.end method
