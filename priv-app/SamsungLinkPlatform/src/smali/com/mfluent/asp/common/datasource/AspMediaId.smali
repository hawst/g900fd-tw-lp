.class public Lcom/mfluent/asp/common/datasource/AspMediaId;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final MEDIA_TYPE_ALBUM:I = 0xc

.field public static final MEDIA_TYPE_AUDIO:I = 0x2

.field public static final MEDIA_TYPE_CAPTION:I = 0xd

.field public static final MEDIA_TYPE_CAPTION_INDEX:I = 0xe

.field public static final MEDIA_TYPE_DOCUMENT:I = 0xf

.field public static final MEDIA_TYPE_IMAGE:I = 0x1

.field public static final MEDIA_TYPE_NONE:I = 0x0

.field public static final MEDIA_TYPE_VIDEO:I = 0x3


# instance fields
.field private mId:I

.field private mMediaType:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static final mediaTypeDesc(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    packed-switch p0, :pswitch_data_0

    .line 86
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 72
    :pswitch_1
    const-string v0, "Audio"

    goto :goto_0

    .line 74
    :pswitch_2
    const-string v0, "Image"

    goto :goto_0

    .line 76
    :pswitch_3
    const-string v0, "Video"

    goto :goto_0

    .line 78
    :pswitch_4
    const-string v0, "Album"

    goto :goto_0

    .line 80
    :pswitch_5
    const-string v0, "Caption"

    goto :goto_0

    .line 82
    :pswitch_6
    const-string v0, "CaptionIndex"

    goto :goto_0

    .line 84
    :pswitch_7
    const-string v0, "Document"

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 42
    if-nez p1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v0

    .line 46
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 47
    goto :goto_0

    .line 48
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/mfluent/asp/common/datasource/AspMediaId;

    if-ne v2, v3, :cond_0

    .line 49
    check-cast p1, Lcom/mfluent/asp/common/datasource/AspMediaId;

    .line 50
    iget v2, p1, Lcom/mfluent/asp/common/datasource/AspMediaId;->mMediaType:I

    iget v3, p0, Lcom/mfluent/asp/common/datasource/AspMediaId;->mMediaType:I

    if-ne v2, v3, :cond_0

    iget v2, p1, Lcom/mfluent/asp/common/datasource/AspMediaId;->mId:I

    iget v3, p0, Lcom/mfluent/asp/common/datasource/AspMediaId;->mId:I

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final getId()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/mfluent/asp/common/datasource/AspMediaId;->mId:I

    return v0
.end method

.method public final getMediaType()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/mfluent/asp/common/datasource/AspMediaId;->mMediaType:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 58
    iget v0, p0, Lcom/mfluent/asp/common/datasource/AspMediaId;->mId:I

    iget v1, p0, Lcom/mfluent/asp/common/datasource/AspMediaId;->mMediaType:I

    shl-int/lit8 v1, v1, 0x1c

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AspMediaId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/mfluent/asp/common/datasource/AspMediaId;->mMediaType:I

    invoke-static {v1}, Lcom/mfluent/asp/common/datasource/AspMediaId;->mediaTypeDesc(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mfluent/asp/common/datasource/AspMediaId;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
