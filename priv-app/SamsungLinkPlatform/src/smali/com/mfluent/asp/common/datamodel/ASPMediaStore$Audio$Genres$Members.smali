.class public final Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres$Members;
.super Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Genres$Members;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$AudioColumns;
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Members"
.end annotation


# static fields
.field public static final SYNC_PATH:Ljava/lang/String; = "genre_members_sync"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 909
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Genres$Members;-><init>()V

    return-void
.end method

.method public static getGeneralGroupingUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 914
    const-string v0, "genre_members"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$4100(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getGeneralGroupingUriBestDevice(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 918
    const-string v0, "genre_members"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUriBestDevice(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$4200(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getSyncingUriForDevice(I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 922
    int-to-long v0, p0

    const-string v2, "genre_members_sync"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$4300(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static setAudioGenresBySourceMediaId(Landroid/content/ContentResolver;Ljava/util/ArrayList;[Ljava/lang/String;ILjava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;[",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v10, 0x2

    const/4 v6, 0x0

    .line 946
    invoke-static {p2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 948
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres$Members;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "name"

    aput-object v0, v2, v6

    const-string v0, "genre_id"

    aput-object v0, v2, v7

    const-string v0, "audio_id"

    aput-object v0, v2, v10

    const/4 v0, 0x3

    const-string v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "genre_map_id"

    aput-object v3, v2, v0

    const-string v3, "source_media_id=? AND device_id=?"

    new-array v4, v10, [Ljava/lang/String;

    aput-object p4, v4, v6

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    const-string v5, "name"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 955
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    .line 956
    const/4 v0, 0x0

    move v2, v1

    move-object v1, v0

    move v0, v6

    .line 958
    :goto_0
    array-length v3, p2

    if-lt v6, v3, :cond_0

    if-eqz v2, :cond_9

    .line 959
    :cond_0
    if-nez v2, :cond_2

    .line 960
    const/4 v3, -0x1

    .line 972
    :goto_1
    if-gez v3, :cond_6

    .line 981
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 982
    const-string v5, "name"

    aget-object v8, p2, v6

    invoke-virtual {v3, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    const-string v5, "device_id"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 984
    if-nez v0, :cond_5

    .line 985
    const-string v5, "source_media_id"

    invoke-virtual {v3, v5, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    :goto_2
    # getter for: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    invoke-static {}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$4600()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v5

    if-gt v5, v10, :cond_1

    .line 991
    # getter for: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$4700()Ljava/lang/String;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "::setAudioGenresBySourceMediaId: Inserting audio genre map:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 993
    :cond_1
    sget-object v5, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres$Members;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    .line 994
    invoke-virtual {v5, v3}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 995
    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 997
    add-int/lit8 v6, v6, 0x1

    .line 998
    goto :goto_0

    .line 961
    :cond_2
    array-length v1, p2

    if-lt v6, v1, :cond_3

    .line 963
    const-string v1, "name"

    invoke-interface {v4, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v4, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move v3, v7

    goto :goto_1

    .line 965
    :cond_3
    if-nez v0, :cond_4

    .line 966
    const-string v0, "audio_id"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 968
    :cond_4
    const-string v1, "name"

    invoke-interface {v4, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v4, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 969
    aget-object v3, p2, v6

    invoke-virtual {v3, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 987
    :cond_5
    const-string v5, "audio_id"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2

    .line 998
    :cond_6
    if-nez v3, :cond_7

    .line 1002
    add-int/lit8 v6, v6, 0x1

    .line 1003
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    goto/16 :goto_0

    .line 1005
    :cond_7
    # getter for: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    invoke-static {}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$4600()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v10, :cond_8

    .line 1006
    # getter for: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$4700()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "::setAudioGenresBySourceMediaId: Removing audio genre map:"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", source_media_id:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    :cond_8
    const-string v2, "genre_map_id"

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-string v5, "genre_members"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {v2, v3, v5}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$4800(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 1016
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1018
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    goto/16 :goto_0

    .line 1021
    :cond_9
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1022
    return-void
.end method


# virtual methods
.method public final instanceGetContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 932
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres$Members;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public final instanceGetContentUriForDevice(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 937
    const-string v0, "genre_members"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$4500(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final instanceGetEntryUri(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 927
    const-string v0, "genre_members"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$4400(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
