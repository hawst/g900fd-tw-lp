.class public final Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Artists;
.super Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Artists;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$ArtistColumns;
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Artists"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 844
    invoke-direct {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Artists;-><init>()V

    return-void
.end method

.method public static getOrphanCleanUriForDevice(I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 847
    int-to-long v0, p0

    const-string v2, "artist"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildOrphanCleanupUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$3300(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final instanceGetContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 857
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Artists;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public final instanceGetContentUriForDevice(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 862
    const-string v0, "artist"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$3700(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final instanceGetEntryUri(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 852
    const-string v0, "artist"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$3600(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
