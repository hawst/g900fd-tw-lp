.class public Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;
.super Lorg/apache/http/entity/FileEntity;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/io/util/StreamProgressListener;


# instance fields
.field private outstream:Ljava/io/OutputStream;

.field private final streamProgressListener:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

.field private totalBytesTransferred:J


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Lcom/mfluent/asp/common/io/util/StreamProgressListener;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lorg/apache/http/entity/FileEntity;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 28
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->totalBytesTransferred:J

    .line 32
    iput-object p3, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->streamProgressListener:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    .line 33
    return-void
.end method


# virtual methods
.method public bytesTransferred(J)V
    .locals 3

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->totalBytesTransferred:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->totalBytesTransferred:J

    .line 51
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->streamProgressListener:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->streamProgressListener:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    invoke-interface {v0, p1, p2}, Lcom/mfluent/asp/common/io/util/StreamProgressListener;->bytesTransferred(J)V

    .line 54
    :cond_0
    return-void
.end method

.method public getTotalBytesTransferred()J
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->totalBytesTransferred:J

    return-wide v0
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 37
    iget-wide v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->totalBytesTransferred:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 38
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->streamProgressListener:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->streamProgressListener:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    iget-wide v2, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->totalBytesTransferred:J

    neg-long v2, v2

    invoke-interface {v0, v2, v3}, Lcom/mfluent/asp/common/io/util/StreamProgressListener;->bytesTransferred(J)V

    .line 41
    :cond_0
    iput-wide v4, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->totalBytesTransferred:J

    .line 43
    :cond_1
    new-instance v0, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;

    new-instance v1, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;

    invoke-direct {v1, p1, p0}, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;-><init>(Ljava/io/OutputStream;Lcom/mfluent/asp/common/io/util/StreamProgressListener;)V

    invoke-direct {v0, v1}, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->outstream:Ljava/io/OutputStream;

    .line 45
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;->outstream:Ljava/io/OutputStream;

    invoke-super {p0, v0}, Lorg/apache/http/entity/FileEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 46
    return-void
.end method
