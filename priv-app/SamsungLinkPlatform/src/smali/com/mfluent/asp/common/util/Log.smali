.class public Lcom/mfluent/asp/common/util/Log;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final ASSERT:I = 0x7

.field public static final DEBUG:I = 0x3

.field public static final ERROR:I = 0x6

.field public static final INFO:I = 0x4

.field private static IS_SHIP:Z = false

.field private static LOGLEVEL:I = 0x0

.field public static final SL_DEBUG_PATH:Ljava/lang/String; = "/mnt/sdcard/SL_DEBUG"

.field public static final VERBOSE:I = 0x2

.field public static final WARN:I = 0x5


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 22
    sput-boolean v0, Lcom/mfluent/asp/common/util/Log;->IS_SHIP:Z

    .line 23
    const/4 v1, 0x2

    sput v1, Lcom/mfluent/asp/common/util/Log;->LOGLEVEL:I

    .line 26
    invoke-static {}, Lcom/mfluent/asp/common/util/Log;->isLoggable()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    sput-boolean v0, Lcom/mfluent/asp/common/util/Log;->IS_SHIP:Z

    .line 27
    const-string v0, "SL_DEBUG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IS_SHIP : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/mfluent/asp/common/util/Log;->IS_SHIP:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x3

    invoke-static {p0, p1, v0}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;I)V

    .line 104
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x3

    invoke-static {p0, p1, p2, v0}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 108
    return-void
.end method

.method public static varargs debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 146
    invoke-static {p1, p2}, Lcom/mfluent/asp/common/util/Log;->getString(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 147
    const/4 v1, 0x3

    invoke-static {p0, v0, v1}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;I)V

    .line 148
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x6

    invoke-static {p0, p1, v0}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;I)V

    .line 134
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x6

    invoke-static {p0, p1, p2, v0}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 138
    return-void
.end method

.method public static varargs error(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 156
    invoke-static {p1, p2}, Lcom/mfluent/asp/common/util/Log;->getString(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 157
    const/4 v1, 0x6

    invoke-static {p0, v0, v1}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;I)V

    .line 158
    return-void
.end method

.method private static varargs getString(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 166
    :try_start_0
    array-length v2, p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    move v1, v0

    move-object v0, p0

    :goto_0
    if-ge v1, v2, :cond_5

    :try_start_1
    aget-object v3, p1, v1

    .line 170
    instance-of v4, v3, Ljava/lang/Integer;

    if-nez v4, :cond_0

    instance-of v4, v3, Ljava/lang/Long;

    if-eqz v4, :cond_2

    .line 171
    :cond_0
    const-string v3, "\\{\\}"

    const-string v4, "%d"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 169
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 172
    :cond_2
    instance-of v4, v3, Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 173
    const-string v3, "\\{\\}"

    const-string v4, "%s"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 174
    :cond_3
    instance-of v4, v3, Ljava/lang/Float;

    if-nez v4, :cond_4

    instance-of v3, v3, Ljava/lang/Double;

    if-eqz v3, :cond_1

    .line 175
    :cond_4
    const-string v3, "\\{\\}"

    const-string v4, "%f"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 178
    :cond_5
    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 183
    :goto_2
    return-object v0

    .line 179
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, p0

    .line 180
    :goto_3
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Log::getString::Exception : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 179
    :catch_1
    move-exception v1

    goto :goto_3
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;I)V

    .line 112
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x4

    invoke-static {p0, p1, p2, v0}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 116
    return-void
.end method

.method public static varargs info(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 141
    invoke-static {p1, p2}, Lcom/mfluent/asp/common/util/Log;->getString(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 142
    const/4 v1, 0x4

    invoke-static {p0, v0, v1}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;I)V

    .line 143
    return-void
.end method

.method public static isLoggable()Z
    .locals 4

    .prologue
    .line 32
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/sdcard/SL_DEBUG"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 33
    const-string v1, "SL_DEBUG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isLoggable:: SL_DEBUG_EXIST = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    const/4 v0, 0x1

    .line 41
    :goto_0
    return v0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    const-string v1, "SL_DEBUG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isLoggable:: Exception occurred : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static log(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 45
    sget-boolean v0, Lcom/mfluent/asp/common/util/Log;->IS_SHIP:Z

    if-eqz v0, :cond_0

    .line 67
    :goto_0
    return-void

    .line 48
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 57
    :pswitch_0
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 50
    :pswitch_1
    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 53
    :pswitch_2
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 60
    :pswitch_3
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 63
    :pswitch_4
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V
    .locals 1

    .prologue
    .line 70
    sget-boolean v0, Lcom/mfluent/asp/common/util/Log;->IS_SHIP:Z

    if-eqz v0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 73
    :cond_0
    packed-switch p3, :pswitch_data_0

    .line 82
    :pswitch_0
    invoke-static {p0, p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 75
    :pswitch_1
    invoke-static {p0, p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 78
    :pswitch_2
    invoke-static {p0, p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 85
    :pswitch_3
    invoke-static {p0, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 88
    :pswitch_4
    invoke-static {p0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static varargs trace(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 161
    invoke-static {p1, p2}, Lcom/mfluent/asp/common/util/Log;->getString(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 162
    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;I)V

    .line 163
    return-void
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x2

    invoke-static {p0, p1, v0}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;I)V

    .line 96
    return-void
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x2

    invoke-static {p0, p1, p2, v0}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 100
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x5

    invoke-static {p0, p1, v0}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;I)V

    .line 120
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x5

    invoke-static {p0, p1, p2, v0}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 124
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 127
    sget-boolean v0, Lcom/mfluent/asp/common/util/Log;->IS_SHIP:Z

    if-nez v0, :cond_0

    .line 128
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 130
    :cond_0
    return-void
.end method

.method public static varargs warn(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 151
    invoke-static {p1, p2}, Lcom/mfluent/asp/common/util/Log;->getString(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 152
    const/4 v1, 0x5

    invoke-static {p0, v0, v1}, Lcom/mfluent/asp/common/util/Log;->log(Ljava/lang/String;Ljava/lang/String;I)V

    .line 153
    return-void
.end method
