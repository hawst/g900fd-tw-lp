.class public interface abstract Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DateTakenGroupingColumns;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DateTakenGroupingColumns"
.end annotation


# static fields
.field public static final DATE_CODE:Ljava/lang/String; = "date_code"

.field public static final DATE_CODE_FORMAT_DIMINISH_BEFORE_TIMESTAMP:Ljava/lang/String; = "CASE WHEN datetaken IS NULL THEN \'\' WHEN datetaken<=0 THEN \'\' WHEN datetaken<%d THEN strftime(\'%%Y-%%m-00\',datetaken/1000, \'unixepoch\', \'localtime\') ELSE strftime(\'%%Y-%%m-%%d\',datetaken/1000,\'unixepoch\',\'localtime\') END AS date_code"
