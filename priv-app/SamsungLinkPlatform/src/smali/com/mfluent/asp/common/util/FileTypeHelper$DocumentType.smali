.class public final enum Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/util/FileTypeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DocumentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

.field public static final enum HWP:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

.field public static final enum MS_EXCEL:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

.field public static final enum MS_POWERPOINT:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

.field public static final enum MS_WORD:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

.field public static final enum PDF:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 94
    new-instance v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    const-string v1, "MS_WORD"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_WORD:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    .line 95
    new-instance v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    const-string v1, "HWP"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->HWP:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    .line 96
    new-instance v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    const-string v1, "PDF"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->PDF:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    .line 97
    new-instance v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    const-string v1, "MS_POWERPOINT"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_POWERPOINT:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    .line 98
    new-instance v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    const-string v1, "MS_EXCEL"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_EXCEL:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    .line 93
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    sget-object v1, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_WORD:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->HWP:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->PDF:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_POWERPOINT:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_EXCEL:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->$VALUES:[Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;
    .locals 1

    .prologue
    .line 93
    const-class v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->$VALUES:[Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-virtual {v0}, [Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    return-object v0
.end method
