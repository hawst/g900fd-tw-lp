.class public Lcom/mfluent/asp/common/datamodel/ASPFileException;
.super Ljava/io/IOException;
.source "SourceFile"


# static fields
.field public static final ERROR_NO_ACCESS:I = 0x1

.field public static final ERROR_RESTRICTED:I = 0x2

.field private static final serialVersionUID:J = 0x6c8073646525f0acL


# instance fields
.field private final mErrorCode:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 25
    iput p2, p0, Lcom/mfluent/asp/common/datamodel/ASPFileException;->mErrorCode:I

    .line 26
    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/mfluent/asp/common/datamodel/ASPFileException;->mErrorCode:I

    return v0
.end method
