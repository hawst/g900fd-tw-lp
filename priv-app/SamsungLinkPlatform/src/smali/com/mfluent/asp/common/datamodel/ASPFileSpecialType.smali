.class public final enum Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

.field public static final enum EXTERNAL_STORAGE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

.field public static final enum HOMESYNC_PERSONAL:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

.field public static final enum HOMESYNC_PRIVATE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

.field public static final enum HOMESYNC_SHARED:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

.field public static final enum INTERNAL_STORAGE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    const-string v1, "INTERNAL_STORAGE"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->INTERNAL_STORAGE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    .line 6
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    const-string v1, "EXTERNAL_STORAGE"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->EXTERNAL_STORAGE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    .line 7
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    const-string v1, "HOMESYNC_PERSONAL"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_PERSONAL:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    .line 8
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    const-string v1, "HOMESYNC_SHARED"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_SHARED:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    .line 9
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    const-string v1, "HOMESYNC_PRIVATE"

    invoke-direct {v0, v1, v6}, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_PRIVATE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    .line 4
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->INTERNAL_STORAGE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->EXTERNAL_STORAGE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_PERSONAL:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_SHARED:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->HOMESYNC_PRIVATE:Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->$VALUES:[Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->$VALUES:[Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    invoke-virtual {v0}, [Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/common/datamodel/ASPFileSpecialType;

    return-object v0
.end method
