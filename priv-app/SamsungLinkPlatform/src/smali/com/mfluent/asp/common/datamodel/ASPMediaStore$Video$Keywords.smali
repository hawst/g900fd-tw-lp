.class public final Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Keywords;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$KeywordColumns;
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$VideoColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Keywords"
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String;

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final ENTRY_CONTENT_TYPE:Ljava/lang/String;

.field public static final PATH:Ljava/lang/String; = "video_keywords"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 719
    const-string v0, "video_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$2300(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Keywords;->CONTENT_TYPE:Ljava/lang/String;

    .line 721
    const-string v0, "video_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$2400(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Keywords;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    .line 723
    const-string v0, "video_keywords"

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Keywords;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 715
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContentUriForDevice(I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 726
    int-to-long v0, p0

    const-string v2, "video_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$2500(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getEntryUri(I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 730
    int-to-long v0, p0

    const-string v2, "video_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$2600(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getGeneralGroupingUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 738
    const-string v0, "video_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGeneralGroupingUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, p0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$2700(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getGroupByFileIdUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 734
    const-string v0, "video_keywords"

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildGroupByFileIdUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final instanceGetContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 748
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Keywords;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public final instanceGetContentUriForDevice(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 753
    const-string v0, "video_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildDeviceContentUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$2900(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final instanceGetEntryUri(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 743
    const-string v0, "video_keywords"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryIdUri(JLjava/lang/String;)Landroid/net/Uri;
    invoke-static {p1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$2800(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
