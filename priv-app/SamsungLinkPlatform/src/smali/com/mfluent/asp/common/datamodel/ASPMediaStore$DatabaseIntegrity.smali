.class public final Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrityColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/datamodel/ASPMediaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DatabaseIntegrity"
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String;

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final ENTRY_CONTENT_TYPE:Ljava/lang/String;

.field public static final PATH:Ljava/lang/String; = "db_integrity"

.field public static final VALUE_AUDIO_JOURNAL_DROPPED:Ljava/lang/String; = "audio_journal_dropped"

.field public static final VALUE_AUDIO_JOURNAL_SYNCED:Ljava/lang/String; = "audio_journal_synced"

.field public static final VALUE_DOCUMENT_JOURNAL_DROPPED:Ljava/lang/String; = "document_journal_dropped"

.field public static final VALUE_DOCUMENT_JOURNAL_SYNCED:Ljava/lang/String; = "document_journal_synced"

.field public static final VALUE_FILES_TABLE_VALIDITY:Ljava/lang/String; = "files_table_validity"

.field public static final VALUE_IMAGE_JOURNAL_DROPPED:Ljava/lang/String; = "image_journal_dropped"

.field public static final VALUE_IMAGE_JOURNAL_SYNCED:Ljava/lang/String; = "image_journal_synced"

.field public static final VALUE_VIDEO_JOURNAL_DROPPED:Ljava/lang/String; = "video_journal_dropped"

.field public static final VALUE_VIDEO_JOURNAL_SYNCED:Ljava/lang/String; = "video_journal_synced"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 450
    const-string v0, "db_integrity"

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->CONTENT_URI:Landroid/net/Uri;

    .line 452
    const-string v0, "db_integrity"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$200(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->CONTENT_TYPE:Ljava/lang/String;

    .line 454
    const-string v0, "db_integrity"

    # invokes: Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->buildEntryContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore;->access$300(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->ENTRY_CONTENT_TYPE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getValue(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 480
    .line 481
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "value"

    aput-object v0, v2, v6

    const-string v3, "name=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 483
    if-eqz v1, :cond_1

    .line 485
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 489
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 493
    :cond_1
    return-object v5

    .line 489
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static setValue(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 468
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 470
    const-string v1, "value"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "name=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 473
    if-nez v1, :cond_0

    .line 474
    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$DatabaseIntegrity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 477
    :cond_0
    return-void
.end method
