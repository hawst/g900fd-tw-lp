.class public Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;
.super Lorg/apache/commons/io/input/ProxyInputStream;
.source "SourceFile"


# static fields
.field private static final DEFAULT_CONSUME_CONTENT_BEFORE_SHUTDOWN:Z = true

.field private static final DEFAULT_REQUEST:Lorg/apache/http/client/methods/HttpRequestBase;

.field public static LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final closeBeforeShutdown:Z

.field private final mClient:Lorg/apache/http/client/HttpClient;

.field private request:Lorg/apache/http/client/methods/HttpRequestBase;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mfl_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->TAG:Ljava/lang/String;

    .line 25
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_HTTPINPUTSTREAM:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->DEFAULT_REQUEST:Lorg/apache/http/client/methods/HttpRequestBase;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lorg/apache/http/client/HttpClient;)V
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->DEFAULT_REQUEST:Lorg/apache/http/client/methods/HttpRequestBase;

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;-><init>(Ljava/io/InputStream;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;Z)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;-><init>(Ljava/io/InputStream;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;Z)V

    .line 61
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;Z)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lorg/apache/commons/io/input/ProxyInputStream;-><init>(Ljava/io/InputStream;)V

    .line 69
    iput-object p2, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->mClient:Lorg/apache/http/client/HttpClient;

    .line 70
    iput-object p3, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 71
    iput-boolean p4, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->closeBeforeShutdown:Z

    .line 72
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lorg/apache/http/client/HttpClient;Z)V
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->DEFAULT_REQUEST:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;-><init>(Ljava/io/InputStream;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;Z)V

    .line 65
    return-void
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->beforeRead(I)V

    .line 138
    invoke-super {p0}, Lorg/apache/commons/io/input/ProxyInputStream;->available()I

    move-result v0

    return v0
.end method

.method public beforeRead(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    if-eqz v0, :cond_0

    .line 171
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->request:Lorg/apache/http/client/methods/HttpRequestBase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :cond_0
    :goto_0
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 179
    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 76
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v2, :cond_0

    .line 77
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->TAG:Ljava/lang/String;

    const-string v1, "close()ing inputStream"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 80
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v2, :cond_1

    .line 81
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->TAG:Ljava/lang/String;

    const-string v1, "thread was interrupted"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    if-eqz v0, :cond_3

    .line 85
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v2, :cond_2

    .line 86
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->TAG:Ljava/lang/String;

    const-string v1, "aborting request"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->request:Lorg/apache/http/client/methods/HttpRequestBase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 100
    :cond_3
    :goto_0
    iget-boolean v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->closeBeforeShutdown:Z

    if-eqz v0, :cond_5

    .line 101
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v2, :cond_4

    .line 102
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->TAG:Ljava/lang/String;

    const-string v1, "super.close()"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_4
    invoke-super {p0}, Lorg/apache/commons/io/input/ProxyInputStream;->close()V

    .line 107
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->mClient:Lorg/apache/http/client/HttpClient;

    if-eqz v0, :cond_7

    .line 108
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v2, :cond_6

    .line 109
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->TAG:Ljava/lang/String;

    const-string v1, "shutting down connection manager"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_6
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->mClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 114
    :cond_7
    iget-boolean v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->closeBeforeShutdown:Z

    if-nez v0, :cond_b

    .line 116
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    if-eqz v0, :cond_9

    .line 117
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v2, :cond_8

    .line 118
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->TAG:Ljava/lang/String;

    const-string v1, "aborting request"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_8
    iget-object v0, p0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->request:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    .line 122
    :cond_9
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v2, :cond_a

    .line 123
    sget-object v0, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->TAG:Ljava/lang/String;

    const-string v1, "super.close()"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_a
    invoke-super {p0}, Lorg/apache/commons/io/input/ProxyInputStream;->close()V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0

    .line 133
    :cond_b
    :goto_1
    return-void

    .line 126
    :catch_0
    move-exception v0

    .line 127
    sget-object v1, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->LOG_LEVEL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v2, :cond_b

    .line 128
    sget-object v1, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->TAG:Ljava/lang/String;

    const-string v2, "exception in request.abort() or super.close()"

    invoke-static {v1, v2, v0}, Lcom/mfluent/asp/common/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public declared-synchronized mark(I)V
    .locals 1

    .prologue
    .line 143
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lorg/apache/commons/io/input/ProxyInputStream;->mark(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    monitor-exit p0

    return-void

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public markSupported()Z
    .locals 1

    .prologue
    .line 148
    invoke-super {p0}, Lorg/apache/commons/io/input/ProxyInputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->beforeRead(I)V

    .line 154
    invoke-super {p0}, Lorg/apache/commons/io/input/ProxyInputStream;->read()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    monitor-exit p0

    return-void

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public skip(J)J
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;->beforeRead(I)V

    .line 160
    invoke-super {p0, p1, p2}, Lorg/apache/commons/io/input/ProxyInputStream;->skip(J)J

    move-result-wide v0

    return-wide v0
.end method
