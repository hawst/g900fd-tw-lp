.class Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;
.super Landroid/database/CursorWrapper;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/common/content/BaseContentAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SelectionCursor"
.end annotation


# instance fields
.field private position:I

.field private final selectedRows:[I


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/common/content/ContentAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/common/content/ContentAdapter",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 742
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 739
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->position:I

    .line 744
    invoke-interface {p1}, Lcom/mfluent/asp/common/content/ContentAdapter;->getAllSelectedRows()[I

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->selectedRows:[I

    .line 745
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 819
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->selectedRows:[I

    array-length v0, v0

    return v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 749
    iget v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->position:I

    return v0
.end method

.method public final isAfterLast()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 811
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 814
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->position:I

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->getCount()I

    move-result v2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isBeforeFirst()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 803
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 806
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->position:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isFirst()Z
    .locals 1

    .prologue
    .line 792
    iget v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->position:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLast()Z
    .locals 3

    .prologue
    .line 797
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->getCount()I

    move-result v0

    .line 798
    iget v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->position:I

    add-int/lit8 v2, v0, -0x1

    if-ne v1, v2, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public move(I)Z
    .locals 1

    .prologue
    .line 787
    iget v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->position:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToFirst()Z
    .locals 1

    .prologue
    .line 767
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToLast()Z
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->selectedRows:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 1

    .prologue
    .line 777
    iget v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->position:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 1

    .prologue
    .line 754
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->selectedRows:[I

    array-length v0, v0

    if-lt p1, v0, :cond_2

    .line 755
    :cond_0
    const/4 v0, 0x0

    .line 762
    :cond_1
    :goto_0
    return v0

    .line 758
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->selectedRows:[I

    aget v0, v0, p1

    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->moveToPosition(I)Z

    move-result v0

    .line 759
    if-eqz v0, :cond_1

    .line 760
    iput p1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->position:I

    goto :goto_0
.end method

.method public moveToPrevious()Z
    .locals 1

    .prologue
    .line 782
    iget v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->position:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method
