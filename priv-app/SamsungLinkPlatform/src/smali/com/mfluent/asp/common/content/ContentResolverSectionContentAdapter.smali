.class public abstract Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;
.super Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/mfluent/asp/common/content/ContentId;",
        ">",
        "Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected mContentResolver:Landroid/content/ContentResolver;

.field private mProjection:[Ljava/lang/String;

.field private mSelection:Ljava/lang/String;

.field private mSelectionArgs:[Ljava/lang/String;

.field private mSortOrder:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;-><init>(Landroid/os/Parcel;)V

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 31
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mUri:Landroid/net/Uri;

    .line 35
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mProjection:[Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelection:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSortOrder:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public getProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mProjection:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mProjection:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 87
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelection:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectionArgs()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 117
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSortOrder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSortOrder:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public initContext(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->initContext(Landroid/content/Context;)V

    .line 44
    if-eqz p1, :cond_0

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    .line 47
    :cond_0
    return-void
.end method

.method protected loadCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 150
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mProjection:[Ljava/lang/String;

    iget-object v3, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelection:Ljava/lang/String;

    iget-object v4, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    iget-object v5, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSortOrder:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public setProjection([Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mProjection:[Ljava/lang/String;

    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    iput-object p1, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mProjection:[Ljava/lang/String;

    .line 76
    const/4 v0, 0x1

    .line 79
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setQueryParams(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->setUri(Landroid/net/Uri;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 52
    invoke-virtual {p0, p2}, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->setProjection([Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 53
    invoke-virtual {p0, p3}, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->setSelection(Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 54
    invoke-virtual {p0, p4}, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->setSelectionArgs([Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 55
    invoke-virtual {p0, p5}, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->setSortOrder(Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 57
    return v0
.end method

.method public setSelection(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelection:Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iput-object p1, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelection:Ljava/lang/String;

    .line 93
    const/4 v0, 0x1

    .line 96
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSelectionArgs([Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    iput-object p1, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    .line 106
    const/4 v0, 0x1

    .line 109
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSortOrder(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSortOrder:Ljava/lang/String;

    invoke-static {p1, v0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    iput-object p1, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSortOrder:Ljava/lang/String;

    .line 123
    const/4 v0, 0x1

    .line 126
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setUri(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 61
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mUri:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 62
    :cond_2
    iput-object p1, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mUri:Landroid/net/Uri;

    .line 63
    const/4 v0, 0x1

    .line 66
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 135
    invoke-super {p0, p1, p2}, Lcom/mfluent/asp/common/content/BaseCursorWrappingSectionContentAdapter;->writeToParcel(Landroid/os/Parcel;I)V

    .line 137
    const/4 v0, 0x0

    .line 138
    iget-object v1, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 139
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 141
    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mProjection:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelection:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSelectionArgs:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/mfluent/asp/common/content/ContentResolverSectionContentAdapter;->mSortOrder:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 146
    return-void
.end method
