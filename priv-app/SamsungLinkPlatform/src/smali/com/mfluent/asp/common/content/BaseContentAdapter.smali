.class abstract Lcom/mfluent/asp/common/content/BaseContentAdapter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/content/ContentAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;,
        Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;,
        Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;,
        Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/mfluent/asp/common/content/ContentId;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/mfluent/asp/common/content/ContentAdapter",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final PARCEL_VERSION:I = 0x1

.field private static final logger:Lorg/slf4j/Logger;

.field private static mExecutor:Ljava/util/concurrent/ScheduledExecutorService;


# instance fields
.field protected mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

.field private mIsDestroyed:Z

.field protected mIsEditing:Z

.field protected mIsInverseSelection:Z

.field protected mIsLoading:Z

.field private mLastChangeTime:J

.field private mLastDataLoadTime:J

.field private final mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadDataStackTrace:Ljava/lang/String;

.field private mLoadToken:I

.field private mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

.field protected final mMultiSelections:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mfluent/asp/common/content/SectionContentAdapter",
            "<*>;"
        }
    .end annotation
.end field

.field protected mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected mSingleSelectedIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    sput-object v0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 57
    const-class v0, Lcom/mfluent/asp/common/content/BaseContentAdapter;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->logger:Lorg/slf4j/Logger;

    return-void
.end method

.method protected constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    .line 62
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastChangeTime:J

    .line 63
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastDataLoadTime:J

    .line 65
    iput-object v3, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    .line 66
    iput-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsDestroyed:Z

    .line 67
    iput-object v3, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadDataStackTrace:Ljava/lang/String;

    .line 68
    iput v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadToken:I

    .line 72
    iput-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    .line 73
    iput-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    .line 75
    iput-object v3, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    .line 76
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    .line 77
    iput-object v3, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    .line 78
    iput-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsLoading:Z

    .line 82
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/BadParcelableException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    const/4 v3, 0x3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    .line 62
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastChangeTime:J

    .line 63
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastDataLoadTime:J

    .line 65
    iput-object v6, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    .line 66
    iput-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsDestroyed:Z

    .line 67
    iput-object v6, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadDataStackTrace:Ljava/lang/String;

    .line 68
    iput v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadToken:I

    .line 72
    iput-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    .line 73
    iput-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    .line 75
    iput-object v6, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    .line 76
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    .line 77
    iput-object v6, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    .line 78
    iput-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsLoading:Z

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 86
    if-lez v0, :cond_0

    if-le v0, v1, :cond_1

    .line 87
    :cond_0
    new-instance v1, Landroid/os/BadParcelableException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid Parcel version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    .line 92
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/ContentId;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    .line 93
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    move v3, v2

    .line 94
    :goto_2
    if-ge v3, v4, :cond_4

    .line 95
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/ContentId;

    .line 96
    iget-object v5, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 94
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_2
    move v0, v2

    .line 90
    goto :goto_0

    :cond_3
    move v0, v2

    .line 91
    goto :goto_1

    .line 98
    :cond_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_6

    .line 99
    :goto_3
    if-eqz v1, :cond_5

    .line 100
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/SectionContentAdapter;

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    .line 102
    :cond_5
    return-void

    :cond_6
    move v1, v2

    .line 98
    goto :goto_3
.end method

.method static synthetic access$000(Lcom/mfluent/asp/common/content/BaseContentAdapter;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->notifyListenerDataSetInvalidated()V

    return-void
.end method

.method static synthetic access$100(Lcom/mfluent/asp/common/content/BaseContentAdapter;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->notifyListenersDataLoaded()V

    return-void
.end method

.method protected static declared-synchronized getDefaultExecutor()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 2

    .prologue
    .line 210
    const-class v1, Lcom/mfluent/asp/common/content/BaseContentAdapter;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    :cond_0
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 215
    :cond_1
    sget-object v0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private notifyListenerDataSetInvalidated()V
    .locals 2

    .prologue
    .line 402
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 403
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should only call notifyListenersDataSetInvalidated from main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 408
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 409
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;

    iget-object v0, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mListener:Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;

    invoke-interface {v0, p0}, Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;->onDataSetInvalidated(Lcom/mfluent/asp/common/content/ContentAdapter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 412
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 413
    return-void

    .line 412
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private notifyListenersDataLoaded()V
    .locals 2

    .prologue
    .line 334
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 335
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should only call notifyListenersDataLoaded from main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 340
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 341
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;

    iget-object v0, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mListener:Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;

    invoke-interface {v0, p0}, Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;->onDataLoaded(Lcom/mfluent/asp/common/content/ContentAdapter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 340
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 344
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 345
    return-void

    .line 344
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private recordLoadingStackTrace()V
    .locals 3

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsDestroyed:Z

    if-nez v0, :cond_0

    .line 161
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    .line 162
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 163
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 166
    :try_start_0
    invoke-virtual {v0, v2}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 167
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadDataStackTrace:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    .line 174
    :cond_0
    return-void

    .line 169
    :catchall_0
    move-exception v0

    .line 170
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    throw v0
.end method


# virtual methods
.method protected cancelLoad(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 842
    instance-of v0, p1, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;

    if-eqz v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    if-eqz v0, :cond_0

    .line 844
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    check-cast p1, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;

    iget-object v1, p1, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;->mSectionLoadContext:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->cancelLoad(Ljava/lang/Object;)V

    .line 847
    :cond_0
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    invoke-interface {v0}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 655
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    invoke-interface {v0}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->close()V

    .line 657
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    .line 658
    return-void
.end method

.method protected abstract copyId(Lcom/mfluent/asp/common/content/ContentId;)Lcom/mfluent/asp/common/content/ContentId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation
.end method

.method public deregisterListener(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)V
    .locals 3

    .prologue
    .line 255
    if-nez p1, :cond_0

    .line 273
    :goto_0
    return-void

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 261
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 262
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;

    .line 263
    iget-object v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mListener:Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;

    if-ne v2, p1, :cond_2

    .line 264
    iget-object v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 265
    iget-boolean v1, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mScheduled:Z

    if-eqz v1, :cond_1

    .line 266
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    const v2, 0x7d166

    invoke-virtual {v1, v2, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->removeMessages(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 261
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 272
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public declared-synchronized destroy()V
    .locals 2

    .prologue
    .line 123
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsDestroyed:Z

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadDataStackTrace:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->clear()V

    .line 128
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->close()V

    .line 129
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    invoke-interface {v0}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    invoke-interface {v0}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->destroy()V

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 134
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 137
    monitor-exit p0

    return-void

    .line 136
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 123
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadDataStackTrace:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 143
    sget-object v0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->logger:Lorg/slf4j/Logger;

    const-string v1, "Did not call destroy on a loaded ContentAdapter:\n{}"

    iget-object v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadDataStackTrace:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 145
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 146
    return-void
.end method

.method protected finishDataLoad(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 856
    if-nez p1, :cond_1

    .line 881
    :cond_0
    :goto_0
    return-void

    .line 859
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastDataLoadTime:J

    .line 861
    instance-of v0, p1, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 862
    check-cast v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;

    .line 863
    iget-boolean v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    if-eqz v1, :cond_2

    .line 864
    check-cast p1, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;

    iget-object v1, p1, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    iput-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    .line 865
    iget-object v1, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;->mNotFoundMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/common/content/ContentId;

    .line 866
    iget-object v3, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 869
    :cond_2
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    iget-object v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    invoke-static {v1, v2}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 870
    iget v1, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;->mSingleSelectedIndex:I

    iput v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    .line 874
    :goto_2
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    if-eqz v1, :cond_0

    .line 875
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    iget-object v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;->mSectionLoadContext:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->hasLoadToFinish(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 876
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    iget-object v0, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;->mSectionLoadContext:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->finishLoad(Ljava/lang/Object;)V

    goto :goto_0

    .line 872
    :cond_3
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    invoke-virtual {p0, v1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getRowOfId(Lcom/mfluent/asp/common/content/ContentId;)I

    move-result v1

    iput v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    goto :goto_2
.end method

.method public getAllSelectedRows()[I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 517
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isEditing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 518
    iget v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    if-ltz v1, :cond_0

    .line 519
    const/4 v1, 0x1

    new-array v1, v1, [I

    .line 520
    iget v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    aput v2, v1, v0

    move-object v0, v1

    .line 553
    :goto_0
    return-object v0

    .line 523
    :cond_0
    new-array v0, v0, [I

    goto :goto_0

    .line 527
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getMultiSelectedRowIndexVector()Lcom/mfluent/asp/common/util/IntVector;

    .line 529
    iget-boolean v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    if-nez v1, :cond_2

    .line 530
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/IntVector;->toArray()[I

    move-result-object v0

    goto :goto_0

    .line 532
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getNumRowsMultiSelected()I

    move-result v1

    new-array v4, v1, [I

    move v1, v0

    move v2, v0

    .line 544
    :goto_1
    array-length v3, v4

    if-ge v2, v3, :cond_4

    .line 545
    iget-object v3, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    iget-object v3, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v3, v1}, Lcom/mfluent/asp/common/util/IntVector;->elementAt(I)I

    move-result v3

    if-ne v3, v0, :cond_3

    .line 546
    add-int/lit8 v1, v1, 0x1

    .line 550
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 548
    :cond_3
    add-int/lit8 v3, v2, 0x1

    aput v0, v4, v2

    move v2, v3

    goto :goto_2

    :cond_4
    move-object v0, v4

    .line 553
    goto :goto_0
.end method

.method protected getAndIncrementLoadToken()I
    .locals 1

    .prologue
    .line 446
    iget v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadToken:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadToken:I

    return v0
.end method

.method protected getExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 219
    invoke-static {}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getDefaultExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 721
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIdIndexes(Ljava/util/Set;)Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<TT;>;)",
            "Ljava/util/HashMap",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, -0x1

    .line 559
    new-instance v2, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 563
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 564
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 568
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->instantiateId()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v3

    move v0, v1

    .line 572
    :cond_0
    invoke-interface {v3, p0}, Lcom/mfluent/asp/common/content/ContentId;->setValues(Landroid/database/Cursor;)V

    .line 573
    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 574
    invoke-virtual {p0, v3}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->copyId(Lcom/mfluent/asp/common/content/ContentId;)Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 577
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 579
    :cond_2
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 580
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 581
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/ContentId;

    .line 582
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 583
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->copyId(Lcom/mfluent/asp/common/content/ContentId;)Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 588
    :cond_4
    const/4 v1, 0x1

    .line 592
    :cond_5
    if-eqz v1, :cond_6

    .line 593
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 595
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/ContentId;

    .line 596
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->copyId(Lcom/mfluent/asp/common/content/ContentId;)Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 600
    :cond_6
    return-object v2
.end method

.method protected abstract getIdOfCurrentRow()Lcom/mfluent/asp/common/content/ContentId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected getLoadToken()I
    .locals 1

    .prologue
    .line 442
    iget v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadToken:I

    return v0
.end method

.method protected getMultiSelectedRowIndexVector()Lcom/mfluent/asp/common/util/IntVector;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 491
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    if-nez v0, :cond_2

    .line 493
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v1

    .line 495
    new-instance v0, Lcom/mfluent/asp/common/util/IntVector;

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZZ)V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    .line 496
    if-lez v1, :cond_2

    .line 497
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 498
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->instantiateId()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v2

    .line 499
    const/4 v0, 0x0

    .line 502
    :cond_0
    invoke-interface {v2, p0}, Lcom/mfluent/asp/common/content/ContentId;->setValues(Landroid/database/Cursor;)V

    .line 503
    iget-object v3, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 504
    iget-object v3, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/common/util/IntVector;->append(I)V

    .line 506
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 507
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v3

    if-lt v3, v1, :cond_0

    .line 512
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    return-object v0
.end method

.method public getNumRowsMultiSelected()I
    .locals 2

    .prologue
    .line 605
    const/4 v0, 0x0

    .line 606
    iget-boolean v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    if-eqz v1, :cond_0

    .line 607
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    .line 608
    iget-boolean v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    if-eqz v1, :cond_0

    .line 609
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getCount()I

    move-result v1

    sub-int v0, v1, v0

    .line 613
    :cond_0
    return v0
.end method

.method public getSectionContentAdapter()Lcom/mfluent/asp/common/content/SectionContentAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/mfluent/asp/common/content/SectionContentAdapter",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    return-object v0
.end method

.method public getSelectionCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 732
    new-instance v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$SelectionCursor;-><init>(Lcom/mfluent/asp/common/content/ContentAdapter;)V

    return-object v0
.end method

.method public getSingleSelectedId()Lcom/mfluent/asp/common/content/ContentId;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 675
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    if-nez v0, :cond_0

    .line 676
    const/4 v0, 0x0

    .line 679
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->copyId(Lcom/mfluent/asp/common/content/ContentId;)Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    goto :goto_0
.end method

.method public getSingleSelectedRow()I
    .locals 1

    .prologue
    .line 684
    iget v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    return v0
.end method

.method public getWantsAllOnMoveCalls()Z
    .locals 1

    .prologue
    .line 715
    const/4 v0, 0x0

    return v0
.end method

.method protected hasDataLoadToFinish(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 824
    .line 826
    instance-of v0, p1, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;

    if-eqz v0, :cond_2

    .line 827
    check-cast p1, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;

    .line 828
    iget v0, p1, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;->mLoadToken:I

    iget v3, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLoadToken:I

    if-ne v0, v3, :cond_1

    move v0, v2

    .line 829
    :goto_0
    if-nez v0, :cond_0

    .line 830
    sget-object v3, Lcom/mfluent/asp/common/content/BaseContentAdapter;->logger:Lorg/slf4j/Logger;

    const-string v4, "::hasDataLoadToFinish Updated load token ({}) does not match current load token ({}), returning false. {}"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p1, Lcom/mfluent/asp/common/content/BaseContentAdapter$LoadContext;->mLoadToken:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getLoadToken()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v1, 0x2

    aput-object p0, v5, v1

    invoke-interface {v3, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 838
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 828
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public initContext(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsDestroyed:Z

    .line 184
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 185
    :cond_0
    new-instance v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;-><init>(Lcom/mfluent/asp/common/content/BaseContentAdapter;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    if-eqz v0, :cond_2

    .line 188
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    invoke-interface {v0, p1}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->initContext(Landroid/content/Context;)V

    .line 190
    :cond_2
    return-void
.end method

.method protected invalidateMultiSelectRows()V
    .locals 1

    .prologue
    .line 487
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    .line 488
    return-void
.end method

.method public isAllSelected()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 474
    iget-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    if-nez v2, :cond_1

    .line 482
    :cond_0
    :goto_0
    return v0

    .line 477
    :cond_1
    iget-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    if-eqz v2, :cond_2

    .line 478
    iget-object v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 479
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isDataLoaded()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 480
    iget-object v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getCount()I

    move-result v3

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public isDataStale()Z
    .locals 4

    .prologue
    .line 330
    iget-wide v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastChangeTime:J

    iget-wide v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastDataLoadTime:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsDestroyed:Z

    return v0
.end method

.method public isEditing()Z
    .locals 1

    .prologue
    .line 418
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 438
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsLoading:Z

    return v0
.end method

.method public isRowMultiSelected(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 453
    .line 455
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 456
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 458
    :cond_0
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->moveToPosition(I)Z

    .line 459
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getIdOfCurrentRow()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    .line 460
    if-eqz v0, :cond_3

    .line 461
    iget-object v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 464
    :goto_0
    iget-boolean v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    if-eqz v2, :cond_2

    .line 465
    if-nez v0, :cond_1

    const/4 v1, 0x1

    .line 469
    :cond_1
    :goto_1
    return v1

    :cond_2
    move v1, v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public loadData()V
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsLoading:Z

    .line 151
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->recordLoadingStackTrace()V

    .line 152
    return-void
.end method

.method public loadDataSynchronously()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->recordLoadingStackTrace()V

    .line 157
    return-void
.end method

.method protected notifyListenersDataSetChanged()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 349
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 350
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should only call notifyListenersDataSetChanged from main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 353
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 355
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 356
    iput-wide v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastChangeTime:J

    move v1, v0

    .line 357
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 358
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;

    .line 359
    iget-boolean v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mPaused:Z

    if-nez v2, :cond_1

    .line 360
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 363
    iget-wide v4, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mLastNotification:J

    iget-wide v6, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mThottle:J

    add-long/2addr v4, v6

    cmp-long v4, v4, v2

    if-gtz v4, :cond_2

    .line 364
    iput-wide v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mLastNotification:J

    .line 365
    iget-object v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mListener:Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;

    invoke-interface {v2, p0}, Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;->onDataSetChanged(Lcom/mfluent/asp/common/content/ContentAdapter;)V

    .line 366
    iget-boolean v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mScheduled:Z

    if-eqz v2, :cond_1

    .line 367
    iget-object v2, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    const v3, 0x7d166

    invoke-virtual {v2, v3, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->removeMessages(ILjava/lang/Object;)V

    .line 368
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mScheduled:Z

    .line 357
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 370
    :cond_2
    iget-boolean v4, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mScheduled:Z

    if-nez v4, :cond_1

    .line 371
    iget-object v4, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    const v5, 0x7d166

    invoke-virtual {v4, v5, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 372
    iget-object v5, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    iget-wide v6, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mLastNotification:J

    iget-wide v8, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mThottle:J

    add-long/2addr v6, v8

    sub-long v2, v6, v2

    invoke-virtual {v5, v4, v2, v3}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 373
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mScheduled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 377
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 378
    return-void
.end method

.method protected notifyListenersLoadFailed()V
    .locals 3

    .prologue
    .line 382
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 383
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should only call notifyListenersDataSetChanged from main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 389
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 390
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;

    .line 391
    iget-boolean v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mPaused:Z

    if-nez v2, :cond_1

    .line 392
    iget-object v0, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mListener:Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;

    invoke-interface {v0, p0}, Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;->onLoadFailed(Lcom/mfluent/asp/common/content/ContentAdapter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 389
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 397
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 398
    return-void

    .line 397
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public pauseDataSetChangedNotifications(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 278
    if-nez p1, :cond_0

    .line 297
    :goto_0
    return-void

    .line 282
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    move v1, v0

    .line 284
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 285
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;

    .line 286
    iget-object v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mListener:Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;

    if-ne v2, p1, :cond_2

    .line 287
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mPaused:Z

    .line 288
    iget-boolean v1, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mScheduled:Z

    if-eqz v1, :cond_1

    .line 289
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    const v2, 0x7d166

    invoke-virtual {v1, v2, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->removeMessages(ILjava/lang/Object;)V

    .line 290
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mScheduled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 284
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 296
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method protected final postDataLoadedMessage(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 884
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsDestroyed:Z

    if-eqz v0, :cond_0

    .line 890
    :goto_0
    return-void

    .line 887
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    const v1, 0x7d164

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 888
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 889
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->handleMessageOrQueue(Landroid/os/Message;)V

    goto :goto_0
.end method

.method protected final postDataSetChangedMessage()V
    .locals 2

    .prologue
    .line 902
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsDestroyed:Z

    if-eqz v0, :cond_0

    .line 908
    :goto_0
    return-void

    .line 906
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    const v1, 0x7d165

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 907
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->handleMessageOrQueue(Landroid/os/Message;)V

    goto :goto_0
.end method

.method protected final postLoadFailedMessage()V
    .locals 2

    .prologue
    .line 893
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsDestroyed:Z

    if-eqz v0, :cond_0

    .line 899
    :goto_0
    return-void

    .line 897
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    const v1, 0x7d167

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 898
    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mHandler:Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter$AsyncContentRefreshHandler;->handleMessageOrQueue(Landroid/os/Message;)V

    goto :goto_0
.end method

.method public registerListener(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;JJ)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 224
    if-nez p1, :cond_0

    .line 250
    :goto_0
    return v1

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 232
    const/4 v2, 0x0

    move v3, v1

    .line 233
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 234
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;

    iget-object v0, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mListener:Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;

    if-ne v0, p1, :cond_2

    .line 235
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;

    .line 239
    :goto_2
    if-nez v0, :cond_1

    .line 240
    new-instance v2, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;

    invoke-direct {v2, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;-><init>(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)V

    .line 241
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    iget-wide v4, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastChangeTime:J

    iget-wide v6, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastDataLoadTime:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    iget-wide v4, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastChangeTime:J

    cmp-long v0, p4, v4

    if-gez v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    move v1, v0

    move-object v0, v2

    .line 244
    :cond_1
    iput-wide p2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mThottle:J

    .line 245
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mScheduled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 233
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 242
    goto :goto_3

    .line 247
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_4
    move-object v0, v2

    goto :goto_2
.end method

.method public requery()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 649
    const/4 v0, 0x0

    return v0
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 727
    const/4 v0, 0x0

    return-object v0
.end method

.method public resumeDataSetChangedNotifications(Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 303
    .line 305
    if-nez p1, :cond_0

    .line 325
    :goto_0
    return v1

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    move v2, v1

    .line 311
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;

    .line 313
    iget-object v3, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mListener:Lcom/mfluent/asp/common/content/ContentAdapter$ContentAdapterListener;

    if-ne v3, p1, :cond_3

    .line 314
    iget-boolean v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mPaused:Z

    if-eqz v2, :cond_1

    .line 315
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mPaused:Z

    .line 316
    iget-wide v2, v0, Lcom/mfluent/asp/common/content/BaseContentAdapter$ListenerState;->mLastNotification:J

    iget-wide v4, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mLastChangeTime:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_2

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isDataStale()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    move v1, v0

    .line 322
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 316
    goto :goto_2

    .line 311
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 322
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mListenerLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public setAllRowsMultiSelected(Z)V
    .locals 1

    .prologue
    .line 662
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    if-eqz v0, :cond_1

    .line 663
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 664
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/IntVector;->removeAll()V

    .line 667
    :cond_0
    iput-boolean p1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    .line 669
    :cond_1
    return-void
.end method

.method public setIsEditing(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x14

    const/4 v1, 0x1

    .line 423
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    if-eq p1, v0, :cond_0

    .line 424
    iput-boolean p1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    .line 425
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    .line 426
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 427
    if-eqz p1, :cond_1

    .line 428
    new-instance v0, Lcom/mfluent/asp/common/util/IntVector;

    invoke-direct {v0, v2, v2, v1, v1}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZZ)V

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 430
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    goto :goto_0
.end method

.method public setRowMultiSelected(IZ)V
    .locals 2

    .prologue
    .line 618
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    if-eqz v0, :cond_3

    .line 620
    if-nez p2, :cond_1

    const/4 v0, 0x1

    .line 622
    :goto_0
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->moveToPosition(I)Z

    .line 623
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getIdOfCurrentRow()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v1

    .line 624
    if-eqz v1, :cond_0

    .line 626
    if-eqz v0, :cond_2

    .line 627
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 628
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 629
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    if-eqz v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/common/util/IntVector;->insert(I)I

    .line 644
    :cond_0
    :goto_1
    return-void

    .line 620
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 634
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 636
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    if-eqz v0, :cond_0

    .line 637
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelectedRows:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/common/util/IntVector;->remove(I)Z

    goto :goto_1

    :cond_3
    move v0, p2

    goto :goto_0
.end method

.method public setSectionContentAdapter(Lcom/mfluent/asp/common/content/SectionContentAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/common/content/SectionContentAdapter",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    if-eq v0, p1, :cond_1

    .line 195
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    invoke-interface {v0}, Lcom/mfluent/asp/common/content/SectionContentAdapter;->destroy()V

    .line 198
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    .line 201
    :cond_1
    return-void
.end method

.method public setSingleSelectedId(Lcom/mfluent/asp/common/content/ContentId;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 689
    if-eqz p1, :cond_0

    .line 690
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->copyId(Lcom/mfluent/asp/common/content/ContentId;)Lcom/mfluent/asp/common/content/ContentId;

    move-result-object p1

    .line 692
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    .line 693
    if-nez p1, :cond_2

    .line 694
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    .line 702
    :cond_1
    :goto_0
    return-void

    .line 695
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->isDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 696
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getRowOfId(Lcom/mfluent/asp/common/content/ContentId;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    .line 697
    iget v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    if-ltz v0, :cond_1

    .line 698
    iget v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->moveToPosition(I)Z

    .line 699
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getIdOfCurrentRow()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    goto :goto_0
.end method

.method public setSingleSelectedRow(I)V
    .locals 1

    .prologue
    .line 706
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 707
    invoke-virtual {p0}, Lcom/mfluent/asp/common/content/BaseContentAdapter;->getIdOfCurrentRow()Lcom/mfluent/asp/common/content/ContentId;

    move-result-object v0

    .line 708
    iput-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    .line 709
    iput p1, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedIndex:I

    .line 711
    :cond_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 106
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsEditing:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 108
    iget-boolean v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mIsInverseSelection:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 109
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSingleSelectedId:Lcom/mfluent/asp/common/content/ContentId;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 110
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    .line 111
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mMultiSelections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/content/ContentId;

    .line 113
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_2

    :cond_0
    move v0, v2

    .line 107
    goto :goto_0

    :cond_1
    move v0, v2

    .line 108
    goto :goto_1

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    if-eqz v0, :cond_4

    :goto_3
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 116
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    if-eqz v0, :cond_3

    .line 117
    iget-object v0, p0, Lcom/mfluent/asp/common/content/BaseContentAdapter;->mSectionAdapter:Lcom/mfluent/asp/common/content/SectionContentAdapter;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 119
    :cond_3
    return-void

    :cond_4
    move v1, v2

    .line 115
    goto :goto_3
.end method
