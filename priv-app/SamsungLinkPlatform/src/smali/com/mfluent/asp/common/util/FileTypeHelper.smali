.class public Lcom/mfluent/asp/common/util/FileTypeHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;
    }
.end annotation


# static fields
.field private static final APPLICATION_OCTET_STREAM:Ljava/lang/String; = "application/octet-stream"

.field private static final extensionsToDocumentTypeMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 107
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 108
    sput-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "DOC"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_WORD:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "DOCX"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_WORD:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "DOT"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_WORD:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "DOTX"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_WORD:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "HWP"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->HWP:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "PDF"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->PDF:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "PPT"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_POWERPOINT:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "PPTX"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_POWERPOINT:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "PPS"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_POWERPOINT:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "PPSX"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_POWERPOINT:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "POT"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_POWERPOINT:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "POTX"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_POWERPOINT:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "XLS"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_EXCEL:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "XLSX"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_EXCEL:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "XLT"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_EXCEL:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "XLTX"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_EXCEL:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    const-string v1, "CSV"

    sget-object v2, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->MS_EXCEL:Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    return-void
.end method

.method public static getAspDocumentExtensions()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static getExtensionsForAspDocumentType(Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;)Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 144
    sget-object v0, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 145
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p0, :cond_0

    .line 146
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 149
    :cond_1
    return-object v1
.end method

.method public static final getMimeTypeForFile(Ljava/io/File;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "application/octet-stream"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final getMimeTypeForFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 62
    const/4 v0, 0x0

    .line 64
    invoke-static {p0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    invoke-static {p0}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 66
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 71
    if-ltz v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 72
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 76
    :cond_0
    if-eqz v1, :cond_1

    .line 77
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v0

    .line 78
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 81
    sget-object v0, Lcom/mfluent/asp/common/util/MimeType;->EXTENSION_TO_MIME_TYPE_MAP:Ljava/util/HashMap;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 86
    :cond_1
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 90
    :goto_0
    return-object p1

    :cond_2
    move-object p1, v0

    goto :goto_0
.end method

.method public static final getMimeTypeForMedia(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 46
    const-string v0, "mime_type"

    invoke-static {p0, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    const-string v0, "_display_name"

    invoke-static {p0, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    const-string v1, "application/octet-stream"

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    :cond_0
    return-object v0
.end method

.method public static final getMimeTypeForMedia(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 30
    if-nez v1, :cond_0

    .line 31
    const-string v0, "application/octet-stream"

    .line 41
    :goto_0
    return-object v0

    .line 35
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 36
    const-string v0, "application/octet-stream"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 39
    :cond_1
    :try_start_1
    invoke-static {v1}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForMedia(Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 41
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static isAspDocument(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 134
    invoke-static {p0}, Lorg/apache/commons/io/FilenameUtils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->upperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 135
    if-nez v0, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 139
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isAspDocumentOfType(Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 153
    if-nez p0, :cond_0

    .line 154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "docType parameter must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_0
    invoke-static {p1}, Lorg/apache/commons/io/FilenameUtils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->upperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    if-nez v0, :cond_1

    .line 158
    const/4 v0, 0x0

    .line 162
    :goto_0
    return v0

    .line 161
    :cond_1
    sget-object v1, Lcom/mfluent/asp/common/util/FileTypeHelper;->extensionsToDocumentTypeMapping:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;

    .line 162
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/common/util/FileTypeHelper$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
