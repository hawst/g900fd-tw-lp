.class public Lcom/mfluent/asp/ASPApplication;
.super Landroid/app/Application;
.source "SourceFile"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# static fields
.field public static a:Z

.field public static b:I

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:Z

.field public static i:Z

.field public static j:Z

.field public static k:Z

.field public static final l:Ljava/util/regex/Pattern;

.field public static m:Ljava/lang/Integer;

.field public static n:Z

.field public static final o:Ljava/lang/String;

.field public static p:Landroid/os/Handler;

.field private static final q:Lorg/slf4j/Logger;

.field private static r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static w:I


# instance fields
.field private A:Z

.field private B:Lcom/mfluent/asp/dws/c;

.field private C:Lcom/mfluent/asp/dws/k;

.field private final D:Landroid/content/BroadcastReceiver;

.field private E:Landroid/content/res/Configuration;

.field private final F:Ljava/util/concurrent/ScheduledExecutorService;

.field private G:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private s:Landroid/app/Activity;

.field private t:Z

.field private u:Ljava/lang/String;

.field private v:I

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 99
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    .line 105
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 109
    sput-boolean v2, Lcom/mfluent/asp/ASPApplication;->a:Z

    .line 124
    const/4 v0, 0x2

    sput v0, Lcom/mfluent/asp/ASPApplication;->b:I

    .line 133
    sput v2, Lcom/mfluent/asp/ASPApplication;->w:I

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_BROADCAST_MASTER_RESET"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ASPApplication;->c:Ljava/lang/String;

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_BROADCAST_MASTER_RESET_COMPLETE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ASPApplication;->d:Ljava/lang/String;

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_BROADCAST_GO_HOME"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ASPApplication;->e:Ljava/lang/String;

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_BROADCAST_START_REVERSE_GEO_LOC_SERVICE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ASPApplication;->f:Ljava/lang/String;

    .line 168
    const/4 v0, 0x0

    sput-object v0, Lcom/mfluent/asp/ASPApplication;->g:Ljava/lang/String;

    .line 169
    sput-boolean v2, Lcom/mfluent/asp/ASPApplication;->h:Z

    .line 170
    sput-boolean v2, Lcom/mfluent/asp/ASPApplication;->i:Z

    .line 171
    sput-boolean v2, Lcom/mfluent/asp/ASPApplication;->j:Z

    .line 172
    sput-boolean v2, Lcom/mfluent/asp/ASPApplication;->k:Z

    .line 174
    const-string v0, "(^SM-E500[a-zA-Z]{0,2}$|^SM-A30[05][a-zA-Z]{0,2}$|^SM-N91[05][a-zA-Z]{0,1}$)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ASPApplication;->l:Ljava/util/regex/Pattern;

    .line 193
    sput-boolean v2, Lcom/mfluent/asp/ASPApplication;->n:Z

    .line 775
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_WAS_HOME_PREF_KEY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ASPApplication;->o:Ljava/lang/String;

    .line 1004
    new-instance v0, Lcom/mfluent/asp/ASPApplication$4;

    invoke-direct {v0}, Lcom/mfluent/asp/ASPApplication$4;-><init>()V

    sput-object v0, Lcom/mfluent/asp/ASPApplication;->p:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 203
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 129
    const-string v0, ""

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->u:Ljava/lang/String;

    .line 149
    const-string v0, "0"

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->x:Ljava/lang/String;

    .line 153
    const-string v0, "0"

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->y:Ljava/lang/String;

    .line 157
    const-string v0, "1.0.0"

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->z:Ljava/lang/String;

    .line 165
    iput-object v1, p0, Lcom/mfluent/asp/ASPApplication;->B:Lcom/mfluent/asp/dws/c;

    .line 166
    iput-object v1, p0, Lcom/mfluent/asp/ASPApplication;->C:Lcom/mfluent/asp/dws/k;

    .line 175
    new-instance v0, Lcom/mfluent/asp/ASPApplication$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ASPApplication$1;-><init>(Lcom/mfluent/asp/ASPApplication;)V

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->D:Landroid/content/BroadcastReceiver;

    .line 891
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->F:Ljava/util/concurrent/ScheduledExecutorService;

    .line 892
    iput-object v1, p0, Lcom/mfluent/asp/ASPApplication;->G:Ljava/util/concurrent/ScheduledFuture;

    .line 204
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {p0, v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 207
    :try_start_0
    const-class v0, Landroid/view/WindowManager$LayoutParams;

    const-string v1, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/common/util/ReflectionUtils;->getConstant(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    sput-object v0, Lcom/mfluent/asp/ASPApplication;->m:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :goto_0
    return-void

    .line 212
    :catch_0
    move-exception v0

    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v1, "::ASPApplication: Fail : ReflectionUtils.getConstant"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/ASPApplication;)Ljava/util/concurrent/ScheduledFuture;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->G:Ljava/util/concurrent/ScheduledFuture;

    return-object v0
.end method

.method private a(I)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 962
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::setApplicationReceiversEnabledState: entered with state = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 965
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 966
    const/4 v0, 0x0

    .line 968
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x282

    invoke-virtual {v4, v3, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 976
    :goto_0
    if-eqz v0, :cond_0

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    if-nez v3, :cond_2

    .line 978
    :cond_0
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v1, "::setApplicationReceiversEnabledState: Quitting without setting enabled state. No receivers found."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 988
    :cond_1
    return-void

    .line 973
    :catch_0
    move-exception v3

    sget-object v3, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v5, "Failed to find our app\'s package: {}"

    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 982
    :cond_2
    iget-object v5, v0, Landroid/content/pm/PackageInfo;->receivers:[Landroid/content/pm/ActivityInfo;

    array-length v6, v5

    move v3, v1

    :goto_1
    if-ge v3, v6, :cond_1

    aget-object v7, v5, v3

    .line 983
    iget-object v0, v7, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    iget-object v0, v7, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    const-string v8, "com.mfluent.asp.ASPApplication.DONT_MANAGE_RECEIVER"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    move v0, v2

    :goto_2
    if-eqz v0, :cond_4

    .line 984
    new-instance v0, Landroid/content/ComponentName;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v0, p0, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 985
    invoke-virtual {v4, v0, p1, v2}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 982
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_5
    move v0, v1

    .line 983
    goto :goto_2
.end method

.method static synthetic b(Lcom/mfluent/asp/ASPApplication;)Ljava/util/concurrent/ScheduledFuture;
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->G:Ljava/util/concurrent/ScheduledFuture;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 562
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 568
    :cond_0
    :goto_0
    return-void

    .line 563
    :catch_0
    move-exception v0

    .line 564
    sget-object v1, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x5

    if-gt v1, v2, :cond_0

    .line 565
    const-string v1, "mfl_ASPApplication"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::clearSharedPreferences:Trouble clearing preferences for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 699
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic l()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method public final varargs a(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 753
    const-string v0, "slpf_pref_20"

    invoke-virtual {p0, v0, v2}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 756
    const-string v1, "chinaCSC"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 758
    invoke-virtual {p0, p1, p2}, Lcom/mfluent/asp/ASPApplication;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 759
    if-eqz v1, :cond_0

    .line 760
    const-string v1, "Wi-Fi"

    const-string v2, "WLAN"

    invoke-static {v0, v1, v2}, Lorg/apache/commons/lang3/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 761
    const-string v1, "link.samsung.com"

    const-string v2, "link.samsung.cn"

    invoke-static {v0, v1, v2}, Lorg/apache/commons/lang3/StringUtils;->replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 764
    :cond_0
    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 772
    iput-object p1, p0, Lcom/mfluent/asp/ASPApplication;->u:Ljava/lang/String;

    .line 773
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x244

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final e()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x5

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 420
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_0

    .line 421
    const-string v0, "mfl_ASPApplication"

    const-string v1, "::masterReset:ASPApplication master reset called"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    :cond_0
    iput-boolean v5, p0, Lcom/mfluent/asp/ASPApplication;->A:Z

    .line 427
    :try_start_0
    invoke-static {p0}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    .line 428
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/mfluent/asp/filetransfer/d;->a(Z)V

    .line 430
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/d;->e()Landroid/util/LruCache;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/LruCache;->evictAll()V

    .line 433
    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/d;->f()V

    .line 436
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferSessions;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 443
    :cond_1
    :goto_0
    invoke-static {p0}, Lcom/mfluent/asp/sync/k;->a(Landroid/content/Context;)Lcom/mfluent/asp/sync/k;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/sync/k;->g(Landroid/content/Intent;)V

    .line 445
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/mfluent/asp/ASPApplication;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcastSync(Landroid/content/Intent;)V

    .line 448
    :try_start_1
    invoke-static {p0}, Landroid/webkit/WebViewDatabase;->getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabase;

    move-result-object v0

    .line 449
    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->clearFormData()V

    .line 450
    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->clearHttpAuthUsernamePassword()V

    .line 451
    invoke-virtual {v0}, Landroid/webkit/WebViewDatabase;->clearUsernamePassword()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 459
    :cond_2
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/mfluent/asp/util/l;->a()Lcom/mfluent/asp/util/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/util/l;->b()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 466
    :cond_3
    :goto_2
    :try_start_3
    invoke-static {p0}, Lcom/mfluent/asp/media/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/media/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/media/d;->b()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 468
    :cond_4
    :goto_3
    invoke-direct {p0, v5}, Lcom/mfluent/asp/ASPApplication;->a(I)V

    .line 470
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mfluent/asp/ASPApplication$2;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/ASPApplication$2;-><init>(Lcom/mfluent/asp/ASPApplication;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 479
    :try_start_4
    invoke-static {p0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/a;->d()V

    .line 480
    const-string v0, "mfl_ASPApplication"

    const-string v1, "resetAccessCredential called by masterReset"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 483
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 484
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->c(Ljava/lang/String;)V

    .line 485
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->O()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 492
    :cond_5
    :goto_4
    const-string v0, "slpf_pref_20"

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ASPApplication;->b(Ljava/lang/String;)V

    const-string v0, "UploaderSetting"

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ASPApplication;->b(Ljava/lang/String;)V

    const-string v0, "cloudStorageTypes"

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ASPApplication;->b(Ljava/lang/String;)V

    const-string v0, "asp_device_pref_15"

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ASPApplication;->b(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/pcw/util/a;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "mfl_ASPApplication"

    const-string v1, "::No samsung account - clearSharedPrefernece PUSH"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "asp_push_pref_15"

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ASPApplication;->b(Ljava/lang/String;)V

    :goto_5
    const-string v0, "ntsSettings"

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ASPApplication;->b(Ljava/lang/String;)V

    const-string v0, "slpf_device_20"

    invoke-direct {p0, v0}, Lcom/mfluent/asp/ASPApplication;->b(Ljava/lang/String;)V

    .line 493
    invoke-static {p0}, Lcom/mfluent/asp/datamodel/t$a;->a(Landroid/content/Context;)Lcom/mfluent/asp/datamodel/t$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t$a;->a()V

    .line 496
    const-string v0, "slpf_pref_20"

    invoke-virtual {p0, v0, v5}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 497
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 498
    const-string v1, "mreset"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 499
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 501
    const-class v0, Lcom/mfluent/asp/dws/e;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/dws/e;

    .line 502
    if-eqz v0, :cond_6

    .line 503
    const-string v1, "mfl_ASPApplication"

    const-string v2, "terminating MediaStreamingProxyServer..."

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    invoke-virtual {v0}, Lcom/mfluent/asp/dws/e;->b()V

    .line 506
    :cond_6
    new-instance v0, Lcom/mfluent/asp/dws/e;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/dws/e;-><init>(Landroid/content/Context;)V

    const-class v1, Lcom/mfluent/asp/dws/e;

    invoke-static {v0, v1}, Lcom/mfluent/asp/b;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 507
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->B:Lcom/mfluent/asp/dws/c;

    if-eqz v0, :cond_7

    .line 508
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->B:Lcom/mfluent/asp/dws/c;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/c;->a()V

    .line 509
    iput-object v4, p0, Lcom/mfluent/asp/ASPApplication;->B:Lcom/mfluent/asp/dws/c;

    .line 511
    :cond_7
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->C:Lcom/mfluent/asp/dws/k;

    if-eqz v0, :cond_8

    .line 512
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->C:Lcom/mfluent/asp/dws/k;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/k;->a()V

    .line 513
    iput-object v4, p0, Lcom/mfluent/asp/ASPApplication;->C:Lcom/mfluent/asp/dws/k;

    .line 516
    :cond_8
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/mfluent/asp/ContentAggregatorService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ASPApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 518
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.BROADCAST_SIGNIN_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 519
    const-string v1, "SlinkSignInUtils.EXTRA_SIGNIN_STATE_SIGNED_IN"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 520
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ASPApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 522
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/mfluent/asp/ASPApplication;->d:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcastSync(Landroid/content/Intent;)V

    .line 523
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_9

    .line 524
    const-string v0, "mfl_ASPApplication"

    const-string v1, "::masterReset:ASPApplication master reset finished - shutting down"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    :cond_9
    return-void

    .line 437
    :catch_0
    move-exception v0

    .line 438
    sget-object v1, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_1

    .line 439
    const-string v1, "mfl_ASPApplication"

    const-string v2, "::masterReset:Trouble cancelling FileTransferManager tasks"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 452
    :catch_1
    move-exception v0

    .line 453
    sget-object v1, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_2

    .line 454
    const-string v1, "mfl_ASPApplication"

    const-string v2, "::masterReset:Trouble clearing webview database"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 460
    :catch_2
    move-exception v0

    .line 461
    sget-object v1, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_3

    .line 462
    const-string v1, "mfl_ASPApplication"

    const-string v2, "::masterReset:Trouble clearing http cookies"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 466
    :catch_3
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_4

    const-string v1, "mfl_ASPApplication"

    const-string v2, "::clearCaches:Trouble clearing AspThumbnailCache"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 487
    :catch_4
    move-exception v0

    sget-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_5

    .line 488
    const-string v0, "mfl_ASPApplication"

    const-string v1, "::masterReset:failed to reset access manager."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 492
    :cond_a
    const-string v0, "mfl_ASPApplication"

    const-string v1, "::Has samsung account - do not clearSharedPrefernece PUSH"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method public final g()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 704
    invoke-static {}, Lcom/mfluent/asp/datamodel/au;->a()Lcom/mfluent/asp/datamodel/au;

    move-result-object v0

    iput-boolean v7, v0, Lcom/mfluent/asp/datamodel/au;->a:Z

    iput-boolean v7, v0, Lcom/mfluent/asp/datamodel/au;->b:Z

    iput-boolean v7, v0, Lcom/mfluent/asp/datamodel/au;->c:Z

    iput-boolean v7, v0, Lcom/mfluent/asp/datamodel/au;->d:Z

    iput-boolean v7, v0, Lcom/mfluent/asp/datamodel/au;->e:Z

    iput-boolean v7, v0, Lcom/mfluent/asp/datamodel/au;->f:Z

    iput-boolean v7, v0, Lcom/mfluent/asp/datamodel/au;->g:Z

    iput-boolean v7, v0, Lcom/mfluent/asp/datamodel/au;->h:Z

    iput-boolean v7, v0, Lcom/mfluent/asp/datamodel/au;->i:Z

    const/4 v1, -0x1

    iput v1, v0, Lcom/mfluent/asp/datamodel/au;->j:I

    iput v7, v0, Lcom/mfluent/asp/datamodel/au;->k:I

    .line 706
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    new-instance v1, Lcom/mfluent/asp/ASPApplication$3;

    invoke-direct {v1, p0, v0}, Lcom/mfluent/asp/ASPApplication$3;-><init>(Lcom/mfluent/asp/ASPApplication;Lcom/mfluent/asp/datamodel/t;)V

    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->G:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->G:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    iput-object v8, p0, Lcom/mfluent/asp/ASPApplication;->G:Ljava/util/concurrent/ScheduledFuture;

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->F:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x258

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->G:Ljava/util/concurrent/ScheduledFuture;

    .line 708
    iput-boolean v7, p0, Lcom/mfluent/asp/ASPApplication;->t:Z

    .line 710
    invoke-virtual {p0, v8}, Lcom/mfluent/asp/ASPApplication;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/util/b;->c(Ljava/io/File;)V

    .line 711
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final i()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 789
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 790
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->isCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 791
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Only call goHome() on the main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 798
    :catch_0
    move-exception v0

    .line 799
    const-string v1, "mfl_ASPApplication"

    const-string v2, "ASPApplication exception"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 802
    :cond_0
    iget-boolean v0, p0, Lcom/mfluent/asp/ASPApplication;->A:Z

    if-nez v0, :cond_4

    .line 803
    iput-boolean v3, p0, Lcom/mfluent/asp/ASPApplication;->A:Z

    .line 805
    invoke-static {p0}, Lcom/mfluent/asp/a/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a/a;

    move-result-object v0

    .line 806
    invoke-virtual {v0, p0}, Lcom/mfluent/asp/a/a;->b(Landroid/content/Context;)V

    .line 807
    invoke-virtual {v0}, Lcom/mfluent/asp/a/a;->d()Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-static {}, Lcom/mfluent/asp/a/a;->a()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/mfluent/asp/ASPApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 809
    invoke-virtual {v0}, Lcom/mfluent/asp/a/a;->d()Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-static {}, Lcom/mfluent/asp/a/a;->b()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/mfluent/asp/ASPApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 810
    invoke-virtual {v0}, Lcom/mfluent/asp/a/a;->d()Landroid/content/BroadcastReceiver;

    move-result-object v0

    invoke-static {}, Lcom/mfluent/asp/a/a;->c()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ASPApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 812
    const-string v0, "slpf_pref_20"

    invoke-virtual {p0, v0, v4}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 813
    sget-object v1, Lcom/mfluent/asp/ASPApplication;->o:Ljava/lang/String;

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 814
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 815
    sget-object v1, Lcom/mfluent/asp/ASPApplication;->o:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 816
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 819
    invoke-direct {p0, v3}, Lcom/mfluent/asp/ASPApplication;->a(I)V

    .line 824
    :cond_1
    const-string v0, "asp_push_pref_15"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "valid_user"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance v0, Lcom/sec/pcw/service/push/b;

    invoke-direct {v0, p0}, Lcom/sec/pcw/service/push/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p0}, Lcom/sec/pcw/service/push/b;->a(Landroid/content/Context;)Z

    .line 828
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 831
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 832
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v1, "::goHome: initializing core"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 833
    invoke-static {p0}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    invoke-virtual {v0}, Lpcloud/net/nat/c;->h()V

    .line 840
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.videoplayer"

    const-string v2, "com.sec.android.app.videoplayer.slink.SCSCoreInitService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 842
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ASPApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 846
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.BROADCAST_SAMSUNG_LINK_STARTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 847
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 851
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mfluent/asp/ContentAggregatorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ASPApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 853
    const-class v0, Lcom/mfluent/asp/dws/e;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/dws/e;

    .line 854
    if-eqz v0, :cond_3

    .line 855
    const-string v1, "mfl_ASPApplication"

    const-string v2, "calling start on MediaStreamingProxyServer..."

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    invoke-virtual {v0}, Lcom/mfluent/asp/dws/e;->start()V

    .line 858
    :cond_3
    new-instance v0, Lcom/mfluent/asp/dws/c;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/dws/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->B:Lcom/mfluent/asp/dws/c;

    .line 859
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->B:Lcom/mfluent/asp/dws/c;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/c;->start()V

    .line 860
    new-instance v0, Lcom/mfluent/asp/dws/k;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/dws/k;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->C:Lcom/mfluent/asp/dws/k;

    .line 861
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->C:Lcom/mfluent/asp/dws/k;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/k;->start()V

    .line 863
    invoke-static {p0}, Lcom/mfluent/asp/sync/k;->a(Landroid/content/Context;)Lcom/mfluent/asp/sync/k;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/mfluent/asp/ContentAggregatorService;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/sync/k;->f(Landroid/content/Intent;)V

    .line 866
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/mfluent/asp/ASPApplication;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 867
    const-string v1, "REFRESH_FROM_KEY"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 868
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 870
    :cond_4
    return-void

    .line 794
    :cond_5
    :try_start_1
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 795
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Only call goHome() on the main thread.s"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public final j()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 878
    const-string v0, "slpf_pref_20"

    invoke-virtual {p0, v0, v2}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 879
    sget-object v1, Lcom/mfluent/asp/ASPApplication;->o:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final k()Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 883
    const-string v0, "slpf_pref_20"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 627
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628
    const-string v0, "mfl_ASPApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onActivityCreated: activity is - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    :cond_0
    sget v0, Lcom/mfluent/asp/ASPApplication;->w:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mfluent/asp/ASPApplication;->w:I

    .line 632
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 636
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637
    const-string v0, "mfl_ASPApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onActivityDestroyed: activity is - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    :cond_0
    sget v0, Lcom/mfluent/asp/ASPApplication;->w:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/mfluent/asp/ASPApplication;->w:I

    .line 641
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 572
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573
    const-string v0, "mfl_ASPApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onActivityPaused: activity is - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->s:Landroid/app/Activity;

    .line 576
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 581
    invoke-static {p0}, Lcom/mfluent/asp/util/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 582
    const-string v0, "mfl_ASPApplication"

    const-string v1, "Because the user is a guest, and then exit the Samsung link."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    :cond_0
    :goto_0
    return-void

    .line 586
    :cond_1
    iput-object p1, p0, Lcom/mfluent/asp/ASPApplication;->s:Landroid/app/Activity;

    .line 588
    invoke-static {p0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v0

    .line 589
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 591
    :goto_1
    const-string v2, "slpf_device_20"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 592
    const-string v3, "setds"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mfluent/asp/ASPApplication;->u:Ljava/lang/String;

    .line 594
    sget-object v2, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 595
    const-string v2, "mfl_ASPApplication"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::onActivityResumed: activity is - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", guid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->G:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 620
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->G:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 621
    iput-object v1, p0, Lcom/mfluent/asp/ASPApplication;->G:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 589
    goto :goto_1
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 645
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 646
    const-string v0, "mfl_ASPApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onActivitySaveInstanceState: activity is - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    :cond_0
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 652
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 653
    const-string v0, "mfl_ASPApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onActivityStarted: activity is - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    :cond_0
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 659
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660
    const-string v0, "mfl_ASPApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onActivityStopped: activity is - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    :cond_0
    invoke-static {p0}, Lcom/mfluent/asp/util/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 664
    const-string v0, "mfl_ASPApplication"

    const-string v1, "Because the user is a guest, and then exit the Samsung link."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 673
    invoke-super {p0, p1}, Landroid/app/Application;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 675
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 676
    const-string v0, "mfl_ASPApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onConfigurationChanged: (old)"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/ASPApplication;->E:Landroid/content/res/Configuration;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    const-string v0, "mfl_ASPApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onConfigurationChanged: (new)"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->E:Landroid/content/res/Configuration;

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/mfluent/asp/ASPApplication;->E:Landroid/content/res/Configuration;

    invoke-virtual {p1, v1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    move-result v1

    and-int/lit8 v1, v1, 0x4

    if-ne v0, v1, :cond_2

    .line 681
    :cond_1
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->r:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 682
    const-string v0, "mfl_ASPApplication"

    const-string v1, "::onConfigurationChanged: locale changed or previous status is empty"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    :cond_2
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->E:Landroid/content/res/Configuration;

    .line 687
    return-void
.end method

.method public onCreate()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 218
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 221
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 222
    sget-object v3, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v4, "::onCreate: PackageInfo versionName: {} versionCode: {}"

    iget-object v5, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v4, v5, v0}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :goto_0
    invoke-static {p0}, Lcom/mfluent/asp/util/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 228
    const-string v0, "mfl_ASPApplication"

    const-string v1, "Because the user is a guest, and then exit the Samsung link."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-direct {p0, v2}, Lcom/mfluent/asp/ASPApplication;->a(I)V

    .line 343
    :cond_0
    :goto_1
    return-void

    .line 223
    :catch_0
    move-exception v0

    .line 224
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v3, "::onCreate: Failed to get package info for logging version info"

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    goto :goto_0

    .line 233
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 235
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->j()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 236
    invoke-direct {p0, v1}, Lcom/mfluent/asp/ASPApplication;->a(I)V

    .line 244
    :goto_2
    invoke-static {p0}, Lcom/samsung/android/sdk/samsung/a;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/a;

    .line 247
    invoke-static {p0}, Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/cloudstorage/CloudStorageBroadcastManager;

    .line 249
    invoke-static {p0}, Lcom/mfluent/asp/common/util/AspLogLevels;->loadLogLevels(Landroid/content/Context;)V

    .line 251
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ASPApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    :try_start_1
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :goto_3
    new-instance v3, Lcom/mfluent/asp/util/g;

    invoke-direct {v3, v0}, Lcom/mfluent/asp/util/g;-><init>(Landroid/app/NotificationManager;)V

    invoke-virtual {v3}, Lcom/mfluent/asp/util/g;->a()V

    .line 257
    :try_start_2
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v3, "version.txt"

    invoke-virtual {v0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v3, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->x:Ljava/lang/String;

    :cond_2
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v3, "::setVersion: version (FILE): {}"

    iget-object v6, p0, Lcom/mfluent/asp/ASPApplication;->x:Ljava/lang/String;

    invoke-interface {v0, v3, v6}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 259
    :goto_4
    :try_start_3
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v3, "platformVersion.txt"

    invoke-virtual {v0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v3, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->y:Ljava/lang/String;

    :cond_3
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v3, "::setPlatformVersion: version (FILE): {}"

    iget-object v6, p0, Lcom/mfluent/asp/ASPApplication;->y:Ljava/lang/String;

    invoke-interface {v0, v3, v6}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    .line 261
    :goto_5
    invoke-virtual {p0, v9}, Lcom/mfluent/asp/ASPApplication;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    .line 265
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v3, 0x244

    if-lt v0, v3, :cond_9

    move v0, v1

    :goto_6
    sput-boolean v0, Lcom/mfluent/asp/ASPApplication;->i:Z

    .line 267
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v3, 0x1e0

    if-ge v0, v3, :cond_a

    move v0, v1

    :goto_7
    sput-boolean v0, Lcom/mfluent/asp/ASPApplication;->j:Z

    .line 269
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-nez v0, :cond_4

    move v2, v1

    :cond_4
    sput-boolean v2, Lcom/mfluent/asp/ASPApplication;->h:Z

    .line 271
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->l:Ljava/util/regex/Pattern;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 272
    sput-boolean v1, Lcom/mfluent/asp/ASPApplication;->k:Z

    .line 275
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_6

    .line 276
    sput-boolean v1, Lcom/mfluent/asp/ASPApplication;->k:Z

    .line 279
    :cond_6
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {p0, v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 280
    new-instance v0, Lcom/mfluent/asp/util/z;

    iget-object v2, p0, Lcom/mfluent/asp/ASPApplication;->y:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/mfluent/asp/util/z;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/mfluent/asp/util/z;

    invoke-static {v0, v2}, Lcom/mfluent/asp/b;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 285
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ASPApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLargeMemoryClass()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/ASPApplication;->v:I

    .line 289
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isPlatformEnabled()Z

    move-result v0

    if-ne v1, v0, :cond_b

    .line 290
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, ""

    invoke-virtual {v0, v1, v2, v9, v9}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 291
    const-string v0, "mfl_ASPApplication"

    const-string v1, "the samsunglink platform is enabled"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :goto_8
    :try_start_4
    new-instance v0, Lcom/mfluent/asp/sync/h;

    invoke-direct {v0}, Lcom/mfluent/asp/sync/h;-><init>()V

    const-class v1, Lcom/mfluent/asp/sync/h;

    invoke-static {v0, v1}, Lcom/mfluent/asp/b;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 300
    new-instance v0, Lcom/mfluent/a/a/b;

    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mfluent/a/a/b;-><init>(Landroid/content/Context;)V

    const-class v1, Lcom/mfluent/a/a/b;

    invoke-static {v0, v1}, Lcom/mfluent/asp/b;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 302
    new-instance v0, Lcom/sec/pcw/analytics/a;

    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/pcw/analytics/a;-><init>(Landroid/content/Context;)V

    .line 303
    const-class v1, Lcom/sec/pcw/analytics/a;

    invoke-static {v0, v1}, Lcom/mfluent/asp/b;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 304
    invoke-virtual {v0}, Lcom/sec/pcw/analytics/a;->b()Z

    .line 306
    invoke-static {p0}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/mfluent/asp/filetransfer/d;->c()V

    .line 310
    new-instance v0, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;

    invoke-direct {v0}, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;-><init>()V

    const-class v1, Lcom/mfluent/asp/media/videotranscoder/VideoTranscodingManager;

    invoke-static {v0, v1}, Lcom/mfluent/asp/b;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 312
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mfluent/asp/ContentAggregatorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/ASPApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 314
    new-instance v0, Lcom/mfluent/asp/dws/e;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/dws/e;-><init>(Landroid/content/Context;)V

    const-class v1, Lcom/mfluent/asp/dws/e;

    invoke-static {v0, v1}, Lcom/mfluent/asp/b;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 316
    invoke-virtual {p0, p0}, Lcom/mfluent/asp/ASPApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 320
    invoke-static {p0}, Lcom/mfluent/asp/util/y;->a(Landroid/content/Context;)Lcom/mfluent/asp/util/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/util/y;->b()V

    .line 323
    invoke-static {p0}, Lcom/sec/pcw/hybrid/update/d;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->z:Ljava/lang/String;

    .line 324
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->z:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/StringUtils;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 325
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->z:Ljava/lang/String;

    const-string v1, " "

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/StringUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    .line 326
    iget-object v1, p0, Lcom/mfluent/asp/ASPApplication;->z:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lorg/apache/commons/lang3/StringUtils;->substring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->z:Ljava/lang/String;

    .line 329
    :cond_7
    const-string v0, "slpf_device_20"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 330
    const-string v1, "setds"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->u:Ljava/lang/String;

    .line 331
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 332
    sget-object v1, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v2, "::onCreate() create LocalDeviceMetadataSyncManager with local device:{}"

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 333
    new-instance v1, Lcom/mfluent/asp/sync/g;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/mfluent/asp/sync/g;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V

    const-class v0, Lcom/mfluent/asp/sync/g;

    invoke-static {v1, v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Object;Ljava/lang/Class;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_8

    .line 340
    iget-object v0, p0, Lcom/mfluent/asp/ASPApplication;->D:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.samsung.android.sdk.samsunglink.SlinkLaunchUtils.BROADCAST_SAMSUNG_LINK_EXITED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/ASPApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 341
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v1, "::onCreate: ASPApplication onCreate() DONE: {}ms"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 342
    :try_start_5
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.samsung.android.app.galaxyfinder"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/mfluent/asp/ASPApplication;->n:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 239
    :cond_8
    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v3, "Resetting receivers because wasHome == false"

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 240
    invoke-direct {p0, v2}, Lcom/mfluent/asp/ASPApplication;->a(I)V

    goto/16 :goto_2

    .line 251
    :catch_2
    move-exception v3

    const-string v6, "mfl_ASPApplication"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "setupNotificationCanceler - Exception : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 257
    :catch_3
    move-exception v0

    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v3, "::setVersion: version file not found"

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    :try_start_6
    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/ASPApplication;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v0, v3, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->x:Ljava/lang/String;

    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v3, "::setVersion: version (PACKAGE): {}"

    iget-object v6, p0, Lcom/mfluent/asp/ASPApplication;->x:Ljava/lang/String;

    invoke-interface {v0, v3, v6}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_4

    :catch_4
    move-exception v0

    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v3, "::setVersion: package name not found"

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    const-string v0, "1.6"

    iput-object v0, p0, Lcom/mfluent/asp/ASPApplication;->x:Ljava/lang/String;

    goto/16 :goto_4

    :catch_5
    move-exception v0

    sget-object v3, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v6, "::setVersion: problem reading version file"

    invoke-interface {v3, v6}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 259
    :catch_6
    move-exception v0

    sget-object v0, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v3, "::setPlatformVersion: version file not found"

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    goto/16 :goto_5

    :catch_7
    move-exception v0

    sget-object v3, Lcom/mfluent/asp/ASPApplication;->q:Lorg/slf4j/Logger;

    const-string v6, "::setPlatformVersion: problem reading version file"

    invoke-interface {v3, v6}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    :cond_9
    move v0, v2

    .line 265
    goto/16 :goto_6

    :cond_a
    move v0, v2

    .line 267
    goto/16 :goto_7

    .line 293
    :cond_b
    const-string v0, "mfl_ASPApplication"

    const-string v1, "the samsunglink platform is not enabled, ignore dummy call"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 334
    :catch_8
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1
.end method
