.class public Lcom/mfluent/asp/NTSLockService;
.super Landroid/app/Service;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/NTSLockService$a;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static volatile b:Z

.field private static volatile d:Z

.field private static final e:Ljava/util/concurrent/Semaphore;


# instance fields
.field private final c:Landroid/os/Binder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-class v0, Lcom/mfluent/asp/NTSLockService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/NTSLockService;->a:Lorg/slf4j/Logger;

    .line 36
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v0, Lcom/mfluent/asp/NTSLockService;->e:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 33
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/NTSLockService;->c:Landroid/os/Binder;

    .line 60
    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 113
    sget-boolean v0, Lcom/mfluent/asp/NTSLockService;->b:Z

    return v0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 121
    sget-boolean v0, Lcom/mfluent/asp/NTSLockService;->d:Z

    return v0
.end method

.method static synthetic c()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/mfluent/asp/NTSLockService;->a:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 44
    sget-object v0, Lcom/mfluent/asp/NTSLockService;->a:Lorg/slf4j/Logger;

    const-string v1, "::onBind: called"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 45
    sput-boolean v2, Lcom/mfluent/asp/NTSLockService;->b:Z

    .line 46
    sput-boolean v2, Lcom/mfluent/asp/NTSLockService;->d:Z

    .line 48
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.sync.DeviceListSyncManager.SYNC_ON_WAKELOCK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 51
    sget-object v0, Lcom/mfluent/asp/NTSLockService;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    new-instance v0, Lcom/mfluent/asp/NTSLockService$a;

    sget-object v1, Lcom/mfluent/asp/NTSLockService;->e:Ljava/util/concurrent/Semaphore;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/mfluent/asp/NTSLockService$a;-><init>(Ljava/util/concurrent/Semaphore;Landroid/content/Context;B)V

    invoke-virtual {v0}, Lcom/mfluent/asp/NTSLockService$a;->start()V

    .line 57
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/NTSLockService;->c:Landroid/os/Binder;

    return-object v0

    .line 54
    :cond_0
    sget-object v0, Lcom/mfluent/asp/NTSLockService;->a:Lorg/slf4j/Logger;

    const-string v1, "::onBind: Skipping initialization - another init thread is already running"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 107
    sget-object v0, Lcom/mfluent/asp/NTSLockService;->a:Lorg/slf4j/Logger;

    const-string v1, "::onUnbind: called"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 108
    sput-boolean v2, Lcom/mfluent/asp/NTSLockService;->b:Z

    .line 109
    return v2
.end method
