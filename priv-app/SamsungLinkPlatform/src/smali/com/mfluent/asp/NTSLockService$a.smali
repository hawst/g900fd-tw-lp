.class final Lcom/mfluent/asp/NTSLockService$a;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/NTSLockService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Semaphore;

.field private final b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Semaphore;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/mfluent/asp/NTSLockService$a;->a:Ljava/util/concurrent/Semaphore;

    .line 68
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/NTSLockService$a;->b:Landroid/content/Context;

    .line 69
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/concurrent/Semaphore;Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/NTSLockService$a;-><init>(Ljava/util/concurrent/Semaphore;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 74
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->c()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::InitThread.run: executing initialization"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 75
    invoke-super {p0}, Ljava/lang/Thread;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/NTSLockService$a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->c()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::InitThread.run: broadcast PresenceManager.BROADCAST_SEND_WAKE_UP_PUSH"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/mfluent/asp/NTSLockService$a;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/mfluent/asp/sync/i;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 83
    iget-object v0, p0, Lcom/mfluent/asp/NTSLockService$a;->b:Landroid/content/Context;

    invoke-static {v0}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    invoke-virtual {v0}, Lpcloud/net/nat/c;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    :cond_0
    :goto_0
    :try_start_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/mfluent/asp/NTSLockService$a;->b:Landroid/content/Context;

    const-class v2, Lcom/mfluent/asp/ContentAggregatorService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    const-string v1, "com.samsung.android.sdk.samsunglink.ContentAggregatorService.UPDATE_CHECK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    iget-object v1, p0, Lcom/mfluent/asp/NTSLockService$a;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 99
    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/NTSLockService$a;->a:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 100
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    :try_start_3
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->c()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::NTSLockService.InitThread: trouble in init thread: "

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 99
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/NTSLockService$a;->a:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0

    .line 94
    :catch_1
    move-exception v0

    .line 95
    :try_start_4
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->c()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::NTSLockService - update reqeust failed: "

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method
