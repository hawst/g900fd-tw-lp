.class public Lcom/mfluent/asp/ContentAggregatorService;
.super Landroid/app/Service;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/ContentAggregatorService$a;,
        Lcom/mfluent/asp/ContentAggregatorService$b;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Lorg/slf4j/Logger;

.field private static c:Z

.field private static d:Z

.field private static final e:J


# instance fields
.field private f:Landroid/os/HandlerThread;

.field private g:Lcom/mfluent/asp/ContentAggregatorService$b;

.field private h:Z

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 37
    const-class v0, Lcom/mfluent/asp/ContentAggregatorService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    .line 39
    sput-boolean v1, Lcom/mfluent/asp/ContentAggregatorService;->c:Z

    .line 40
    sput-boolean v1, Lcom/mfluent/asp/ContentAggregatorService;->d:Z

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/ContentAggregatorService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_SERVICE_ON_CREATE_INTENT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/ContentAggregatorService;->a:Ljava/lang/String;

    .line 52
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/ContentAggregatorService;->e:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 57
    iput-boolean v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->h:Z

    .line 58
    iput v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->i:I

    .line 148
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "<Secret>"

    goto :goto_0
.end method

.method static synthetic a()Z
    .locals 1

    .prologue
    .line 35
    sget-boolean v0, Lcom/mfluent/asp/ContentAggregatorService;->c:Z

    return v0
.end method

.method static synthetic a(Lcom/mfluent/asp/ContentAggregatorService;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->h:Z

    return v0
.end method

.method static synthetic b(Lcom/mfluent/asp/ContentAggregatorService;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->i:I

    return v0
.end method

.method static synthetic b()J
    .locals 2

    .prologue
    .line 35
    sget-wide v0, Lcom/mfluent/asp/ContentAggregatorService;->e:J

    return-wide v0
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 35
    sget-boolean v0, Lcom/mfluent/asp/ContentAggregatorService;->d:Z

    return v0
.end method

.method static synthetic d()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic e()V
    .locals 3

    .prologue
    .line 35
    sget-object v0, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    const-string v1, "cleanUpSCSFolder()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/storage/emulated/0/Proxy_debug"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    const-string v2, "/mnt/sdcard/SL_DEBUG"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->deleteDirectory(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    const-string v2, "Error in cleanUpSCSFolder"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 154
    sget-object v0, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    const-string v1, "onBind"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->h:Z

    .line 156
    new-instance v0, Lcom/mfluent/asp/ContentAggregatorService$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/ContentAggregatorService$a;-><init>(Lcom/mfluent/asp/ContentAggregatorService;B)V

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 169
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 171
    :try_start_0
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "N910"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    const/4 v0, 0x1

    sput-boolean v0, Lcom/mfluent/asp/ContentAggregatorService;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->h:Z

    .line 181
    invoke-static {p0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v3

    .line 184
    if-nez v3, :cond_3

    move-object v0, v1

    .line 188
    :goto_1
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v2

    .line 190
    sget-object v4, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::onCreate: LocalDevice peerId : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/mfluent/asp/ContentAggregatorService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 192
    if-nez v2, :cond_1

    if-eqz v3, :cond_1

    .line 193
    invoke-virtual {v3}, Lcom/sec/pcw/hybrid/b/b;->i()Ljava/lang/String;

    move-result-object v2

    .line 195
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 197
    invoke-static {p0}, Lcom/sec/pcw/service/account/b;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 198
    invoke-static {v2}, Lcom/sec/pcw/util/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 200
    sget-object v3, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::onCreate: providing peerId from hardware: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/mfluent/asp/ContentAggregatorService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 211
    :cond_1
    :goto_2
    sget-object v3, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    const-string v4, "::onCreate: getting nts instance id"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 212
    invoke-static {}, Lpcloud/net/nat/c;->g()Ljava/lang/String;

    move-result-object v3

    .line 213
    sget-object v4, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    const-string v5, "::onCreate: got nts instance id"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 214
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 221
    :cond_2
    invoke-static {v1}, Lcom/mfluent/asp/util/r;->a(Lcom/msc/seclib/CoreConfig;)V

    .line 225
    :goto_3
    new-instance v0, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->f:Landroid/os/HandlerThread;

    .line 226
    iget-object v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->f:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 228
    new-instance v0, Lcom/mfluent/asp/ContentAggregatorService$b;

    iget-object v1, p0, Lcom/mfluent/asp/ContentAggregatorService;->f:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/ContentAggregatorService$b;-><init>(Lcom/mfluent/asp/ContentAggregatorService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->g:Lcom/mfluent/asp/ContentAggregatorService$b;

    .line 231
    return-void

    .line 174
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 184
    :cond_3
    invoke-virtual {v3}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 203
    :cond_4
    invoke-static {p0, v2}, Lcom/sec/pcw/service/account/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 204
    invoke-static {v2}, Lcom/sec/pcw/util/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 206
    sget-object v3, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::onCreate: providing peerId from hardware using SA info: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/mfluent/asp/ContentAggregatorService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_2

    .line 223
    :cond_5
    invoke-static {v0, v2, v3}, Lcom/mfluent/asp/util/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 303
    sget-object v0, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    const-string v1, "onDestroy"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 304
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 308
    iget-object v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->f:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 309
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v2, 0x2

    .line 235
    if-nez p1, :cond_1

    const/4 v0, 0x0

    move-object v1, v0

    .line 237
    :goto_0
    sget-object v0, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    const-string v3, "onStartCommand: {}, {}"

    invoke-interface {v0, v3, v1, p1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 239
    new-instance v0, Lcom/mfluent/asp/ContentAggregatorService$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/ContentAggregatorService$1;-><init>(Lcom/mfluent/asp/ContentAggregatorService;)V

    .line 248
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 250
    iput p3, p0, Lcom/mfluent/asp/ContentAggregatorService;->i:I

    .line 251
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 253
    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->j()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 254
    sget-object v3, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    const-string v4, "::onStartCommand: user has logged in; doing full start up of {}"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 255
    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->i()V

    .line 262
    const/4 v0, -0x1

    .line 264
    const-string v3, "com.samsung.android.sdk.samsunglink.ContentAggregatorService.UPDATE_CHECK"

    invoke-static {v1, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 266
    invoke-static {p0}, Lcom/mfluent/asp/util/y;->a(Landroid/content/Context;)Lcom/mfluent/asp/util/y;

    move-result-object v1

    const-class v3, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v3}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/mfluent/asp/util/y;->a()V

    .line 288
    :cond_0
    :goto_1
    if-ltz v0, :cond_7

    .line 289
    iget-object v1, p0, Lcom/mfluent/asp/ContentAggregatorService;->g:Lcom/mfluent/asp/ContentAggregatorService$b;

    iget-object v3, p0, Lcom/mfluent/asp/ContentAggregatorService;->g:Lcom/mfluent/asp/ContentAggregatorService$b;

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/ContentAggregatorService$b;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/ContentAggregatorService$b;->sendMessage(Landroid/os/Message;)Z

    .line 294
    :goto_2
    sget-boolean v0, Lcom/mfluent/asp/ContentAggregatorService;->c:Z

    if-eqz v0, :cond_8

    move v0, v2

    .line 297
    :goto_3
    return v0

    .line 235
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 257
    :cond_2
    sget-object v0, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    const-string v1, "::onStartCommand: quitting {} because user has not logged in yet"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 258
    invoke-virtual {p0}, Lcom/mfluent/asp/ContentAggregatorService;->stopSelf()V

    move v0, v2

    .line 259
    goto :goto_3

    .line 268
    :cond_3
    const-string v3, "com.samsung.android.sdk.samsunglink.ContentAggregatorService.PROMOTION_CHECK"

    invoke-static {v1, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 269
    const/4 v0, 0x3

    goto :goto_1

    .line 272
    :cond_4
    const-string v3, "com.samsung.android.sdk.samsunglink.ContentAggregatorService.NOTIFICATION_CHECK"

    invoke-static {v1, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v2

    .line 274
    goto :goto_1

    .line 278
    :cond_5
    const-string v3, "com.samsung.android.sdk.samsunglink.ContentAggregatorService.DEVICE_CHECK"

    invoke-static {v1, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 280
    const/4 v0, 0x4

    goto :goto_1

    .line 283
    :cond_6
    const-string v3, "com.samsung.android.sdk.samsunglink.ContentAggregatorService.SAMSUNG_HUB_CHECK"

    invoke-static {v1, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 285
    const/4 v0, 0x5

    goto :goto_1

    .line 291
    :cond_7
    iget-object v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->g:Lcom/mfluent/asp/ContentAggregatorService$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/ContentAggregatorService$b;->a()V

    goto :goto_2

    .line 297
    :cond_8
    const/4 v0, 0x1

    goto :goto_3
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 161
    sget-object v0, Lcom/mfluent/asp/ContentAggregatorService;->b:Lorg/slf4j/Logger;

    const-string v1, "onUnbind"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->h:Z

    .line 163
    iget-object v0, p0, Lcom/mfluent/asp/ContentAggregatorService;->g:Lcom/mfluent/asp/ContentAggregatorService$b;

    invoke-virtual {v0}, Lcom/mfluent/asp/ContentAggregatorService$b;->a()V

    .line 164
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
