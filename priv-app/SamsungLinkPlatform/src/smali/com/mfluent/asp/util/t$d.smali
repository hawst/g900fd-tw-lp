.class public final Lcom/mfluent/asp/util/t$d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/util/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation


# static fields
.field private static final c:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field protected final a:I

.field protected final b:I

.field private d:I

.field private final e:Lcom/mfluent/asp/util/t;

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/mfluent/asp/util/t$d;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private constructor <init>(Lcom/mfluent/asp/util/t;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/util/t$d;->d:I

    .line 65
    iput-object p1, p0, Lcom/mfluent/asp/util/t$d;->e:Lcom/mfluent/asp/util/t;

    .line 66
    sget-object v0, Lcom/mfluent/asp/util/t$d;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/util/t$d;->b:I

    .line 67
    iput-object p2, p0, Lcom/mfluent/asp/util/t$d;->f:Ljava/lang/String;

    .line 68
    iput p3, p0, Lcom/mfluent/asp/util/t$d;->a:I

    .line 69
    return-void
.end method

.method synthetic constructor <init>(Lcom/mfluent/asp/util/t;Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/mfluent/asp/util/t$d;-><init>(Lcom/mfluent/asp/util/t;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/mfluent/asp/util/t$d;->d:I

    if-lez v0, :cond_0

    .line 73
    iget v0, p0, Lcom/mfluent/asp/util/t$d;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/util/t$d;->d:I

    .line 74
    iget v0, p0, Lcom/mfluent/asp/util/t$d;->d:I

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/mfluent/asp/util/t$d;->e:Lcom/mfluent/asp/util/t;

    invoke-static {v0, p0}, Lcom/mfluent/asp/util/t;->a(Lcom/mfluent/asp/util/t;Lcom/mfluent/asp/util/t$d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :cond_0
    monitor-exit p0

    return-void

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final finalize()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 82
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 84
    iget v0, p0, Lcom/mfluent/asp/util/t$d;->d:I

    if-lez v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/mfluent/asp/util/t$d;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 86
    invoke-static {}, Lcom/mfluent/asp/util/t;->c()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Finalizing ProcessorLock that was not released! : {}"

    iget-object v2, p0, Lcom/mfluent/asp/util/t$d;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 88
    :cond_0
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/mfluent/asp/util/t$c;

    iget-object v2, p0, Lcom/mfluent/asp/util/t$d;->e:Lcom/mfluent/asp/util/t;

    iget v3, p0, Lcom/mfluent/asp/util/t$d;->b:I

    iget v4, p0, Lcom/mfluent/asp/util/t$d;->a:I

    invoke-direct {v1, v2, v3, v4}, Lcom/mfluent/asp/util/t$c;-><init>(Lcom/mfluent/asp/util/t;II)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/util/t$d;->d:I

    .line 91
    :cond_1
    return-void
.end method
