.class public final Lcom/mfluent/asp/util/m;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:Ljava/io/InputStream;

.field private final c:J

.field private final d:Lpcloud/net/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_HTTPSERVER:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/util/m;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;J)V
    .locals 2

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/mfluent/asp/util/m;-><init>(Ljava/io/InputStream;JLpcloud/net/a;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;JLpcloud/net/a;)V
    .locals 4

    .prologue
    .line 69
    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    .line 70
    if-nez p1, :cond_0

    .line 71
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Source input stream may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/util/m;->b:Ljava/io/InputStream;

    .line 74
    iput-wide p2, p0, Lcom/mfluent/asp/util/m;->c:J

    .line 75
    sget-object v0, Lcom/mfluent/asp/util/m;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_1

    .line 76
    const-string v0, "mfl_InputStreamEntityFixed"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InputStreamEntityFixed created; length = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_1
    iput-object p4, p0, Lcom/mfluent/asp/util/m;->d:Lpcloud/net/a;

    .line 79
    return-void
.end method


# virtual methods
.method public final consumeContent()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/mfluent/asp/util/m;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 195
    return-void
.end method

.method public final getContent()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/mfluent/asp/util/m;->b:Ljava/io/InputStream;

    return-object v0
.end method

.method public final getContentLength()J
    .locals 2

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/mfluent/asp/util/m;->c:J

    return-wide v0
.end method

.method public final isRepeatable()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x1

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, -0x1

    const/4 v9, 0x4

    const/4 v1, 0x0

    const/4 v8, 0x3

    .line 98
    if-nez p1, :cond_0

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Output stream may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    sget-object v0, Lcom/mfluent/asp/util/m;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_1

    .line 102
    const-string v0, "mfl_InputStreamEntityFixed"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "copying "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/mfluent/asp/util/m;->c:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " byte(s)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 107
    iget-object v4, p0, Lcom/mfluent/asp/util/m;->b:Ljava/io/InputStream;

    .line 108
    iget-wide v2, p0, Lcom/mfluent/asp/util/m;->c:J

    .line 112
    const/16 v0, 0x800

    :try_start_0
    new-array v0, v0, [B

    .line 115
    iget-wide v6, p0, Lcom/mfluent/asp/util/m;->c:J

    cmp-long v5, v6, v12

    if-gez v5, :cond_6

    .line 117
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-eq v2, v10, :cond_7

    .line 118
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    :try_start_1
    instance-of v2, v0, Ljava/net/SocketException;

    if-eqz v2, :cond_a

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ECONNRESET"

    invoke-static {v2, v3}, Lorg/apache/commons/lang3/StringUtils;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 144
    sget-object v2, Lcom/mfluent/asp/util/m;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v8, :cond_2

    .line 145
    const-string v2, "mfl_InputStreamEntityFixed"

    const-string v3, "::writeTo: ignoring ECONNRESET"

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :cond_2
    sget-object v0, Lcom/mfluent/asp/util/m;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v9, :cond_3

    .line 152
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 153
    :cond_3
    if-nez v1, :cond_5

    iget-object v0, p0, Lcom/mfluent/asp/util/m;->d:Lpcloud/net/a;

    if-eqz v0, :cond_5

    .line 164
    sget-object v0, Lcom/mfluent/asp/util/m;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_4

    .line 165
    const-string v0, "mfl_InputStreamEntityFixed"

    const-string v1, "::writeTo: invalidating NTS socket"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/util/m;->d:Lpcloud/net/a;

    invoke-virtual {v0}, Lpcloud/net/a;->b()V

    .line 176
    :cond_5
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 177
    :goto_1
    return-void

    .line 123
    :cond_6
    :goto_2
    cmp-long v5, v2, v12

    if-lez v5, :cond_7

    .line 124
    const/4 v5, 0x0

    const-wide/16 v6, 0x800

    :try_start_2
    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {v4, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    .line 125
    if-eq v5, v10, :cond_7

    .line 126
    if-eqz v5, :cond_6

    .line 128
    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6, v5}, Ljava/io/OutputStream;->write([BII)V

    .line 131
    int-to-long v6, v5

    sub-long/2addr v2, v6

    .line 132
    goto :goto_2

    .line 136
    :cond_7
    const/4 v1, 0x1

    .line 137
    sget-object v0, Lcom/mfluent/asp/util/m;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_8

    .line 138
    const-string v0, "mfl_InputStreamEntityFixed"

    const-string v2, "normal termination of copying loop"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 151
    :cond_8
    sget-object v0, Lcom/mfluent/asp/util/m;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v9, :cond_9

    .line 152
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 153
    :cond_9
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto :goto_1

    .line 148
    :cond_a
    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 151
    :catchall_0
    move-exception v0

    sget-object v2, Lcom/mfluent/asp/util/m;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v9, :cond_b

    .line 152
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 153
    :cond_b
    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/mfluent/asp/util/m;->d:Lpcloud/net/a;

    if-eqz v1, :cond_d

    .line 164
    sget-object v1, Lcom/mfluent/asp/util/m;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_c

    .line 165
    const-string v1, "mfl_InputStreamEntityFixed"

    const-string v2, "::writeTo: invalidating NTS socket"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_c
    iget-object v1, p0, Lcom/mfluent/asp/util/m;->d:Lpcloud/net/a;

    invoke-virtual {v1}, Lpcloud/net/a;->b()V

    .line 176
    :cond_d
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    throw v0
.end method
