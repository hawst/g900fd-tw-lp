.class public final Lcom/mfluent/asp/util/bitmap/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CACHE:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/util/bitmap/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method private static a(Landroid/graphics/BitmapFactory$Options;IIII)I
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 230
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 231
    iget v3, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 234
    if-gt v2, p2, :cond_0

    if-le v3, p1, :cond_3

    .line 235
    :cond_0
    if-le v3, v2, :cond_1

    .line 236
    int-to-float v0, v2

    int-to-float v4, p2

    div-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 248
    :goto_0
    mul-int v4, v3, v2

    int-to-float v4, v4

    .line 252
    mul-int v5, p1, p2

    mul-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    .line 254
    :goto_1
    mul-int v6, v0, v0

    int-to-float v6, v6

    div-float v6, v4, v6

    cmpl-float v6, v6, v5

    if-lez v6, :cond_2

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 238
    :cond_1
    int-to-float v0, v3

    int-to-float v4, p1

    div-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 263
    :cond_2
    if-lez p3, :cond_4

    if-lez p4, :cond_4

    .line 264
    int-to-double v4, v3

    int-to-double v6, p3

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 265
    int-to-double v4, v2

    int-to-double v6, p4

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 267
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 269
    if-le v2, v0, :cond_4

    move v0, v1

    .line 276
    :goto_2
    if-ge v0, v2, :cond_4

    .line 277
    mul-int/lit8 v0, v0, 0x2

    goto :goto_2

    :cond_3
    move v0, v1

    .line 283
    :cond_4
    return v0
.end method

.method public static a(Landroid/graphics/Bitmap;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v4, 0x3

    .line 343
    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapWidth()I

    move-result v0

    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 344
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 347
    if-ge v0, v1, :cond_1

    .line 348
    int-to-float v2, v0

    int-to-float v3, v1

    div-float/2addr v2, v3

    .line 352
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-ne v1, v3, :cond_2

    .line 354
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 357
    :goto_0
    if-lez v0, :cond_0

    if-gtz v1, :cond_3

    .line 362
    :cond_0
    sget-object v0, Lcom/mfluent/asp/util/bitmap/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 363
    const-string v0, "mfl_ImageResizer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::resampleBitmap: desired height/width not set! "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_1
    :goto_1
    return-object p0

    .line 356
    :cond_2
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_0

    .line 369
    :cond_3
    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 370
    if-eqz v0, :cond_1

    .line 371
    :try_start_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-object p0, v0

    .line 372
    goto :goto_1

    .line 376
    :catch_0
    move-exception v0

    .line 377
    :goto_2
    sget-object v1, Lcom/mfluent/asp/util/bitmap/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v4, :cond_1

    .line 378
    const-string v1, "mfl_ImageResizer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::resampleBitmap OutOfMemoryError request:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 376
    :catch_1
    move-exception v1

    move-object p0, v0

    move-object v0, v1

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/graphics/BitmapFactory$Options;Landroid/graphics/Point;)Landroid/graphics/Bitmap;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 92
    sget-object v2, Lcom/mfluent/asp/util/bitmap/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 93
    const-string v2, "mfl_ImageResizer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::decodeSampledBitmapFromFile:decoding bitmap from file "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for imageInfo "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapHeight()I

    move-result v5

    .line 97
    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapWidth()I

    move-result v6

    .line 104
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    const/4 v3, 0x1

    :try_start_1
    iput-boolean v3, p2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 109
    const/4 v3, 0x0

    invoke-static {v2, v3, p2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 111
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 126
    if-eqz p3, :cond_5

    .line 127
    iget v3, p3, Landroid/graphics/Point;->x:I

    .line 128
    iget v1, p3, Landroid/graphics/Point;->y:I

    move v4, v3

    .line 132
    :goto_0
    invoke-static {p2, v6, v5, v4, v1}, Lcom/mfluent/asp/util/bitmap/b;->a(Landroid/graphics/BitmapFactory$Options;IIII)I

    move-result v3

    iput v3, p2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 135
    const/4 v3, 0x0

    iput-boolean v3, p2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 141
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 144
    const/4 v2, 0x0

    :try_start_2
    invoke-static {v3, v2, p2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v0

    .line 179
    :goto_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 190
    :goto_2
    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbnailSize()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->FULL_SCREEN:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    if-ne v2, v3, :cond_3

    if-eqz v0, :cond_3

    if-lez v4, :cond_3

    if-lez v1, :cond_3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-gt v2, v4, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v2, v1, :cond_3

    .line 194
    :cond_1
    sget-object v2, Lcom/mfluent/asp/util/bitmap/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 195
    const-string v2, "mfl_ImageResizer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "bitmap too large! img: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "x"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", max: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "x"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_2
    new-instance v2, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v2, p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    .line 198
    invoke-virtual {v2, v4}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setDesiredBitmapWidth(I)V

    .line 199
    invoke-virtual {v2, v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setDesiredBitmapHeight(I)V

    .line 200
    invoke-static {v0, v2}, Lcom/mfluent/asp/util/bitmap/b;->a(Landroid/graphics/Bitmap;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 203
    :cond_3
    :goto_3
    return-object v0

    .line 146
    :catch_0
    move-exception v2

    :try_start_4
    const-string v2, "mfl_ImageResizer"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "OutOfMemoryError: file: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; reqWidth: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; reqHeight: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", sample: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 157
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 158
    :try_start_5
    iget v3, p2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    mul-int/lit8 v3, v3, 0x2

    .line 159
    const-string v7, "mfl_ImageResizer"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "OutOfMemoryError: retry sample: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "->"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", file: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iput v3, p2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 162
    const/4 v3, 0x0

    :try_start_6
    invoke-static {v2, v3, p2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v0

    move-object v3, v2

    .line 173
    goto/16 :goto_1

    .line 163
    :catch_1
    move-exception v1

    .line 164
    :try_start_7
    const-string v3, "mfl_ImageResizer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "OutOfMemoryError: file2: "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "; reqWidth: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "; reqHeight: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", sample: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    throw v1
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 175
    :catch_2
    move-exception v1

    .line 176
    :goto_4
    :try_start_8
    const-string v3, "mfl_ImageResizer"

    const-string v4, "Error when decoding bitmap"

    invoke-static {v3, v4, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 177
    if-eqz v2, :cond_3

    .line 181
    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_3

    :catch_3
    move-exception v1

    goto/16 :goto_3

    .line 179
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_5
    if-eqz v2, :cond_4

    .line 181
    :try_start_a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    .line 183
    :cond_4
    :goto_6
    throw v0

    :catch_4
    move-exception v2

    goto/16 :goto_2

    :catch_5
    move-exception v1

    goto :goto_6

    .line 179
    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_5

    .line 175
    :catch_6
    move-exception v1

    move-object v2, v0

    goto :goto_4

    :catch_7
    move-exception v1

    move-object v2, v3

    goto :goto_4

    :cond_5
    move v4, v1

    goto/16 :goto_0
.end method
