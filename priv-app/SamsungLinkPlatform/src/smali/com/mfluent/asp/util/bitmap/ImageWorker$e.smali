.class abstract Lcom/mfluent/asp/util/bitmap/ImageWorker$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/util/bitmap/ImageWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "e"
.end annotation


# instance fields
.field private a:Lcom/mfluent/asp/util/bitmap/a;

.field protected final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/mfluent/asp/util/bitmap/ImageWorker$a;",
            ">;"
        }
    .end annotation
.end field

.field protected final c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

.field protected final d:Landroid/os/Handler;

.field protected final e:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

.field final synthetic f:Lcom/mfluent/asp/util/bitmap/ImageWorker;

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/os/Handler;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V
    .locals 1

    .prologue
    .line 469
    iput-object p1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->f:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->g:Z

    .line 470
    new-instance v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v0, p3}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    iput-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    .line 471
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->b:Ljava/lang/ref/WeakReference;

    .line 472
    iput-object p4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->d:Landroid/os/Handler;

    .line 473
    iput-object p5, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->e:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    .line 474
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/util/bitmap/ImageWorker$e;)Lcom/mfluent/asp/util/bitmap/a;
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->a:Lcom/mfluent/asp/util/bitmap/a;

    return-object v0
.end method


# virtual methods
.method protected abstract a()Lcom/mfluent/asp/util/bitmap/a;
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 502
    iput p1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->h:I

    .line 503
    return-void
.end method

.method protected a(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)V
    .locals 0

    .prologue
    .line 638
    invoke-interface {p1}, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;->a()V

    .line 640
    return-void
.end method

.method protected a(Lcom/mfluent/asp/util/bitmap/a;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 559
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;

    .line 560
    if-eqz p1, :cond_0

    .line 561
    invoke-virtual {p1}, Lcom/mfluent/asp/util/bitmap/a;->c()V

    .line 564
    :cond_0
    if-nez v0, :cond_3

    .line 565
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v3, :cond_1

    .line 566
    const-string v0, "mfl_ImageWorker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onPostExecute client reference has gone away! imageInfo:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 616
    :cond_1
    if-eqz p1, :cond_2

    .line 617
    invoke-virtual {p1}, Lcom/mfluent/asp/util/bitmap/a;->d()V

    .line 620
    :cond_2
    :goto_0
    return-void

    .line 572
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 573
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v3, :cond_4

    .line 574
    const-string v0, "mfl_ImageWorker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onPostExecute I have been cancelled! imageInfo:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 616
    :cond_4
    if-eqz p1, :cond_2

    .line 617
    invoke-virtual {p1}, Lcom/mfluent/asp/util/bitmap/a;->d()V

    goto :goto_0

    .line 580
    :cond_5
    const/high16 v1, 0x7f090000

    :try_start_2
    invoke-interface {v0, v1}, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 581
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eq v1, p0, :cond_8

    .line 582
    :cond_6
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v3, :cond_7

    .line 583
    const-string v0, "mfl_ImageWorker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::onPostExecute client\'s tag does not match me! imageInfo:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 616
    :cond_7
    if-eqz p1, :cond_2

    .line 617
    invoke-virtual {p1}, Lcom/mfluent/asp/util/bitmap/a;->d()V

    goto :goto_0

    .line 589
    :cond_8
    if-nez p1, :cond_a

    .line 590
    :try_start_3
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->e:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    sget-object v2, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->d:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    if-eq v1, v2, :cond_9

    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v3, :cond_9

    .line 591
    const-string v1, "mfl_ImageWorker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::onPostExecute failed to get Bitmap. imageInfo:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    :cond_9
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 616
    if-eqz p1, :cond_2

    .line 617
    invoke-virtual {p1}, Lcom/mfluent/asp/util/bitmap/a;->d()V

    goto/16 :goto_0

    .line 614
    :cond_a
    :try_start_4
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->f:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-static {v0, p1}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/util/bitmap/a;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 616
    if-eqz p1, :cond_2

    .line 617
    invoke-virtual {p1}, Lcom/mfluent/asp/util/bitmap/a;->d()V

    goto/16 :goto_0

    .line 616
    :catchall_0
    move-exception v0

    if-eqz p1, :cond_b

    .line 617
    invoke-virtual {p1}, Lcom/mfluent/asp/util/bitmap/a;->d()V

    :cond_b
    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 480
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->g:Z

    if-nez v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 482
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    :cond_0
    monitor-exit p0

    return-void

    .line 480
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 492
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 511
    iget v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->h:I

    return v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 528
    invoke-virtual {p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->a()Lcom/mfluent/asp/util/bitmap/a;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->a:Lcom/mfluent/asp/util/bitmap/a;

    .line 529
    new-instance v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e$1;-><init>(Lcom/mfluent/asp/util/bitmap/ImageWorker$e;)V

    .line 537
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 538
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 542
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " imageInfo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
