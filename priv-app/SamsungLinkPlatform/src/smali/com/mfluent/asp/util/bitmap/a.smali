.class public Lcom/mfluent/asp/util/bitmap/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private final b:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

.field private final c:Landroid/graphics/Bitmap;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/mfluent/asp/util/bitmap/a;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/util/bitmap/a;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v0, p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    iput-object v0, p0, Lcom/mfluent/asp/util/bitmap/a;->b:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    .line 26
    iput-object p2, p0, Lcom/mfluent/asp/util/bitmap/a;->c:Landroid/graphics/Bitmap;

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/util/bitmap/a;->d:I

    .line 28
    return-void
.end method


# virtual methods
.method public final a()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/a;->b:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    return-object v0
.end method

.method public final b()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/a;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 39
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/a;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    iget v0, p0, Lcom/mfluent/asp/util/bitmap/a;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/util/bitmap/a;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    :cond_0
    monitor-exit p0

    return-void

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 4

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/mfluent/asp/util/bitmap/a;->d:I

    if-lez v0, :cond_0

    .line 46
    iget v0, p0, Lcom/mfluent/asp/util/bitmap/a;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/util/bitmap/a;->d:I

    .line 47
    iget v0, p0, Lcom/mfluent/asp/util/bitmap/a;->d:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/a;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    sget-object v0, Lcom/mfluent/asp/util/bitmap/a;->a:Lorg/slf4j/Logger;

    const-string v1, "Recycling bitmap. ImageInfo: {} size: {} bytes."

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/a;->b:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    iget-object v3, p0, Lcom/mfluent/asp/util/bitmap/a;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 49
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/a;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :cond_0
    monitor-exit p0

    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 4

    .prologue
    .line 58
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/mfluent/asp/util/bitmap/a;->d:I

    if-lez v0, :cond_0

    .line 59
    iget v0, p0, Lcom/mfluent/asp/util/bitmap/a;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/util/bitmap/a;->d:I

    .line 60
    sget-object v0, Lcom/mfluent/asp/util/bitmap/a;->a:Lorg/slf4j/Logger;

    const-string v1, "Released in finalizer. No recycle. ImageInfo: {} refCount: {}."

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/a;->b:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    iget v3, p0, Lcom/mfluent/asp/util/bitmap/a;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    :cond_0
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
