.class public final Lcom/mfluent/asp/util/p;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a()Z
    .locals 2

    .prologue
    .line 25
    const/4 v0, 0x0

    .line 28
    :try_start_0
    invoke-static {}, Landroid/os/UserManager;->supportsMultipleUsers()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-eqz v1, :cond_0

    .line 29
    const/4 v0, 0x1

    .line 37
    :cond_0
    :goto_0
    return v0

    .line 35
    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 15
    invoke-static {}, Lcom/mfluent/asp/util/p;->a()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/mfluent/asp/util/p;->b(Landroid/content/Context;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 16
    const/4 v0, 0x0

    .line 20
    :cond_0
    return v0
.end method

.method private static b(Landroid/content/Context;)I
    .locals 5

    .prologue
    .line 41
    const/4 v1, -0x1

    .line 44
    :try_start_0
    const-string v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 46
    const-class v2, Landroid/os/UserManager;

    const-string v3, "getUserHandle"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 48
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 56
    :goto_0
    return v0

    .line 54
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0
.end method
