.class public Lcom/mfluent/asp/util/t;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/util/t$a;,
        Lcom/mfluent/asp/util/t$b;,
        Lcom/mfluent/asp/util/t$c;,
        Lcom/mfluent/asp/util/t$d;
    }
.end annotation


# static fields
.field public static a:Z

.field private static b:Lcom/mfluent/asp/util/t;

.field private static final c:Lorg/slf4j/Logger;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Landroid/os/PowerManager$WakeLock;

.field private final f:Landroid/net/wifi/WifiManager$WifiLock;

.field private final g:Landroid/app/PendingIntent;

.field private final h:Lcom/mfluent/asp/common/util/IntVector;

.field private final i:Lcom/mfluent/asp/common/util/IntVector;

.field private final j:Lcom/mfluent/asp/common/util/IntVector;

.field private k:Z

.field private l:J

.field private m:J

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/mfluent/asp/util/t;->b:Lcom/mfluent/asp/util/t;

    .line 29
    const-class v0, Lcom/mfluent/asp/util/t;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/util/t;->a:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x5

    const/4 v2, 0x0

    const/16 v1, 0x14

    const/4 v3, 0x1

    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    new-instance v0, Lcom/mfluent/asp/common/util/IntVector;

    invoke-direct {v0, v4, v4, v3, v3}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZZ)V

    iput-object v0, p0, Lcom/mfluent/asp/util/t;->h:Lcom/mfluent/asp/common/util/IntVector;

    .line 163
    new-instance v0, Lcom/mfluent/asp/common/util/IntVector;

    invoke-direct {v0, v1, v1, v3, v3}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZZ)V

    iput-object v0, p0, Lcom/mfluent/asp/util/t;->i:Lcom/mfluent/asp/common/util/IntVector;

    .line 164
    new-instance v0, Lcom/mfluent/asp/common/util/IntVector;

    invoke-direct {v0, v1, v1, v3, v3}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZZ)V

    iput-object v0, p0, Lcom/mfluent/asp/util/t;->j:Lcom/mfluent/asp/common/util/IntVector;

    .line 165
    iput-boolean v2, p0, Lcom/mfluent/asp/util/t;->k:Z

    .line 168
    iput-wide v6, p0, Lcom/mfluent/asp/util/t;->l:J

    .line 169
    iput-wide v6, p0, Lcom/mfluent/asp/util/t;->m:J

    .line 170
    iput v2, p0, Lcom/mfluent/asp/util/t;->n:I

    .line 198
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/util/t;->d:Landroid/content/Context;

    .line 199
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 200
    const-string v1, "com.mfluent.asp.util.ProcessorManager"

    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/util/t;->e:Landroid/os/PowerManager$WakeLock;

    .line 202
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 203
    const/4 v1, 0x3

    const-string v2, "com.mfluent.asp.util.ProcessorManager"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/util/t;->f:Landroid/net/wifi/WifiManager$WifiLock;

    .line 205
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 206
    const-string v1, "com.mfluent.asp.util.ProcessorManager.INTENT_ALARM_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    const-class v1, Lcom/mfluent/asp/util/ProcessorManagerAlarmReceiver;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 208
    iget-object v1, p0, Lcom/mfluent/asp/util/t;->d:Landroid/content/Context;

    const/high16 v2, 0x10000000

    invoke-static {v1, v3, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/util/t;->g:Landroid/app/PendingIntent;

    .line 210
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->d:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 211
    iget-object v1, p0, Lcom/mfluent/asp/util/t;->g:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 212
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/mfluent/asp/util/t;
    .locals 2

    .prologue
    .line 42
    const-class v1, Lcom/mfluent/asp/util/t;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 43
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 46
    :cond_0
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/util/t;->b:Lcom/mfluent/asp/util/t;

    if-nez v0, :cond_1

    .line 47
    new-instance v0, Lcom/mfluent/asp/util/t;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/util/t;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/util/t;->b:Lcom/mfluent/asp/util/t;

    .line 50
    :cond_1
    sget-object v0, Lcom/mfluent/asp/util/t;->b:Lcom/mfluent/asp/util/t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method private declared-synchronized a(II)V
    .locals 4

    .prologue
    .line 255
    monitor-enter p0

    .line 257
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 258
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->i:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/common/util/IntVector;->find(I)I

    move-result v0

    .line 259
    if-ltz v0, :cond_2

    .line 260
    iget-object v1, p0, Lcom/mfluent/asp/util/t;->i:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/common/util/IntVector;->removeElementAt(I)Z

    .line 262
    sget-object v0, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    const-string v1, "::releaseProcessorLock id:{} cpu locks left:{}"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/util/t;->i:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 264
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 265
    invoke-direct {p0}, Lcom/mfluent/asp/util/t;->d()V

    .line 273
    :cond_0
    :goto_0
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->j:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/common/util/IntVector;->find(I)I

    move-result v0

    .line 275
    if-ltz v0, :cond_3

    .line 276
    iget-object v1, p0, Lcom/mfluent/asp/util/t;->j:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/common/util/IntVector;->removeElementAt(I)Z

    .line 278
    sget-object v0, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    const-string v1, "::releaseProcessorLock id:{} wifi locks left:{}"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/util/t;->j:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 280
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->f:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 269
    :cond_2
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    const-string v1, "::releaseProcessorLock cpu lock not found! id:{}"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 255
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 282
    :cond_3
    :try_start_2
    sget-object v0, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    const-string v1, "::releaseProcessorLock wifi lock not found! id:{}"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method static synthetic a(Lcom/mfluent/asp/util/t;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/mfluent/asp/util/t;->f()V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/util/t;II)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/util/t;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/util/t;Lcom/mfluent/asp/util/t$d;)V
    .locals 2

    .prologue
    .line 26
    iget v0, p1, Lcom/mfluent/asp/util/t$d;->b:I

    iget v1, p1, Lcom/mfluent/asp/util/t$d;->a:I

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/util/t;->a(II)V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/util/t;I)Z
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/mfluent/asp/util/t;->b(I)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized b(I)Z
    .locals 2

    .prologue
    .line 345
    monitor-enter p0

    const/4 v0, 0x0

    .line 346
    :try_start_0
    iget-object v1, p0, Lcom/mfluent/asp/util/t;->h:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v1, p1}, Lcom/mfluent/asp/common/util/IntVector;->find(I)I

    move-result v1

    .line 347
    if-ltz v1, :cond_0

    .line 348
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->h:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/IntVector;->removeElementAt(I)Z

    .line 349
    invoke-direct {p0}, Lcom/mfluent/asp/util/t;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 350
    const/4 v0, 0x1

    .line 353
    :cond_0
    monitor-exit p0

    return v0

    .line 345
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    return-object v0
.end method

.method private d()V
    .locals 8

    .prologue
    const-wide/16 v4, 0x3a98

    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 312
    iget-boolean v0, p0, Lcom/mfluent/asp/util/t;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/util/t;->i:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/util/t;->h:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v0, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    if-nez v0, :cond_1

    move v0, v7

    :goto_0
    if-eqz v0, :cond_0

    .line 313
    sget-object v0, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    const-string v1, "Beginning timer. CPU locks held:{} Tokens held:{}"

    iget-object v2, p0, Lcom/mfluent/asp/util/t;->i:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/util/t;->h:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 314
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->d:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 315
    const/4 v1, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, v4

    iget-object v6, p0, Lcom/mfluent/asp/util/t;->g:Landroid/app/PendingIntent;

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 320
    iput-boolean v7, p0, Lcom/mfluent/asp/util/t;->k:Z

    .line 322
    :cond_0
    return-void

    .line 312
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/mfluent/asp/ASPApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "level"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "scale"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "plugged"

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    if-ltz v1, :cond_2

    if-lez v2, :cond_2

    mul-int/lit8 v0, v1, 0x64

    div-int/2addr v0, v2

    const/16 v1, 0x32

    if-ge v0, v1, :cond_2

    sget-object v1, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::isBatteryStatusGood can\'t start wakelock due to battery level = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "%%"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v7

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 326
    iget-boolean v0, p0, Lcom/mfluent/asp/util/t;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/util/t;->i:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/util/t;->h:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 327
    :cond_0
    sget-object v0, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    const-string v1, "Ending timer. CPU locks held:{} Tokens held:{}"

    iget-object v2, p0, Lcom/mfluent/asp/util/t;->i:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/util/t;->h:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 328
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->d:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 329
    iget-object v1, p0, Lcom/mfluent/asp/util/t;->g:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 330
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/util/t;->k:Z

    .line 332
    :cond_1
    return-void
.end method

.method private declared-synchronized f()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 402
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->h:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 403
    sget-object v0, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    const-string v1, "Giving it a chance to run for {} ms."

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 404
    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 411
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 412
    monitor-exit p0

    return-void

    .line 407
    :cond_0
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/mfluent/asp/util/t;->k:Z

    .line 408
    invoke-direct {p0}, Lcom/mfluent/asp/util/t;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 411
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/mfluent/asp/util/t;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 412
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 402
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(I)Lcom/mfluent/asp/util/t$d;
    .locals 5

    .prologue
    .line 215
    monitor-enter p0

    const/4 v0, 0x0

    .line 216
    :try_start_0
    sget-boolean v1, Lcom/mfluent/asp/util/t;->a:Z

    if-eqz v1, :cond_0

    .line 217
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    .line 218
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 219
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 222
    :try_start_1
    invoke-virtual {v0, v2}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 223
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 225
    :try_start_2
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    .line 231
    :cond_0
    new-instance v1, Lcom/mfluent/asp/util/t$d;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, p1, v2}, Lcom/mfluent/asp/util/t$d;-><init>(Lcom/mfluent/asp/util/t;Ljava/lang/String;IB)V

    .line 232
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 234
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->i:Lcom/mfluent/asp/common/util/IntVector;

    iget v2, v1, Lcom/mfluent/asp/util/t$d;->b:I

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/common/util/IntVector;->insert(I)I

    .line 236
    sget-object v0, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    const-string v2, "::createLock id:{} cpu locks held:{}"

    iget v3, v1, Lcom/mfluent/asp/util/t$d;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/mfluent/asp/util/t;->i:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 238
    invoke-direct {p0}, Lcom/mfluent/asp/util/t;->e()V

    .line 241
    :cond_1
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->f:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 243
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->j:Lcom/mfluent/asp/common/util/IntVector;

    iget v2, v1, Lcom/mfluent/asp/util/t$d;->b:I

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/common/util/IntVector;->insert(I)I

    .line 244
    sget-object v0, Lcom/mfluent/asp/util/t;->c:Lorg/slf4j/Logger;

    const-string v2, "::createLock id:{} wifi locks held:{}"

    iget v3, v1, Lcom/mfluent/asp/util/t$d;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/mfluent/asp/util/t;->j:Lcom/mfluent/asp/common/util/IntVector;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 247
    :cond_2
    monitor-exit p0

    return-object v1

    .line 225
    :catchall_0
    move-exception v0

    .line 226
    :try_start_3
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 215
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 336
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/mfluent/asp/util/t$a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/util/t$a;-><init>(Lcom/mfluent/asp/util/t;)V

    .line 337
    iget-object v1, p0, Lcom/mfluent/asp/util/t;->h:Lcom/mfluent/asp/common/util/IntVector;

    iget v2, v0, Lcom/mfluent/asp/util/t$a;->a:I

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/IntVector;->insert(I)I

    .line 339
    invoke-direct {p0}, Lcom/mfluent/asp/util/t;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    monitor-exit p0

    return-object v0

    .line 336
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 357
    instance-of v0, p1, Lcom/mfluent/asp/util/t$a;

    if-eqz v0, :cond_0

    .line 358
    check-cast p1, Lcom/mfluent/asp/util/t$a;

    .line 360
    iget v0, p1, Lcom/mfluent/asp/util/t$a;->a:I

    invoke-direct {p0, v0}, Lcom/mfluent/asp/util/t;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {p1}, Lcom/mfluent/asp/util/t$a;->a()V

    .line 364
    :cond_0
    return-void
.end method

.method final declared-synchronized b()V
    .locals 2

    .prologue
    .line 368
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/util/t;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 369
    new-instance v0, Lcom/mfluent/asp/util/t$1;

    const-string v1, "com.mfluent.asp.util.ProcessorManager.AlarmReceiver"

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/util/t$1;-><init>(Lcom/mfluent/asp/util/t;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/util/t$1;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    monitor-exit p0

    return-void

    .line 368
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
