.class final Lcom/mfluent/asp/util/bitmap/ImageWorker$b;
.super Lcom/mfluent/asp/util/bitmap/ImageWorker$e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/util/bitmap/ImageWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/os/Handler;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V
    .locals 0

    .prologue
    .line 665
    iput-object p1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    .line 666
    invoke-direct/range {p0 .. p5}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;-><init>(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/os/Handler;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V

    .line 667
    return-void
.end method


# virtual methods
.method protected final a()Lcom/mfluent/asp/util/bitmap/a;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x3

    const/4 v2, 0x1

    .line 678
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/media/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/media/d;

    move-result-object v3

    .line 679
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/media/d;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Lcom/mfluent/asp/media/c/d;

    move-result-object v4

    .line 680
    if-nez v4, :cond_0

    .line 681
    iput-boolean v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->g:Z

    .line 722
    :goto_0
    return-object v1

    .line 687
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    if-nez v4, :cond_5

    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    .line 693
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_1

    .line 694
    const-string v0, "mfl_ImageWorker"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "file cache hit too small for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    :cond_1
    iput-boolean v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->g:Z

    .line 701
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->e:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    sget-object v2, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->d:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    if-eq v0, v2, :cond_8

    .line 703
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->b(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)Landroid/graphics/Point;

    move-result-object v0

    invoke-static {v4, v2, v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/media/c/d;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/graphics/Point;)Lcom/mfluent/asp/util/bitmap/a;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 704
    if-nez v0, :cond_4

    .line 705
    :try_start_1
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v7, :cond_3

    .line 706
    const-string v1, "mfl_ImageWorker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "::doInBackground:Failed to decode bitmap from file cache hit - removing from file cache "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    :cond_3
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v3, v1}, Lcom/mfluent/asp/media/d;->b(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    .line 709
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->g:Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    :cond_4
    :goto_2
    move-object v1, v0

    .line 722
    goto :goto_0

    .line 687
    :cond_5
    invoke-virtual {v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredWidth()I

    move-result v5

    invoke-virtual {v4}, Lcom/mfluent/asp/media/c/d;->a()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredWidth()I

    move-result v6

    if-gt v5, v6, :cond_6

    invoke-virtual {v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredHeight()I

    move-result v0

    invoke-virtual {v4}, Lcom/mfluent/asp/media/c/d;->a()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredHeight()I

    move-result v5

    if-le v0, v5, :cond_7

    :cond_6
    move v0, v2

    goto/16 :goto_1

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 711
    :catch_0
    move-exception v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    .line 712
    :goto_3
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v7, :cond_4

    .line 713
    const-string v2, "mfl_ImageWorker"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::doInBackground:OutOfMemoryError "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 717
    :cond_8
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v7, :cond_9

    .line 718
    const-string v0, "mfl_ImageWorker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pre-fetch file in cache. Doing remote fetch:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->g:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move-object v0, v1

    goto :goto_2

    .line 711
    :catch_1
    move-exception v1

    goto :goto_3
.end method

.method protected final a(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)V
    .locals 1

    .prologue
    .line 761
    iget-boolean v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->g:Z

    if-nez v0, :cond_0

    .line 763
    invoke-super {p0, p1}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)V

    .line 765
    :cond_0
    return-void
.end method

.method protected final a(Lcom/mfluent/asp/util/bitmap/a;)V
    .locals 5

    .prologue
    .line 734
    invoke-super {p0, p1}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->a(Lcom/mfluent/asp/util/bitmap/a;)V

    .line 736
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;

    .line 737
    if-nez v0, :cond_1

    .line 748
    :cond_0
    :goto_0
    return-void

    .line 741
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 745
    iget-boolean v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->g:Z

    if-eqz v1, :cond_0

    .line 746
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->d()I

    move-result v3

    iget-object v4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;->e:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;ILcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V

    goto :goto_0
.end method
