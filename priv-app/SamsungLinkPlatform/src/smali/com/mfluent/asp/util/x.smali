.class public final Lcom/mfluent/asp/util/x;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/util/x$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method private static a(Ljava/lang/String;IZ)Ljava/lang/String;
    .locals 3

    .prologue
    .line 214
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x64

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 215
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    if-eqz p2, :cond_1

    .line 218
    const-string v0, " IN ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_2

    .line 224
    if-lez v0, :cond_0

    .line 225
    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 227
    :cond_0
    const/16 v2, 0x3f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 220
    :cond_1
    const-string v0, " NOT IN ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 229
    :cond_2
    const/16 v0, 0x29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 231
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V
    .locals 6

    .prologue
    .line 169
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 171
    if-nez v1, :cond_0

    .line 185
    :goto_0
    return-void

    .line 176
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 184
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 181
    :cond_1
    :try_start_1
    invoke-interface {p5, v1}, Lcom/mfluent/asp/util/x$a;->a(Landroid/database/Cursor;)Z

    .line 182
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 184
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/16 v11, 0xfa

    const/16 v10, 0x27

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 82
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIds()[Ljava/lang/String;

    move-result-object v0

    .line 83
    if-nez v0, :cond_e

    .line 84
    new-array v0, v9, [Ljava/lang/String;

    move-object v7, v0

    .line 87
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 88
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIdColumnName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "_id"

    invoke-static {v0, v2}, Lorg/apache/commons/lang3/StringUtils;->defaultIfEmpty(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    .line 89
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->isFileListUri(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 90
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSortOrder()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v2, p2

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_3

    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIdColumnName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v7}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->isInclude()Z

    move-result v4

    :cond_0
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_4

    move v1, v8

    :goto_1
    if-ne v4, v1, :cond_1

    invoke-interface {p3, v2}, Lcom/mfluent/asp/util/x$a;->a(Landroid/database/Cursor;)Z

    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 146
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v1, v9

    .line 90
    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 92
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->isInclude()Z

    move-result v0

    if-nez v0, :cond_d

    .line 95
    array-length v0, v7

    if-nez v0, :cond_6

    .line 97
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSelection()Ljava/lang/String;

    move-result-object v3

    .line 98
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v4

    :goto_3
    move-object v0, p0

    move-object v2, p2

    move-object v5, p3

    .line 131
    invoke-static/range {v0 .. v5}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    goto :goto_2

    .line 100
    :cond_6
    array-length v2, v7

    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    move v0, v9

    :goto_4
    add-int/2addr v0, v2

    if-le v0, v11, :cond_c

    .line 102
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " NOT IN ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIdColumnType()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_9

    move v0, v8

    .line 105
    :goto_5
    array-length v3, v7

    if-ge v9, v3, :cond_b

    .line 106
    if-lez v9, :cond_7

    .line 107
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 109
    :cond_7
    if-eqz v0, :cond_a

    .line 110
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 111
    aget-object v3, v7, v9

    const-string v4, "\'"

    const-string v5, "\'\'"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 105
    :goto_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 100
    :cond_8
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    goto :goto_4

    :cond_9
    move v0, v9

    .line 104
    goto :goto_5

    .line 114
    :cond_a
    aget-object v3, v7, v9

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 117
    :cond_b
    const/16 v0, 0x29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSelection()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/database/DatabaseUtils;->concatenateWhere(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 120
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 123
    :cond_c
    array-length v0, v7

    invoke-static {v6, v0, v9}, Lcom/mfluent/asp/util/x;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    .line 124
    array-length v2, v7

    invoke-static {v7, v9, v2}, Lcom/mfluent/asp/util/x;->a([Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v2

    .line 126
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSelection()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/database/DatabaseUtils;->concatenateWhere(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 127
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/database/DatabaseUtils;->appendSelectionArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 134
    :cond_d
    :goto_7
    array-length v0, v7

    if-ge v9, v0, :cond_3

    .line 135
    array-length v0, v7

    sub-int/2addr v0, v9

    invoke-static {v0, v11}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 136
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->isInclude()Z

    move-result v2

    invoke-static {v6, v0, v2}, Lcom/mfluent/asp/util/x;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v2

    .line 137
    invoke-static {v7, v9, v0}, Lcom/mfluent/asp/util/x;->a([Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v0

    .line 139
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSelection()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/database/DatabaseUtils;->concatenateWhere(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 140
    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSelectionArgs()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/database/DatabaseUtils;->appendSelectionArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v2, p2

    move-object v5, p3

    .line 142
    invoke-static/range {v0 .. v5}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    .line 134
    add-int/lit16 v9, v9, 0xfa

    goto :goto_7

    :cond_e
    move-object v7, v0

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/ContentResolver;[Ljava/lang/String;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 149
    if-nez p1, :cond_0

    .line 150
    new-array p1, v0, [Ljava/lang/String;

    :cond_0
    move v6, v0

    .line 153
    :goto_0
    array-length v0, p1

    if-ge v6, v0, :cond_1

    .line 154
    array-length v0, p1

    sub-int/2addr v0, v6

    const/16 v1, 0xfa

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 155
    const-string v1, "_id"

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/mfluent/asp/util/x;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v3

    .line 156
    invoke-static {p1, v6, v0}, Lcom/mfluent/asp/util/x;->a([Ljava/lang/String;II)[Ljava/lang/String;

    move-result-object v4

    .line 158
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    .line 153
    add-int/lit16 v0, v6, 0xfa

    move v6, v0

    goto :goto_0

    .line 160
    :cond_1
    return-void
.end method

.method public static a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;)Z
    .locals 2

    .prologue
    .line 244
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "local_source_media_id"

    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIdColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->isInclude()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIds()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIds()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([Ljava/lang/String;II)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 235
    new-array v1, p2, [Ljava/lang/String;

    move v0, p1

    .line 236
    :goto_0
    add-int v2, p1, p2

    if-ge v0, v2, :cond_0

    .line 237
    sub-int v2, v0, p1

    aget-object v3, p0, v0

    aput-object v3, v1, v2

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/t;)Lcom/mfluent/asp/datamodel/Device;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 41
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->isLocalFilePathsMediaSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {p3}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    .line 46
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->isSlinkUri()Z

    move-result v0

    if-nez v0, :cond_1

    .line 48
    invoke-virtual {p3}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_1
    invoke-static {p2}, Lcom/mfluent/asp/util/x;->a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    invoke-virtual {p3}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    goto :goto_0

    .line 56
    :cond_2
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->isFileListUri(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 57
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getDeviceIdFromUri(Landroid/net/Uri;)J

    move-result-wide v0

    .line 59
    invoke-virtual {p3, v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    goto :goto_0

    .line 62
    :cond_3
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "DISTINCT device_id"

    aput-object v2, v0, v1

    .line 64
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 65
    new-instance v2, Lcom/mfluent/asp/util/x$1;

    invoke-direct {v2, p0, v1}, Lcom/mfluent/asp/util/x$1;-><init>(Lcom/mfluent/asp/util/x;Ljava/util/Set;)V

    invoke-static {p1, p2, v0, v2}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    .line 74
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 75
    const/4 v0, 0x0

    goto :goto_0

    .line 78
    :cond_4
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p3, v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    goto :goto_0
.end method
