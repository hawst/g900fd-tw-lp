.class public abstract Lcom/mfluent/asp/util/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/util/a/a/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private final b:Ljava/util/concurrent/locks/Condition;

.field private final c:Ljava/util/concurrent/locks/Condition;

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/util/a/a/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/mfluent/asp/util/a/a/a$a;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 28
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->b:Ljava/util/concurrent/locks/Condition;

    .line 29
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->c:Ljava/util/concurrent/locks/Condition;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->d:Ljava/util/ArrayList;

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    .line 32
    return-void
.end method

.method private a(Ljava/lang/Thread;)Lcom/mfluent/asp/util/a/a/a$a;
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 39
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 40
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/a/a/a$a;

    .line 41
    iget-object v3, v0, Lcom/mfluent/asp/util/a/a/a$a;->b:Ljava/lang/Thread;

    if-ne v3, p1, :cond_0

    .line 46
    :goto_1
    return-object v0

    .line 39
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 46
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 195
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/a/a/a$a;

    iget-object v3, v0, Lcom/mfluent/asp/util/a/a/a$a;->a:Ljava/lang/Object;

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 196
    :goto_1
    if-eqz v0, :cond_3

    .line 197
    iget v1, v0, Lcom/mfluent/asp/util/a/a/a$a;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/mfluent/asp/util/a/a/a$a;->c:I

    .line 199
    iget v1, v0, Lcom/mfluent/asp/util/a/a/a$a;->c:I

    if-gtz v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 201
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 213
    const/4 v0, 0x1

    return v0

    .line 195
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 206
    :cond_3
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Read token is invalid. Cannot unlock."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 87
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 88
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 90
    :try_start_0
    invoke-direct {p0, v1}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Thread;)Lcom/mfluent/asp/util/a/a/a$a;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    .line 94
    iget v1, v0, Lcom/mfluent/asp/util/a/a/a$a;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/mfluent/asp/util/a/a/a$a;->c:I

    .line 112
    :goto_0
    iget-object v0, v0, Lcom/mfluent/asp/util/a/a/a$a;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v0

    .line 97
    :cond_0
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    iget-object v0, v0, Lcom/mfluent/asp/util/a/a/a$a;->b:Ljava/lang/Thread;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v0, v1, :cond_1

    .line 99
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 102
    :catch_0
    move-exception v0

    goto :goto_1

    .line 105
    :cond_1
    :try_start_3
    new-instance v0, Lcom/mfluent/asp/util/a/a/a$a;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/mfluent/asp/util/a/a/a$a;-><init>(B)V

    .line 106
    iput-object v1, v0, Lcom/mfluent/asp/util/a/a/a$a;->b:Ljava/lang/Thread;

    .line 107
    invoke-virtual {p0}, Lcom/mfluent/asp/util/a/a/a;->a()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/util/a/a/a$a;->a:Ljava/lang/Object;

    .line 109
    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 114
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 217
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 221
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    iget-object v0, v0, Lcom/mfluent/asp/util/a/a/a$a;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 222
    :goto_0
    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    iget v2, v0, Lcom/mfluent/asp/util/a/a/a$a;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/mfluent/asp/util/a/a/a$a;->c:I

    .line 224
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    iget v0, v0, Lcom/mfluent/asp/util/a/a/a$a;->c:I

    if-gtz v0, :cond_0

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    .line 226
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 235
    return v1

    .line 221
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 229
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Write token is invalid. Cannot unlock."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final c()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 156
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 159
    :try_start_0
    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/util/a/a/a$a;->b:Ljava/lang/Thread;

    if-ne v1, v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    iget v1, v0, Lcom/mfluent/asp/util/a/a/a$a;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/mfluent/asp/util/a/a/a$a;->c:I

    .line 184
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    iget-object v0, v0, Lcom/mfluent/asp/util/a/a/a$a;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v0

    .line 162
    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, Lcom/mfluent/asp/util/a/a/a;->a(Ljava/lang/Thread;)Lcom/mfluent/asp/util/a/a/a$a;

    move-result-object v1

    .line 163
    if-eqz v1, :cond_1

    .line 164
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You should not try to acquire a write lock while holding a read lock."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 167
    :cond_1
    :goto_1
    :try_start_2
    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_2

    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_4

    .line 169
    :cond_2
    :try_start_3
    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    if-eqz v1, :cond_3

    .line 170
    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V

    goto :goto_1

    .line 176
    :catch_0
    move-exception v1

    goto :goto_1

    .line 172
    :cond_3
    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 179
    :cond_4
    :try_start_4
    new-instance v1, Lcom/mfluent/asp/util/a/a/a$a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/mfluent/asp/util/a/a/a$a;-><init>(B)V

    iput-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    .line 180
    iget-object v1, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    iput-object v0, v1, Lcom/mfluent/asp/util/a/a/a$a;->b:Ljava/lang/Thread;

    .line 181
    iget-object v0, p0, Lcom/mfluent/asp/util/a/a/a;->e:Lcom/mfluent/asp/util/a/a/a$a;

    invoke-virtual {p0}, Lcom/mfluent/asp/util/a/a/a;->a()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/util/a/a/a$a;->a:Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method
