.class public final Lcom/mfluent/asp/util/bitmap/ImageWorker;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/util/bitmap/ImageWorker$1;,
        Lcom/mfluent/asp/util/bitmap/ImageWorker$d;,
        Lcom/mfluent/asp/util/bitmap/ImageWorker$c;,
        Lcom/mfluent/asp/util/bitmap/ImageWorker$b;,
        Lcom/mfluent/asp/util/bitmap/ImageWorker$e;,
        Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;,
        Lcom/mfluent/asp/util/bitmap/ImageWorker$a;
    }
.end annotation


# static fields
.field public static final a:[I

.field public static final b:I

.field public static final c:I

.field private static d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static e:Landroid/graphics/Point;

.field private static final g:I

.field private static final h:[Lcom/mfluent/asp/util/bitmap/c;

.field private static final i:Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;

.field private static final j:[Lcom/mfluent/asp/util/bitmap/c;

.field private static final k:Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;


# instance fields
.field private final f:Landroid/content/Context;

.field private final l:I

.field private final m:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x0

    const/4 v5, 0x4

    .line 119
    sget-object v1, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 120
    const/4 v1, 0x0

    sput-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->e:Landroid/graphics/Point;

    .line 140
    new-array v1, v5, [I

    sput-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a:[I

    move v1, v0

    .line 141
    :goto_0
    if-ge v1, v5, :cond_0

    .line 142
    sget-object v2, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a:[I

    add-int/lit8 v3, v1, 0x1

    aput v3, v2, v1

    .line 141
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 144
    :cond_0
    const/4 v1, 0x5

    sput v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->b:I

    .line 145
    const/4 v1, 0x6

    sput v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->c:I

    .line 146
    const/4 v1, 0x7

    sput v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->g:I

    .line 148
    new-instance v1, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;

    sget v2, Lcom/mfluent/asp/util/bitmap/ImageWorker;->g:I

    sget v3, Lcom/mfluent/asp/util/bitmap/ImageWorker;->b:I

    invoke-direct {v1, v2}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;-><init>(I)V

    sput-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->i:Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;

    .line 149
    new-array v1, v6, [Lcom/mfluent/asp/util/bitmap/c;

    sput-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->h:[Lcom/mfluent/asp/util/bitmap/c;

    move v1, v0

    .line 150
    :goto_1
    if-ge v1, v6, :cond_1

    .line 151
    sget-object v2, Lcom/mfluent/asp/util/bitmap/ImageWorker;->h:[Lcom/mfluent/asp/util/bitmap/c;

    new-instance v3, Lcom/mfluent/asp/util/bitmap/c;

    sget-object v4, Lcom/mfluent/asp/util/bitmap/ImageWorker;->i:Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;

    invoke-direct {v3, v4}, Lcom/mfluent/asp/util/bitmap/c;-><init>(Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;)V

    aput-object v3, v2, v1

    .line 152
    sget-object v2, Lcom/mfluent/asp/util/bitmap/ImageWorker;->h:[Lcom/mfluent/asp/util/bitmap/c;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/mfluent/asp/util/bitmap/c;->start()V

    .line 150
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 155
    :cond_1
    new-instance v1, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;

    sget v2, Lcom/mfluent/asp/util/bitmap/ImageWorker;->g:I

    sget v3, Lcom/mfluent/asp/util/bitmap/ImageWorker;->b:I

    invoke-direct {v1, v2}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;-><init>(I)V

    sput-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->k:Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;

    .line 156
    new-array v1, v5, [Lcom/mfluent/asp/util/bitmap/c;

    sput-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->j:[Lcom/mfluent/asp/util/bitmap/c;

    .line 157
    :goto_2
    if-ge v0, v5, :cond_2

    .line 158
    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->j:[Lcom/mfluent/asp/util/bitmap/c;

    new-instance v2, Lcom/mfluent/asp/util/bitmap/c;

    sget-object v3, Lcom/mfluent/asp/util/bitmap/ImageWorker;->k:Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/util/bitmap/c;-><init>(Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;)V

    aput-object v2, v1, v0

    .line 159
    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->j:[Lcom/mfluent/asp/util/bitmap/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/mfluent/asp/util/bitmap/c;->start()V

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 161
    :cond_2
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 179
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/util/bitmap/ImageWorker;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    .line 180
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->f:Landroid/content/Context;

    .line 189
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->m:Landroid/os/Handler;

    .line 191
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 192
    if-nez v0, :cond_0

    .line 193
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->l:I

    .line 197
    :goto_0
    return-void

    .line 195
    :cond_0
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->l:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/util/bitmap/ImageWorker;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->f:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Landroid/graphics/BitmapFactory$Options;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->c(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/media/c/d;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/graphics/Point;)Lcom/mfluent/asp/util/bitmap/a;
    .locals 3

    .prologue
    .line 62
    invoke-static {p1}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->c(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/media/c/d;->b()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1, v0, p2}, Lcom/mfluent/asp/util/bitmap/b;->a(Ljava/lang/String;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/graphics/BitmapFactory$Options;Landroid/graphics/Point;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbnailSize()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    move-result-object v1

    sget-object v2, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->FULL_SCREEN:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    if-eq v1, v2, :cond_1

    invoke-static {v0, p1}, Lcom/mfluent/asp/util/bitmap/b;->a(Landroid/graphics/Bitmap;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_1
    new-instance v1, Lcom/mfluent/asp/util/bitmap/a;

    invoke-direct {v1, p1, v0}, Lcom/mfluent/asp/util/bitmap/a;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/graphics/Bitmap;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;ILcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    .line 302
    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDeviceId()I

    move-result v0

    iget v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->l:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 303
    sget-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    const-string v0, "mfl_ImageWorker"

    const-string v1, "Device is local."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_0
    new-instance v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;

    iget-object v4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->m:Landroid/os/Handler;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;-><init>(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/os/Handler;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V

    .line 314
    :goto_1
    const/high16 v1, 0x7f090000

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {p2, v1, v2}, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;->setTag(ILjava/lang/Object;)V

    .line 316
    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->i:Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;

    invoke-virtual {v1, v0, p3}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker$e;I)V

    .line 317
    return-void

    .line 302
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 308
    :cond_2
    sget-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 309
    const-string v0, "mfl_ImageWorker"

    const-string v1, "Device is remote."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :cond_3
    new-instance v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;

    iget-object v4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->m:Landroid/os/Handler;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/util/bitmap/ImageWorker$b;-><init>(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/os/Handler;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V

    goto :goto_1
.end method

.method public static a(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 348
    if-nez p0, :cond_1

    .line 368
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    const/high16 v0, 0x7f090000

    invoke-interface {p0, v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 353
    if-eqz v0, :cond_0

    .line 354
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;

    .line 355
    if-eqz v0, :cond_0

    .line 356
    invoke-virtual {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->b()V

    .line 357
    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->i:Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker$e;)Z

    move-result v1

    .line 358
    if-nez v1, :cond_2

    .line 359
    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->k:Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker$e;)Z

    move-result v1

    .line 360
    if-eqz v1, :cond_0

    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 361
    const-string v1, "mfl_ImageWorker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Task removed from REMOTE QUEUE: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 363
    :cond_2
    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 364
    const-string v1, "mfl_ImageWorker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Task removed from FILE QUEUE: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/util/bitmap/a;)V
    .locals 3

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/mfluent/asp/util/bitmap/a;->a()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getOrientation()I

    move-result v0

    new-instance v1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {p1}, Lcom/mfluent/asp/util/bitmap/a;->a()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    invoke-interface {p0, p1, v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;->a(Lcom/mfluent/asp/util/bitmap/a;I)V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;ILcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V
    .locals 6

    .prologue
    .line 62
    new-instance v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;

    iget-object v4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->m:Landroid/os/Handler;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;-><init>(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/os/Handler;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V

    const/high16 v1, 0x7f090000

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {p2, v1, v2}, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;->setTag(ILjava/lang/Object;)V

    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;->k:Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;

    invoke-virtual {v1, v0, p3}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker$e;I)V

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 62
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapWidth()I

    move-result v1

    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapWidth()I

    move-result v2

    if-gt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapHeight()I

    move-result v1

    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapHeight()I

    move-result v2

    if-gt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbnailSize()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbnailSize()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->c(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Lcom/mfluent/asp/datamodel/Device;
    .locals 4

    .prologue
    .line 62
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDeviceId()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    return-object v0
.end method

.method private static c(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Landroid/graphics/BitmapFactory$Options;
    .locals 3

    .prologue
    .line 1122
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1123
    invoke-virtual {p0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbnailSize()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    move-result-object v0

    sget-object v2, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->FULL_SCREEN:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    if-eq v0, v2, :cond_0

    .line 1124
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1126
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    instance-of v0, v0, Lcom/mfluent/asp/util/bitmap/c;

    if-eqz v0, :cond_1

    .line 1127
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/bitmap/c;

    iget-object v0, v0, Lcom/mfluent/asp/util/bitmap/c;->a:[B

    iput-object v0, v1, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 1129
    :cond_1
    return-object v1
.end method

.method private static declared-synchronized c(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 164
    const-class v1, Lcom/mfluent/asp/util/bitmap/ImageWorker;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->e:Landroid/graphics/Point;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    .line 165
    invoke-interface {p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;->b()Landroid/graphics/Point;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_0

    iget v2, v0, Landroid/graphics/Point;->x:I

    if-lez v2, :cond_0

    iget v2, v0, Landroid/graphics/Point;->y:I

    if-lez v2, :cond_0

    .line 167
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    sput-object v2, Lcom/mfluent/asp/util/bitmap/ImageWorker;->e:Landroid/graphics/Point;

    .line 171
    :cond_0
    sget-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->e:Landroid/graphics/Point;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;I)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 240
    if-nez p2, :cond_1

    .line 241
    sget-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 242
    const-string v0, "mfl_ImageWorker"

    const-string v1, "loadImage - ImageWorkerClient is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 247
    :cond_1
    sget-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker;->d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v1, :cond_2

    .line 248
    const-string v0, "mfl_ImageWorker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "loadImage - memoryCheck : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", priority : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_2
    sget-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$1;->a:[I

    invoke-virtual {p3}, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 262
    invoke-direct {p0, p1, p2, p4, p3}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;ILcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V

    goto :goto_0

    .line 254
    :pswitch_1
    invoke-direct {p0, p1, p2, p4, p3}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;ILcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V

    goto :goto_0

    .line 251
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
