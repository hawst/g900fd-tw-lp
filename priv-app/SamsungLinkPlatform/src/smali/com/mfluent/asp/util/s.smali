.class public final Lcom/mfluent/asp/util/s;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static b:Z

.field private static c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/util/s;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 18
    sput-boolean v1, Lcom/mfluent/asp/util/s;->b:Z

    .line 20
    sput-boolean v1, Lcom/mfluent/asp/util/s;->c:Z

    return-void
.end method

.method public static a()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 23
    sget-boolean v0, Lcom/mfluent/asp/util/s;->b:Z

    if-eqz v0, :cond_0

    .line 24
    sget-boolean v0, Lcom/mfluent/asp/util/s;->c:Z

    .line 44
    :goto_0
    return v0

    .line 28
    :cond_0
    :try_start_0
    const-string v0, "com.mfluent.cloud.preload.PreloadMarker"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 29
    if-eqz v0, :cond_1

    .line 30
    const/4 v0, 0x1

    sput-boolean v0, Lcom/mfluent/asp/util/s;->c:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :cond_1
    :goto_1
    sget-object v0, Lcom/mfluent/asp/util/s;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_2

    .line 41
    const-string v0, "mfl_PluginCheck"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::isPluginPreloaded: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/mfluent/asp/util/s;->c:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    :cond_2
    sput-boolean v4, Lcom/mfluent/asp/util/s;->b:Z

    .line 44
    sget-boolean v0, Lcom/mfluent/asp/util/s;->c:Z

    goto :goto_0

    .line 32
    :catch_0
    move-exception v0

    .line 33
    sget-object v1, Lcom/mfluent/asp/util/s;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_3

    .line 34
    const-string v1, "mfl_PluginCheck"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::isPluginPreloaded:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    :cond_3
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/util/s;->c:Z

    goto :goto_1
.end method
