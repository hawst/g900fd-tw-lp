.class public final Lcom/mfluent/asp/util/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final b:Lcom/mfluent/asp/util/i;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/mfluent/asp/util/i;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 15
    new-instance v0, Lcom/mfluent/asp/util/i;

    invoke-direct {v0}, Lcom/mfluent/asp/util/i;-><init>()V

    sput-object v0, Lcom/mfluent/asp/util/i;->b:Lcom/mfluent/asp/util/i;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public static a()Lcom/mfluent/asp/util/i;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/mfluent/asp/util/i;->b:Lcom/mfluent/asp/util/i;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Handler$Callback;)Landroid/os/Handler;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Handler-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/mfluent/asp/util/i;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/mfluent/asp/util/i;->a(Ljava/lang/String;Landroid/os/Handler$Callback;)Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Landroid/os/Handler$Callback;)Landroid/os/Handler;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    .line 31
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/mfluent/asp/util/i$1;

    invoke-direct {v2, p0, v0, p2}, Lcom/mfluent/asp/util/i$1;-><init>(Lcom/mfluent/asp/util/i;Ljava/util/concurrent/BlockingQueue;Landroid/os/Handler$Callback;)V

    invoke-direct {v1, v2, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 42
    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    return-object v0
.end method
