.class public final Lcom/mfluent/asp/util/y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/util/y;


# instance fields
.field private final b:Lcom/sec/pcw/hybrid/update/b;

.field private final c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/mfluent/asp/util/y;->a:Lcom/mfluent/asp/util/y;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    .line 51
    new-instance v0, Lcom/sec/pcw/hybrid/update/b;

    invoke-direct {v0, p1}, Lcom/sec/pcw/hybrid/update/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mfluent/asp/util/y;->b:Lcom/sec/pcw/hybrid/update/b;

    .line 52
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/mfluent/asp/util/y;
    .locals 3

    .prologue
    .line 39
    const-class v1, Lcom/mfluent/asp/util/y;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 40
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Context must not be null."

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 42
    :cond_0
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/util/y;->a:Lcom/mfluent/asp/util/y;

    if-nez v0, :cond_1

    .line 43
    new-instance v0, Lcom/mfluent/asp/util/y;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/util/y;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/util/y;->a:Lcom/mfluent/asp/util/y;

    .line 45
    :cond_1
    sget-object v0, Lcom/mfluent/asp/util/y;->a:Lcom/mfluent/asp/util/y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/util/y;->b:Lcom/sec/pcw/hybrid/update/b;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "mfl_UpgradeManager"

    const-string v1, "::updateCheck - doing far call"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/mfluent/asp/util/y;->b:Lcom/sec/pcw/hybrid/update/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/pcw/hybrid/update/b;->a(Z)V

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    const-string v0, "mfl_UpgradeManager"

    const-string v1, "::updateCheck - skipped, less than alloted time"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/mfluent/asp/util/y;->b:Lcom/sec/pcw/hybrid/update/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/pcw/hybrid/update/b;->a(Z)V

    .line 71
    return-void
.end method

.method public final b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 65
    const-string v0, "mfl_UpgradeManager"

    const-string v1, "::updateFromMarket"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/mfluent/asp/util/y;->b:Lcom/sec/pcw/hybrid/update/b;

    invoke-virtual {v0, p1}, Lcom/sec/pcw/hybrid/update/b;->a(Landroid/content/Context;)V

    .line 67
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/mfluent/asp/util/y;->b:Lcom/sec/pcw/hybrid/update/b;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/b;->a()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 85
    iget-object v2, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    const-string v3, "slpf_pref_20"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 86
    const-string v3, "upgrade_manager_last_prompt_action_time"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 88
    iget-object v4, p0, Lcom/mfluent/asp/util/y;->b:Lcom/sec/pcw/hybrid/update/b;

    invoke-virtual {v4}, Lcom/sec/pcw/hybrid/update/b;->a()Z

    move-result v4

    .line 89
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x2932e00

    add-long/2addr v2, v8

    cmp-long v2, v6, v2

    if-ltz v2, :cond_0

    move v2, v0

    .line 90
    :goto_0
    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    .line 92
    :goto_1
    const-string v1, "mfl_UpgradeManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "::isUpdatePromptAvailable result:"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " hasUpdate:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " canPrompt:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    return v0

    :cond_0
    move v2, v1

    .line 89
    goto :goto_0

    :cond_1
    move v0, v1

    .line 90
    goto :goto_1
.end method

.method public final e()V
    .locals 6

    .prologue
    const v5, 0x7f0a028b

    const v4, 0x7f09000e

    .line 98
    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v0, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 99
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    const-class v3, Lcom/mfluent/asp/ui/UpgradeActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 100
    const-string v2, "com.mfluent.asp.ui.UpgradeActivity.ACTION_DO_UPGRADE_FROM_NOTIFICATION_CLICK"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    iget-object v2, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    const/high16 v3, 0x8000000

    invoke-static {v2, v4, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 102
    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 103
    const/4 v0, 0x0

    .line 105
    :try_start_0
    iget-object v2, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mfluent/asp/util/UiUtils;->c(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 109
    :goto_0
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    .line 111
    const v0, 0x7f020078

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 116
    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    const v2, 0x7f0a0048

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 117
    iget-object v0, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    invoke-virtual {v0, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 118
    iget-object v0, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    invoke-virtual {v0, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 120
    iget-object v0, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 121
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 122
    return-void

    .line 114
    :cond_0
    const v0, 0x7f0200f8

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    goto :goto_1

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 126
    const v1, 0x7f09000e

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 127
    return-void
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 130
    iget-object v0, p0, Lcom/mfluent/asp/util/y;->c:Landroid/content/Context;

    const-string v1, "slpf_pref_20"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 132
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 133
    const-string v1, "upgrade_manager_last_prompt_action_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 134
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 135
    return-void
.end method
