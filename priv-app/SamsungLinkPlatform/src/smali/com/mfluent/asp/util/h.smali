.class public final Lcom/mfluent/asp/util/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lorg/slf4j/Logger;

.field private final b:Ljava/lang/String;

.field private c:J

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/util/h;->a:Lorg/slf4j/Logger;

    .line 22
    iput-object p1, p0, Lcom/mfluent/asp/util/h;->b:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public final a()Lcom/mfluent/asp/util/h;
    .locals 5

    .prologue
    .line 36
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/mfluent/asp/util/h;->c:J

    sub-long/2addr v0, v2

    .line 37
    iget-object v2, p0, Lcom/mfluent/asp/util/h;->a:Lorg/slf4j/Logger;

    if-eqz v2, :cond_0

    .line 38
    iget-object v2, p0, Lcom/mfluent/asp/util/h;->a:Lorg/slf4j/Logger;

    iget-object v3, p0, Lcom/mfluent/asp/util/h;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 43
    :goto_0
    return-object p0

    .line 40
    :cond_0
    iget-object v2, p0, Lcom/mfluent/asp/util/h;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/mfluent/asp/util/h;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/mfluent/asp/util/h;
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/mfluent/asp/util/h;->a:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " {} ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/util/h;->d:Ljava/lang/String;

    .line 31
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/util/h;->c:J

    .line 32
    return-object p0

    .line 29
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/util/h;->d:Ljava/lang/String;

    goto :goto_0
.end method
