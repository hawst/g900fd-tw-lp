.class final Lcom/mfluent/asp/util/bitmap/ImageWorker$c;
.super Lcom/mfluent/asp/util/bitmap/ImageWorker$e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/util/bitmap/ImageWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/util/bitmap/ImageWorker;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/os/Handler;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V
    .locals 0

    .prologue
    .line 805
    iput-object p1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    .line 806
    invoke-direct/range {p0 .. p5}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;-><init>(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/os/Handler;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V

    .line 807
    return-void
.end method


# virtual methods
.method protected final a()Lcom/mfluent/asp/util/bitmap/a;
    .locals 10

    .prologue
    const/4 v9, 0x6

    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x3

    const/4 v5, 0x0

    .line 818
    .line 821
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->e:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->d:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    if-ne v0, v1, :cond_1

    .line 962
    :cond_0
    :goto_0
    return-object v5

    .line 827
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getThumbnailSize()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    move-result-object v0

    .line 840
    sget-object v1, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->FULL_SCREEN:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    if-eq v0, v1, :cond_2

    .line 841
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapWidth()I

    move-result v1

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDesiredBitmapHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/16 v2, 0x180

    if-le v1, v2, :cond_2

    .line 842
    sget-object v0, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->FULL_SCREEN:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    .line 846
    :cond_2
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getMediaType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 937
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 941
    const-string v0, "mfl_ImageWorker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown media type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getMediaType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v5

    move-object v1, v5

    .line 945
    :goto_1
    if-eqz v0, :cond_9

    .line 946
    new-instance v5, Lcom/mfluent/asp/util/bitmap/a;

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v5, v1, v0}, Lcom/mfluent/asp/util/bitmap/a;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 848
    :sswitch_0
    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker$1;->b:[I

    invoke-virtual {v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 862
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceMediaId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v2, v1

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-static {v1}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    invoke-static {v0, v2, v3, v4, v1}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v5

    .line 867
    goto :goto_1

    .line 850
    :pswitch_0
    new-instance v0, Lcom/mfluent/asp/media/c/d;

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getData()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/media/c/d;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Ljava/io/File;)V

    move-object v1, v0

    move-object v0, v5

    .line 851
    goto :goto_1

    .line 854
    :pswitch_1
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceMediaId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v8, v5}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v5

    .line 859
    goto :goto_1

    .line 872
    :sswitch_1
    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker$1;->b:[I

    invoke-virtual {v0}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    .line 883
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceMediaId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v2, v1

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-static {v1}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    invoke-static {v0, v2, v3, v4, v1}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v5

    .line 888
    goto/16 :goto_1

    .line 875
    :pswitch_2
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceMediaId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3, v8, v5}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v5

    .line 880
    goto/16 :goto_1

    .line 893
    :sswitch_2
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "album_art"

    aput-object v3, v2, v7

    const-string v3, "_id=?"

    new-array v4, v4, [Ljava/lang/String;

    iget-object v6, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceAlbumId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 900
    if-nez v1, :cond_4

    .line 901
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v9, :cond_3

    .line 902
    const-string v0, "mfl_ImageWorker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetchImageHelper: album not found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceAlbumId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    move-object v1, v5

    goto/16 :goto_1

    .line 908
    :cond_4
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_5

    .line 935
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v5

    move-object v1, v5

    goto/16 :goto_1

    .line 912
    :cond_5
    :try_start_1
    const-string v0, "album_art"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 913
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_7

    .line 935
    :cond_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v5

    move-object v1, v5

    goto/16 :goto_1

    .line 917
    :cond_7
    :try_start_2
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 918
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_8

    .line 920
    const-string v0, "content://media/external/audio/albumart"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v3, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getSourceAlbumId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 924
    :try_start_3
    iget-object v3, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-static {v3}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 925
    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 933
    :cond_8
    :goto_2
    :try_start_4
    new-instance v0, Lcom/mfluent/asp/media/c/d;

    iget-object v3, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v0, v3, v2}, Lcom/mfluent/asp/media/c/d;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Ljava/io/File;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 935
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v1, v0

    move-object v0, v5

    .line 936
    goto/16 :goto_1

    .line 927
    :catch_0
    move-exception v0

    :try_start_5
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_8

    .line 928
    const-string v0, "mfl_ImageWorker"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::doInBackground:Trouble getting MediaStore to generate album art for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 935
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 947
    :cond_9
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 949
    :try_start_6
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->b(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)Landroid/graphics/Point;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/media/c/d;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/graphics/Point;)Lcom/mfluent/asp/util/bitmap/a;
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    move-result-object v5

    goto/16 :goto_0

    .line 950
    :catch_1
    move-exception v0

    .line 951
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_0

    .line 952
    const-string v1, "mfl_ImageWorker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doInBackground:OutOfMemoryError "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$c;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 955
    :catch_2
    move-exception v0

    .line 956
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v9, :cond_0

    .line 957
    const-string v1, "mfl_ImageWorker"

    const-string v2, "::doInBackground: Exception"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 846
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0xc -> :sswitch_2
    .end sparse-switch

    .line 848
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 872
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
    .end packed-switch
.end method
