.class public final Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

.field private final c:Ljava/util/concurrent/locks/ReentrantLock;

.field private final d:Ljava/util/concurrent/locks/Condition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-array v0, p1, [Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    iput-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    move v0, v1

    .line 53
    :goto_0
    if-ge v0, p1, :cond_0

    .line 54
    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    new-instance v3, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    invoke-direct {v3, v1}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;-><init>(B)V

    aput-object v3, v2, v0

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_0
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 58
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->d:Ljava/util/concurrent/locks/Condition;

    .line 59
    return-void
.end method


# virtual methods
.method public final a()Lcom/mfluent/asp/util/bitmap/ImageWorker$e;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 69
    const/4 v0, 0x0

    .line 71
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V

    move-object v1, v0

    .line 73
    :goto_0
    if-nez v1, :cond_1

    .line 74
    const/4 v0, 0x0

    :goto_1
    :try_start_0
    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 75
    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 76
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    aget-object v0, v1, v0

    invoke-virtual {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;

    .line 81
    :goto_2
    if-nez v0, :cond_3

    .line 82
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v0

    goto :goto_0

    .line 74
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 89
    return-object v1

    .line 86
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Lcom/mfluent/asp/util/bitmap/ImageWorker$e;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v6, 0x2

    .line 123
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    move v3, v2

    .line 125
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    array-length v0, v0

    if-ge v3, v0, :cond_9

    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    aget-object v0, v0, v3

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_3

    move v1, v3

    :goto_1
    new-instance v3, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 127
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p2, :cond_5

    .line 128
    :cond_0
    const/4 v1, 0x1

    .line 129
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_8

    .line 131
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v0, p2, :cond_7

    .line 134
    :goto_2
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-object v1, v1, v0

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;

    .line 135
    iget-object v4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    iget-object v1, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v1, v4, v1

    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 136
    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_1

    .line 137
    const-string v1, "mfl_ImageWorkerQueue"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Put with different priority. Removed:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " oldPriority:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move v0, v2

    .line 141
    :goto_3
    if-eqz v0, :cond_4

    .line 142
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;->addLast(Ljava/lang/Object;)V

    .line 147
    :goto_4
    invoke-virtual {p1, p2}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->a(I)V

    .line 149
    sget-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_2

    .line 150
    const-string v0, "mfl_ImageWorkerQueue"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Put task: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " priority:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", queue size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    aget-object v2, v2, p2

    invoke-virtual {v2}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    :goto_5
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 163
    return-void

    .line 125
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 144
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;->addFirst(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 162
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 155
    :cond_5
    :try_start_2
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-object v1, v1, v0

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0, p1}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;

    .line 156
    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_6

    .line 157
    const-string v1, "mfl_ImageWorkerQueue"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Replaced same priority task: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " oldTask: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " priority:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_6
    invoke-virtual {p1, p2}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    :cond_7
    move v2, v1

    goto/16 :goto_2

    :cond_8
    move v0, v1

    goto/16 :goto_3

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final a(Lcom/mfluent/asp/util/bitmap/ImageWorker$e;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 197
    .line 199
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    move v1, v0

    .line 201
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 202
    iget-object v2, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->b:[Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue$PriorityQueue;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    .line 203
    const/4 v0, 0x1

    .line 208
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 211
    return v0

    .line 201
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorkerQueue;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
