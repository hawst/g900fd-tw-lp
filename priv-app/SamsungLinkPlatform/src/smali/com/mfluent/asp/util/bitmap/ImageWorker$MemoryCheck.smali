.class public final enum Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/util/bitmap/ImageWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MemoryCheck"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

.field public static final enum b:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

.field public static final enum c:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

.field public static final enum d:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

.field private static final synthetic e:[Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 206
    new-instance v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    .line 207
    new-instance v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    const-string v1, "MEMORY_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->b:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    .line 208
    new-instance v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    const-string v1, "SKIP_MEMORY"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->c:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    .line 209
    new-instance v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    const-string v1, "PRE_FETCH"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->d:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    .line 205
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->b:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->c:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->d:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->e:[Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 205
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;
    .locals 1

    .prologue
    .line 205
    const-class v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->e:[Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    invoke-virtual {v0}, [Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    return-object v0
.end method
