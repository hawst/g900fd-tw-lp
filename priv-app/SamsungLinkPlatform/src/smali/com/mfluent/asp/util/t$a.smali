.class final Lcom/mfluent/asp/util/t$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/util/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field private static final b:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field protected final a:I

.field private c:Z

.field private final d:Lcom/mfluent/asp/util/t;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 131
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/mfluent/asp/util/t$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/util/t;)V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/util/t$a;->c:Z

    .line 138
    iput-object p1, p0, Lcom/mfluent/asp/util/t$a;->d:Lcom/mfluent/asp/util/t;

    .line 139
    sget-object v0, Lcom/mfluent/asp/util/t$a;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/util/t$a;->a:I

    .line 140
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/util/t$a;->c:Z

    .line 144
    return-void
.end method

.method protected final finalize()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 148
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 149
    iget-boolean v0, p0, Lcom/mfluent/asp/util/t$a;->c:Z

    if-nez v0, :cond_0

    .line 150
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/mfluent/asp/util/t$b;

    iget-object v2, p0, Lcom/mfluent/asp/util/t$a;->d:Lcom/mfluent/asp/util/t;

    iget v3, p0, Lcom/mfluent/asp/util/t$a;->a:I

    invoke-direct {v1, v2, v3}, Lcom/mfluent/asp/util/t$b;-><init>(Lcom/mfluent/asp/util/t;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/util/t$a;->c:Z

    .line 153
    :cond_0
    return-void
.end method
