.class final Lcom/mfluent/asp/util/bitmap/ImageWorker$d;
.super Lcom/mfluent/asp/util/bitmap/ImageWorker$e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/util/bitmap/ImageWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/util/bitmap/ImageWorker;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/os/Handler;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V
    .locals 0

    .prologue
    .line 987
    iput-object p1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    .line 988
    invoke-direct/range {p0 .. p5}, Lcom/mfluent/asp/util/bitmap/ImageWorker$e;-><init>(Lcom/mfluent/asp/util/bitmap/ImageWorker;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/os/Handler;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;)V

    .line 989
    return-void
.end method

.method private e()Lcom/mfluent/asp/util/bitmap/a;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v2, 0x0

    .line 1022
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->b(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 1023
    if-nez v1, :cond_0

    move-object v0, v2

    .line 1076
    :goto_0
    return-object v0

    .line 1027
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v2

    .line 1028
    goto :goto_0

    .line 1033
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/media/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/media/d;

    move-result-object v3

    .line 1039
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/media/d;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Lcom/mfluent/asp/media/c/d;

    move-result-object v0

    .line 1040
    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    iget-object v4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0}, Lcom/mfluent/asp/media/c/d;->a()Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v0, v2

    .line 1043
    :cond_2
    if-nez v0, :cond_7

    .line 1044
    sget-object v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$1;->c:[I

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move-object v0, v2

    .line 1058
    goto :goto_0

    .line 1046
    :pswitch_0
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->e:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    sget-object v1, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->d:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    invoke-virtual {v0}, Lpcloud/net/nat/c;->i()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1047
    const-string v0, "mfl_ImageWorker"

    const-string v1, "Skipping remote image fetch because memory check is PRE_FETCH and nts is not initialized"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 1048
    goto :goto_0

    .line 1050
    :cond_3
    new-instance v0, Lcom/mfluent/asp/media/c/a;

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/media/c/a;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/media/d;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/media/c/a;->g()Lcom/mfluent/asp/media/c/d;

    move-result-object v0

    move-object v1, v0

    .line 1062
    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->e:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    sget-object v3, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->d:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    if-eq v0, v3, :cond_5

    invoke-virtual {p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->c()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1064
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->a:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    iget-object v3, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;

    invoke-static {v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->b(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)Landroid/graphics/Point;

    move-result-object v0

    invoke-static {v1, v3, v0}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/media/c/d;Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Landroid/graphics/Point;)Lcom/mfluent/asp/util/bitmap/a;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 1054
    :pswitch_1
    new-instance v0, Lcom/mfluent/asp/media/c/c;

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/media/c/c;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/media/d;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/media/c/c;->g()Lcom/mfluent/asp/media/c/d;

    move-result-object v0

    move-object v1, v0

    .line 1055
    goto :goto_1

    .line 1065
    :catch_0
    move-exception v0

    .line 1066
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_4

    .line 1067
    const-string v1, "mfl_ImageWorker"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::doGetImage:OutOfMemoryError "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_4
    :goto_2
    move-object v0, v2

    .line 1076
    goto/16 :goto_0

    .line 1070
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->e:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    sget-object v3, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->d:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    if-ne v0, v3, :cond_4

    .line 1071
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_4

    .line 1072
    const-string v3, "mfl_ImageWorker"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "::doGetImage: pre-fetch "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_6

    const-string v0, "SUCCESS "

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->c:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    const-string v0, "FAILED "

    goto :goto_3

    :cond_7
    move-object v1, v0

    goto/16 :goto_1

    .line 1044
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected final a()Lcom/mfluent/asp/util/bitmap/a;
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 1001
    :try_start_0
    invoke-direct {p0}, Lcom/mfluent/asp/util/bitmap/ImageWorker$d;->e()Lcom/mfluent/asp/util/bitmap/a;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1011
    :goto_0
    return-object v0

    .line 1002
    :catch_0
    move-exception v0

    .line 1003
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v2, :cond_0

    .line 1004
    const-string v1, "mfl_ImageWorker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doInBackground:Trouble getting remote thumbnail: FileNotFoundException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1006
    :catch_1
    move-exception v0

    .line 1007
    invoke-static {}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v2, :cond_0

    .line 1008
    const-string v1, "mfl_ImageWorker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doInBackground:Trouble getting remote thumbnail"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
