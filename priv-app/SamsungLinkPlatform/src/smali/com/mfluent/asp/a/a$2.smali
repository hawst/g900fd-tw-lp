.class final Lcom/mfluent/asp/a/a$2;
.super Landroid/telephony/PhoneStateListener;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/a/a;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/a/a;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/mfluent/asp/a/a$2;->a:Lcom/mfluent/asp/a/a;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDataConnectionStateChanged(II)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 269
    invoke-super {p0, p1, p2}, Landroid/telephony/PhoneStateListener;->onDataConnectionStateChanged(II)V

    .line 270
    invoke-static {p2}, Lcom/mfluent/asp/a/b;->a(I)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v3

    .line 272
    const-string v0, "mfl_BroadcastReceiverManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Enter onDataConnectionStateChanged() : dataState:"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " networkType:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 275
    if-nez v0, :cond_1

    .line 276
    const-string v0, "mfl_BroadcastReceiverManager"

    const-string v1, "context is null in mPhoneStateListener"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 280
    :cond_1
    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ASPApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 284
    if-eqz v1, :cond_2

    .line 285
    :try_start_0
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    move-object v2, v1

    .line 292
    :goto_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 294
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-nez v1, :cond_0

    .line 295
    iget-object v1, p0, Lcom/mfluent/asp/a/a$2;->a:Lcom/mfluent/asp/a/a;

    invoke-virtual {v1, v0, v3}, Lcom/mfluent/asp/a/a;->a(Landroid/content/Context;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    goto :goto_0

    .line 287
    :catch_0
    move-exception v1

    .line 289
    const-string v4, "mfl_BroadcastReceiverManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ConnectivityManager throws exception : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move-object v1, v2

    goto :goto_1
.end method
