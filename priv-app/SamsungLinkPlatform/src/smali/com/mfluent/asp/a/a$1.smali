.class final Lcom/mfluent/asp/a/a$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/a/a;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/a/a;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/mfluent/asp/a/a$1;->a:Lcom/mfluent/asp/a/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 64
    invoke-static {}, Lcom/mfluent/asp/a/a;->f()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 65
    const-string v0, "mfl_BroadcastReceiverManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::mEventReceiver::onReceive:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 72
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 73
    const-string v0, "mfl_BroadcastReceiverManager"

    const-string v1, "CONNECTIVITY_ACTION CHANGE"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/mfluent/asp/a/a$1;->a:Lcom/mfluent/asp/a/a;

    invoke-static {v0, p1, p2}, Lcom/mfluent/asp/a/a;->a(Lcom/mfluent/asp/a/a;Landroid/content/Context;Landroid/content/Intent;)V

    .line 180
    :cond_1
    :goto_0
    return-void

    .line 76
    :cond_2
    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 78
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 80
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 90
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 100
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 118
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 129
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 131
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 133
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 134
    const-string v0, "mfl_BroadcastReceiverManager"

    const-string v1, "WIFI_P2P_CONNECTION"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-static {v5}, Lcom/sec/pcw/util/c;->a(Z)V

    goto :goto_0

    .line 137
    :cond_3
    const-string v0, "mfl_BroadcastReceiverManager"

    const-string v1, "WIFI_P2P_DISCONNECTION"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-static {v4}, Lcom/sec/pcw/util/c;->a(Z)V

    goto :goto_0

    .line 141
    :cond_4
    const-string v1, "SAMSUNG_LINK_ACTION_RECEIVE_WEARABLE_DEVICE_CONNECTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 143
    const-string v0, "WearableDeviceManager.receive_wearable_device_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 144
    const-string v1, "mfl_BroadcastReceiverManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::mHostManaverEventReceiver::onReceive::ACTION_RECEIVE_DEVICE_CONNECTED : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    new-instance v1, Lcom/mfluent/asp/a/a$1$1;

    invoke-direct {v1, p0, p1}, Lcom/mfluent/asp/a/a$1$1;-><init>(Lcom/mfluent/asp/a/a$1;Landroid/content/Context;)V

    sget-object v2, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v3, v5, [Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/a/a$1$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 160
    :cond_5
    const-string v1, "SAMSUNG_LINK_ACTION_RECEIVE_WEARABLE_DEVICE_DISCONNECTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    const-string v0, "WearableDeviceManager.receive_wearable_device_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 163
    const-string v1, "mfl_BroadcastReceiverManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::mHostManaverEventReceiver::onReceive::ACTION_RECEIVE_DEVICE_CONNECTED : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    new-instance v1, Lcom/mfluent/asp/a/a$1$2;

    invoke-direct {v1, p0, p1}, Lcom/mfluent/asp/a/a$1$2;-><init>(Lcom/mfluent/asp/a/a$1;Landroid/content/Context;)V

    sget-object v2, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v3, v5, [Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/mfluent/asp/a/a$1$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method
