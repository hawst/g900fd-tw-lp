.class public Lcom/mfluent/asp/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static e:Landroid/telephony/TelephonyManager;

.field private static f:Lcom/mfluent/asp/a/a;


# instance fields
.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

.field private final g:Landroid/content/BroadcastReceiver;

.field private final h:Landroid/telephony/PhoneStateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_BROADCAST:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/a/a;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/a/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_BROADCAST_NETWORK_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/a/a;->a:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/mfluent/asp/a/a;->f:Lcom/mfluent/asp/a/a;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/a/a;->c:Ljava/util/HashMap;

    .line 60
    new-instance v0, Lcom/mfluent/asp/a/a$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/a/a$1;-><init>(Lcom/mfluent/asp/a/a;)V

    iput-object v0, p0, Lcom/mfluent/asp/a/a;->g:Landroid/content/BroadcastReceiver;

    .line 265
    new-instance v0, Lcom/mfluent/asp/a/a$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/a/a$2;-><init>(Lcom/mfluent/asp/a/a;)V

    iput-object v0, p0, Lcom/mfluent/asp/a/a;->h:Landroid/telephony/PhoneStateListener;

    .line 48
    invoke-static {p1}, Lcom/sec/pcw/util/c;->d(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/a/a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 50
    return-void
.end method

.method public static a()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 302
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 303
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 304
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 305
    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/mfluent/asp/a/a;
    .locals 2

    .prologue
    .line 53
    const-class v1, Lcom/mfluent/asp/a/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mfluent/asp/a/a;->f:Lcom/mfluent/asp/a/a;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/mfluent/asp/a/a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/a/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/a/a;->f:Lcom/mfluent/asp/a/a;

    .line 57
    :cond_0
    sget-object v0, Lcom/mfluent/asp/a/a;->f:Lcom/mfluent/asp/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/mfluent/asp/a/a;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 31
    const-string v0, "noConnectivity"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    :goto_0
    invoke-static {}, Lcom/sec/pcw/util/b;->c()V

    invoke-virtual {p0, p1, v0}, Lcom/mfluent/asp/a/a;->a(Landroid/content/Context;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    return-void

    :cond_0
    invoke-static {p1}, Lcom/sec/pcw/util/c;->d(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v0

    goto :goto_0
.end method

.method public static b()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 309
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 310
    const-string v1, "SAMSUNG_LINK_ACTION_RECEIVE_WEARABLE_DEVICE_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 311
    return-object v0
.end method

.method public static c()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 315
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 316
    const-string v1, "SAMSUNG_LINK_ACTION_RECEIVE_WEARABLE_DEVICE_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 317
    return-object v0
.end method

.method static synthetic f()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/mfluent/asp/a/a;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/mfluent/asp/a/a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne p2, v0, :cond_1

    .line 196
    sget-object v0, Lcom/mfluent/asp/a/a;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 197
    const-string v0, "mfl_BroadcastReceiverManager"

    const-string v1, "::updateNetworkStatus:network status unchanged. Will suppress update."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    const-string v0, "mfl_BroadcastReceiverManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::updateNetworkStatus: new type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " old type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/a/a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iput-object p2, p0, Lcom/mfluent/asp/a/a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 206
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 208
    iget-object v2, p0, Lcom/mfluent/asp/a/a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    .line 209
    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/t;->updateDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V

    .line 212
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/mfluent/asp/a/a;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 253
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 254
    sput-object v0, Lcom/mfluent/asp/a/a;->e:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/mfluent/asp/a/a;->h:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 256
    return-void
.end method

.method public final d()Landroid/content/BroadcastReceiver;
    .locals 3

    .prologue
    .line 322
    const-string v0, "mfl_BroadcastReceiverManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getBroadcastReceiver: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/a/a;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lcom/mfluent/asp/a/a;->g:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method public final e()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/mfluent/asp/a/a;->d:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    return-object v0
.end method
