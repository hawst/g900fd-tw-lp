.class public final Lcom/mfluent/asp/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z


# direct methods
.method public static a(I)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 29
    const-string v0, "NetworkType"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mobile type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    packed-switch p0, :pswitch_data_0

    .line 49
    :pswitch_0
    sput-boolean v3, Lcom/mfluent/asp/a/b;->a:Z

    .line 50
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_2G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    :goto_0
    return-object v0

    .line 33
    :pswitch_1
    sput-boolean v3, Lcom/mfluent/asp/a/b;->a:Z

    .line 34
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_LTE:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    goto :goto_0

    .line 40
    :pswitch_2
    const/4 v0, 0x1

    sput-boolean v0, Lcom/mfluent/asp/a/b;->a:Z

    .line 46
    :pswitch_3
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_3G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    goto :goto_0

    .line 31
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 57
    sget-boolean v0, Lcom/mfluent/asp/a/b;->a:Z

    return v0
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/a/b;->a:Z

    .line 62
    return-void
.end method
