.class public final Lcom/mfluent/asp/filetransfer/FileUploader;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/filetransfer/FileUploader$3;,
        Lcom/mfluent/asp/filetransfer/FileUploader$StorageUploadFileTooLargeException;,
        Lcom/mfluent/asp/filetransfer/FileUploader$b;,
        Lcom/mfluent/asp/filetransfer/FileUploader$a;,
        Lcom/mfluent/asp/filetransfer/FileUploader$c;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final b:Lcom/mfluent/asp/datamodel/t;


# instance fields
.field private final c:Lcom/mfluent/asp/datamodel/Device;

.field private final d:Z

.field private e:Z

.field private final f:Landroid/content/Context;

.field private final g:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/filetransfer/FileUploader$a;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/mfluent/asp/filetransfer/FileUploader$c;

.field private j:I

.field private final k:Ljava/lang/String;

.field private final l:Z

.field private final m:Z

.field private final n:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileUploader;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 65
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileUploader;->b:Lcom/mfluent/asp/datamodel/t;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;ZLcom/mfluent/asp/common/io/util/StreamProgressListener;Lcom/mfluent/asp/filetransfer/FileUploader$c;Ljava/lang/String;ZZLjava/io/File;)V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->h:Ljava/util/List;

    .line 123
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->f:Landroid/content/Context;

    .line 124
    iput-object p2, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    .line 125
    iput-boolean p3, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->d:Z

    .line 126
    iput-object p4, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->g:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    .line 127
    iput-object p5, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->i:Lcom/mfluent/asp/filetransfer/FileUploader$c;

    .line 128
    iput-object p6, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->k:Ljava/lang/String;

    .line 129
    iput-boolean p7, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->l:Z

    .line 130
    iput-boolean p8, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->m:Z

    .line 131
    iput-object p9, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->n:Ljava/io/File;

    .line 132
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/filetransfer/FileUploader;)Lcom/mfluent/asp/filetransfer/FileUploader$c;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->i:Lcom/mfluent/asp/filetransfer/FileUploader$c;

    return-object v0
.end method

.method private c()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 201
    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->W()Z

    move-result v2

    .line 202
    if-nez v2, :cond_0

    .line 203
    new-instance v2, Ljava/io/IOException;

    const-string v3, "web storage failed to upload. Plugin is not ready."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 205
    :cond_0
    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v2

    .line 206
    if-nez v2, :cond_2

    .line 207
    sget-object v2, Lcom/mfluent/asp/filetransfer/FileUploader;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x6

    if-gt v2, v3, :cond_1

    .line 208
    const-string v2, "mfl_FileUploader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::doWebStorageUpload:Trying to upload to a device that is not found. ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_1
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Device does not exist: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 214
    :cond_2
    :try_start_0
    invoke-interface {v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->startUploadBatch()V

    .line 215
    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    move-object v10, v0

    .line 216
    iget-boolean v3, v10, Lcom/mfluent/asp/filetransfer/FileUploader$a;->i:Z

    if-eqz v3, :cond_4

    .line 217
    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->g:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    iget-object v4, v10, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lcom/mfluent/asp/common/io/util/StreamProgressListener;->bytesTransferred(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 248
    :catchall_0
    move-exception v3

    invoke-interface {v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->endUploadBatch()V

    throw v3

    .line 221
    :cond_4
    :try_start_1
    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    iget-object v4, v10, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    iget v5, v10, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    iget-object v6, v10, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    iget-object v7, v10, Lcom/mfluent/asp/filetransfer/FileUploader$a;->f:Ljava/lang/String;

    iget-object v8, v10, Lcom/mfluent/asp/filetransfer/FileUploader$a;->g:Ljava/lang/String;

    iget-object v9, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->g:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    invoke-interface/range {v2 .. v9}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->upload(Lcom/mfluent/asp/common/datamodel/CloudDevice;Ljava/io/File;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/common/io/util/StreamProgressListener;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    move-result-object v3

    .line 229
    sget-object v4, Lcom/mfluent/asp/filetransfer/FileUploader;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v5, 0x3

    if-gt v4, v5, :cond_5

    .line 230
    const-string v4, "mfl_FileUploader"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::doWebStorageUpload: result = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_5
    sget-object v4, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->OK:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    if-eq v3, v4, :cond_7

    .line 234
    sget-object v4, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;->TOO_LARGE:Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync$UploadResult;

    if-ne v3, v4, :cond_6

    .line 235
    new-instance v3, Lcom/mfluent/asp/filetransfer/FileUploader$StorageUploadFileTooLargeException;

    invoke-direct {v3}, Lcom/mfluent/asp/filetransfer/FileUploader$StorageUploadFileTooLargeException;-><init>()V

    throw v3

    .line 239
    :cond_6
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "web storage upload failed: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 243
    :cond_7
    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->i:Lcom/mfluent/asp/filetransfer/FileUploader$c;

    if-eqz v3, :cond_3

    .line 244
    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->i:Lcom/mfluent/asp/filetransfer/FileUploader$c;

    invoke-interface {v3, v10}, Lcom/mfluent/asp/filetransfer/FileUploader$c;->a(Lcom/mfluent/asp/filetransfer/FileUploader$a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 248
    :cond_8
    invoke-interface {v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->endUploadBatch()V

    .line 249
    return-void
.end method

.method private d()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 253
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v0

    .line 255
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/b/i;->c(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 257
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileUploader;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 258
    const-string v0, "mfl_FileUploader"

    const-string v1, "::doAspAutoUpload:Aborting auto upload since target device is offline"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :cond_0
    new-instance v0, Lcom/mfluent/asp/exception/TargetOfflineException;

    const-string v1, "Aborting auto upload since target device is offline"

    invoke-direct {v0, v1}, Lcom/mfluent/asp/exception/TargetOfflineException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_1
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    .line 266
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    .line 267
    iget-boolean v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->i:Z

    if-eqz v2, :cond_2

    .line 268
    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->g:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lcom/mfluent/asp/common/io/util/StreamProgressListener;->bytesTransferred(J)V

    goto :goto_0

    .line 272
    :cond_2
    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    .line 273
    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    .line 275
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v4, "UTF-8"

    invoke-static {v0, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 276
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyy-MM-dd"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 277
    new-instance v5, Ljava/util/Date;

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 284
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/api/pCloud/transfer/autoUpload?fileName="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "&path="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->l:Z

    if-eqz v0, :cond_3

    const-string v0, "&isPersonal=true"

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/nts/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 290
    new-instance v4, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v4, v0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 291
    new-instance v0, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;

    iget-object v5, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->g:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    invoke-direct {v0, v2, v3, v5}, Lcom/mfluent/asp/common/io/util/ProgressUpdatingFileEntity;-><init>(Ljava/io/File;Ljava/lang/String;Lcom/mfluent/asp/common/io/util/StreamProgressListener;)V

    invoke-virtual {v4, v0}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 293
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    new-instance v2, Lcom/mfluent/asp/filetransfer/FileUploader$1;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/filetransfer/FileUploader$1;-><init>(Lcom/mfluent/asp/filetransfer/FileUploader;)V

    invoke-static {v4, v0, v2}, Lcom/mfluent/asp/nts/b;->a(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/mfluent/asp/datamodel/Device;Lorg/apache/http/client/HttpRequestRetryHandler;)Lorg/json/JSONObject;

    goto :goto_0

    .line 284
    :cond_3
    const-string v0, ""

    goto :goto_1

    .line 302
    :cond_4
    return-void
.end method

.method private e()V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 305
    const-string v3, "FILE"

    .line 306
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->h:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    iget v2, v2, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    sparse-switch v2, :sswitch_data_0

    .line 323
    :goto_0
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    .line 325
    const-string v2, ""

    .line 326
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    .line 327
    if-eqz v4, :cond_17

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 328
    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v2

    const-string v4, "UTF-8"

    invoke-static {v2, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    .line 331
    :goto_1
    const-string v2, ""

    .line 332
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v2

    const-string v5, "UTF-8"

    invoke-static {v2, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v5, v2

    .line 336
    :goto_2
    const/4 v2, 0x0

    .line 337
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->n:Ljava/io/File;

    if-eqz v6, :cond_15

    .line 338
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->n:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v6, "UTF-8"

    invoke-static {v2, v6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v6, v2

    .line 341
    :goto_3
    const-wide/16 v10, 0x0

    .line 342
    const-wide/16 v8, 0x0

    .line 343
    const/4 v13, 0x0

    .line 344
    const/4 v7, 0x0

    .line 346
    const/16 v16, 0x0

    .line 348
    const-string v2, "STOPPED"

    .line 350
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v15, "/api/pCloud/transfer/save/sessions/"

    invoke-direct {v12, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->k:Ljava/lang/String;

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->l:Z

    if-eqz v12, :cond_0

    const-string v12, "?isPersonal=true"

    :goto_4
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v14, v12}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 353
    const-string v14, "currentBytesSent"

    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    invoke-virtual {v12, v14, v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 354
    const-string v14, "indexOfFile"

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    int-to-long v8, v8

    .line 355
    const-string v14, "currentFile"

    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 356
    const-string v14, "status"

    invoke-virtual {v12, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    move-object v12, v7

    move-wide v14, v8

    move-wide v8, v10

    .line 361
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->e:Z

    if-eqz v7, :cond_1

    const-string v7, "SENDING"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 362
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Target is still receiving...sleep 5 sec"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 308
    :sswitch_0
    const-string v2, "PHOTO"

    move-object v3, v2

    .line 309
    goto/16 :goto_0

    .line 313
    :sswitch_1
    const-string v2, "VIDEO"

    move-object v3, v2

    .line 314
    goto/16 :goto_0

    .line 316
    :sswitch_2
    const-string v2, "MUSIC"

    move-object v3, v2

    .line 317
    goto/16 :goto_0

    .line 319
    :sswitch_3
    const-string v2, "DOC"

    move-object v3, v2

    goto/16 :goto_0

    .line 350
    :cond_0
    :try_start_1
    const-string v12, ""
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :catch_0
    move-exception v12

    move-object v12, v7

    move-wide v14, v8

    move-wide v8, v10

    goto :goto_5

    .line 365
    :cond_1
    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-gtz v2, :cond_2

    const-wide/16 v10, 0x0

    cmp-long v2, v14, v10

    if-lez v2, :cond_14

    .line 366
    :cond_2
    const/4 v2, 0x1

    move v7, v2

    .line 369
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->d:Z

    if-eqz v2, :cond_13

    .line 370
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->h:Ljava/util/List;

    const/4 v10, 0x0

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    .line 371
    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    .line 372
    new-instance v10, Ljava/text/SimpleDateFormat;

    const-string v11, "yyyy-MM-dd"

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v10, v11, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 373
    new-instance v11, Ljava/util/Date;

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-direct {v11, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v10, v11}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 377
    :goto_7
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "/api/pCloud/transfer/save?sessionId="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->k:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    if-eqz v7, :cond_3

    const-string v7, ""

    :goto_8
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->l:Z

    if-eqz v7, :cond_4

    const-string v7, "&isPersonal=true"

    :goto_9
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->m:Z

    if-eqz v7, :cond_5

    const-string v7, "&isDigitalSecure=TRUE"

    :goto_a
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "&currentTab="

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "&sName="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "&cName="

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&tName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&deviceProgress=VISIBLE"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v6, :cond_6

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "&baseTargetPath="

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_b
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->d:Z

    if-eqz v3, :cond_7

    const-string v3, "&isAutoArchive=true"

    :goto_c
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->d:Z

    if-eqz v4, :cond_8

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "&path="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_d
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/nts/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 392
    new-instance v16, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 394
    new-instance v17, Lcom/mfluent/asp/filetransfer/a/b;

    sget-object v2, Lorg/apache/http/entity/mime/HttpMultipartMode;->STRICT:Lorg/apache/http/entity/mime/HttpMultipartMode;

    const-string v3, "UTF-8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3}, Lcom/mfluent/asp/filetransfer/a/b;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/nio/charset/Charset;)V

    .line 396
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 399
    const/4 v2, 0x0

    move-wide v4, v8

    move v13, v2

    :goto_e
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v13, v2, :cond_e

    .line 400
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->h:Ljava/util/List;

    invoke-interface {v2, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    .line 401
    iget-object v6, v2, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    .line 403
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->m:Z

    if-eqz v3, :cond_11

    .line 404
    const-class v3, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v3}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v3}, Lcom/mfluent/asp/ASPApplication;->getCacheDir()Ljava/io/File;

    move-result-object v7

    .line 405
    new-instance v3, Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 406
    invoke-static {v3}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 409
    :try_start_3
    sget-object v7, Lcom/mfluent/asp/filetransfer/FileUploader;->b:Lcom/mfluent/asp/datamodel/t;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v7

    .line 414
    :try_start_4
    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v8

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v9, v10, v7}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->EncryptSecureData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 419
    if-eqz v7, :cond_9

    .line 420
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Got failure code "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " when trying to encrypt "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for secure transfer"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 476
    :catchall_0
    move-exception v2

    move-object v3, v2

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 477
    invoke-static {v2}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    goto :goto_f

    .line 377
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v11, "&files="

    invoke-direct {v7, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v11, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->j:I

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_8

    :cond_4
    const-string v7, ""

    goto/16 :goto_9

    :cond_5
    const-string v7, ""

    goto/16 :goto_a

    :cond_6
    const-string v3, ""

    goto/16 :goto_b

    :cond_7
    const-string v3, ""

    goto/16 :goto_c

    :cond_8
    const-string v2, ""

    goto/16 :goto_d

    .line 410
    :catch_1
    move-exception v2

    :try_start_5
    throw v2

    .line 424
    :cond_9
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 427
    :goto_10
    int-to-long v6, v13

    cmp-long v6, v14, v6

    if-lez v6, :cond_a

    .line 429
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->g:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-interface {v2, v6, v7}, Lcom/mfluent/asp/common/io/util/StreamProgressListener;->bytesTransferred(J)V

    move-wide v2, v4

    .line 399
    :goto_11
    add-int/lit8 v4, v13, 0x1

    move v13, v4

    move-wide v4, v2

    goto/16 :goto_e

    .line 434
    :cond_a
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_12

    if-eqz v12, :cond_12

    .line 435
    sget-object v6, Lcom/mfluent/asp/filetransfer/FileUploader;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 436
    const-string v6, "AutoRetry"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "partial file name = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", 2="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    :cond_b
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v12, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_12

    .line 439
    const-wide/16 v4, 0x0

    move-wide v6, v4

    .line 444
    :goto_12
    const-wide/16 v4, 0x0

    cmp-long v4, v6, v4

    if-lez v4, :cond_d

    .line 445
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->g:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    invoke-interface {v4, v6, v7}, Lcom/mfluent/asp/common/io/util/StreamProgressListener;->bytesTransferred(J)V

    .line 446
    new-instance v4, Lcom/mfluent/asp/filetransfer/i;

    iget-object v5, v2, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    invoke-direct {v4, v3, v5, v6, v7}, Lcom/mfluent/asp/filetransfer/i;-><init>(Ljava/io/File;Ljava/lang/String;J)V

    .line 447
    new-instance v5, Lcom/mfluent/asp/filetransfer/FileUploader$b;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->g:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v2, v8, v4}, Lcom/mfluent/asp/filetransfer/FileUploader$b;-><init>(Lcom/mfluent/asp/filetransfer/FileUploader;Lcom/mfluent/asp/filetransfer/FileUploader$a;Lcom/mfluent/asp/common/io/util/StreamProgressListener;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 448
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v10

    .line 449
    new-instance v3, Lcom/mfluent/asp/filetransfer/j;

    const-string v4, "file"

    const-wide/16 v8, 0x1

    sub-long v8, v10, v8

    invoke-direct/range {v3 .. v11}, Lcom/mfluent/asp/filetransfer/j;-><init>(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;JJJ)V

    .line 450
    const-wide/16 v4, 0x0

    .line 457
    :goto_13
    const-string v6, "File-Index"

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lorg/apache/http/entity/mime/FormBodyPart;->addField(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    const-string v6, "Content-Length"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lorg/apache/http/entity/mime/FormBodyPart;->getBody()Lorg/apache/http/entity/mime/content/ContentBody;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/entity/mime/content/ContentBody;->getContentLength()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lorg/apache/http/entity/mime/FormBodyPart;->addField(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    iget-object v6, v2, Lcom/mfluent/asp/filetransfer/FileUploader$a;->m:Ljava/lang/String;

    if-eqz v6, :cond_c

    .line 460
    const-string v6, "GroupId"

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/FileUploader$a;->m:Ljava/lang/String;

    invoke-virtual {v3, v6, v2}, Lorg/apache/http/entity/mime/FormBodyPart;->addField(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_c
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/filetransfer/a/b;->a(Lorg/apache/http/entity/mime/FormBodyPart;)V

    move-wide v2, v4

    goto/16 :goto_11

    .line 452
    :cond_d
    new-instance v4, Lorg/apache/http/entity/mime/content/FileBody;

    iget-object v5, v2, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    invoke-direct {v4, v3, v5}, Lorg/apache/http/entity/mime/content/FileBody;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 453
    new-instance v5, Lcom/mfluent/asp/filetransfer/FileUploader$b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->g:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v2, v3, v4}, Lcom/mfluent/asp/filetransfer/FileUploader$b;-><init>(Lcom/mfluent/asp/filetransfer/FileUploader;Lcom/mfluent/asp/filetransfer/FileUploader$a;Lcom/mfluent/asp/common/io/util/StreamProgressListener;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 454
    new-instance v3, Lorg/apache/http/entity/mime/FormBodyPart;

    const-string v4, "file"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/entity/mime/FormBodyPart;-><init>(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    move-wide v4, v6

    goto :goto_13

    .line 465
    :cond_e
    invoke-virtual/range {v16 .. v17}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 467
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    new-instance v3, Lcom/mfluent/asp/filetransfer/FileUploader$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/mfluent/asp/filetransfer/FileUploader$2;-><init>(Lcom/mfluent/asp/filetransfer/FileUploader;)V

    move-object/from16 v0, v16

    invoke-static {v0, v2, v3}, Lcom/mfluent/asp/nts/b;->a(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/mfluent/asp/datamodel/Device;Lorg/apache/http/client/HttpRequestRetryHandler;)Lorg/json/JSONObject;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 476
    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_14
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 477
    invoke-static {v2}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    goto :goto_14

    .line 478
    :cond_f
    throw v3

    .line 479
    :cond_10
    return-void

    :cond_11
    move-object v3, v6

    goto/16 :goto_10

    :cond_12
    move-wide v6, v4

    goto/16 :goto_12

    :cond_13
    move-object/from16 v2, v16

    goto/16 :goto_7

    :cond_14
    move v7, v13

    goto/16 :goto_6

    :cond_15
    move-object v6, v2

    goto/16 :goto_3

    :cond_16
    move-object v5, v2

    goto/16 :goto_2

    :cond_17
    move-object v4, v2

    goto/16 :goto_1

    .line 306
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0xd -> :sswitch_1
        0xe -> :sswitch_1
        0xf -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public final a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 189
    :goto_0
    return-void

    .line 169
    :cond_0
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileUploader$3;->a:[I

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 191
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upload not supported for device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :pswitch_0
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 174
    :cond_1
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileUploader;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_2

    .line 175
    const-string v0, "mfl_FileUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::doTransfer:isSupportsAutoArchive="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->C()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->c:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->C()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 179
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/FileUploader;->e()V

    goto :goto_0

    .line 181
    :cond_3
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/FileUploader;->d()V

    goto :goto_0

    .line 184
    :cond_4
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/FileUploader;->e()V

    goto :goto_0

    .line 188
    :pswitch_1
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/FileUploader;->c()V

    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/mfluent/asp/filetransfer/FileUploader$a;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    iget-boolean v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->h:Z

    if-nez v0, :cond_0

    .line 154
    iget v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->j:I

    .line 156
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 197
    iput-boolean p1, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->e:Z

    .line 198
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader;->k:Ljava/lang/String;

    return-object v0
.end method
