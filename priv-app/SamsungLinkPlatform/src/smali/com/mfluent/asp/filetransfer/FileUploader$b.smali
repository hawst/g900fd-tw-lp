.class final Lcom/mfluent/asp/filetransfer/FileUploader$b;
.super Lorg/apache/http/entity/mime/content/AbstractContentBody;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/filetransfer/FileUploader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/filetransfer/FileUploader;

.field private final b:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

.field private final c:Lcom/mfluent/asp/filetransfer/FileUploader$a;

.field private final d:Lorg/apache/http/entity/mime/content/ContentBody;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/filetransfer/FileUploader;Lcom/mfluent/asp/filetransfer/FileUploader$a;Lcom/mfluent/asp/common/io/util/StreamProgressListener;Lorg/apache/http/entity/mime/content/ContentBody;)V
    .locals 1

    .prologue
    .line 490
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->a:Lcom/mfluent/asp/filetransfer/FileUploader;

    .line 491
    iget-object v0, p2, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/apache/http/entity/mime/content/AbstractContentBody;-><init>(Ljava/lang/String;)V

    .line 493
    iput-object p2, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->c:Lcom/mfluent/asp/filetransfer/FileUploader$a;

    .line 494
    iput-object p3, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->b:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    .line 495
    iput-object p4, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->d:Lorg/apache/http/entity/mime/content/ContentBody;

    .line 496
    return-void
.end method


# virtual methods
.method public final getCharset()Ljava/lang/String;
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->d:Lorg/apache/http/entity/mime/content/ContentBody;

    invoke-interface {v0}, Lorg/apache/http/entity/mime/content/ContentBody;->getCharset()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getContentLength()J
    .locals 2

    .prologue
    .line 525
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->d:Lorg/apache/http/entity/mime/content/ContentBody;

    invoke-interface {v0}, Lorg/apache/http/entity/mime/content/ContentBody;->getContentLength()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->d:Lorg/apache/http/entity/mime/content/ContentBody;

    invoke-interface {v0}, Lorg/apache/http/entity/mime/content/ContentBody;->getFilename()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTransferEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->d:Lorg/apache/http/entity/mime/content/ContentBody;

    invoke-interface {v0}, Lorg/apache/http/entity/mime/content/ContentBody;->getTransferEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 505
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->d:Lorg/apache/http/entity/mime/content/ContentBody;

    new-instance v1, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;

    new-instance v2, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;

    new-instance v3, Lcom/mfluent/asp/filetransfer/k;

    iget-object v4, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->d:Lorg/apache/http/entity/mime/content/ContentBody;

    invoke-interface {v4}, Lorg/apache/http/entity/mime/content/ContentBody;->getContentLength()J

    move-result-wide v4

    invoke-direct {v3, p1, v4, v5}, Lcom/mfluent/asp/filetransfer/k;-><init>(Ljava/io/OutputStream;J)V

    iget-object v4, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->b:Lcom/mfluent/asp/common/io/util/StreamProgressListener;

    invoke-direct {v2, v3, v4}, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;-><init>(Ljava/io/OutputStream;Lcom/mfluent/asp/common/io/util/StreamProgressListener;)V

    invoke-direct {v1, v2}, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-interface {v0, v1}, Lorg/apache/http/entity/mime/content/ContentBody;->writeTo(Ljava/io/OutputStream;)V

    .line 508
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->a:Lcom/mfluent/asp/filetransfer/FileUploader;

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/FileUploader;->a(Lcom/mfluent/asp/filetransfer/FileUploader;)Lcom/mfluent/asp/filetransfer/FileUploader$c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->a:Lcom/mfluent/asp/filetransfer/FileUploader;

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/FileUploader;->a(Lcom/mfluent/asp/filetransfer/FileUploader;)Lcom/mfluent/asp/filetransfer/FileUploader$c;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileUploader$b;->c:Lcom/mfluent/asp/filetransfer/FileUploader$a;

    invoke-interface {v0, v1}, Lcom/mfluent/asp/filetransfer/FileUploader$c;->a(Lcom/mfluent/asp/filetransfer/FileUploader$a;)V

    .line 511
    :cond_0
    return-void
.end method
