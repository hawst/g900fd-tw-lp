.class public final Lcom/mfluent/asp/filetransfer/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/HttpEntity;


# static fields
.field private static final a:[C


# instance fields
.field private final b:Lcom/mfluent/asp/filetransfer/a/a;

.field private final c:Lorg/apache/http/Header;

.field private d:J

.field private volatile e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-string v0, "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/filetransfer/a/b;->a:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 98
    sget-object v0, Lorg/apache/http/entity/mime/HttpMultipartMode;->STRICT:Lorg/apache/http/entity/mime/HttpMultipartMode;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mfluent/asp/filetransfer/a/b;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/nio/charset/Charset;)V

    .line 99
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/nio/charset/Charset;)V
    .locals 5

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {}, Lcom/mfluent/asp/filetransfer/a/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 75
    if-nez p1, :cond_0

    .line 76
    sget-object p1, Lorg/apache/http/entity/mime/HttpMultipartMode;->STRICT:Lorg/apache/http/entity/mime/HttpMultipartMode;

    .line 78
    :cond_0
    new-instance v1, Lcom/mfluent/asp/filetransfer/a/a;

    const-string v2, "form-data"

    invoke-direct {v1, v2, p2, v0, p1}, Lcom/mfluent/asp/filetransfer/a/a;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/lang/String;Lorg/apache/http/entity/mime/HttpMultipartMode;)V

    iput-object v1, p0, Lcom/mfluent/asp/filetransfer/a/b;->b:Lcom/mfluent/asp/filetransfer/a/a;

    .line 79
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-Type"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "multipart/form-data; boundary="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_1

    const-string v0, "; charset="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/mfluent/asp/filetransfer/a/b;->c:Lorg/apache/http/Header;

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/a/b;->e:Z

    .line 81
    return-void
.end method

.method private static a()Ljava/lang/String;
    .locals 6

    .prologue
    .line 113
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 115
    const/16 v0, 0xb

    invoke-virtual {v2, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v3, v0, 0x1e

    .line 116
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 117
    sget-object v4, Lcom/mfluent/asp/filetransfer/a/b;->a:[C

    sget-object v5, Lcom/mfluent/asp/filetransfer/a/b;->a:[C

    array-length v5, v5

    invoke-virtual {v2, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    aget-char v4, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lorg/apache/http/entity/mime/FormBodyPart;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a/b;->b:Lcom/mfluent/asp/filetransfer/a/a;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/filetransfer/a/a;->a(Lorg/apache/http/entity/mime/FormBodyPart;)V

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/a/b;->e:Z

    .line 125
    return-void
.end method

.method public final consumeContent()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a/b;->isStreaming()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Streaming entity does not implement #consumeContent()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_0
    return-void
.end method

.method public final getContent()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 180
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Multipart form entity does not implement #getContent()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getContentEncoding()Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getContentLength()J
    .locals 2

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/a/b;->e:Z

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a/b;->b:Lcom/mfluent/asp/filetransfer/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/a/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/a/b;->d:J

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/a/b;->e:Z

    .line 158
    :cond_0
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/a/b;->d:J

    return-wide v0
.end method

.method public final getContentType()Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a/b;->c:Lorg/apache/http/Header;

    return-object v0
.end method

.method public final isChunked()Z
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a/b;->isRepeatable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isRepeatable()Z
    .locals 6

    .prologue
    .line 133
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a/b;->b:Lcom/mfluent/asp/filetransfer/a/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/a/a;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/entity/mime/FormBodyPart;

    .line 134
    invoke-virtual {v0}, Lorg/apache/http/entity/mime/FormBodyPart;->getBody()Lorg/apache/http/entity/mime/content/ContentBody;

    move-result-object v0

    .line 135
    invoke-interface {v0}, Lorg/apache/http/entity/mime/content/ContentBody;->getContentLength()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 139
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final isStreaming()Z
    .locals 1

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a/b;->isRepeatable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a/b;->b:Lcom/mfluent/asp/filetransfer/a/a;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/filetransfer/a/a;->a(Ljava/io/OutputStream;)V

    .line 186
    return-void
.end method
