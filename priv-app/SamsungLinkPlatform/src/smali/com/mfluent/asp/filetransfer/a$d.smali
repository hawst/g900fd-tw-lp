.class final Lcom/mfluent/asp/filetransfer/a$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaScannerConnection$OnScanCompletedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/filetransfer/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/filetransfer/a;

.field private final b:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

.field private final c:Lcom/mfluent/asp/filetransfer/a$a;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/filetransfer/a;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Lcom/mfluent/asp/filetransfer/a$a;)V
    .locals 0

    .prologue
    .line 913
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/a$d;->a:Lcom/mfluent/asp/filetransfer/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 914
    iput-object p2, p0, Lcom/mfluent/asp/filetransfer/a$d;->b:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    .line 915
    iput-object p3, p0, Lcom/mfluent/asp/filetransfer/a$d;->c:Lcom/mfluent/asp/filetransfer/a$a;

    .line 916
    return-void
.end method

.method private static a(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 971
    invoke-static {p2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 972
    invoke-static {p0, p3}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 973
    invoke-static {v0, p2}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 974
    invoke-virtual {p1, p3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    :cond_0
    return-void
.end method


# virtual methods
.method public final onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6

    .prologue
    .line 921
    invoke-static {}, Lcom/mfluent/asp/filetransfer/a;->D()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Enter MetadataFixingScanCompleteListener::onScanCompleted() : path:{} uri:{}"

    invoke-interface {v0, v1, p1, p2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 924
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/filetransfer/a;->D()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Enter MetadataFixingScanCompleteListener::fixMetadata()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a$d;->c:Lcom/mfluent/asp/filetransfer/a$a;

    iget v0, v0, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a$d;->a:Lcom/mfluent/asp/filetransfer/a;

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/a;->O()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 931
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a$d;->b:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    if-eqz v0, :cond_1

    .line 932
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a$d;->b:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    invoke-interface {v0, p1, p2}, Landroid/media/MediaScannerConnection$OnScanCompletedListener;->onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V

    .line 934
    :cond_1
    return-void

    .line 924
    :cond_2
    :try_start_3
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/a$d;->c:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/a$a;->m:Ljava/lang/String;

    const-string v3, "title"

    invoke-static {v1, v0, v2, v3}, Lcom/mfluent/asp/filetransfer/a$d;->a(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/a$d;->c:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/a$a;->n:Ljava/lang/String;

    const-string v3, "artist"

    invoke-static {v1, v0, v2, v3}, Lcom/mfluent/asp/filetransfer/a$d;->a(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/a$d;->c:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/a$a;->o:Ljava/lang/String;

    const-string v3, "album"

    invoke-static {v1, v0, v2, v3}, Lcom/mfluent/asp/filetransfer/a$d;->a(Landroid/database/Cursor;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/ContentValues;->size()I

    move-result v2

    if-lez v2, :cond_3

    const-string v2, "date_modified"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/a$d;->a:Lcom/mfluent/asp/filetransfer/a;

    invoke-virtual {v2}, Lcom/mfluent/asp/filetransfer/a;->O()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, p2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 925
    :catch_0
    move-exception v0

    .line 926
    invoke-static {}, Lcom/mfluent/asp/filetransfer/a;->D()Lorg/slf4j/Logger;

    move-result-object v1

    invoke-interface {v1}, Lorg/slf4j/Logger;->isWarnEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 927
    invoke-static {}, Lcom/mfluent/asp/filetransfer/a;->D()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::onScanCompleted: Trouble fixing metadata for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 924
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
.end method
