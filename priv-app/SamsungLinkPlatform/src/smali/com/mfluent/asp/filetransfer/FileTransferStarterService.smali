.class public Lcom/mfluent/asp/filetransfer/FileTransferStarterService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method private static a(Landroid/content/Intent;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 4

    .prologue
    .line 73
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromIntent(Landroid/content/Intent;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 74
    if-nez v0, :cond_0

    .line 75
    const-string v1, "rowIds"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v1

    .line 76
    if-eqz v1, :cond_0

    .line 77
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 81
    :cond_0
    if-nez v0, :cond_1

    .line 82
    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;->a:Lorg/slf4j/Logger;

    const-string v2, "Intent {} missing required extra \'{}\'"

    const-class v3, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, p0, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 85
    :cond_1
    return-object v0
.end method

.method private static b(Landroid/content/Intent;)Lcom/mfluent/asp/datamodel/Device;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 89
    const-string v1, "deviceId"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 90
    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;->a:Lorg/slf4j/Logger;

    const-string v2, "Intent {} missing required extra \'{}\'"

    const-string v3, "deviceId"

    invoke-interface {v1, v2, p0, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 101
    :goto_0
    return-object v0

    .line 93
    :cond_0
    const-string v1, "deviceId"

    const-wide/16 v2, -0x1

    invoke-virtual {p0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 95
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    long-to-int v4, v2

    int-to-long v4, v4

    invoke-virtual {v1, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 96
    if-nez v1, :cond_1

    .line 97
    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;->a:Lorg/slf4j/Logger;

    const-string v4, "Device {} does not exist"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v4, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 101
    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 26
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;->a:Lorg/slf4j/Logger;

    const-string v1, "onHandleIntent {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 28
    invoke-static {p0}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v1

    .line 30
    const-string v0, "transferOptions"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .line 32
    const-string v2, "com.samsung.android.sdk.samsunglink.filetransfer.Download"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 33
    invoke-static {p1}, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;->a(Landroid/content/Intent;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v2

    .line 34
    if-nez v2, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    invoke-interface {v1, v2, v0}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    goto :goto_0

    .line 39
    :cond_2
    const-string v2, "com.samsung.android.sdk.samsunglink.filetransfer.Transfer"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 40
    invoke-static {p1}, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;->a(Landroid/content/Intent;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v2

    .line 41
    if-eqz v2, :cond_0

    .line 45
    invoke-static {p1}, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;->b(Landroid/content/Intent;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    .line 46
    if-eqz v3, :cond_0

    .line 50
    invoke-interface {v1, v2, v3, v0}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    goto :goto_0

    .line 51
    :cond_3
    const-string v2, "com.samsung.android.sdk.samsunglink.filetransfer.Upload"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 52
    invoke-static {p1}, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;->a(Landroid/content/Intent;)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v2

    .line 53
    if-eqz v2, :cond_0

    .line 57
    invoke-static {p1}, Lcom/mfluent/asp/filetransfer/FileTransferStarterService;->b(Landroid/content/Intent;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    .line 58
    if-eqz v3, :cond_0

    .line 62
    invoke-interface {v1, v2, v3, v0}, Lcom/mfluent/asp/filetransfer/d;->a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    goto :goto_0

    .line 63
    :cond_4
    const-string v0, "com.samsung.android.sdk.samsunglink.filetransfer.Cancel"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "isCancelForAutoUpload"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 66
    if-eqz v0, :cond_0

    .line 67
    invoke-interface {v1}, Lcom/mfluent/asp/filetransfer/d;->a()V

    goto :goto_0
.end method
