.class public final Lcom/mfluent/asp/filetransfer/i;
.super Lorg/apache/http/entity/mime/content/FileBody;
.source "SourceFile"


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lorg/apache/http/entity/mime/content/FileBody;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 32
    iput-wide p3, p0, Lcom/mfluent/asp/filetransfer/i;->a:J

    .line 33
    return-void
.end method


# virtual methods
.method public final getContentLength()J
    .locals 4

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/i;->getFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/mfluent/asp/filetransfer/i;->a:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    if-nez p1, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Output stream may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/i;->getFile()Ljava/io/File;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 57
    :try_start_0
    iget-wide v2, p0, Lcom/mfluent/asp/filetransfer/i;->a:J

    invoke-static {v1, v2, v3}, Lorg/apache/commons/io/IOUtils;->skipFully(Ljava/io/InputStream;J)V

    .line 58
    const/16 v0, 0x1000

    new-array v0, v0, [B

    .line 60
    :goto_0
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 61
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0

    .line 63
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 66
    return-void
.end method
