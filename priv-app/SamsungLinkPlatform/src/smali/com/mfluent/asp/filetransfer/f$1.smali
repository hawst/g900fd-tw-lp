.class final Lcom/mfluent/asp/filetransfer/f$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/filetransfer/f;->M()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/filetransfer/f;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/filetransfer/f;)V
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/f$1;->a:Lcom/mfluent/asp/filetransfer/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 483
    sget-object v0, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 484
    const-string v0, "AutoRetry"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Auto Retry start seesionid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/f$1;->a:Lcom/mfluent/asp/filetransfer/f;

    invoke-virtual {v2}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f$1;->a:Lcom/mfluent/asp/filetransfer/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/mfluent/asp/filetransfer/f;->b:Z

    .line 487
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.mfluent.asp.filetransfer.FileTransferManager.RETRY"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 488
    const-string v0, "sessionId"

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/f$1;->a:Lcom/mfluent/asp/filetransfer/f;

    invoke-virtual {v2}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 489
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 490
    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 491
    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 492
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f$1;->a:Lcom/mfluent/asp/filetransfer/f;

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/f;->a(Lcom/mfluent/asp/filetransfer/f;)Z

    .line 493
    return-void
.end method
