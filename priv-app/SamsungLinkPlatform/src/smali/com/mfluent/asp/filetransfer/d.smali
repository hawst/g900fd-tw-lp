.class public interface abstract Lcom/mfluent/asp/filetransfer/d;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(Landroid/database/Cursor;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;
.end method

.method public abstract a(Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/mfluent/asp/common/datamodel/ASPFile;",
            ">(",
            "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
            "<TT;>;",
            "Lcom/mfluent/asp/datamodel/Device;",
            "Lcom/mfluent/asp/datamodel/Device;",
            "Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;",
            ")",
            "Lcom/mfluent/asp/filetransfer/f;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;
.end method

.method public abstract a(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/f;
.end method

.method public abstract a(Ljava/util/List;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/filetransfer/l;",
            ">;",
            "Lcom/mfluent/asp/datamodel/Device;",
            "Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;",
            ")",
            "Lcom/mfluent/asp/filetransfer/f;"
        }
    .end annotation
.end method

.method public abstract a()V
.end method

.method public abstract a(Lcom/mfluent/asp/datamodel/Device;)V
.end method

.method public abstract a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/mfluent/asp/dws/handlers/ap;)V
.end method

.method public abstract a(Lcom/mfluent/asp/dws/handlers/ap;Z)V
.end method

.method public abstract a(Lcom/mfluent/asp/filetransfer/FileTransferSession;)V
.end method

.method public abstract a(Lcom/mfluent/asp/filetransfer/b;)V
.end method

.method public abstract a(Lcom/mfluent/asp/filetransfer/c;)V
.end method

.method public abstract a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
.end method

.method public abstract a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
.end method

.method public abstract a(Ljava/lang/String;Z)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/f;
.end method

.method public abstract b(Lcom/mfluent/asp/filetransfer/c;)V
.end method

.method public abstract b()Z
.end method

.method public abstract b(Lcom/mfluent/asp/datamodel/Device;)Z
.end method

.method public abstract c(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/ap;
.end method

.method public abstract c()V
.end method

.method public abstract d(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/ap;
.end method

.method public abstract d()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/filetransfer/f;",
            ">;"
        }
    .end annotation
.end method

.method public abstract e()Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/filetransfer/f;",
            ">;"
        }
    .end annotation
.end method

.method public abstract f()V
.end method

.method public abstract g()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/mfluent/asp/dws/handlers/ap;",
            ">;"
        }
    .end annotation
.end method

.method public abstract h()Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/filetransfer/f;",
            ">;"
        }
    .end annotation
.end method
