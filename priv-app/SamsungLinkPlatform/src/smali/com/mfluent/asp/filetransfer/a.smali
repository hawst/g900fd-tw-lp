.class Lcom/mfluent/asp/filetransfer/a;
.super Lcom/mfluent/asp/filetransfer/f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/filetransfer/a$1;,
        Lcom/mfluent/asp/filetransfer/a$d;,
        Lcom/mfluent/asp/filetransfer/a$c;,
        Lcom/mfluent/asp/filetransfer/a$b;,
        Lcom/mfluent/asp/filetransfer/a$a;
    }
.end annotation


# static fields
.field private static final d:Lorg/slf4j/Logger;


# instance fields
.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/filetransfer/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/filetransfer/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/filetransfer/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

.field private final i:Ljava/io/File;

.field private final j:Lcom/mfluent/asp/datamodel/t;

.field private final k:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lcom/mfluent/asp/filetransfer/a;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/filetransfer/g;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Ljava/io/File;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 2

    .prologue
    .line 123
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p5}, Lcom/mfluent/asp/filetransfer/f;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    .line 106
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->f:Ljava/util/Set;

    .line 107
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->g:Ljava/util/Set;

    .line 114
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->k:Landroid/util/SparseArray;

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    .line 125
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::DownloadTask()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 127
    iput-object p3, p0, Lcom/mfluent/asp/filetransfer/a;->h:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    .line 128
    iput-object p4, p0, Lcom/mfluent/asp/filetransfer/a;->i:Ljava/io/File;

    .line 129
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->j:Lcom/mfluent/asp/datamodel/t;

    .line 130
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    .line 131
    return-void
.end method

.method static synthetic D()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    return-object v0
.end method

.method private a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/a$b;Lcom/mfluent/asp/filetransfer/a$a;)Lcom/mfluent/asp/filetransfer/a$c;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    .line 769
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::getAspInputStream(): sourceDevice:{}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 771
    iget-object v0, p2, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-lez v0, :cond_0

    .line 772
    iget-object v0, p2, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/filetransfer/a;->bytesTransferred(J)V

    .line 777
    :cond_0
    iget-object v0, p3, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    if-eqz v0, :cond_5

    .line 779
    iget-object v0, p3, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    check-cast v0, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    .line 781
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->e()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v1

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-eq v1, v3, :cond_1

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v1

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v1, v3, :cond_4

    .line 783
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "contents/file/"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 791
    :goto_0
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    invoke-static {v0}, Lcom/mfluent/asp/nts/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 793
    sget-object v1, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v3, "::getAspInputStream: Sending download file request: {}"

    invoke-interface {v1, v3, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 795
    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v3, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 796
    iget-object v0, p2, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-lez v0, :cond_2

    .line 798
    const-string v0, "Range"

    iget-object v1, p2, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-static {v1, p3}, Lcom/mfluent/asp/filetransfer/a;->a(Ljava/io/File;Lcom/mfluent/asp/filetransfer/a$a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    :cond_2
    const/4 v1, 0x1

    .line 802
    new-instance v4, Lpcloud/net/a/a/a;

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lpcloud/net/a/a/a;-><init>(Ljava/lang/String;)V

    .line 804
    :try_start_0
    invoke-interface {v4, v3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 805
    sget-object v3, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v5, "::getAspInputStream: Got response status line: {}"

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 807
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    const/16 v5, 0xc8

    if-ne v3, v5, :cond_6

    .line 808
    iget-object v3, p2, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v6

    cmp-long v3, v6, v8

    if-lez v3, :cond_3

    .line 810
    iget-object v3, p2, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 811
    const-wide/16 v6, -0x1

    iget-object v3, p2, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v8

    mul-long/2addr v6, v8

    invoke-virtual {p0, v6, v7}, Lcom/mfluent/asp/filetransfer/a;->bytesTransferred(J)V

    .line 819
    :cond_3
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 820
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 823
    :try_start_1
    new-instance v3, Lcom/mfluent/asp/filetransfer/a$c;

    const/4 v5, 0x0

    invoke-direct {v3, v5}, Lcom/mfluent/asp/filetransfer/a$c;-><init>(B)V

    .line 824
    new-instance v5, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;

    invoke-direct {v5, v1, v4}, Lcom/mfluent/asp/common/io/util/InputStreamWithHttpClient;-><init>(Ljava/io/InputStream;Lorg/apache/http/client/HttpClient;)V

    iput-object v5, v3, Lcom/mfluent/asp/filetransfer/a$c;->a:Ljava/io/InputStream;

    .line 825
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    iput-wide v0, v3, Lcom/mfluent/asp/filetransfer/a$c;->b:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 828
    return-object v3

    .line 785
    :cond_4
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 788
    :cond_5
    iget-object v0, p3, Lcom/mfluent/asp/filetransfer/a$a;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 813
    :cond_6
    :try_start_2
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    const/16 v5, 0xce

    if-eq v3, v5, :cond_3

    .line 816
    new-instance v2, Ljava/lang/Exception;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Got error response from download request: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 828
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_7

    .line 829
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    :cond_7
    throw v0

    .line 828
    :catchall_1
    move-exception v0

    move v1, v2

    goto :goto_1
.end method

.method private static a(Ljava/io/File;Lcom/mfluent/asp/filetransfer/a$a;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 835
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 836
    iget-wide v2, p1, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    .line 837
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "bytes="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/database/Cursor;Lcom/mfluent/asp/filetransfer/a$b;)V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const-wide/16 v10, -0x1

    const/4 v8, 0x0

    .line 248
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::addVideoPairedFiles()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 250
    const-string v0, "caption_type"

    invoke-static {p0, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    sget-object v1, Lcom/sec/pcw/util/Common;->s:[Ljava/lang/String;

    invoke-static {v1, v0}, Lorg/apache/commons/lang3/ArrayUtils;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 258
    if-ltz v0, :cond_0

    .line 259
    const-string v1, "caption_uri"

    invoke-static {p0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 260
    iget-object v2, p1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    invoke-static {v2}, Lorg/apache/commons/io/FilenameUtils;->getBaseName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 262
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 263
    new-instance v3, Lcom/mfluent/asp/filetransfer/a$a;

    invoke-direct {v3, v8}, Lcom/mfluent/asp/filetransfer/a$a;-><init>(B)V

    .line 264
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/sec/pcw/util/Common;->q:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    .line 265
    iput-object v1, v3, Lcom/mfluent/asp/filetransfer/a$a;->a:Ljava/lang/String;

    .line 266
    iput-wide v10, v3, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    .line 267
    const/16 v1, 0xd

    iput v1, v3, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    .line 268
    iget-object v1, p1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->b:Ljava/lang/String;

    iput-object v1, v3, Lcom/mfluent/asp/filetransfer/a$a;->b:Ljava/lang/String;

    .line 269
    new-instance v1, Ljava/io/File;

    iget-object v4, p1, Lcom/mfluent/asp/filetransfer/a$b;->d:Ljava/io/File;

    iget-object v5, v3, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    invoke-direct {v1, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, v3, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    .line 271
    iget-object v1, p1, Lcom/mfluent/asp/filetransfer/a$b;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    iget-wide v4, v3, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    cmp-long v1, v4, v12

    if-lez v1, :cond_2

    .line 273
    iget-wide v4, p1, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    iget-wide v6, v3, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    add-long/2addr v4, v6

    iput-wide v4, p1, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    .line 276
    :cond_2
    const-string v1, "caption_index_uri"

    invoke-static {p0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 277
    sget-object v3, Lcom/sec/pcw/util/Common;->r:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 278
    new-instance v3, Lcom/mfluent/asp/filetransfer/a$a;

    invoke-direct {v3, v8}, Lcom/mfluent/asp/filetransfer/a$a;-><init>(B)V

    .line 279
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lcom/sec/pcw/util/Common;->r:[Ljava/lang/String;

    aget-object v0, v4, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    .line 280
    iput-object v1, v3, Lcom/mfluent/asp/filetransfer/a$a;->a:Ljava/lang/String;

    .line 281
    iput-wide v10, v3, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    .line 282
    const/16 v0, 0xe

    iput v0, v3, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    .line 283
    iget-object v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$a;->b:Ljava/lang/String;

    iput-object v0, v3, Lcom/mfluent/asp/filetransfer/a$a;->b:Ljava/lang/String;

    .line 284
    new-instance v0, Ljava/io/File;

    iget-object v1, p1, Lcom/mfluent/asp/filetransfer/a$b;->d:Ljava/io/File;

    iget-object v2, v3, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, v3, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    .line 286
    iget-object v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 287
    iget-wide v0, v3, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    cmp-long v0, v0, v12

    if-lez v0, :cond_0

    .line 288
    iget-wide v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    iget-wide v2, v3, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    add-long/2addr v0, v2

    iput-wide v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    goto/16 :goto_0
.end method

.method private static a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/a$b;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 381
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::addVideoPairedFilesForStorageTask() : sourceDevice:{}"

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 383
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v6

    .line 384
    if-nez v6, :cond_1

    .line 386
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->t()V

    .line 424
    :cond_0
    :goto_0
    return-void

    .line 390
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v1

    .line 392
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "caption_uri IS NOT NULL"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 397
    if-eqz v1, :cond_0

    .line 399
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 401
    :cond_2
    const-string v0, "media_type"

    invoke-static {v1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getIntOrThrow(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    .line 402
    const-string v2, "source_media_id"

    invoke-static {v1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 403
    const-string v3, "full_uri"

    invoke-static {v1, v3}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 407
    :try_start_1
    invoke-interface {v6, v0, v3, v2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getStorageGatewayFileId(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 413
    :try_start_2
    iget-object v2, p1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/a$a;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 415
    invoke-static {v1, p1}, Lcom/mfluent/asp/filetransfer/a;->a(Landroid/database/Cursor;Lcom/mfluent/asp/filetransfer/a$b;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 421
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 408
    :catch_0
    move-exception v0

    .line 409
    :try_start_3
    sget-object v2, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FileNotFound for media "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/database/DatabaseUtils;->dumpCurrentRowToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 418
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 421
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Lcom/mfluent/asp/filetransfer/a$b;Lcom/mfluent/asp/filetransfer/a$a;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 613
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::doItemTransfer()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 615
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->i:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 617
    iget-object v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    .line 618
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/a;->i:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DownloadTask.tmp."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p2, Lcom/mfluent/asp/filetransfer/a$a;->j:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    .line 620
    if-eqz v0, :cond_0

    iget-object v1, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 621
    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    .line 624
    :cond_0
    iget-object v5, p1, Lcom/mfluent/asp/filetransfer/a$b;->a:Lcom/mfluent/asp/datamodel/Device;

    .line 627
    const/4 v2, 0x0

    .line 628
    const/4 v0, 0x0

    .line 629
    sget-object v1, Lcom/mfluent/asp/filetransfer/a$1;->a:[I

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 652
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "::doItemTransfer: unsupported transport: {}"

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    .line 653
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to download "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": unsupported device transport type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 633
    :pswitch_0
    :try_start_0
    invoke-direct {p0, v5, p1, p2}, Lcom/mfluent/asp/filetransfer/a;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/a$b;Lcom/mfluent/asp/filetransfer/a$a;)Lcom/mfluent/asp/filetransfer/a$c;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v2

    .line 659
    :cond_1
    :goto_0
    sget v2, Lcom/mfluent/asp/ASPApplication;->b:I

    if-lez v2, :cond_6

    .line 660
    if-nez v0, :cond_7

    .line 661
    if-eqz v1, :cond_1d

    .line 662
    throw v1

    .line 634
    :catch_0
    move-exception v1

    .line 635
    sget v3, Lcom/mfluent/asp/ASPApplication;->b:I

    if-lez v3, :cond_1

    .line 636
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "404 Not Found"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 637
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "::doItemTransfer-SysDBGTest-Return HTTP/1.1 404 error caused by Sync error"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 638
    const/4 v0, 0x0

    .line 639
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ag()I

    move-object v1, v2

    goto :goto_0

    .line 649
    :pswitch_1
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::getCloudInputStream(): sourceDevice:{}"

    invoke-interface {v0, v1, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "CloudStorageSync instance not found."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    invoke-interface {v1}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->isRangeDownloadSupported()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_3

    iget-object v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lcom/mfluent/asp/filetransfer/a;->bytesTransferred(J)V

    iget-object v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-static {v0, p2}, Lcom/mfluent/asp/filetransfer/a;->a(Ljava/io/File;Lcom/mfluent/asp/filetransfer/a$a;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    iget-object v3, p2, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    if-eqz v3, :cond_4

    iget-object v3, p2, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    invoke-interface {v1, v3, v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getFile(Lcom/mfluent/asp/common/datamodel/ASPFile;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;

    move-result-object v0

    :goto_1
    new-instance v1, Lcom/mfluent/asp/filetransfer/a$c;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/mfluent/asp/filetransfer/a$c;-><init>(B)V

    invoke-interface {v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    iput-object v3, v1, Lcom/mfluent/asp/filetransfer/a$c;->a:Ljava/io/InputStream;

    invoke-interface {v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;->getContentLength()J

    move-result-wide v6

    iput-wide v6, v1, Lcom/mfluent/asp/filetransfer/a$c;->b:J

    move-object v0, v1

    move-object v1, v2

    .line 650
    goto :goto_0

    .line 649
    :cond_4
    iget-object v3, p2, Lcom/mfluent/asp/filetransfer/a$a;->e:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, p2, Lcom/mfluent/asp/filetransfer/a$a;->e:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getFile(Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;

    move-result-object v0

    goto :goto_1

    :cond_5
    iget v3, p2, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    iget-object v4, p2, Lcom/mfluent/asp/filetransfer/a$a;->a:Ljava/lang/String;

    iget-object v6, p2, Lcom/mfluent/asp/filetransfer/a$a;->b:Ljava/lang/String;

    invoke-interface {v1, v3, v4, v6, v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getFile(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStreamInfo;

    move-result-object v0

    goto :goto_1

    .line 667
    :cond_6
    if-eqz v1, :cond_7

    .line 668
    throw v1

    .line 671
    :cond_7
    const/4 v2, 0x0

    .line 672
    const/4 v3, 0x0

    .line 676
    :try_start_1
    iget-object v1, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    const/4 v4, 0x1

    invoke-static {v1, v4}, Lcom/mfluent/asp/util/b;->a(Ljava/io/File;Z)Ljava/io/FileOutputStream;

    move-result-object v3

    .line 677
    iget-wide v6, p2, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-lez v1, :cond_21

    .line 678
    new-instance v4, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;

    invoke-direct {v4, v3, p0}, Lcom/mfluent/asp/common/io/util/ProgressUpdatingOutputStream;-><init>(Ljava/io/OutputStream;Lcom/mfluent/asp/common/io/util/StreamProgressListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 680
    :goto_2
    :try_start_2
    new-instance v3, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;

    invoke-direct {v3, v4}, Lcom/mfluent/asp/common/io/util/InterruptibleOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 682
    :try_start_3
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$c;->a:Ljava/io/InputStream;

    invoke-static {v1, v3}, Lorg/apache/commons/io/IOUtils;->copyLarge(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    move-result-wide v6

    .line 685
    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 686
    invoke-static {v3}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 687
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$c;->a:Ljava/io/InputStream;

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 690
    iget-wide v0, v0, Lcom/mfluent/asp/filetransfer/a$c;->b:J

    .line 691
    cmp-long v2, v0, v6

    if-eqz v2, :cond_8

    .line 693
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "only "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " byte(s) were transferred!!!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 685
    :catchall_0
    move-exception v1

    :goto_3
    invoke-static {v3}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 686
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    .line 687
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$c;->a:Ljava/io/InputStream;

    invoke-static {v0}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    throw v1

    .line 697
    :cond_8
    const/4 v2, 0x0

    .line 700
    :try_start_4
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->W()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 701
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->i:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DownloadTask.tmp.decrypted."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p2, Lcom/mfluent/asp/filetransfer/a$a;->j:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 702
    :try_start_5
    invoke-static {v1}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 705
    :try_start_6
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->j:Lcom/mfluent/asp/datamodel/t;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;
    :try_end_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v0

    .line 710
    :try_start_7
    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v2

    iget-object v3, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v4, v0}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->DecryptSecureData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 715
    if-eqz v0, :cond_9

    .line 716
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Got failure code "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " when trying to decrypt "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " after secure download"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 732
    :catchall_1
    move-exception v0

    :goto_4
    iget-object v2, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-static {v2}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    .line 733
    invoke-static {v1}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    .line 735
    const/4 v1, 0x0

    iput-object v1, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    .line 736
    throw v0

    .line 706
    :catch_1
    move-exception v0

    :try_start_8
    throw v0

    .line 719
    :cond_9
    iget-object v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    .line 720
    iput-object v1, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    .line 724
    :goto_5
    iget-object v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-static {v0}, Lcom/mfluent/asp/util/b;->d(Ljava/io/File;)J

    move-result-wide v2

    .line 725
    iget-object v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    iget-object v4, p2, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-static {v0, v4}, Lcom/mfluent/asp/util/b;->b(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 726
    iget-object v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    iget-object v4, p2, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-static {v0, v4}, Lcom/mfluent/asp/util/b;->a(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p2, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    .line 727
    iget-object v4, p1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v6, "Enter ::logAnalyticsDataForDownloading() : downloadItem.mediaType:{} Target Path:{} sourceDevice:{}"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, v4, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, v4, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    aput-object v5, v7, v8

    invoke-interface {v0, v6, v7}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    iget v0, v4, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    iget-object v4, v4, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v4

    const-string v6, "files"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    const/4 v0, 0x0

    :cond_a
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ah()I

    move-result v4

    invoke-static {v4}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(I)Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    move-result-object v4

    sget-object v6, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->h:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    if-eq v4, v6, :cond_b

    if-nez v5, :cond_d

    :cond_b
    sget-object v4, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v6, "Invalid value. clientAppId:{} mediaType:{} sourceDevice:{}"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ah()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v8

    const/4 v0, 0x2

    aput-object v5, v7, v0

    invoke-interface {v4, v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 730
    :cond_c
    :goto_6
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->L()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 732
    iget-object v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    .line 733
    invoke-static {v1}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    .line 735
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    .line 740
    iget-object v0, p2, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-static {v0}, Lcom/mfluent/asp/util/b;->d(Ljava/io/File;)J

    move-result-wide v0

    .line 741
    cmp-long v4, v2, v0

    if-eqz v4, :cond_1c

    .line 742
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "only "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " byte(s) were copied!!!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 727
    :cond_d
    :try_start_9
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->b(I)Z

    move-result v5

    if-eqz v5, :cond_e

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->p:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_6

    :cond_e
    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->c(I)Z

    move-result v5

    if-eqz v5, :cond_f

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->q:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_6

    :cond_f
    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->d(I)Z

    move-result v5

    if-eqz v5, :cond_10

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->r:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_6

    :cond_10
    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->e(I)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->t:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_6

    :cond_11
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->s:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_6

    :cond_12
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v5, v6, :cond_17

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->b(I)Z

    move-result v5

    if-eqz v5, :cond_13

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->u:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_6

    :cond_13
    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->c(I)Z

    move-result v5

    if-eqz v5, :cond_14

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->v:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_6

    :cond_14
    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->d(I)Z

    move-result v5

    if-eqz v5, :cond_15

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->w:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_6

    :cond_15
    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->e(I)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->y:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_6

    :cond_16
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->x:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_6

    :cond_17
    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->b(I)Z

    move-result v5

    if-eqz v5, :cond_18

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->z:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_6

    :cond_18
    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->c(I)Z

    move-result v5

    if-eqz v5, :cond_19

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->A:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_6

    :cond_19
    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->d(I)Z

    move-result v5

    if-eqz v5, :cond_1a

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->B:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_6

    :cond_1a
    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/a;->e(I)Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->D:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_6

    :cond_1b
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->l:Landroid/content/Context;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->C:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_6

    .line 745
    :cond_1c
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->W()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->temporary:Z

    if-eqz v0, :cond_1e

    .line 746
    iget-object v0, p2, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 747
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->h:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    iget-object v2, p2, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/media/MediaScannerConnection$OnScanCompletedListener;->onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V

    .line 758
    :cond_1d
    :goto_7
    return-void

    .line 751
    :cond_1e
    if-eqz p3, :cond_1d

    .line 752
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->O()Landroid/content/Context;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v3, p2, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iget-object v0, p2, Lcom/mfluent/asp/filetransfer/a$a;->g:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1f

    const/4 v0, 0x0

    :goto_8
    new-instance v3, Lcom/mfluent/asp/filetransfer/a$d;

    iget-object v4, p0, Lcom/mfluent/asp/filetransfer/a;->h:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    invoke-direct {v3, p0, v4, p2}, Lcom/mfluent/asp/filetransfer/a$d;-><init>(Lcom/mfluent/asp/filetransfer/a;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Lcom/mfluent/asp/filetransfer/a$a;)V

    invoke-static {v1, v2, v0, v3}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    goto :goto_7

    :cond_1f
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p2, Lcom/mfluent/asp/filetransfer/a$a;->g:Ljava/lang/String;

    aput-object v4, v0, v3

    goto :goto_8

    .line 732
    :catchall_2
    move-exception v0

    move-object v1, v2

    goto/16 :goto_4

    .line 685
    :catchall_3
    move-exception v1

    move-object v3, v4

    goto/16 :goto_3

    :catchall_4
    move-exception v1

    move-object v2, v3

    move-object v3, v4

    goto/16 :goto_3

    :cond_20
    move-object v1, v2

    goto/16 :goto_5

    :cond_21
    move-object v4, v3

    goto/16 :goto_2

    .line 629
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private ak()Ljava/io/File;
    .locals 3

    .prologue
    .line 228
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::getTargetPrivateDirectory()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 230
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->O()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, ".tmp"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 231
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 234
    new-instance v1, Ljava/io/File;

    const-string v2, ".nomedia"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 235
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 237
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    :cond_0
    :goto_0
    return-object v0

    .line 238
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private f(I)Ljava/io/File;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 194
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::getTargetDirectory() : mediaType:{}"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 197
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->targetDirectory:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 198
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->targetDirectory:Ljava/io/File;

    .line 223
    :cond_0
    :goto_0
    return-object v0

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->k:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 202
    if-nez v0, :cond_0

    .line 203
    sparse-switch p1, :sswitch_data_0

    .line 217
    invoke-static {}, Lcom/mfluent/a/a/b;->g()Ljava/io/File;

    move-result-object v0

    .line 220
    :goto_1
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->k:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 205
    :sswitch_0
    invoke-static {v3}, Lcom/mfluent/a/a/b;->b(Z)Ljava/io/File;

    move-result-object v0

    goto :goto_1

    .line 208
    :sswitch_1
    invoke-static {v3}, Lcom/mfluent/a/a/b;->c(Z)Ljava/io/File;

    move-result-object v0

    goto :goto_1

    .line 211
    :sswitch_2
    invoke-static {v3}, Lcom/mfluent/a/a/b;->a(Z)Ljava/io/File;

    move-result-object v0

    goto :goto_1

    .line 214
    :sswitch_3
    invoke-static {}, Lcom/mfluent/a/a/b;->h()Ljava/io/File;

    move-result-object v0

    goto :goto_1

    .line 203
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0xf -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method protected final A()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 458
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::doTransfer()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 460
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/a$b;

    .line 461
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->f:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 463
    iget-wide v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/filetransfer/a;->bytesTransferred(J)V

    goto :goto_0

    .line 467
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v1

    iget-boolean v1, v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->transferVideoPairedFilesOnly:Z

    if-eqz v1, :cond_3

    .line 469
    sget-object v1, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v2, "::doTransfer: skip file due to transferVideoPairedFilesOnly options: {}"

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v3, v3, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 472
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/filetransfer/a$a;

    .line 473
    invoke-direct {p0, v0, v1, v11}, Lcom/mfluent/asp/filetransfer/a;->a(Lcom/mfluent/asp/filetransfer/a$b;Lcom/mfluent/asp/filetransfer/a$a;Z)V

    .line 475
    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/a;->h:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    if-eqz v3, :cond_1

    .line 476
    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/a;->h:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    iget-object v4, v1, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v3, v4, v1}, Landroid/media/MediaScannerConnection$OnScanCompletedListener;->onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_1

    .line 482
    :cond_2
    iget-wide v2, v0, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    invoke-virtual {p0, v2, v3}, Lcom/mfluent/asp/filetransfer/a;->bytesTransferred(J)V

    .line 484
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->f:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 485
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 489
    :cond_3
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v1

    iget-boolean v1, v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->skipIfDuplicate:Z

    if-eqz v1, :cond_5

    .line 490
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    if-eqz v1, :cond_8

    .line 491
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 492
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    invoke-interface {v1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->length()J

    move-result-wide v2

    cmp-long v1, v2, v12

    if-gtz v1, :cond_7

    .line 493
    iput-boolean v10, v0, Lcom/mfluent/asp/filetransfer/a$b;->e:Z

    .line 497
    :cond_4
    :goto_2
    iget-boolean v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->e:Z

    if-eqz v1, :cond_5

    .line 498
    sget-object v1, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v2, "::doTransfer: duplicate file: {}"

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v3, v3, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 571
    :cond_5
    :goto_3
    iget-boolean v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->e:Z

    if-eqz v1, :cond_c

    .line 572
    iget-wide v2, v0, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    invoke-virtual {p0, v2, v3}, Lcom/mfluent/asp/filetransfer/a;->bytesTransferred(J)V

    .line 573
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->h:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    if-eqz v1, :cond_6

    .line 574
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->f:Landroid/net/Uri;

    if-nez v1, :cond_b

    .line 575
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->O()Landroid/content/Context;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/String;

    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v11

    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->g:Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    move-object v1, v8

    :goto_4
    new-instance v4, Lcom/mfluent/asp/filetransfer/a$d;

    iget-object v5, p0, Lcom/mfluent/asp/filetransfer/a;->h:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    iget-object v6, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    invoke-direct {v4, p0, v5, v6}, Lcom/mfluent/asp/filetransfer/a$d;-><init>(Lcom/mfluent/asp/filetransfer/a;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Lcom/mfluent/asp/filetransfer/a$a;)V

    invoke-static {v2, v3, v1, v4}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 584
    :cond_6
    :goto_5
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->f:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 585
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 494
    :cond_7
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    invoke-interface {v1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->length()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    .line 495
    iput-boolean v10, v0, Lcom/mfluent/asp/filetransfer/a$b;->e:Z

    goto :goto_2

    .line 502
    :cond_8
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->j:Lcom/mfluent/asp/datamodel/t;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v3, v1, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->l:I

    int-to-long v4, v1

    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->k:I

    int-to-long v6, v1

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/mfluent/asp/filetransfer/a;->a(IIJJ)Ljava/lang/String;

    move-result-object v2

    .line 508
    if-eqz v2, :cond_9

    .line 509
    iput-boolean v10, v0, Lcom/mfluent/asp/filetransfer/a$b;->e:Z

    .line 510
    sget-object v1, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v3, "::doTransfer: duplicate file: {}, sourceMediaId:{}"

    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v4, v4, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    invoke-interface {v1, v3, v4, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 513
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    sparse-switch v1, :sswitch_data_0

    move-object v1, v8

    .line 528
    :goto_6
    if-eqz v1, :cond_5

    .line 529
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 530
    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->f:Landroid/net/Uri;

    goto/16 :goto_3

    .line 515
    :sswitch_0
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_6

    .line 518
    :sswitch_1
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_6

    .line 521
    :sswitch_2
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_6

    .line 524
    :sswitch_3
    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_6

    .line 535
    :cond_9
    :try_start_0
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 536
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-wide v4, v1, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_5

    .line 537
    sget-object v1, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::it seems duplicated file "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v3, v3, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",len="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-wide v4, v3, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 539
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    sparse-switch v1, :sswitch_data_1

    move-object v1, v8

    .line 554
    :goto_7
    if-eqz v1, :cond_5

    .line 555
    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {p0, v2, v1}, Lcom/mfluent/asp/filetransfer/a;->a(Ljava/io/File;Landroid/net/Uri;)J

    move-result-wide v2

    .line 556
    cmp-long v4, v2, v12

    if-lez v4, :cond_5

    .line 557
    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->f:Landroid/net/Uri;

    .line 558
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->e:Z

    .line 559
    sget-object v1, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::task.duplicateMediaStoreUri="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/a$b;->f:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    .line 564
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 541
    :sswitch_4
    :try_start_1
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_7

    .line 544
    :sswitch_5
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_7

    .line 547
    :sswitch_6
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_7

    .line 550
    :sswitch_7
    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_7

    .line 575
    :cond_a
    new-array v1, v10, [Ljava/lang/String;

    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v4, v4, Lcom/mfluent/asp/filetransfer/a$a;->g:Ljava/lang/String;

    aput-object v4, v1, v11

    goto/16 :goto_4

    .line 581
    :cond_b
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->h:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/a$b;->f:Landroid/net/Uri;

    invoke-interface {v1, v2, v3}, Landroid/media/MediaScannerConnection$OnScanCompletedListener;->onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_5

    .line 589
    :cond_c
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->G()V

    .line 592
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/filetransfer/a$a;

    .line 593
    invoke-direct {p0, v0, v1, v11}, Lcom/mfluent/asp/filetransfer/a;->a(Lcom/mfluent/asp/filetransfer/a$b;Lcom/mfluent/asp/filetransfer/a$a;Z)V

    goto :goto_8

    .line 596
    :cond_d
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    invoke-direct {p0, v0, v1, v10}, Lcom/mfluent/asp/filetransfer/a;->a(Lcom/mfluent/asp/filetransfer/a$b;Lcom/mfluent/asp/filetransfer/a$a;Z)V

    .line 597
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->f:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 600
    :cond_e
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->deleteSourceFilesWhenTransferIsComplete:Z

    if-eqz v0, :cond_10

    .line 601
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/a$b;

    .line 602
    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    if-eqz v2, :cond_f

    .line 603
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v2, "TransferOptions.deleteSourceFilesWhenTransferIsComplete for non media files is not supported"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    goto :goto_9

    .line 605
    :cond_f
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->O()Landroid/content/Context;

    move-result-object v2

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v0, v0, Lcom/mfluent/asp/filetransfer/a$a;->k:I

    int-to-long v4, v0

    invoke-static {v2, v4, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;->deleteFile(Landroid/content/Context;J)V

    goto :goto_9

    .line 609
    :cond_10
    return-void

    .line 513
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0xf -> :sswitch_3
    .end sparse-switch

    .line 539
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_6
        0x3 -> :sswitch_5
        0xf -> :sswitch_7
    .end sparse-switch
.end method

.method public final B()V
    .locals 2

    .prologue
    .line 874
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/a$b;

    .line 875
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->g:Ljava/io/File;

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    goto :goto_0

    .line 877
    :cond_0
    return-void
.end method

.method public final C()Ljava/lang/String;
    .locals 7

    .prologue
    .line 881
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 882
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/a$b;

    .line 884
    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/a;->f:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 885
    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/a$b;->a:Lcom/mfluent/asp/datamodel/Device;

    .line 888
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 889
    const-string v4, "."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 890
    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v4, v4, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    if-eqz v4, :cond_1

    .line 891
    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v4, v4, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    invoke-static {v4}, Lcom/mfluent/asp/util/b;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 899
    :goto_1
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/a$a;

    .line 900
    const-string v5, ";"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$a;->a:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 893
    :cond_1
    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v4, v4, Lcom/mfluent/asp/filetransfer/a$a;->k:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 902
    :cond_2
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 904
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/Cursor;Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 136
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::addTask()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 138
    if-nez p2, :cond_0

    .line 139
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Downloads must use SlinkMediaStore cursors"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    new-instance v0, Lcom/mfluent/asp/filetransfer/a$b;

    invoke-direct {v0, v2}, Lcom/mfluent/asp/filetransfer/a$b;-><init>(B)V

    .line 144
    new-instance v1, Lcom/mfluent/asp/filetransfer/a$a;

    invoke-direct {v1, v2}, Lcom/mfluent/asp/filetransfer/a$a;-><init>(B)V

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    .line 145
    const-string v1, "device_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 146
    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/a;->j:Lcom/mfluent/asp/datamodel/t;

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->a:Lcom/mfluent/asp/datamodel/Device;

    .line 148
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    .line 149
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "media_type"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    .line 150
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "_display_name"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    .line 153
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "<unknown>"

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    .line 156
    :cond_1
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "mime_type"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->g:Ljava/lang/String;

    .line 157
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "full_uri"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->a:Ljava/lang/String;

    .line 158
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "source_media_id"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->b:Ljava/lang/String;

    .line 159
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "_size"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    .line 160
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "_id"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->k:I

    .line 161
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "dup_id"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->l:I

    .line 162
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "title"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->m:Ljava/lang/String;

    .line 164
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    invoke-direct {p0, v1}, Lcom/mfluent/asp/filetransfer/a;->f(I)Ljava/io/File;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->d:Ljava/io/File;

    .line 165
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    sparse-switch v1, :sswitch_data_0

    .line 177
    sget-object v1, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v2, "::addTask: nsupported mediaType {}"

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v3, v3, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    .line 179
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported mediaType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v0, v0, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 170
    :sswitch_0
    invoke-static {p1, v0}, Lcom/mfluent/asp/filetransfer/a;->a(Landroid/database/Cursor;Lcom/mfluent/asp/filetransfer/a$b;)V

    .line 182
    :goto_0
    :sswitch_1
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    new-instance v2, Ljava/io/File;

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/a$b;->d:Ljava/io/File;

    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v4, v4, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    .line 184
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-wide v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    iput-wide v2, v0, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    .line 186
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->k()J

    move-result-wide v2

    iget-wide v4, v0, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    add-long/2addr v2, v4

    invoke-virtual {p0, v2, v3}, Lcom/mfluent/asp/filetransfer/a;->a(J)V

    .line 188
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/filetransfer/a;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 190
    return-void

    .line 173
    :sswitch_2
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "artist"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->n:Ljava/lang/String;

    .line 174
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    const-string v2, "album"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->o:Ljava/lang/String;

    goto :goto_0

    .line 165
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_0
        0xf -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 298
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::addTask() ASPFile:{} sourceDevice:{}"

    invoke-interface {v0, v1, p1, p2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 300
    new-instance v1, Lcom/mfluent/asp/filetransfer/a$b;

    invoke-direct {v1, v4}, Lcom/mfluent/asp/filetransfer/a$b;-><init>(B)V

    .line 302
    new-instance v0, Lcom/mfluent/asp/filetransfer/a$a;

    invoke-direct {v0, v4}, Lcom/mfluent/asp/filetransfer/a$a;-><init>(B)V

    iput-object v0, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    .line 303
    iget-object v0, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iput-object p1, v0, Lcom/mfluent/asp/filetransfer/a$a;->d:Lcom/mfluent/asp/common/datamodel/ASPFile;

    .line 304
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->W()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->temporary:Z

    if-eqz v0, :cond_1

    .line 305
    iget-object v0, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "(copy)"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    .line 309
    :goto_0
    iput-boolean v4, v1, Lcom/mfluent/asp/filetransfer/a$b;->e:Z

    .line 310
    iput-object v5, v1, Lcom/mfluent/asp/filetransfer/a$b;->f:Landroid/net/Uri;

    .line 311
    iget-object v0, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iput-object v5, v0, Lcom/mfluent/asp/filetransfer/a$a;->a:Ljava/lang/String;

    .line 312
    iget-object v0, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iput-object v5, v0, Lcom/mfluent/asp/filetransfer/a$a;->b:Ljava/lang/String;

    .line 313
    iput-object p2, v1, Lcom/mfluent/asp/filetransfer/a$b;->a:Lcom/mfluent/asp/datamodel/Device;

    .line 315
    iget-object v0, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    .line 316
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->W()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 317
    const-string v2, "enc"

    invoke-static {v0}, Lorg/apache/commons/io/FilenameUtils;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 318
    invoke-static {v0}, Lorg/apache/commons/io/FilenameUtils;->removeExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 323
    :cond_0
    iget-object v2, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    invoke-static {v0}, Lcom/mfluent/asp/util/UiUtils;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/mfluent/asp/filetransfer/a$a;->g:Ljava/lang/String;

    .line 324
    iget-object v2, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v3, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v3, v3, Lcom/mfluent/asp/filetransfer/a$a;->g:Ljava/lang/String;

    invoke-static {v3}, Lcom/mfluent/asp/util/UiUtils;->c(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    .line 325
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->W()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->temporary:Z

    if-eqz v2, :cond_2

    .line 326
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/a;->ak()Ljava/io/File;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$b;->d:Ljava/io/File;

    .line 330
    :goto_1
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->targetDirectory:Ljava/io/File;

    if-eqz v2, :cond_3

    .line 331
    iget-object v2, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->targetDirectory:Ljava/io/File;

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v3, v2, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    .line 335
    :goto_2
    iget-object v0, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->length()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    .line 337
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->length()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    .line 339
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->k()J

    move-result-wide v2

    iget-wide v4, v1, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    add-long/2addr v2, v4

    invoke-virtual {p0, v2, v3}, Lcom/mfluent/asp/filetransfer/a;->a(J)V

    .line 341
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 342
    invoke-virtual {p0, p2}, Lcom/mfluent/asp/filetransfer/a;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 343
    return-void

    .line 307
    :cond_1
    iget-object v0, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 328
    :cond_2
    invoke-direct {p0, v4}, Lcom/mfluent/asp/filetransfer/a;->f(I)Ljava/io/File;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$b;->d:Ljava/io/File;

    goto :goto_1

    .line 333
    :cond_3
    iget-object v2, v1, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    new-instance v3, Ljava/io/File;

    iget-object v4, v1, Lcom/mfluent/asp/filetransfer/a$b;->d:Ljava/io/File;

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v3, v2, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    goto :goto_2
.end method

.method public final a(Lcom/mfluent/asp/filetransfer/l;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 347
    sget-object v0, Lcom/mfluent/asp/filetransfer/a;->d:Lorg/slf4j/Logger;

    const-string v1, "Enter ::addTask() : storageGatewayFileInfo:{} sourceDevice:{}"

    invoke-interface {v0, v1, p1, p2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 349
    new-instance v0, Lcom/mfluent/asp/filetransfer/a$b;

    invoke-direct {v0, v3}, Lcom/mfluent/asp/filetransfer/a$b;-><init>(B)V

    .line 351
    iput-object p2, v0, Lcom/mfluent/asp/filetransfer/a$b;->a:Lcom/mfluent/asp/datamodel/Device;

    .line 352
    new-instance v1, Lcom/mfluent/asp/filetransfer/a$a;

    invoke-direct {v1, v3}, Lcom/mfluent/asp/filetransfer/a$a;-><init>(B)V

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    .line 353
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    invoke-virtual {p1}, Lcom/mfluent/asp/filetransfer/l;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->e:Ljava/lang/String;

    .line 354
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    invoke-virtual {p1}, Lcom/mfluent/asp/filetransfer/l;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    .line 355
    iput-boolean v3, v0, Lcom/mfluent/asp/filetransfer/a$b;->e:Z

    .line 356
    iput-object v4, v0, Lcom/mfluent/asp/filetransfer/a$b;->f:Landroid/net/Uri;

    .line 357
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iput-object v4, v1, Lcom/mfluent/asp/filetransfer/a$a;->a:Ljava/lang/String;

    .line 358
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iput-object v4, v1, Lcom/mfluent/asp/filetransfer/a$a;->b:Ljava/lang/String;

    .line 361
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    invoke-static {v2}, Lcom/mfluent/asp/util/UiUtils;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->g:Ljava/lang/String;

    .line 362
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/a$a;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/mfluent/asp/util/UiUtils;->c(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    .line 363
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    invoke-direct {p0, v1}, Lcom/mfluent/asp/filetransfer/a;->f(I)Ljava/io/File;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->d:Ljava/io/File;

    .line 364
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    new-instance v2, Ljava/io/File;

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/a$b;->d:Ljava/io/File;

    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v4, v4, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    .line 366
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    invoke-virtual {p1}, Lcom/mfluent/asp/filetransfer/l;->c()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    .line 367
    invoke-virtual {p1}, Lcom/mfluent/asp/filetransfer/l;->c()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    .line 369
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget v1, v1, Lcom/mfluent/asp/filetransfer/a$a;->c:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 370
    invoke-static {p2, v0}, Lcom/mfluent/asp/filetransfer/a;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/a$b;)V

    .line 373
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->k()J

    move-result-wide v2

    iget-wide v4, v0, Lcom/mfluent/asp/filetransfer/a$b;->h:J

    add-long/2addr v2, v4

    invoke-virtual {p0, v2, v3}, Lcom/mfluent/asp/filetransfer/a;->a(J)V

    .line 375
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 376
    invoke-virtual {p0, p2}, Lcom/mfluent/asp/filetransfer/a;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 377
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 982
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/a$b;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 428
    const/4 v0, 0x1

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 433
    const/4 v0, 0x0

    return v0
.end method

.method public final r()I
    .locals 10

    .prologue
    .line 999
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->l()J

    move-result-wide v4

    .line 1000
    const-wide/16 v2, 0x0

    .line 1001
    const/4 v0, 0x0

    .line 1002
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v0

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/a$b;

    .line 1003
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-wide v8, v0, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    add-long/2addr v2, v8

    .line 1004
    cmp-long v0, v4, v2

    if-lez v0, :cond_0

    .line 1005
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1009
    goto :goto_0

    .line 1011
    :cond_0
    return v1
.end method

.method public final s()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1016
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->r()I

    move-result v0

    .line 1017
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/a$b;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final t()J
    .locals 8

    .prologue
    .line 987
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->l()J

    move-result-wide v2

    .line 989
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->r()I

    move-result v4

    .line 990
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 991
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/a$b;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-wide v6, v0, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    sub-long/2addr v2, v6

    .line 990
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 994
    :cond_0
    return-wide v2
.end method

.method public final u()J
    .locals 2

    .prologue
    .line 1022
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/a;->r()I

    move-result v0

    .line 1023
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/a;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/a$b;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-wide v0, v0, Lcom/mfluent/asp/filetransfer/a$a;->i:J

    return-wide v0
.end method

.method public final z()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 448
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 449
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/a;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/a$b;

    .line 450
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$b;->b:Lcom/mfluent/asp/filetransfer/a$a;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/a$a;->h:Ljava/io/File;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 452
    :cond_0
    return-object v1
.end method
