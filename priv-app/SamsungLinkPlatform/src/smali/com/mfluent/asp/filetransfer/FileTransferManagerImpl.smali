.class public Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/filetransfer/d;
.implements Lcom/mfluent/asp/filetransfer/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$8;,
        Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;,
        Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;,
        Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static l:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static m:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final w:J


# instance fields
.field private final b:I

.field private final c:I

.field private final d:Landroid/content/Context;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:Ljava/util/concurrent/ExecutorService;

.field private final h:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lcom/mfluent/asp/filetransfer/c;",
            "Lcom/mfluent/asp/filetransfer/c;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/concurrent/locks/Lock;

.field private j:Landroid/content/BroadcastReceiver;

.field private k:Landroid/content/BroadcastReceiver;

.field private final n:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/filetransfer/f;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/dws/handlers/ap;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/dws/handlers/ap;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/filetransfer/f;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/filetransfer/f;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/io/File;

.field private final u:Landroid/os/Handler;

.field private v:Landroid/app/Notification;

.field private final x:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue",
            "<",
            "Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 81
    const-class v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    .line 109
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 110
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 144
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->w:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const v4, 0x7f0a00ee

    const/16 v2, 0xa

    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const/4 v0, 0x2

    iput v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b:I

    .line 91
    const/4 v0, 0x3

    iput v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->c:I

    .line 95
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->e:Ljava/util/concurrent/ExecutorService;

    .line 97
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->f:Ljava/util/concurrent/ExecutorService;

    .line 99
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->g:Ljava/util/concurrent/ExecutorService;

    .line 101
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->h:Ljava/util/concurrent/ConcurrentMap;

    .line 103
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    .line 112
    new-instance v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$1;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->n:Landroid/util/LruCache;

    .line 122
    new-instance v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$2;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->o:Landroid/util/LruCache;

    .line 130
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->p:Landroid/util/LruCache;

    .line 133
    new-instance v0, Landroid/util/LruCache;

    invoke-direct {v0, v2}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->q:Landroid/util/LruCache;

    .line 136
    new-instance v0, Landroid/util/LruCache;

    invoke-direct {v0, v2}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->r:Landroid/util/LruCache;

    .line 138
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    .line 142
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->u:Landroid/os/Handler;

    .line 188
    new-instance v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->x:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;

    .line 836
    new-instance v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->y:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;

    .line 191
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    .line 192
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/a/a/b;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    const-string v2, "filetransfer_cache"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->t:Ljava/io/File;

    .line 193
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->t:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 195
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 197
    const-string v1, "com.mfluent.asp.filetransfer.FileTransferManager.CANCELED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 199
    new-instance v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$3;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$3;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)V

    iput-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->j:Landroid/content/BroadcastReceiver;

    .line 211
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 213
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 214
    const-string v1, "com.mfluent.asp.filetransfer.FileTransferManager.RETRY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 216
    new-instance v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$4;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$4;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)V

    iput-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->k:Landroid/content/BroadcastReceiver;

    .line 233
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 236
    const-string v0, "slpf_pref_20"

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "fileTransferInProgress"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 240
    if-eqz v0, :cond_1

    .line 241
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 243
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f020103

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0246

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 251
    const-class v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 252
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->x:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;

    new-instance v4, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;

    invoke-direct {v4, p0, v3, v5}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;Ljava/lang/String;I)V

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;

    .line 253
    if-eqz v1, :cond_0

    .line 254
    invoke-virtual {v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;->b()I

    move-result v1

    invoke-virtual {v0, v4, v1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 256
    :cond_0
    invoke-virtual {v0, v3, v5, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 258
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->m()V

    .line 259
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->l()V

    .line 261
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1227
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::buildFileTransferTask(sourceDevice:{}, targetDevice:{}, onScanCompletedListener, options, bIsController:{})"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v6

    aput-object p2, v2, v7

    const/4 v3, 0x2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1234
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1237
    new-instance v0, Lcom/mfluent/asp/filetransfer/h;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p0, p3}, Lcom/mfluent/asp/filetransfer/h;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 1254
    :goto_0
    return-object v0

    .line 1239
    :cond_0
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1240
    new-instance v0, Lcom/mfluent/asp/filetransfer/a;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->t:Ljava/io/File;

    move-object v2, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/filetransfer/a;-><init>(Landroid/content/Context;Lcom/mfluent/asp/filetransfer/g;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Ljava/io/File;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 1242
    invoke-virtual {v0, v6, v7, v6}, Lcom/mfluent/asp/filetransfer/f;->a(ZZZ)V

    goto :goto_0

    .line 1243
    :cond_1
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1244
    new-instance v0, Lcom/mfluent/asp/filetransfer/n;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p0, p3}, Lcom/mfluent/asp/filetransfer/n;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 1245
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->P()V

    .line 1247
    invoke-virtual {v0, v6, v6, v6}, Lcom/mfluent/asp/filetransfer/f;->a(ZZZ)V

    goto :goto_0

    .line 1249
    :cond_2
    new-instance v0, Lcom/mfluent/asp/filetransfer/m;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p0, p3}, Lcom/mfluent/asp/filetransfer/m;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 1250
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->P()V

    .line 1252
    invoke-virtual {v0, v6, v6, v7}, Lcom/mfluent/asp/filetransfer/f;->a(ZZZ)V

    goto :goto_0
.end method

.method private a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/Device;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1359
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::transfer(mediaSet, targetDevice:{}, scanListener, options)"

    invoke-interface {v0, v1, p2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1361
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::buildFileTransferTask(mediaSet:{}, targetDevice:{}, onScanCompletedListener, options, bIsController:{})"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v7

    aput-object p2, v2, v6

    const/4 v3, 0x2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mfluent/asp/filetransfer/a;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    iget-object v4, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->t:Ljava/io/File;

    move-object v2, p0

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/filetransfer/a;-><init>(Landroid/content/Context;Lcom/mfluent/asp/filetransfer/g;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Ljava/io/File;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    invoke-virtual {v0, v6, v6, v7}, Lcom/mfluent/asp/filetransfer/f;->a(ZZZ)V

    :goto_0
    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v2, "::buildFileTransferTask() : ClientAppId:{}"

    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSlinkPlatformClientAppId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSlinkPlatformClientAppId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/filetransfer/f;->a(I)V

    .line 1362
    invoke-direct {p0, v0, p1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/mfluent/asp/filetransfer/f;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;)V

    .line 1363
    return-object v0

    .line 1361
    :cond_0
    new-instance v0, Lcom/mfluent/asp/util/x;

    invoke-direct {v0}, Lcom/mfluent/asp/util/x;-><init>()V

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/t;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v2, "::buildFileTransferTask() : sourceDevice:{}"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "can\'t upload or 3box transfer from multiple source devices"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/mfluent/asp/filetransfer/n;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p0, p4}, Lcom/mfluent/asp/filetransfer/n;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    invoke-virtual {v0, v6, v7, v7}, Lcom/mfluent/asp/filetransfer/f;->a(ZZZ)V

    :goto_1
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->P()V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/mfluent/asp/filetransfer/m;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-direct {v0, v1, p2, p0, p4}, Lcom/mfluent/asp/filetransfer/m;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    invoke-virtual {v0, v6, v7, v6}, Lcom/mfluent/asp/filetransfer/f;->a(ZZZ)V

    goto :goto_1
.end method

.method private a(Lcom/mfluent/asp/filetransfer/f;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;)V
    .locals 12

    .prologue
    .line 1368
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter FileTransferManagerImpl::transfer(task, mediaSet)"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 1370
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->isLocalFilePathsMediaSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1371
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    .line 1372
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getLocalFilePaths()[Ljava/lang/String;

    move-result-object v3

    .line 1373
    const/4 v0, 0x0

    :goto_0
    array-length v1, v3

    if-ge v0, v1, :cond_5

    .line 1375
    :try_start_0
    new-instance v1, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    new-instance v4, Ljava/io/File;

    aget-object v5, v3, v0

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    invoke-virtual {p1, v1, v4}, Lcom/mfluent/asp/filetransfer/f;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/datamodel/Device;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1373
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1376
    :catch_0
    move-exception v1

    .line 1377
    sget-object v4, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v5, "Unable to add File transfer task:"

    invoke-interface {v4, v5, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1380
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->isFileListUri(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1381
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 1382
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getDeviceIdFromUri(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v6

    .line 1384
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "FileTransferManagerImpl::transfer():sourceDevice:{}"

    invoke-interface {v0, v1, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1386
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$FileList;->getDirectoryIdFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 1387
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getSortOrder()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->getSortTypeFromCursorSortOrder(Ljava/lang/String;)Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    move-result-object v5

    .line 1388
    if-nez v5, :cond_1

    .line 1389
    invoke-static {}, Lcom/mfluent/asp/common/datamodel/ASPFileSortType;->getDefaultSortType()Lcom/mfluent/asp/common/datamodel/ASPFileSortType;

    move-result-object v5

    .line 1392
    :cond_1
    const/4 v1, 0x0

    .line 1395
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/mfluent/asp/datamodel/filebrowser/e;->a(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)Lcom/mfluent/asp/common/datamodel/ASPFileProvider;

    move-result-object v7

    .line 1396
    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a()Lcom/mfluent/asp/datamodel/filebrowser/d;

    move-result-object v0

    invoke-virtual {v0, v7, v4, v5}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/common/datamodel/ASPFileProvider;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;)Lcom/mfluent/asp/datamodel/filebrowser/d$a;

    move-result-object v1

    .line 1398
    if-eqz v1, :cond_4

    .line 1400
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIds()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 1401
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 1402
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/d$a;->a()Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;

    move-result-object v8

    .line 1403
    invoke-interface {v8}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getCount()I

    move-result v9

    .line 1404
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v9, :cond_4

    .line 1405
    invoke-interface {v8, v3}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getFile(I)Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v10

    .line 1406
    invoke-interface {v10}, Lcom/mfluent/asp/common/datamodel/ASPFile;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1407
    invoke-interface {v7, v10}, Lcom/mfluent/asp/common/datamodel/ASPFileProvider;->getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v2

    .line 1408
    invoke-static {v0, v2}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-ltz v2, :cond_3

    const/4 v2, 0x1

    :goto_3
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->isInclude()Z

    move-result v11

    if-ne v2, v11, :cond_2

    .line 1409
    invoke-virtual {p1, v10, v6}, Lcom/mfluent/asp/filetransfer/f;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/datamodel/Device;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1404
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 1408
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 1417
    :cond_4
    if-eqz v1, :cond_5

    .line 1418
    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a()Lcom/mfluent/asp/datamodel/filebrowser/d;

    move-result-object v0

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    int-to-long v2, v2

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    .line 1450
    :cond_5
    :goto_4
    invoke-direct {p0, p1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Lcom/mfluent/asp/filetransfer/f;)V

    .line 1453
    invoke-virtual {p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1454
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->r:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 1456
    :cond_6
    return-void

    .line 1414
    :catch_1
    move-exception v0

    .line 1415
    :try_start_2
    sget-object v2, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Unable to initiate FileBrowser based transfer for:"

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1417
    if-eqz v1, :cond_5

    .line 1418
    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a()Lcom/mfluent/asp/datamodel/filebrowser/d;

    move-result-object v0

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    int-to-long v2, v2

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    goto :goto_4

    .line 1417
    :catchall_0
    move-exception v0

    move-object v7, v0

    if-eqz v1, :cond_7

    .line 1418
    invoke-static {}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a()Lcom/mfluent/asp/datamodel/filebrowser/d;

    move-result-object v0

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    int-to-long v2, v2

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/datamodel/filebrowser/d;->a(Lcom/mfluent/asp/datamodel/filebrowser/d$a;JLjava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPFileSortType;Ljava/lang/String;)V

    :cond_7
    throw v7

    .line 1422
    :cond_8
    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->isSlinkUri()Z

    move-result v0

    .line 1423
    const/4 v1, 0x1

    new-array v1, v1, [Z

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-boolean v3, v1, v2

    .line 1425
    new-instance v2, Lcom/mfluent/asp/util/x;

    invoke-direct {v2}, Lcom/mfluent/asp/util/x;-><init>()V

    .line 1426
    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    new-instance v4, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$6;

    invoke-direct {v4, p0, p1, v0, v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$6;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;Lcom/mfluent/asp/filetransfer/f;Z[Z)V

    invoke-static {v2, p2, v3, v4}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    .line 1437
    const/4 v0, 0x0

    aget-boolean v0, v1, v0

    if-nez v0, :cond_5

    instance-of v0, p1, Lcom/mfluent/asp/filetransfer/n;

    if-eqz v0, :cond_5

    invoke-static {p2}, Lcom/mfluent/asp/util/x;->a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1439
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->getIds()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$7;

    invoke-direct {v3, p0, p1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$7;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;Lcom/mfluent/asp/filetransfer/f;)V

    invoke-static {v0, v1, v2, v3}, Lcom/mfluent/asp/util/x;->a(Landroid/content/ContentResolver;[Ljava/lang/String;[Ljava/lang/String;Lcom/mfluent/asp/util/x$a;)V

    goto/16 :goto_4
.end method

.method private a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 302
    sget v0, Lcom/mfluent/asp/ASPApplication;->b:I

    if-gtz v0, :cond_1

    .line 333
    :cond_0
    return-void

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->n:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 306
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->J()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 307
    const/4 v1, 0x0

    .line 308
    if-nez p2, :cond_5

    if-nez p3, :cond_5

    move v1, v2

    .line 324
    :cond_3
    :goto_1
    if-eqz v1, :cond_2

    .line 325
    if-eqz p1, :cond_4

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 326
    :cond_4
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->n:Landroid/util/LruCache;

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::cancelAll-SysDBGTest-cancel task for session id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 328
    invoke-virtual {v0, v2}, Lcom/mfluent/asp/filetransfer/f;->a(Z)V

    goto :goto_0

    .line 310
    :cond_5
    if-eqz p2, :cond_7

    if-nez p3, :cond_7

    .line 311
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->p()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 312
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->h()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    .line 314
    :cond_6
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    .line 316
    :cond_7
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 317
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 318
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->C()Ljava/lang/String;

    move-result-object v5

    .line 319
    invoke-virtual {v5, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 320
    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v5, "::cancelMediaForDevice: FOUND: {}, {}"

    invoke-interface {v1, v5, v4, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    move v1, v2

    .line 321
    goto :goto_1
.end method

.method private static a(Lcom/mfluent/asp/filetransfer/FileTransferSession;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 891
    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {p1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->CANCELLED:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {p1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ERROR:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {p1, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 892
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->f()I

    move-result v2

    if-ne v2, v1, :cond_1

    .line 898
    :cond_0
    :goto_0
    return v0

    .line 894
    :cond_1
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->r()I

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->f()I

    move-result v2

    if-ne v2, v1, :cond_0

    :cond_2
    move v0, v1

    .line 898
    goto :goto_0
.end method

.method static synthetic b(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->x:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;

    return-object v0
.end method

.method private b(Lcom/mfluent/asp/filetransfer/FileTransferSession;)V
    .locals 12

    .prologue
    const v4, 0x7f0a0341

    const v3, 0x7f0a033b

    const/4 v2, 0x1

    const v0, 0x7f0a033d

    const/4 v1, 0x0

    .line 739
    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    const-class v7, Lcom/mfluent/asp/ui/FileTransferListActivity;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 740
    const-string v6, "isAutoUpload"

    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->q()Z

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 741
    const-string v6, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->y()Z

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 742
    iget-object v6, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v7

    const/high16 v8, 0xc000000

    invoke-static {v6, v7, v5, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 748
    invoke-static {p1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->c(Lcom/mfluent/asp/filetransfer/FileTransferSession;)Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    move-result-object v9

    .line 757
    sget-object v5, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$8;->a:[I

    invoke-virtual {v9}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 789
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected task to be complete, but it was: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move v3, v2

    move v4, v0

    move v5, v0

    move v0, v1

    .line 794
    :goto_0
    invoke-static {p1, v9}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 834
    :cond_0
    :goto_1
    return-void

    :pswitch_1
    move v3, v1

    move v4, v0

    move v5, v0

    move v0, v2

    .line 773
    goto :goto_0

    :pswitch_2
    move v0, v1

    move v4, v3

    move v5, v3

    move v3, v1

    .line 778
    goto :goto_0

    .line 784
    :pswitch_3
    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->q()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    move v3, v1

    move v5, v4

    goto :goto_0

    .line 798
    :cond_1
    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->q()Z

    move-result v6

    invoke-direct {p0, v6}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Z)V

    .line 800
    if-eqz v3, :cond_4

    sget-object v6, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v6

    move v7, v6

    .line 801
    :goto_2
    if-eqz v0, :cond_5

    sget-object v6, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v6

    .line 802
    :goto_3
    if-nez v3, :cond_2

    if-eqz v0, :cond_6

    :cond_2
    add-int/2addr v6, v7

    .line 804
    :goto_4
    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->m()Ljava/lang/String;

    invoke-static {v7, v6, v1}, Lcom/mfluent/asp/util/UiUtils;->a(IIZ)Ljava/lang/String;

    move-result-object v6

    .line 806
    sget-object v7, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "sendTaskCompletedNotification: autoUpload "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 808
    iget-object v7, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 809
    new-instance v9, Landroid/app/Notification$Builder;

    iget-object v10, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-direct {v9, v10}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 810
    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 811
    invoke-virtual {v9, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 812
    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 813
    const v4, 0x7f020103

    invoke-virtual {v9, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 814
    invoke-virtual {v9, v8}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 815
    invoke-virtual {v9, v1}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 816
    invoke-virtual {v9, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 817
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 819
    const/4 v1, 0x2

    invoke-virtual {v9, v1}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 820
    invoke-virtual {v9}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 825
    :goto_5
    iput-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->v:Landroid/app/Notification;

    .line 826
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->u:Landroid/os/Handler;

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->y:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 827
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->y:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;

    invoke-virtual {v1, v3}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->a(Z)V

    .line 828
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->y:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->b(Z)V

    .line 829
    if-nez v3, :cond_3

    if-eqz v0, :cond_8

    .line 830
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->y:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;

    sget-wide v2, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->w:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 800
    :cond_4
    sget-object v6, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    move v7, v6

    goto/16 :goto_2

    .line 801
    :cond_5
    sget-object v6, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    goto/16 :goto_3

    .line 802
    :cond_6
    add-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_4

    .line 822
    :cond_7
    invoke-virtual {v9}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v1

    goto :goto_5

    .line 832
    :cond_8
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->y:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 757
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private b(Lcom/mfluent/asp/filetransfer/f;)V
    .locals 3

    .prologue
    .line 1071
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1072
    const-class v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 1073
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1075
    :try_start_0
    invoke-virtual {p1}, Lcom/mfluent/asp/filetransfer/f;->E()V

    .line 1076
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/mfluent/asp/filetransfer/f;)V

    .line 1079
    invoke-virtual {p1}, Lcom/mfluent/asp/filetransfer/f;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->transferImmediately:Z

    if-eqz v0, :cond_2

    .line 1080
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->f:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 1085
    :goto_0
    invoke-virtual {p1, v0}, Lcom/mfluent/asp/filetransfer/f;->a(Ljava/util/concurrent/Future;)V

    .line 1086
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1088
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->m()V

    .line 1089
    invoke-virtual {p1}, Lcom/mfluent/asp/filetransfer/f;->ad()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1090
    invoke-virtual {p1}, Lcom/mfluent/asp/filetransfer/f;->ab()V

    .line 1091
    invoke-virtual {p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Z)V

    .line 1096
    :cond_0
    invoke-virtual {p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1097
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->y:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1100
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1101
    return-void

    .line 1082
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->e:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1100
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 1111
    if-nez p1, :cond_0

    .line 1114
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/mfluent/asp/filetransfer/FileTransferService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1116
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)Landroid/app/Notification;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->v:Landroid/app/Notification;

    return-object v0
.end method

.method private static c(Lcom/mfluent/asp/filetransfer/FileTransferSession;)Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;
    .locals 1

    .prologue
    .line 904
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 905
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->PENDING:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    .line 916
    :goto_0
    return-object v0

    .line 905
    :cond_0
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->IN_PROGRESS:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    goto :goto_0

    .line 908
    :cond_1
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 909
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->CANCELLED:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    goto :goto_0

    .line 912
    :cond_2
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 913
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ERROR:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    goto :goto_0

    .line 916
    :cond_3
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    goto :goto_0
.end method

.method static synthetic i()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic j()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic k()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    return-object v0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 1107
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/mfluent/asp/filetransfer/FileTransferService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1108
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1534
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    const-string v1, "slpf_pref_20"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1535
    const-string v1, "fileTransferInProgress"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1536
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b()Z

    move-result v2

    if-eq v1, v2, :cond_0

    .line 1537
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1538
    const-string v1, "fileTransferInProgress"

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1539
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1541
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1182
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::transferAll(cursor, sourceDevice:{}, targetDevice:{}, options, isController:{})"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p2, v2, v4

    aput-object p3, v2, v5

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1184
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::transferAll(cursor, sourceDevice:{}, targetDevice:{}, onScanCompletedListener, options,isController:{})"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p2, v2, v4

    aput-object p3, v2, v5

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is empty"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, p2, p3, p4}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {v0, p1, v5}, Lcom/mfluent/asp/filetransfer/f;->a(Landroid/database/Cursor;Z)V

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_2
    invoke-direct {p0, v0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Lcom/mfluent/asp/filetransfer/f;)V

    return-object v0
.end method

.method public final a(Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/mfluent/asp/common/datamodel/ASPFile;",
            ">(",
            "Lcom/mfluent/asp/common/datamodel/ASPFileBrowser",
            "<TT;>;",
            "Lcom/mfluent/asp/datamodel/Device;",
            "Lcom/mfluent/asp/datamodel/Device;",
            "Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;",
            ")",
            "Lcom/mfluent/asp/filetransfer/f;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1467
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::transferAllSelected(fileBrowser, sourceDevice:{}, targetDevice:{}, onScanCompletedListener, options)"

    invoke-interface {v0, v1, p2, p3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {p0, p2, p3, p4}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v1

    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getSelectedFileIndexes()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/mfluent/asp/common/datamodel/ASPFileBrowser;->getFile(I)Lcom/mfluent/asp/common/datamodel/ASPFile;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0, p2}, Lcom/mfluent/asp/filetransfer/f;->a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Lcom/mfluent/asp/filetransfer/f;)V

    return-object v1
.end method

.method public final a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;
    .locals 2

    .prologue
    .line 1342
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::download(mediaSet, OnScanCompletedListener, options)"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 1343
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 1344
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/Device;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/f;
    .locals 6

    .prologue
    .line 489
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 491
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->n:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 492
    if-eqz v0, :cond_0

    .line 493
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->H()V

    .line 494
    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v2, "::retry: transferId: {}, {}, retryCache.size: {}"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->n:Landroid/util/LruCache;

    invoke-virtual {v5}, Landroid/util/LruCache;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 495
    invoke-direct {p0, v0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Lcom/mfluent/asp/filetransfer/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 501
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 504
    :goto_0
    return-object v0

    .line 498
    :cond_0
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "::retry: unknown transferId: {}, retryCache.size: {}"

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->n:Landroid/util/LruCache;

    invoke-virtual {v2}, Landroid/util/LruCache;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 501
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 504
    const/4 v0, 0x0

    goto :goto_0

    .line 501
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/util/List;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/filetransfer/l;",
            ">;",
            "Lcom/mfluent/asp/datamodel/Device;",
            "Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;",
            ")",
            "Lcom/mfluent/asp/filetransfer/f;"
        }
    .end annotation

    .prologue
    .line 1322
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::downloadCloudFiles(storageGatewayFileInfos, sourceDevice, options)"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 1323
    new-instance v0, Lcom/mfluent/asp/filetransfer/a;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->t:Ljava/io/File;

    move-object v2, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/filetransfer/a;-><init>(Landroid/content/Context;Lcom/mfluent/asp/filetransfer/g;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Ljava/io/File;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 1325
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/filetransfer/l;

    .line 1326
    invoke-virtual {v0, v1, p2}, Lcom/mfluent/asp/filetransfer/a;->a(Lcom/mfluent/asp/filetransfer/l;Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_0

    .line 1329
    :cond_0
    invoke-direct {p0, v0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Lcom/mfluent/asp/filetransfer/f;)V

    .line 1330
    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 465
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 467
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 468
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 469
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 470
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 483
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 475
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->r:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 476
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 477
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/filetransfer/f;->a(Z)V

    goto :goto_1

    .line 480
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->r:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 484
    return-void
.end method

.method public final a(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 369
    if-eqz p1, :cond_0

    .line 370
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 371
    invoke-virtual {p0, v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Z)V

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 375
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 376
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 378
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->p()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 379
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->h()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 383
    :goto_2
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 384
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 390
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 381
    :cond_3
    :try_start_1
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_2

    .line 388
    :cond_4
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 412
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 413
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 415
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 416
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 417
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->C()Ljava/lang/String;

    move-result-object v3

    .line 418
    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 419
    sget-object v2, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v3, "::cancelMediaForDevice: FOUND: {}, {}"

    invoke-interface {v2, v3, v1, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 420
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 427
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 431
    :goto_0
    return-void

    .line 425
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, v0, p1, p2}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 427
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 430
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v2, "::cancelMediaForDevice: NOT FOUND: {}"

    invoke-interface {v0, v2, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 427
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Lcom/mfluent/asp/dws/handlers/ap;)V
    .locals 2

    .prologue
    .line 1599
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->o:Landroid/util/LruCache;

    invoke-virtual {p1}, Lcom/mfluent/asp/dws/handlers/ap;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1600
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->p:Landroid/util/LruCache;

    invoke-virtual {p1}, Lcom/mfluent/asp/dws/handlers/ap;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1601
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->l()V

    .line 1602
    return-void
.end method

.method public final a(Lcom/mfluent/asp/dws/handlers/ap;Z)V
    .locals 3

    .prologue
    .line 1588
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->o:Landroid/util/LruCache;

    invoke-virtual {p1}, Lcom/mfluent/asp/dws/handlers/ap;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1590
    if-eqz p2, :cond_0

    .line 1591
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const-string v1, "com.mfluent.asp.filetransfer.FileTransferManagerImpl.TRANSFER_NOTIFICATION_TAG"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/mfluent/asp/filetransfer/FileTransferService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "isAutoUpload"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1595
    :goto_0
    return-void

    .line 1593
    :cond_0
    invoke-virtual {p1}, Lcom/mfluent/asp/dws/handlers/ap;->q()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Z)V

    goto :goto_0
.end method

.method public final a(Lcom/mfluent/asp/filetransfer/FileTransferSession;)V
    .locals 12

    .prologue
    const v1, 0x7f0a02f8

    const/4 v11, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x0

    const v2, 0x7f0a0341

    .line 648
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    const-string v5, "notification"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 649
    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    const-class v7, Lcom/mfluent/asp/ui/FileTransferListActivity;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 650
    const-string v6, "isAutoUpload"

    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->q()Z

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 651
    const-string v6, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->y()Z

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 652
    iget-object v6, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v7

    const/high16 v8, 0xc000000

    invoke-static {v6, v7, v5, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 658
    invoke-static {p1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->c(Lcom/mfluent/asp/filetransfer/FileTransferSession;)Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    move-result-object v7

    .line 666
    sget-object v5, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$8;->a:[I

    invoke-virtual {v7}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ordinal()I

    move-result v8

    aget v5, v5, v8

    packed-switch v5, :pswitch_data_0

    .line 690
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected task to be complete, but it was: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 670
    :pswitch_0
    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->f()I

    move-result v1

    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->g()I

    move-result v5

    if-ne v1, v5, :cond_0

    const v1, 0x7f0a03ce

    :goto_0
    move v5, v1

    move v1, v3

    .line 695
    :goto_1
    invoke-static {p1, v7}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 735
    :goto_2
    return-void

    :cond_0
    move v1, v2

    .line 670
    goto :goto_0

    :pswitch_1
    move v2, v1

    move v5, v1

    move v1, v4

    .line 677
    goto :goto_1

    .line 679
    :pswitch_2
    const v2, 0x7f0a00ee

    .line 681
    const v1, 0x7f0a033c

    move v5, v1

    move v1, v4

    .line 682
    goto :goto_1

    :pswitch_3
    move v1, v4

    move v5, v2

    .line 688
    goto :goto_1

    .line 699
    :cond_1
    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->q()Z

    move-result v8

    invoke-direct {p0, v8}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Z)V

    .line 703
    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->m()Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->f()I

    move-result v1

    :goto_3
    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->f()I

    move-result v8

    invoke-static {v1, v8, v4}, Lcom/mfluent/asp/util/UiUtils;->a(IIZ)Ljava/lang/String;

    move-result-object v1

    .line 709
    sget-object v8, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "sendTaskCompletedNotification: fileTransfer "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v8, v7}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 711
    iget-object v7, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 712
    new-instance v8, Landroid/app/Notification$Builder;

    iget-object v9, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 713
    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 714
    invoke-virtual {v8, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 715
    invoke-virtual {v7, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 716
    const v1, 0x7f020103

    invoke-virtual {v8, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 717
    invoke-virtual {v8, v6}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 718
    invoke-virtual {v8, v4}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 719
    invoke-virtual {v8, v3}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 720
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 722
    const/4 v1, 0x2

    invoke-virtual {v8, v1}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 723
    invoke-virtual {v8}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    move-object v2, v1

    .line 728
    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "com.mfluent.asp.filetransfer.FileTransferManagerImpl.TRANSFER_NOTIFICATION_TAG"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 730
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->x:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;

    new-instance v4, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v4, p0, v3, v11}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;Ljava/lang/String;I)V

    invoke-virtual {v1, v4}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;

    .line 731
    if-eqz v1, :cond_2

    .line 732
    invoke-virtual {v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;->b()I

    move-result v1

    invoke-virtual {v0, v4, v1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 734
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v0, v3, v11, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_2

    .line 703
    :cond_3
    invoke-interface {p1}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->r()I

    move-result v1

    goto/16 :goto_3

    .line 725
    :cond_4
    invoke-virtual {v8}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v1

    move-object v2, v1

    goto :goto_4

    .line 666
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/mfluent/asp/filetransfer/b;)V
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->h:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/c;

    .line 514
    invoke-interface {v0, p1}, Lcom/mfluent/asp/filetransfer/c;->a(Lcom/mfluent/asp/filetransfer/b;)V

    goto :goto_0

    .line 516
    :cond_0
    return-void
.end method

.method public final a(Lcom/mfluent/asp/filetransfer/c;)V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->h:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, p1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    return-void
.end method

.method public final a(Lcom/mfluent/asp/filetransfer/f;)V
    .locals 23

    .prologue
    .line 525
    const/4 v3, 0x0

    .line 526
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->o()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->aj()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v2

    if-nez v2, :cond_0

    .line 527
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Ljava/lang/String;Z)V

    .line 529
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->o()Z

    move-result v2

    if-nez v2, :cond_10

    .line 530
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 532
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 534
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->e()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->d()Z

    move-result v2

    if-nez v2, :cond_6

    .line 535
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v2

    if-nez v2, :cond_5

    .line 537
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->I()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 538
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->n:Landroid/util/LruCache;

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v2, v3, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->K()Z

    move-result v3

    .line 541
    sget-object v2, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SysDBGTest::bAutoRetryAvailable="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    move v2, v3

    .line 575
    :goto_0
    if-nez v2, :cond_1

    .line 576
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->ad()Z

    move-result v3

    if-nez v3, :cond_1

    .line 577
    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->m()V

    .line 578
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 582
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 586
    if-nez v2, :cond_f

    .line 587
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.mfluent.asp.filetransfer.FileTransferManager.TRANSFER_COMPLETED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 588
    const-string v4, "com.mfluent.asp.filetransfer.FileTransferManager.TRANSFER_COMPLETE_DEVICE_ID"

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 589
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-static {v4}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v4

    .line 590
    invoke-virtual {v4, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 591
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->ad()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v3

    if-nez v3, :cond_2

    .line 592
    invoke-virtual/range {p0 .. p1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession;)V

    .line 595
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->I()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 596
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->q:Landroid/util/LruCache;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 622
    :cond_3
    :goto_1
    if-nez v2, :cond_4

    .line 623
    new-instance v3, Lcom/mfluent/asp/filetransfer/b;

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->f()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->k()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->l()J

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->c()I

    move-result v10

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->o()Z

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->e()Z

    move-result v12

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->d()Z

    move-result v13

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->p()Z

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->z()Ljava/util/List;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v16

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->Q()Z

    move-result v17

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->S()Z

    move-result v18

    const/16 v19, 0x1

    const/16 v20, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->n()Z

    move-result v21

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->T()Z

    move-result v22

    invoke-direct/range {v3 .. v22}, Lcom/mfluent/asp/filetransfer/b;-><init>(Ljava/lang/String;IJJIZZZZLjava/util/List;ZZZZZZZ)V

    .line 642
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/mfluent/asp/filetransfer/b;)V

    .line 644
    :cond_4
    return-void

    .line 544
    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->r:Landroid/util/LruCache;

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v2, v4, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.samsung.android.sdk.samsunglink.AUTO_UPLOAD_STATE_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 548
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->ad()Z

    move-result v2

    if-nez v2, :cond_9

    .line 549
    invoke-direct/range {p0 .. p1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Lcom/mfluent/asp/filetransfer/FileTransferSession;)V

    move v2, v3

    goto/16 :goto_0

    .line 552
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->Y()Z

    move-result v2

    if-nez v2, :cond_9

    .line 553
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->ad()Z

    move-result v2

    if-nez v2, :cond_9

    .line 554
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->ae()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 555
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->Z()V

    .line 560
    :cond_7
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 561
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.samsung.android.sdk.samsunglink.AUTO_UPLOAD_STATE_CHANGED"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 562
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->d()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 563
    const-string v4, "pressedCancelButton"

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 565
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 567
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->ad()Z

    move-result v2

    if-nez v2, :cond_9

    .line 568
    invoke-direct/range {p0 .. p1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Lcom/mfluent/asp/filetransfer/FileTransferSession;)V

    :cond_9
    move v2, v3

    goto/16 :goto_0

    .line 557
    :cond_a
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->d()Z

    move-result v2

    if-eqz v2, :cond_c

    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->CANCELLED:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v2, v6}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toContentValues(Landroid/content/ContentValues;)V

    :goto_3
    const-string v2, "uuid"

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "createdTime"

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "endTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-wide/16 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->h()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_d

    const-wide/16 v4, -0x62

    :cond_b
    :goto_4
    const-string v2, "sourceDeviceId"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "targetDeviceId"

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "transferSize"

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->k()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "firstFileName"

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "numFiles"

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->f()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "transferarchive"

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v2

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "numFilesSkipped"

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->g()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "currentTransferSize"

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$5;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v6}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$5;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;Landroid/content/ContentValues;)V

    invoke-interface {v2, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->Z()V

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->d()Z

    move-result v2

    if-nez v2, :cond_7

    sget-object v2, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v4, "writeCompletedTaskRecord: isAutoUpload"

    invoke-interface {v2, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 582
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2

    .line 557
    :cond_c
    :try_start_2
    sget-object v2, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v2, v6}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->toContentValues(Landroid/content/ContentValues;)V

    goto/16 :goto_3

    :cond_d
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_b

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    int-to-long v4, v2

    goto/16 :goto_4

    :cond_e
    const/4 v2, 0x0

    goto :goto_5

    .line 599
    :cond_f
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->M()Z

    goto/16 :goto_1

    .line 606
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->aa()Z

    move-result v2

    if-nez v2, :cond_12

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->ad()Z

    move-result v2

    if-nez v2, :cond_12

    .line 607
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->ab()V

    .line 609
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Z)V

    .line 611
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-eq v2, v4, :cond_12

    .line 612
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 613
    invoke-direct/range {p0 .. p1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Lcom/mfluent/asp/filetransfer/FileTransferSession;)V

    move v2, v3

    goto/16 :goto_1

    .line 615
    :cond_11
    invoke-virtual/range {p0 .. p1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/mfluent/asp/filetransfer/FileTransferSession;)V

    :cond_12
    move v2, v3

    goto/16 :goto_1
.end method

.method public final a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 3

    .prologue
    .line 1349
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::transfer(mediaSet, targetDevice:{}, options)"

    invoke-interface {v0, v1, p2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1352
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/Device;Landroid/media/MediaScannerConnection$OnScanCompletedListener;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Lcom/mfluent/asp/filetransfer/f;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1356
    :goto_0
    return-void

    .line 1353
    :catch_0
    move-exception v0

    .line 1354
    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v2, "transfer IllegalArgumentException"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 2

    .prologue
    .line 1335
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::download(mediaSet, options)"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 1336
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 1337
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 1338
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 435
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 438
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 439
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 445
    :goto_0
    if-eqz v0, :cond_1

    .line 446
    invoke-virtual {v0, p2}, Lcom/mfluent/asp/filetransfer/f;->a(Z)V

    .line 448
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->o()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->n()Z

    move-result v1

    if-nez v1, :cond_1

    .line 449
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/mfluent/asp/filetransfer/f;)V

    .line 453
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v0

    if-nez v0, :cond_3

    .line 456
    :cond_2
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 459
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 460
    return-void

    .line 459
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 341
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 343
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 344
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 345
    invoke-virtual {v0, p1}, Lcom/mfluent/asp/filetransfer/f;->a(Z)V

    .line 346
    if-eqz p1, :cond_0

    .line 347
    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 359
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 351
    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_1
    invoke-direct {p0, v0, v1, v2}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)V

    .line 352
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->o:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/dws/handlers/ap;

    .line 353
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.mfluent.asp.filetransfer.FileTransferManager.CANCELED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 354
    const-string v3, "sessionId"

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 355
    const-string v0, "isDeleteSessionAfterCancel"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 356
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 359
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 360
    return-void
.end method

.method public final b(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/f;
    .locals 3

    .prologue
    .line 1555
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1557
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 1558
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 1563
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1566
    :goto_0
    return-object v0

    .line 1563
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1566
    const/4 v0, 0x0

    goto :goto_0

    .line 1563
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final b(Lcom/mfluent/asp/filetransfer/c;)V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->h:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1520
    .line 1521
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->n:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 1522
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    .line 1527
    :goto_0
    if-lez v0, :cond_2

    move v2, v3

    .line 1530
    :cond_1
    :goto_1
    return v2

    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->g()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/dws/handlers/ap;

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->o()Z

    move-result v0

    if-eqz v0, :cond_5

    add-int/lit8 v0, v1, 0x1

    :goto_3
    move v1, v0

    goto :goto_2

    :cond_3
    if-lez v1, :cond_1

    :cond_4
    move v2, v3

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method public final b(Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 3

    .prologue
    .line 1504
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 1505
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->h()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1506
    :cond_1
    const/4 v0, 0x1

    .line 1510
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bytesTransferred(J)V
    .locals 0

    .prologue
    .line 1068
    return-void
.end method

.method public final c(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/ap;
    .locals 1

    .prologue
    .line 1576
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->o:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/dws/handlers/ap;

    return-object v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 266
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->t:Ljava/io/File;

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->cleanDirectory(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 272
    :goto_0
    return-void

    .line 267
    :catch_0
    move-exception v0

    .line 268
    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v2, "Trouble cleaning up cache directory {}"

    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->t:Ljava/io/File;

    invoke-interface {v1, v2, v3, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 269
    :catch_1
    move-exception v0

    .line 270
    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a:Lorg/slf4j/Logger;

    const-string v2, "Trouble cleaning up cache directory {}"

    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->t:Ljava/io/File;

    invoke-interface {v1, v2, v3, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Lcom/mfluent/asp/dws/handlers/ap;
    .locals 1

    .prologue
    .line 1581
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->p:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/dws/handlers/ap;

    return-object v0
.end method

.method public final d()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/filetransfer/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1545
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1547
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->s:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1549
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final e()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/filetransfer/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1571
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->n:Landroid/util/LruCache;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1655
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->r:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1657
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.AUTO_UPLOAD_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1658
    const-string v1, "pressedRetryButton"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1660
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1661
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1606
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->o:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 1608
    return-void
.end method

.method public final g()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/mfluent/asp/dws/handlers/ap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1612
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->o:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final h()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/filetransfer/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1647
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->r:Landroid/util/LruCache;

    return-object v0
.end method
