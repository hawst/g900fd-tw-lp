.class public final Lcom/mfluent/asp/filetransfer/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:J

.field private final d:J

.field private final e:I

.field private final f:Z

.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:Z

.field private q:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;IJJIZZZLjava/util/List;Z)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IJJIZZZ",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 70
    const/4 v12, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move-object/from16 v13, p11

    move/from16 v18, p12

    invoke-direct/range {v1 .. v20}, Lcom/mfluent/asp/filetransfer/b;-><init>(Ljava/lang/String;IJJIZZZZLjava/util/List;ZZZZZZZ)V

    .line 88
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJJIZZZZLjava/util/List;ZZZZZZZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IJJIZZZZ",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;ZZZZZZZ)V"
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/b;->a:Ljava/lang/String;

    .line 110
    iput p2, p0, Lcom/mfluent/asp/filetransfer/b;->b:I

    .line 111
    iput-wide p3, p0, Lcom/mfluent/asp/filetransfer/b;->c:J

    .line 112
    iput-wide p5, p0, Lcom/mfluent/asp/filetransfer/b;->d:J

    .line 113
    iput p7, p0, Lcom/mfluent/asp/filetransfer/b;->e:I

    .line 114
    iput-boolean p8, p0, Lcom/mfluent/asp/filetransfer/b;->f:Z

    .line 115
    iput-boolean p9, p0, Lcom/mfluent/asp/filetransfer/b;->g:Z

    .line 116
    iput-boolean p10, p0, Lcom/mfluent/asp/filetransfer/b;->h:Z

    .line 117
    iput-boolean p11, p0, Lcom/mfluent/asp/filetransfer/b;->i:Z

    .line 118
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/mfluent/asp/filetransfer/b;->j:Ljava/util/List;

    .line 119
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/b;->k:Z

    .line 120
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/b;->l:Z

    .line 121
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/b;->m:Z

    .line 122
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/b;->n:Z

    .line 123
    move/from16 v0, p17

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/b;->o:Z

    .line 124
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/b;->p:Z

    .line 125
    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/b;->q:Z

    .line 126
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/mfluent/asp/filetransfer/b;->e:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/b;->f:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/b;->g:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/b;->n:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    invoke-static {p0}, Lorg/apache/commons/lang3/builder/ReflectionToStringBuilder;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
