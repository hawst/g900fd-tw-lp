.class public Lcom/mfluent/asp/filetransfer/FileTransferService;
.super Landroid/app/Service;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/filetransfer/FileTransferService$3;,
        Lcom/mfluent/asp/filetransfer/FileTransferService$a;,
        Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private final b:Landroid/os/IBinder;

.field private c:J

.field private d:I

.field private final e:Landroid/os/Handler;

.field private f:Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;

.field private g:Landroid/app/Notification$Builder;

.field private h:Landroid/app/Notification;

.field private i:Ljava/lang/String;

.field private final j:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/mfluent/asp/filetransfer/FileTransferService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileTransferService;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 29
    new-instance v0, Lcom/mfluent/asp/filetransfer/FileTransferService$a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/filetransfer/FileTransferService$a;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferService;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->b:Landroid/os/IBinder;

    .line 30
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->c:J

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->d:I

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->e:Landroid/os/Handler;

    .line 41
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;->a:Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->f:Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;

    .line 277
    new-instance v0, Lcom/mfluent/asp/filetransfer/FileTransferService$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/filetransfer/FileTransferService$2;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferService;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->j:Ljava/lang/Runnable;

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 223
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;->a:Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->f:Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;

    .line 224
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->c:J

    .line 225
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->d:I

    .line 227
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->stopForeground(Z)V

    .line 228
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->stopSelf()V

    .line 229
    return-void
.end method

.method private static a(Lcom/mfluent/asp/filetransfer/d;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 232
    .line 234
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/d;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 235
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->n()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 241
    :goto_0
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/d;->g()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/dws/handlers/ap;

    .line 242
    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->n()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 248
    :goto_1
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    :goto_2
    return v2

    :cond_2
    move v2, v3

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_0
.end method

.method private static b(Lcom/mfluent/asp/filetransfer/d;)Lcom/mfluent/asp/filetransfer/FileTransferSession;
    .locals 3

    .prologue
    .line 256
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/d;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 257
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->n()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 274
    :goto_0
    return-object v0

    .line 262
    :cond_1
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/d;->g()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/dws/handlers/ap;

    .line 263
    invoke-virtual {v0}, Lcom/mfluent/asp/dws/handlers/ap;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 269
    :cond_3
    invoke-interface {p0}, Lcom/mfluent/asp/filetransfer/d;->e()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/f;

    .line 270
    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/f;->J()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_0

    .line 274
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->b:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 63
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    .line 64
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->i:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 10

    .prologue
    const v6, 0x7f0a00eb

    const v9, 0x7f09000d

    const v8, 0x7f020103

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 74
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferService;->a:Lorg/slf4j/Logger;

    const-string v2, "onStartCommand {}"

    invoke-interface {v0, v2, p1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 79
    if-eqz p1, :cond_b

    .line 81
    const-string v0, "isAutoUpload"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 84
    :goto_0
    invoke-static {p0}, Lcom/mfluent/asp/filetransfer/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/filetransfer/d;

    move-result-object v2

    .line 85
    if-nez v2, :cond_1

    .line 86
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->a()V

    .line 219
    :cond_0
    :goto_1
    return v3

    .line 90
    :cond_1
    invoke-static {v2}, Lcom/mfluent/asp/filetransfer/FileTransferService;->b(Lcom/mfluent/asp/filetransfer/d;)Lcom/mfluent/asp/filetransfer/FileTransferSession;

    move-result-object v4

    .line 91
    if-nez v4, :cond_2

    .line 92
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->a()V

    goto :goto_1

    .line 98
    :cond_2
    invoke-static {v2}, Lcom/mfluent/asp/filetransfer/FileTransferService;->b(Lcom/mfluent/asp/filetransfer/d;)Lcom/mfluent/asp/filetransfer/FileTransferSession;

    move-result-object v4

    .line 99
    if-eqz v2, :cond_a

    invoke-interface {v2}, Lcom/mfluent/asp/filetransfer/d;->b()Z

    move-result v5

    if-eqz v5, :cond_a

    if-eqz v4, :cond_a

    invoke-interface {v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->q()Z

    move-result v4

    if-nez v4, :cond_a

    if-nez v0, :cond_a

    .line 105
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferService$3;->a:[I

    iget-object v4, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->f:Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;

    invoke-virtual {v4}, Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 108
    :pswitch_0
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 109
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 110
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    invoke-virtual {v0, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 111
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;->b:Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->f:Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;

    .line 112
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->c:J

    .line 114
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 115
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->h:Landroid/app/Notification;

    .line 119
    :goto_2
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 120
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->h:Landroid/app/Notification;

    invoke-virtual {p0, v9, v1}, Lcom/mfluent/asp/filetransfer/FileTransferService;->startForeground(ILandroid/app/Notification;)V

    .line 121
    new-instance v1, Lcom/mfluent/asp/filetransfer/FileTransferService$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/filetransfer/FileTransferService$1;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferService;)V

    const-wide/16 v4, 0x7d0

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 117
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->h:Landroid/app/Notification;

    goto :goto_2

    .line 134
    :pswitch_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->c:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x7d0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    .line 135
    invoke-static {v2}, Lcom/mfluent/asp/filetransfer/FileTransferService;->b(Lcom/mfluent/asp/filetransfer/d;)Lcom/mfluent/asp/filetransfer/FileTransferSession;

    move-result-object v4

    .line 136
    invoke-static {v2}, Lcom/mfluent/asp/filetransfer/FileTransferService;->a(Lcom/mfluent/asp/filetransfer/d;)Z

    move-result v5

    .line 140
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->i:Ljava/lang/String;

    const-string v2, "..."

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 141
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->i:Ljava/lang/String;

    const-string v2, "..."

    const-string v6, ""

    invoke-virtual {v0, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->i:Ljava/lang/String;

    .line 148
    :goto_3
    if-eqz v4, :cond_0

    .line 150
    if-eqz v5, :cond_6

    .line 160
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a02b8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 161
    const/4 v2, -0x1

    iput v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->d:I

    .line 174
    :goto_4
    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    invoke-virtual {v2, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 175
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 177
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->getApplication()Landroid/app/Application;

    move-result-object v2

    const-class v6, Lcom/mfluent/asp/ui/FileTransferListActivity;

    invoke-direct {v0, v2, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 178
    const-string v2, "mfl_FileTransferService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Make fileTransfer notificationIntent : startedTask is UiAppTheme() : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->y()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v2, "com.samsung.android.sdk.samsunglink.SLINK_UI_APP_THEME"

    invoke-interface {v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->y()Z

    move-result v6

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 180
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->getApplication()Landroid/app/Application;

    move-result-object v2

    const/high16 v6, 0xc000000

    invoke-static {v2, v1, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 184
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    invoke-virtual {v0, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 185
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;->c:Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->f:Lcom/mfluent/asp/filetransfer/FileTransferService$STATE;

    .line 187
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 188
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->h:Landroid/app/Notification;

    .line 194
    :goto_5
    iget v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->d:I

    invoke-interface {v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->r()I

    .line 196
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->h:Landroid/app/Notification;

    invoke-virtual {p0, v9, v0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->startForeground(ILandroid/app/Notification;)V

    .line 198
    if-nez v5, :cond_4

    .line 199
    invoke-interface {v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->r()I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->d:I

    .line 202
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 203
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->j:Ljava/lang/Runnable;

    const-wide/16 v4, 0xbb8

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 143
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->i:Ljava/lang/String;

    goto/16 :goto_3

    .line 163
    :cond_6
    invoke-interface {v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->r()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 164
    invoke-interface {v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->f()I

    move-result v2

    if-ge v2, v0, :cond_7

    .line 165
    invoke-interface {v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->f()I

    move-result v0

    .line 167
    :cond_7
    invoke-interface {v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->m()Ljava/lang/String;

    invoke-interface {v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->f()I

    move-result v6

    invoke-interface {v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession;->q()Z

    move-result v2

    if-eqz v2, :cond_8

    move v2, v1

    :goto_6
    invoke-static {v0, v6, v2}, Lcom/mfluent/asp/util/UiUtils;->a(IIZ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    :cond_8
    move v2, v3

    goto :goto_6

    .line 190
    :cond_9
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->g:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferService;->h:Landroid/app/Notification;

    goto :goto_5

    .line 214
    :cond_a
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/FileTransferService;->a()V

    goto/16 :goto_1

    :cond_b
    move v0, v1

    goto/16 :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
