.class public Lcom/mfluent/asp/filetransfer/m;
.super Lcom/mfluent/asp/filetransfer/f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/filetransfer/m$a;
    }
.end annotation


# static fields
.field private static final d:Lorg/slf4j/Logger;


# instance fields
.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/filetransfer/m$a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:J

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Lcom/mfluent/asp/datamodel/Device;

.field private n:I

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/mfluent/asp/filetransfer/m;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 75
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mfluent/asp/filetransfer/f;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->e:Ljava/util/List;

    .line 59
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/m;->i:J

    .line 61
    iput-boolean v2, p0, Lcom/mfluent/asp/filetransfer/m;->j:Z

    .line 63
    iput-boolean v2, p0, Lcom/mfluent/asp/filetransfer/m;->k:Z

    .line 70
    iput v2, p0, Lcom/mfluent/asp/filetransfer/m;->n:I

    .line 72
    iput v2, p0, Lcom/mfluent/asp/filetransfer/m;->o:I

    .line 76
    return-void
.end method

.method private D()V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/m;->e:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/filetransfer/m$a;

    iget v2, v2, Lcom/mfluent/asp/filetransfer/m$a;->c:I

    .line 271
    packed-switch v2, :pswitch_data_0

    .line 295
    :pswitch_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unsupported media type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 273
    :pswitch_1
    const-string v3, "PHOTO"

    .line 274
    const-string v2, "IMAGE"

    move-object v10, v2

    move-object v11, v3

    .line 298
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->O()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/a;->b()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v17

    .line 300
    if-nez v17, :cond_0

    const/4 v2, 0x0

    move-object/from16 v16, v2

    .line 301
    :goto_1
    if-nez v16, :cond_1

    .line 302
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Missing GUID information from AccessManager."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 279
    :pswitch_2
    const-string v3, "VIDEO"

    .line 280
    const-string v2, "CLIP"

    move-object v10, v2

    move-object v11, v3

    .line 281
    goto :goto_0

    .line 283
    :pswitch_3
    const-string v3, "MUSIC"

    .line 284
    const-string v2, "SONG"

    move-object v10, v2

    move-object v11, v3

    .line 285
    goto :goto_0

    .line 287
    :pswitch_4
    const-string v3, "DOC"

    .line 288
    const-string v2, "DOC"

    move-object v10, v2

    move-object v11, v3

    .line 289
    goto :goto_0

    .line 291
    :pswitch_5
    const-string v3, "FILE"

    .line 292
    const-string v2, ""

    move-object v10, v2

    move-object v11, v3

    .line 293
    goto :goto_0

    .line 300
    :cond_0
    invoke-virtual/range {v17 .. v17}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v16, v2

    goto :goto_1

    .line 305
    :cond_1
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    .line 310
    const/4 v2, 0x0

    .line 311
    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->ak()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 312
    const-string v4, "api/pCloud/externalService/upload"

    .line 313
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 314
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/mfluent/asp/filetransfer/m;->k:Z

    if-eqz v5, :cond_3

    .line 315
    const-string v5, "Files"

    invoke-virtual {v14, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 320
    :goto_2
    const-string v5, "SPName"

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 321
    const-string v5, "UserID"

    move-object/from16 v0, v16

    invoke-virtual {v14, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 322
    const-string v5, "CurrentTab"

    invoke-virtual {v14, v5, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-object v12, v3

    move-object v13, v4

    .line 355
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->al()Z

    move-result v3

    if-nez v3, :cond_a

    .line 356
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/mfluent/asp/filetransfer/m;->k:Z

    if-eqz v2, :cond_f

    .line 358
    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->ak()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 359
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 360
    const-string v3, "depth1"

    invoke-virtual {v12, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-object v12, v2

    .line 363
    :cond_2
    const-string v2, "status"

    const-string v3, "INCLUDE"

    invoke-virtual {v12, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 364
    const-string v2, "path"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/m;->l:Ljava/lang/String;

    invoke-virtual {v12, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 365
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 366
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/m;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/filetransfer/m$a;

    .line 367
    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/m$a;->g:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_4

    .line 317
    :cond_3
    const-string v5, "Contents"

    invoke-virtual {v14, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 323
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->al()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 324
    const-string v15, "api/pCloud/externalService/download/include"

    .line 325
    const/4 v13, 0x0

    .line 327
    new-instance v12, Lorg/json/JSONArray;

    invoke-direct {v12}, Lorg/json/JSONArray;-><init>()V

    .line 328
    const-string v2, "Files"

    invoke-virtual {v14, v2, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/m;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/filetransfer/m$a;

    .line 331
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v3

    iget-boolean v3, v3, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->skipIfDuplicate:Z

    if-eqz v3, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    iget v5, v2, Lcom/mfluent/asp/filetransfer/m$a;->c:I

    iget-wide v6, v2, Lcom/mfluent/asp/filetransfer/m$a;->b:J

    iget-wide v8, v2, Lcom/mfluent/asp/filetransfer/m$a;->a:J

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/mfluent/asp/filetransfer/m;->a(IIJJ)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_6

    .line 333
    :cond_5
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 335
    const-string v4, "ID"

    iget-object v5, v2, Lcom/mfluent/asp/filetransfer/m$a;->i:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 336
    const-string v4, "Name"

    iget-object v5, v2, Lcom/mfluent/asp/filetransfer/m$a;->g:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 337
    const-string v4, "Size"

    iget-wide v6, v2, Lcom/mfluent/asp/filetransfer/m$a;->h:J

    invoke-virtual {v3, v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 338
    invoke-virtual {v12, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_5

    .line 340
    :cond_6
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/mfluent/asp/filetransfer/m$a;->j:Z

    iput-boolean v3, v2, Lcom/mfluent/asp/filetransfer/m$a;->f:Z

    .line 341
    move-object/from16 v0, p0

    iget v3, v0, Lcom/mfluent/asp/filetransfer/m;->h:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/mfluent/asp/filetransfer/m;->h:I

    .line 342
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/mfluent/asp/filetransfer/m;->i:J

    iget-wide v6, v2, Lcom/mfluent/asp/filetransfer/m$a;->h:J

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/mfluent/asp/filetransfer/m;->i:J

    .line 343
    sget-object v3, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v4, "::startTransfer skipping duplicate file: {}"

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/m$a;->g:Ljava/lang/String;

    invoke-interface {v3, v4, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_5

    .line 347
    :cond_7
    const-string v2, "SPName"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 348
    const-string v2, "UserID"

    move-object/from16 v0, v16

    invoke-virtual {v14, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 349
    const-string v2, "CurrentTab"

    invoke-virtual {v14, v2, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-object v2, v12

    move-object v12, v13

    move-object v13, v15

    goto/16 :goto_3

    .line 351
    :cond_8
    const-string v3, "api/pCloud/transfer/upload"

    move-object v12, v14

    move-object v13, v3

    .line 352
    goto/16 :goto_3

    .line 369
    :cond_9
    const-string v2, "items"

    invoke-virtual {v12, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 371
    const-string v2, "search"

    const-string v4, "."

    invoke-virtual {v12, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-object v2, v3

    .line 396
    :cond_a
    :goto_6
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/mfluent/asp/filetransfer/m;->i:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/mfluent/asp/filetransfer/m;->bytesTransferred(J)V

    .line 398
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/mfluent/asp/filetransfer/m;->n:I

    .line 399
    move-object/from16 v0, p0

    iget v2, v0, Lcom/mfluent/asp/filetransfer/m;->n:I

    if-lez v2, :cond_e

    .line 401
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    .line 403
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    .line 405
    invoke-static {v13}, Lcom/mfluent/asp/nts/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "currentTab"

    invoke-virtual {v2, v4, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "controllerId"

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "sourceId"

    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->al()Z

    move-result v2

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v2

    :goto_7
    invoke-virtual {v4, v5, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "targetId"

    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->ak()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v2

    :goto_8
    invoke-virtual {v4, v5, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "deleteAfterUpload"

    const-string v5, "NO"

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "cName"

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "sName"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "isPersonal"

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->X()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "isDigitalSecure"

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->W()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "tName"

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 418
    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 420
    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->al()Z

    move-result v4

    if-nez v4, :cond_b

    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->ak()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 422
    :cond_b
    const-string v4, "token"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const-string v4, "GUID"

    move-object/from16 v0, v16

    invoke-virtual {v3, v4, v0}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    :cond_c
    new-instance v4, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v4}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 427
    invoke-virtual {v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 429
    new-instance v4, Lcom/mfluent/asp/util/n;

    invoke-direct {v4, v14}, Lcom/mfluent/asp/util/n;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 431
    sget-object v4, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    invoke-interface {v4}, Lorg/slf4j/Logger;->isTraceEnabled()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 432
    sget-object v4, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v5, "Request URI: {}"

    invoke-interface {v4, v5, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 433
    sget-object v2, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v4, "Request Headers: {}"

    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpPost;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 434
    sget-object v2, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v4, "Request Body: {}"

    invoke-interface {v2, v4, v14}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 437
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->al()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    :goto_9
    invoke-static {v3, v2}, Lcom/mfluent/asp/nts/b;->a(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/mfluent/asp/datamodel/Device;)Lorg/json/JSONObject;

    move-result-object v2

    .line 438
    const-string v3, "sessionId"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    .line 440
    :cond_e
    return-void

    .line 373
    :cond_f
    const-string v2, "view"

    invoke-virtual {v12, v2, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 375
    new-instance v15, Lorg/json/JSONObject;

    invoke-direct {v15}, Lorg/json/JSONObject;-><init>()V

    .line 376
    const-string v2, "status"

    const-string v3, "INCLUDE"

    invoke-virtual {v15, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 378
    new-instance v10, Lorg/json/JSONArray;

    invoke-direct {v10}, Lorg/json/JSONArray;-><init>()V

    .line 379
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/m;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_a
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/filetransfer/m$a;

    .line 380
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v3

    iget-boolean v3, v3, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->skipIfDuplicate:Z

    if-eqz v3, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    iget v5, v2, Lcom/mfluent/asp/filetransfer/m$a;->c:I

    iget-wide v6, v2, Lcom/mfluent/asp/filetransfer/m$a;->b:J

    iget-wide v8, v2, Lcom/mfluent/asp/filetransfer/m$a;->a:J

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/mfluent/asp/filetransfer/m;->a(IIJJ)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_10

    .line 382
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/mfluent/asp/filetransfer/m$a;->f:Z

    iput-boolean v3, v2, Lcom/mfluent/asp/filetransfer/m$a;->j:Z

    .line 383
    move-object/from16 v0, p0

    iget v3, v0, Lcom/mfluent/asp/filetransfer/m;->h:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/mfluent/asp/filetransfer/m;->h:I

    .line 384
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/mfluent/asp/filetransfer/m;->i:J

    iget-wide v6, v2, Lcom/mfluent/asp/filetransfer/m$a;->h:J

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/mfluent/asp/filetransfer/m;->i:J

    .line 385
    sget-object v3, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v4, "::startTransfer skipping duplicate file: {}"

    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/m$a;->g:Ljava/lang/String;

    invoke-interface {v3, v4, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_a

    .line 387
    :cond_10
    iget-object v2, v2, Lcom/mfluent/asp/filetransfer/m$a;->d:Ljava/lang/String;

    invoke-virtual {v10, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_a

    .line 390
    :cond_11
    const-string v2, "items"

    invoke-virtual {v15, v2, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 392
    const-string v2, "depth1"

    invoke-virtual {v12, v2, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-object v2, v10

    goto/16 :goto_6

    .line 405
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_7

    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    .line 437
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    goto/16 :goto_9

    .line 271
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic a(Lcom/mfluent/asp/filetransfer/m;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mfluent/asp/filetransfer/m;->b(Z)V

    return-void
.end method

.method private ak()Z
    .locals 2

    .prologue
    .line 443
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v0

    return v0
.end method

.method private al()Z
    .locals 2

    .prologue
    .line 447
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v0

    return v0
.end method

.method private am()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 452
    const/4 v4, 0x0

    .line 458
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 459
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    .line 462
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 468
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->al()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->ak()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 469
    :cond_0
    const-string v0, "api/pCloud/externalService/sessions/"

    .line 474
    :goto_0
    const/4 v3, 0x0

    .line 475
    const/4 v2, 0x0

    .line 476
    const/16 v1, 0xa

    .line 478
    iget-boolean v5, p0, Lcom/mfluent/asp/filetransfer/m;->b:Z

    if-eqz v5, :cond_c

    .line 479
    const/16 v1, 0x14

    move v5, v3

    move v3, v1

    move v14, v2

    move v2, v4

    move v4, v14

    .line 482
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 483
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->ak()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 484
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    .line 497
    :goto_2
    sget-object v7, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v8, "Start to check threebox status..."

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 500
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/mfluent/asp/nts/b;->b(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 541
    const/4 v1, 0x1

    .line 543
    const-string v7, "noOfTotalFiles"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    iget v8, p0, Lcom/mfluent/asp/filetransfer/m;->h:I

    add-int/2addr v7, v8

    iput v7, p0, Lcom/mfluent/asp/filetransfer/m;->g:I

    .line 544
    const-string v7, "totalBytes"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    iget-wide v10, p0, Lcom/mfluent/asp/filetransfer/m;->i:J

    add-long/2addr v8, v10

    invoke-virtual {p0, v8, v9}, Lcom/mfluent/asp/filetransfer/m;->a(J)V

    .line 546
    const-string v7, "indexOfFile"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/mfluent/asp/filetransfer/m;->o:I

    .line 548
    const-string v7, "totalBytesSent"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 549
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->l()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/mfluent/asp/filetransfer/m;->i:J

    sub-long/2addr v10, v12

    sub-long/2addr v8, v10

    invoke-virtual {p0, v8, v9}, Lcom/mfluent/asp/filetransfer/m;->bytesTransferred(J)V

    .line 551
    const-string v7, "status"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 552
    sget-object v7, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "GOT STATUS "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 554
    const-string v7, "COMPLETED"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 555
    sget-object v0, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v1, "::SysDBGTest-session completed"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 572
    :goto_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/filetransfer/m;->b(Z)V

    .line 578
    :goto_4
    return-void

    .line 463
    :catch_0
    move-exception v0

    .line 464
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 471
    :cond_1
    const-string v0, "api/pCloud/transfer/save/sessions/"

    goto/16 :goto_0

    .line 485
    :cond_2
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->al()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 486
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    goto/16 :goto_2

    .line 488
    :cond_3
    const/4 v0, 0x4

    if-le v2, v0, :cond_4

    .line 489
    const/4 v2, 0x0

    .line 490
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    .line 491
    const-string v0, "api/pCloud/transfer/upload/sessions/"

    goto/16 :goto_2

    .line 493
    :cond_4
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 494
    const-string v0, "api/pCloud/transfer/save/sessions/"

    goto/16 :goto_2

    .line 501
    :catch_1
    move-exception v1

    .line 502
    invoke-virtual {v1}, Lcom/mfluent/asp/nts/NonOkHttpResponseException;->a()I

    move-result v7

    const/16 v8, 0x194

    if-ne v7, v8, :cond_8

    .line 503
    if-nez v5, :cond_7

    .line 505
    if-lt v4, v3, :cond_5

    .line 507
    sget-object v0, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v1, "::SysDBGTest-session is wrong"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 508
    new-instance v0, Ljava/io/IOException;

    const-string v1, "HTTP Not Found Error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 510
    :cond_5
    const-wide/16 v8, 0x7d0

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    .line 511
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    if-ne v1, v7, :cond_6

    .line 512
    const-string v0, "INFO"

    const-string v1, "Local-copy transfer, it may return before watchProgress, return OK"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 516
    :cond_6
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    .line 518
    goto/16 :goto_1

    .line 522
    :cond_7
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->V()V

    .line 523
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->F()V

    .line 524
    sget-object v0, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v1, "::NonOkHttpResponseException session previously existed on the target device but no longer exists - assume it has completed"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    goto :goto_4

    .line 530
    :cond_8
    throw v1

    .line 560
    :cond_9
    const-string v7, "STOPPED"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 562
    sget-object v0, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v1, "::SysDBGTest-session stopped"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 563
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Remote transfer stopped"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_a
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->L()V

    .line 567
    const-wide/16 v8, 0x7d0

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    move v5, v1

    .line 568
    goto/16 :goto_1

    .line 569
    :cond_b
    iget v0, p0, Lcom/mfluent/asp/filetransfer/m;->h:I

    iput v0, p0, Lcom/mfluent/asp/filetransfer/m;->g:I

    .line 571
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/m;->i:J

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/filetransfer/m;->a(J)V

    goto/16 :goto_3

    :cond_c
    move v5, v3

    move v3, v1

    move v14, v2

    move v2, v4

    move v4, v14

    goto/16 :goto_1
.end method

.method private b(Z)V
    .locals 5

    .prologue
    .line 594
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    .line 597
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->al()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->ak()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 598
    :cond_0
    const-string v0, "api/pCloud/externalService/sessions/"

    .line 603
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/nts/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 604
    new-instance v1, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    .line 607
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    invoke-static {v1, v0}, Lcom/mfluent/asp/nts/b;->a(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/mfluent/asp/datamodel/Device;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 614
    :goto_1
    if-eqz p1, :cond_1

    .line 616
    :try_start_1
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mfluent/asp/nts/b;->a(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/mfluent/asp/datamodel/Device;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3

    .line 623
    :cond_1
    :goto_2
    return-void

    .line 600
    :cond_2
    const-string v0, "api/pCloud/transfer/upload/sessions/"

    goto :goto_0

    .line 608
    :catch_0
    move-exception v0

    .line 609
    sget-object v2, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v3, "Trouble deleting remote session {}"

    iget-object v4, p0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 610
    :catch_1
    move-exception v0

    .line 611
    sget-object v2, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v3, "Trouble deleting remote session {}"

    iget-object v4, p0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 617
    :catch_2
    move-exception v0

    .line 618
    sget-object v1, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v2, "Trouble deleting remote session {}"

    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    .line 619
    :catch_3
    move-exception v0

    .line 620
    sget-object v1, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v2, "Trouble deleting remote session {}"

    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2
.end method


# virtual methods
.method protected final A()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v2, 0x3

    .line 172
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->G()V

    .line 174
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 175
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->D()V

    .line 219
    :cond_0
    :goto_0
    :try_start_0
    iget v0, p0, Lcom/mfluent/asp/filetransfer/m;->n:I

    if-lez v0, :cond_1

    .line 220
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->am()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 226
    :cond_1
    const-string v0, "ThreeBoxTest3"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hasWrittenComplete : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->Y()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const-string v0, "ThreeBoxTest3"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isDeleteSessionAfterCancel : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->ae()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    const-string v0, "ThreeBoxTest3"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isInterrupted() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const-string v0, "ThreeBoxTest3"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isErrorOccurred() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->U()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const-string v0, "ThreeBoxTest3"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getStatus() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->N()V

    .line 234
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->deleteSourceFilesWhenTransferIsComplete:Z

    if-eqz v0, :cond_a

    .line 235
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/m$a;

    .line 236
    iget-boolean v2, v0, Lcom/mfluent/asp/filetransfer/m$a;->j:Z

    if-nez v2, :cond_2

    .line 237
    iget-wide v2, v0, Lcom/mfluent/asp/filetransfer/m$a;->a:J

    cmp-long v2, v2, v6

    if-gtz v2, :cond_b

    .line 238
    sget-object v0, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v2, "TransferOptions.deleteSourceFilesWhenTransferIsComplete for non media files is not supported"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    goto :goto_1

    .line 178
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {p0, v0, v1, v2}, Lcom/mfluent/asp/filetransfer/m;->a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;I)I

    move-result v0

    .line 179
    if-nez v0, :cond_8

    .line 181
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    :try_start_2
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->al()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->ak()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    const-string v0, "api/pCloud/externalService/sessions/"

    move-object v1, v0

    :goto_2
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->ak()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 191
    const-string v0, "ThreeBoxTest2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hasWrittenComplete : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->Y()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    const-string v0, "ThreeBoxTest2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isDeleteSessionAfterCancel : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->ae()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const-string v0, "ThreeBoxTest2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isInterrupted() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const-string v0, "ThreeBoxTest2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isErrorOccurred() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->U()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const-string v0, "ThreeBoxTest2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getStatus() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->N()V

    goto/16 :goto_0

    .line 181
    :catch_0
    move-exception v0

    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 182
    :catch_1
    move-exception v0

    .line 183
    :try_start_4
    sget-object v1, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v2, "Unable to resume transfer for session {}"

    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/m;->f:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 185
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/m;->b:Z

    if-eqz v0, :cond_5

    .line 186
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/filetransfer/m;->h:I

    .line 187
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/m;->i:J

    .line 189
    :cond_5
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->D()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 191
    const-string v0, "ThreeBoxTest2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hasWrittenComplete : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->Y()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    const-string v0, "ThreeBoxTest2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isDeleteSessionAfterCancel : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->ae()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const-string v0, "ThreeBoxTest2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isInterrupted() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const-string v0, "ThreeBoxTest2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isErrorOccurred() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->U()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const-string v0, "ThreeBoxTest2"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getStatus() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->N()V

    goto/16 :goto_0

    .line 181
    :cond_6
    :try_start_5
    const-string v0, "api/pCloud/transfer/upload/sessions/"

    move-object v1, v0

    goto/16 :goto_2

    :cond_7
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->i()Lcom/mfluent/asp/datamodel/Device;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    goto/16 :goto_3

    .line 191
    :catchall_0
    move-exception v0

    const-string v1, "ThreeBoxTest2"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "hasWrittenComplete : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->Y()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    const-string v1, "ThreeBoxTest2"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isDeleteSessionAfterCancel : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->ae()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const-string v1, "ThreeBoxTest2"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isInterrupted() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->d()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const-string v1, "ThreeBoxTest2"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isErrorOccurred() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->U()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const-string v1, "ThreeBoxTest2"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getStatus() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->N()V

    throw v0

    .line 198
    :cond_8
    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    .line 199
    const-string v0, "ThreeBoxTest1"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hasWrittenComplete : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->Y()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    const-string v0, "ThreeBoxTest1"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isDeleteSessionAfterCancel : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->ae()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    const-string v0, "ThreeBoxTest1"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isInterrupted() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const-string v0, "ThreeBoxTest1"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isErrorOccurred() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->U()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const-string v0, "ThreeBoxTest1"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getStatus() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->N()V

    .line 205
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 206
    :cond_9
    if-ne v0, v2, :cond_0

    .line 207
    const-string v0, "ThreeBoxTest0"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hasWrittenComplete : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->Y()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const-string v0, "ThreeBoxTest0"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isDeleteSessionAfterCancel : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->ae()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    const-string v0, "ThreeBoxTest0"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isInterrupted() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->d()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    const-string v0, "ThreeBoxTest0"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isErrorOccurred() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->U()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const-string v0, "ThreeBoxTest0"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getStatus() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->ai()V

    .line 213
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->N()V

    .line 245
    :cond_a
    return-void

    .line 222
    :catch_2
    move-exception v0

    .line 223
    :try_start_6
    sget-object v1, Lcom/mfluent/asp/filetransfer/m;->d:Lorg/slf4j/Logger;

    const-string v2, "SysDBGTest:io error in 3box transfer"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 224
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 226
    :catchall_1
    move-exception v0

    const-string v1, "ThreeBoxTest3"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "hasWrittenComplete : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->Y()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const-string v1, "ThreeBoxTest3"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isDeleteSessionAfterCancel : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->ae()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    const-string v1, "ThreeBoxTest3"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isInterrupted() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->d()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const-string v1, "ThreeBoxTest3"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isErrorOccurred() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->U()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const-string v1, "ThreeBoxTest3"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getStatus() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->N()V

    throw v0

    .line 240
    :cond_b
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->O()Landroid/content/Context;

    move-result-object v2

    iget-wide v4, v0, Lcom/mfluent/asp/filetransfer/m$a;->a:J

    invoke-static {v2, v4, v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;->deleteFile(Landroid/content/Context;J)V

    goto/16 :goto_1
.end method

.method public final C()Ljava/lang/String;
    .locals 5

    .prologue
    .line 637
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 638
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/m$a;

    .line 640
    iget-boolean v3, v0, Lcom/mfluent/asp/filetransfer/m$a;->f:Z

    if-nez v3, :cond_0

    .line 641
    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 644
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/m$a;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 645
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 647
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/Cursor;Z)V
    .locals 5

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/m;->k:Z

    if-eqz v0, :cond_0

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t mix media and file transfers in the same task"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    if-nez p2, :cond_1

    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Threebox transfers must use slinkMediaStore cursors"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/m;->j:Z

    .line 90
    const-string v0, "device_id"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getLongOrThrow(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v0

    .line 91
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 92
    invoke-virtual {p0, v0}, Lcom/mfluent/asp/filetransfer/m;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 94
    new-instance v0, Lcom/mfluent/asp/filetransfer/m$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/mfluent/asp/filetransfer/m$a;-><init>(B)V

    .line 95
    const-string v1, "_id"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getLongOrThrow(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/mfluent/asp/filetransfer/m$a;->a:J

    .line 96
    const-string v1, "media_type"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getIntOrThrow(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/mfluent/asp/filetransfer/m$a;->c:I

    .line 97
    const-string v1, "source_media_id"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/m$a;->d:Ljava/lang/String;

    .line 98
    const-string v1, "full_uri"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/m$a;->e:Ljava/lang/String;

    .line 99
    const-string v1, "_display_name"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/m$a;->g:Ljava/lang/String;

    .line 100
    const-string v1, "_size"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getLongOrThrow(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/mfluent/asp/filetransfer/m$a;->h:J

    .line 101
    const-string v1, "dup_id"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, v0, Lcom/mfluent/asp/filetransfer/m$a;->b:J

    .line 103
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->al()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 105
    :try_start_0
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v1

    iget v2, v0, Lcom/mfluent/asp/filetransfer/m$a;->c:I

    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/m$a;->e:Ljava/lang/String;

    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/m$a;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getStorageGatewayFileId(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/m$a;->i:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :cond_2
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/m;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    iget v0, p0, Lcom/mfluent/asp/filetransfer/m;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/filetransfer/m;->g:I

    .line 116
    return-void

    .line 109
    :catch_0
    move-exception v0

    .line 110
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trouble getting storageGatewayFileId for cursor record "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/database/DatabaseUtils;->dumpCurrentRowToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 120
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/m;->j:Z

    if-eqz v0, :cond_0

    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t mix media and file transfers in the same task"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/m;->k:Z

    .line 126
    new-instance v0, Lcom/mfluent/asp/filetransfer/m$a;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/filetransfer/m$a;-><init>(B)V

    .line 127
    iput v1, v0, Lcom/mfluent/asp/filetransfer/m$a;->c:I

    .line 128
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/m$a;->g:Ljava/lang/String;

    .line 129
    invoke-interface {p1}, Lcom/mfluent/asp/common/datamodel/ASPFile;->length()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/mfluent/asp/filetransfer/m$a;->h:J

    .line 131
    invoke-virtual {p0, p2}, Lcom/mfluent/asp/filetransfer/m;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 133
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/m;->al()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    :try_start_0
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getStorageGatewayFileId(Lcom/mfluent/asp/common/datamodel/ASPFile;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/m$a;->i:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :goto_0
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/m;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    iget v0, p0, Lcom/mfluent/asp/filetransfer/m;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/filetransfer/m;->g:I

    .line 145
    return-void

    .line 136
    :catch_0
    move-exception v0

    .line 137
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trouble getting storageGatewayFileId for file "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 140
    :cond_1
    check-cast p1, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->d()Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/filebrowser/ASP10File;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mfluent/asp/filetransfer/m;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 2

    .prologue
    .line 149
    invoke-super {p0, p1}, Lcom/mfluent/asp/filetransfer/f;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 150
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/m;->h()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 151
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "3box transfer only supports a single source device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :cond_0
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/m;->m:Lcom/mfluent/asp/datamodel/Device;

    .line 154
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 582
    invoke-super {p0, p1}, Lcom/mfluent/asp/filetransfer/f;->a(Z)V

    .line 584
    new-instance v0, Lcom/mfluent/asp/filetransfer/m$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/filetransfer/m$1;-><init>(Lcom/mfluent/asp/filetransfer/m;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/m$1;->start()V

    .line 591
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 627
    iget v0, p0, Lcom/mfluent/asp/filetransfer/m;->g:I

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 632
    iget v0, p0, Lcom/mfluent/asp/filetransfer/m;->h:I

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 652
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/m;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/m$a;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/m$a;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 671
    iget v0, p0, Lcom/mfluent/asp/filetransfer/m;->o:I

    return v0
.end method
