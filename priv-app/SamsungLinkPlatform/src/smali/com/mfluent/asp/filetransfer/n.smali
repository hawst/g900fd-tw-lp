.class Lcom/mfluent/asp/filetransfer/n;
.super Lcom/mfluent/asp/filetransfer/f;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/filetransfer/FileUploader$c;


# static fields
.field private static final d:Lorg/slf4j/Logger;


# instance fields
.field private e:Lcom/mfluent/asp/filetransfer/FileUploader;

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/filetransfer/FileUploader$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/mfluent/asp/filetransfer/n;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mfluent/asp/filetransfer/f;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    .line 64
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    .line 65
    return-void
.end method

.method static synthetic D()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/filetransfer/n;)V
    .locals 4

    .prologue
    .line 51
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    const-string v0, "api/pCloud/transfer/save/sessions/"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/nts/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/mfluent/asp/nts/b;->a(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/mfluent/asp/datamodel/Device;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v2, "Trouble deleting remote session {}"

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v2, "Trouble deleting remote session {}"

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private b(Lcom/mfluent/asp/filetransfer/FileUploader$a;)Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 345
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->O()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ArchivedMedia;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "COUNT(*)"

    aput-object v3, v2, v7

    const-string v3, "device_id=? AND _size=? AND _display_name=? AND datetaken=?"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v5

    int-to-long v8, v5

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x2

    iget-object v8, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->l:Ljava/lang/String;

    aput-object v8, v4, v5

    const/4 v5, 0x3

    iget-wide v8, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->k:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 365
    if-eqz v1, :cond_2

    .line 366
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_0

    move v0, v6

    .line 370
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 373
    :goto_1
    return v0

    :cond_0
    move v0, v7

    .line 367
    goto :goto_0

    :cond_1
    move v0, v7

    goto :goto_0

    :cond_2
    move v0, v7

    goto :goto_1
.end method

.method private c(Lcom/mfluent/asp/filetransfer/FileUploader$a;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 379
    const-string v0, "source_media_id"

    .line 381
    iget v1, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    sparse-switch v1, :sswitch_data_0

    .line 397
    sget-object v0, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v1, "::deleteContent: createDeleteMediaAsyncTask: unknown mediaType: {}"

    iget v2, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    .line 425
    :goto_0
    return-void

    .line 383
    :sswitch_0
    new-instance v1, Lcom/mfluent/asp/media/i;

    invoke-direct {v1}, Lcom/mfluent/asp/media/i;-><init>()V

    .line 401
    :goto_1
    new-instance v2, Lcom/mfluent/asp/filetransfer/n$1;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/filetransfer/n$1;-><init>(Lcom/mfluent/asp/filetransfer/n;)V

    .line 423
    new-array v3, v6, [Ljava/lang/String;

    iget v4, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v3, v4, v2}, Lcom/mfluent/asp/media/f;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/media/l;)V

    .line 424
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v6, [Landroid/content/Context;

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->O()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/media/f;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 386
    :sswitch_1
    new-instance v1, Lcom/mfluent/asp/media/g;

    invoke-direct {v1}, Lcom/mfluent/asp/media/g;-><init>()V

    goto :goto_1

    .line 389
    :sswitch_2
    new-instance v1, Lcom/mfluent/asp/media/k;

    invoke-direct {v1}, Lcom/mfluent/asp/media/k;-><init>()V

    .line 390
    const-string v0, "_id"

    goto :goto_1

    .line 393
    :sswitch_3
    new-instance v1, Lcom/mfluent/asp/media/h;

    invoke-direct {v1}, Lcom/mfluent/asp/media/h;-><init>()V

    .line 394
    const-string v0, "_id"

    goto :goto_1

    .line 381
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0xf -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method protected final A()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 219
    sget-object v0, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v1, "::doTransfer: Start"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 221
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->U()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    if-nez v0, :cond_1

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getCapacityInBytes()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->m()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->k()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 227
    sget-object v0, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v1, "::doTransfer: Device Full"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 228
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->R()V

    .line 229
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->V()V

    goto :goto_0

    .line 233
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->G()V

    .line 235
    sget-object v0, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v1, "::doTransfer: isSecureTransfer : {}"

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->W()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 236
    sget-object v0, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v1, "::doTransfer: isPersonal : {}"

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->X()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 239
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->e:Lcom/mfluent/asp/filetransfer/FileUploader;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/n;->b:Z

    if-eqz v0, :cond_3

    .line 241
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->e:Lcom/mfluent/asp/filetransfer/FileUploader;

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/FileUploader;->b()Ljava/lang/String;

    move-result-object v0

    .line 242
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v12}, Lcom/mfluent/asp/filetransfer/n;->a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;I)I

    move-result v0

    .line 243
    if-ne v0, v13, :cond_3

    .line 244
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->ai()V

    goto :goto_0

    .line 249
    :cond_3
    new-instance v0, Lcom/mfluent/asp/filetransfer/FileUploader;

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->O()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v3

    iget-boolean v3, v3, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->autoUpload:Z

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->X()Z

    move-result v7

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->W()Z

    move-result v8

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v4

    iget-object v9, v4, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->targetDirectory:Ljava/io/File;

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v9}, Lcom/mfluent/asp/filetransfer/FileUploader;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;ZLcom/mfluent/asp/common/io/util/StreamProgressListener;Lcom/mfluent/asp/filetransfer/FileUploader$c;Ljava/lang/String;ZZLjava/io/File;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->e:Lcom/mfluent/asp/filetransfer/FileUploader;

    .line 260
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->e:Lcom/mfluent/asp/filetransfer/FileUploader;

    iget-boolean v1, p0, Lcom/mfluent/asp/filetransfer/n;->b:Z

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/filetransfer/FileUploader;->a(Z)V

    .line 261
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    .line 262
    iget-object v1, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    .line 264
    sget-object v2, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v3, "::doTransfer() : fileUpload.displayName:{} fileUpload.mediaType:{} fileUpload.mimeType:{}"

    new-array v4, v13, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->l:Ljava/lang/String;

    aput-object v5, v4, v11

    iget v5, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v10

    iget-object v5, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    aput-object v5, v4, v12

    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_4

    .line 271
    sget-object v2, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v3, "::doTransfer: upload: file doesn\'t exist! id: {}, {}"

    iget v4, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 272
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->V()V

    .line 275
    invoke-direct {p0, v0}, Lcom/mfluent/asp/filetransfer/n;->c(Lcom/mfluent/asp/filetransfer/FileUploader$a;)V

    .line 276
    invoke-virtual {p0, v11}, Lcom/mfluent/asp/filetransfer/n;->a(Z)V

    goto/16 :goto_0

    .line 279
    :cond_4
    iget-boolean v1, p0, Lcom/mfluent/asp/filetransfer/n;->b:Z

    if-nez v1, :cond_6

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v1

    iget-boolean v1, v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->skipIfDuplicate:Z

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    iget v3, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    iget v1, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->c:I

    int-to-long v4, v1

    iget v1, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->b:I

    int-to-long v6, v1

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/mfluent/asp/filetransfer/n;->a(IIJJ)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    move v1, v10

    .line 285
    :goto_2
    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->autoUpload:Z

    if-eqz v2, :cond_5

    invoke-direct {p0, v0}, Lcom/mfluent/asp/filetransfer/n;->b(Lcom/mfluent/asp/filetransfer/FileUploader$a;)Z

    move-result v2

    if-eqz v2, :cond_5

    move v1, v10

    .line 290
    :cond_5
    if-eqz v1, :cond_7

    .line 291
    sget-object v1, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v2, "::doTransfer skipping file due to duplicate. file: {}"

    iget v3, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 292
    iput-boolean v10, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->i:Z

    iput-boolean v10, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->j:Z

    .line 293
    iget v1, p0, Lcom/mfluent/asp/filetransfer/n;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/mfluent/asp/filetransfer/n;->h:I

    .line 294
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/filetransfer/n;->bytesTransferred(J)V

    goto/16 :goto_1

    :cond_6
    move v1, v11

    .line 279
    goto :goto_2

    .line 296
    :cond_7
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/n;->e:Lcom/mfluent/asp/filetransfer/FileUploader;

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/filetransfer/FileUploader;->a(Lcom/mfluent/asp/filetransfer/FileUploader$a;)V

    goto/16 :goto_1

    .line 300
    :cond_8
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->e:Lcom/mfluent/asp/filetransfer/FileUploader;

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/FileUploader;->a()V

    .line 302
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    .line 303
    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    .line 305
    sget-object v3, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v4, "::doTransfer() : fileUpload.displayName:{} fileUpload.mediaType:{} fileUpload.mimeType:{}"

    new-array v5, v13, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->l:Ljava/lang/String;

    aput-object v6, v5, v11

    iget v6, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    iget-object v6, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    aput-object v6, v5, v12

    invoke-interface {v3, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 311
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v3

    iget-boolean v3, v3, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->deleteSourceFilesWhenTransferIsComplete:Z

    if-eqz v3, :cond_a

    .line 312
    sget-object v3, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v4, "::doTransfer: delete: {}, {}"

    iget v5, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 313
    iget v3, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->b:I

    if-lez v3, :cond_c

    .line 315
    invoke-direct {p0, v0}, Lcom/mfluent/asp/filetransfer/n;->c(Lcom/mfluent/asp/filetransfer/FileUploader$a;)V

    .line 322
    :cond_a
    :goto_3
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v2

    iget-boolean v2, v2, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->autoUpload:Z

    if-eqz v2, :cond_b

    iget-boolean v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->h:Z

    if-nez v2, :cond_b

    .line 323
    invoke-direct {p0, v0}, Lcom/mfluent/asp/filetransfer/n;->b(Lcom/mfluent/asp/filetransfer/FileUploader$a;)Z

    move-result v2

    if-nez v2, :cond_b

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "device_id"

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "_size"

    iget-object v4, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "datetaken"

    iget-wide v4, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->k:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "_display_name"

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->l:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->O()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$ArchivedMedia;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 326
    :cond_b
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->U()Z

    move-result v0

    if-eqz v0, :cond_9

    goto/16 :goto_0

    .line 318
    :cond_c
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_3
.end method

.method public final C()Ljava/lang/String;
    .locals 6

    .prologue
    .line 461
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 462
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 463
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    .line 465
    iget-boolean v4, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->i:Z

    if-nez v4, :cond_0

    .line 466
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 469
    iget v4, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->b:I

    if-lez v4, :cond_1

    .line 470
    iget v0, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->b:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 474
    :goto_1
    const-string v0, ";"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 472
    :cond_1
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 476
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/Cursor;Z)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x2

    const/4 v10, 0x1

    .line 116
    sget-object v1, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v2, "addTask(cursor, isSlinkMediaStoreCursor:{})"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 118
    const-string v1, "_data"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 119
    if-nez v1, :cond_0

    .line 120
    sget-object v0, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v1, "::addTask: invalid file to upload!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->V()V

    .line 179
    :goto_0
    return-void

    .line 127
    :cond_0
    new-instance v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    invoke-direct {v4}, Lcom/mfluent/asp/filetransfer/FileUploader$a;-><init>()V

    .line 128
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    .line 129
    const-string v1, "media_type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    .line 130
    invoke-static {p1}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForMedia(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    .line 131
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->b:I

    .line 132
    const-string v1, "dup_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 134
    iget v2, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    if-ne v10, v2, :cond_1

    .line 135
    const-string v2, "group_id"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->m:Ljava/lang/String;

    .line 138
    :cond_1
    if-ltz v1, :cond_2

    .line 139
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->c:I

    .line 142
    :cond_2
    const-string v1, "_display_name"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->l:Ljava/lang/String;

    .line 144
    iget-object v1, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 146
    iget v1, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    const/4 v5, 0x3

    if-ne v1, v5, :cond_4

    .line 147
    const-string v1, "datetaken"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->k:J

    .line 148
    new-array v1, v8, [Ljava/lang/String;

    const-string v5, "caption_uri"

    aput-object v5, v1, v0

    const-string v5, "caption_index_uri"

    aput-object v5, v1, v10

    .line 149
    new-array v5, v8, [I

    fill-array-data v5, :array_0

    .line 151
    :goto_1
    array-length v6, v1

    if-ge v0, v6, :cond_5

    .line 152
    aget-object v6, v1, v0

    invoke-static {p1, v6}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 154
    invoke-static {v6}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 155
    new-instance v7, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    invoke-direct {v7}, Lcom/mfluent/asp/filetransfer/FileUploader$a;-><init>()V

    .line 156
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v8, v7, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    .line 157
    iput-boolean v10, v7, Lcom/mfluent/asp/filetransfer/FileUploader$a;->h:Z

    .line 158
    iget-object v6, v7, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 159
    iget-object v6, v7, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v8

    add-long/2addr v2, v8

    .line 160
    aget v6, v5, v0

    iput v6, v7, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    .line 161
    iget-object v6, v7, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-static {v6}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    .line 162
    iget-object v6, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    iget v6, p0, Lcom/mfluent/asp/filetransfer/n;->g:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/mfluent/asp/filetransfer/n;->g:I

    .line 151
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 167
    :cond_4
    iget v0, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    if-ne v0, v8, :cond_6

    .line 168
    const-string v0, "artist"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->f:Ljava/lang/String;

    .line 169
    const-string v0, "album"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->g:Ljava/lang/String;

    .line 174
    :cond_5
    :goto_2
    sget-object v0, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v1, "::addTask() mediaType:{} mimeType:{}"

    iget v5, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 175
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->k()J

    move-result-wide v0

    add-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/filetransfer/n;->a(J)V

    .line 178
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/filetransfer/n;->a(Lcom/mfluent/asp/datamodel/Device;)V

    goto/16 :goto_0

    .line 170
    :cond_6
    iget v0, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    if-ne v0, v10, :cond_5

    .line 171
    const-string v0, "datetaken"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, v4, Lcom/mfluent/asp/filetransfer/FileUploader$a;->k:J

    goto :goto_2

    .line 149
    nop

    :array_0
    .array-data 4
        0xd
        0xe
    .end array-data
.end method

.method public final a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 74
    sget-object v0, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v1, "addTask(ASPFile, sourceDevice)"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Upload source device must be the local device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    check-cast p1, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v2, "addTask(ASPFile, mediaType:{})"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    new-instance v1, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    invoke-direct {v1}, Lcom/mfluent/asp/filetransfer/FileUploader$a;-><init>()V

    iput-object v0, v1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-static {v0}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    iput v4, v1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    iput-object v5, v1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->f:Ljava/lang/String;

    iput-object v5, v1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->g:Ljava/lang/String;

    sget-object v2, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v3, "::addTask() mediaType:{} mimeType:{}"

    iget v4, v1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, v1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->k()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    add-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/mfluent/asp/filetransfer/n;->a(J)V

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/filetransfer/n;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 79
    invoke-virtual {p0, p2}, Lcom/mfluent/asp/filetransfer/n;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 80
    return-void
.end method

.method public final a(Lcom/mfluent/asp/filetransfer/FileUploader$a;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 434
    sget-object v0, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v1, "::onUploadComplete() fileUploadInfo.displayName:{} fileUploadInfo.mediaType:{} fileUploadInfo.mimeType:{}"

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->l:Ljava/lang/String;

    aput-object v3, v2, v7

    iget v3, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->e:Ljava/lang/String;

    aput-object v3, v2, v8

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 440
    iput-boolean v6, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->i:Z

    .line 442
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->L()V

    .line 443
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->h()Ljava/util/Set;

    move-result-object v0

    .line 444
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-ne v1, v6, :cond_1

    .line 445
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->i()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    sget-object v2, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v3, "Enter ::logAnalyticsDataForUpload() : fileName:{} downloadItem.mediaType:{} sourceDevice:{} targetDevice:{}"

    new-array v4, v10, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->l:Ljava/lang/String;

    aput-object v5, v4, v7

    iget v5, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v0, v4, v8

    aput-object v1, v4, v9

    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->ah()I

    move-result v2

    invoke-static {v2}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(I)Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    move-result-object v2

    sget-object v3, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->h:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    if-eq v2, v3, :cond_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    sget-object v2, Lcom/mfluent/asp/filetransfer/n;->d:Lorg/slf4j/Logger;

    const-string v3, "Invalid value. clientAppId:{} fileUploadInfo.mediaType:{} fileName:{} sourceDevice:{} targetDevice:{}"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->ah()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    iget v5, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    aput-object v0, v4, v9

    aput-object v1, v4, v10

    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 447
    :cond_1
    :goto_0
    return-void

    .line 445
    :cond_2
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v0

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_0

    :cond_3
    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->c(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->b:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_0

    :cond_4
    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->d(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->c:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_0

    :cond_5
    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->e(I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->e:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->d:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_0

    :cond_7
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, v1, :cond_c

    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->b(I)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->f:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_0

    :cond_8
    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->c(I)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->g:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_0

    :cond_9
    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->d(I)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->h:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_0

    :cond_a
    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->e(I)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->j:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->i:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_0

    :cond_c
    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->b(I)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->k:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_0

    :cond_d
    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->c(I)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->l:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_0

    :cond_e
    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->d(I)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->m:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_0

    :cond_f
    iget v0, p1, Lcom/mfluent/asp/filetransfer/FileUploader$a;->d:I

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/n;->e(I)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->o:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_0

    :cond_10
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->i:Landroid/content/Context;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->n:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, v2, v1}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 529
    invoke-super {p0, p1}, Lcom/mfluent/asp/filetransfer/f;->a(Z)V

    .line 531
    new-instance v0, Lcom/mfluent/asp/filetransfer/n$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/filetransfer/n$2;-><init>(Lcom/mfluent/asp/filetransfer/n;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/filetransfer/n$2;->start()V

    .line 538
    return-void
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/mfluent/asp/filetransfer/n;->g:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/mfluent/asp/filetransfer/n;->h:I

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 3

    .prologue
    .line 481
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    .line 482
    iget-boolean v2, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->h:Z

    if-nez v2, :cond_0

    .line 483
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 487
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x0

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->autoUpload:Z

    return v0
.end method

.method public final r()I
    .locals 2

    .prologue
    .line 505
    const/4 v0, 0x0

    move v1, v0

    .line 506
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    iget-boolean v0, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->i:Z

    if-eqz v0, :cond_0

    .line 508
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 512
    :cond_0
    return v1
.end method

.method public final s()Ljava/lang/String;
    .locals 2

    .prologue
    .line 517
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->r()I

    move-result v0

    .line 518
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final t()J
    .locals 5

    .prologue
    .line 492
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->l()J

    move-result-wide v0

    .line 494
    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    .line 495
    iget-boolean v1, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->i:Z

    if-eqz v1, :cond_1

    .line 496
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    sub-long v0, v2, v0

    :goto_1
    move-wide v2, v0

    .line 498
    goto :goto_0

    .line 500
    :cond_0
    return-wide v2

    :cond_1
    move-wide v0, v2

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 451
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "UploadTask: #"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 452
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v1

    iget-boolean v1, v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->deleteSourceFilesWhenTransferIsComplete:Z

    if-eqz v1, :cond_0

    .line 453
    const-string v1, " +delete"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 455
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()J
    .locals 2

    .prologue
    .line 523
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/n;->r()I

    move-result v0

    .line 524
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/FileUploader$a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method
