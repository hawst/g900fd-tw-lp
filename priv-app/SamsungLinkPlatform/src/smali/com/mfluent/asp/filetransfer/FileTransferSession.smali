.class public interface abstract Lcom/mfluent/asp/filetransfer/FileTransferSession;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;
    }
.end annotation


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V
.end method

.method public abstract b()J
.end method

.method public abstract c()I
.end method

.method public abstract d()Z
.end method

.method public abstract e()Z
.end method

.method public abstract f()I
.end method

.method public abstract g()I
.end method

.method public abstract h()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation
.end method

.method public abstract i()Lcom/mfluent/asp/datamodel/Device;
.end method

.method public abstract j()J
.end method

.method public abstract k()J
.end method

.method public abstract l()J
.end method

.method public abstract m()Ljava/lang/String;
.end method

.method public abstract n()Z
.end method

.method public abstract o()Z
.end method

.method public abstract p()Z
.end method

.method public abstract q()Z
.end method

.method public abstract r()I
.end method

.method public abstract s()Ljava/lang/String;
.end method

.method public abstract t()J
.end method

.method public abstract u()J
.end method

.method public abstract v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;
.end method

.method public abstract w()Ljava/lang/String;
.end method

.method public abstract x()Z
.end method

.method public abstract y()Z
.end method
