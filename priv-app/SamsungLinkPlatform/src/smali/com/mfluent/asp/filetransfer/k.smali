.class public final Lcom/mfluent/asp/filetransfer/k;
.super Ljava/io/OutputStream;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/OutputStream;

.field private final b:J

.field private c:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;J)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/k;->a:Ljava/io/OutputStream;

    .line 17
    iput-wide p2, p0, Lcom/mfluent/asp/filetransfer/k;->b:J

    .line 18
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/k;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 53
    return-void
.end method

.method public final flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/k;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 48
    return-void
.end method

.method public final write(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/k;->c:J

    iget-wide v2, p0, Lcom/mfluent/asp/filetransfer/k;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/k;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 24
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/k;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/k;->c:J

    .line 26
    :cond_0
    return-void
.end method

.method public final write([B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/mfluent/asp/filetransfer/k;->write([BII)V

    .line 31
    return-void
.end method

.method public final write([BII)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/k;->c:J

    iget-wide v2, p0, Lcom/mfluent/asp/filetransfer/k;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 36
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/k;->b:J

    iget-wide v2, p0, Lcom/mfluent/asp/filetransfer/k;->c:J

    sub-long/2addr v0, v2

    .line 37
    int-to-long v2, p3

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 38
    long-to-int p3, v0

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/k;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 41
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/k;->c:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/k;->c:J

    .line 43
    :cond_1
    return-void
.end method
