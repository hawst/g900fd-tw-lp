.class Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;
.super Ljava/util/LinkedList;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotificationQueue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedList",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final limit:I

.field final synthetic this$0:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)V
    .locals 1

    .prologue
    .line 173
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;->this$0:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;

    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    .line 174
    const/16 v0, 0x28

    iput v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;->limit:I

    .line 175
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)TE;"
        }
    .end annotation

    .prologue
    .line 178
    const/4 v0, 0x0

    .line 179
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;->size()I

    move-result v1

    iget v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;->limit:I

    if-le v1, v2, :cond_0

    .line 180
    invoke-super {p0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    .line 182
    :cond_0
    invoke-super {p0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 184
    return-object v0
.end method
