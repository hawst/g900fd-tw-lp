.class public final Lcom/mfluent/asp/filetransfer/h;
.super Lcom/mfluent/asp/filetransfer/f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/filetransfer/h$a;
    }
.end annotation


# instance fields
.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mfluent/asp/filetransfer/h$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mfluent/asp/filetransfer/f;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/h;->d:Ljava/util/List;

    .line 29
    iget-object v0, p4, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->targetDirectory:Ljava/io/File;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Target Directory cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    return-void
.end method


# virtual methods
.method protected final A()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/h;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v0

    iget-object v1, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->targetDirectory:Ljava/io/File;

    .line 83
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Target directory can\'t be created : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/h;->G()V

    .line 91
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/h;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/h$a;

    .line 93
    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/h$a;->a:Ljava/io/File;

    invoke-static {v3, v1}, Lorg/apache/commons/io/FileUtils;->copyFileToDirectory(Ljava/io/File;Ljava/io/File;)V

    .line 95
    iget-object v3, v0, Lcom/mfluent/asp/filetransfer/h$a;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/mfluent/asp/filetransfer/h;->bytesTransferred(J)V

    .line 97
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/h;->ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    move-result-object v3

    iget-boolean v3, v3, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->deleteSourceFilesWhenTransferIsComplete:Z

    if-eqz v3, :cond_1

    .line 98
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/h$a;->a:Ljava/io/File;

    invoke-static {v0}, Lorg/apache/commons/io/FileUtils;->deleteQuietly(Ljava/io/File;)Z

    goto :goto_0

    .line 103
    :cond_2
    return-void
.end method

.method public final C()Ljava/lang/String;
    .locals 3

    .prologue
    .line 107
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 108
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/h;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/h$a;

    .line 109
    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/h$a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 110
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 112
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/Cursor;Z)V
    .locals 4

    .prologue
    .line 56
    new-instance v0, Lcom/mfluent/asp/filetransfer/h$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/filetransfer/h$a;-><init>(Lcom/mfluent/asp/filetransfer/h;B)V

    .line 58
    const-string v1, "_data"

    invoke-static {p1, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 59
    const-string v2, "_id"

    invoke-static {p1, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getStringOrThrow(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 61
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v3, v0, Lcom/mfluent/asp/filetransfer/h$a;->a:Ljava/io/File;

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/h$a;->b:Ljava/lang/String;

    .line 64
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/h;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mfluent/asp/filetransfer/h;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 67
    return-void
.end method

.method public final a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 3

    .prologue
    .line 71
    new-instance v0, Lcom/mfluent/asp/filetransfer/h$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/filetransfer/h$a;-><init>(Lcom/mfluent/asp/filetransfer/h;B)V

    .line 72
    check-cast p1, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/filebrowser/LocalASPFile;->a()Ljava/io/File;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/h$a;->a:Ljava/io/File;

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/mfluent/asp/filetransfer/h$a;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mfluent/asp/filetransfer/h$a;->b:Ljava/lang/String;

    .line 74
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/h;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    invoke-virtual {p0, p2}, Lcom/mfluent/asp/filetransfer/h;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 76
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/h;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/h;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/h$a;

    iget-object v0, v0, Lcom/mfluent/asp/filetransfer/h$a;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method
