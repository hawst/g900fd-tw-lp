.class public final enum Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/filetransfer/FileTransferSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

.field public static final enum b:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

.field public static final enum c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

.field public static final enum d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

.field private static final synthetic e:[Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v2}, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->a:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    .line 14
    new-instance v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    const-string v1, "SENDING"

    invoke-direct {v0, v1, v3}, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->b:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    .line 15
    new-instance v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v4}, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    .line 16
    new-instance v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v5}, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    .line 12
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->a:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->b:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->e:[Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    return-object v0
.end method

.method public static values()[Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->e:[Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    invoke-virtual {v0}, [Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    return-object v0
.end method
