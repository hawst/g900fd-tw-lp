.class final synthetic Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$8;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 666
    invoke-static {}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->values()[Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$8;->a:[I

    :try_start_0
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$8;->a:[I

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->COMPLETE:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$8;->a:[I

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->CANCELLED:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    :try_start_2
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$8;->a:[I

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ERROR:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$8;->a:[I

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->PENDING:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$8;->a:[I

    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->IN_PROGRESS:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$FileTransferState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    return-void

    :catch_0
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v0

    goto :goto_0
.end method
