.class final Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field a:Z

.field b:Z

.field final synthetic c:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 838
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->c:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 840
    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->a:Z

    .line 850
    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 0

    .prologue
    .line 847
    iput-boolean p1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->a:Z

    .line 848
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 857
    iput-boolean p1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->b:Z

    .line 858
    return-void
.end method

.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 862
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->b:Z

    if-eqz v0, :cond_1

    .line 863
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->i()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    move-result v0

    .line 864
    invoke-static {}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->j()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    move-result v1

    .line 865
    add-int v2, v0, v1

    .line 867
    invoke-static {}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->k()Lorg/slf4j/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "showAutoUploadNotification: reset completed count of AutoUpload ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "(completed:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ",canceled:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") -> 0)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 876
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->c:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;

    invoke-static {v0}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->a(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 877
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->c:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;

    invoke-static {v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->b(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;

    move-result-object v1

    new-instance v2, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;

    iget-object v3, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->c:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;

    const-string v4, "com.mfluent.asp.filetransfer.FileTransferManagerImpl.TRANSFER_NOTIFICATION_TAG"

    invoke-direct {v2, v3, v4, v6}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;-><init>(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$NotificationQueue;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;

    .line 880
    if-eqz v1, :cond_2

    .line 881
    invoke-virtual {v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$b;->b()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 883
    :cond_2
    const-string v1, "com.mfluent.asp.filetransfer.FileTransferManagerImpl.TRANSFER_NOTIFICATION_TAG"

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl$a;->c:Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;

    invoke-static {v2}, Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;->c(Lcom/mfluent/asp/filetransfer/FileTransferManagerImpl;)Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v6, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 887
    return-void
.end method
