.class public abstract Lcom/mfluent/asp/filetransfer/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/common/io/util/StreamProgressListener;
.implements Lcom/mfluent/asp/filetransfer/FileTransferSession;
.implements Ljava/lang/Runnable;


# static fields
.field protected static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:Z

.field private F:Landroid/os/Handler;

.field private G:Z

.field private H:Z

.field private I:I

.field private J:Z

.field private K:Z

.field private L:Lcom/mfluent/asp/util/t$d;

.field public b:Z

.field protected c:I

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/mfluent/asp/datamodel/Device;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/mfluent/asp/filetransfer/g;

.field private final h:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

.field private i:J

.field private j:J

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private final s:Ljava/lang/String;

.field private final t:J

.field private u:J

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V
    .locals 6

    .prologue
    .line 141
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/mfluent/asp/filetransfer/f;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;B)V

    .line 142
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/filetransfer/g;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;B)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->d:Ljava/util/Set;

    .line 70
    iput-wide v6, p0, Lcom/mfluent/asp/filetransfer/f;->i:J

    .line 72
    iput-wide v6, p0, Lcom/mfluent/asp/filetransfer/f;->j:J

    .line 93
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->t:J

    .line 105
    iput v4, p0, Lcom/mfluent/asp/filetransfer/f;->z:I

    .line 114
    iput v4, p0, Lcom/mfluent/asp/filetransfer/f;->A:I

    .line 115
    iput v4, p0, Lcom/mfluent/asp/filetransfer/f;->B:I

    .line 117
    iput v4, p0, Lcom/mfluent/asp/filetransfer/f;->D:I

    .line 121
    iput-boolean v3, p0, Lcom/mfluent/asp/filetransfer/f;->G:Z

    .line 122
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->H:Z

    .line 123
    iput v4, p0, Lcom/mfluent/asp/filetransfer/f;->I:I

    .line 124
    iput-boolean v3, p0, Lcom/mfluent/asp/filetransfer/f;->J:Z

    .line 125
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->K:Z

    .line 135
    iput-object v2, p0, Lcom/mfluent/asp/filetransfer/f;->L:Lcom/mfluent/asp/util/t$d;

    .line 136
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->c:I

    .line 146
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->f:Landroid/content/Context;

    .line 147
    iput-object p2, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    .line 148
    iput-object p3, p0, Lcom/mfluent/asp/filetransfer/f;->g:Lcom/mfluent/asp/filetransfer/g;

    .line 150
    if-nez p4, :cond_0

    .line 151
    new-instance p4, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {p4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 153
    :cond_0
    iput-object p4, p0, Lcom/mfluent/asp/filetransfer/f;->h:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    .line 155
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 156
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyyMMddHHmmss"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "-"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_1

    const-string v0, "mfl_FileTransferTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::createSessionId:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.sdk.samsunglink.FILE_TRANSFER_SESSION_ID_BROADCAST_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "sessionId"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/f;->f:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->s:Ljava/lang/String;

    .line 161
    :goto_0
    iput v4, p0, Lcom/mfluent/asp/filetransfer/f;->C:I

    .line 162
    iput v4, p0, Lcom/mfluent/asp/filetransfer/f;->D:I

    .line 163
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->E:Z

    .line 164
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->b:Z

    .line 165
    iget v0, p0, Lcom/mfluent/asp/filetransfer/f;->B:I

    if-nez v0, :cond_2

    .line 167
    const/16 v0, 0xc

    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->B:I

    .line 170
    :cond_2
    iget v0, p0, Lcom/mfluent/asp/filetransfer/f;->A:I

    if-nez v0, :cond_3

    .line 172
    const/4 v0, 0x4

    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->A:I

    .line 175
    :cond_3
    return-void

    .line 158
    :cond_4
    iput-object v2, p0, Lcom/mfluent/asp/filetransfer/f;->s:Ljava/lang/String;

    goto :goto_0
.end method

.method private declared-synchronized D()V
    .locals 0

    .prologue
    .line 269
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;)I
    .locals 12

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v11, 0x4

    const/4 v1, 0x0

    .line 879
    const/4 v2, 0x0

    .line 880
    const-string v3, "null"

    .line 883
    if-eqz p2, :cond_f

    .line 886
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {p2, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v0

    .line 888
    :goto_0
    iget-object v7, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    sget-object v8, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v7, v8}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Z

    move-result v8

    .line 890
    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 984
    :cond_0
    :goto_1
    return v1

    .line 894
    :cond_1
    :try_start_0
    const-string v7, "UTF-8"

    invoke-static {p1, v7}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_8

    move-result-object v9

    .line 898
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    .line 905
    if-nez v0, :cond_2

    if-eqz v8, :cond_6

    .line 906
    :cond_2
    const-string v7, "api/pCloud/externalService/sessions/"

    .line 911
    :goto_2
    if-eqz v8, :cond_3

    move-object p3, p2

    .line 914
    :cond_3
    if-eqz p3, :cond_4

    .line 915
    :try_start_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p3, v7}, Lcom/mfluent/asp/nts/b;->b(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v2

    :cond_4
    move v7, v6

    .line 928
    :goto_3
    if-eqz v7, :cond_e

    .line 930
    if-eqz v2, :cond_e

    .line 931
    :try_start_2
    const-string v3, "status"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v2

    .line 938
    :goto_4
    sget-object v3, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3, v11}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 939
    const-string v3, "AutoRetry"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "Check Target Status="

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    :cond_5
    const-string v3, "COMPLETED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    move v1, v5

    .line 942
    goto :goto_1

    .line 908
    :cond_6
    const-string v7, "api/pCloud/transfer/save/sessions/"

    goto :goto_2

    .line 918
    :catch_0
    move-exception v7

    move v7, v1

    .line 927
    goto :goto_3

    .line 920
    :catch_1
    move-exception v0

    sget-object v0, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v11, :cond_7

    .line 921
    const-string v0, "AutoRetry"

    const-string v1, "checkSessionCanceledByRemote can\'t connect to target dev"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move v1, v4

    .line 923
    goto :goto_1

    .line 924
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 933
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 944
    :cond_8
    if-eqz p2, :cond_9

    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v3

    if-nez v3, :cond_9

    if-eqz v0, :cond_a

    .line 945
    :cond_9
    if-eqz v7, :cond_0

    const-string v0, "STOPPED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v6

    .line 948
    goto/16 :goto_1

    .line 954
    :cond_a
    const-string v0, "api/pCloud/transfer/upload/sessions/"

    .line 956
    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/mfluent/asp/nts/b;->b(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lcom/mfluent/asp/nts/NonOkHttpResponseException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_5

    move-result-object v0

    .line 970
    :try_start_4
    const-string v2, "status"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_6

    move-result-object v0

    .line 975
    sget-object v2, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2, v11}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 976
    const-string v2, "AutoRetry"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Check Source Status="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    :cond_b
    const-string v2, "COMPLETED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    move v1, v5

    .line 979
    goto/16 :goto_1

    .line 959
    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 961
    sget-object v0, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v11, :cond_c

    .line 962
    const-string v0, "AutoRetry"

    const-string v1, "checkSessionCanceledByRemote can\'t connect to source dev"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move v1, v4

    .line 964
    goto/16 :goto_1

    .line 965
    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_1

    .line 971
    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_1

    .line 981
    :cond_d
    const-string v2, "STOPPED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v6

    .line 984
    goto/16 :goto_1

    .line 958
    :catch_7
    move-exception v0

    goto/16 :goto_1

    .line 896
    :catch_8
    move-exception v0

    goto/16 :goto_1

    :cond_e
    move-object v2, v3

    goto/16 :goto_4

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/filetransfer/f;)Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->E:Z

    return v0
.end method

.method private declared-synchronized ak()V
    .locals 0

    .prologue
    .line 276
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public static b(I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1034
    if-ne p0, v0, :cond_0

    .line 1037
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(I)Z
    .locals 1

    .prologue
    .line 1042
    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-eq p0, v0, :cond_0

    const/16 v0, 0xe

    if-ne p0, v0, :cond_1

    .line 1046
    :cond_0
    const/4 v0, 0x1

    .line 1048
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(I)Z
    .locals 1

    .prologue
    .line 1053
    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    .line 1054
    const/4 v0, 0x1

    .line 1056
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(I)Z
    .locals 1

    .prologue
    .line 1061
    const/16 v0, 0xf

    if-ne p0, v0, :cond_0

    .line 1062
    const/4 v0, 0x1

    .line 1064
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract A()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public B()V
    .locals 0

    .prologue
    .line 727
    return-void
.end method

.method public abstract C()Ljava/lang/String;
.end method

.method public final E()V
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    .line 258
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/f;->D()V

    .line 261
    return-void
.end method

.method public final F()V
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->n:Z

    .line 285
    return-void
.end method

.method public final G()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 367
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->m:Z

    if-eq v1, v0, :cond_0

    .line 368
    iput-boolean v1, p0, Lcom/mfluent/asp/filetransfer/f;->m:Z

    .line 369
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->N()V

    .line 371
    :cond_0
    return-void
.end method

.method public final H()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 385
    iput-wide v2, p0, Lcom/mfluent/asp/filetransfer/f;->j:J

    .line 386
    iput-boolean v1, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    .line 387
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/f;->D()V

    .line 388
    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    .line 389
    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->n:Z

    .line 390
    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->p:Z

    .line 391
    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->q:Z

    .line 392
    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->r:Z

    .line 393
    iput-wide v2, p0, Lcom/mfluent/asp/filetransfer/f;->u:J

    .line 395
    iput-boolean v1, p0, Lcom/mfluent/asp/filetransfer/f;->G:Z

    .line 396
    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->H:Z

    .line 397
    return-void
.end method

.method public final I()Z
    .locals 1

    .prologue
    .line 401
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->J:Z

    return v0
.end method

.method public final J()Z
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->E:Z

    return v0
.end method

.method public final K()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x4

    const/4 v0, 0x0

    .line 411
    sget v2, Lcom/mfluent/asp/ASPApplication;->b:I

    if-gtz v2, :cond_1

    .line 445
    :cond_0
    :goto_0
    return v0

    .line 414
    :cond_1
    iget-boolean v2, p0, Lcom/mfluent/asp/filetransfer/f;->J:Z

    if-nez v2, :cond_2

    .line 415
    sget-object v1, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416
    const-string v1, "AutoRetry"

    const-string v2, "autoretry only for transfer controller"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 420
    :cond_2
    sget v2, Lcom/mfluent/asp/ASPApplication;->b:I

    if-gt v2, v1, :cond_3

    .line 421
    iget v2, p0, Lcom/mfluent/asp/filetransfer/f;->I:I

    if-eq v2, v1, :cond_3

    .line 422
    sget-object v1, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 423
    const-string v1, "AutoRetry"

    const-string v2, "only for download autoretry!"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 428
    :cond_3
    iget v2, p0, Lcom/mfluent/asp/filetransfer/f;->D:I

    iget v3, p0, Lcom/mfluent/asp/filetransfer/f;->A:I

    if-lt v2, v3, :cond_5

    .line 429
    sget-object v1, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 430
    const-string v1, "AutoRetry"

    const-string v2, "remote device is offline!"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :cond_4
    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->D:I

    goto :goto_0

    .line 435
    :cond_5
    iget-wide v2, p0, Lcom/mfluent/asp/filetransfer/f;->i:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    iget-wide v2, p0, Lcom/mfluent/asp/filetransfer/f;->j:J

    iget-wide v4, p0, Lcom/mfluent/asp/filetransfer/f;->i:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_6

    .line 436
    const-string v1, "AutoRetry"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "filetransfer actually completed bytesTransferred="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/mfluent/asp/filetransfer/f;->j:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 439
    :cond_6
    iget-boolean v2, p0, Lcom/mfluent/asp/filetransfer/f;->G:Z

    if-eqz v2, :cond_7

    .line 440
    sget-object v1, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 441
    const-string v1, "AutoRetry"

    const-string v2, "bIsCriticalError true"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    move v0, v1

    .line 445
    goto/16 :goto_0
.end method

.method public final L()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 451
    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->C:I

    .line 452
    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->D:I

    .line 453
    return-void
.end method

.method public final declared-synchronized M()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 457
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->E:Z

    if-nez v0, :cond_3

    .line 459
    const-wide/16 v0, 0x1388

    .line 460
    iget v2, p0, Lcom/mfluent/asp/filetransfer/f;->C:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/mfluent/asp/filetransfer/f;->C:I

    .line 461
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/mfluent/asp/filetransfer/f;->E:Z

    .line 462
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    .line 463
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    .line 464
    iget v2, p0, Lcom/mfluent/asp/filetransfer/f;->C:I

    iget v3, p0, Lcom/mfluent/asp/filetransfer/f;->B:I

    if-le v2, v3, :cond_1

    .line 465
    sget-object v0, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    const-string v0, "AutoRetry"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "errorCount="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/mfluent/asp/filetransfer/f;->C:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", switch to long retry interval=300000"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->H:Z

    .line 469
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->C:I

    .line 470
    iget v0, p0, Lcom/mfluent/asp/filetransfer/f;->D:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->D:I

    .line 471
    const-wide/32 v0, 0x493e0

    .line 473
    :cond_1
    sget-object v2, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 474
    const-string v2, "AutoRetry"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "trigger AutoRety After "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sec!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :cond_2
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/mfluent/asp/filetransfer/f;->F:Landroid/os/Handler;

    .line 477
    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/f;->F:Landroid/os/Handler;

    if-eqz v2, :cond_3

    .line 478
    iget-object v2, p0, Lcom/mfluent/asp/filetransfer/f;->F:Landroid/os/Handler;

    new-instance v3, Lcom/mfluent/asp/filetransfer/f$1;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/filetransfer/f$1;-><init>(Lcom/mfluent/asp/filetransfer/f;)V

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 497
    :cond_3
    monitor-exit p0

    return v6

    .line 457
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final N()V
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->g:Lcom/mfluent/asp/filetransfer/g;

    invoke-interface {v0, p0}, Lcom/mfluent/asp/filetransfer/g;->a(Lcom/mfluent/asp/filetransfer/f;)V

    .line 617
    return-void
.end method

.method public final O()Landroid/content/Context;
    .locals 1

    .prologue
    .line 643
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->f:Landroid/content/Context;

    return-object v0
.end method

.method public final P()V
    .locals 1

    .prologue
    .line 658
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->o:Z

    .line 659
    return-void
.end method

.method public final Q()Z
    .locals 1

    .prologue
    .line 665
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->p:Z

    return v0
.end method

.method public final R()V
    .locals 1

    .prologue
    .line 673
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->p:Z

    .line 674
    return-void
.end method

.method public final S()Z
    .locals 1

    .prologue
    .line 680
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->r:Z

    return v0
.end method

.method public final T()Z
    .locals 1

    .prologue
    .line 687
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->q:Z

    return v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 710
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    return v0
.end method

.method public final V()V
    .locals 1

    .prologue
    .line 718
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    .line 719
    return-void
.end method

.method public final W()Z
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->h:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->homesyncSecureTransfer:Z

    return v0
.end method

.method public final X()Z
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->h:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->homesyncPersonalTransfer:Z

    return v0
.end method

.method public final Y()Z
    .locals 1

    .prologue
    .line 751
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->v:Z

    return v0
.end method

.method public final Z()V
    .locals 1

    .prologue
    .line 755
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->v:Z

    .line 756
    return-void
.end method

.method protected final a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;I)I
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v0, 0x0

    .line 990
    .line 992
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    if-nez p2, :cond_0

    if-eq p3, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    if-nez v1, :cond_3

    .line 993
    :cond_1
    sget-object v1, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_2

    .line 994
    const-string v1, "AutoRetry"

    const-string v2, "checkAutoRetry wrong parameter error"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    :cond_2
    :goto_0
    return v0

    .line 998
    :cond_3
    iget-boolean v1, p0, Lcom/mfluent/asp/filetransfer/f;->b:Z

    if-eqz v1, :cond_4

    .line 999
    if-ne p3, v2, :cond_5

    .line 1000
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, p1, p2, v0}, Lcom/mfluent/asp/filetransfer/f;->a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;)I

    move-result v0

    .line 1005
    :cond_4
    :goto_1
    sget-object v1, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x4

    if-gt v1, v2, :cond_2

    .line 1006
    const-string v1, "AutoRetry"

    const-string v2, "checkAutoRetry ioex duringautoretry=false watcherror=false"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1001
    :cond_5
    const/4 v1, 0x2

    if-ne p3, v1, :cond_4

    .line 1002
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-direct {p0, p1, v0, v1}, Lcom/mfluent/asp/filetransfer/f;->a(Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;)I

    move-result v0

    goto :goto_1
.end method

.method protected final a(Ljava/io/File;Landroid/net/Uri;)J
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 293
    const-wide/16 v6, -0x1

    .line 294
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v8

    .line 295
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "_data = ?"

    new-array v4, v1, [Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 296
    if-eqz v3, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 297
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 298
    aget-object v0, v2, v8

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 299
    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 300
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 302
    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v6

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 723
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->s:Ljava/lang/String;

    return-object v0
.end method

.method protected final a(IIJJ)Ljava/lang/String;
    .locals 9

    .prologue
    .line 306
    const/4 v1, 0x0

    .line 308
    sparse-switch p2, :sswitch_data_0

    .line 323
    :goto_0
    if-nez v1, :cond_0

    .line 324
    const/4 v0, 0x0

    .line 363
    :goto_1
    return-object v0

    .line 310
    :sswitch_0
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 313
    :sswitch_1
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 316
    :sswitch_2
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 319
    :sswitch_3
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 327
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v0, p3, v2

    if-nez v0, :cond_2

    const-wide/16 v2, 0x0

    cmp-long v0, p5, v2

    if-lez v0, :cond_2

    .line 328
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "dup_id"

    aput-object v4, v2, v3

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 334
    if-eqz v0, :cond_2

    .line 335
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 336
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p3

    .line 339
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 343
    :cond_2
    const-wide/16 v2, 0x0

    cmp-long v0, p3, v2

    if-nez v0, :cond_3

    .line 344
    const/4 v0, 0x0

    goto :goto_1

    .line 347
    :cond_3
    const/4 v6, 0x0

    .line 349
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "source_media_id"

    aput-object v4, v2, v3

    const-string v3, "device_id=? AND dup_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 355
    if-eqz v1, :cond_5

    .line 356
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 357
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 360
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :cond_4
    move-object v0, v6

    goto :goto_2

    :cond_5
    move-object v0, v6

    goto/16 :goto_1

    .line 308
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0xf -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1025
    const-string v0, "mfl_FileTransferTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::setSlinkPlatformClientAppId() : clientAppId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1026
    iput p1, p0, Lcom/mfluent/asp/filetransfer/f;->c:I

    .line 1027
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 227
    iput-wide p1, p0, Lcom/mfluent/asp/filetransfer/f;->i:J

    .line 228
    return-void
.end method

.method public abstract a(Landroid/database/Cursor;Z)V
.end method

.method public abstract a(Lcom/mfluent/asp/common/datamodel/ASPFile;Lcom/mfluent/asp/datamodel/Device;)V
.end method

.method protected a(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 204
    return-void
.end method

.method public final a(Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;)V
    .locals 1

    .prologue
    .line 872
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    if-ne p1, v0, :cond_0

    .line 873
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    .line 875
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/concurrent/Future;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 763
    iput-object p1, p0, Lcom/mfluent/asp/filetransfer/f;->y:Ljava/util/concurrent/Future;

    .line 764
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 620
    iput-boolean v1, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    .line 621
    iput-boolean v1, p0, Lcom/mfluent/asp/filetransfer/f;->n:Z

    .line 622
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    .line 623
    iput-boolean p1, p0, Lcom/mfluent/asp/filetransfer/f;->x:Z

    .line 624
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->g:Lcom/mfluent/asp/filetransfer/g;

    invoke-interface {v0, p0}, Lcom/mfluent/asp/filetransfer/g;->a(Lcom/mfluent/asp/filetransfer/f;)V

    .line 625
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->y:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->y:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 628
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/f;->ak()V

    .line 637
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 638
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->N()V

    .line 640
    :cond_1
    return-void
.end method

.method public final a(ZZZ)V
    .locals 1

    .prologue
    .line 837
    iput-boolean p1, p0, Lcom/mfluent/asp/filetransfer/f;->J:Z

    .line 838
    if-eqz p2, :cond_0

    .line 839
    const/4 v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->I:I

    .line 847
    :goto_0
    return-void

    .line 841
    :cond_0
    if-eqz p3, :cond_1

    .line 842
    const/4 v0, 0x3

    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->I:I

    goto :goto_0

    .line 844
    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->I:I

    goto :goto_0
.end method

.method public final aa()Z
    .locals 1

    .prologue
    .line 809
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->w:Z

    return v0
.end method

.method public final ab()V
    .locals 1

    .prologue
    .line 813
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->w:Z

    .line 814
    return-void
.end method

.method public final ac()Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    .locals 1

    .prologue
    .line 817
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->h:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    return-object v0
.end method

.method public final ad()Z
    .locals 1

    .prologue
    .line 821
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->h:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->hideTransferStatusWhenSkipped:Z

    if-eqz v0, :cond_0

    .line 823
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->m:Z

    if-nez v0, :cond_0

    .line 824
    const/4 v0, 0x1

    .line 827
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ae()Z
    .locals 1

    .prologue
    .line 832
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->x:Z

    return v0
.end method

.method public final af()I
    .locals 1

    .prologue
    .line 1012
    iget v0, p0, Lcom/mfluent/asp/filetransfer/f;->z:I

    return v0
.end method

.method public final ag()I
    .locals 1

    .prologue
    .line 1016
    iget v0, p0, Lcom/mfluent/asp/filetransfer/f;->z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/filetransfer/f;->z:I

    .line 1017
    iget v0, p0, Lcom/mfluent/asp/filetransfer/f;->z:I

    return v0
.end method

.method public final ah()I
    .locals 1

    .prologue
    .line 1030
    iget v0, p0, Lcom/mfluent/asp/filetransfer/f;->c:I

    return v0
.end method

.method final ai()V
    .locals 1

    .prologue
    .line 1070
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->K:Z

    .line 1071
    return-void
.end method

.method final aj()Z
    .locals 1

    .prologue
    .line 1074
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->K:Z

    return v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 238
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->t:J

    return-wide v0
.end method

.method public bytesTransferred(J)V
    .locals 5

    .prologue
    .line 602
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->c()I

    move-result v0

    .line 604
    iget-wide v2, p0, Lcom/mfluent/asp/filetransfer/f;->j:J

    add-long/2addr v2, p1

    iput-wide v2, p0, Lcom/mfluent/asp/filetransfer/f;->j:J

    .line 605
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/f;->g:Lcom/mfluent/asp/filetransfer/g;

    invoke-interface {v1, p1, p2}, Lcom/mfluent/asp/filetransfer/g;->bytesTransferred(J)V

    .line 606
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->c()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 607
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->N()V

    .line 609
    :cond_0
    return-void
.end method

.method public final c()I
    .locals 4

    .prologue
    .line 213
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 214
    const/4 v0, 0x0

    .line 218
    :goto_0
    return v0

    .line 215
    :cond_0
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->j:J

    iget-wide v2, p0, Lcom/mfluent/asp/filetransfer/f;->i:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 216
    const/16 v0, 0x64

    goto :goto_0

    .line 218
    :cond_1
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->j:J

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/mfluent/asp/filetransfer/f;->i:J

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 280
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->n:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    return v0
.end method

.method public abstract f()I
.end method

.method public abstract g()I
.end method

.method public final h()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->d:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 243
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->u:J

    return-wide v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 223
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->i:J

    return-wide v0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 593
    iget-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->j:J

    return-wide v0
.end method

.method public abstract m()Ljava/lang/String;
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 289
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->m:Z

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    return v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 768
    const/4 v0, 0x0

    return v0
.end method

.method public run()V
    .locals 5

    .prologue
    const/4 v3, 0x6

    const/4 v4, 0x0

    .line 502
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->H()V

    .line 504
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->N()V

    .line 507
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 508
    const-string v0, "mfl_FileTransferTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::run:Starting transfer for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->A()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mfluent/asp/filetransfer/FileUploader$StorageUploadFileTooLargeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/mfluent/asp/exception/TargetOfflineException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 577
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    .line 578
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/f;->ak()V

    .line 579
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->u:J

    .line 580
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->b:Z

    .line 581
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->N()V

    .line 583
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->o:Z

    if-eqz v0, :cond_1

    .line 584
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->t()V

    .line 587
    :cond_1
    :goto_0
    return-void

    .line 512
    :catch_0
    move-exception v0

    .line 513
    :try_start_1
    sget-object v1, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v3, :cond_2

    .line 514
    const-string v1, "mfl_FileTransferTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::run:Transfer interrupted "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 516
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    .line 517
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->n:Z

    .line 518
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 577
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    .line 578
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/f;->ak()V

    .line 579
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->u:J

    .line 580
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->b:Z

    .line 581
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->N()V

    .line 583
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->o:Z

    if-eqz v0, :cond_1

    .line 584
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->t()V

    goto :goto_0

    .line 520
    :catch_1
    move-exception v0

    .line 521
    :try_start_2
    sget-object v1, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v3, :cond_3

    .line 522
    const-string v1, "mfl_FileTransferTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::run:Trouble transferring; file too large "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 524
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    .line 526
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->r:Z

    .line 528
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 529
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->n:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 577
    :cond_4
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    .line 578
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/f;->ak()V

    .line 579
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->u:J

    .line 580
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->b:Z

    .line 581
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->N()V

    .line 583
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->o:Z

    if-eqz v0, :cond_1

    .line 584
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->t()V

    goto/16 :goto_0

    .line 532
    :catch_2
    move-exception v0

    .line 533
    :try_start_3
    sget-object v1, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v3, :cond_5

    .line 534
    const-string v1, "mfl_FileTransferTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::run:Trouble transferring "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 536
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    .line 537
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->q:Z

    .line 539
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 540
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->n:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 577
    :goto_1
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    .line 578
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/f;->ak()V

    .line 579
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->u:J

    .line 580
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->b:Z

    .line 581
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->N()V

    .line 583
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->o:Z

    if-eqz v0, :cond_1

    .line 584
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->t()V

    goto/16 :goto_0

    .line 543
    :cond_6
    const/4 v0, 0x0

    :try_start_4
    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->G:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 577
    :catchall_0
    move-exception v0

    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    .line 578
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/f;->ak()V

    .line 579
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/mfluent/asp/filetransfer/f;->u:J

    .line 580
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->b:Z

    .line 581
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->N()V

    .line 583
    iget-boolean v1, p0, Lcom/mfluent/asp/filetransfer/f;->o:Z

    if-eqz v1, :cond_7

    .line 584
    iget-object v1, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->t()V

    :cond_7
    throw v0

    .line 545
    :catch_3
    move-exception v0

    .line 546
    :try_start_5
    sget-object v1, Lcom/mfluent/asp/filetransfer/f;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v3, :cond_8

    .line 547
    const-string v1, "mfl_FileTransferTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::run:Trouble transferring "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 549
    :cond_8
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    .line 551
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".IOException"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 552
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ConnectException"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 554
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 558
    if-eqz v1, :cond_9

    const-string v2, "Connection timed out"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 559
    :cond_9
    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 561
    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 562
    if-eqz v1, :cond_b

    const-string v2, "Connection timed out"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "Unable to resolve host"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 563
    :cond_a
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mfluent/asp/filetransfer/f;->G:Z

    .line 568
    :cond_b
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ENOSPC"

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/StringUtils;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 570
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->p:Z

    .line 573
    :cond_c
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 574
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->n:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 577
    :cond_d
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    .line 578
    invoke-direct {p0}, Lcom/mfluent/asp/filetransfer/f;->ak()V

    .line 579
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/filetransfer/f;->u:J

    .line 580
    iput-boolean v4, p0, Lcom/mfluent/asp/filetransfer/f;->b:Z

    .line 581
    invoke-virtual {p0}, Lcom/mfluent/asp/filetransfer/f;->N()V

    .line 583
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->o:Z

    if-eqz v0, :cond_1

    .line 584
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->t()V

    goto/16 :goto_0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 773
    const/4 v0, 0x0

    return-object v0
.end method

.method public t()J
    .locals 2

    .prologue
    .line 778
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public u()J
    .locals 2

    .prologue
    .line 783
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final v()Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;
    .locals 1

    .prologue
    .line 788
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->m:Z

    if-nez v0, :cond_0

    .line 789
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->a:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    .line 800
    :goto_0
    return-object v0

    .line 792
    :cond_0
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->l:Z

    if-eqz v0, :cond_1

    .line 793
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->b:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    goto :goto_0

    .line 796
    :cond_1
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->k:Z

    if-eqz v0, :cond_2

    .line 797
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->c:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    goto :goto_0

    .line 800
    :cond_2
    sget-object v0, Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;->d:Lcom/mfluent/asp/filetransfer/FileTransferSession$Status;

    goto :goto_0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 805
    const-string v0, "FILE"

    return-object v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 852
    iget-boolean v0, p0, Lcom/mfluent/asp/filetransfer/f;->H:Z

    return v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 866
    iget-object v0, p0, Lcom/mfluent/asp/filetransfer/f;->h:Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->isUiAppTheme:Z

    return v0
.end method

.method public z()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
