.class final Lcom/mfluent/asp/sync/g$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/sync/g$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/sync/g;->m()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/g;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/sync/g;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/mfluent/asp/sync/g$5;->a:Lcom/mfluent/asp/sync/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$5;->a:Lcom/mfluent/asp/sync/g;

    invoke-static {p1, p2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Landroid/content/ContentValues;Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 327
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$5;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "_data"

    invoke-static {p2, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 328
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$5;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "_display_name"

    invoke-static {p2, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 329
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 333
    invoke-static {v1}, Lorg/apache/commons/io/FilenameUtils;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 336
    :cond_0
    const-string v2, "date_added"

    iget-object v3, p0, Lcom/mfluent/asp/sync/g$5;->a:Lcom/mfluent/asp/sync/g;

    const-string v3, "date_added"

    invoke-static {p2, v3}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 337
    const-string v2, "date_modified"

    iget-object v3, p0, Lcom/mfluent/asp/sync/g$5;->a:Lcom/mfluent/asp/sync/g;

    const-string v3, "date_modified"

    invoke-static {p2, v3}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 338
    const-string v2, "_display_name"

    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v0, "mime_type"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$5;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "mime_type"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const-string v0, "_size"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$5;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "_size"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 341
    const-string v0, "title"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$5;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "title"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    const-string v0, "full_uri"

    invoke-static {v1}, Lcom/mfluent/asp/dws/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const-string v0, "_data"

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const/4 v0, 0x0

    .line 345
    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 346
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 347
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 348
    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 351
    :cond_1
    const-string v1, "bucket_display_name"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    return-void
.end method
