.class final Lcom/mfluent/asp/sync/g$4;
.super Landroid/database/ContentObserver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/sync/g;->c(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/g;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/sync/g;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/mfluent/asp/sync/g$4;->a:Lcom/mfluent/asp/sync/g;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public final onChange(Z)V
    .locals 2

    .prologue
    .line 212
    invoke-static {}, Lcom/mfluent/asp/sync/g;->c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    const-string v0, "mfl_LocalDeviceMetadataSyncManager"

    const-string v1, "Received change notification for Audio."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$4;->a:Lcom/mfluent/asp/sync/g;

    invoke-static {v0}, Lcom/mfluent/asp/sync/g;->a(Lcom/mfluent/asp/sync/g;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$4;->a:Lcom/mfluent/asp/sync/g;

    invoke-static {}, Lcom/mfluent/asp/sync/g;->i()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/sync/g;->e(Landroid/content/Intent;)V

    .line 219
    :cond_1
    return-void
.end method
