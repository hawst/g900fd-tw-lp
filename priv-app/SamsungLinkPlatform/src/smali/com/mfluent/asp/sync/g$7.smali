.class final Lcom/mfluent/asp/sync/g$7;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/sync/g$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/sync/g;->o()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/g;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/sync/g;)V
    .locals 0

    .prologue
    .line 526
    iput-object p1, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 635
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    invoke-static {p1, p2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    .line 637
    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "longitude"

    const-string v3, "longitude"

    invoke-static {p1, v2, p2, v3}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v2

    and-int/2addr v0, v2

    .line 643
    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "latitude"

    const-string v3, "latitude"

    invoke-static {p1, v2, p2, v3}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v2

    and-int/2addr v0, v2

    .line 644
    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "thumb_data"

    invoke-static {p1, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 646
    const-string v0, "thumb_width"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v6

    .line 647
    const-string v0, "thumb_height"

    invoke-static {p1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v7

    .line 648
    const/4 v0, 0x0

    .line 651
    if-eqz p3, :cond_8

    .line 652
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "_data"

    invoke-static {p3, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 653
    const-string v0, "width"

    invoke-static {p3, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v3

    .line 654
    const-string v0, "height"

    invoke-static {p3, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    .line 656
    :goto_0
    invoke-static {v2, v5}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    if-ne v3, v6, :cond_5

    if-ne v0, v7, :cond_5

    move v0, v4

    .line 660
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    const-string v2, "isPlayed"

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_1

    .line 661
    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "isPlayed"

    const-string v3, "isPlayed"

    invoke-static {p1, v2, p2, v3}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v2

    and-int/2addr v0, v2

    .line 663
    :cond_1
    if-eqz v0, :cond_4

    .line 664
    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "_data"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 665
    iget-object v3, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    invoke-static {v2}, Lcom/mfluent/asp/sync/g;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 666
    new-array v5, v8, [Ljava/lang/String;

    .line 668
    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "caption_type"

    invoke-static {p1, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 669
    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "caption_uri"

    invoke-static {p1, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v4

    .line 670
    const/4 v2, 0x2

    iget-object v4, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v4, "caption_index_uri"

    invoke-static {p1, v4}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v2

    move v2, v1

    .line 672
    :goto_2
    if-ge v2, v8, :cond_4

    .line 673
    aget-object v4, v5, v2

    if-nez v4, :cond_2

    .line 674
    const-string v4, ""

    aput-object v4, v5, v2

    .line 676
    :cond_2
    aget-object v4, v3, v2

    if-nez v4, :cond_3

    .line 677
    const-string v4, ""

    aput-object v4, v3, v2

    .line 679
    :cond_3
    aget-object v4, v5, v2

    aget-object v6, v3, v2

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    move v0, v1

    .line 687
    :cond_4
    if-eqz v0, :cond_7

    :goto_3
    return v1

    :cond_5
    move v0, v1

    .line 656
    goto :goto_1

    .line 672
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 687
    :cond_7
    const/4 v1, -0x1

    goto :goto_3

    :cond_8
    move-object v2, v0

    move v3, v1

    move v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/content/ContentValues;Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 530
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "resolution"

    invoke-static {p2, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 531
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "_data"

    invoke-static {p2, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 533
    const-string v0, "album"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "album"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    const-string v0, "artist"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "artist"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    const-string v0, "bookmark"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "bookmark"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 536
    const-string v0, "bucket_display_name"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "bucket_display_name"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    const-string v0, "bucket_id"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "bucket_id"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const-string v0, "category"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "category"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    const-string v0, "datetaken"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "datetaken"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 540
    const-string v0, "description"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "description"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    const-string v0, "duration"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "duration"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 542
    const-string v0, "isprivate"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "isprivate"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 543
    const-string v0, "language"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "language"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    const-string v0, "latitude"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "latitude"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 545
    const-string v0, "longitude"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "longitude"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 546
    const-string v0, "resolution"

    invoke-virtual {p1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    const-string v0, "tags"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "tags"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    const-string v0, "date_added"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "date_added"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 549
    const-string v0, "date_modified"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "date_modified"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 550
    const-string v0, "_display_name"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "_display_name"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const-string v0, "mime_type"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "mime_type"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    const-string v0, "_size"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "_size"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 553
    const-string v0, "title"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "title"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    const-string v0, "full_uri"

    invoke-static {v5}, Lcom/mfluent/asp/dws/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    const-string v0, "_data"

    invoke-virtual {p1, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    if-eqz p3, :cond_9

    .line 561
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "_data"

    invoke-static {p3, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 562
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "width"

    invoke-static {p3, v0}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 563
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "height"

    invoke-static {p3, v0}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 565
    :goto_0
    const-string v6, "thumb_data"

    invoke-virtual {p1, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    const-string v6, "thumb_width"

    invoke-virtual {p1, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 567
    const-string v2, "thumb_height"

    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 568
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 569
    const-string v0, "thumbnail_uri"

    invoke-static {v3}, Lcom/mfluent/asp/dws/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    :goto_1
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 587
    new-instance v3, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v3}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 588
    invoke-virtual {v3, v5}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 589
    const/16 v0, 0x18

    invoke-virtual {v3, v0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v6

    .line 590
    const/16 v0, 0x12

    invoke-virtual {v3, v0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 591
    const/16 v2, 0x13

    invoke-virtual {v3, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v7

    .line 592
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {v7}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 593
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 594
    :try_start_1
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    move-object v0, v2

    .line 596
    :goto_2
    :try_start_2
    invoke-static {v6}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 597
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 599
    const-string v6, "orientation"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 601
    :cond_0
    invoke-virtual {v3}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .line 611
    :cond_1
    :goto_3
    invoke-static {v4}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_4

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_4

    .line 612
    :cond_3
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 614
    const/16 v3, 0x78

    invoke-static {v2, v3}, Lorg/apache/commons/lang3/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v2

    .line 615
    array-length v3, v2

    if-ne v3, v10, :cond_4

    .line 616
    aget-object v0, v2, v8

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 617
    aget-object v1, v2, v9

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 621
    :cond_4
    const-string v2, "width"

    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 622
    const-string v0, "height"

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 624
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    invoke-static {v5}, Lcom/mfluent/asp/sync/g;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 625
    const-string v1, "caption_type"

    aget-object v2, v0, v8

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    const-string v1, "caption_uri"

    aget-object v2, v0, v9

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    const-string v1, "caption_index_uri"

    aget-object v0, v0, v10

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    const-string v0, "isPlayed"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_5

    .line 629
    const-string v0, "isPlayed"

    iget-object v1, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v1, "isPlayed"

    invoke-static {p2, v1}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 631
    :cond_5
    return-void

    .line 571
    :cond_6
    const-string v0, "thumbnail_uri"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "v1/video/thumbnail/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/sync/g$7;->a:Lcom/mfluent/asp/sync/g;

    const-string v3, "_id"

    invoke-static {p2, v3}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 603
    :catch_0
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    .line 604
    :goto_4
    invoke-static {}, Lcom/mfluent/asp/sync/g;->c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v6, 0x6

    if-gt v3, v6, :cond_1

    .line 605
    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    const-string v6, "syncVideos:: Exception occurred when using MediaMetadataRetriever"

    invoke-static {v3, v6, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 606
    const-string v2, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "syncVideos:: Data File path "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 608
    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "syncVideos:: Data File path exist : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 603
    :catch_1
    move-exception v0

    move-object v11, v0

    move-object v0, v2

    move-object v2, v11

    goto :goto_4

    :catch_2
    move-exception v2

    goto :goto_4

    :cond_7
    move-object v0, v1

    goto/16 :goto_2

    :cond_8
    move-object v0, v1

    goto/16 :goto_3

    :cond_9
    move-object v0, v1

    move-object v2, v1

    move-object v3, v1

    goto/16 :goto_0
.end method
