.class public abstract Lcom/mfluent/asp/sync/BatchMediaProcessingService;
.super Landroid/app/Service;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;,
        Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;
    }
.end annotation


# instance fields
.field private a:Z

.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/Thread;

.field private final d:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/locks/ReentrantLock;

.field private final g:Ljava/util/concurrent/locks/Condition;

.field private h:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a:Z

    .line 71
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->d:Ljava/util/LinkedHashSet;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->e:Ljava/util/ArrayList;

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->h:I

    .line 78
    iput-object p1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->b:Ljava/lang/String;

    .line 79
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    .line 80
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->g:Ljava/util/concurrent/locks/Condition;

    .line 81
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method static synthetic b(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a:Z

    return v0
.end method

.method static synthetic c(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Z
    .locals 10

    .prologue
    const-wide/16 v4, -0x1

    const/4 v6, -0x1

    .line 32
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v2, v4

    move v1, v6

    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v8, 0xfa

    if-ge v0, v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;

    if-ne v1, v6, :cond_1

    iget v1, v0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->c:I

    :cond_0
    cmp-long v8, v2, v4

    if-nez v8, :cond_3

    iget-wide v2, v0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->a:J

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    iget-object v8, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->e:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget v8, v0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->c:I

    if-eq v1, v8, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_3
    iget-wide v8, v0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->a:J

    cmp-long v8, v2, v8

    if-nez v8, :cond_2

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method static synthetic d(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/concurrent/locks/Condition;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->g:Ljava/util/concurrent/locks/Condition;

    return-object v0
.end method

.method static synthetic g(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->h:I

    return v0
.end method


# virtual methods
.method protected a(Landroid/content/ContentResolver;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 305
    return-void
.end method

.method protected a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154
    return-void
.end method

.method protected a(Ljava/util/ArrayList;Landroid/database/Cursor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 295
    return-void
.end method

.method protected final a(Ljava/util/ArrayList;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;",
            ">;",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 207
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 291
    :cond_0
    return-void

    .line 211
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;

    iget-wide v2, v2, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    .line 213
    new-instance v11, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v11, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 214
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 216
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 218
    const/4 v6, 0x0

    .line 219
    const/4 v8, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v8, v3, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 220
    const/4 v9, 0x0

    .line 222
    new-instance v13, Ljava/lang/StringBuilder;

    const/16 v3, 0x2ee

    invoke-direct {v13, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 223
    invoke-static/range {p4 .. p4}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 224
    const/16 v3, 0x28

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") AND ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int/2addr v3, v8

    const/16 v4, 0xfa

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 227
    if-eqz p5, :cond_3

    .line 228
    move-object/from16 v0, p5

    array-length v4, v0

    add-int/2addr v3, v4

    .line 230
    :cond_3
    invoke-static {v10}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 231
    const-string v4, "device_id=? AND "

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    add-int/lit8 v3, v3, 0x1

    .line 234
    :cond_4
    if-eqz v6, :cond_5

    array-length v4, v6

    if-eq v4, v3, :cond_6

    .line 235
    :cond_5
    new-array v6, v3, [Ljava/lang/String;

    .line 237
    :cond_6
    const/4 v3, 0x0

    .line 238
    if-eqz p5, :cond_7

    move-object/from16 v0, p5

    array-length v4, v0

    if-lez v4, :cond_7

    .line 239
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p5

    array-length v5, v0

    move-object/from16 v0, p5

    invoke-static {v0, v3, v6, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 240
    move-object/from16 v0, p5

    array-length v3, v0

    add-int/lit8 v3, v3, 0x0

    .line 242
    :cond_7
    invoke-static {v10}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 243
    add-int/lit8 v4, v3, 0x1

    aput-object v10, v6, v3

    move v3, v4

    .line 246
    :cond_8
    const-string v4, "source_media_id IN ("

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    const/4 v4, 0x1

    move v7, v3

    move v3, v4

    move v4, v8

    .line 248
    :goto_1
    array-length v5, v6

    if-ge v7, v5, :cond_a

    .line 249
    if-nez v3, :cond_9

    .line 250
    const/16 v5, 0x2c

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v5, v3

    .line 254
    :goto_2
    const/16 v3, 0x3f

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 255
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;

    iget-object v3, v3, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->b:Ljava/lang/String;

    aput-object v3, v6, v7

    .line 248
    add-int/lit8 v7, v7, 0x1

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v5

    goto :goto_1

    .line 252
    :cond_9
    const/4 v5, 0x0

    goto :goto_2

    .line 257
    :cond_a
    const/16 v3, 0x29

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 258
    invoke-static/range {p4 .. p4}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 259
    const/16 v3, 0x29

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 262
    :cond_b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v7, p6

    .line 263
    :try_start_0
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 267
    if-eqz v3, :cond_d

    .line 268
    :goto_3
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a()Z

    move-result v4

    if-nez v4, :cond_d

    .line 269
    invoke-virtual {p0, v12, v11, v3}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Ljava/util/HashMap;Ljava/util/ArrayList;Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 274
    :catchall_0
    move-exception v2

    :goto_4
    if-eqz v3, :cond_c

    .line 275
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v2

    .line 274
    :cond_d
    if-eqz v3, :cond_e

    .line 275
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 279
    :cond_e
    invoke-virtual {p0, v2}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Landroid/content/ContentResolver;)V

    .line 281
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_f

    .line 282
    const-string v3, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    invoke-virtual {v2, v3, v11}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 283
    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 286
    :cond_f
    invoke-virtual {v12}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_10

    .line 287
    invoke-virtual {p0, v12, v2}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Ljava/util/HashMap;Landroid/content/ContentResolver;)V

    .line 288
    invoke-virtual {v12}, Ljava/util/HashMap;->clear()V

    .line 219
    :cond_10
    add-int/lit16 v8, v8, 0xfa

    move-object/from16 p4, v5

    goto/16 :goto_0

    .line 274
    :catchall_1
    move-exception v2

    move-object v3, v9

    goto :goto_4
.end method

.method protected a(Ljava/util/HashMap;Landroid/content/ContentResolver;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/location/Address;",
            ">;",
            "Landroid/content/ContentResolver;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 310
    return-void
.end method

.method protected a(Ljava/util/HashMap;Ljava/util/ArrayList;Landroid/database/Cursor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/location/Address;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 298
    invoke-virtual {p0, p2, p3}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Ljava/util/ArrayList;Landroid/database/Cursor;)V

    .line 300
    return-void
.end method

.method protected final a()Z
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 198
    :try_start_0
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    iget-object v1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 87
    new-instance v0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;

    iget-object v1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;-><init>(Lcom/mfluent/asp/sync/BatchMediaProcessingService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->c:Ljava/lang/Thread;

    .line 120
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->c:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 121
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 187
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a:Z

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->c:Ljava/lang/Thread;

    .line 189
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->g:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 192
    return-void

    .line 191
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 159
    if-eqz p1, :cond_2

    .line 160
    const-string v1, "mfl_DEVICE_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 161
    const-string v1, "mfl_MEDIA_TYPE"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 162
    const-string v4, "mfl_SOURCE_MEDIA_ID_ARRAY"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 163
    if-eqz v4, :cond_1

    array-length v5, v4

    if-lez v5, :cond_1

    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-lez v5, :cond_1

    .line 164
    iget-object v5, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 166
    :goto_0
    :try_start_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    .line 167
    new-instance v5, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;

    aget-object v6, v4, v0

    invoke-direct {v5, v2, v3, v6, v1}, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;-><init>(JLjava/lang/String;I)V

    .line 168
    iget-object v6, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v6, v5}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->g:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 176
    :cond_1
    iput p3, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->h:I

    .line 179
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0

    .line 172
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
