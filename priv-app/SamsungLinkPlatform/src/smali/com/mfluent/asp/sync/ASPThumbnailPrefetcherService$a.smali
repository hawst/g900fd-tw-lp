.class final Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/util/bitmap/ImageWorker$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/Object;

.field private final b:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;->b:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    .line 50
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 58
    invoke-static {}, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->a()Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 60
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->b()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;->b:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 61
    invoke-static {}, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->b()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;->b:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->a()Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 65
    return-void

    .line 64
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->a()Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;->c()V

    .line 86
    return-void
.end method

.method public final a(Lcom/mfluent/asp/util/bitmap/a;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;->c()V

    .line 55
    return-void
.end method

.method public final b()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTag(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final setTag(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 75
    iput-object p2, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;->a:Ljava/lang/Object;

    .line 76
    return-void
.end method
