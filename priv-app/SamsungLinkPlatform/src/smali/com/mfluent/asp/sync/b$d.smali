.class abstract Lcom/mfluent/asp/sync/b$d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/sync/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "d"
.end annotation


# instance fields
.field protected final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field protected final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/b$d;->a:Ljava/util/ArrayList;

    .line 628
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/b$d;->b:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 614
    invoke-direct {p0}, Lcom/mfluent/asp/sync/b$d;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(I)Landroid/net/Uri;
.end method

.method public a()V
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 632
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$d;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 633
    return-void
.end method

.method public a(Landroid/content/ContentResolver;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 644
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$d;->a:Ljava/util/ArrayList;

    invoke-static {p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 647
    :try_start_0
    const-string v0, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    iget-object v1, p0, Lcom/mfluent/asp/sync/b$d;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 649
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 650
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$d;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 651
    return-void

    .line 649
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/b$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 650
    iget-object v1, p0, Lcom/mfluent/asp/sync/b$d;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    throw v0
.end method

.method public a(Landroid/content/ContentResolver;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 641
    return-void
.end method

.method public a(Landroid/content/ContentResolver;ILjava/lang/String;Lorg/json/JSONObject;Landroid/content/ContentValues;)V
    .locals 0

    .prologue
    .line 637
    return-void
.end method

.method public abstract b()Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;
.end method

.method public abstract c()Landroid/net/Uri;
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract e()[Lcom/mfluent/asp/dws/a$g;
.end method
