.class final Lcom/mfluent/asp/sync/g$8;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/sync/g$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/sync/g;->p()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/g;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/sync/g;)V
    .locals 0

    .prologue
    .line 752
    iput-object p1, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 808
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    invoke-static {v0}, Lcom/mfluent/asp/sync/g;->b(Lcom/mfluent/asp/sync/g;)Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "album_art"

    aput-object v3, v2, v6

    const-string v3, "_id=?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 815
    if-nez v1, :cond_0

    .line 816
    const-string v0, ""

    .line 825
    :goto_0
    return-object v0

    .line 820
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 821
    const-string v0, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 825
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 823
    :cond_1
    :try_start_1
    const-string v0, "album_art"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 825
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;)I
    .locals 2

    .prologue
    .line 831
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    invoke-static {p1, p2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    .line 833
    if-eqz v0, :cond_0

    const-string v1, "recently_played"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_0

    .line 834
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "recently_played"

    const-string v1, "recently_played"

    invoke-static {p1, v0, p2, v1}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    .line 836
    :cond_0
    if-eqz v0, :cond_1

    const-string v1, "most_played"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_1

    .line 837
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "most_played"

    const-string v1, "most_played"

    invoke-static {p1, v0, p2, v1}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    .line 840
    :cond_1
    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Landroid/content/ContentValues;Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 756
    const-string v0, "album"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "album"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    const-string v0, "artist"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "artist"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    const-string v0, "bookmark"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "bookmark"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 759
    const-string v0, "composer"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "composer"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    const-string v0, "duration"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "duration"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 761
    const-string v0, "is_music"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "is_music"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 762
    const-string v0, "is_podcast"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "is_podcast"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 763
    const-string v0, "track"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "track"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    rem-int/lit16 v2, v2, 0x3e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 764
    const-string v0, "year"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "year"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 765
    const-string v0, "date_added"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "date_added"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 766
    const-string v0, "date_modified"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "date_modified"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 767
    const-string v0, "_display_name"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "_display_name"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    const-string v0, "mime_type"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "mime_type"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    const-string v0, "_size"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "_size"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 770
    const-string v0, "title"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "title"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "album_id"

    invoke-static {p2, v0}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 772
    invoke-direct {p0, v0}, Lcom/mfluent/asp/sync/g$8;->a(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v0

    .line 773
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 774
    const-string v2, "thumbnail_uri"

    invoke-static {v0}, Lcom/mfluent/asp/dws/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    const-string v2, "thumb_data"

    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "_data"

    invoke-static {p2, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 781
    const-string v0, "full_uri"

    invoke-static {v2}, Lcom/mfluent/asp/dws/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    const-string v0, "_data"

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    const-string v0, "source_album_id"

    iget-object v3, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v3, "album_id"

    invoke-static {p2, v3}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "album_artist"

    invoke-static {p2, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 785
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 786
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "artist"

    invoke-static {p2, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 788
    :cond_0
    const-string v3, "album_artist"

    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    const-string v0, "recently_played"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 791
    const-string v0, "recently_played"

    iget-object v3, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v3, "recently_played"

    invoke-static {p2, v3}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 793
    :cond_1
    const-string v0, "most_played"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_2

    .line 794
    const-string v0, "most_played"

    iget-object v3, p0, Lcom/mfluent/asp/sync/g$8;->a:Lcom/mfluent/asp/sync/g;

    const-string v3, "most_played"

    invoke-static {p2, v3}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 798
    :cond_2
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 799
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 800
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 801
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 804
    :goto_1
    const-string v1, "bucket_display_name"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    return-void

    .line 777
    :cond_3
    const-string v0, "thumbnail_uri"

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    const-string v0, "thumb_data"

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
