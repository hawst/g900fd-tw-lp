.class public Lcom/mfluent/asp/sync/g;
.super Lcom/mfluent/asp/sync/f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/sync/g$b;,
        Lcom/mfluent/asp/sync/g$c;,
        Lcom/mfluent/asp/sync/g$a;
    }
.end annotation


# static fields
.field public static b:I

.field private static e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final f:Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static final h:Ljava/lang/String;

.field private static final m:Landroid/content/Intent;

.field private static final n:Landroid/content/Intent;

.field private static final o:Landroid/content/Intent;

.field private static final p:Landroid/content/Intent;


# instance fields
.field private final i:Landroid/content/ContentResolver;

.field private final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/database/ContentObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/mfluent/asp/sync/h;

.field private q:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 75
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_SYNC:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 82
    const/4 v0, -0x1

    sput v0, Lcom/mfluent/asp/sync/g;->b:I

    .line 86
    invoke-static {}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getAspDocumentExtensions()Ljava/util/Collection;

    move-result-object v1

    .line 87
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 88
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 89
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "%."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 91
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, Lcom/mfluent/asp/sync/g;->g:[Ljava/lang/String;

    .line 92
    const-string v0, "_data LIKE ?"

    const-string v2, " OR "

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-static {v0, v2, v1}, Lorg/apache/commons/lang3/StringUtils;->repeat(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/g;->f:Ljava/lang/String;

    .line 94
    const/4 v1, 0x0

    .line 97
    :try_start_0
    const-string v0, "com.samsung.android.secretmode.SecretModeManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 98
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/secretmode/SecretModeManager;->getSecretDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->trimToNull(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 105
    :goto_1
    sput-object v0, Lcom/mfluent/asp/sync/g;->h:Ljava/lang/String;

    .line 116
    new-instance v0, Landroid/content/Intent;

    const-string v1, "SYNC_IMAGES"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/sync/g;->m:Landroid/content/Intent;

    .line 117
    new-instance v0, Landroid/content/Intent;

    const-string v1, "SYNC_VIDEOS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/sync/g;->n:Landroid/content/Intent;

    .line 118
    new-instance v0, Landroid/content/Intent;

    const-string v1, "SYNC_AUDIO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/sync/g;->o:Landroid/content/Intent;

    .line 119
    new-instance v0, Landroid/content/Intent;

    const-string v1, "SYNC_DOCUMENTS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/mfluent/asp/sync/g;->p:Landroid/content/Intent;

    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    sget-object v2, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 101
    const-string v2, "mfl_LocalDeviceMetadataSyncManager"

    const-string v3, "SecretModeManager not available"

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/sync/f;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    .line 112
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/g;->k:Ljava/util/Set;

    .line 114
    const-class v0, Lcom/mfluent/asp/sync/h;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/h;

    iput-object v0, p0, Lcom/mfluent/asp/sync/g;->l:Lcom/mfluent/asp/sync/h;

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/sync/g;->q:Landroid/content/Context;

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    .line 125
    iput-object p1, p0, Lcom/mfluent/asp/sync/g;->q:Landroid/content/Context;

    .line 126
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/sync/g;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->q:Landroid/content/Context;

    return-object v0
.end method

.method private static a(ILandroid/database/Cursor;I)Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 1619
    const/4 v1, 0x0

    .line 1621
    if-eqz p1, :cond_4

    .line 1622
    const/4 v2, -0x1

    .line 1623
    invoke-interface {p1}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v3, v2

    move-object v2, v1

    move v1, v3

    .line 1625
    :goto_1
    if-eqz v0, :cond_2

    .line 1626
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1627
    if-ne v1, p0, :cond_2

    move v2, v1

    move-object v0, p1

    .line 1631
    :goto_2
    if-ge v2, p0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1634
    :cond_0
    :goto_3
    return-object v0

    .line 1623
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v2

    move v2, v1

    goto :goto_2

    :cond_3
    move v3, v1

    move v1, v2

    move-object v2, v0

    move v0, v3

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method static synthetic a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/mfluent/asp/sync/g;->h(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1699
    if-eq p1, v0, :cond_0

    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->q:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1701
    const-string v1, "mfl_LocalDeviceMetadataSyncManager"

    const-string v2, "::considerOnlyTaggedMediaFiles true"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1705
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/database/Cursor;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    .line 1723
    const-string v1, "longitude"

    invoke-static {p0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->isNull(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "latitude"

    invoke-static {p0, v1}, Lcom/mfluent/asp/common/util/CursorUtils;->isNull(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1746
    :cond_0
    :goto_0
    return v0

    .line 1729
    :cond_1
    const-string v1, "latitude"

    invoke-static {p0, v1}, Lcom/mfluent/asp/sync/g;->g(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    .line 1730
    const-string v2, "longitude"

    invoke-static {p0, v2}, Lcom/mfluent/asp/sync/g;->g(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    .line 1741
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1742
    const-string v1, "mfl_LocalDeviceMetadataSyncManager"

    const-string v2, "isMapCoordinateValid(): return false - Invalid Map coordinates(0.0, 0.0)"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1746
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 72
    const-string v2, "date_modified"

    const-string v3, "date_modified"

    invoke-static {p0, v2, p1, v3}, Lcom/mfluent/asp/sync/g;->d(Landroid/database/Cursor;Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "_data"

    const-string v3, "_data"

    invoke-static {p0, v2}, Lcom/mfluent/asp/sync/g;->h(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v3}, Lcom/mfluent/asp/sync/g;->h(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Landroid/database/Cursor;Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/mfluent/asp/sync/g;->g(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-static {p2, p3}, Lcom/mfluent/asp/sync/g;->g(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;Lcom/mfluent/asp/sync/g$c;ILcom/mfluent/asp/sync/g$a;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Lcom/mfluent/asp/sync/g$c;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x5

    .line 1028
    :try_start_0
    invoke-direct/range {p0 .. p7}, Lcom/mfluent/asp/sync/g;->b(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;Lcom/mfluent/asp/sync/g$c;ILcom/mfluent/asp/sync/g$a;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Lcom/mfluent/asp/sync/g$c;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    .line 1044
    :goto_0
    return v0

    .line 1029
    :catch_0
    move-exception v0

    throw v0

    .line 1031
    :catch_1
    move-exception v0

    throw v0

    .line 1033
    :catch_2
    move-exception v0

    .line 1034
    sget-object v1, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1035
    const-string v1, "mfl_LocalDeviceMetadataSyncManager"

    const-string v2, "Trouble syncing - trying one more time"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1039
    :cond_0
    :try_start_1
    invoke-direct/range {p0 .. p7}, Lcom/mfluent/asp/sync/g;->b(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;Lcom/mfluent/asp/sync/g$c;ILcom/mfluent/asp/sync/g$a;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Lcom/mfluent/asp/sync/g$c;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result v0

    goto :goto_0

    .line 1040
    :catch_3
    move-exception v0

    .line 1041
    sget-object v1, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1042
    const-string v1, "mfl_LocalDeviceMetadataSyncManager"

    const-string v2, "Trouble syncing - giving up"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1044
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 72
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    if-nez p0, :cond_0

    move-object v0, v4

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x2e

    invoke-virtual {v3, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    if-ltz v5, :cond_4

    invoke-virtual {v3, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v6

    move v0, v1

    :goto_1
    sget-object v3, Lcom/sec/pcw/util/Common;->q:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    new-instance v7, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v8, Lcom/sec/pcw/util/Common;->q:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v7, v6, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/sec/pcw/util/Common;->r:[Ljava/lang/String;

    aget-object v3, v3, v0

    if-eqz v3, :cond_5

    new-instance v3, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v8, Lcom/sec/pcw/util/Common;->r:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v6, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v5, v3

    :goto_2
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_1
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lcom/sec/pcw/util/Common;->s:[Ljava/lang/String;

    aget-object v0, v6, v0

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    :cond_2
    :goto_3
    aput-object v0, v4, v1

    const/4 v0, 0x1

    aput-object v3, v4, v0

    const/4 v0, 0x2

    aput-object v2, v4, v0

    move-object v0, v4

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move-object v0, v2

    move-object v3, v2

    goto :goto_3

    :cond_5
    move-object v5, v2

    goto :goto_2
.end method

.method static synthetic b(Lcom/mfluent/asp/sync/g;)Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/mfluent/asp/sync/g;->f(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Landroid/database/Cursor;Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 72
    invoke-static {p0, p1, p2, p3}, Lcom/mfluent/asp/sync/g;->d(Landroid/database/Cursor;Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;Lcom/mfluent/asp/sync/g$c;ILcom/mfluent/asp/sync/g$a;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Lcom/mfluent/asp/sync/g$c;)Z
    .locals 29
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 1139
    const/4 v15, 0x0

    .line 1140
    const-string v2, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Syncing "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1141
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/g;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v18

    .line 1143
    sget-object v19, Lcom/mfluent/asp/sync/g;->h:Ljava/lang/String;

    .line 1145
    const/4 v11, 0x0

    .line 1146
    const/4 v10, 0x0

    .line 1147
    const/4 v9, 0x0

    .line 1150
    new-instance v2, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/g;->q:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/g;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    int-to-long v4, v4

    const-string v7, "com.mfluent.asp.sync.LocalMediaReverseGeolocationService.REVERSE_LOOKUP"

    const-class v6, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    move/from16 v6, p3

    invoke-direct/range {v2 .. v8}, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;-><init>(Landroid/content/Context;JILjava/lang/String;Ljava/lang/String;)V

    .line 1178
    new-instance v20, Lcom/mfluent/asp/common/util/IntVector;

    const/16 v3, 0x1f4

    const/16 v4, 0x32

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/mfluent/asp/common/util/IntVector;-><init>(IIZZ)V

    .line 1180
    new-instance v21, Lcom/mfluent/asp/util/h;

    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    move-object/from16 v0, v21

    invoke-direct {v0, v3}, Lcom/mfluent/asp/util/h;-><init>(Ljava/lang/String;)V

    .line 1181
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p1 .. p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Local Media Sync"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/util/h;->a(Ljava/lang/String;)Lcom/mfluent/asp/util/h;

    .line 1182
    const/4 v14, 0x0

    .line 1183
    const/4 v13, 0x0

    .line 1184
    const/4 v12, 0x0

    .line 1188
    const/4 v6, 0x0

    .line 1189
    :try_start_0
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/mfluent/asp/sync/g;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1191
    const-string v6, "latitude IS NOT NULL AND longitude IS NOT NULL"

    .line 1194
    :cond_0
    move/from16 v0, v18

    int-to-long v4, v0

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v4

    .line 1195
    const-string v8, "CAST (source_media_id as integer)"

    .line 1198
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    move-result-object v11

    .line 1203
    :try_start_1
    const-string v3, "latitude IS NOT NULL AND longitude IS NOT NULL"

    .line 1204
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/mfluent/asp/sync/g;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1205
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/mfluent/asp/sync/g$c;->c:Ljava/lang/String;

    if-nez v4, :cond_5

    .line 1206
    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/mfluent/asp/sync/g$c;->c:Ljava/lang/String;

    .line 1212
    :cond_1
    :goto_0
    if-nez v11, :cond_9

    .line 1213
    sget-object v2, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x6

    if-gt v2, v3, :cond_2

    .line 1214
    const-string v2, "mfl_LocalDeviceMetadataSyncManager"

    const-string v3, "::syncHelper:Did not expect dbCursor to be NULL"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1216
    :cond_2
    if-eqz v11, :cond_3

    .line 1570
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1571
    :cond_3
    const/4 v3, 0x0

    .line 1615
    :cond_4
    :goto_1
    return v3

    .line 1208
    :cond_5
    :try_start_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/mfluent/asp/sync/g$c;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/mfluent/asp/sync/g$c;->c:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1565
    :catchall_0
    move-exception v2

    move-object v3, v9

    move-object v4, v10

    move-object v5, v11

    :goto_2
    if-eqz v3, :cond_6

    .line 1566
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1567
    :cond_6
    if-eqz v5, :cond_7

    .line 1570
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 1571
    :cond_7
    if-eqz v4, :cond_8

    .line 1574
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1575
    :cond_8
    throw v2

    .line 1218
    :cond_9
    :try_start_3
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1220
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/mfluent/asp/sync/g$c;->a:Landroid/net/Uri;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/mfluent/asp/sync/g$c;->b:[Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/mfluent/asp/sync/g$c;->c:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/mfluent/asp/sync/g$c;->d:[Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/mfluent/asp/sync/g$c;->e:Ljava/lang/String;

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v10

    .line 1227
    if-nez v10, :cond_d

    .line 1228
    :try_start_4
    sget-object v2, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x6

    if-gt v2, v3, :cond_a

    .line 1229
    const-string v2, "mfl_LocalDeviceMetadataSyncManager"

    const-string v3, "::syncHelper:Did not expect mediaStoreCursor to be null"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    .line 1231
    :cond_a
    if-eqz v11, :cond_b

    .line 1570
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1571
    :cond_b
    if-eqz v10, :cond_c

    .line 1574
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1575
    :cond_c
    const/4 v3, 0x0

    goto :goto_1

    .line 1233
    :cond_d
    :try_start_5
    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Total "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetContentUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1234
    const/4 v3, 0x1

    move/from16 v0, p3

    if-ne v0, v3, :cond_e

    .line 1235
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v3

    sput v3, Lcom/mfluent/asp/sync/g;->b:I

    .line 1237
    :cond_e
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1239
    const-string v3, "source_media_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    .line 1240
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/mfluent/asp/sync/g$c;->e:Ljava/lang/String;

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v23

    .line 1241
    if-eqz p7, :cond_37

    .line 1242
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/mfluent/asp/sync/g$c;->a:Landroid/net/Uri;

    move-object/from16 v0, p7

    iget-object v5, v0, Lcom/mfluent/asp/sync/g$c;->b:[Ljava/lang/String;

    move-object/from16 v0, p7

    iget-object v6, v0, Lcom/mfluent/asp/sync/g$c;->c:Ljava/lang/String;

    move-object/from16 v0, p7

    iget-object v7, v0, Lcom/mfluent/asp/sync/g$c;->d:[Ljava/lang/String;

    move-object/from16 v0, p7

    iget-object v8, v0, Lcom/mfluent/asp/sync/g$c;->e:Ljava/lang/String;

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    move-result-object v4

    .line 1250
    :goto_3
    if-eqz v4, :cond_17

    .line 1251
    :try_start_6
    move-object/from16 v0, p7

    iget-object v3, v0, Lcom/mfluent/asp/sync/g$c;->e:Ljava/lang/String;

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move/from16 v17, v3

    .line 1256
    :goto_4
    const-string v3, "geo_loc_locale"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_18

    const/4 v3, 0x1

    move/from16 v16, v3

    .line 1257
    :goto_5
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v24

    .line 1259
    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mediaStoreUri:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/mfluent/asp/sync/g$c;->a:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " MediaStore Record Count:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1260
    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "aspStoreUri:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v18

    int-to-long v6, v0

    move-object/from16 v0, p1

    invoke-interface {v0, v6, v7}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ASP Record Count:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v12

    move v6, v13

    move v7, v14

    move v8, v15

    .line 1266
    :goto_6
    invoke-interface {v11}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v3

    if-nez v3, :cond_19

    invoke-interface {v11}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_19

    const/4 v3, 0x1

    .line 1291
    :goto_7
    invoke-interface {v10}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v9

    if-nez v9, :cond_1a

    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v9

    if-nez v9, :cond_1a

    const/4 v9, 0x1

    .line 1294
    :goto_8
    if-eqz v3, :cond_1d

    .line 1295
    move/from16 v0, v22

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 1296
    if-eqz v9, :cond_1c

    .line 1297
    move/from16 v0, v23

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1299
    if-ne v12, v9, :cond_1b

    .line 1300
    const/4 v3, 0x1

    .line 1326
    :goto_9
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    .line 1328
    packed-switch v3, :pswitch_data_0

    :goto_a
    move v3, v5

    move v5, v6

    move v6, v7

    move v7, v8

    .line 1536
    :goto_b
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v8

    .line 1541
    const/16 v9, 0x1f4

    if-lt v8, v9, :cond_10

    .line 1543
    :try_start_7
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    const-string v9, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v8, v9, v12}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v9

    .line 1544
    if-nez v7, :cond_f

    .line 1545
    const/4 v8, 0x0

    :goto_c
    invoke-virtual/range {v20 .. v20}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v12

    if-ge v8, v12, :cond_f

    .line 1546
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Lcom/mfluent/asp/common/util/IntVector;->elementAt(I)I

    move-result v12

    .line 1547
    aget-object v12, v9, v12

    iget-object v12, v12, Landroid/content/ContentProviderResult;->count:Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result v12

    if-lez v12, :cond_33

    .line 1548
    const/4 v7, 0x1

    .line 1554
    :cond_f
    :try_start_8
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 1557
    invoke-virtual/range {v20 .. v20}, Lcom/mfluent/asp/common/util/IntVector;->removeAll()V

    .line 1559
    :cond_10
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/mfluent/asp/sync/g;->q:Landroid/content/Context;

    invoke-static {v8}, Lcom/samsung/android/sdk/samsung/a;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/a;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/samsung/a;->b()Z

    move-result v8

    if-eqz v8, :cond_34

    .line 1560
    const-string v8, "INFO"

    const-string v9, "Device is overheated high SIOP level, return during sync..."

    invoke-static {v8, v9}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move/from16 v28, v3

    move v3, v7

    move v7, v6

    move v6, v5

    move/from16 v5, v28

    .line 1565
    :goto_d
    if-eqz v4, :cond_11

    .line 1566
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1567
    :cond_11
    if-eqz v11, :cond_12

    .line 1570
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1571
    :cond_12
    if-eqz v10, :cond_13

    .line 1574
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1579
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_15

    .line 1581
    :try_start_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    const-string v8, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v4, v8, v9}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v8

    .line 1582
    if-nez v3, :cond_14

    .line 1583
    const/4 v4, 0x0

    :goto_e
    invoke-virtual/range {v20 .. v20}, Lcom/mfluent/asp/common/util/IntVector;->size()I

    move-result v9

    if-ge v4, v9, :cond_14

    .line 1584
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/common/util/IntVector;->elementAt(I)I

    move-result v9

    .line 1585
    aget-object v9, v8, v9

    iget-object v9, v9, Landroid/content/ContentProviderResult;->count:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    move-result v9

    if-lez v9, :cond_35

    .line 1586
    const/4 v3, 0x1

    .line 1592
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1596
    :cond_15
    invoke-virtual {v2}, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->a()V

    .line 1603
    if-eqz p6, :cond_16

    .line 1604
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    move-object/from16 v0, p6

    invoke-static {v2, v0}, Lcom/mfluent/asp/datamodel/al;->a(Landroid/content/ContentResolver;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;)V

    .line 1607
    :cond_16
    invoke-virtual/range {v21 .. v21}, Lcom/mfluent/asp/util/h;->a()Lcom/mfluent/asp/util/h;

    .line 1608
    const-string v2, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "insert: "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " update: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " deleteCount: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1610
    if-lez v7, :cond_4

    .line 1611
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.samsung.android.sdk.samsunglink.LOCAL_MEDIA_INSERTED"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1612
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/g;->q:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 1253
    :cond_17
    const/4 v3, -0x1

    move/from16 v17, v3

    goto/16 :goto_4

    .line 1256
    :cond_18
    const/4 v3, 0x0

    move/from16 v16, v3

    goto/16 :goto_5

    .line 1266
    :cond_19
    const/4 v3, 0x0

    goto/16 :goto_7

    .line 1291
    :cond_1a
    const/4 v9, 0x0

    goto/16 :goto_8

    .line 1301
    :cond_1b
    if-lt v12, v9, :cond_1c

    .line 1302
    const/4 v3, 0x0

    goto/16 :goto_9

    .line 1311
    :cond_1c
    const/4 v3, 0x2

    move v9, v12

    .line 1316
    goto/16 :goto_9

    .line 1317
    :cond_1d
    if-eqz v9, :cond_36

    .line 1318
    const/4 v3, 0x0

    .line 1319
    :try_start_a
    move/from16 v0, v23

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    goto/16 :goto_9

    .line 1331
    :pswitch_0
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 1332
    const-string v3, "device_id"

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v13, v3, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1333
    const-string v3, "source_media_id"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v13, v3, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1335
    move/from16 v0, v17

    invoke-static {v9, v4, v0}, Lcom/mfluent/asp/sync/g;->a(ILandroid/database/Cursor;I)Landroid/database/Cursor;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-interface {v0, v13, v10, v3}, Lcom/mfluent/asp/sync/g$a;->a(Landroid/content/ContentValues;Landroid/database/Cursor;Landroid/database/Cursor;)V

    .line 1337
    const-string v3, "mime_type"

    invoke-virtual {v13, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1e

    const-string v3, "_data"

    invoke-virtual {v13, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 1339
    const-string v3, "mime_type"

    new-instance v9, Ljava/io/File;

    const-string v14, "_data"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v9, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v13, v3, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1344
    :cond_1e
    const/4 v3, 0x0

    .line 1346
    const-string v9, "_data"

    invoke-virtual {v13, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1347
    const/4 v14, 0x3

    move/from16 v0, p3

    if-ne v0, v14, :cond_21

    invoke-static {v9}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_21

    .line 1348
    invoke-static {v9}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1349
    if-eqz v9, :cond_21

    const-string v14, "dm"

    invoke-virtual {v9, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1f

    const-string v14, "dcf"

    invoke-virtual {v9, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1f

    const-string v14, "pyv"

    invoke-virtual {v9, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1f

    const-string v14, "pya"

    invoke-virtual {v9, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1f

    const-string v14, "wvm"

    invoke-virtual {v9, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1f

    const-string v14, "sm4"

    invoke-virtual {v9, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_21

    .line 1355
    :cond_1f
    sget-object v3, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v14, 0x2

    if-gt v3, v14, :cond_20

    .line 1356
    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "mimeType : "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v14, " is DRM file. ignoring"

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1358
    :cond_20
    const/4 v3, 0x1

    .line 1363
    :cond_21
    if-eqz v19, :cond_22

    const-string v9, "_data"

    invoke-virtual {v13, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_26

    :cond_22
    if-nez v3, :cond_26

    .line 1376
    invoke-interface/range {p1 .. p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    .line 1377
    invoke-virtual {v3, v13}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 1378
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1380
    if-eqz p6, :cond_23

    .line 1381
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    move-object/from16 v0, p6

    invoke-static {v0, v12}, Lcom/mfluent/asp/datamodel/al;->a(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1384
    :cond_23
    new-instance v3, Ljava/io/File;

    const-string v8, "_data"

    invoke-static {v10, v8}, Lcom/mfluent/asp/sync/g;->h(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1385
    if-eqz v16, :cond_24

    invoke-static {v10}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_24

    .line 1389
    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "::syncHelperInner() insert case : sourceMediaIdStr added to geolocationList:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1390
    invoke-virtual {v2, v12}, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->a(Ljava/lang/String;)V

    .line 1391
    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Enter logAnalyticsDataForGeotaggedFiles mediaType:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    packed-switch p3, :pswitch_data_1

    .line 1393
    :cond_24
    :goto_f
    :pswitch_1
    const/4 v8, 0x1

    .line 1395
    add-int/lit8 v7, v7, 0x1

    .line 1405
    :cond_25
    :goto_10
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move v3, v5

    move v5, v6

    move v6, v7

    move v7, v8

    .line 1411
    goto/16 :goto_b

    .line 1391
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/g;->q:Landroid/content/Context;

    sget-object v8, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v9, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->E:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v3, v8, v9}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_f

    .line 1565
    :catchall_1
    move-exception v2

    move-object v3, v4

    move-object v5, v11

    move-object v4, v10

    goto/16 :goto_2

    .line 1391
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/g;->q:Landroid/content/Context;

    sget-object v8, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v9, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->G:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v3, v8, v9}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_f

    .line 1397
    :cond_26
    sget-object v3, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v9, 0x2

    if-gt v3, v9, :cond_25

    .line 1398
    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v12, "(Local Device) Skipping media insert because of secure mode: "

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetContentUri()Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, ": "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p5

    invoke-virtual {v13, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, " or drm video file."

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_10

    .line 1415
    :pswitch_4
    const/4 v3, 0x0

    .line 1418
    if-eqz v16, :cond_27

    .line 1419
    const-string v3, "geo_loc_locale"

    invoke-static {v11, v3}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1421
    invoke-static {v10}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;)Z

    move-result v13

    if-eqz v13, :cond_2f

    const-string v13, "latitude"

    invoke-static {v10, v13}, Lcom/mfluent/asp/sync/g;->g(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v13

    const-string v14, "longitude"

    invoke-static {v10, v14}, Lcom/mfluent/asp/sync/g;->g(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v14

    move-object/from16 v0, v24

    invoke-static {v0, v13, v14}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/util/Locale;Ljava/lang/Double;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v3, v13}, Lorg/apache/commons/lang3/ObjectUtils;->notEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2f

    const/4 v3, 0x1

    .line 1432
    :cond_27
    :goto_11
    move/from16 v0, v17

    invoke-static {v9, v4, v0}, Lcom/mfluent/asp/sync/g;->a(ILandroid/database/Cursor;I)Landroid/database/Cursor;

    move-result-object v13

    .line 1433
    move-object/from16 v0, p4

    invoke-interface {v0, v11, v10, v13}, Lcom/mfluent/asp/sync/g$a;->a(Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;)I

    move-result v14

    if-eqz v14, :cond_30

    .line 1436
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1437
    const-string v14, "device_id"

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v8, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1438
    const-string v14, "source_media_id"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1439
    move-object/from16 v0, p4

    invoke-interface {v0, v8, v10, v13}, Lcom/mfluent/asp/sync/g$a;->a(Landroid/content/ContentValues;Landroid/database/Cursor;Landroid/database/Cursor;)V

    .line 1441
    const-string v9, "mime_type"

    invoke-virtual {v8, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_28

    const-string v9, "_data"

    invoke-virtual {v8, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_28

    .line 1443
    const-string v9, "mime_type"

    new-instance v13, Ljava/io/File;

    const-string v14, "_data"

    invoke-virtual {v8, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v13}, Lcom/mfluent/asp/common/util/FileTypeHelper;->getMimeTypeForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v9, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1448
    :cond_28
    sget-object v9, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v9}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v9

    const/4 v13, 0x3

    if-gt v9, v13, :cond_29

    .line 1449
    const-string v9, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "(Local Device) Updating "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetContentUri()Landroid/net/Uri;

    move-result-object v14

    invoke-virtual {v14}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ": "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p5

    invoke-virtual {v8, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v9, v13}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    :cond_29
    const-string v9, "_id"

    invoke-static {v11, v9}, Lcom/mfluent/asp/sync/g;->e(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    int-to-long v14, v9

    move-object/from16 v0, p1

    invoke-interface {v0, v14, v15}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetEntryUri(J)Landroid/net/Uri;

    move-result-object v9

    invoke-static {v9}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    .line 1460
    invoke-virtual {v9, v8}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 1461
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1465
    if-eqz p6, :cond_2a

    .line 1466
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    move-object/from16 v0, p6

    invoke-static {v0, v12}, Lcom/mfluent/asp/datamodel/al;->b(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1468
    :cond_2a
    const/4 v8, 0x1

    .line 1470
    new-instance v9, Ljava/io/File;

    const-string v13, "_data"

    invoke-static {v10, v13}, Lcom/mfluent/asp/sync/g;->h(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v9, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1471
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_2b

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v14

    const-wide/16 v26, 0x0

    cmp-long v13, v14, v26

    if-lez v13, :cond_2b

    .line 1472
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v14

    const-string v13, "file_digest_size"

    invoke-static {v11, v13}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v26

    cmp-long v13, v14, v26

    if-nez v13, :cond_2b

    invoke-virtual {v9}, Ljava/io/File;->lastModified()J

    const-string v9, "file_digest_time"

    invoke-static {v11, v9}, Lcom/mfluent/asp/common/util/CursorUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    .line 1476
    :cond_2b
    add-int/lit8 v6, v6, 0x1

    .line 1485
    :cond_2c
    :goto_12
    if-eqz v3, :cond_2e

    .line 1490
    const-string v3, "geo_is_valid_map_coord"

    invoke-static {v11, v3}, Lcom/mfluent/asp/sync/g;->e(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    .line 1491
    const-string v9, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "::syncHelperInner() update case : isMapCoordValid:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " sourceMediaIdStr:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v9, v13}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1492
    if-eqz v3, :cond_2d

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_2e

    .line 1493
    :cond_2d
    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v13, "::syncHelperInner() update case : sourceMediaIdStr added to geolocationList:"

    invoke-direct {v9, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1494
    invoke-virtual {v2, v12}, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->a(Ljava/lang/String;)V

    .line 1498
    :cond_2e
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    .line 1503
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move v3, v5

    move v5, v6

    move v6, v7

    move v7, v8

    .line 1505
    goto/16 :goto_b

    .line 1421
    :cond_2f
    const/4 v3, 0x0

    goto/16 :goto_11

    .line 1478
    :cond_30
    new-instance v9, Ljava/io/File;

    const-string v13, "_data"

    invoke-static {v10, v13}, Lcom/mfluent/asp/sync/g;->h(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v9, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1479
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_2c

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v14

    const-wide/16 v26, 0x0

    cmp-long v9, v14, v26

    if-lez v9, :cond_2c

    .line 1480
    const-string v9, "file_digest"

    invoke-static {v11, v9}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2c

    const-string v9, "file_digest_time"

    invoke-static {v11, v9}, Lcom/mfluent/asp/sync/g;->f(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    goto :goto_12

    .line 1509
    :pswitch_5
    sget-object v3, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v9, 0x3

    if-gt v3, v9, :cond_31

    .line 1510
    const-string v3, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v13, "(Local Device) Deleting "

    invoke-direct {v9, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetContentUri()Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {v13}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v13, ": "

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v13, "_id"

    invoke-static {v11, v13}, Lcom/mfluent/asp/sync/g;->e(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1518
    :cond_31
    const-string v3, "_id"

    invoke-static {v11, v3}, Lcom/mfluent/asp/sync/g;->e(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v14, v3

    move-object/from16 v0, p1

    invoke-interface {v0, v14, v15}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetEntryUri(J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    .line 1521
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1524
    if-eqz p6, :cond_32

    .line 1525
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/common/util/IntVector;->append(I)V

    .line 1526
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    move-object/from16 v0, p6

    invoke-static {v0, v12}, Lcom/mfluent/asp/datamodel/al;->c(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1529
    :cond_32
    add-int/lit8 v5, v5, 0x1

    .line 1531
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_a

    .line 1545
    :cond_33
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_c

    .line 1554
    :catchall_2
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_34
    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v3

    .line 1563
    goto/16 :goto_6

    .line 1583
    :cond_35
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_e

    .line 1592
    :catchall_3
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    throw v2

    .line 1565
    :catchall_4
    move-exception v2

    move-object v3, v9

    move-object v4, v10

    move-object v5, v11

    goto/16 :goto_2

    :catchall_5
    move-exception v2

    move-object v3, v9

    move-object v4, v10

    move-object v5, v11

    goto/16 :goto_2

    :cond_36
    move v3, v8

    goto/16 :goto_d

    :cond_37
    move-object v4, v9

    goto/16 :goto_3

    .line 1328
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1391
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method

.method static synthetic c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/mfluent/asp/sync/g;->e(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Landroid/database/Cursor;Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/mfluent/asp/sync/g;->e(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2, p3}, Lcom/mfluent/asp/sync/g;->e(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;
    .locals 1

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/mfluent/asp/sync/g;->g(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/database/Cursor;Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1674
    invoke-static {p0, p1}, Lcom/mfluent/asp/sync/g;->f(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p2, p3}, Lcom/mfluent/asp/sync/g;->f(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static e(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 1638
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1639
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1640
    :cond_0
    const/4 v0, 0x0

    .line 1642
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic f()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/mfluent/asp/sync/g;->p:Landroid/content/Intent;

    return-object v0
.end method

.method private static f(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 1646
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1647
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1648
    :cond_0
    const/4 v0, 0x0

    .line 1650
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic g()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/mfluent/asp/sync/g;->m:Landroid/content/Intent;

    return-object v0
.end method

.method private static g(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;
    .locals 2

    .prologue
    .line 1654
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1655
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1656
    :cond_0
    const/4 v0, 0x0

    .line 1658
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic h()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/mfluent/asp/sync/g;->n:Landroid/content/Intent;

    return-object v0
.end method

.method private static h(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1662
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1663
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1664
    const/4 v0, 0x0

    .line 1666
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic i()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/mfluent/asp/sync/g;->o:Landroid/content/Intent;

    return-object v0
.end method

.method private m()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 318
    new-instance v2, Lcom/mfluent/asp/sync/g$c;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Lcom/mfluent/asp/sync/g$c;-><init>(B)V

    .line 319
    sget-object v0, Lcom/mfluent/asp/util/e;->a:Landroid/net/Uri;

    iput-object v0, v2, Lcom/mfluent/asp/sync/g$c;->a:Landroid/net/Uri;

    .line 320
    sget-object v0, Lcom/mfluent/asp/sync/g;->f:Ljava/lang/String;

    iput-object v0, v2, Lcom/mfluent/asp/sync/g$c;->c:Ljava/lang/String;

    .line 321
    sget-object v0, Lcom/mfluent/asp/sync/g;->g:[Ljava/lang/String;

    iput-object v0, v2, Lcom/mfluent/asp/sync/g$c;->d:[Ljava/lang/String;

    .line 323
    new-instance v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;

    invoke-direct {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;-><init>()V

    const/16 v3, 0xf

    new-instance v4, Lcom/mfluent/asp/sync/g$5;

    invoke-direct {v4, p0}, Lcom/mfluent/asp/sync/g$5;-><init>(Lcom/mfluent/asp/sync/g;)V

    const-string v5, "_display_name"

    new-instance v6, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Journal;

    invoke-direct {v6}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Journal;-><init>()V

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/mfluent/asp/sync/g;->a(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;Lcom/mfluent/asp/sync/g$c;ILcom/mfluent/asp/sync/g$a;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Lcom/mfluent/asp/sync/g$c;)Z

    move-result v0

    return v0
.end method

.method private n()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 362
    new-instance v2, Lcom/mfluent/asp/sync/g$c;

    invoke-direct {v2, v4}, Lcom/mfluent/asp/sync/g$c;-><init>(B)V

    .line 363
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, v2, Lcom/mfluent/asp/sync/g$c;->a:Landroid/net/Uri;

    .line 365
    new-instance v7, Lcom/mfluent/asp/sync/g$c;

    invoke-direct {v7, v4}, Lcom/mfluent/asp/sync/g$c;-><init>(B)V

    .line 366
    sget-object v0, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, v7, Lcom/mfluent/asp/sync/g$c;->a:Landroid/net/Uri;

    .line 367
    const-string v0, "image_id"

    iput-object v0, v7, Lcom/mfluent/asp/sync/g$c;->e:Ljava/lang/String;

    .line 368
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "image_id"

    aput-object v1, v0, v4

    const-string v1, "_data"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v4, "width"

    aput-object v4, v0, v1

    const/4 v1, 0x3

    const-string v4, "height"

    aput-object v4, v0, v1

    iput-object v0, v7, Lcom/mfluent/asp/sync/g$c;->b:[Ljava/lang/String;

    .line 374
    new-instance v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;

    invoke-direct {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;-><init>()V

    new-instance v4, Lcom/mfluent/asp/sync/g$6;

    invoke-direct {v4, p0}, Lcom/mfluent/asp/sync/g$6;-><init>(Lcom/mfluent/asp/sync/g;)V

    const-string v5, "_display_name"

    new-instance v6, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Journal;

    invoke-direct {v6}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Journal;-><init>()V

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/mfluent/asp/sync/g;->a(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;Lcom/mfluent/asp/sync/g$c;ILcom/mfluent/asp/sync/g$a;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Lcom/mfluent/asp/sync/g$c;)Z

    move-result v0

    return v0
.end method

.method private o()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    const/4 v4, 0x0

    .line 512
    new-instance v2, Lcom/mfluent/asp/sync/g$c;

    invoke-direct {v2, v4}, Lcom/mfluent/asp/sync/g$c;-><init>(B)V

    .line 513
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, v2, Lcom/mfluent/asp/sync/g$c;->a:Landroid/net/Uri;

    .line 515
    new-instance v7, Lcom/mfluent/asp/sync/g$c;

    invoke-direct {v7, v4}, Lcom/mfluent/asp/sync/g$c;-><init>(B)V

    .line 516
    sget-object v0, Landroid/provider/MediaStore$Video$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, v7, Lcom/mfluent/asp/sync/g$c;->a:Landroid/net/Uri;

    .line 517
    const-string v0, "video_id"

    iput-object v0, v7, Lcom/mfluent/asp/sync/g$c;->e:Ljava/lang/String;

    .line 518
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "video_id"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v4, "_data"

    aput-object v4, v0, v1

    const/4 v1, 0x2

    const-string v4, "width"

    aput-object v4, v0, v1

    const-string v1, "height"

    aput-object v1, v0, v3

    iput-object v0, v7, Lcom/mfluent/asp/sync/g$c;->b:[Ljava/lang/String;

    .line 524
    new-instance v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;

    invoke-direct {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;-><init>()V

    new-instance v4, Lcom/mfluent/asp/sync/g$7;

    invoke-direct {v4, p0}, Lcom/mfluent/asp/sync/g$7;-><init>(Lcom/mfluent/asp/sync/g;)V

    const-string v5, "_display_name"

    new-instance v6, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Journal;

    invoke-direct {v6}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Journal;-><init>()V

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/mfluent/asp/sync/g;->a(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;Lcom/mfluent/asp/sync/g$c;ILcom/mfluent/asp/sync/g$a;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Lcom/mfluent/asp/sync/g$c;)Z

    move-result v0

    return v0
.end method

.method private p()Z
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 743
    new-instance v2, Lcom/mfluent/asp/sync/g$c;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Lcom/mfluent/asp/sync/g$c;-><init>(B)V

    .line 746
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, v2, Lcom/mfluent/asp/sync/g$c;->a:Landroid/net/Uri;

    .line 747
    const-string v0, "is_music=? OR is_podcast=?"

    iput-object v0, v2, Lcom/mfluent/asp/sync/g$c;->c:Ljava/lang/String;

    .line 748
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "1"

    aput-object v3, v0, v1

    const/4 v1, 0x1

    const-string v3, "1"

    aput-object v3, v0, v1

    iput-object v0, v2, Lcom/mfluent/asp/sync/g$c;->d:[Ljava/lang/String;

    .line 750
    new-instance v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;

    invoke-direct {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;-><init>()V

    const/4 v3, 0x2

    new-instance v4, Lcom/mfluent/asp/sync/g$8;

    invoke-direct {v4, p0}, Lcom/mfluent/asp/sync/g$8;-><init>(Lcom/mfluent/asp/sync/g;)V

    const-string v5, "_display_name"

    new-instance v6, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;

    invoke-direct {v6}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Journal;-><init>()V

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/mfluent/asp/sync/g;->a(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;Lcom/mfluent/asp/sync/g$c;ILcom/mfluent/asp/sync/g$a;Ljava/lang/String;Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Lcom/mfluent/asp/sync/g$c;)Z

    move-result v7

    .line 847
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/g;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Artists;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 848
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/g;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Albums;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 850
    if-eqz v7, :cond_1

    .line 851
    new-instance v8, Landroid/util/SparseArray;

    const/16 v0, 0x32

    invoke-direct {v8, v0}, Landroid/util/SparseArray;-><init>(I)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v8, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    sget-object v0, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v0, "all"

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "members"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "genre_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "audio_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-eqz v9, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/g;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    invoke-static {v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres$Members;->getSyncingUriForDevice(I)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "genre_key"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "audio_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "source_media_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "CAST (source_media_id as integer)"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-nez v10, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 854
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->q()V

    .line 856
    return v7

    .line 851
    :cond_2
    new-instance v11, Ljava/util/ArrayList;

    const/4 v0, 0x5

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v12, Ljava/util/HashSet;

    const/4 v0, 0x5

    invoke-direct {v12, v0}, Ljava/util/HashSet;-><init>(I)V

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_3

    if-nez v0, :cond_16

    :cond_3
    const/4 v5, 0x0

    const/4 v2, 0x0

    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    const-string v3, "<unknown>"

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12}, Ljava/util/HashSet;->clear()V

    const/4 v3, -0x1

    const/4 v4, -0x1

    if-eqz v1, :cond_8

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    :cond_4
    :goto_4
    if-eqz v5, :cond_d

    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    :goto_5
    if-nez v0, :cond_d

    const/4 v5, 0x0

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-ne v4, v5, :cond_d

    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v8, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    goto :goto_5

    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    goto :goto_3

    :cond_8
    if-eqz v0, :cond_9

    const/4 v2, 0x1

    const-string v3, "source_media_id"

    invoke-static {v10, v3}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v3

    goto :goto_4

    :cond_9
    if-nez v0, :cond_4

    if-nez v1, :cond_4

    const-string v3, "source_media_id"

    invoke-static {v10, v3}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x0

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-ne v4, v3, :cond_a

    const/4 v2, 0x1

    move v5, v2

    goto :goto_4

    :cond_a
    if-le v4, v3, :cond_b

    const/4 v2, 0x1

    goto :goto_4

    :cond_b
    if-ge v4, v3, :cond_4

    const/4 v3, -0x1

    const/4 v5, 0x1

    goto :goto_4

    :cond_c
    const/4 v0, 0x0

    goto :goto_5

    :cond_d
    move v6, v0

    if-eqz v2, :cond_11

    move v0, v1

    :goto_6
    if-nez v0, :cond_10

    const/4 v1, 0x2

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v3, v1, :cond_10

    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-virtual {v12, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_e
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    goto :goto_6

    :cond_f
    const/4 v0, 0x0

    goto :goto_6

    :cond_10
    move v1, v0

    :cond_11
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    move v5, v0

    :goto_7
    if-ltz v5, :cond_12

    invoke-virtual {v11, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->keyFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_17

    invoke-virtual {v12, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, -0x1

    :goto_8
    add-int/lit8 v2, v5, -0x1

    move v5, v2

    move v2, v0

    goto :goto_7

    :cond_12
    if-gtz v2, :cond_13

    invoke-virtual {v12}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_14

    :cond_13
    iget-object v5, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    iget-object v13, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/g;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v14

    if-lez v3, :cond_15

    move v2, v3

    :goto_9
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v13, v0, v14, v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres$Members;->setAudioGenresBySourceMediaId(Landroid/content/ContentResolver;Ljava/util/ArrayList;[Ljava/lang/String;ILjava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v2, 0x1f4

    if-lt v0, v2, :cond_14

    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->q()V

    :cond_14
    move v0, v6

    goto/16 :goto_3

    :cond_15
    move v2, v4

    goto :goto_9

    :cond_16
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/g;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->q()V

    goto/16 :goto_1

    :cond_17
    move v0, v2

    goto :goto_8
.end method

.method private q()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1002
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1004
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1007
    :cond_0
    return-void

    .line 1004
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    throw v0
.end method


# virtual methods
.method protected final a()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    invoke-super {p0}, Lcom/mfluent/asp/sync/f;->a()Ljava/util/Collection;

    move-result-object v0

    .line 131
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.DataModel.REFRESH_ALL"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 132
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/mfluent/asp/ASPApplication;->e:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 133
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->a:Lcom/mfluent/asp/datamodel/Device;

    const-string v2, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_REFRESH"

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->buildDeviceIntentFilterForAction(Ljava/lang/String;)Landroid/content/IntentFilter;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 134
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/mfluent/asp/ASPApplication;->f:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 136
    return-object v0
.end method

.method protected final a_(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x1

    return v0
.end method

.method protected final b(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v3, 0x6

    .line 249
    sget-object v0, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 250
    const-string v0, "mfl_LocalDeviceMetadataSyncManager"

    const-string v1, "Syncing local device metadata"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/a;

    move-result-object v0

    .line 254
    invoke-static {}, Lcom/samsung/android/sdk/samsung/a;->c()I

    move-result v1

    .line 255
    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsung/a;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 256
    const-string v1, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "device siop level = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsung/a;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/mfluent/asp/sync/g;->a(Landroid/content/Intent;J)V

    .line 307
    :cond_1
    :goto_0
    return-void

    .line 260
    :cond_2
    if-lez v1, :cond_3

    .line 261
    const-string v0, "INFO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "High CPU usage, delay sync after "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sec"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/mfluent/asp/sync/g;->a(Landroid/content/Intent;J)V

    goto :goto_0

    .line 266
    :cond_3
    const/4 v1, 0x0

    .line 268
    :try_start_0
    sget-object v0, Lcom/mfluent/asp/sync/g;->m:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 269
    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->n()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 287
    :goto_1
    :try_start_1
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/mfluent/asp/sync/g;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-static {v2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files$Keywords;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289
    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->q()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_2

    .line 299
    :cond_4
    :goto_2
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 304
    if-eqz v0, :cond_1

    .line 305
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->l:Lcom/mfluent/asp/sync/h;

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/h;->a()V

    goto :goto_0

    .line 270
    :cond_5
    :try_start_2
    sget-object v0, Lcom/mfluent/asp/sync/g;->n:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 271
    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->o()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    .line 272
    :cond_6
    sget-object v0, Lcom/mfluent/asp/sync/g;->o:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 273
    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->p()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    .line 274
    :cond_7
    sget-object v0, Lcom/mfluent/asp/sync/g;->p:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 275
    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->m()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    .line 277
    :cond_8
    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->n()Z

    move-result v0

    or-int/lit8 v1, v0, 0x0

    .line 278
    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->o()Z

    move-result v0

    or-int/2addr v1, v0

    .line 280
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->q:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 281
    const-string v0, "mfl_LocalDeviceMetadataSyncManager"

    const-string v2, "::doStart() isSignedIn is true -> Starting Audio and Document Synching"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->p()Z

    move-result v0

    or-int/2addr v1, v0

    .line 283
    invoke-direct {p0}, Lcom/mfluent/asp/sync/g;->m()Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v0

    or-int/2addr v0, v1

    goto :goto_1

    .line 290
    :catch_0
    move-exception v0

    move-object v4, v0

    move v0, v1

    move-object v1, v4

    .line 291
    :goto_3
    sget-object v2, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 292
    const-string v2, "mfl_LocalDeviceMetadataSyncManager"

    const-string v3, "RemoteException thrown during sync."

    invoke-static {v2, v3, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 294
    :catch_1
    move-exception v0

    move-object v4, v0

    move v0, v1

    move-object v1, v4

    .line 295
    :goto_4
    sget-object v2, Lcom/mfluent/asp/sync/g;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 296
    const-string v2, "mfl_LocalDeviceMetadataSyncManager"

    const-string v3, "OperationApplicationException thrown during sync."

    invoke-static {v2, v3, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 294
    :catch_2
    move-exception v1

    goto :goto_4

    .line 290
    :catch_3
    move-exception v1

    goto :goto_3

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method protected final b()Z
    .locals 1

    .prologue
    .line 1695
    const/4 v0, 0x1

    return v0
.end method

.method protected final c(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 146
    invoke-super {p0, p1}, Lcom/mfluent/asp/sync/f;->c(Landroid/content/Intent;)V

    .line 148
    const-string v0, "mfl_LocalDeviceMetadataSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::doStart() with intent:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    new-instance v0, Lcom/mfluent/asp/sync/g$1;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/g;->k()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/sync/g$1;-><init>(Lcom/mfluent/asp/sync/g;Landroid/os/Handler;)V

    .line 167
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    sget-object v2, Lcom/mfluent/asp/util/e;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 168
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->k:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 170
    new-instance v0, Lcom/mfluent/asp/sync/g$2;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/g;->k()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/sync/g$2;-><init>(Lcom/mfluent/asp/sync/g;Landroid/os/Handler;)V

    .line 184
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 185
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->k:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 187
    new-instance v0, Lcom/mfluent/asp/sync/g$3;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/g;->k()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/sync/g$3;-><init>(Lcom/mfluent/asp/sync/g;Landroid/os/Handler;)V

    .line 201
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 202
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->k:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 204
    new-instance v0, Lcom/mfluent/asp/sync/g$4;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/g;->k()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/sync/g$4;-><init>(Lcom/mfluent/asp/sync/g;Landroid/os/Handler;)V

    .line 222
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 223
    iget-object v1, p0, Lcom/mfluent/asp/sync/g;->k:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 225
    return-void
.end method

.method protected final d(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 233
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/ContentObserver;

    .line 234
    iget-object v2, p0, Lcom/mfluent/asp/sync/g;->i:Landroid/content/ContentResolver;

    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/g;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 239
    invoke-super {p0, p1}, Lcom/mfluent/asp/sync/f;->d(Landroid/content/Intent;)V

    .line 240
    return-void
.end method

.method public final declared-synchronized e(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 141
    monitor-enter p0

    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-virtual {p0, p1, v0, v1}, Lcom/mfluent/asp/sync/g;->a(Landroid/content/Intent;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    monitor-exit p0

    return-void

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
