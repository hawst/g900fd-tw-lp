.class public Lcom/mfluent/asp/sync/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:Landroid/os/Handler;

.field private c:Ljava/lang/Runnable;

.field private d:Z

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private f:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/sync/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/h;->b:Landroid/os/Handler;

    .line 30
    iput-object v1, p0, Lcom/mfluent/asp/sync/h;->c:Ljava/lang/Runnable;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/h;->d:Z

    .line 32
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/h;->e:Ljava/util/concurrent/ExecutorService;

    .line 33
    iput-object v1, p0, Lcom/mfluent/asp/sync/h;->f:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;Z)V
    .locals 4

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "mfl_MflNotificationManager"

    const-string v1, "doNTSDeviceListPoke"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "SPName"

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "IsSignin"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/sync/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_1

    const-string v1, "mfl_MflNotificationManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doNTSDeviceListPoke: Error sending NTS content changed notification to device: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    sget-object v1, Lcom/mfluent/asp/sync/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_0

    const-string v1, "mfl_MflNotificationManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doNTSDeviceListPoke: Error sending NTS content changed notification to device peerId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "PeerId"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    const-string v1, "/v1/media/notification/contentChanged"

    invoke-static {p0, v1, v0}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/sync/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_1

    const-string v1, "mfl_MflNotificationManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doNTSPoke: Error sending NTS content changed notification to device: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    sget-object v1, Lcom/mfluent/asp/sync/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_0

    const-string v1, "mfl_MflNotificationManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doNTSPoke: Error sending NTS content changed notification to device peerId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/mfluent/asp/sync/h;)V
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/h;->d:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mfluent/asp/sync/h;->b()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/h;->d:Z

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/sync/h;->c:Ljava/lang/Runnable;

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 68
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    .line 69
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 71
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 73
    if-eq v0, v1, :cond_0

    .line 74
    iget-object v3, p0, Lcom/mfluent/asp/sync/h;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/mfluent/asp/sync/h$2;

    invoke-direct {v4, p0, v0, v1}, Lcom/mfluent/asp/sync/h$2;-><init>(Lcom/mfluent/asp/sync/h;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 87
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/mfluent/asp/sync/h;->c:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 40
    invoke-direct {p0}, Lcom/mfluent/asp/sync/h;->b()V

    .line 49
    :goto_0
    new-instance v0, Lcom/mfluent/asp/sync/h$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/h$1;-><init>(Lcom/mfluent/asp/sync/h;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/h;->c:Ljava/lang/Runnable;

    .line 56
    iget-object v0, p0, Lcom/mfluent/asp/sync/h;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/sync/h;->c:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 57
    return-void

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/h;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/sync/h;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/h;->d:Z

    goto :goto_0
.end method

.method public final a(Lcom/mfluent/asp/datamodel/Device;Z)V
    .locals 4

    .prologue
    .line 111
    const-string v0, "mfl_MflNotificationManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deviceListChange : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v0, "mfl_MflNotificationManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isWebStorageSignedIn : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    new-instance v0, Lcom/mfluent/asp/sync/h$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/mfluent/asp/sync/h$3;-><init>(Lcom/mfluent/asp/sync/h;Lcom/mfluent/asp/datamodel/Device;Z)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/h;->f:Ljava/lang/Runnable;

    .line 122
    iget-object v0, p0, Lcom/mfluent/asp/sync/h;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/sync/h;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 124
    iget-object v0, p0, Lcom/mfluent/asp/sync/h;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mfluent/asp/sync/h;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 125
    return-void
.end method

.method public final b(Lcom/mfluent/asp/datamodel/Device;Z)V
    .locals 5

    .prologue
    .line 128
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    .line 129
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 131
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 133
    if-eq v0, v1, :cond_0

    .line 134
    iget-object v3, p0, Lcom/mfluent/asp/sync/h;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/mfluent/asp/sync/h$4;

    invoke-direct {v4, p0, v0, p1, p2}, Lcom/mfluent/asp/sync/h$4;-><init>(Lcom/mfluent/asp/sync/h;Lcom/mfluent/asp/datamodel/Device;Lcom/mfluent/asp/datamodel/Device;Z)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 147
    :cond_1
    return-void
.end method
