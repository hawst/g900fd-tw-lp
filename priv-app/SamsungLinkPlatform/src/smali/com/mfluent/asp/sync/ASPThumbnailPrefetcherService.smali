.class public Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;
.super Landroid/app/IntentService;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static final c:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;",
            "Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/mfluent/asp/util/bitmap/ImageWorker;

.field private e:Z

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    const-class v0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->a:Lorg/slf4j/Logger;

    .line 96
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 97
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->d:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 102
    const-string v0, "ASPThumbnailPrefetcher"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->e:Z

    .line 103
    return-void
.end method

.method static synthetic a()Ljava/util/concurrent/locks/ReentrantLock;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->c:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method static synthetic b()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->d:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 109
    new-instance v0, Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/util/bitmap/ImageWorker;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->b:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->e:Z

    .line 112
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 113
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->f:I

    .line 114
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->e:Z

    .line 121
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 14

    .prologue
    const/4 v6, 0x4

    const/4 v13, 0x2

    const/4 v12, 0x3

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 125
    .line 126
    const-string v2, "com.mfluent.asp.sync.ASPThumbnailPrefetcherService.ACTION_PREFETCH_DEVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 127
    sget-object v2, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->a:Lorg/slf4j/Logger;

    const-string v3, "Unknown intent action: {}"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    const-string v3, "DEVICE_ID"

    invoke-virtual {p1, v3, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v11

    .line 131
    if-nez v11, :cond_2

    .line 132
    sget-object v2, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->a:Lorg/slf4j/Logger;

    const-string v3, "Device was not found. Device id: {}"

    const-string v4, "DEVICE_ID"

    invoke-virtual {p1, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 136
    :cond_2
    :try_start_0
    const-string v2, "TRIGGER_BY_SFINDER"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 137
    if-ne v2, v8, :cond_f

    move v2, v8

    :goto_1
    move v10, v2

    .line 144
    :goto_2
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    .line 145
    if-eq v11, v2, :cond_0

    .line 149
    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-eq v3, v4, :cond_3

    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v3, v4, :cond_4

    :cond_3
    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-eq v2, v3, :cond_5

    .line 151
    :cond_4
    sget-object v2, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->a:Lorg/slf4j/Logger;

    const-string v3, "Device {} is not on WIFI or local device is not on WIFI. Giving up."

    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 152
    if-eqz v10, :cond_0

    .line 155
    sget-object v2, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->a:Lorg/slf4j/Logger;

    const-string v3, "triggered by SFINDER=>prefetch more..."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 158
    :cond_5
    const/16 v2, 0xd

    new-array v4, v2, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v4, v9

    const-string v2, "device_id"

    aput-object v2, v4, v8

    const-string v2, "source_media_id"

    aput-object v2, v4, v13

    const-string v2, "media_type"

    aput-object v2, v4, v12

    const-string v2, "full_uri"

    aput-object v2, v4, v6

    const/4 v2, 0x5

    const-string v3, "_data"

    aput-object v3, v4, v2

    const/4 v2, 0x6

    const-string v3, "_display_name"

    aput-object v3, v4, v2

    const/4 v2, 0x7

    const-string v3, "transport_type"

    aput-object v3, v4, v2

    const/16 v2, 0x8

    const-string v3, "orientation"

    aput-object v3, v4, v2

    const/16 v2, 0x9

    const-string v3, "thumbnail_uri"

    aput-object v3, v4, v2

    const/16 v2, 0xa

    const-string v3, "width"

    aput-object v3, v4, v2

    const/16 v2, 0xb

    const-string v3, "height"

    aput-object v3, v4, v2

    const/16 v2, 0xc

    const-string v3, "album_id"

    aput-object v3, v4, v2

    .line 173
    if-nez v10, :cond_6

    .line 174
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "device_id=? AND (media_type=? OR media_type=?)"

    new-array v6, v12, [Ljava/lang/String;

    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v13

    const-string v7, "date_added DESC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 203
    :goto_3
    new-instance v4, Ljava/util/ArrayList;

    const/16 v3, 0x32

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 205
    if-eqz v2, :cond_9

    .line 206
    :goto_4
    iget-boolean v3, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->e:Z

    if-nez v3, :cond_8

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v5, 0x32

    if-ge v3, v5, :cond_8

    .line 207
    invoke-static {v2}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->fromCursor(Landroid/database/Cursor;)Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    move-result-object v5

    .line 208
    iget v3, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->f:I

    invoke-virtual {v5, v3}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setDesiredWidth(I)V

    .line 209
    iget v3, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->f:I

    invoke-virtual {v5, v3}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setDesiredHeight(I)V

    .line 210
    invoke-virtual {v5}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getMediaType()I

    move-result v3

    if-ne v3, v8, :cond_7

    if-nez v10, :cond_7

    sget-object v3, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->FULL_SCREEN:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    :goto_5
    invoke-virtual {v5, v3}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->setThumbnailSize(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;)V

    .line 212
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 140
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move v10, v9

    goto/16 :goto_2

    .line 184
    :cond_6
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "device_id=? AND (media_type=? OR media_type=? OR media_type=? )"

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v13

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v12

    const-string v7, "date_added DESC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_3

    .line 210
    :cond_7
    sget-object v3, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;->MINI:Lcom/mfluent/asp/common/media/thumbnails/ImageInfo$ThumbnailSize;

    goto :goto_5

    .line 215
    :cond_8
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 218
    :cond_9
    iget-boolean v2, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->e:Z

    if-nez v2, :cond_0

    .line 222
    invoke-static {p0}, Lcom/mfluent/asp/media/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/media/d;

    move-result-object v2

    .line 223
    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    invoke-virtual {v2, v3, v4}, Lcom/mfluent/asp/media/d;->a(ILjava/util/ArrayList;)V

    .line 227
    sget-object v2, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 229
    :try_start_1
    new-instance v5, Ljava/util/HashMap;

    sget-object v2, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->d:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-direct {v5, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 230
    sget-object v2, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->d:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_a
    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/Map$Entry;

    move-object v3, v0

    .line 231
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;->getDeviceId()I

    move-result v2

    invoke-virtual {v11}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    if-ne v2, v7, :cond_a

    .line 232
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6

    .line 236
    :catchall_0
    move-exception v2

    sget-object v3, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    :cond_b
    sget-object v2, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 239
    :goto_7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v9, v2, :cond_d

    iget-boolean v2, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->e:Z

    if-nez v2, :cond_d

    .line 240
    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;

    .line 241
    sget-object v3, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 244
    :try_start_2
    sget-object v3, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->d:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 245
    sget-object v3, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->a:Lorg/slf4j/Logger;

    const-string v6, "Prefetching thumbnail: {}"

    invoke-interface {v3, v6, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 246
    new-instance v3, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;

    invoke-direct {v3, v2}, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService$a;-><init>(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;)V

    .line 247
    sget-object v6, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->d:Ljava/util/HashMap;

    invoke-virtual {v6, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    iget-object v6, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->b:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    sget-object v7, Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;->d:Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;

    sget v8, Lcom/mfluent/asp/util/bitmap/ImageWorker;->c:I

    invoke-virtual {v6, v2, v3, v7, v8}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/common/media/thumbnails/ImageInfo;Lcom/mfluent/asp/util/bitmap/ImageWorker$a;Lcom/mfluent/asp/util/bitmap/ImageWorker$MemoryCheck;I)V

    .line 252
    :goto_8
    invoke-virtual {v5, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 254
    sget-object v2, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 239
    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    .line 250
    :cond_c
    :try_start_3
    sget-object v3, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->a:Lorg/slf4j/Logger;

    const-string v6, "Already prefetching thumbnail: {}"

    invoke-interface {v3, v6, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_8

    .line 254
    :catchall_1
    move-exception v2

    sget-object v3, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    .line 258
    :cond_d
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 259
    sget-object v4, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 261
    :try_start_4
    sget-object v4, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->d:Ljava/util/HashMap;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_e

    .line 262
    sget-object v4, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->a:Lorg/slf4j/Logger;

    const-string v5, "Cancelling thumbnail prefetch for: {}"

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 263
    iget-object v4, p0, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->b:Lcom/mfluent/asp/util/bitmap/ImageWorker;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/util/bitmap/ImageWorker$a;

    invoke-static {v2}, Lcom/mfluent/asp/util/bitmap/ImageWorker;->a(Lcom/mfluent/asp/util/bitmap/ImageWorker$a;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 266
    :cond_e
    sget-object v2, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_9

    :catchall_2
    move-exception v2

    sget-object v3, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    :cond_f
    move v2, v9

    goto/16 :goto_1
.end method
