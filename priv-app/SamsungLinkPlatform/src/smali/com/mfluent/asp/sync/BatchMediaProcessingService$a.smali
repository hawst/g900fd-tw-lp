.class public final Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/sync/BatchMediaProcessingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;JILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 318
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0xfa

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->e:Ljava/util/HashSet;

    .line 322
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->a:Landroid/content/Context;

    .line 323
    iput p4, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->b:I

    .line 324
    iput-object p5, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->c:Ljava/lang/String;

    .line 325
    iput-wide p2, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->d:J

    .line 326
    iput-object p6, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->f:Ljava/lang/String;

    .line 327
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 340
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 366
    :goto_0
    return-void

    .line 344
    :cond_0
    new-instance v4, Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->f:Ljava/lang/String;

    invoke-direct {v4, v0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 345
    const/4 v1, 0x0

    .line 346
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->e:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->e:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move v2, v3

    .line 347
    :goto_1
    array-length v5, v0

    if-ge v2, v5, :cond_3

    .line 348
    array-length v5, v0

    sub-int/2addr v5, v2

    const/16 v6, 0xfa

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 349
    if-eqz v1, :cond_1

    array-length v6, v1

    if-eq v6, v5, :cond_2

    .line 350
    :cond_1
    new-array v1, v5, [Ljava/lang/String;

    .line 352
    :cond_2
    invoke-static {v0, v2, v1, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 354
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 356
    iget-object v6, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 357
    invoke-virtual {v5, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 358
    const-string v6, "mfl_MEDIA_TYPE"

    iget v7, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->b:I

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 359
    const-string v6, "mfl_DEVICE_ID"

    iget-wide v8, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->d:J

    invoke-virtual {v5, v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 360
    const-string v6, "mfl_SOURCE_MEDIA_ID_ARRAY"

    invoke-virtual {v5, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 362
    iget-object v6, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->a:Landroid/content/Context;

    invoke-virtual {v6, v5}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 347
    add-int/lit16 v2, v2, 0xfa

    goto :goto_1

    .line 365
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 330
    invoke-static {p1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$a;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 333
    :cond_0
    return-void
.end method
