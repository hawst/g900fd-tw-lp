.class final Lcom/mfluent/asp/sync/DeviceListSyncManager$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/sync/DeviceListSyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/DeviceListSyncManager;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/sync/DeviceListSyncManager;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager$1;->a:Lcom/mfluent/asp/sync/DeviceListSyncManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 80
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PEER_INFO_EXTRA"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/seclib/PeerInfo;

    .line 81
    invoke-virtual {v0}, Lcom/msc/seclib/PeerInfo;->getPeer_id()Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/t;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 83
    if-nez v1, :cond_0

    .line 84
    invoke-static {p1}, Lcom/mfluent/asp/datamodel/t$a;->a(Landroid/content/Context;)Lcom/mfluent/asp/datamodel/t$a;

    move-result-object v1

    .line 85
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/t$a;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 86
    invoke-static {}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::peerStatusChangedListener: Received status change for blacklisted peer ID: {}. Skipping sync."

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    invoke-static {}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "::peerStatusChangedListener: A new device may have been registered - peer ID: {}"

    invoke-interface {v2, v3, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 91
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/t$a;->a(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager$1;->a:Lcom/mfluent/asp/sync/DeviceListSyncManager;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "SYNC_ON_NEW_DEVICE_REGISTERED_INTENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->e(Landroid/content/Intent;)V

    goto :goto_0
.end method
