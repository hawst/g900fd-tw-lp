.class public Lcom/mfluent/asp/sync/CloudStorageSyncEnablerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/mfluent/asp/sync/CloudStorageSyncEnablerReceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/CloudStorageSyncEnablerReceiver;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 17
    sget-object v0, Lcom/mfluent/asp/sync/CloudStorageSyncEnablerReceiver;->a:Lorg/slf4j/Logger;

    const-string v1, "Got intent {}"

    invoke-interface {v0, v1, p2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 18
    invoke-static {p1}, Lcom/mfluent/asp/sync/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/sync/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/d;->b()V

    .line 19
    return-void
.end method
