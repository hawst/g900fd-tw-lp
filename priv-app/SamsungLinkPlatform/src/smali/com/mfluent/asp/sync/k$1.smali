.class final Lcom/mfluent/asp/sync/k$1;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/sync/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/k;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/sync/k;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/mfluent/asp/sync/k$1;->a:Lcom/mfluent/asp/sync/k;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 78
    invoke-static {}, Lcom/mfluent/asp/sync/k;->d()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "masterReset"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/mfluent/asp/sync/k$1;->a:Lcom/mfluent/asp/sync/k;

    invoke-virtual {v0, p2}, Lcom/mfluent/asp/sync/k;->g(Landroid/content/Intent;)V

    .line 82
    iget-object v0, p0, Lcom/mfluent/asp/sync/k$1;->a:Lcom/mfluent/asp/sync/k;

    invoke-static {v0}, Lcom/mfluent/asp/sync/k;->a(Lcom/mfluent/asp/sync/k;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 83
    iget-object v0, p0, Lcom/mfluent/asp/sync/k$1;->a:Lcom/mfluent/asp/sync/k;

    invoke-static {v0}, Lcom/mfluent/asp/sync/k;->b(Lcom/mfluent/asp/sync/k;)Lcom/mfluent/asp/sync/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/e;->c()V

    .line 85
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/k$1;->a:Lcom/mfluent/asp/sync/k;

    invoke-static {v1}, Lcom/mfluent/asp/sync/k;->c(Lcom/mfluent/asp/sync/k;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/mfluent/asp/ASPApplication;->d:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 88
    return-void
.end method
