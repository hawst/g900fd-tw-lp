.class final Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/sync/BatchMediaProcessingService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/sync/BatchMediaProcessingService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 93
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->b(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 94
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->c(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    iget-object v1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v1}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->d(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Ljava/util/ArrayList;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 101
    :try_start_2
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v1}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    :try_start_3
    const-string v1, "mfl_BatchMediaProcessingService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v3}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->e(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "::processNextBatch uncaught throwable."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 101
    :try_start_4
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    goto :goto_0

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v1}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 106
    :cond_1
    :try_start_5
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->f(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    const-wide/16 v2, 0x7530

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    const-string v0, "mfl_BatchMediaProcessingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v2}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->e(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " calling stopSelf due to no activity for 30 seconds."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    iget-object v1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v1}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->g(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->stopSelfResult(I)Z
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 112
    :catch_1
    move-exception v0

    goto/16 :goto_0

    .line 116
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$1;->a:Lcom/mfluent/asp/sync/BatchMediaProcessingService;

    invoke-static {v0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->a(Lcom/mfluent/asp/sync/BatchMediaProcessingService;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 117
    return-void
.end method
