.class public Lcom/mfluent/asp/sync/LocalMediaReverseGeoLocStarterService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeoLocStarterService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeoLocStarterService;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeoLocStarterService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 23
    new-instance v1, Landroid/content/Intent;

    sget-object v0, Lcom/mfluent/asp/ASPApplication;->f:Ljava/lang/String;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeoLocStarterService;->a:Lorg/slf4j/Logger;

    const-string v2, "::startLocalDeviceSync() intent:{}"

    invoke-interface {v0, v2, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    const-class v0, Lcom/mfluent/asp/sync/g;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/mfluent/asp/sync/g;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/g;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/sync/g;->f(Landroid/content/Intent;)V

    .line 24
    :cond_0
    return-void
.end method
