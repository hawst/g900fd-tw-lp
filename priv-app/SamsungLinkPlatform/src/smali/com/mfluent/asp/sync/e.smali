.class public Lcom/mfluent/asp/sync/e;
.super Lcom/mfluent/asp/sync/j;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final b:J

.field private static final e:J

.field private static final f:J


# instance fields
.field private g:Z

.field private h:J

.field private i:Z

.field private j:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 60
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 62
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 64
    sput-wide v0, Lcom/mfluent/asp/sync/e;->b:J

    sput-wide v0, Lcom/mfluent/asp/sync/e;->e:J

    .line 66
    sget-wide v0, Lcom/mfluent/asp/sync/e;->b:J

    sput-wide v0, Lcom/mfluent/asp/sync/e;->f:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 97
    invoke-direct {p0, p1}, Lcom/mfluent/asp/sync/j;-><init>(Landroid/content/Context;)V

    .line 79
    iput-boolean v2, p0, Lcom/mfluent/asp/sync/e;->g:Z

    .line 81
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/e;->h:J

    .line 91
    iput-boolean v2, p0, Lcom/mfluent/asp/sync/e;->i:Z

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/sync/e;->j:Ljava/util/Timer;

    .line 98
    return-void
.end method

.method private a(Z)V
    .locals 7

    .prologue
    .line 598
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/e;->g:Z

    .line 599
    if-eqz p1, :cond_1

    .line 600
    const/4 v0, 0x1

    move v1, v0

    .line 603
    :goto_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/e;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v0

    .line 604
    invoke-virtual {v0}, Lcom/mfluent/asp/b/g;->e()Ljava/util/List;

    move-result-object v2

    .line 606
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 607
    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getCacheDir()Ljava/io/File;

    move-result-object v3

    .line 609
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/b/h;

    .line 611
    iget-object v4, p0, Lcom/mfluent/asp/sync/e;->c:Landroid/content/Context;

    invoke-static {v4}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Landroid/content/Context;)Lcom/mfluent/asp/ui/StorageTypeHelper;

    move-result-object v4

    .line 612
    sget-object v5, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->a:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6, v1}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    .line 613
    sget-object v5, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->e:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6, v1}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    .line 615
    sget-object v5, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->f:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6, v1}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    .line 617
    sget-object v5, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->b:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6, v1}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    .line 618
    sget-object v5, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->c:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6, v1}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    .line 620
    sget-object v5, Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;->d:Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;

    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v3, v5, v0, v1}, Lcom/mfluent/asp/ui/StorageTypeHelper;->a(Ljava/io/File;Lcom/mfluent/asp/ui/StorageTypeHelper$ImgType;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    goto :goto_1

    .line 623
    :cond_0
    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method private static a(Lcom/mfluent/asp/b/h;)Z
    .locals 14

    .prologue
    const/4 v3, 0x0

    const/4 v12, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 450
    invoke-static {}, Lcom/mfluent/asp/util/s;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 451
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 452
    invoke-static {p0, v0}, Lcom/mfluent/asp/cloudstorage/a;->b(Lcom/mfluent/asp/b/h;Landroid/content/Context;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    .line 454
    if-eqz v0, :cond_0

    .line 528
    :goto_0
    return v1

    :cond_0
    move v1, v2

    .line 454
    goto :goto_0

    .line 461
    :cond_1
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 462
    new-instance v8, Ljava/io/File;

    const-string v4, "cloudplugins_dl"

    invoke-virtual {v0, v4, v2}, Lcom/mfluent/asp/ASPApplication;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v4

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->i()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v8, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 465
    :try_start_0
    sget-object v4, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    if-gt v4, v12, :cond_2

    .line 466
    const-string v4, "mfl_CloudStorageTypeListSyncManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Loading remote jar: ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :cond_2
    new-instance v4, Ljava/net/URL;

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->h()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v4

    .line 470
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-direct {v5, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 471
    :try_start_1
    new-instance v4, Ljava/io/BufferedOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 473
    const/16 v3, 0x2000

    :try_start_2
    new-array v3, v3, [B

    .line 475
    const-wide/16 v6, 0x0

    .line 476
    const-string v9, "MD5"

    invoke-static {v9}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v9

    .line 478
    :goto_1
    const/4 v10, 0x0

    const/16 v11, 0x2000

    invoke-virtual {v5, v3, v10, v11}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v10

    if-lez v10, :cond_3

    .line 479
    const/4 v11, 0x0

    invoke-virtual {v4, v3, v11, v10}, Ljava/io/OutputStream;->write([BII)V

    .line 480
    const/4 v11, 0x0

    invoke-virtual {v9, v3, v11, v10}, Ljava/security/MessageDigest;->update([BII)V

    .line 481
    int-to-long v10, v10

    add-long/2addr v6, v10

    goto :goto_1

    .line 483
    :cond_3
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 484
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V

    .line 485
    sget-object v3, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v12, :cond_4

    .line 486
    const-string v3, "mfl_CloudStorageTypeListSyncManager"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Stored Temp Cloud plugin: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    :cond_4
    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->k()J

    move-result-wide v10

    cmp-long v3, v6, v10

    if-eqz v3, :cond_7

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->k()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v3, v10, v12

    if-eqz v3, :cond_7

    .line 497
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Downloaded filesize does not match. Downloaded: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " expected:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->k()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 515
    :catch_0
    move-exception v0

    move-object v1, v4

    move-object v3, v5

    .line 516
    :goto_2
    :try_start_3
    sget-object v4, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v5, 0x6

    if-gt v4, v5, :cond_5

    .line 517
    const-string v4, "mfl_CloudStorageTypeListSyncManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Failed to download ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 520
    :cond_5
    invoke-static {v3}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 521
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    move v0, v2

    .line 524
    :goto_3
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 525
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_6
    move v1, v0

    .line 528
    goto/16 :goto_0

    .line 500
    :cond_7
    :try_start_4
    invoke-virtual {v9}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    invoke-static {v3}, Lcom/mfluent/asp/util/k;->a([B)Ljava/lang/String;

    move-result-object v3

    .line 501
    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lorg/apache/commons/lang3/StringUtils;->equalsIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 502
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "MD5 checksum for downloaded JAR is invalid. downloaded:"

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " expected:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 520
    :catchall_0
    move-exception v0

    :goto_4
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 521
    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    throw v0

    .line 505
    :cond_8
    :try_start_5
    new-instance v3, Ljava/io/File;

    const-string v6, "cloudplugins"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Lcom/mfluent/asp/ASPApplication;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mfluent/asp/b/h;->i()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v0, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 507
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_9

    .line 508
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "Could not delete file to move temp file over. File:"

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 511
    :cond_9
    invoke-static {v8, v3}, Lorg/apache/commons/io/FileUtils;->moveFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 520
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 521
    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    move v0, v1

    .line 522
    goto/16 :goto_3

    .line 520
    :catchall_1
    move-exception v0

    move-object v4, v3

    move-object v5, v3

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v4, v3

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v4, v1

    move-object v5, v3

    goto :goto_4

    .line 515
    :catch_1
    move-exception v0

    move-object v1, v3

    goto/16 :goto_2

    :catch_2
    move-exception v0

    move-object v1, v3

    move-object v3, v5

    goto/16 :goto_2
.end method

.method static synthetic d()Z
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Lcom/mfluent/asp/sync/e;->m()Z

    move-result v0

    return v0
.end method

.method static synthetic e()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method

.method private f()V
    .locals 11

    .prologue
    const/4 v10, 0x6

    const/4 v0, 0x0

    .line 251
    :try_start_0
    iget-object v1, p0, Lcom/mfluent/asp/sync/e;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v3

    .line 253
    const-string v1, "CatalogHost"

    const-string v2, "http://storage.allshareplay.com"

    invoke-virtual {v3, v1, v2}, Lcom/mfluent/asp/b/g;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 255
    const-string v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 256
    new-instance v2, Ljava/net/URL;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/jar_collection/android/plugins/v43/cloudPlugins_ui18.lst"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 257
    invoke-static {}, Lcom/mfluent/asp/util/l;->a()Lcom/mfluent/asp/util/l;

    invoke-static {v2}, Lcom/mfluent/asp/util/l;->a(Ljava/net/URL;)Lorg/json/JSONObject;

    move-result-object v1

    move-object v2, v1

    .line 264
    :goto_0
    const-string v1, "Items"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    move v1, v0

    .line 265
    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-ge v1, v0, :cond_3

    .line 267
    :try_start_1
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 269
    const-string v5, "SpName"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 270
    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 271
    sget-object v0, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v10, :cond_0

    .line 272
    const-string v0, "mfl_CloudStorageTypeListSyncManager"

    const-string v5, "::syncStorageList:Did not expect an empty provider here."

    invoke-static {v0, v5}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 265
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 259
    :cond_1
    :try_start_2
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 260
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {v2}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 261
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->toString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 262
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-object v2, v1

    goto :goto_0

    .line 276
    :cond_2
    :try_start_3
    invoke-virtual {v3, v5}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v6

    .line 278
    if-nez v6, :cond_4

    .line 282
    sget-object v0, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v6, 0x3

    if-gt v0, v6, :cond_0

    .line 283
    const-string v0, "mfl_CloudStorageTypeListSyncManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::syncStorageList:Will not load: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 331
    :catch_0
    move-exception v0

    .line 332
    :try_start_4
    sget-object v5, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v5

    if-gt v5, v10, :cond_0

    .line 333
    const-string v5, "mfl_CloudStorageTypeListSyncManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::syncStorageList:Failed up update plugin information because: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " json("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 341
    :catch_1
    move-exception v0

    .line 342
    const-string v1, "mfl_CloudStorageTypeListSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trouble syncing web storage list because "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    :cond_3
    return-void

    .line 288
    :cond_4
    const/16 v5, 0x2b

    :try_start_5
    invoke-virtual {v6, v5}, Lcom/mfluent/asp/b/h;->c(I)V

    .line 290
    const-string v5, "mainClass"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 291
    invoke-virtual {v6, v5}, Lcom/mfluent/asp/b/h;->d(Ljava/lang/String;)V

    .line 293
    const-string v5, "download"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 294
    invoke-virtual {v6, v5}, Lcom/mfluent/asp/b/h;->e(Ljava/lang/String;)V

    .line 296
    const-string v5, "jarName"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 297
    invoke-virtual {v6, v5}, Lcom/mfluent/asp/b/h;->f(Ljava/lang/String;)V

    .line 299
    const-string v5, "ver"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 300
    invoke-virtual {v6, v5}, Lcom/mfluent/asp/b/h;->h(Ljava/lang/String;)V

    .line 302
    const-string v5, "md5sum"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 303
    invoke-virtual {v6, v5}, Lcom/mfluent/asp/b/h;->g(Ljava/lang/String;)V

    .line 305
    const-string v5, "filesize"

    invoke-virtual {v6}, Lcom/mfluent/asp/b/h;->k()J

    move-result-wide v8

    invoke-virtual {v0, v5, v8, v9}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 306
    invoke-virtual {v6, v8, v9}, Lcom/mfluent/asp/b/h;->a(J)V

    .line 308
    const-string v5, "iconSmall"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 309
    invoke-virtual {v6, v5}, Lcom/mfluent/asp/b/h;->k(Ljava/lang/String;)V

    .line 311
    const-string v5, "iconSmallWhiteTheme"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 312
    invoke-virtual {v6, v5}, Lcom/mfluent/asp/b/h;->l(Ljava/lang/String;)V

    .line 314
    const-string v5, "iconSmallBlackTheme"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 315
    invoke-virtual {v6, v5}, Lcom/mfluent/asp/b/h;->m(Ljava/lang/String;)V

    .line 317
    const-string v5, "iconMed"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 318
    invoke-virtual {v6, v5}, Lcom/mfluent/asp/b/h;->n(Ljava/lang/String;)V

    .line 320
    const-string v5, "iconLarge"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 321
    invoke-virtual {v6, v5}, Lcom/mfluent/asp/b/h;->o(Ljava/lang/String;)V

    .line 323
    const-string v5, "logo"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 324
    invoke-virtual {v6, v0}, Lcom/mfluent/asp/b/h;->p(Ljava/lang/String;)V

    .line 326
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Lcom/mfluent/asp/b/h;->c(Z)V

    .line 330
    invoke-virtual {v3, v6}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_2
.end method

.method private g()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v3, 0x0

    .line 359
    iget-object v0, p0, Lcom/mfluent/asp/sync/e;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v4

    .line 361
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 363
    invoke-virtual {v4}, Lcom/mfluent/asp/b/g;->e()Ljava/util/List;

    move-result-object v1

    .line 364
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/b/h;

    .line 372
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->l()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->w()I

    move-result v6

    if-nez v6, :cond_2

    .line 373
    :cond_1
    sget-object v6, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v6

    const/4 v7, 0x6

    if-gt v6, v7, :cond_0

    .line 374
    const-string v6, "mfl_CloudStorageTypeListSyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "::syncJarCache: error provider had no version information from server."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 379
    :cond_2
    new-instance v6, Ljava/io/File;

    const-string v7, "cloudplugins"

    invoke-virtual {v0, v7, v3}, Lcom/mfluent/asp/ASPApplication;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->i()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 380
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    .line 382
    if-eqz v6, :cond_3

    const-string v6, "0"

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->w()I

    move-result v6

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->v()I

    move-result v7

    if-eq v6, v7, :cond_0

    .line 387
    :cond_3
    invoke-static {v1}, Lcom/mfluent/asp/sync/e;->a(Lcom/mfluent/asp/b/h;)Z

    move-result v6

    .line 388
    if-eqz v6, :cond_0

    .line 389
    sget-object v6, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v6

    if-gt v6, v9, :cond_4

    .line 390
    const-string v6, "mfl_CloudStorageTypeListSyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "::syncJarCache: upgraded "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->m()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    :cond_4
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->m()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-static {v1, v0}, Lcom/mfluent/asp/cloudstorage/a;->a(Lcom/mfluent/asp/b/h;Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 402
    const/4 v2, 0x1

    .line 405
    :cond_5
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/mfluent/asp/b/h;->i(Ljava/lang/String;)V

    .line 406
    invoke-virtual {v1}, Lcom/mfluent/asp/b/h;->w()I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/mfluent/asp/b/h;->b(I)V

    .line 407
    invoke-virtual {v4, v1}, Lcom/mfluent/asp/b/g;->a(Lcom/mfluent/asp/b/h;)V

    goto/16 :goto_0

    .line 414
    :cond_6
    invoke-static {}, Lcom/mfluent/asp/util/s;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 419
    :goto_1
    if-eqz v3, :cond_8

    .line 420
    sget-object v0, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v9, :cond_7

    .line 421
    const-string v0, "mfl_CloudStorageTypeListSyncManager"

    const-string v1, "::syncJarCache:Storage Plugin Upgraded. Restarting SyncManger."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    :cond_7
    invoke-direct {p0}, Lcom/mfluent/asp/sync/e;->h()V

    .line 426
    :cond_8
    return-void

    :cond_9
    move v3, v2

    goto :goto_1
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 431
    invoke-static {}, Lcom/mfluent/asp/sync/e;->m()Z

    move-result v0

    if-nez v0, :cond_1

    .line 432
    sget-object v0, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 433
    const-string v0, "mfl_CloudStorageTypeListSyncManager"

    const-string v1, "::checkDoUpgrade:"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :cond_0
    iput-boolean v2, p0, Lcom/mfluent/asp/sync/e;->g:Z

    .line 438
    invoke-direct {p0, v2}, Lcom/mfluent/asp/sync/e;->a(Z)V

    .line 441
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 447
    :goto_0
    return-void

    .line 445
    :cond_1
    iput-boolean v2, p0, Lcom/mfluent/asp/sync/e;->i:Z

    goto :goto_0
.end method

.method private i()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 533
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    .line 535
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 536
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 537
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/e;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v0

    .line 541
    invoke-virtual {v0}, Lcom/mfluent/asp/b/g;->c()Ljava/util/List;

    move-result-object v0

    .line 543
    sget-object v1, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v4, 0x3

    if-gt v1, v4, :cond_1

    .line 544
    const-string v1, "mfl_CloudStorageTypeListSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::syncDeviceList: Found "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " logged in cloud services."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/b/h;

    .line 548
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/mfluent/asp/datamodel/t;->c(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    .line 549
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    if-nez v1, :cond_4

    .line 552
    new-instance v1, Lcom/mfluent/asp/datamodel/Device;

    iget-object v5, p0, Lcom/mfluent/asp/sync/e;->c:Landroid/content/Context;

    invoke-direct {v1, v5}, Lcom/mfluent/asp/datamodel/Device;-><init>(Landroid/content/Context;)V

    .line 553
    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1, v5}, Lcom/mfluent/asp/datamodel/Device;->b(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V

    .line 554
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 555
    sget-object v5, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v5

    const/4 v6, 0x6

    if-gt v5, v6, :cond_3

    .line 556
    const-string v5, "mfl_CloudStorageTypeListSyncManager"

    const-string v6, "::syncDeviceList:Storage type should not be null here."

    invoke-static {v5, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    :cond_3
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->m()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 562
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/mfluent/asp/datamodel/Device;->i(Ljava/lang/String;)V

    .line 566
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    .line 569
    :cond_4
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    .line 571
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->isWebStorageSignedIn()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v5

    if-gtz v5, :cond_5

    .line 572
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lcom/mfluent/asp/datamodel/Device;->setWebStorageSignedIn(Z)V

    .line 575
    :cond_5
    invoke-virtual {v0}, Lcom/mfluent/asp/b/h;->x()I

    move-result v0

    .line 576
    invoke-virtual {v1, v0}, Lcom/mfluent/asp/datamodel/Device;->c(I)V

    .line 578
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 579
    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->s()V

    goto/16 :goto_1

    .line 583
    :cond_6
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 584
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->E()Z

    move-result v2

    if-nez v2, :cond_7

    .line 585
    sget-object v2, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x4

    if-gt v2, v3, :cond_8

    .line 586
    const-string v2, "mfl_CloudStorageTypeListSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " ::syncDeviceList: Removing: storage device: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    :cond_8
    new-instance v2, Lcom/mfluent/asp/ui/StorageSignInOutHelper;

    iget-object v3, p0, Lcom/mfluent/asp/sync/e;->c:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/ui/StorageSignInOutHelper;-><init>(Landroid/content/Context;)V

    .line 590
    invoke-virtual {v2, v0}, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->a(Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_2

    .line 594
    :cond_9
    return-void
.end method

.method private static m()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 626
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 628
    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 629
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    .line 630
    if-nez v1, :cond_0

    move v0, v2

    .line 639
    :goto_0
    return v0

    .line 633
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 634
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 635
    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v5, 0x64

    if-ne v4, v5, :cond_1

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 636
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 639
    goto :goto_0
.end method

.method private n()V
    .locals 6

    .prologue
    .line 648
    invoke-direct {p0}, Lcom/mfluent/asp/sync/e;->o()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 649
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 650
    iget-wide v2, p0, Lcom/mfluent/asp/sync/e;->h:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 651
    const-string v1, "com.mfluent.asp.sync.CloudStorageTypeListSyncManager.PREVIOUS_SYNC_TIME"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 655
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 656
    return-void

    .line 653
    :cond_0
    const-string v1, "com.mfluent.asp.sync.CloudStorageTypeListSyncManager.PREVIOUS_SYNC_TIME"

    iget-wide v2, p0, Lcom/mfluent/asp/sync/e;->h:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method private o()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 664
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 665
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 666
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 667
    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    invoke-super {p0}, Lcom/mfluent/asp/sync/j;->a()Ljava/util/Collection;

    move-result-object v0

    .line 211
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.DataModel.REFRESH_ALL"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 212
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/mfluent/asp/ASPApplication;->e:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 213
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/mfluent/asp/ui/StorageSignInOutHelper;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 214
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.AccessManager.BROADCAST_NEW_ACCOUNT"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 216
    return-object v0
.end method

.method protected final a(Landroid/content/Intent;)V
    .locals 11

    .prologue
    const-wide/16 v6, 0x0

    const/4 v10, 0x6

    const/4 v9, 0x0

    const/4 v8, 0x4

    .line 140
    iget-object v0, p0, Lcom/mfluent/asp/sync/e;->j:Ljava/util/Timer;

    if-nez v0, :cond_1

    sget-object v0, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_0

    const-string v0, "mfl_CloudStorageTypeListSyncManager"

    const-string v1, "::startUpgradeMonitor:  init cloud upgrade timer"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/e;->j:Ljava/util/Timer;

    new-instance v1, Lcom/mfluent/asp/sync/e$1;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/sync/e$1;-><init>(Lcom/mfluent/asp/sync/e;)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/e;->j:Ljava/util/Timer;

    const-wide/16 v2, 0x3e8

    sget-wide v4, Lcom/mfluent/asp/sync/e;->f:J

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 141
    :cond_1
    iget-wide v0, p0, Lcom/mfluent/asp/sync/e;->h:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_2

    .line 142
    invoke-direct {p0}, Lcom/mfluent/asp/sync/e;->o()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "com.mfluent.asp.sync.CloudStorageTypeListSyncManager.PREVIOUS_SYNC_TIME"

    invoke-interface {v0, v1, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/e;->h:J

    .line 144
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/mfluent/asp/sync/e;->h:J

    sub-long v2, v0, v2

    .line 145
    const-string v0, "REFRESH_FROM_KEY"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 146
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1e

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 148
    const-string v5, "com.mfluent.asp.DataModel.REFRESH_ALL"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 150
    const/4 v5, 0x5

    if-eq v4, v5, :cond_3

    if-eq v4, v10, :cond_3

    const/4 v5, 0x7

    if-eq v4, v5, :cond_3

    const/16 v5, 0x8

    if-eq v4, v5, :cond_3

    .line 155
    sget-wide v0, Lcom/mfluent/asp/sync/e;->b:J

    .line 159
    :cond_3
    cmp-long v5, v2, v0

    if-gez v5, :cond_5

    .line 160
    sget-object v5, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v5

    const/4 v6, 0x3

    if-gt v5, v6, :cond_4

    .line 161
    const-string v5, "mfl_CloudStorageTypeListSyncManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::doSync: trying to sync too soon. Canceling sync. reason: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", sinceLastSync: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_4
    :goto_0
    return-void

    .line 173
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 174
    sget-object v0, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_6

    .line 175
    const-string v0, "mfl_CloudStorageTypeListSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "::doSync: start sync. Reason: "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_6
    iget-object v0, p0, Lcom/mfluent/asp/sync/e;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/b/e;->b()V

    .line 180
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    invoke-direct {p0}, Lcom/mfluent/asp/sync/e;->f()V

    .line 182
    invoke-direct {p0}, Lcom/mfluent/asp/sync/e;->g()V

    .line 184
    invoke-direct {p0, v9}, Lcom/mfluent/asp/sync/e;->a(Z)V

    .line 187
    :try_start_0
    invoke-direct {p0}, Lcom/mfluent/asp/sync/e;->i()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    sget-object v0, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_7

    .line 195
    const-string v0, "mfl_CloudStorageTypeListSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "::doSync: finish sync "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :cond_7
    :goto_1
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/e;->i:Z

    if-eqz v0, :cond_8

    .line 200
    invoke-direct {p0}, Lcom/mfluent/asp/sync/e;->h()V

    .line 203
    :cond_8
    iput-boolean v9, p0, Lcom/mfluent/asp/sync/e;->g:Z

    .line 204
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/e;->h:J

    .line 205
    invoke-direct {p0}, Lcom/mfluent/asp/sync/e;->n()V

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    :try_start_1
    sget-object v1, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v10, :cond_9

    .line 191
    const-string v1, "mfl_CloudStorageTypeListSyncManager"

    const-string v4, "::doSync: Failed to syncDeviceList()"

    invoke-static {v1, v4, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    :cond_9
    sget-object v0, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_7

    .line 195
    const-string v0, "mfl_CloudStorageTypeListSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "::doSync: finish sync "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 194
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/sync/e;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v8, :cond_a

    .line 195
    const-string v1, "mfl_CloudStorageTypeListSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::doSync: finish sync "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    throw v0
.end method

.method protected final b()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 643
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/e;->h:J

    .line 644
    invoke-direct {p0}, Lcom/mfluent/asp/sync/e;->n()V

    .line 645
    return-void
.end method

.method protected final d(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 221
    invoke-super {p0, p1}, Lcom/mfluent/asp/sync/j;->d(Landroid/content/Intent;)V

    .line 223
    iget-object v0, p0, Lcom/mfluent/asp/sync/e;->j:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/mfluent/asp/sync/e;->j:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/sync/e;->j:Ljava/util/Timer;

    .line 227
    :cond_0
    return-void
.end method
