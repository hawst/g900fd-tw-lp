.class public Lcom/mfluent/asp/sync/LocalMediaDigestService;
.super Lcom/mfluent/asp/sync/BatchMediaProcessingService;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException;
    }
.end annotation


# static fields
.field public static a:Z

.field private static final b:Lorg/slf4j/Logger;


# instance fields
.field private final c:Ljava/lang/Object;

.field private final d:J

.field private final e:Ljava/security/MessageDigest;

.field private final f:[B

.field private final g:Landroid/content/ContentValues;

.field private h:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;

.field private i:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

.field private j:Z

.field private k:J

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->b:Lorg/slf4j/Logger;

    .line 57
    const/4 v0, 0x1

    sput-boolean v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    const-string v0, "LocalMediaDigestService"

    invoke-direct {p0, v0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;-><init>(Ljava/lang/String;)V

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->c:Ljava/lang/Object;

    .line 45
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->d:J

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->j:Z

    .line 63
    const/4 v0, 0x0

    .line 65
    :try_start_0
    const-string v1, "SHA-1"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 69
    :goto_0
    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->e:Ljava/security/MessageDigest;

    .line 70
    const/high16 v0, 0x40000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->f:[B

    .line 71
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    .line 72
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    return-void
.end method

.method protected final a(Ljava/util/ArrayList;Landroid/database/Cursor;)V
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 191
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 193
    :try_start_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->c:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->c:Ljava/lang/Object;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, Ljava/lang/Object;->wait(J)V

    .line 195
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaDigestService;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 327
    :goto_1
    return-void

    .line 195
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5

    throw v4
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 198
    :catch_0
    move-exception v4

    goto :goto_0

    .line 210
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 211
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->e:Ljava/security/MessageDigest;

    invoke-virtual {v4}, Ljava/security/MessageDigest;->reset()V

    .line 212
    const-string v4, "_id"

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v14

    .line 213
    const/4 v5, 0x0

    .line 216
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    move-object/from16 v0, p2

    invoke-static {v0, v4}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 218
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    const-string v6, "_id"

    invoke-virtual {v4, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 220
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    const-string v6, "_data"

    invoke-virtual {v4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 221
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    const-string v7, "_size"

    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 222
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    const-string v7, "file_digest"

    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 224
    invoke-static {v6}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 225
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 227
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    .line 229
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 230
    new-instance v4, Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "File does not exist: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3
    .catch Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 321
    :catch_1
    move-exception v4

    .line 322
    :goto_2
    :try_start_4
    sget-object v6, Lcom/mfluent/asp/sync/LocalMediaDigestService;->b:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Skipping file hash for id:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ". "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 326
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    goto/16 :goto_1

    .line 231
    :cond_1
    :try_start_5
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-gtz v7, :cond_2

    .line 232
    new-instance v4, Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "File length is 0: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_5
    .catch Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 323
    :catch_2
    move-exception v4

    .line 324
    :goto_3
    :try_start_6
    sget-object v6, Lcom/mfluent/asp/sync/LocalMediaDigestService;->b:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Failed to produce and set file hash for id:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 326
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    goto/16 :goto_1

    .line 233
    :cond_2
    :try_start_7
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v8

    cmp-long v7, v16, v8

    if-eqz v7, :cond_3

    const-wide/16 v8, 0x0

    cmp-long v7, v16, v8

    if-eqz v7, :cond_3

    .line 234
    new-instance v7, Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Filesize does not match! Expected: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v16

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " actual: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " path:"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v4}, Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_7
    .catch Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 326
    :catchall_1
    move-exception v4

    :goto_4
    invoke-static {v5}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    throw v4

    .line 236
    :cond_3
    :try_start_8
    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v20

    .line 237
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 238
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    const-string v7, "file_digest_time"

    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    .line 239
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    const-string v10, "file_digest_size"

    invoke-virtual {v7, v10}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    .line 241
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v4, v20, v10

    if-nez v4, :cond_4

    if-eqz v7, :cond_4

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v4, v10, v8

    if-nez v4, :cond_4

    .line 242
    new-instance v4, Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "No need to re-calculate digest. Skipping file: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 245
    :cond_4
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_8
    .catch Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 247
    const-wide/16 v12, 0x0

    .line 248
    const-wide/16 v10, 0x0

    .line 249
    const/4 v5, 0x0

    .line 253
    :try_start_9
    sget-boolean v6, Lcom/mfluent/asp/sync/LocalMediaDigestService;->a:Z

    if-eqz v6, :cond_6

    const-wide/32 v6, 0x80000

    cmp-long v6, v8, v6

    if-lez v6, :cond_6

    .line 254
    const-wide/32 v6, 0x80000

    .line 258
    :cond_5
    :goto_5
    cmp-long v22, v12, v6

    if-gez v22, :cond_7

    if-ltz v5, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaDigestService;->a()Z

    move-result v5

    if-nez v5, :cond_7

    .line 259
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->f:[B

    const/16 v22, 0x0

    sub-long v24, v6, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->f:[B

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v26, v0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v24

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v4, v5, v0, v1}, Ljava/io/FileInputStream;->read([BII)I

    move-result v5

    .line 260
    if-lez v5, :cond_5

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->e:Ljava/security/MessageDigest;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->f:[B

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2, v5}, Ljava/security/MessageDigest;->update([BII)V

    .line 262
    int-to-long v0, v5

    move-wide/from16 v22, v0

    add-long v12, v12, v22

    .line 263
    int-to-long v0, v5

    move-wide/from16 v22, v0

    add-long v10, v10, v22

    .line 264
    const-wide/32 v22, 0x40000

    cmp-long v22, v10, v22

    if-ltz v22, :cond_5

    .line 265
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 266
    const-wide/16 v10, 0x0

    goto :goto_5

    :cond_6
    move-wide v6, v8

    .line 256
    goto :goto_5

    .line 270
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaDigestService;->a()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 271
    sget-object v5, Lcom/mfluent/asp/sync/LocalMediaDigestService;->b:Lorg/slf4j/Logger;

    const-string v6, "Service is destroyed. Bailing out."

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V
    :try_end_9
    .catch Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 326
    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    goto/16 :goto_1

    .line 275
    :cond_8
    :try_start_a
    sget-boolean v5, Lcom/mfluent/asp/sync/LocalMediaDigestService;->a:Z

    if-eqz v5, :cond_e

    .line 277
    const/16 v5, 0xa

    invoke-static {v8, v9, v5}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v5

    .line 278
    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    .line 279
    const/4 v5, 0x0

    :goto_6
    const/16 v7, 0x200

    if-ge v5, v7, :cond_a

    .line 280
    array-length v7, v6

    if-ge v5, v7, :cond_9

    .line 281
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->f:[B

    aget-byte v10, v6, v5

    aput-byte v10, v7, v5

    .line 279
    :goto_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 283
    :cond_9
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->f:[B

    const/4 v10, 0x0

    aput-byte v10, v7, v5

    goto :goto_7

    .line 321
    :catch_3
    move-exception v5

    move-object/from16 v28, v5

    move-object v5, v4

    move-object/from16 v4, v28

    goto/16 :goto_2

    .line 286
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->e:Ljava/security/MessageDigest;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->f:[B

    const/4 v7, 0x0

    const/16 v10, 0x200

    invoke-virtual {v5, v6, v7, v10}, Ljava/security/MessageDigest;->update([BII)V

    .line 292
    :cond_b
    sget-boolean v5, Lcom/mfluent/asp/sync/LocalMediaDigestService;->a:Z

    if-eqz v5, :cond_f

    .line 293
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->k:J

    add-long/2addr v6, v8

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->k:J

    .line 297
    :goto_8
    move-object/from16 v0, p0

    iget v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->l:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->l:I

    .line 299
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->e:Ljava/security/MessageDigest;

    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    invoke-static {v5}, Lcom/mfluent/asp/util/k;->a([B)Ljava/lang/String;

    move-result-object v5

    .line 300
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v6, v6, v18

    .line 302
    sget-object v10, Lcom/mfluent/asp/sync/LocalMediaDigestService;->b:Lorg/slf4j/Logger;

    const-string v11, "ID: {} size: {} time: {} ms SHA1: {}"

    const/4 v12, 0x4

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v12, v13

    const/4 v13, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    aput-object v18, v12, v13

    const/4 v13, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v12, v13

    const/4 v6, 0x3

    aput-object v5, v12, v6

    invoke-interface {v10, v11, v12}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 304
    invoke-static {v5, v15}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_d

    .line 305
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    const-string v7, "file_digest"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    const-string v6, "file_digest_size"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 307
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    const-string v6, "file_digest_time"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 308
    const-wide/16 v6, 0x0

    cmp-long v5, v16, v6

    if-nez v5, :cond_c

    .line 309
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    const-string v6, "_size"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 311
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->h:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;

    int-to-long v6, v14

    invoke-interface {v5, v6, v7}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetEntryUri(J)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    invoke-virtual {v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->j:Z

    .line 316
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->i:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->g:Landroid/content/ContentValues;

    const-string v7, "source_media_id"

    invoke-virtual {v6, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mfluent/asp/datamodel/al;->b(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 326
    :cond_d
    :goto_9
    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    goto/16 :goto_1

    .line 287
    :cond_e
    cmp-long v5, v12, v8

    if-gez v5, :cond_b

    .line 288
    :try_start_b
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to digest in entire file! Expected:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v16

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " read:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 323
    :catch_4
    move-exception v5

    move-object/from16 v28, v5

    move-object v5, v4

    move-object/from16 v4, v28

    goto/16 :goto_3

    .line 295
    :cond_f
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->k:J

    add-long/2addr v6, v12

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->k:J
    :try_end_b
    .catch Lcom/mfluent/asp/sync/LocalMediaDigestService$ShortCircuitException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto/16 :goto_8

    .line 326
    :catchall_2
    move-exception v5

    move-object/from16 v28, v5

    move-object v5, v4

    move-object/from16 v4, v28

    goto/16 :goto_4

    :cond_10
    move-object v4, v5

    goto :goto_9
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 76
    iget-object v1, p0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaDigestService;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 78
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    invoke-super {p0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->onDestroy()V

    .line 80
    return-void

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
