.class final Lcom/mfluent/asp/sync/CloudStorageSyncManager$a;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/sync/CloudStorageSyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/CloudStorageSyncManager;


# direct methods
.method private constructor <init>(Lcom/mfluent/asp/sync/CloudStorageSyncManager;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager$a;->a:Lcom/mfluent/asp/sync/CloudStorageSyncManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mfluent/asp/sync/CloudStorageSyncManager;B)V
    .locals 0

    .prologue
    .line 236
    invoke-direct {p0, p1}, Lcom/mfluent/asp/sync/CloudStorageSyncManager$a;-><init>(Lcom/mfluent/asp/sync/CloudStorageSyncManager;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 242
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "DEVICE_ID_EXTRA_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 243
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 244
    invoke-static {}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v4, :cond_0

    .line 245
    const-string v1, "mfl_CloudStorageSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::deviceUpdatedBroadcastReceiver:onReceive:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/mfluent/asp/common/util/IntentHelper;->intentToString(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager$a;->a:Lcom/mfluent/asp/sync/CloudStorageSyncManager;

    invoke-virtual {v1}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v1

    if-nez v1, :cond_2

    .line 249
    invoke-static {}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_1

    .line 250
    const-string v0, "mfl_CloudStorageSyncManager"

    const-string v1, "::deviceUpdatedBroadcastReceiver:onReceive: cloudStorage is null!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_1
    :goto_0
    return-void

    .line 255
    :cond_2
    iget-object v1, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager$a;->a:Lcom/mfluent/asp/sync/CloudStorageSyncManager;

    iget-object v1, v1, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 256
    invoke-static {}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v4, :cond_3

    .line 257
    const-string v0, "mfl_CloudStorageSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Notify Device("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager$a;->a:Lcom/mfluent/asp/sync/CloudStorageSyncManager;

    iget-object v2, v2, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :cond_3
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager$a;->a:Lcom/mfluent/asp/sync/CloudStorageSyncManager;

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 260
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager$a;->a:Lcom/mfluent/asp/sync/CloudStorageSyncManager;

    invoke-virtual {v0, p2}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e(Landroid/content/Intent;)V

    goto :goto_0
.end method
