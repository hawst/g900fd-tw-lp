.class public abstract Lcom/mfluent/asp/sync/j;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/sync/j$a;,
        Lcom/mfluent/asp/sync/j$b;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;


# instance fields
.field private b:Landroid/os/Handler;

.field protected final c:Landroid/content/Context;

.field protected final d:Landroid/content/BroadcastReceiver;

.field private e:Z

.field private f:I

.field private final g:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Lcom/mfluent/asp/sync/j$a;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/mfluent/asp/sync/j;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/j;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-boolean v2, p0, Lcom/mfluent/asp/sync/j;->e:Z

    .line 50
    iput v2, p0, Lcom/mfluent/asp/sync/j;->f:I

    .line 52
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/j;->g:Landroid/util/LruCache;

    .line 54
    new-instance v0, Lcom/mfluent/asp/sync/j$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/j$1;-><init>(Lcom/mfluent/asp/sync/j;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/j;->d:Landroid/content/BroadcastReceiver;

    .line 62
    iput-boolean v2, p0, Lcom/mfluent/asp/sync/j;->h:Z

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/j;->c:Landroid/content/Context;

    .line 66
    return-void
.end method

.method protected static i(Landroid/content/Intent;)I
    .locals 2

    .prologue
    .line 239
    const-string v0, "REFRESH_FROM_KEY"

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method static synthetic l()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/mfluent/asp/sync/j;->a:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method protected a()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    return-object v0
.end method

.method protected abstract a(Landroid/content/Intent;)V
.end method

.method public final declared-synchronized a(Landroid/content/Intent;J)V
    .locals 12

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/j;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/mfluent/asp/ContentAggregatorService;->a:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    :goto_0
    monitor-exit p0

    return-void

    .line 132
    :cond_0
    :try_start_1
    new-instance v2, Lcom/mfluent/asp/sync/j$a;

    invoke-direct {v2, p0, p1}, Lcom/mfluent/asp/sync/j$a;-><init>(Lcom/mfluent/asp/sync/j;Landroid/content/Intent;)V

    .line 133
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->g:Landroid/util/LruCache;

    invoke-virtual {v0, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 134
    if-nez v0, :cond_1

    .line 135
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 138
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 139
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gez v1, :cond_2

    .line 140
    sget-object v0, Lcom/mfluent/asp/sync/j;->a:Lorg/slf4j/Logger;

    const-string v1, "Ignoring sync intent {} because one is already queued"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 144
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v1, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    .line 146
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/j;->f_()J

    move-result-wide v6

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v4, v0

    sub-long v0, v6, v0

    .line 147
    cmp-long v6, v0, p2

    if-lez v6, :cond_3

    .line 148
    sget-object v6, Lcom/mfluent/asp/sync/j;->a:Lorg/slf4j/Logger;

    const-string v7, "Delaying sync intent {} because not enough time has elapsed since the last one. Original delay: {} New Delay: {}"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-interface {v6, v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    move-wide p2, v0

    .line 155
    :cond_3
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_4

    .line 156
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->g:Landroid/util/LruCache;

    add-long/2addr v4, p2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;

    invoke-virtual {v0, v3, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 163
    :goto_1
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 164
    const/4 v0, 0x0

    iput v0, p0, Lcom/mfluent/asp/sync/j;->f:I

    goto :goto_0

    .line 159
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->g:Landroid/util/LruCache;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method protected a(Lorg/apache/commons/lang3/builder/EqualsBuilder;Landroid/content/Intent;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 288
    return-void
.end method

.method protected a(Lorg/apache/commons/lang3/builder/HashCodeBuilder;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 291
    return-void
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x0

    return v0
.end method

.method protected c(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 81
    sget-object v0, Lcom/mfluent/asp/sync/j;->a:Lorg/slf4j/Logger;

    const-string v1, "Starting {}"

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 84
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/util/i;->a()Lcom/mfluent/asp/util/i;

    move-result-object v0

    new-instance v1, Lcom/mfluent/asp/sync/j$b;

    invoke-direct {v1, p0}, Lcom/mfluent/asp/sync/j$b;-><init>(Lcom/mfluent/asp/sync/j;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/util/i;->a(Landroid/os/Handler$Callback;)Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/j;->a()Ljava/util/Collection;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_0

    .line 91
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    .line 92
    iget-object v2, p0, Lcom/mfluent/asp/sync/j;->c:Landroid/content/Context;

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    iget-object v3, p0, Lcom/mfluent/asp/sync/j;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/j;->h:Z

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    .line 86
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Interrupted while waiting for handler exchange"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 97
    :cond_0
    return-void
.end method

.method protected d(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 112
    sget-object v0, Lcom/mfluent/asp/sync/j;->a:Lorg/slf4j/Logger;

    const-string v1, "Stopping {}"

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 114
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/j;->h:Z

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/j;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 120
    :cond_1
    return-void
.end method

.method public declared-synchronized e(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 123
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0, v1}, Lcom/mfluent/asp/sync/j;->a(Landroid/content/Intent;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    monitor-exit p0

    return-void

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/j;->e:Z

    if-nez v0, :cond_0

    .line 74
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/sync/j;->c(Landroid/content/Intent;)V

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/j;->e:Z

    .line 77
    :cond_0
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/sync/j;->e(Landroid/content/Intent;)V

    .line 78
    return-void
.end method

.method protected f_()J
    .locals 2

    .prologue
    .line 243
    const-wide/16 v0, 0x2710

    return-wide v0
.end method

.method public final g(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/j;->e:Z

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/sync/j;->d(Landroid/content/Intent;)V

    .line 106
    iput-boolean v1, p0, Lcom/mfluent/asp/sync/j;->e:Z

    .line 107
    iput v1, p0, Lcom/mfluent/asp/sync/j;->f:I

    .line 109
    :cond_0
    return-void
.end method

.method protected final declared-synchronized h(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 172
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->hasMessages(ILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->hasMessages(ILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/mfluent/asp/sync/j;->f:I

    if-ge v0, v3, :cond_2

    .line 173
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    .line 175
    iget v0, p0, Lcom/mfluent/asp/sync/j;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mfluent/asp/sync/j;->f:I

    .line 176
    const-wide/16 v0, 0x0

    .line 177
    :goto_0
    iget v4, p0, Lcom/mfluent/asp/sync/j;->f:I

    if-gt v2, v4, :cond_0

    .line 178
    int-to-long v4, v2

    add-long/2addr v0, v4

    .line 177
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 180
    :cond_0
    const-wide/32 v4, 0xea60

    mul-long/2addr v0, v4

    .line 182
    sget-object v2, Lcom/mfluent/asp/sync/j;->a:Lorg/slf4j/Logger;

    const-string v4, "{}::syncRetry will retry sync in {} millis."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v2, v4, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 183
    iget-object v2, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 184
    :cond_2
    :try_start_1
    iget v0, p0, Lcom/mfluent/asp/sync/j;->f:I

    if-lt v0, v3, :cond_1

    .line 185
    sget-object v0, Lcom/mfluent/asp/sync/j;->a:Lorg/slf4j/Logger;

    const-string v1, "{}::syncRetry too many retries. Giving up."

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized j()V
    .locals 1

    .prologue
    .line 168
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/mfluent/asp/sync/j;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    monitor-exit p0

    return-void

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final k()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/mfluent/asp/sync/j;->b:Landroid/os/Handler;

    return-object v0
.end method
