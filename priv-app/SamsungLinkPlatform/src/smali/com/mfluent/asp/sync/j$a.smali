.class final Lcom/mfluent/asp/sync/j$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/sync/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/j;

.field private final b:I

.field private final c:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/sync/j;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 252
    iput-object p1, p0, Lcom/mfluent/asp/sync/j$a;->a:Lcom/mfluent/asp/sync/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    const/4 v0, 0x2

    iput v0, p0, Lcom/mfluent/asp/sync/j$a;->b:I

    .line 254
    iput-object p2, p0, Lcom/mfluent/asp/sync/j$a;->c:Landroid/content/Intent;

    .line 255
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 259
    check-cast p1, Lcom/mfluent/asp/sync/j$a;

    .line 260
    iget-object v0, p1, Lcom/mfluent/asp/sync/j$a;->c:Landroid/content/Intent;

    .line 262
    new-instance v1, Lorg/apache/commons/lang3/builder/EqualsBuilder;

    invoke-direct {v1}, Lorg/apache/commons/lang3/builder/EqualsBuilder;-><init>()V

    iget v2, p0, Lcom/mfluent/asp/sync/j$a;->b:I

    iget v3, p1, Lcom/mfluent/asp/sync/j$a;->b:I

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/lang3/builder/EqualsBuilder;->append(II)Lorg/apache/commons/lang3/builder/EqualsBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/sync/j$a;->c:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/lang3/builder/EqualsBuilder;->append(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/EqualsBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/sync/j$a;->c:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/lang3/builder/EqualsBuilder;->append(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/EqualsBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mfluent/asp/sync/j$a;->a:Lcom/mfluent/asp/sync/j;

    iget-object v2, p0, Lcom/mfluent/asp/sync/j$a;->c:Landroid/content/Intent;

    invoke-static {v2}, Lcom/mfluent/asp/sync/j;->i(Landroid/content/Intent;)I

    move-result v2

    iget-object v3, p0, Lcom/mfluent/asp/sync/j$a;->a:Lcom/mfluent/asp/sync/j;

    invoke-static {v0}, Lcom/mfluent/asp/sync/j;->i(Landroid/content/Intent;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/lang3/builder/EqualsBuilder;->append(II)Lorg/apache/commons/lang3/builder/EqualsBuilder;

    move-result-object v1

    .line 267
    iget-object v2, p0, Lcom/mfluent/asp/sync/j$a;->a:Lcom/mfluent/asp/sync/j;

    iget-object v3, p0, Lcom/mfluent/asp/sync/j$a;->c:Landroid/content/Intent;

    invoke-virtual {v2, v1, v3, v0}, Lcom/mfluent/asp/sync/j;->a(Lorg/apache/commons/lang3/builder/EqualsBuilder;Landroid/content/Intent;Landroid/content/Intent;)V

    .line 269
    invoke-virtual {v1}, Lorg/apache/commons/lang3/builder/EqualsBuilder;->isEquals()Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 275
    new-instance v0, Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    invoke-direct {v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;-><init>()V

    iget v1, p0, Lcom/mfluent/asp/sync/j$a;->b:I

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(I)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/j$a;->c:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/j$a;->c:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/j$a;->a:Lcom/mfluent/asp/sync/j;

    iget-object v1, p0, Lcom/mfluent/asp/sync/j$a;->c:Landroid/content/Intent;

    invoke-static {v1}, Lcom/mfluent/asp/sync/j;->i(Landroid/content/Intent;)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(I)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    move-result-object v0

    .line 282
    iget-object v1, p0, Lcom/mfluent/asp/sync/j$a;->a:Lcom/mfluent/asp/sync/j;

    iget-object v2, p0, Lcom/mfluent/asp/sync/j$a;->c:Landroid/content/Intent;

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/sync/j;->a(Lorg/apache/commons/lang3/builder/HashCodeBuilder;Landroid/content/Intent;)V

    .line 283
    invoke-virtual {v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->hashCode()I

    move-result v0

    return v0
.end method
