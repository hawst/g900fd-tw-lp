.class public Lcom/mfluent/asp/sync/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/sync/b$b;,
        Lcom/mfluent/asp/sync/b$f;,
        Lcom/mfluent/asp/sync/b$c;,
        Lcom/mfluent/asp/sync/b$a;,
        Lcom/mfluent/asp/sync/b$d;,
        Lcom/mfluent/asp/sync/b$e;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final b:Lorg/slf4j/Logger;

.field private static c:Z


# instance fields
.field private final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/sync/b$d;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/mfluent/asp/datamodel/Device;

.field private final f:Landroid/content/ContentResolver;

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 62
    const-class v0, Lcom/mfluent/asp/sync/b;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/b;->b:Lorg/slf4j/Logger;

    .line 68
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/sync/b;->c:Z

    .line 71
    :try_start_0
    sget-boolean v0, Lcom/mfluent/asp/sync/b;->c:Z

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/sdcard/SL_SFINDER2"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 73
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x1

    sput-boolean v0, Lcom/mfluent/asp/sync/b;->c:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    sget-object v1, Lcom/mfluent/asp/sync/b;->b:Lorg/slf4j/Logger;

    const-string v2, "Failed to check SFinder Path."

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;Landroid/content/ContentResolver;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/b;->d:Ljava/util/HashMap;

    .line 96
    iget-object v0, p0, Lcom/mfluent/asp/sync/b;->d:Ljava/util/HashMap;

    const-string v1, "Photo"

    new-instance v2, Lcom/mfluent/asp/sync/b$c;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/sync/b$c;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget-object v0, p0, Lcom/mfluent/asp/sync/b;->d:Ljava/util/HashMap;

    const-string v1, "Music"

    new-instance v2, Lcom/mfluent/asp/sync/b$a;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/sync/b$a;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v0, p0, Lcom/mfluent/asp/sync/b;->d:Ljava/util/HashMap;

    const-string v1, "Video"

    new-instance v2, Lcom/mfluent/asp/sync/b$f;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/sync/b$f;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lcom/mfluent/asp/sync/b;->d:Ljava/util/HashMap;

    const-string v1, "Document"

    new-instance v2, Lcom/mfluent/asp/sync/b$b;

    invoke-direct {v2, v3}, Lcom/mfluent/asp/sync/b$b;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/b;->g:Ljava/util/Set;

    .line 110
    iput-object p1, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    .line 111
    iput-object p2, p0, Lcom/mfluent/asp/sync/b;->f:Landroid/content/ContentResolver;

    .line 112
    return-void
.end method

.method private a(ILjava/lang/String;)I
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 875
    const/4 v7, -0x1

    .line 878
    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;->CONTENT_URI:Landroid/net/Uri;

    .line 879
    iget-object v0, p0, Lcom/mfluent/asp/sync/b;->f:Landroid/content/ContentResolver;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "device_id=? AND source_media_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 883
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 884
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 885
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    .line 886
    :try_start_2
    sget-object v0, Lcom/mfluent/asp/sync/b;->b:Lorg/slf4j/Logger;

    const-string v3, "::getAspIdFromMediaId() - mediaId:{}"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v0, v2

    .line 891
    :goto_0
    if-eqz v1, :cond_0

    .line 892
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 895
    :cond_0
    :goto_1
    return v0

    .line 888
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v2, v6

    move v0, v7

    .line 889
    :goto_2
    :try_start_3
    sget-object v3, Lcom/mfluent/asp/sync/b;->b:Lorg/slf4j/Logger;

    const-string v4, "Exception at getAspIdFromMediaId."

    invoke-interface {v3, v4, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 891
    if-eqz v2, :cond_0

    .line 892
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 891
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_1

    .line 892
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 891
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v6, v2

    goto :goto_3

    .line 888
    :catch_1
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move v0, v7

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v9, v0

    move v0, v2

    move-object v2, v1

    move-object v1, v9

    goto :goto_2

    :cond_2
    move v0, v7

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;)Lorg/json/JSONObject;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/sync/b$d;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 572
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 573
    const-string v0, "Command"

    const-string v1, "Sync"

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 576
    const-string v0, "MaxRecords"

    const/16 v1, 0x64

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 579
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 580
    const-string v0, "Items"

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 584
    invoke-static {}, Lcom/mfluent/asp/sync/b;->c()Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    move-result-object v0

    .line 585
    sget-object v1, Lcom/mfluent/asp/sync/b;->b:Lorg/slf4j/Logger;

    const-string v4, "::createSyncCommandEntity: {} - Hi Priority Media Type = {}"

    iget-object v5, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 586
    if-nez v0, :cond_2

    const/4 v0, 0x0

    move-object v1, v0

    .line 588
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589
    invoke-direct {p0, v3, v1}, Lcom/mfluent/asp/sync/b;->a(Lorg/json/JSONArray;Lcom/mfluent/asp/sync/b$d;)V

    .line 593
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/b$d;

    .line 594
    if-eq v0, v1, :cond_1

    .line 595
    invoke-direct {p0, v3, v0}, Lcom/mfluent/asp/sync/b;->a(Lorg/json/JSONArray;Lcom/mfluent/asp/sync/b$d;)V

    goto :goto_1

    .line 586
    :cond_2
    invoke-direct {p0, v0}, Lcom/mfluent/asp/sync/b;->b(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Lcom/mfluent/asp/sync/b$d;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 599
    :cond_3
    return-object v2
.end method

.method private static a(Landroid/content/ContentValues;Lorg/json/JSONObject;[Lcom/mfluent/asp/dws/a$g;)V
    .locals 3

    .prologue
    .line 560
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_2

    .line 561
    aget-object v1, p2, v0

    .line 562
    invoke-virtual {v1}, Lcom/mfluent/asp/dws/a$g;->b()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/mfluent/asp/dws/a$g;->a:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 563
    :cond_0
    invoke-virtual {v1, p1, p0}, Lcom/mfluent/asp/dws/a$g;->a(Lorg/json/JSONObject;Landroid/content/ContentValues;)V

    .line 560
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 567
    :cond_2
    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 855
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 872
    :cond_0
    :goto_0
    return-void

    .line 859
    :cond_1
    sget-object v0, Lcom/mfluent/asp/sync/b;->b:Lorg/slf4j/Logger;

    const-string v1, "::notifySFinder() - uri:{} mediaIds:{} USE_SFINDER2_REMOTE_NOTIFY:{}"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    sget-boolean v4, Lcom/mfluent/asp/sync/b;->c:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 861
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 862
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 863
    iget-object v3, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/mfluent/asp/sync/b;->a(ILjava/lang/String;)I

    move-result v0

    .line 864
    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    .line 865
    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 869
    :cond_3
    sget-boolean v0, Lcom/mfluent/asp/sync/b;->c:Z

    if-eqz v0, :cond_0

    .line 870
    invoke-static {p1, v1}, Lcom/mfluent/asp/util/a;->a(Landroid/net/Uri;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public static a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)V
    .locals 3

    .prologue
    .line 823
    if-nez p0, :cond_0

    .line 830
    :goto_0
    return-void

    .line 827
    :cond_0
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->k()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 828
    const-string v1, "com.mfluent.asp.sync.ASP15DeviceMetadataSyncManager.PRIORITIZED_SYNC_MEDIA_TYPE_PREF_KEY"

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 829
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private static a(Lcom/mfluent/asp/sync/b$d;Landroid/content/ContentProviderOperation;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 436
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$d;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    .line 437
    if-eqz v0, :cond_1

    .line 438
    sget-object v1, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 439
    const-string v1, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Duplicate operation removed from list for mediaId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " old: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_0
    iget-object v1, p0, Lcom/mfluent/asp/sync/b$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 443
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 444
    return-void
.end method

.method private a(Lcom/mfluent/asp/sync/b$d;Lorg/json/JSONArray;)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 335
    if-nez p2, :cond_0

    .line 410
    :goto_0
    return-void

    .line 339
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/sync/b$d;->a()V

    .line 341
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 344
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/sync/b$d;->c()Landroid/net/Uri;

    move-result-object v11

    .line 345
    invoke-virtual {v11}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "insert_or_update"

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v14

    .line 346
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/sync/b$d;->e()[Lcom/mfluent/asp/dws/a$g;

    move-result-object v15

    .line 347
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 349
    const/4 v2, 0x0

    move v8, v2

    :goto_1
    invoke-virtual/range {p2 .. p2}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v8, v2, :cond_f

    .line 350
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->M()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 352
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->L()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 354
    const-string v2, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " deleted during sync"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->M()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 358
    :cond_1
    const/4 v2, 0x0

    .line 360
    :try_start_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v10

    .line 361
    :try_start_2
    const-string v2, "Command"

    const/4 v3, 0x0

    invoke-virtual {v10, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 363
    const-string v3, "Add"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "Update"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-eqz v3, :cond_c

    .line 365
    :cond_2
    :try_start_3
    const-string v2, "id"

    invoke-virtual {v10, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v17

    :try_start_4
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    move-object/from16 v0, v18

    invoke-static {v0, v10, v15}, Lcom/mfluent/asp/sync/b;->a(Landroid/content/ContentValues;Lorg/json/JSONObject;[Lcom/mfluent/asp/dws/a$g;)V

    const-string v2, "device_id"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {v14}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v2, v1}, Lcom/mfluent/asp/sync/b;->a(Lcom/mfluent/asp/sync/b$d;Landroid/content/ContentProviderOperation;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/sync/b$d;->d()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Photo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/sync/b$d;->d()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    const-string v2, "geo_loc_locale"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::updateGeoLocationRecord(): mediatypeHelper:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/sync/b$d;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/sync/b;->a(Ljava/lang/String;)V

    const-string v2, "geo_loc_locale"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_4

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    :goto_2
    new-instance v2, Ljava/lang/Integer;

    move-object/from16 v0, v17

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/b;->f:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    move-object/from16 v2, p1

    move-object/from16 v5, v17

    move-object v6, v10

    move-object/from16 v7, v18

    invoke-virtual/range {v2 .. v7}, Lcom/mfluent/asp/sync/b$d;->a(Landroid/content/ContentResolver;ILjava/lang/String;Lorg/json/JSONObject;Landroid/content/ContentValues;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 389
    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->M()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 349
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto/16 :goto_1

    .line 365
    :cond_7
    const/4 v9, 0x0

    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/b;->f:Landroid/content/ContentResolver;

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$GeoLocation;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "geo_loc_locale"

    aput-object v6, v4, v5

    const-string v5, "geo_loc_locale=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v19, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v3

    if-eqz v3, :cond_8

    :try_start_6
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "::updateGeoLocationRecord(): Found Duplicate geoLocale in DB:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/sync/b;->a(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-eqz v3, :cond_4

    :try_start_7
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    .line 379
    :catch_0
    move-exception v2

    move-object v2, v10

    :goto_4
    :try_start_8
    sget-object v3, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x3

    if-gt v3, v4, :cond_6

    .line 380
    const-string v3, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to parse 1.5 sync response command for mediaType "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/sync/b$d;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at index "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\nCommand: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_3

    .line 389
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->M()Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2

    .line 365
    :cond_8
    if-eqz v3, :cond_9

    :try_start_9
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/b;->g:Ljava/util/Set;

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::updateGeoLocationRecord(): Found Duplicate geoLocale:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/sync/b;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :catchall_1
    move-exception v2

    move-object v3, v9

    :goto_5
    if-eqz v3, :cond_a

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v2

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/b;->g:Ljava/util/Set;

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::updateGeoLocationRecord(): Inserting geoLocale:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/sync/b;->a(Ljava/lang/String;)V

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "latitude"

    const-string v4, "latitude"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "longitude"

    const-string v4, "longitude"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "geo_loc_locale"

    const-string v4, "geo_loc_locale"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "geo_loc_country"

    const-string v4, "geo_loc_country"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "geo_loc_province"

    const-string v4, "geo_loc_province"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "geo_loc_sub_province"

    const-string v4, "geo_loc_sub_province"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "geo_loc_locality"

    const-string v4, "geo_loc_locality"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "geo_loc_sub_locality"

    const-string v4, "geo_loc_sub_locality"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "geo_loc_feature"

    const-string v4, "geo_loc_feature"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "geo_loc_thoroughfare"

    const-string v4, "geo_loc_thoroughfare"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "geo_loc_sub_thoroughfare"

    const-string v4, "geo_loc_sub_thoroughfare"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "geo_loc_premises"

    const-string v4, "geo_loc_premises"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$GeoLocation;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/mfluent/asp/sync/b$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 366
    :cond_c
    const-string v3, "Delete"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result v3

    if-eqz v3, :cond_d

    .line 367
    :try_start_a
    const-string v2, "id"

    invoke-virtual {v10, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v2

    :try_start_b
    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "source_media_id=? AND device_id=?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3, v2}, Lcom/mfluent/asp/sync/b;->a(Lcom/mfluent/asp/sync/b$d;Landroid/content/ContentProviderOperation;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/b;->f:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v2}, Lcom/mfluent/asp/sync/b$d;->a(Landroid/content/ContentResolver;ILjava/lang/String;)V

    goto/16 :goto_3

    .line 369
    :cond_d
    sget-object v3, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x3

    if-gt v3, v4, :cond_6

    .line 370
    const-string v3, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bad command field for mediaType "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/sync/b$d;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at index "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\nCommand: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez v2, :cond_e

    const-string v2, "<null>"

    :cond_e
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_3

    .line 393
    :cond_f
    sget-object v2, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 394
    const-string v2, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::processCommandsArray - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - processed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lorg/json/JSONArray;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/sync/b$d;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " commands."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v12

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms. remoteMediaIdsWithMapCoord:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/b;->f:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/sync/b$d;->a(Landroid/content/ContentResolver;I)V

    .line 408
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/sync/b$d;->c()Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v2, v1}, Lcom/mfluent/asp/sync/b;->a(Landroid/net/Uri;Ljava/util/ArrayList;)V

    .line 409
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 379
    :catch_1
    move-exception v3

    goto/16 :goto_4

    .line 367
    :catch_2
    move-exception v2

    goto/16 :goto_3

    .line 365
    :catchall_2
    move-exception v2

    goto/16 :goto_5

    :catch_3
    move-exception v2

    goto/16 :goto_3
.end method

.method private static a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 849
    sget-object v0, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 850
    const-string v0, "mfl_ASP15DeviceMetadataSyncManager"

    invoke-static {v0, p0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    :cond_0
    return-void
.end method

.method private a(Lorg/json/JSONArray;Lcom/mfluent/asp/sync/b$d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 603
    iget-object v0, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {p2}, Lcom/mfluent/asp/sync/b$d;->b()Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v0

    .line 604
    if-nez v0, :cond_0

    .line 605
    const-string v0, "0"

    .line 608
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 609
    const-string v2, "SyncKey"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 610
    const-string v0, "Class"

    invoke-virtual {p2}, Lcom/mfluent/asp/sync/b$d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 611
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 612
    return-void
.end method

.method private a(Lorg/json/JSONArray;Ljava/util/Collection;Ljava/util/Map;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            "Ljava/util/Collection",
            "<",
            "Lcom/mfluent/asp/sync/b$d;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/mfluent/asp/sync/b$e;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 242
    const/4 v1, 0x0

    .line 243
    if-nez p1, :cond_1

    .line 244
    const/4 v1, 0x0

    .line 331
    :cond_0
    :goto_0
    return v1

    .line 254
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 255
    const/4 v0, 0x0

    .line 257
    :try_start_0
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v5

    .line 258
    :try_start_1
    const-string v0, "Class"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 259
    const-string v0, "Status"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    move v3, v0

    .line 261
    :goto_2
    :try_start_2
    const-string v0, "SyncKey"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    move-object v4, v0

    .line 281
    :goto_3
    invoke-interface {p3, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 282
    const-string v0, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received unknown media type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 259
    :cond_2
    const/4 v0, 0x0

    move v3, v0

    goto :goto_2

    .line 262
    :catch_0
    move-exception v0

    .line 263
    if-eqz v3, :cond_4

    .line 264
    :try_start_3
    const-string v4, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "Received \"Invalid Sync Key\" status, but missing sync key in 1.5 sync response item at index "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ". This violates the protocol, but we\'ll perform full sync anyway.\nItem: "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v5, :cond_3

    const-string v0, "<null>"

    :goto_5
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const-string v0, "0"

    move-object v4, v0

    goto :goto_3

    .line 264
    :cond_3
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 271
    :cond_4
    throw v0
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 276
    :catch_1
    move-exception v0

    move-object v0, v5

    :goto_6
    const-string v3, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to parse 1.5 sync response item at index "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\nItem: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez v0, :cond_5

    const-string v0, "<null>"

    :goto_7
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 286
    :cond_6
    iget-object v0, p0, Lcom/mfluent/asp/sync/b;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/b$d;

    .line 289
    if-eqz v3, :cond_8

    .line 290
    const-string v4, "0"

    .line 291
    iget-object v7, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/mfluent/asp/sync/b$d;->a(I)Landroid/net/Uri;

    move-result-object v7

    .line 292
    if-eqz v7, :cond_7

    .line 293
    iget-object v8, p0, Lcom/mfluent/asp/sync/b;->f:Landroid/content/ContentResolver;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v7, v9, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 301
    :cond_7
    :goto_8
    iget-object v7, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->L()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 302
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 297
    :cond_8
    const-string v7, "Commands"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 298
    invoke-direct {p0, v0, v7}, Lcom/mfluent/asp/sync/b;->a(Lcom/mfluent/asp/sync/b$d;Lorg/json/JSONArray;)V

    goto :goto_8

    .line 305
    :cond_9
    const-string v7, "MoreAvailable"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 309
    invoke-virtual {v0}, Lcom/mfluent/asp/sync/b$d;->b()Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    move-result-object v8

    .line 311
    iget-object v5, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v5, v8}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 312
    const-string v1, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "Storing sync key "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ": "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v1, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1, v8, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    .line 314
    const/4 v1, 0x1

    move v4, v1

    .line 318
    :goto_9
    invoke-interface {p3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/sync/b$e;

    .line 319
    if-eqz v3, :cond_b

    iget v5, v1, Lcom/mfluent/asp/sync/b$e;->a:I

    add-int/lit8 v5, v5, 0x1

    :goto_a
    iput v5, v1, Lcom/mfluent/asp/sync/b$e;->a:I

    .line 324
    if-eqz v3, :cond_c

    iget v1, v1, Lcom/mfluent/asp/sync/b$e;->a:I

    const/4 v3, 0x2

    if-ge v1, v3, :cond_c

    const/4 v1, 0x1

    .line 325
    :goto_b
    const-string v3, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " moreAvailable: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " resync from invalid key:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    if-nez v1, :cond_a

    if-nez v7, :cond_a

    .line 327
    invoke-interface {p2, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    :cond_a
    move v1, v4

    goto/16 :goto_4

    .line 319
    :cond_b
    const/4 v5, 0x0

    goto :goto_a

    .line 324
    :cond_c
    const/4 v1, 0x0

    goto :goto_b

    .line 276
    :catch_2
    move-exception v3

    goto/16 :goto_6

    :cond_d
    move v4, v1

    goto :goto_9
.end method

.method private b(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Lcom/mfluent/asp/sync/b$d;
    .locals 2

    .prologue
    .line 236
    invoke-static {p1}, Lcom/mfluent/asp/dws/a$h;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcom/mfluent/asp/sync/b;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/b$d;

    return-object v0
.end method

.method private b()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/sync/b$d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 225
    iget-object v0, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->J()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    .line 226
    invoke-direct {p0, v0}, Lcom/mfluent/asp/sync/b;->b(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Lcom/mfluent/asp/sync/b$d;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_0

    .line 228
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 232
    :cond_1
    return-object v1
.end method

.method private static c()Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 833
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->k()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "com.mfluent.asp.sync.ASP15DeviceMetadataSyncManager.PRIORITIZED_SYNC_MEDIA_TYPE_PREF_KEY"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 835
    if-nez v0, :cond_0

    .line 845
    :goto_0
    return-object v1

    .line 841
    :cond_0
    :try_start_0
    invoke-static {v0}, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->valueOf(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 845
    goto :goto_0

    .line 843
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v11, 0x3

    .line 117
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 118
    sget-object v0, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v11}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const-string v0, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "::doSync - "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " - sync started."

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 124
    sget-object v0, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v11}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    const-string v0, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doSync - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - sync complete. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_1
    :goto_0
    return v1

    .line 128
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/mfluent/asp/sync/b;->b()Ljava/util/ArrayList;

    move-result-object v6

    .line 130
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 131
    const-string v0, "Photo"

    new-instance v3, Lcom/mfluent/asp/sync/b$e;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/sync/b$e;-><init>(Lcom/mfluent/asp/sync/b;)V

    invoke-virtual {v7, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    const-string v0, "Music"

    new-instance v3, Lcom/mfluent/asp/sync/b$e;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/sync/b$e;-><init>(Lcom/mfluent/asp/sync/b;)V

    invoke-virtual {v7, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    const-string v0, "Video"

    new-instance v3, Lcom/mfluent/asp/sync/b$e;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/sync/b$e;-><init>(Lcom/mfluent/asp/sync/b;)V

    invoke-virtual {v7, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    const-string v0, "Document"

    new-instance v3, Lcom/mfluent/asp/sync/b$e;

    invoke-direct {v3, p0}, Lcom/mfluent/asp/sync/b$e;-><init>(Lcom/mfluent/asp/sync/b;)V

    invoke-virtual {v7, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v3, v1

    .line 137
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_b

    .line 139
    :try_start_2
    invoke-direct {p0, v6}, Lcom/mfluent/asp/sync/b;->a(Ljava/util/ArrayList;)Lorg/json/JSONObject;

    move-result-object v0

    .line 141
    sget-object v8, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 142
    const-string v8, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "::doSync - "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v10}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Sending sync request: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_3
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    iget-object v8, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    const-string v9, "/v1/media/sync"

    invoke-static {v8, v9, v0}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_4

    const-string v8, "Command"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "Sync"

    invoke-static {v8, v9}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 146
    :cond_4
    sget-object v0, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 147
    const-string v0, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::doSync - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - Quitting sync. No response object or no commands in response."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 149
    :cond_5
    sget-object v0, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v11}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    const-string v0, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doSync - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - sync complete. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 151
    :cond_6
    :try_start_3
    sget-object v8, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 152
    const-string v8, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "::doSync - "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v10}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Received response"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_7
    const-string v8, "Items"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 194
    invoke-direct {p0, v0, v6, v7}, Lcom/mfluent/asp/sync/b;->a(Lorg/json/JSONArray;Ljava/util/Collection;Ljava/util/Map;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    :cond_8
    move v0, v2

    :goto_2
    move v3, v0

    .line 201
    goto/16 :goto_1

    :cond_9
    move v0, v1

    .line 194
    goto :goto_2

    .line 195
    :catch_0
    move-exception v0

    .line 196
    :try_start_4
    const-string v1, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "::doSync - "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " - Trouble with http request - "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 197
    sget-object v0, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v11}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 214
    const-string v0, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::doSync - "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " - sync complete. "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "ms"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    move v1, v2

    goto/16 :goto_0

    .line 198
    :catch_1
    move-exception v0

    .line 199
    :try_start_5
    const-string v2, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::doSync - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - Trouble with http request - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 200
    :cond_b
    if-eqz v3, :cond_c

    .line 207
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    iget-object v2, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/t;->updateDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V

    .line 208
    sget-boolean v0, Lcom/mfluent/asp/sync/b;->c:Z

    if-eqz v0, :cond_c

    .line 209
    iget-object v2, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    const-string v3, "slpf_pref_20"

    const/4 v6, 0x0

    invoke-virtual {v0, v3, v6}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "com.mfluent.asp.sync.ASP15DeviceMetadataSyncManager.INITIAL_SYNC_COMPLETE_NOTIFIED_PREF_KEY_DEVICE_ID_"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_c

    invoke-static {v2}, Lcom/mfluent/asp/util/a;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x1

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v0, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " set to true "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 213
    :cond_c
    sget-object v0, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v11}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    const-string v0, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doSync - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - sync complete. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 213
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/mfluent/asp/sync/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v11}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 214
    const-string v1, "mfl_ASP15DeviceMetadataSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doSync - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/sync/b;->e:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - sync complete. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v4, v6, v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    throw v0
.end method
