.class public Lcom/mfluent/asp/sync/CloudStorageSyncManager;
.super Lcom/mfluent/asp/sync/f;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageAppDelegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/sync/CloudStorageSyncManager$a;
    }
.end annotation


# static fields
.field private static final b:Lorg/slf4j/Logger;

.field private static e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static j:Z


# instance fields
.field private f:Ljava/util/concurrent/ScheduledExecutorService;

.field private g:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private final h:Landroid/content/BroadcastReceiver;

.field private i:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->b:Lorg/slf4j/Logger;

    .line 43
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_CLOUD:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 329
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->j:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/sync/f;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V

    .line 47
    iput-object v2, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 49
    iput-object v2, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->g:Ljava/util/concurrent/ScheduledFuture;

    .line 51
    new-instance v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mfluent/asp/sync/CloudStorageSyncManager$a;-><init>(Lcom/mfluent/asp/sync/CloudStorageSyncManager;B)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->h:Landroid/content/BroadcastReceiver;

    .line 53
    iput-object v2, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->i:Landroid/content/Intent;

    .line 57
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V
    .locals 3

    .prologue
    .line 332
    const/4 v0, 0x1

    sput-boolean v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->j:Z

    .line 333
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.sync.CLOUD_LAUNCH_OAUTH1_BROWSER"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 334
    return-void
.end method

.method public static b(Landroid/content/Context;Landroid/content/BroadcastReceiver;)V
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    sput-boolean v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->j:Z

    .line 338
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 339
    return-void
.end method

.method static synthetic c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 110
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.DataModel.REFRESH_ALL"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 111
    iget-object v1, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    const-string v2, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_REFRESH"

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->buildDeviceIntentFilterForAction(Ljava/lang/String;)Landroid/content/IntentFilter;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 112
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/mfluent/asp/sync/d;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 114
    return-object v0
.end method

.method protected final a_(Landroid/content/Intent;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v8, 0x3

    const/4 v1, 0x0

    .line 119
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/sync/d;->a(Landroid/content/Context;)Lcom/mfluent/asp/sync/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    sget-object v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->b:Lorg/slf4j/Logger;

    const-string v2, "Skipping sync for device {} because cloud storage sync is disabled"

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    move v0, v1

    .line 156
    :goto_0
    return v0

    .line 124
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    if-nez v0, :cond_2

    .line 125
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    if-nez v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/g;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/g;

    move-result-object v0

    iget-object v3, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/b/g;->a(Ljava/lang/String;)Lcom/mfluent/asp/b/h;

    move-result-object v3

    if-nez v3, :cond_6

    sget-object v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_1

    const-string v0, "mfl_CloudStorageSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::loadCloudStoragePlugin: No storage type defined for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 127
    sget-object v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v3, 0x4

    if-gt v0, v3, :cond_2

    .line 128
    const-string v0, "mfl_CloudStorageSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::doSync:Loaded: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v4

    invoke-interface {v4}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getVersion()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->g:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_3

    .line 134
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->g:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->g:Ljava/util/concurrent/ScheduledFuture;

    .line 138
    :cond_3
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    if-nez v0, :cond_8

    .line 140
    sget-object v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v2, 0x6

    if-gt v0, v2, :cond_4

    .line 141
    const-string v0, "mfl_CloudStorageSyncManager"

    const-string v2, "::doSync:Could not load web storage types from server."

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_4
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->f:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v0, :cond_5

    .line 144
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 146
    :cond_5
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->f:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/mfluent/asp/sync/CloudStorageSyncManager$1;

    invoke-direct {v2, p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager$1;-><init>(Lcom/mfluent/asp/sync/CloudStorageSyncManager;)V

    const-wide/16 v4, 0x2710

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v4, v5, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->g:Ljava/util/concurrent/ScheduledFuture;

    move v0, v1

    .line 153
    goto/16 :goto_0

    .line 125
    :cond_6
    :try_start_1
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->c:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/mfluent/asp/cloudstorage/a;->b(Lcom/mfluent/asp/b/h;Landroid/content/Context;)Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v4

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;)V

    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.mfluent.asp.syn.PROVIDER_CONFIGURATION"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/mfluent/asp/b/h;->o()Ljava/lang/String;

    move-result-object v3

    const-string v6, "countryCode"

    invoke-virtual {v5, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-interface {v4, v0, v5}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-interface {v4, v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->setContentProvider(Landroid/content/ContentResolver;)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-interface {v4, v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->setCloudDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V

    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-interface {v4, v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->setDataModel(Lcom/mfluent/asp/common/datamodel/CloudDataModel;)V

    invoke-interface {v4, p0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->setStorageAppDelegate(Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageAppDelegate;)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/e;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/e;

    move-result-object v0

    invoke-interface {v4, v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->setStorageGatewayHelper(Lcom/mfluent/asp/cloudstorage/api/sync/StorageGatewayHelper;)V

    invoke-interface {v4}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->init()V

    sget-object v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v8, :cond_7

    const-string v0, "mfl_CloudStorageSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Loaded: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " version: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v4}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getVersion()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    invoke-interface {v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->getIntentFilters()[Landroid/content/IntentFilter;

    move-result-object v4

    array-length v5, v4

    move v0, v1

    :goto_2
    if-ge v0, v5, :cond_1

    aget-object v6, v4, v0

    iget-object v7, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v7, v6}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    sget-object v3, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v8, :cond_1

    const-string v3, "mfl_CloudStorageSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::loadCloudStoragePlugin:failed to load web storage plugin for ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 156
    goto/16 :goto_0
.end method

.method protected final b(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 162
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 163
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->l(Z)V

    .line 164
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->m(Z)V

    .line 169
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.fluent.asp.syn.UI_STATE"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 170
    const-string v0, "com.fluent.asp.syn.CAN_LAUNCH_OAUTH"

    sget-boolean v4, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->j:Z

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 171
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 172
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v4

    invoke-interface {v4, v0, v1}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 174
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    invoke-interface {v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->sync()V

    .line 176
    sget-object v0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    .line 177
    const-string v0, "mfl_CloudStorageSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "::onReceive:"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " sync took "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->V()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e()V

    .line 194
    return-void

    .line 181
    :catch_0
    move-exception v0

    .line 182
    sget-object v1, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_2

    .line 183
    const-string v1, "mfl_CloudStorageSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doSync: Last change exception caught for cloud storage. Sync failed for device: ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 185
    :cond_2
    sget-boolean v1, Lcom/mfluent/asp/common/util/AspLogLevels;->SAVE_LOG_ON_CRASH:Z

    if-eqz v1, :cond_1

    .line 187
    :try_start_1
    new-instance v1, Lcom/mfluent/asp/util/f;

    invoke-direct {v1}, Lcom/mfluent/asp/util/f;-><init>()V

    invoke-static {v0}, Lcom/mfluent/asp/util/f;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method protected final b()Z
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x1

    return v0
.end method

.method protected final c(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/mfluent/asp/sync/f;->c(Landroid/content/Intent;)V

    .line 72
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->h:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 75
    return-void
.end method

.method protected final d(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/mfluent/asp/sync/f;->d(Landroid/content/Intent;)V

    .line 81
    if-eqz p1, :cond_0

    .line 82
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 83
    sget-object v1, Lcom/mfluent/asp/ASPApplication;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->y()Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;

    move-result-object v0

    invoke-interface {v0}, Lcom/mfluent/asp/cloudstorage/api/sync/CloudStorageSync;->reset()V

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 93
    return-void
.end method

.method public requestSync()V
    .locals 3

    .prologue
    .line 216
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->i:Landroid/content/Intent;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_STATE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->i:Landroid/content/Intent;

    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->i:Landroid/content/Intent;

    const-string v1, "DEVICE_ID_EXTRA_KEY"

    iget-object v2, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->i:Landroid/content/Intent;

    .line 217
    iget-object v1, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 218
    return-void
.end method

.method public sendBroadcastMessage(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 205
    iget-object v0, p0, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 206
    if-eqz v0, :cond_1

    .line 207
    sget-object v1, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->e:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_0

    .line 208
    const-string v1, "mfl_CloudStorageSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::sendBroadcastMessage:CloudStorage SendBroadcast: ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] for device id=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_0
    invoke-virtual {v0, p1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 212
    :cond_1
    return-void
.end method
