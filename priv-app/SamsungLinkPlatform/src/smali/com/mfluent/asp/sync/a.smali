.class public final Lcom/mfluent/asp/sync/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/sync/a$a;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:Lcom/mfluent/asp/datamodel/Device;

.field private final c:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_SYNC:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/sync/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Lcom/mfluent/asp/datamodel/Device;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    .line 59
    iput-object p2, p0, Lcom/mfluent/asp/sync/a;->c:Landroid/content/ContentResolver;

    .line 60
    return-void
.end method

.method private a(Lcom/mfluent/asp/datamodel/ao;)Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mfluent/asp/datamodel/ao;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 475
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 476
    iget-object v0, p0, Lcom/mfluent/asp/sync/a;->c:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {p1, v4, v5}, Lcom/mfluent/asp/datamodel/ao;->b(J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v7

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 484
    :goto_0
    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 485
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 488
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    .line 489
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 488
    :cond_1
    if-eqz v1, :cond_2

    .line 489
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 493
    :cond_2
    return-object v6
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/ao;Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Lcom/mfluent/asp/sync/a$a;)Z
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 294
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "?_="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&sort=RECENT&maxItems=1&queue=clear&forceRefresh=TRUE"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 304
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 306
    const/4 v2, 0x0

    .line 471
    :goto_0
    return v2

    .line 297
    :catch_0
    move-exception v2

    .line 298
    sget-object v3, Lcom/mfluent/asp/sync/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x3

    if-gt v3, v4, :cond_0

    .line 299
    const-string v3, "mfl_ASP10DeviceMetadataSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::syncHelper:Trouble syncing "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 301
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 311
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/mfluent/asp/sync/a;->a(Lcom/mfluent/asp/datamodel/ao;)Ljava/util/Set;

    move-result-object v11

    .line 313
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 315
    const/4 v2, 0x0

    .line 316
    const-string v3, ""

    move-object v10, v2

    .line 319
    :goto_1
    :try_start_1
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "?_="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&sort=RECENT&maxItems=100&queue=clear&forceRefresh=TRUE&id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 322
    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v14

    .line 324
    const/4 v2, 0x0

    move v8, v2

    move-object v2, v3

    :goto_2
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v8, v3, :cond_d

    .line 325
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 326
    sget-object v2, Lcom/mfluent/asp/sync/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x3

    if-gt v2, v3, :cond_2

    .line 327
    const-string v2, "mfl_ASP10DeviceMetadataSyncManager"

    const-string v3, "::syncHelper:Aborting sync because thread is interrupted"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 332
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->M()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 334
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->L()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 336
    const-string v2, "mfl_ASP10DeviceMetadataSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " deleted during sync"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 337
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->M()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    const/4 v2, 0x0

    goto/16 :goto_0

    .line 340
    :cond_4
    :try_start_4
    invoke-virtual {v14, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v15

    .line 341
    const-string v2, "id"

    invoke-virtual {v15, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 343
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->c:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    int-to-long v4, v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Lcom/mfluent/asp/datamodel/ao;->b(J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const-string v5, "source_media_id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v9, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 350
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x1

    move v4, v2

    .line 351
    :goto_3
    const-wide/16 v2, 0x0

    .line 353
    if-nez v4, :cond_5

    .line 354
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    .line 355
    const/4 v2, 0x0

    invoke-interface {v5, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 356
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v11, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 358
    :cond_5
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 360
    if-eqz v4, :cond_9

    .line 361
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 362
    const-string v3, "device_id"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 363
    const-string v3, "source_media_id"

    invoke-virtual {v2, v3, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    move-object/from16 v0, p6

    invoke-interface {v0, v2, v15}, Lcom/mfluent/asp/sync/a$a;->a(Landroid/content/ContentValues;Lorg/json/JSONObject;)V

    .line 367
    const-string v3, "fileName"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 369
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v4, v5}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 370
    const-string v4, "_display_name"

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    :cond_6
    sget-object v3, Lcom/mfluent/asp/sync/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_7

    .line 374
    const-string v3, "mfl_ASP10DeviceMetadataSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::syncHelper:("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") Inserting media: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_display_name"

    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    :cond_7
    invoke-virtual/range {p3 .. p3}, Lcom/mfluent/asp/datamodel/ao;->h()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 383
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->c:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-object/from16 v0, p6

    invoke-interface {v0, v9, v15}, Lcom/mfluent/asp/sync/a$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 414
    :goto_4
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->M()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 324
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move-object v2, v9

    goto/16 :goto_2

    .line 350
    :cond_8
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_3

    .line 387
    :cond_9
    :try_start_6
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 388
    const-string v5, "device_id"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 389
    const-string v5, "source_media_id"

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    move-object/from16 v0, p6

    invoke-interface {v0, v4, v15}, Lcom/mfluent/asp/sync/a$a;->a(Landroid/content/ContentValues;Lorg/json/JSONObject;)V

    .line 393
    const-string v5, "fileName"

    invoke-virtual {v15, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 395
    invoke-static {v5}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v6, v7}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 396
    const-string v6, "_display_name"

    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    :cond_a
    sget-object v5, Lcom/mfluent/asp/sync/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v5

    const/4 v6, 0x2

    if-gt v5, v6, :cond_b

    .line 399
    const-string v5, "mfl_ASP10DeviceMetadataSyncManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::syncHelper:("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") Updating media: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_display_name"

    invoke-virtual {v4, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_b
    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/ao;->a(J)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 408
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 410
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->c:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-object/from16 v0, p6

    invoke-interface {v0, v9, v15}, Lcom/mfluent/asp/sync/a$a;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_4

    .line 416
    :catchall_0
    move-exception v2

    :try_start_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->M()Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v2
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    .line 443
    :catch_1
    move-exception v2

    .line 444
    sget-object v3, Lcom/mfluent/asp/sync/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x3

    if-gt v3, v4, :cond_c

    .line 445
    const-string v3, "mfl_ASP10DeviceMetadataSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::syncHelper:Trouble syncing "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 447
    :cond_c
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 420
    :cond_d
    :try_start_8
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_e

    .line 421
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/a;->c:Landroid/content/ContentResolver;

    const-string v4, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    invoke-virtual {v3, v4, v12}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 422
    invoke-virtual {v12}, Ljava/util/ArrayList;->clear()V

    .line 424
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/a;->c:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v4}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v4

    move-object/from16 v0, p6

    invoke-interface {v0, v3, v12, v4}, Lcom/mfluent/asp/sync/a$a;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;I)V

    .line 426
    move-object/from16 v0, p4

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 427
    if-eqz v10, :cond_10

    invoke-static {v10, v3}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 429
    sget-object v2, Lcom/mfluent/asp/sync/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x3

    if-gt v2, v3, :cond_f

    .line 430
    const-string v2, "mfl_ASP10DeviceMetadataSyncManager"

    const-string v3, "::syncHelper:mediaRevision changed during the sync - starting the sync over"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :cond_f
    const-string v3, ""

    .line 433
    const/4 v2, 0x0

    move-object v10, v2

    .line 434
    goto/16 :goto_1

    .line 439
    :cond_10
    const-string v4, "isLastpage"

    const/4 v5, 0x0

    invoke-virtual {v13, v4, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 440
    if-nez v4, :cond_11

    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    move-result v4

    if-nez v4, :cond_13

    .line 457
    :cond_11
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 458
    sget-object v5, Lcom/mfluent/asp/sync/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v5}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v5

    const/4 v6, 0x2

    if-gt v5, v6, :cond_12

    .line 459
    const-string v5, "mfl_ASP10DeviceMetadataSyncManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::syncHelper:("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") Deleting media: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    :cond_12
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v7}, Lcom/mfluent/asp/datamodel/ao;->a(J)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_13
    move-object v10, v3

    move-object v3, v2

    .line 453
    goto/16 :goto_1

    .line 448
    :catch_2
    move-exception v2

    .line 449
    sget-object v3, Lcom/mfluent/asp/sync/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x3

    if-gt v3, v4, :cond_14

    .line 450
    const-string v3, "mfl_ASP10DeviceMetadataSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::syncHelper:Trouble syncing "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 452
    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 464
    :cond_15
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_16

    .line 465
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->c:Landroid/content/ContentResolver;

    const-string v4, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    invoke-virtual {v2, v4, v12}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 468
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    move-object/from16 v0, p5

    invoke-virtual {v2, v0, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    .line 469
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/datamodel/t;->updateDevice(Lcom/mfluent/asp/common/datamodel/CloudDevice;)V

    .line 471
    const/4 v2, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 10

    .prologue
    const/4 v9, 0x6

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 63
    iget-object v0, p0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v7

    .line 278
    :goto_0
    return v0

    .line 72
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v1

    .line 74
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne v0, v2, :cond_1

    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne v1, v0, :cond_1

    .line 75
    const-string v0, "REFRESH_FROM_KEY"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 76
    packed-switch v0, :pswitch_data_0

    .line 90
    :cond_1
    :goto_1
    :try_start_0
    const-string v1, "/api/pCloud/device/media/photo/images"

    const-string v2, "images"

    invoke-static {}, Lcom/mfluent/asp/datamodel/aj;->l()Lcom/mfluent/asp/datamodel/aj;

    move-result-object v3

    const-string v4, "photoRevision"

    sget-object v5, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    new-instance v6, Lcom/mfluent/asp/sync/a$1;

    invoke-direct {v6, p0}, Lcom/mfluent/asp/sync/a$1;-><init>(Lcom/mfluent/asp/sync/a;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/mfluent/asp/sync/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/ao;Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Lcom/mfluent/asp/sync/a$a;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v8

    .line 139
    if-eqz v8, :cond_2

    move v0, v8

    .line 140
    goto :goto_0

    .line 78
    :pswitch_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v0, v1, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    goto :goto_1

    .line 83
    :pswitch_1
    iget-object v0, p0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v0, v1, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    goto :goto_1

    .line 88
    :pswitch_2
    iget-object v0, p0, Lcom/mfluent/asp/sync/a;->b:Lcom/mfluent/asp/datamodel/Device;

    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v0, v1, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Ljava/lang/String;)V

    goto :goto_1

    .line 142
    :cond_2
    :try_start_1
    const-string v1, "/api/pCloud/device/media/video/clips"

    const-string v2, "clips"

    invoke-static {}, Lcom/mfluent/asp/datamodel/ax;->a()Lcom/mfluent/asp/datamodel/ax;

    move-result-object v3

    const-string v4, "videoRevision"

    sget-object v5, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    new-instance v6, Lcom/mfluent/asp/sync/a$2;

    invoke-direct {v6, p0}, Lcom/mfluent/asp/sync/a$2;-><init>(Lcom/mfluent/asp/sync/a;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/mfluent/asp/sync/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/ao;Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Lcom/mfluent/asp/sync/a$a;)Z

    move-result v0

    or-int/2addr v8, v0

    .line 199
    if-eqz v8, :cond_3

    move v0, v8

    .line 200
    goto :goto_0

    .line 202
    :cond_3
    const-string v1, "/api/pCloud/device/media/music/songs"

    const-string v2, "songs"

    invoke-static {}, Lcom/mfluent/asp/datamodel/j;->a()Lcom/mfluent/asp/datamodel/j;

    move-result-object v3

    const-string v4, "musicRevision"

    sget-object v5, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    new-instance v6, Lcom/mfluent/asp/sync/a$3;

    invoke-direct {v6, p0}, Lcom/mfluent/asp/sync/a$3;-><init>(Lcom/mfluent/asp/sync/a;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/mfluent/asp/sync/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/mfluent/asp/datamodel/ao;Ljava/lang/String;Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;Lcom/mfluent/asp/sync/a$a;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    or-int/2addr v0, v8

    goto/16 :goto_0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    sget-object v1, Lcom/mfluent/asp/sync/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v9}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 269
    const-string v1, "mfl_ASP10DeviceMetadataSyncManager"

    const-string v2, "::doSync RemoteException thrown"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_4
    move v0, v7

    .line 272
    goto/16 :goto_0

    .line 273
    :catch_1
    move-exception v0

    .line 274
    sget-object v1, Lcom/mfluent/asp/sync/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v9}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 275
    const-string v1, "mfl_ASP10DeviceMetadataSyncManager"

    const-string v2, "::doSync OperationApplicationThrown thrown"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_5
    move v0, v7

    .line 278
    goto/16 :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
