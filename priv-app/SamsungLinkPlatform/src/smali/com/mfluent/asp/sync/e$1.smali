.class final Lcom/mfluent/asp/sync/e$1;
.super Ljava/util/TimerTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/sync/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/e;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/sync/e;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/mfluent/asp/sync/e$1;->a:Lcom/mfluent/asp/sync/e;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/e$1;->a:Lcom/mfluent/asp/sync/e;

    invoke-static {}, Lcom/mfluent/asp/sync/e;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/sync/e$1;->a:Lcom/mfluent/asp/sync/e;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "SYNC_ON_TIMER_INTENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/sync/e;->e(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    .line 123
    invoke-static {}, Lcom/mfluent/asp/sync/e;->e()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_0

    .line 124
    const-string v1, "mfl_CloudStorageTypeListSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::run:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
