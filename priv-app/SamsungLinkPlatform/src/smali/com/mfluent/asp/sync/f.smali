.class public abstract Lcom/mfluent/asp/sync/f;
.super Lcom/mfluent/asp/sync/j;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/mfluent/asp/datamodel/Device;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/mfluent/asp/sync/j;-><init>(Landroid/content/Context;)V

    .line 27
    iput-object p2, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    .line 28
    return-void
.end method


# virtual methods
.method protected a()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    invoke-super {p0}, Lcom/mfluent/asp/sync/j;->a()Ljava/util/Collection;

    move-result-object v0

    .line 33
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.DataModel.REFRESH_ALL"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 34
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/mfluent/asp/ASPApplication;->e:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 35
    iget-object v1, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    const-string v2, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_REFRESH"

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->buildDeviceIntentFilterForAction(Ljava/lang/String;)Landroid/content/IntentFilter;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 37
    return-object v0
.end method

.method protected final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/sync/f;->a_(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    :goto_0
    return-void

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->l(Z)V

    .line 47
    iget-object v0, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->m(Z)V

    .line 48
    iget-object v0, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    .line 51
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/sync/f;->b(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    iget-object v0, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v2}, Lcom/mfluent/asp/datamodel/Device;->l(Z)V

    .line 54
    iget-object v0, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->m(Z)V

    .line 55
    iget-object v0, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    goto :goto_0

    .line 53
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->l(Z)V

    .line 54
    iget-object v1, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1, v3}, Lcom/mfluent/asp/datamodel/Device;->m(Z)V

    .line 55
    iget-object v1, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    throw v0
.end method

.method protected abstract a_(Landroid/content/Intent;)Z
.end method

.method protected abstract b(Landroid/content/Intent;)V
.end method

.method public final d()Lcom/mfluent/asp/datamodel/Device;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    return-object v0
.end method

.method protected final e()V
    .locals 5

    .prologue
    .line 69
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 70
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.sync.ASPThumbnailPrefetcherService.ACTION_PREFETCH_DEVICE"

    const/4 v3, 0x0

    const-class v4, Lcom/mfluent/asp/sync/ASPThumbnailPrefetcherService;

    invoke-direct {v1, v2, v3, v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 71
    const-string v2, "DEVICE_ID"

    iget-object v3, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 73
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 74
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 65
    new-instance v0, Lorg/apache/commons/lang3/builder/ToStringBuilder;

    invoke-direct {v0, p0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;-><init>(Ljava/lang/Object;)V

    const-string v1, "device"

    iget-object v2, p0, Lcom/mfluent/asp/sync/f;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
