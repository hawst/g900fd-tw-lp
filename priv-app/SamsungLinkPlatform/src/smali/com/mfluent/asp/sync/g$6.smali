.class final Lcom/mfluent/asp/sync/g$6;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mfluent/asp/sync/g$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mfluent/asp/sync/g;->n()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/g;

.field private b:Z

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:Landroid/graphics/BitmapFactory$Options;


# direct methods
.method constructor <init>(Lcom/mfluent/asp/sync/g;)V
    .locals 2

    .prologue
    .line 376
    iput-object p1, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/g$6;->b:Z

    .line 396
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/g$6;->p:Landroid/graphics/BitmapFactory$Options;

    .line 397
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$6;->p:Landroid/graphics/BitmapFactory$Options;

    const/16 v1, 0x4000

    new-array v1, v1, [B

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 398
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$6;->p:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 399
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;)I
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 462
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/g$6;->b:Z

    if-nez v0, :cond_0

    .line 463
    const-string v0, "date_modified"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->c:I

    .line 464
    const-string v0, "date_modified"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->d:I

    .line 465
    const-string v0, "_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->e:I

    .line 466
    const-string v0, "_data"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->f:I

    .line 467
    const-string v0, "orientation"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->g:I

    .line 468
    const-string v0, "orientation"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->j:I

    .line 469
    const-string v0, "longitude"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->h:I

    .line 470
    const-string v0, "longitude"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->k:I

    .line 471
    const-string v0, "latitude"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->i:I

    .line 472
    const-string v0, "latitude"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->l:I

    .line 473
    const-string v0, "thumb_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->m:I

    .line 474
    const-string v0, "thumb_width"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->n:I

    .line 475
    const-string v0, "thumb_height"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mfluent/asp/sync/g$6;->o:I

    .line 476
    iput-boolean v4, p0, Lcom/mfluent/asp/sync/g$6;->b:Z

    .line 479
    :cond_0
    iget v0, p0, Lcom/mfluent/asp/sync/g$6;->c:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iget v0, p0, Lcom/mfluent/asp/sync/g$6;->d:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    cmp-long v0, v2, v6

    if-nez v0, :cond_2

    iget v0, p0, Lcom/mfluent/asp/sync/g$6;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v2, p0, Lcom/mfluent/asp/sync/g$6;->f:I

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/mfluent/asp/sync/g$6;->g:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iget v2, p0, Lcom/mfluent/asp/sync/g$6;->j:I

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v0, v2, :cond_2

    iget v0, p0, Lcom/mfluent/asp/sync/g$6;->h:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    iget v0, p0, Lcom/mfluent/asp/sync/g$6;->k:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    cmpl-double v0, v2, v6

    if-nez v0, :cond_2

    iget v0, p0, Lcom/mfluent/asp/sync/g$6;->i:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    iget v0, p0, Lcom/mfluent/asp/sync/g$6;->l:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    cmpl-double v0, v2, v6

    if-nez v0, :cond_2

    move v0, v4

    .line 485
    :goto_0
    if-eqz v0, :cond_1

    .line 486
    iget v0, p0, Lcom/mfluent/asp/sync/g$6;->m:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 487
    iget v0, p0, Lcom/mfluent/asp/sync/g$6;->n:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 488
    iget v0, p0, Lcom/mfluent/asp/sync/g$6;->o:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 489
    const/4 v0, 0x0

    .line 492
    if-eqz p3, :cond_5

    .line 493
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "_data"

    invoke-static {p3, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 494
    const-string v0, "width"

    invoke-static {p3, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    .line 495
    const-string v0, "height"

    invoke-static {p3, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    .line 497
    :goto_1
    invoke-static {v3, v5}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-ne v2, v6, :cond_3

    if-ne v0, v7, :cond_3

    move v0, v4

    .line 502
    :cond_1
    :goto_2
    if-eqz v0, :cond_4

    :goto_3
    return v1

    :cond_2
    move v0, v1

    .line 479
    goto :goto_0

    :cond_3
    move v0, v1

    .line 497
    goto :goto_2

    .line 502
    :cond_4
    const/4 v1, -0x1

    goto :goto_3

    :cond_5
    move v2, v1

    move-object v3, v0

    move v0, v1

    goto :goto_1
.end method

.method public final a(Landroid/content/ContentValues;Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 403
    const-string v1, "bucket_display_name"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "bucket_display_name"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const-string v1, "bucket_id"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "bucket_id"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    const-string v1, "datetaken"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "datetaken"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 406
    const-string v1, "description"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "description"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v1, "isprivate"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "isprivate"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 408
    const-string v1, "latitude"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "latitude"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 409
    const-string v1, "longitude"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "longitude"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 410
    const-string v1, "orientation"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "orientation"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 411
    const-string v1, "orientation"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "orientation"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 412
    const-string v1, "picasa_id"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "picasa_id"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    const-string v1, "date_added"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "date_added"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 414
    const-string v1, "date_modified"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "date_modified"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 415
    const-string v1, "_display_name"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "_display_name"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const-string v1, "mime_type"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "mime_type"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    const-string v1, "_size"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "_size"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 418
    const-string v1, "title"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "title"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const-string v1, "full_uri"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "_data"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/dws/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string v1, "_data"

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "_data"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    if-eqz p3, :cond_3

    .line 426
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "_data"

    invoke-static {p3, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 427
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "width"

    invoke-static {p3, v0}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 428
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "height"

    invoke-static {p3, v0}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 430
    :goto_0
    const-string v4, "thumb_data"

    invoke-virtual {p1, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    const-string v4, "thumb_width"

    invoke-virtual {p1, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 432
    const-string v1, "thumb_height"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 433
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    const-string v0, "thumbnail_uri"

    invoke-static {v2}, Lcom/mfluent/asp/dws/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :goto_1
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 444
    const-string v0, "width"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    .line 445
    const-string v0, "height"

    invoke-static {p2, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    .line 447
    :goto_2
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v0, "_data"

    invoke-static {p2, v0}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/g$6;->p:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 450
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$6;->p:Landroid/graphics/BitmapFactory$Options;

    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 451
    iget-object v0, p0, Lcom/mfluent/asp/sync/g$6;->p:Landroid/graphics/BitmapFactory$Options;

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 454
    :cond_0
    const-string v2, "width"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 455
    const-string v1, "height"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 457
    const-string v0, "group_id"

    iget-object v1, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v1, "group_id"

    invoke-static {p2, v1}, Lcom/mfluent/asp/sync/g;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    return-void

    .line 436
    :cond_1
    const-string v0, "thumbnail_uri"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v1/images/thumbnail/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mfluent/asp/sync/g$6;->a:Lcom/mfluent/asp/sync/g;

    const-string v2, "_id"

    invoke-static {p2, v2}, Lcom/mfluent/asp/sync/g;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move v0, v3

    move v1, v3

    goto :goto_2

    :cond_3
    move-object v1, v0

    move-object v2, v0

    goto/16 :goto_0
.end method
