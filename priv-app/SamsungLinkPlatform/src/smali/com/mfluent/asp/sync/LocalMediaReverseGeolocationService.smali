.class public Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;
.super Lcom/mfluent/asp/sync/BatchMediaProcessingService;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;,
        Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException;
    }
.end annotation


# static fields
.field private static final A:[Ljava/lang/String;

.field private static final B:[Ljava/lang/String;

.field private static final C:[[Ljava/lang/String;

.field private static final a:Lorg/slf4j/Logger;

.field private static final b:J

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static final q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final s:[Ljava/lang/String;

.field private static final t:[Ljava/lang/String;

.field private static final u:[Ljava/lang/String;

.field private static final v:[Ljava/lang/String;

.field private static final w:[Ljava/lang/String;

.field private static final x:[Ljava/lang/String;

.field private static final y:[Ljava/lang/String;

.field private static final z:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/Object;

.field private final d:J

.field private final h:Landroid/content/ContentValues;

.field private i:Ljava/util/Locale;

.field private j:Z

.field private k:Landroid/location/Geocoder;

.field private l:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

.field private m:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;

.field private n:Z

.field private final o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation
.end field

.field private p:I

.field private final r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 64
    const-class v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    .line 70
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x18

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b:J

    .line 80
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "geo_loc_country"

    aput-object v1, v0, v4

    const-string v1, "geo_loc_province"

    aput-object v1, v0, v5

    const-string v1, "geo_loc_sub_province"

    aput-object v1, v0, v6

    const-string v1, "geo_loc_locality"

    aput-object v1, v0, v7

    const-string v1, "geo_loc_sub_locality"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "geo_loc_feature"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "geo_loc_thoroughfare"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "geo_loc_sub_thoroughfare"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "geo_loc_premises"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->e:[Ljava/lang/String;

    .line 91
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "latitude"

    aput-object v1, v0, v5

    const-string v1, "longitude"

    aput-object v1, v0, v6

    const-string v1, "device_id"

    aput-object v1, v0, v7

    const-string v1, "source_media_id"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "geo_loc_locale"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->f:[Ljava/lang/String;

    .line 99
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "addr"

    aput-object v1, v0, v5

    const-string v1, "langagecode"

    aput-object v1, v0, v6

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->g:[Ljava/lang/String;

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->q:Ljava/util/HashMap;

    .line 767
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "country"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->s:[Ljava/lang/String;

    .line 768
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "administrative_area_level_1"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->t:[Ljava/lang/String;

    .line 769
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "administrative_area_level_2"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->u:[Ljava/lang/String;

    .line 770
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "locality"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->v:[Ljava/lang/String;

    .line 771
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sublocality"

    aput-object v1, v0, v4

    const-string v1, "sublocality_level_1"

    aput-object v1, v0, v5

    const-string v1, "sublocality_level_2"

    aput-object v1, v0, v6

    const-string v1, "sublocality_level_3"

    aput-object v1, v0, v7

    const-string v1, "sublocality_level_4"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "sublocality_level_5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "neighborhood"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "administrative_area_level_3"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->w:[Ljava/lang/String;

    .line 780
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "route"

    aput-object v1, v0, v4

    const-string v1, "street_address"

    aput-object v1, v0, v5

    const-string v1, "intersection"

    aput-object v1, v0, v6

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->x:[Ljava/lang/String;

    .line 781
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "street_number"

    aput-object v1, v0, v4

    const-string v1, "premises"

    aput-object v1, v0, v5

    const-string v1, "post_box"

    aput-object v1, v0, v6

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->y:[Ljava/lang/String;

    .line 782
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "postal_code"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->z:[Ljava/lang/String;

    .line 783
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "premises"

    aput-object v1, v0, v4

    const-string v1, "colloquial_area"

    aput-object v1, v0, v5

    const-string v1, "natural_feature"

    aput-object v1, v0, v6

    const-string v1, "point_of_interest"

    aput-object v1, v0, v7

    const-string v1, "airport"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "park"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "establishment"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "street_number"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->A:[Ljava/lang/String;

    .line 792
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "colloquial_area"

    aput-object v1, v0, v4

    const-string v1, "natural_feature"

    aput-object v1, v0, v5

    const-string v1, "point_of_interest"

    aput-object v1, v0, v6

    const-string v1, "airport"

    aput-object v1, v0, v7

    const-string v1, "park"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "establishment"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->B:[Ljava/lang/String;

    .line 794
    const/16 v0, 0xa

    new-array v0, v0, [[Ljava/lang/String;

    sget-object v1, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->s:[Ljava/lang/String;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->t:[Ljava/lang/String;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->u:[Ljava/lang/String;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->v:[Ljava/lang/String;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->w:[Ljava/lang/String;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->x:[Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->y:[Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->z:[Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->A:[Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->B:[Ljava/lang/String;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->C:[[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 115
    const-string v0, "LocalMediaReverseGeolocationService"

    invoke-direct {p0, v0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;-><init>(Ljava/lang/String;)V

    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->c:Ljava/lang/Object;

    .line 76
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->d:J

    .line 103
    iput-boolean v2, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->j:Z

    .line 107
    iput-boolean v2, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->n:Z

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->o:Ljava/util/HashMap;

    .line 110
    iput v2, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->r:Ljava/util/ArrayList;

    .line 117
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    .line 118
    return-void
.end method

.method private a(ILjava/lang/String;)Landroid/location/Address;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 518
    sget-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::getAddressFromMediaDb() mediaType:{} sourceMediaId:{} "

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2, p2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 522
    if-ne p1, v8, :cond_1

    .line 525
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 532
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->g:[Ljava/lang/String;

    aget-object v2, v2, v7

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " == ? AND (addr IS NOT NULL AND langagecode == ?)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 541
    new-array v4, v9, [Ljava/lang/String;

    aput-object p2, v4, v7

    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->i:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 543
    sget-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v2, "::getAddressFromMediaDb() target:{} where:{} whereArgs:{}"

    new-array v5, v10, [Ljava/lang/Object;

    aput-object v1, v5, v7

    aput-object v3, v5, v8

    aput-object v4, v5, v9

    invoke-interface {v0, v2, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 547
    :try_start_0
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->g:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 552
    if-eqz v1, :cond_6

    .line 553
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 554
    const-string v0, "addr"

    invoke-static {v1, v0}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 556
    const-string v2, "\\|"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 558
    sget-object v3, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v4, "::getAddressFromMediaDb() formattedAddress:{} Address Parts:{}"

    array-length v5, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v0, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 560
    array-length v0, v2

    const/16 v3, 0x9

    if-ge v0, v3, :cond_2

    .line 561
    sget-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v1, "::getAddressFromMediaDb() Invalid Address"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 585
    :cond_0
    :goto_1
    return-object v6

    .line 526
    :cond_1
    if-ne p1, v10, :cond_0

    .line 527
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 565
    :cond_2
    aget-object v0, v2, v7

    if-eqz v0, :cond_3

    aget-object v0, v2, v7

    const-string v3, "null"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 566
    :cond_3
    sget-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v1, "::getAddressFromMediaDb() Invalid Address"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1

    .line 570
    :cond_4
    new-instance v0, Landroid/location/Address;

    iget-object v3, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->i:Ljava/util/Locale;

    invoke-direct {v0, v3}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 572
    aget-object v3, v2, v7

    invoke-static {v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    .line 573
    aget-object v3, v2, v8

    invoke-static {v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/location/Address;->setAdminArea(Ljava/lang/String;)V

    .line 574
    aget-object v3, v2, v9

    invoke-static {v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/location/Address;->setSubAdminArea(Ljava/lang/String;)V

    .line 575
    aget-object v3, v2, v10

    invoke-static {v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    .line 576
    const/4 v3, 0x4

    aget-object v3, v2, v3

    invoke-static {v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/location/Address;->setSubLocality(Ljava/lang/String;)V

    .line 577
    const/4 v3, 0x5

    aget-object v3, v2, v3

    invoke-static {v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/location/Address;->setFeatureName(Ljava/lang/String;)V

    .line 578
    const/4 v3, 0x6

    aget-object v3, v2, v3

    invoke-static {v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/location/Address;->setThoroughfare(Ljava/lang/String;)V

    .line 579
    const/4 v3, 0x7

    aget-object v3, v2, v3

    invoke-static {v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/location/Address;->setSubThoroughfare(Ljava/lang/String;)V

    .line 580
    const/16 v3, 0x8

    aget-object v2, v2, v3

    invoke-static {v2}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/location/Address;->setPremises(Ljava/lang/String;)V

    .line 582
    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v3, ":: getAddressFromMediaDb() Found matching record in Media DB for sourceMediaId:{} address:{}"

    invoke-virtual {v0}, Landroid/location/Address;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, p2, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 584
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_3
    move-object v6, v0

    .line 585
    goto/16 :goto_1

    .line 549
    :catch_0
    move-exception v0

    goto/16 :goto_1

    :cond_5
    move-object v0, v6

    goto :goto_2

    :cond_6
    move-object v0, v6

    goto :goto_3
.end method

.method private static a(Ljava/lang/Double;)Ljava/lang/Double;
    .locals 4

    .prologue
    const-wide v2, 0x408f400000000000L    # 1000.0

    .line 164
    invoke-virtual {p0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 593
    const-string v0, "null"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    const/4 p0, 0x0

    .line 597
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/String;DD)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v3, 0x3a

    .line 137
    invoke-static {p0, v3}, Lorg/apache/commons/lang3/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v0

    .line 138
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    .line 139
    :cond_0
    const/4 v0, 0x0

    .line 142
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/Double;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/Double;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Locale;Ljava/lang/Double;Ljava/lang/Double;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x3a

    .line 129
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-nez p0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/Double;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/Double;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(DDLjava/util/Locale;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DD",
            "Ljava/util/Locale;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 707
    const/4 v0, 0x0

    .line 708
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://maps.googleapis.com/maps/api/geocode/json?latlng="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&sensor=true&language="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 710
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 711
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 713
    invoke-interface {v1, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 715
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_0

    .line 716
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Result status code is:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 719
    :cond_0
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 720
    const-string v2, "UTF-8"

    invoke-static {v1, v2}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 722
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 725
    const-string v1, "status"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 726
    const-string v3, "OK"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 727
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "JSON result is invalid:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 730
    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Not valid JSON response."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 733
    :cond_1
    const-string v1, "results"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 734
    if-eqz v2, :cond_4

    .line 735
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 736
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_3

    .line 737
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 738
    if-eqz v3, :cond_2

    .line 739
    new-instance v4, Landroid/location/Address;

    invoke-direct {v4, p4}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 743
    const-string v5, "address_components"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 744
    if-eqz v3, :cond_2

    .line 745
    invoke-static {v4, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Landroid/location/Address;Lorg/json/JSONArray;)V

    .line 746
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 736
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 752
    :cond_4
    return-object v0
.end method

.method private static a(Landroid/location/Address;Lorg/json/JSONArray;)V
    .locals 7

    .prologue
    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 807
    new-array v4, v3, [I

    move v0, v1

    .line 809
    :goto_0
    if-ge v0, v3, :cond_0

    .line 810
    const v2, 0x7fffffff

    aput v2, v4, v0

    .line 809
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 813
    :goto_1
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 814
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 815
    if-eqz v3, :cond_3

    .line 816
    const-string v2, "long_name"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 819
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 820
    const-string v2, "short_name"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 823
    :cond_1
    invoke-static {v2}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 824
    const-string v5, "types"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 825
    if-eqz v5, :cond_2

    move v3, v1

    .line 826
    :goto_2
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_3

    .line 827
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    .line 828
    invoke-static {p0, v4, v2, v6}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Landroid/location/Address;[ILjava/lang/String;Ljava/lang/String;)V

    .line 826
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 831
    :cond_2
    const-string v5, "types"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 832
    invoke-static {p0, v4, v2, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Landroid/location/Address;[ILjava/lang/String;Ljava/lang/String;)V

    .line 813
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 837
    :cond_4
    return-void
.end method

.method private static a(Landroid/location/Address;[ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 902
    invoke-static {p3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 913
    :cond_0
    return-void

    .line 906
    :cond_1
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 907
    sget-object v1, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->C:[[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-static {v1, p3}, Lorg/apache/commons/lang3/ArrayUtils;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 908
    if-ltz v1, :cond_2

    aget v2, p1, v0

    if-gt v1, v2, :cond_2

    .line 909
    aput v1, p1, v0

    .line 910
    packed-switch v0, :pswitch_data_0

    .line 906
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 910
    :pswitch_0
    invoke-virtual {p0, p2}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0, p2}, Landroid/location/Address;->setAdminArea(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0, p2}, Landroid/location/Address;->setSubAdminArea(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0, p2}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0, p2}, Landroid/location/Address;->setSubLocality(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_5
    invoke-virtual {p0, p2}, Landroid/location/Address;->setThoroughfare(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_6
    invoke-virtual {p0, p2}, Landroid/location/Address;->setSubThoroughfare(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_7
    invoke-virtual {p0, p2}, Landroid/location/Address;->setPostalCode(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_8
    invoke-virtual {p0, p2}, Landroid/location/Address;->setPremises(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_9
    invoke-virtual {p0, p2}, Landroid/location/Address;->setFeatureName(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z
    .locals 2

    .prologue
    .line 697
    invoke-static {p1}, Lorg/apache/commons/lang3/StringUtils;->trimToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 698
    invoke-virtual {p2, p0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 699
    invoke-virtual {p2, p0, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    const/4 v0, 0x1

    .line 703
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/Double;)Ljava/lang/Double;
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    .line 169
    invoke-virtual {p0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 677
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 678
    :cond_0
    const-string p0, "null"

    .line 680
    :cond_1
    return-object p0
.end method

.method public static b(Ljava/lang/String;DD)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v3, 0x3a

    .line 150
    invoke-static {p0, v3}, Lorg/apache/commons/lang3/StringUtils;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v0

    .line 151
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    .line 152
    :cond_0
    sget-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v1, "::convertEncodingToDecimalPlaces() - Invalid format oldLocalEncoding:{}"

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 153
    const/4 v0, 0x0

    .line 156
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/Double;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/Double;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentResolver;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 457
    sget-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::postProcessBatchByCursor()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 460
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 461
    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->r:Ljava/util/ArrayList;

    sget-object v1, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v2, "Enter ::updateGeoLocationTableWithLocation() - geoLocations size:{}"

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->d()Landroid/location/Address;

    move-result-object v4

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->a()D

    move-result-wide v6

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->b()D

    move-result-wide v8

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v5, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v10, "Enter ::updateContentValuesForGeoLocColumns()"

    invoke-interface {v5, v10}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    const-string v5, "latitude"

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v5, "longitude"

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v5, "geo_loc_locale"

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "geo_loc_country"

    invoke-virtual {v4}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    const-string v0, "geo_loc_province"

    invoke-virtual {v4}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    const-string v0, "geo_loc_sub_province"

    invoke-virtual {v4}, Landroid/location/Address;->getSubAdminArea()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    const-string v0, "geo_loc_locality"

    invoke-virtual {v4}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    const-string v0, "geo_loc_sub_locality"

    invoke-virtual {v4}, Landroid/location/Address;->getSubLocality()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    const-string v0, "geo_loc_feature"

    invoke-virtual {v4}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    const-string v0, "geo_loc_thoroughfare"

    invoke-virtual {v4}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    const-string v0, "geo_loc_sub_thoroughfare"

    invoke-virtual {v4}, Landroid/location/Address;->getSubThoroughfare()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    const-string v0, "geo_loc_premises"

    invoke-virtual {v4}, Landroid/location/Address;->getPremises()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$GeoLocation;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 464
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    throw v0

    .line 461
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    sget-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v2, "::updateGeoLocationTableWithLocation() - No of records:{}"

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 464
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 465
    return-void
.end method

.method protected final a(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 191
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Ljava/lang/Thread;->setPriority(I)V

    .line 193
    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    iput-object v1, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->m:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;

    .line 202
    iput-object v1, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->l:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    .line 203
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;

    iget v0, v0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->c:I

    iput v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    .line 205
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;

    iget v0, v0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->c:I

    packed-switch v0, :pswitch_data_0

    .line 216
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->m:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;

    if-eqz v0, :cond_0

    .line 220
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->i:Ljava/util/Locale;

    .line 221
    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 222
    iput-boolean v3, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->n:Z

    .line 223
    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/location/Geocoder;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->i:Ljava/util/Locale;

    invoke-direct {v0, v1, v4}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    :goto_2
    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->k:Landroid/location/Geocoder;

    .line 224
    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->k:Landroid/location/Geocoder;

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->j:Z

    .line 227
    :try_start_0
    const-string v4, "geo_loc_last_timestamp IS NULL OR geo_loc_last_timestamp<=?"

    .line 231
    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v6, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b:J

    sub-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    .line 233
    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->m:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;

    invoke-interface {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetContentUri()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->f:[Ljava/lang/String;

    const-string v6, "latitude,longitude"

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/util/ArrayList;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->n:Z

    if-eqz v0, :cond_0

    .line 242
    const-class v0, Lcom/mfluent/asp/sync/h;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/h;

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/h;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 244
    :catch_0
    move-exception v0

    .line 245
    sget-object v1, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v2, "Failed to process media batch."

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 207
    :pswitch_1
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;

    invoke-direct {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->m:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;

    .line 208
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Journal;

    invoke-direct {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Journal;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->l:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    goto :goto_1

    .line 211
    :pswitch_2
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;

    invoke-direct {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->m:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;

    .line 212
    new-instance v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Journal;

    invoke-direct {v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Journal;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->l:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    goto/16 :goto_1

    :cond_2
    move-object v0, v1

    .line 223
    goto :goto_2

    :cond_3
    move v0, v3

    .line 224
    goto :goto_3

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(Ljava/util/HashMap;Landroid/content/ContentResolver;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/location/Address;",
            ">;",
            "Landroid/content/ContentResolver;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x1

    .line 604
    sget-object v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v1, "Enter ::updateMediaDbRecordWithLocation - recordIdAddressMap Size:{} MediaType:{}"

    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 606
    invoke-virtual {p1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 645
    :cond_0
    :goto_0
    return-void

    .line 610
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 611
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 613
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 615
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 616
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Address;

    .line 618
    const-string v6, "addr"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "|"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "|"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/location/Address;->getSubAdminArea()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "|"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "|"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/location/Address;->getSubLocality()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "|"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "|"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "|"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/location/Address;->getSubThoroughfare()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "|"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/location/Address;->getPremises()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v8, "getFormattedAddress() Formatted Address:{}"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v8, v9}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1, v5}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    .line 619
    const-string v1, "langagecode"

    iget-object v6, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->i:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6, v5}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    .line 621
    iget v1, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    if-ne v1, v10, :cond_2

    .line 623
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 624
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 632
    :goto_2
    sget-object v1, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v6, "::updateMediaDbRecordWithLocation() MediaType:{} URI:{} contentValues:{}"

    new-array v7, v11, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    aput-object v0, v7, v10

    const/4 v8, 0x2

    aput-object v5, v7, v8

    invoke-interface {v1, v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 634
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 625
    :cond_2
    iget v1, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    if-ne v1, v11, :cond_0

    .line 626
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 627
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_2

    .line 637
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 638
    const-string v0, "media"

    invoke-virtual {p2, v0, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 639
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 642
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 643
    iget v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    if-ne v0, v10, :cond_5

    const-string v0, "content://media/external/images/media"

    :goto_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/mfluent/asp/util/a;->a(Landroid/net/Uri;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    :cond_5
    if-ne v0, v11, :cond_0

    const-string v0, "content://media/external/video/media"

    goto :goto_3
.end method

.method protected final a(Ljava/util/HashMap;Ljava/util/ArrayList;Landroid/database/Cursor;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/location/Address;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 263
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 265
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->c:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->c:Ljava/lang/Object;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 267
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 271
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 452
    :cond_0
    :goto_1
    return-void

    .line 267
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3

    throw v2
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 270
    :catch_0
    move-exception v2

    goto :goto_0

    .line 276
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 277
    const/4 v14, 0x0

    .line 280
    const-wide v10, 0x40b3880000000000L    # 5000.0

    .line 281
    const-wide v12, 0x40b3880000000000L    # 5000.0

    .line 282
    const/4 v2, 0x1

    :try_start_3
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 283
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v10

    .line 285
    :cond_2
    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 286
    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v12

    .line 289
    :cond_3
    const/4 v2, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 290
    const/4 v2, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 291
    new-instance v20, Landroid/util/Pair;

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-direct {v0, v2, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 293
    const-wide v4, 0x4066800000000000L    # 180.0

    cmpl-double v2, v12, v4

    if-gtz v2, :cond_4

    const-wide v4, -0x3f99800000000000L    # -180.0

    cmpg-double v2, v12, v4

    if-ltz v2, :cond_4

    const-wide v4, 0x4056800000000000L    # 90.0

    cmpl-double v2, v10, v4

    if-gtz v2, :cond_4

    const-wide v4, -0x3fa9800000000000L    # -90.0

    cmpg-double v2, v10, v4

    if-ltz v2, :cond_4

    const-wide/16 v4, 0x0

    cmpl-double v2, v10, v4

    if-nez v2, :cond_5

    const-wide/16 v4, 0x0

    cmpl-double v2, v12, v4

    if-nez v2, :cond_5

    :cond_4
    const/4 v2, 0x0

    :goto_2
    if-nez v2, :cond_8

    .line 294
    new-instance v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_ID:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Latitude and/or longitude are invalid. Lat:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " long:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 443
    :catch_1
    move-exception v2

    move-object v3, v14

    .line 444
    :goto_3
    :try_start_4
    sget-object v4, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Failed to get geo-location for file id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 448
    if-eqz v3, :cond_0

    .line 449
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 293
    :cond_5
    :try_start_5
    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->q:Ljava/util/HashMap;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    sget-object v4, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Enter ::isMapCoordinateValid() count:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v4, 0x2

    if-ge v2, v4, :cond_7

    :cond_6
    const/4 v2, 0x1

    goto :goto_2

    :cond_7
    const/4 v2, 0x0

    goto :goto_2

    .line 297
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->i:Ljava/util/Locale;

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/util/Locale;Ljava/lang/Double;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v8

    .line 303
    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v4, "oldEncodedValue:{} newEncodedValue:{}"

    invoke-interface {v2, v4, v3, v8}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 305
    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 306
    new-instance v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No need to re-do reverse geolocation. Encoded values are the same:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 445
    :catch_2
    move-exception v2

    move-object v15, v14

    .line 446
    :goto_4
    :try_start_6
    sget-object v3, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    invoke-virtual {v2}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 448
    if-eqz v15, :cond_0

    .line 449
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 312
    :cond_9
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    const-string v3, "geo_loc_locale"

    invoke-virtual {v2, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const/16 v16, 0x0

    .line 317
    const/4 v15, 0x1

    .line 319
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->o:Ljava/util/HashMap;

    invoke-virtual {v2, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/location/Address;

    move-object v9, v0

    .line 320
    if-nez v9, :cond_d

    .line 322
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$GeoLocation;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->e:[Ljava/lang/String;

    const-string v5, "geo_loc_locale=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v8, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-object v3

    .line 329
    if-eqz v3, :cond_18

    .line 330
    :try_start_8
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 331
    new-instance v9, Landroid/location/Address;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->i:Ljava/util/Locale;

    invoke-direct {v9, v2}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 333
    const-string v2, "geo_loc_country"

    invoke-static {v3, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    .line 334
    const-string v2, "geo_loc_province"

    invoke-static {v3, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/location/Address;->setAdminArea(Ljava/lang/String;)V

    .line 335
    const-string v2, "geo_loc_sub_province"

    invoke-static {v3, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/location/Address;->setSubAdminArea(Ljava/lang/String;)V

    .line 336
    const-string v2, "geo_loc_locality"

    invoke-static {v3, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    .line 337
    const-string v2, "geo_loc_sub_locality"

    invoke-static {v3, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/location/Address;->setSubLocality(Ljava/lang/String;)V

    .line 338
    const-string v2, "geo_loc_feature"

    invoke-static {v3, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/location/Address;->setFeatureName(Ljava/lang/String;)V

    .line 339
    const-string v2, "geo_loc_thoroughfare"

    invoke-static {v3, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/location/Address;->setThoroughfare(Ljava/lang/String;)V

    .line 340
    const-string v2, "geo_loc_sub_thoroughfare"

    invoke-static {v3, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/location/Address;->setSubThoroughfare(Ljava/lang/String;)V

    .line 341
    const-string v2, "geo_loc_premises"

    invoke-static {v3, v2}, Lcom/mfluent/asp/common/util/CursorUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/location/Address;->setPremises(Ljava/lang/String;)V

    .line 343
    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v4, "Found matching record in DB for encoding:{}"

    invoke-interface {v2, v4, v8}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 345
    :cond_a
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_9
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 346
    const/4 v14, 0x0

    .line 348
    :goto_5
    if-nez v9, :cond_17

    .line 350
    :try_start_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v2, v1}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(ILjava/lang/String;)Landroid/location/Address;

    move-result-object v9

    .line 351
    if-eqz v9, :cond_17

    .line 352
    new-instance v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;

    move-object/from16 v3, p0

    move-wide v4, v10

    move-wide v6, v12

    invoke-direct/range {v2 .. v9}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;-><init>(Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;DDLjava/lang/String;Landroid/location/Address;)V

    .line 353
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->r:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_a
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_9 .. :try_end_9} :catch_8
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 354
    const/4 v2, 0x0

    move/from16 v17, v2

    move-object v15, v14

    .line 361
    :goto_6
    if-eqz v9, :cond_16

    .line 362
    :try_start_a
    new-instance v3, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 363
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    :goto_7
    if-nez v3, :cond_c

    .line 367
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->j:Z
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    if-nez v2, :cond_b

    .line 369
    :try_start_b
    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v4, "Doing far call using Geocoder for lat:{} long:{};"

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-interface {v2, v4, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 370
    move-object/from16 v0, p0

    iget v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    sget-object v4, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v5, "Enter ::logAnalyticsDataForGeocoderCall mediaType:{}"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    packed-switch v2, :pswitch_data_0

    .line 371
    :goto_8
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->k:Landroid/location/Geocoder;

    const/4 v14, 0x1

    invoke-virtual/range {v9 .. v14}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-object v14

    .line 373
    if-eqz v14, :cond_f

    :try_start_c
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_f

    .line 374
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/location/Address;

    .line 375
    new-instance v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;

    move-object/from16 v3, p0

    move-wide v4, v10

    move-wide v6, v12

    invoke-direct/range {v2 .. v9}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;-><init>(Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;DDLjava/lang/String;Landroid/location/Address;)V

    .line 376
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->r:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    move-object/from16 v0, p0

    iget v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    sget-object v3, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v4, "Enter ::logAnalyticsDataForGeocoderCallSuccess mediaType:{}"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    packed-switch v2, :pswitch_data_1

    :goto_9
    :pswitch_1
    move-object v3, v14

    .line 394
    :cond_b
    :goto_a
    if-nez v3, :cond_c

    :try_start_d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->j:Z

    if-eqz v2, :cond_c

    .line 395
    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v3, "Doing far call using Google Maps Api for lat:{} long{};"

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 396
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->i:Ljava/util/Locale;

    invoke-static {v10, v11, v12, v13, v2}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(DDLjava/util/Locale;)Ljava/util/List;

    move-result-object v3

    .line 401
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 402
    new-instance v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException;

    const-string v3, "Service is destroyed. Bailing out."

    invoke-direct {v2, v3}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 443
    :catch_3
    move-exception v2

    move-object v3, v15

    goto/16 :goto_3

    .line 358
    :cond_d
    :try_start_e
    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v3, "Found matching record in memory for encoding:{}"

    invoke-interface {v2, v3, v8}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_1
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    move/from16 v17, v15

    move-object v15, v14

    goto/16 :goto_6

    .line 370
    :pswitch_2
    :try_start_f
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v4, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->F:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v2, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_4
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_8

    .line 382
    :catch_4
    move-exception v2

    .line 383
    :goto_b
    :try_start_10
    sget-object v4, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v5, "Exception : {}"

    invoke-interface {v4, v5, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 384
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Service not Available"

    invoke-static {v4, v5}, Lorg/apache/commons/lang3/StringUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v4

    .line 385
    if-ltz v4, :cond_12

    .line 386
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->j:Z

    .line 387
    move-object/from16 v0, p0

    iget v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    sget-object v4, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v5, "Enter ::logAnalyticsDataForGeocoderCallExcServiceNotAvail mediaType:{}"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    packed-switch v2, :pswitch_data_2

    :pswitch_3
    goto :goto_a

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v4, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->K:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v2, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_3
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_10 .. :try_end_10} :catch_5
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto/16 :goto_a

    .line 445
    :catch_5
    move-exception v2

    goto/16 :goto_4

    .line 370
    :pswitch_5
    :try_start_11
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v4, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->H:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v2, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_4
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_11 .. :try_end_11} :catch_5
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/16 :goto_8

    .line 448
    :catchall_1
    move-exception v2

    :goto_c
    if-eqz v15, :cond_e

    .line 449
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v2

    .line 377
    :pswitch_6
    :try_start_12
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v4, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->I:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v2, v3, v4}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    move-object v3, v14

    goto/16 :goto_a

    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v4, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->M:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v2, v3, v4}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_9

    .line 382
    :catch_6
    move-exception v2

    move-object v3, v14

    goto :goto_b

    .line 379
    :cond_f
    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v3, "getFromLocation() returns no address for lat:{} long:{}"

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 380
    sget-object v2, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->q:Ljava/util/HashMap;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    sget-object v3, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v4, "::handleEmptyAddressFromGeocoderAPI() entryId:{} count:{}"

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    if-nez v2, :cond_11

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->q:Ljava/util/HashMap;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_10
    :goto_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    sget-object v3, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v4, "Enter ::logAnalyticsDataForGeocoderCallReturnEmpty mediaType:{}"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    packed-switch v2, :pswitch_data_3

    :goto_e
    :pswitch_8
    move-object v3, v14

    .line 392
    goto/16 :goto_a

    .line 380
    :cond_11
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lez v3, :cond_10

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->q:Ljava/util/HashMap;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v4, "::handleEmptyAddressFromGeocoderAPI() invalid coord for entry:{} newCount:{}"

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5, v2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "geo_is_valid_map_coord"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Files;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_d

    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v4, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->J:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v2, v3, v4}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    move-object v3, v14

    goto/16 :goto_a

    :pswitch_a
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v4, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->N:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v2, v3, v4}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_6
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_12 .. :try_end_12} :catch_5
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    goto :goto_e

    .line 387
    :pswitch_b
    :try_start_13
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v4, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->O:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v2, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto/16 :goto_a

    .line 389
    :cond_12
    move-object/from16 v0, p0

    iget v3, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->p:I

    sget-object v4, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v5, "Enter ::logAnalyticsDataForGeocoderCallExcOthers mediaType:{}"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    packed-switch v3, :pswitch_data_4

    .line 390
    :goto_f
    :pswitch_c
    throw v2

    .line 389
    :pswitch_d
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->L:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v3, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_f

    :pswitch_e
    invoke-virtual/range {p0 .. p0}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v5, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->P:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v3, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger;->a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z

    goto :goto_f

    .line 404
    :cond_13
    if-eqz v3, :cond_14

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_14

    .line 405
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Address;

    .line 407
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->o:Ljava/util/HashMap;

    invoke-virtual {v3, v8, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    sget-object v3, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a:Lorg/slf4j/Logger;

    const-string v4, "Geocoder location for ID:{}"

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 413
    const-string v3, "geo_loc_country"

    invoke-virtual {v2}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-static {v3, v4, v5}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v3

    or-int/lit8 v3, v3, 0x0

    .line 414
    const-string v4, "geo_loc_province"

    invoke-virtual {v2}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-static {v4, v5, v6}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 415
    const-string v4, "geo_loc_sub_province"

    invoke-virtual {v2}, Landroid/location/Address;->getSubAdminArea()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-static {v4, v5, v6}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 416
    const-string v4, "geo_loc_locality"

    invoke-virtual {v2}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-static {v4, v5, v6}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 417
    const-string v4, "geo_loc_sub_locality"

    invoke-virtual {v2}, Landroid/location/Address;->getSubLocality()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-static {v4, v5, v6}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 418
    invoke-virtual {v2}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Landroid/location/Address;->getSubThoroughfare()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/apache/commons/lang3/ObjectUtils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 419
    const-string v4, "geo_loc_feature"

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-static {v4, v5, v6}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 423
    :goto_10
    const-string v4, "geo_loc_thoroughfare"

    invoke-virtual {v2}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-static {v4, v5, v6}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 424
    const-string v4, "geo_loc_sub_thoroughfare"

    invoke-virtual {v2}, Landroid/location/Address;->getSubThoroughfare()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-static {v4, v5, v6}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 425
    const-string v4, "geo_loc_premises"

    invoke-virtual {v2}, Landroid/location/Address;->getPremises()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-static {v4, v5, v6}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v4

    or-int/2addr v3, v4

    .line 427
    if-eqz v3, :cond_14

    .line 428
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->m:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;

    move/from16 v0, v18

    int-to-long v4, v0

    invoke-interface {v3, v4, v5}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$UriProvider;->instanceGetEntryUri(J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-virtual {v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->l:Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/mfluent/asp/datamodel/al;->b(Lcom/mfluent/asp/common/datamodel/ASPMediaStore$JournalColumns;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->n:Z

    .line 437
    if-eqz v17, :cond_14

    .line 438
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_3
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_13 .. :try_end_13} :catch_5
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 448
    :cond_14
    if-eqz v15, :cond_0

    .line 449
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 421
    :cond_15
    :try_start_14
    const-string v4, "geo_loc_feature"

    invoke-virtual {v2}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->h:Landroid/content/ContentValues;

    invoke-static {v4, v5, v6}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_3
    .catch Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$ShortCircuitException; {:try_start_14 .. :try_end_14} :catch_5
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    move-result v4

    or-int/2addr v3, v4

    goto/16 :goto_10

    .line 448
    :catchall_2
    move-exception v2

    move-object v15, v14

    goto/16 :goto_c

    :catchall_3
    move-exception v2

    move-object v15, v3

    goto/16 :goto_c

    :catchall_4
    move-exception v2

    move-object v15, v14

    goto/16 :goto_c

    .line 445
    :catch_7
    move-exception v2

    move-object v15, v3

    goto/16 :goto_4

    :catch_8
    move-exception v2

    move-object v15, v14

    goto/16 :goto_4

    .line 443
    :catch_9
    move-exception v2

    goto/16 :goto_3

    :catch_a
    move-exception v2

    move-object v3, v14

    goto/16 :goto_3

    :cond_16
    move-object/from16 v3, v16

    goto/16 :goto_7

    :cond_17
    move/from16 v17, v15

    move-object v15, v14

    goto/16 :goto_6

    :cond_18
    move-object v14, v3

    goto/16 :goto_5

    .line 370
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 377
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_1
        :pswitch_7
    .end packed-switch

    .line 387
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_b
    .end packed-switch

    .line 380
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_a
    .end packed-switch

    .line 389
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_e
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 179
    iget-object v1, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 180
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 181
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    invoke-super {p0}, Lcom/mfluent/asp/sync/BatchMediaProcessingService;->onDestroy()V

    .line 183
    return-void

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
