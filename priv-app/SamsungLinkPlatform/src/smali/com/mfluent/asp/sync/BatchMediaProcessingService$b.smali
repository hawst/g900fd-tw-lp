.class public final Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/sync/BatchMediaProcessingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "b"
.end annotation


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:I


# direct methods
.method public constructor <init>(JLjava/lang/String;I)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-wide p1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->a:J

    .line 42
    iput-object p3, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->b:Ljava/lang/String;

    .line 43
    iput p4, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->c:I

    .line 44
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 53
    if-ne p1, p0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v0

    .line 56
    :cond_1
    check-cast p1, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;

    .line 57
    iget-wide v2, p1, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->a:J

    iget-wide v4, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    iget-object v1, p1, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->a:J

    const/16 v2, 0x18

    shl-long/2addr v0, v2

    long-to-int v0, v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/BatchMediaProcessingService$b;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method
