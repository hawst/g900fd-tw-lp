.class public Lcom/mfluent/asp/sync/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Lorg/slf4j/Logger;

.field private static c:Lcom/mfluent/asp/sync/d;


# instance fields
.field private final d:Landroid/content/Context;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    const-class v0, Lcom/mfluent/asp/sync/d;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/d;->b:Lorg/slf4j/Logger;

    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/sync/d;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_ACTION_CLOUD_ENABLED_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/d;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/d;->d:Landroid/content/Context;

    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/sync/d;->a(Z)V

    .line 35
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/mfluent/asp/sync/d;
    .locals 2

    .prologue
    .line 25
    const-class v1, Lcom/mfluent/asp/sync/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mfluent/asp/sync/d;->c:Lcom/mfluent/asp/sync/d;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/mfluent/asp/sync/d;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/d;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/sync/d;->c:Lcom/mfluent/asp/sync/d;

    .line 28
    :cond_0
    sget-object v0, Lcom/mfluent/asp/sync/d;->c:Lcom/mfluent/asp/sync/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized a(Z)V
    .locals 3

    .prologue
    .line 46
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/d;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.permission.PRIVATE_ACCESS"

    const-string v2, "com.sec.pcw"

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 47
    if-nez v0, :cond_1

    .line 49
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/d;->e:Z

    if-nez v0, :cond_0

    .line 50
    sget-object v0, Lcom/mfluent/asp/sync/d;->b:Lorg/slf4j/Logger;

    const-string v1, "Enabling cloud storage sync"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/d;->e:Z

    .line 52
    if-eqz p1, :cond_0

    .line 53
    invoke-direct {p0}, Lcom/mfluent/asp/sync/d;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 58
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/d;->e:Z

    if-eqz v0, :cond_0

    .line 59
    sget-object v0, Lcom/mfluent/asp/sync/d;->b:Lorg/slf4j/Logger;

    const-string v1, "Disabling cloud storage sync"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mfluent/asp/sync/d;->e:Z

    .line 61
    if-eqz p1, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/mfluent/asp/sync/d;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/mfluent/asp/sync/d;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/mfluent/asp/sync/d;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 70
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/d;->e:Z

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mfluent/asp/sync/d;->a(Z)V

    .line 43
    return-void
.end method
