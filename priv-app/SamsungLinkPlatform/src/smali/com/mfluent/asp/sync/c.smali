.class public final Lcom/mfluent/asp/sync/c;
.super Lcom/mfluent/asp/sync/f;
.source "SourceFile"


# static fields
.field private static b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static e:Z

.field private static f:J


# instance fields
.field private final g:Landroid/content/ContentResolver;

.field private final h:Lcom/mfluent/asp/sync/a;

.field private final i:Lcom/mfluent/asp/sync/b;

.field private j:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/sync/c;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 45
    const/4 v0, 0x1

    sput-boolean v0, Lcom/mfluent/asp/sync/c;->e:Z

    .line 47
    sget-wide v0, Lcom/mfluent/asp/nts/a;->f:J

    sput-wide v0, Lcom/mfluent/asp/sync/c;->f:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/mfluent/asp/sync/f;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V

    .line 55
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/c;->j:J

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/c;->g:Landroid/content/ContentResolver;

    .line 62
    new-instance v0, Lcom/mfluent/asp/sync/a;

    iget-object v1, p0, Lcom/mfluent/asp/sync/c;->g:Landroid/content/ContentResolver;

    invoke-direct {v0, p2, v1}, Lcom/mfluent/asp/sync/a;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/c;->h:Lcom/mfluent/asp/sync/a;

    .line 63
    new-instance v0, Lcom/mfluent/asp/sync/b;

    iget-object v1, p0, Lcom/mfluent/asp/sync/c;->g:Landroid/content/ContentResolver;

    invoke-direct {v0, p2, v1}, Lcom/mfluent/asp/sync/b;-><init>(Lcom/mfluent/asp/datamodel/Device;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/c;->i:Lcom/mfluent/asp/sync/b;

    .line 64
    return-void
.end method

.method private static a(Lorg/json/JSONObject;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;
    .locals 3

    .prologue
    .line 364
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 367
    :try_start_0
    const-string v1, "networkStatus"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 369
    const-string v2, "3G"

    invoke-static {v1, v2}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 370
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_3G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    .line 382
    :goto_0
    return-object v0

    .line 371
    :cond_0
    const-string v2, "BLUETOOTH"

    invoke-static {v1, v2}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 372
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->BLUETOOTH:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    goto :goto_0

    .line 373
    :cond_1
    const-string v2, "WiFi"

    invoke-static {v1, v2}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 374
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    goto :goto_0

    .line 376
    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 378
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private f()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v2, 0x1

    const-wide/16 v4, -0x1

    const/4 v10, 0x0

    const/4 v1, 0x0

    .line 142
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "/api/pCloud/device/info?_="

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 150
    if-nez v6, :cond_1

    .line 151
    sget-object v0, Lcom/mfluent/asp/sync/c;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x5

    if-gt v0, v1, :cond_0

    .line 152
    const-string v0, "mfl_ASPDeviceMetadataSyncManager"

    const-string v1, "::updateDeviceInfo:Trouble fetching device info - got null respoonse"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 143
    :catch_0
    move-exception v0

    .line 144
    sget-object v1, Lcom/mfluent/asp/sync/c;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x5

    if-gt v1, v2, :cond_0

    .line 145
    const-string v1, "mfl_ASPDeviceMetadataSyncManager"

    const-string v2, "::updateDeviceInfo:Trouble fetching device info"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 160
    :cond_1
    const-string v0, "zCapabilities"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 161
    if-nez v0, :cond_2

    .line 162
    const-string v0, "capabilities"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 165
    :cond_2
    if-eqz v0, :cond_8

    .line 166
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->u()Z

    move-result v3

    if-nez v3, :cond_3

    .line 167
    const-string v3, "syncServer"

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 168
    if-eqz v3, :cond_3

    .line 169
    const-string v3, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Device "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " has been upgraded to support sync"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/mfluent/asp/datamodel/Device;->c(Z)V

    .line 171
    const-string v3, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Deleting all metadata for device "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mfluent/asp/sync/c;->g:Landroid/content/ContentResolver;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    int-to-long v8, v7

    invoke-static {v8, v9}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Images$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v3, v7, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mfluent/asp/sync/c;->g:Landroid/content/ContentResolver;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    int-to-long v8, v7

    invoke-static {v8, v9}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Video$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v3, v7, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mfluent/asp/sync/c;->g:Landroid/content/ContentResolver;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    int-to-long v8, v7

    invoke-static {v8, v9}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v3, v7, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mfluent/asp/sync/c;->g:Landroid/content/ContentResolver;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v7

    int-to-long v8, v7

    invoke-static {v8, v9}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v3, v7, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 175
    :cond_3
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->z()Z

    move-result v3

    if-nez v3, :cond_4

    .line 176
    const-string v3, "supportsPush"

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 177
    if-eqz v3, :cond_4

    .line 178
    const-string v3, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Device "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " has been upgraded to support push notifications"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/mfluent/asp/datamodel/Device;->f(Z)V

    .line 183
    :cond_4
    const-string v3, "3boxTransferDevice"

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 184
    iget-object v7, p0, Lcom/mfluent/asp/sync/c;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7, v3}, Lcom/mfluent/asp/datamodel/Device;->g(Z)V

    .line 186
    const-string v3, "3boxTransferStorage"

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 189
    iget-object v7, p0, Lcom/mfluent/asp/sync/c;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7, v3}, Lcom/mfluent/asp/datamodel/Device;->h(Z)V

    .line 191
    const-string v3, "AutoArchive"

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 192
    iget-object v7, p0, Lcom/mfluent/asp/sync/c;->a:Lcom/mfluent/asp/datamodel/Device;

    invoke-virtual {v7, v3}, Lcom/mfluent/asp/datamodel/Device;->i(Z)V

    .line 194
    const-string v3, "mediaTypes"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 195
    if-eqz v7, :cond_8

    .line 196
    const-class v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v8

    move v0, v1

    .line 199
    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_6

    .line 201
    :try_start_1
    invoke-virtual {v7, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 202
    invoke-static {v3}, Lcom/mfluent/asp/dws/a$e;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    move-result-object v3

    .line 203
    if-eqz v3, :cond_5

    .line 204
    invoke-interface {v8, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 199
    :cond_5
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 206
    :catch_1
    move-exception v3

    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 215
    :cond_6
    sget-boolean v0, Lcom/mfluent/asp/sync/c;->e:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, v3, :cond_7

    .line 216
    sget-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->d:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_7
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/mfluent/asp/datamodel/Device;->a(Ljava/util/Set;)V

    .line 225
    :cond_8
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->PC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, v3, :cond_f

    .line 226
    const-string v0, "pc"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    move-object v6, v0

    .line 235
    :goto_3
    if-eqz v6, :cond_15

    .line 236
    const-string v0, "totalMemory"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 237
    if-eqz v0, :cond_a

    .line 238
    const-string v3, "total"

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 239
    cmp-long v3, v8, v4

    if-eqz v3, :cond_9

    .line 240
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Lcom/mfluent/asp/datamodel/Device;->setCapacityInBytes(J)V

    .line 243
    :cond_9
    const-string v3, "used"

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 244
    cmp-long v0, v8, v4

    if-eqz v0, :cond_a

    .line 245
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Lcom/mfluent/asp/datamodel/Device;->setUsedCapacityInBytes(J)V

    .line 249
    :cond_a
    const-string v0, "remoteWakeupMode"

    const-string v3, "OFF"

    invoke-virtual {v6, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 250
    iget-object v3, p0, Lcom/mfluent/asp/sync/c;->a:Lcom/mfluent/asp/datamodel/Device;

    const-string v7, "ON"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/datamodel/Device;->o(Z)V

    .line 252
    const-string v0, "KLiteCodecInstalled"

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 253
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/datamodel/Device;->d(Z)V

    .line 255
    const-string v0, "isTSServer"

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 256
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/datamodel/Device;->e(Z)V

    .line 258
    const-string v0, "udnWiFi"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 259
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 260
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Ljava/lang/String;)V

    .line 263
    :cond_b
    const-string v0, "udnWiFiDirect"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 264
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 265
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/mfluent/asp/datamodel/Device;->b(Ljava/lang/String;)V

    .line 268
    :cond_c
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    const-string v3, "is4GDevice"

    invoke-virtual {v6, v3, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Z)V

    .line 270
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    const-string v3, "batteryPercent"

    const/4 v7, -0x1

    invoke-virtual {v6, v3, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/Device;->e(I)V

    .line 271
    const/4 v0, -0x1

    .line 272
    const-string v3, "batteryCharging"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 273
    const-string v0, "batteryCharging"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    move v0, v2

    .line 275
    :cond_d
    :goto_4
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mfluent/asp/datamodel/Device;->a(B)V

    .line 278
    const-string v0, "driveName"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 280
    const-string v0, "driveName"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 281
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    move-wide v2, v4

    move v4, v1

    .line 283
    :goto_5
    if-ge v4, v8, :cond_14

    .line 286
    invoke-virtual {v7, v4}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 287
    const-string v1, "name"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_16

    .line 290
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "total"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 291
    cmp-long v5, v0, v12

    if-lez v5, :cond_16

    cmp-long v5, v2, v12

    if-ltz v5, :cond_e

    cmp-long v5, v2, v0

    if-lez v5, :cond_16

    .line 283
    :cond_e
    :goto_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-wide v2, v0

    goto :goto_5

    .line 227
    :cond_f
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->SPC:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, v3, :cond_10

    .line 228
    const-string v0, "spc"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    move-object v6, v0

    goto/16 :goto_3

    .line 229
    :cond_10
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v0

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->BLURAY:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v0, v3, :cond_11

    .line 230
    const-string v0, "bd"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    move-object v6, v0

    goto/16 :goto_3

    .line 232
    :cond_11
    const-string v0, "phone"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    move-object v6, v0

    goto/16 :goto_3

    :cond_12
    move v0, v1

    .line 273
    goto :goto_4

    :cond_13
    move-wide v2, v4

    .line 297
    :cond_14
    cmp-long v0, v2, v12

    if-lez v0, :cond_15

    .line 298
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/mfluent/asp/datamodel/Device;->a(J)V

    .line 302
    :cond_15
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    goto/16 :goto_0

    :cond_16
    move-wide v0, v2

    goto :goto_6
.end method

.method private g()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x5

    .line 307
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v2

    .line 309
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v3

    .line 313
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 314
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 316
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v3, v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(J)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v5

    .line 318
    const/4 v1, 0x0

    .line 321
    :try_start_0
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v0

    const-string v6, ":"

    invoke-static {v0, v6}, Lorg/apache/commons/lang3/StringUtils;->remove(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 323
    invoke-static {}, Lcom/mfluent/asp/nts/b;->a()Lcom/mfluent/asp/nts/b;

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "/api/pCloud/device/info?_="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&uniqueId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/mfluent/asp/nts/b;->a(Lcom/mfluent/asp/datamodel/Device;Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 332
    :goto_1
    if-nez v0, :cond_3

    .line 333
    sget-object v0, Lcom/mfluent/asp/sync/c;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v10, :cond_1

    .line 334
    const-string v0, "mfl_ASPDeviceMetadataSyncManager"

    const-string v1, "::updateDeviceInfo:Trouble fetching device info - got null respoonse"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v5, v0}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    .line 337
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    goto :goto_0

    .line 326
    :catch_0
    move-exception v0

    .line 327
    sget-object v6, Lcom/mfluent/asp/sync/c;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v6

    if-gt v6, v10, :cond_2

    .line 328
    const-string v6, "mfl_ASPDeviceMetadataSyncManager"

    const-string v7, "::updateDeviceInfo:Trouble fetching device info"

    invoke-static {v6, v7, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 339
    :cond_3
    invoke-static {v0}, Lcom/mfluent/asp/sync/c;->a(Lorg/json/JSONObject;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    .line 341
    const-string v1, "memoryTotal"

    invoke-virtual {v0, v1, v12, v13}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 342
    invoke-virtual {v5, v6, v7}, Lcom/mfluent/asp/datamodel/Device;->setCapacityInBytes(J)V

    .line 344
    const-string v1, "memoryUsed"

    invoke-virtual {v0, v1, v12, v13}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 345
    invoke-virtual {v5, v6, v7}, Lcom/mfluent/asp/datamodel/Device;->setUsedCapacityInBytes(J)V

    .line 347
    const-string v1, "batteryLevel"

    const/4 v6, -0x1

    invoke-virtual {v0, v1, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    .line 348
    invoke-virtual {v5, v0}, Lcom/mfluent/asp/datamodel/Device;->e(I)V

    .line 350
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    move-result v0

    .line 352
    if-eqz v0, :cond_4

    .line 353
    const-string v0, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "::ASPDeviceMetadataSyncManager::updateWearableDeviceInfo "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " : Updated in DB"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 355
    :cond_4
    const-string v0, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "::ASPDeviceMetadataSyncManager::updateWearableDeviceInfo "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " : NOT Updated in DB!!"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 360
    :cond_5
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    invoke-super {p0}, Lcom/mfluent/asp/sync/f;->a()Ljava/util/Collection;

    move-result-object v0

    .line 69
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    const-string v2, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_NOW_ONLINE"

    invoke-virtual {v1, v2}, Lcom/mfluent/asp/datamodel/Device;->buildDeviceIntentFilterForAction(Ljava/lang/String;)Landroid/content/IntentFilter;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 71
    return-object v0
.end method

.method protected final a_(Landroid/content/Intent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v0, 0x0

    .line 76
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 77
    sget-object v1, Lcom/mfluent/asp/sync/c;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_0

    .line 78
    const-string v1, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doSync:Skip sync because device is offline: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 82
    :cond_1
    const-string v1, "com.mfluent.asp.datamodel.Device.BROADCAST_DEVICE_NOW_ONLINE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-wide v2, p0, Lcom/mfluent/asp/sync/c;->j:J

    sget-wide v4, Lcom/mfluent/asp/sync/c;->f:J

    add-long/2addr v2, v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 84
    sget-object v1, Lcom/mfluent/asp/sync/c;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1, v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    const-string v1, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::doSync: Skipping sync for device coming online because last sync was less than "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v4, Lcom/mfluent/asp/sync/c;->f:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms ago."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final b(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 98
    const-string v0, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::doSync:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v0, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::key image:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v0, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::key audio:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v0, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::key video:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    sget-object v3, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->a:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    sget-object v1, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->c:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 107
    const-string v0, "mfl_ASPDeviceMetadataSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::doSync:sendInitialSyncStartBroadcast()"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/mfluent/asp/sync/c;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "BROADCAST_ACTION_DEVICE_INITAIL_SYNC_START"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "BROADCAST_EXTRA_INT_DEVICE_ID"

    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 111
    :cond_0
    invoke-direct {p0}, Lcom/mfluent/asp/sync/c;->g()V

    .line 113
    invoke-direct {p0}, Lcom/mfluent/asp/sync/c;->f()V

    .line 115
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->d()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/mfluent/asp/sync/c;->i:Lcom/mfluent/asp/sync/b;

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/b;->a()Z

    move-result v0

    .line 122
    :goto_0
    if-eqz v0, :cond_2

    .line 123
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/sync/c;->h(Landroid/content/Intent;)V

    .line 129
    :goto_1
    return-void

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/sync/c;->h:Lcom/mfluent/asp/sync/a;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/sync/a;->a(Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 125
    :cond_2
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->j()V

    .line 126
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/c;->e()V

    .line 127
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/c;->j:J

    goto :goto_1
.end method
