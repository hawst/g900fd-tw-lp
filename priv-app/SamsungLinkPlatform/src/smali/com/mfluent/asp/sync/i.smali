.class public Lcom/mfluent/asp/sync/i;
.super Lcom/mfluent/asp/sync/j;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Lorg/slf4j/Logger;


# instance fields
.field private e:J

.field private final f:Landroid/content/BroadcastReceiver;

.field private final g:Landroid/content/BroadcastReceiver;

.field private final h:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    const-class v0, Lcom/mfluent/asp/sync/i;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/i;->b:Lorg/slf4j/Logger;

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/mfluent/asp/sync/j;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".BROADCAST_SEND_WAKE_UP_PUSH"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/i;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/mfluent/asp/sync/j;-><init>(Landroid/content/Context;)V

    .line 54
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/i;->e:J

    .line 56
    new-instance v0, Lcom/mfluent/asp/sync/i$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/i$1;-><init>(Lcom/mfluent/asp/sync/i;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/i;->f:Landroid/content/BroadcastReceiver;

    .line 79
    new-instance v0, Lcom/mfluent/asp/sync/i$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/i$2;-><init>(Lcom/mfluent/asp/sync/i;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/i;->g:Landroid/content/BroadcastReceiver;

    .line 94
    new-instance v0, Lcom/mfluent/asp/sync/i$3;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/i$3;-><init>(Lcom/mfluent/asp/sync/i;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/i;->h:Landroid/content/BroadcastReceiver;

    .line 106
    return-void
.end method

.method private a(Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;ZLcom/msc/seclib/PeerInfo;)V
    .locals 3

    .prologue
    .line 280
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    .line 281
    invoke-virtual {p1, p2}, Lcom/mfluent/asp/datamodel/Device;->a(Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;)V

    .line 283
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-eq v1, v2, :cond_0

    .line 284
    invoke-virtual {p4}, Lcom/msc/seclib/PeerInfo;->getNat_type()S

    move-result v1

    invoke-virtual {p1, v1}, Lcom/mfluent/asp/datamodel/Device;->b(I)V

    .line 285
    invoke-virtual {p4}, Lcom/msc/seclib/PeerInfo;->getLocal_ip()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/mfluent/asp/datamodel/Device;->h(Ljava/lang/String;)V

    .line 286
    invoke-static {p1}, Lcom/mfluent/asp/sync/i;->a(Lcom/mfluent/asp/datamodel/Device;)Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/mfluent/asp/datamodel/Device;->j(Z)V

    .line 289
    :cond_0
    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 291
    if-eqz p3, :cond_1

    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    invoke-static {v1}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v1

    invoke-virtual {v1}, Lpcloud/net/nat/c;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 293
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/mfluent/asp/datamodel/Device;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v0

    .line 295
    invoke-virtual {v0, p1}, Lcom/mfluent/asp/b/i;->d(Lcom/mfluent/asp/datamodel/Device;)V

    .line 299
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/mfluent/asp/sync/i;)V
    .locals 4

    .prologue
    .line 45
    sget-object v0, Lcom/mfluent/asp/sync/i;->b:Lorg/slf4j/Logger;

    const-string v1, "Enter ::sendPushFromUI()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/mfluent/asp/sync/i;->e:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/b/i;->e()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/i;->e:J

    :cond_0
    return-void
.end method

.method private static a(Lcom/mfluent/asp/datamodel/Device;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 197
    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    :goto_0
    return v0

    .line 203
    :cond_0
    :try_start_0
    new-instance v1, Ljava/net/Socket;

    invoke-direct {v1}, Ljava/net/Socket;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    .line 209
    :try_start_1
    new-instance v2, Ljava/net/InetSocketAddress;

    invoke-virtual {p0}, Lcom/mfluent/asp/datamodel/Device;->l()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x4150

    invoke-direct {v2, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    const/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    :try_start_2
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 219
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    .line 211
    :try_start_3
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    .line 213
    :catchall_0
    move-exception v0

    .line 214
    :try_start_4
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 216
    :goto_2
    throw v0

    .line 217
    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_2

    .line 205
    :catch_4
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected final a()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    invoke-super {p0}, Lcom/mfluent/asp/sync/j;->a()Ljava/util/Collection;

    move-result-object v0

    .line 111
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.DataModel.DEVICE_LIST_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 112
    sget-object v1, Lcom/mfluent/asp/sync/i;->b:Lorg/slf4j/Logger;

    const-string v2, "::getSyncIntentFilters: adding LifecycleManagingNetworkTraversal filters"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 113
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/mfluent/asp/nts/a;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 114
    sget-object v1, Lcom/mfluent/asp/sync/i;->b:Lorg/slf4j/Logger;

    const-string v2, "::getSyncIntentFilters: added LifecycleManagingNetworkTraversal filters"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 115
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.DataModel.REFRESH_ALL"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 116
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/mfluent/asp/ASPApplication;->e:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 117
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/mfluent/asp/nts/a;->e:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 119
    return-object v0
.end method

.method protected final a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 224
    sget-object v0, Lcom/mfluent/asp/sync/i;->b:Lorg/slf4j/Logger;

    const-string v1, "Refreshing presence"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    invoke-static {v0}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    invoke-virtual {v0}, Lpcloud/net/nat/c;->i()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    invoke-static {v0}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    invoke-virtual {v0}, Lpcloud/net/nat/c;->j()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 228
    :try_start_0
    const-string v0, "SEND_PUSH_OPTION"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 232
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 237
    iget-object v0, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    invoke-static {v0}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    invoke-virtual {v0, v1}, Lpcloud/net/nat/c;->a(Ljava/util/Vector;)V

    .line 240
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 241
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    .line 242
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 244
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 245
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 263
    :catch_0
    move-exception v0

    .line 264
    sget-object v1, Lcom/mfluent/asp/sync/i;->b:Lorg/slf4j/Logger;

    const-string v2, "Trouble refreshing presence"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 265
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->f()V

    .line 268
    :cond_0
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_CHANGED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 270
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/b/i;->e()V

    .line 277
    :cond_1
    :goto_1
    return-void

    .line 248
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/msc/seclib/PeerInfo;

    .line 249
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v1

    invoke-virtual {v0}, Lcom/msc/seclib/PeerInfo;->getPeer_id()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/mfluent/asp/datamodel/t;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;

    move-result-object v5

    if-eqz v5, :cond_4

    sget-object v1, Lcom/mfluent/asp/sync/i;->b:Lorg/slf4j/Logger;

    const-string v6, "::updateDevicePresence: Updating presence for device: {} with peerInfo: {}"

    invoke-interface {v1, v6, v5, v0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/msc/seclib/PeerInfo;->getPeer_status()S

    move-result v1

    const/4 v6, 0x1

    if-eq v1, v6, :cond_3

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    :goto_3
    invoke-direct {p0, v5, v1, v2, v0}, Lcom/mfluent/asp/sync/i;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;ZLcom/msc/seclib/PeerInfo;)V

    .line 250
    :goto_4
    invoke-virtual {v0}, Lcom/msc/seclib/PeerInfo;->getPeer_id()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 249
    :cond_3
    invoke-virtual {v0}, Lcom/msc/seclib/PeerInfo;->getNetwork_type()S

    move-result v1

    packed-switch v1, :pswitch_data_0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    goto :goto_3

    :pswitch_0
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    goto :goto_3

    :pswitch_1
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    goto :goto_3

    :pswitch_2
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_3G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    goto :goto_3

    :pswitch_3
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_LTE:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    goto :goto_3

    :pswitch_4
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->MOBILE_2G:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    goto :goto_3

    :cond_4
    sget-object v1, Lcom/mfluent/asp/sync/i;->b:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::updateDevicePresence: unknown id-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/msc/seclib/PeerInfo;->getPeer_id()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    sget-object v1, Lcom/mfluent/asp/sync/i;->b:Lorg/slf4j/Logger;

    const-string v5, "::updateDevicePresence: Could not update peer because there is no device with that device id."

    invoke-interface {v1, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_4

    .line 253
    :cond_5
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 254
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 255
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 257
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEARABLE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 258
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    const/4 v4, 0x0

    invoke-direct {p0, v0, v3, v2, v4}, Lcom/mfluent/asp/sync/i;->a(Lcom/mfluent/asp/datamodel/Device;Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;ZLcom/msc/seclib/PeerInfo;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5

    .line 275
    :cond_7
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->f()V

    goto/16 :goto_1

    .line 249
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(Lorg/apache/commons/lang3/builder/EqualsBuilder;Landroid/content/Intent;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 319
    const-string v0, "SEND_PUSH_OPTION"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "SEND_PUSH_OPTION"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/commons/lang3/builder/EqualsBuilder;->append(ZZ)Lorg/apache/commons/lang3/builder/EqualsBuilder;

    .line 320
    const-string v0, "STATUS_CHANGE_PEERID"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "STATUS_CHANGE_PEERID"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/commons/lang3/builder/EqualsBuilder;->append(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/EqualsBuilder;

    .line 321
    return-void
.end method

.method protected final a(Lorg/apache/commons/lang3/builder/HashCodeBuilder;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 325
    const-string v0, "SEND_PUSH_OPTION"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(Z)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    .line 326
    const-string v0, "STATUS_CHANGE_PEERID"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/commons/lang3/builder/HashCodeBuilder;->append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/HashCodeBuilder;

    .line 327
    return-void
.end method

.method protected final c(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 124
    invoke-super {p0, p1}, Lcom/mfluent/asp/sync/j;->c(Landroid/content/Intent;)V

    .line 126
    iget-object v0, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/i;->f:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/mfluent/asp/nts/a;->d:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 130
    iget-object v0, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/i;->g:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/mfluent/asp/sync/i;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 134
    iget-object v0, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/mfluent/asp/sync/i;->h:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v3, "com.samsung.android.sdk.samsunglink.permission.BROADCAST_SAMSUNG_LINK"

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 139
    return-void
.end method

.method protected final d(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0, p1}, Lcom/mfluent/asp/sync/j;->d(Landroid/content/Intent;)V

    .line 145
    iget-object v0, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/i;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 146
    iget-object v0, p0, Lcom/mfluent/asp/sync/i;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/i;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 147
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    iget-object v1, p0, Lcom/mfluent/asp/sync/i;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ASPApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 148
    return-void
.end method

.method protected final f_()J
    .locals 2

    .prologue
    .line 314
    const-wide/16 v0, 0x1f4

    return-wide v0
.end method
