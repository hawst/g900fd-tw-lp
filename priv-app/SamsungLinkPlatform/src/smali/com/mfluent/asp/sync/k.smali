.class public Lcom/mfluent/asp/sync/k;
.super Lcom/mfluent/asp/sync/j;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mfluent/asp/sync/k$3;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static j:Lcom/mfluent/asp/sync/k;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/mfluent/asp/datamodel/Device;",
            "Lcom/mfluent/asp/sync/f;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/mfluent/asp/sync/DeviceListSyncManager;

.field private final f:Lcom/mfluent/asp/sync/e;

.field private final g:Lcom/mfluent/asp/sync/i;

.field private h:Z

.field private final i:Ljava/util/concurrent/locks/Lock;

.field private final k:Landroid/content/BroadcastReceiver;

.field private final l:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/mfluent/asp/sync/k;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/k;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/mfluent/asp/sync/j;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/k;->b:Ljava/util/Map;

    .line 50
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/k;->i:Ljava/util/concurrent/locks/Lock;

    .line 73
    new-instance v0, Lcom/mfluent/asp/sync/k$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/k$1;-><init>(Lcom/mfluent/asp/sync/k;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/k;->k:Landroid/content/BroadcastReceiver;

    .line 91
    new-instance v0, Lcom/mfluent/asp/sync/k$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/k$2;-><init>(Lcom/mfluent/asp/sync/k;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/k;->l:Landroid/content/BroadcastReceiver;

    .line 67
    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->a(Landroid/content/Context;)Lcom/mfluent/asp/sync/DeviceListSyncManager;

    move-result-object v0

    iput-object v0, p0, Lcom/mfluent/asp/sync/k;->e:Lcom/mfluent/asp/sync/DeviceListSyncManager;

    .line 68
    new-instance v0, Lcom/mfluent/asp/sync/e;

    iget-object v1, p0, Lcom/mfluent/asp/sync/k;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/sync/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/k;->f:Lcom/mfluent/asp/sync/e;

    .line 69
    new-instance v0, Lcom/mfluent/asp/sync/i;

    iget-object v1, p0, Lcom/mfluent/asp/sync/k;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/mfluent/asp/sync/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/k;->g:Lcom/mfluent/asp/sync/i;

    .line 70
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/k;->k:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/mfluent/asp/ASPApplication;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 71
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/mfluent/asp/sync/k;
    .locals 3

    .prologue
    .line 55
    const-class v1, Lcom/mfluent/asp/sync/k;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 56
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Context must not be null."

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 58
    :cond_0
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/sync/k;->j:Lcom/mfluent/asp/sync/k;

    if-nez v0, :cond_1

    .line 59
    new-instance v0, Lcom/mfluent/asp/sync/k;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/k;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/sync/k;->j:Lcom/mfluent/asp/sync/k;

    .line 62
    :cond_1
    sget-object v0, Lcom/mfluent/asp/sync/k;->j:Lcom/mfluent/asp/sync/k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method static synthetic a(Lcom/mfluent/asp/sync/k;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/mfluent/asp/sync/k;)Lcom/mfluent/asp/sync/e;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->f:Lcom/mfluent/asp/sync/e;

    return-object v0
.end method

.method static synthetic c(Lcom/mfluent/asp/sync/k;)Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->l:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic d()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/mfluent/asp/sync/k;->a:Lorg/slf4j/Logger;

    return-object v0
.end method


# virtual methods
.method protected final a()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    invoke-super {p0}, Lcom/mfluent/asp/sync/j;->a()Ljava/util/Collection;

    move-result-object v0

    .line 104
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.DataModel.DEVICE_LIST_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 105
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/mfluent/asp/ASPApplication;->e:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 107
    return-object v0
.end method

.method protected final a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 177
    const-string v0, "INFO"

    const-string v1, "Trace synclock: doSync start"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 180
    :try_start_0
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->j()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 243
    const-string v0, "INFO"

    const-string v1, "Trace synclock: doSync end"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :goto_0
    return-void

    .line 192
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/k;->h:Z

    if-nez v0, :cond_3

    sget-object v0, Lcom/mfluent/asp/sync/k;->a:Lorg/slf4j/Logger;

    const-string v1, "Starting up sync managers"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->f:Lcom/mfluent/asp/sync/e;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/sync/e;->f(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->g:Lcom/mfluent/asp/sync/i;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/sync/i;->f(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->e:Lcom/mfluent/asp/sync/DeviceListSyncManager;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->f(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/f;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/sync/f;->f(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 238
    :catch_0
    move-exception v0

    .line 239
    :try_start_2
    sget-object v1, Lcom/mfluent/asp/sync/k;->a:Lorg/slf4j/Logger;

    const-string v2, "::doSync: Trouble syncing"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 243
    const-string v0, "INFO"

    const-string v1, "Trace synclock: doSync end"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 192
    :cond_2
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lcom/mfluent/asp/sync/k;->h:Z

    .line 194
    :cond_3
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 196
    new-instance v2, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/mfluent/asp/sync/k;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 198
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 200
    invoke-static {}, Lcom/mfluent/asp/datamodel/Device;->j()Z

    .line 202
    iget-object v1, p0, Lcom/mfluent/asp/sync/k;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mfluent/asp/sync/f;

    .line 203
    if-nez v1, :cond_4

    .line 204
    sget-object v4, Lcom/mfluent/asp/sync/k$3;->a:[I

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_2

    .line 206
    :pswitch_0
    sget-object v4, Lcom/mfluent/asp/sync/k;->a:Lorg/slf4j/Logger;

    const-string v5, "Sending Intent ACTION_LOCAL_MEDIA_REVERSE_GEO_SERVICE_STARTER"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 207
    iget-object v4, p0, Lcom/mfluent/asp/sync/k;->c:Landroid/content/Context;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.LOCAL_MEDIA_REVERSE_GEO_SERVICE_STARTER"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 221
    :goto_3
    if-eqz v1, :cond_4

    .line 222
    invoke-virtual {v1, p1}, Lcom/mfluent/asp/sync/f;->f(Landroid/content/Intent;)V

    .line 223
    iget-object v4, p0, Lcom/mfluent/asp/sync/k;->b:Ljava/util/Map;

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :cond_4
    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 242
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/k;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 243
    const-string v1, "INFO"

    const-string v2, "Trace synclock: doSync end"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 211
    :pswitch_1
    :try_start_4
    new-instance v1, Lcom/mfluent/asp/sync/c;

    iget-object v4, p0, Lcom/mfluent/asp/sync/k;->c:Landroid/content/Context;

    invoke-direct {v1, v4, v0}, Lcom/mfluent/asp/sync/c;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_3

    .line 214
    :pswitch_2
    new-instance v1, Lcom/mfluent/asp/sync/CloudStorageSyncManager;

    iget-object v4, p0, Lcom/mfluent/asp/sync/k;->c:Landroid/content/Context;

    invoke-direct {v1, v4, v0}, Lcom/mfluent/asp/sync/CloudStorageSyncManager;-><init>(Landroid/content/Context;Lcom/mfluent/asp/datamodel/Device;)V

    goto :goto_3

    .line 231
    :cond_5
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 232
    sget-object v2, Lcom/mfluent/asp/sync/k;->a:Lorg/slf4j/Logger;

    const-string v3, "::doSync:Removing {} from sync manager"

    invoke-interface {v2, v3, v0}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 233
    iget-object v2, p0, Lcom/mfluent/asp/sync/k;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/f;

    .line 234
    if-eqz v0, :cond_6

    .line 235
    invoke-virtual {v0, p1}, Lcom/mfluent/asp/sync/f;->g(Landroid/content/Intent;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final b()Z
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x1

    return v0
.end method

.method public final c(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 116
    invoke-super {p0, p1}, Lcom/mfluent/asp/sync/j;->c(Landroid/content/Intent;)V

    .line 118
    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->f:Lcom/mfluent/asp/sync/e;

    const-class v1, Lcom/mfluent/asp/sync/e;

    invoke-static {v0, v1}, Lcom/mfluent/asp/b;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 119
    return-void
.end method

.method protected final d(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/k;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 145
    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 147
    :try_start_0
    iget-boolean v0, p0, Lcom/mfluent/asp/sync/k;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->f:Lcom/mfluent/asp/sync/e;

    invoke-virtual {v0}, Lcom/mfluent/asp/sync/e;->c()V

    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->g:Lcom/mfluent/asp/sync/i;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/sync/i;->g(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->e:Lcom/mfluent/asp/sync/DeviceListSyncManager;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->g(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->f:Lcom/mfluent/asp/sync/e;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/sync/e;->g(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/a/a;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsung/a/a;->a()V

    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/sync/f;

    invoke-virtual {v0, p1}, Lcom/mfluent/asp/sync/f;->g(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/k;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 147
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/mfluent/asp/sync/k;->h:Z

    .line 149
    :cond_1
    invoke-super {p0, p1}, Lcom/mfluent/asp/sync/j;->d(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    iget-object v0, p0, Lcom/mfluent/asp/sync/k;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 152
    return-void
.end method
