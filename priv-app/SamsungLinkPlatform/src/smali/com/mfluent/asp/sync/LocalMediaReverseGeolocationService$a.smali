.class public final Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;

.field private final b:D

.field private final c:D

.field private final d:Ljava/lang/String;

.field private final e:Landroid/location/Address;


# direct methods
.method public constructor <init>(Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;DDLjava/lang/String;Landroid/location/Address;)V
    .locals 2

    .prologue
    .line 1045
    iput-object p1, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->a:Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046
    invoke-static {}, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService;->b()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::GeolocationData()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 1047
    iput-wide p2, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->b:D

    .line 1048
    iput-wide p4, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->c:D

    .line 1049
    iput-object p6, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->d:Ljava/lang/String;

    .line 1050
    iput-object p7, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->e:Landroid/location/Address;

    .line 1051
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    .line 1054
    iget-wide v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->b:D

    return-wide v0
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 1058
    iget-wide v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->c:D

    return-wide v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1062
    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Landroid/location/Address;
    .locals 1

    .prologue
    .line 1066
    iget-object v0, p0, Lcom/mfluent/asp/sync/LocalMediaReverseGeolocationService$a;->e:Landroid/location/Address;

    return-object v0
.end method
