.class final Lcom/mfluent/asp/sync/b$a;
.super Lcom/mfluent/asp/sync/b$d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mfluent/asp/sync/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 655
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mfluent/asp/sync/b$d;-><init>(B)V

    .line 657
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mfluent/asp/sync/b$a;->c:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 655
    invoke-direct {p0}, Lcom/mfluent/asp/sync/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 724
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 661
    invoke-super {p0}, Lcom/mfluent/asp/sync/b$d;->a()V

    .line 663
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$a;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 664
    return-void
.end method

.method public final a(Landroid/content/ContentResolver;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 696
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$a;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 698
    :try_start_0
    const-string v0, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    iget-object v1, p0, Lcom/mfluent/asp/sync/b$a;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 700
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 701
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$a;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 705
    :cond_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$a;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 706
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 707
    iget-object v3, p0, Lcom/mfluent/asp/sync/b$a;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v3, v1, p2, v0}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres$Members;->setAudioGenresBySourceMediaId(Landroid/content/ContentResolver;Ljava/util/ArrayList;[Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 700
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/mfluent/asp/sync/b$a;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 701
    iget-object v1, p0, Lcom/mfluent/asp/sync/b$a;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    throw v0

    .line 709
    :cond_1
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$a;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 710
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$a;->a:Ljava/util/ArrayList;

    invoke-static {p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Artists;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 711
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$a;->a:Ljava/util/ArrayList;

    invoke-static {p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Albums;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 712
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$a;->a:Ljava/util/ArrayList;

    invoke-static {p2}, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Genres;->getOrphanCleanUriForDevice(I)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 714
    invoke-super {p0, p1, p2}, Lcom/mfluent/asp/sync/b$d;->a(Landroid/content/ContentResolver;I)V

    .line 715
    return-void
.end method

.method public final a(Landroid/content/ContentResolver;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 687
    invoke-super {p0, p1, p2, p3}, Lcom/mfluent/asp/sync/b$d;->a(Landroid/content/ContentResolver;ILjava/lang/String;)V

    .line 691
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$a;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 692
    return-void
.end method

.method public final a(Landroid/content/ContentResolver;ILjava/lang/String;Lorg/json/JSONObject;Landroid/content/ContentValues;)V
    .locals 3

    .prologue
    .line 673
    invoke-super/range {p0 .. p5}, Lcom/mfluent/asp/sync/b$d;->a(Landroid/content/ContentResolver;ILjava/lang/String;Lorg/json/JSONObject;Landroid/content/ContentValues;)V

    .line 675
    const-string v0, "genre"

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 676
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 677
    const-string v0, "<unknown>"

    .line 679
    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 681
    iget-object v0, p0, Lcom/mfluent/asp/sync/b$a;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 683
    return-void
.end method

.method public final b()Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;
    .locals 1

    .prologue
    .line 668
    sget-object v0, Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;->b:Lcom/mfluent/asp/datamodel/Device$SyncedMediaType;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 719
    sget-object v0, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 729
    const-string v0, "Music"

    return-object v0
.end method

.method public final e()[Lcom/mfluent/asp/dws/a$g;
    .locals 1

    .prologue
    .line 734
    sget-object v0, Lcom/mfluent/asp/dws/a;->b:[Lcom/mfluent/asp/dws/a$g;

    return-object v0
.end method
