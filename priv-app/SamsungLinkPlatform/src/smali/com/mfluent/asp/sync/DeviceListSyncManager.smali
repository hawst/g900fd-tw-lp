.class public Lcom/mfluent/asp/sync/DeviceListSyncManager;
.super Lcom/mfluent/asp/sync/j;
.source "SourceFile"


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final g:J

.field private static i:Lcom/mfluent/asp/sync/DeviceListSyncManager;


# instance fields
.field private final e:Landroid/content/BroadcastReceiver;

.field private final f:Landroid/content/BroadcastReceiver;

.field private h:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 67
    const-class v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->a:Lorg/slf4j/Logger;

    .line 70
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 107
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->g:J

    .line 111
    const/4 v0, 0x0

    sput-object v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->i:Lcom/mfluent/asp/sync/DeviceListSyncManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 139
    invoke-direct {p0, p1}, Lcom/mfluent/asp/sync/j;-><init>(Landroid/content/Context;)V

    .line 76
    new-instance v0, Lcom/mfluent/asp/sync/DeviceListSyncManager$1;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/DeviceListSyncManager$1;-><init>(Lcom/mfluent/asp/sync/DeviceListSyncManager;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->e:Landroid/content/BroadcastReceiver;

    .line 98
    new-instance v0, Lcom/mfluent/asp/sync/DeviceListSyncManager$2;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/DeviceListSyncManager$2;-><init>(Lcom/mfluent/asp/sync/DeviceListSyncManager;)V

    iput-object v0, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->f:Landroid/content/BroadcastReceiver;

    .line 109
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->h:J

    .line 141
    return-void
.end method

.method private a(Lcom/mfluent/asp/datamodel/t;Lorg/json/JSONArray;Landroid/content/Intent;)I
    .locals 24

    .prologue
    .line 527
    new-instance v12, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm:ss.S"

    sget-object v3, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-direct {v12, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 529
    const/4 v9, 0x0

    .line 530
    const/4 v8, 0x0

    .line 531
    const/4 v6, 0x0

    .line 532
    const/4 v5, 0x0

    .line 533
    const/4 v4, 0x0

    .line 535
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 536
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 538
    const-class v2, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v2}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mfluent/asp/ASPApplication;

    .line 540
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mfluent/asp/datamodel/Device;

    .line 541
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v10

    .line 543
    sget-object v11, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v10, v11, :cond_0

    .line 545
    sget-object v11, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEARABLE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v10, v11, :cond_1

    .line 546
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->Q()Ljava/lang/String;

    move-result-object v10

    .line 547
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->F()Ljava/lang/String;

    move-result-object v11

    .line 548
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, "-"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v14, v10, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 551
    :cond_1
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->f()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v13, v10, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 556
    :cond_2
    invoke-static {v2}, Lcom/sec/pcw/service/account/b;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    .line 558
    const/4 v3, 0x0

    move v10, v3

    :goto_1
    invoke-virtual/range {p2 .. p2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v10, v3, :cond_24

    .line 559
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v16

    .line 562
    const-string v3, "dvceTpCd"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 563
    const-string v7, "modelType"

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 564
    const-string v11, "softwareVersion"

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 565
    invoke-static {v7}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_37

    .line 568
    :goto_2
    invoke-static {v11}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    const/4 v7, 0x0

    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v11, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const-string v11, "WP"

    invoke-virtual {v7, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 569
    const-string v3, "PHONE DEVICE WINDOWS"

    .line 572
    :cond_3
    invoke-static {v3}, Lcom/mfluent/asp/datamodel/t;->d(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v11

    .line 574
    const-string v3, "enableFlag"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 575
    const-string v7, "Y"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    .line 577
    const-string v3, "dvcePhslAddrTxt"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 578
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->f()Ljava/lang/String;

    move-result-object v3

    .line 579
    invoke-static/range {v18 .. v18}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 580
    sget-object v3, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v7, 0x3

    if-gt v3, v7, :cond_36

    .line 581
    const-string v3, "mfl_DeviceListSyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v11, "Ignoring deviceInfo because \'deviceImei\' property not found: "

    invoke-direct {v7, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    move-object v7, v8

    move v4, v5

    move-object v5, v6

    move-object v6, v9

    .line 558
    :goto_3
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    move-object v9, v6

    move-object v6, v5

    move-object v8, v7

    move v5, v4

    move v4, v3

    goto/16 :goto_1

    .line 584
    :cond_4
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    sget-boolean v3, Lcom/mfluent/asp/ASPApplication;->a:Z

    if-eqz v3, :cond_8

    .line 585
    const/4 v7, 0x1

    .line 586
    const/4 v3, 0x1

    .line 587
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/samsung/android/sdk/samsung/c;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/c;

    move-result-object v19

    .line 588
    const-string v20, "serviceNoti"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 590
    const-string v21, "Y"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_19

    .line 591
    const/4 v7, 0x1

    .line 596
    :cond_5
    :goto_4
    if-eqz v19, :cond_6

    .line 597
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/samsung/c;->b()Z

    move-result v3

    .line 600
    :cond_6
    if-ne v7, v3, :cond_7

    invoke-static/range {v20 .. v20}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 601
    :cond_7
    const-string v3, "mfl_DeviceListSyncManager"

    const-string v7, "clientPushEnable and serverPushEnable is different"

    invoke-static {v3, v7}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    const/4 v3, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/samsung/c;->c(Z)V

    .line 603
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v3}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mfluent/asp/b/i;->c()Ljava/lang/String;

    .line 607
    :cond_8
    move-object/from16 v0, v18

    invoke-interface {v13, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mfluent/asp/datamodel/Device;

    .line 609
    if-nez v3, :cond_35

    .line 610
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_35

    .line 612
    invoke-virtual/range {p1 .. p1}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v3

    move-object v7, v3

    .line 616
    :goto_5
    if-nez v7, :cond_9

    .line 617
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->WEARABLE_DEVICE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v11, v3, :cond_9

    .line 619
    const-string v3, "parentDevice"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 621
    const-string v3, "dvceUnqId"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 623
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v20, "-"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v14, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mfluent/asp/datamodel/Device;

    .line 625
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v20, "-"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v14, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v7, v3

    .line 629
    :cond_9
    if-nez v7, :cond_b

    .line 637
    sget-object v3, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v7, 0x2

    if-gt v3, v7, :cond_a

    .line 638
    const-string v3, "mfl_DeviceListSyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v19, "NEW DEVICE : adding new device with : "

    move-object/from16 v0, v19

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    :cond_a
    new-instance v7, Lcom/mfluent/asp/datamodel/Device;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-direct {v7, v3}, Lcom/mfluent/asp/datamodel/Device;-><init>(Landroid/content/Context;)V

    .line 643
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->WEARABLE_DEVICE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v11, v3, :cond_1a

    .line 644
    const-string v3, "parentDevice"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Lcom/mfluent/asp/datamodel/Device;->m(Ljava/lang/String;)V

    .line 650
    :cond_b
    :goto_6
    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v18

    .line 652
    invoke-virtual {v7, v11}, Lcom/mfluent/asp/datamodel/Device;->b(Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;)V

    .line 653
    const-string v3, "deviceID"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Lcom/mfluent/asp/datamodel/Device;->e(Ljava/lang/String;)V

    .line 654
    const-string v3, "dvceUnqId"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Lcom/mfluent/asp/datamodel/Device;->l(Ljava/lang/String;)V

    .line 655
    const-string v3, "dvceName"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    .line 657
    const-string v3, "modelName"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/commons/lang3/StringEscapeUtils;->unescapeHtml4(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 658
    invoke-static {v3}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 659
    const-string v3, "dvceModelId"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 663
    :cond_c
    const-string v19, "BTAddress"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/mfluent/asp/datamodel/Device;->n(Ljava/lang/String;)V

    .line 664
    const-string v19, "BLEAddress"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/mfluent/asp/datamodel/Device;->o(Ljava/lang/String;)V

    .line 665
    const-string v19, "WiFiAddress"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/mfluent/asp/datamodel/Device;->p(Ljava/lang/String;)V

    .line 666
    const-string v19, "WiFiDirectAddress"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/mfluent/asp/datamodel/Device;->q(Ljava/lang/String;)V

    .line 670
    const-string v19, "remoteWakeupSupport"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 672
    const-string v20, "Y"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lorg/apache/commons/lang3/StringUtils;->equalsIgnoreCase(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_1b

    .line 673
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/mfluent/asp/datamodel/Device;->p(Z)V

    .line 687
    :goto_7
    invoke-virtual {v7, v3}, Lcom/mfluent/asp/datamodel/Device;->f(Ljava/lang/String;)V

    .line 689
    const-string v3, "peerId"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 690
    invoke-static/range {v19 .. v19}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v3

    if-nez v3, :cond_d

    .line 691
    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/mfluent/asp/datamodel/Device;->c(Ljava/lang/String;)V

    .line 694
    :cond_d
    if-eqz v17, :cond_1c

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 697
    :goto_8
    sget-object v20, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->WEARABLE_DEVICE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-object/from16 v0, v20

    if-ne v11, v0, :cond_e

    .line 698
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEARABLE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 699
    const-string v11, "parentDevice"

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/mfluent/asp/datamodel/Device;->m(Ljava/lang/String;)V

    .line 703
    :cond_e
    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->h()Z

    move-result v11

    if-eqz v11, :cond_34

    .line 705
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    .line 710
    const-string v9, "dvceSeq"

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 713
    invoke-static {v9}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 714
    const-string v11, "slpf_device_20"

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v2, v11, v0}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 715
    const-string v20, "setds"

    const-string v21, ""

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 717
    move-object/from16 v0, v20

    invoke-static {v0, v9}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_f

    .line 718
    invoke-virtual {v2, v9}, Lcom/mfluent/asp/ASPApplication;->a(Ljava/lang/String;)V

    .line 721
    invoke-interface {v11}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 722
    const-string v20, "setds"

    move-object/from16 v0, v20

    invoke-interface {v11, v0, v9}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 723
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 728
    :cond_f
    if-eqz v17, :cond_12

    .line 734
    invoke-static {v15}, Lcom/sec/pcw/util/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 736
    if-eqz v4, :cond_11

    .line 738
    const-string v9, ":"

    invoke-static {v15, v9}, Lorg/apache/commons/lang3/StringUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v9

    .line 739
    add-int/lit8 v9, v9, 0x16

    .line 741
    const/4 v11, 0x0

    invoke-static {v4, v11, v9}, Lorg/apache/commons/lang3/StringUtils;->substring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v11

    .line 745
    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v1, v9}, Lorg/apache/commons/lang3/StringUtils;->substring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v9

    .line 747
    invoke-static {v11, v9}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_11

    .line 749
    sget-object v9, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v9}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v9

    const/4 v11, 0x2

    if-gt v9, v11, :cond_10

    .line 750
    const-string v9, "mfl_DeviceListSyncManager"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v19, "::processDeviceList: sending setdeviceenable with encrypted peerID: "

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v9, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    :cond_10
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 757
    :cond_11
    const/4 v4, 0x1

    .line 760
    :cond_12
    invoke-virtual {v2}, Lcom/mfluent/asp/ASPApplication;->k()Landroid/content/SharedPreferences;

    move-result-object v9

    .line 761
    const-string v11, "mreset"

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v9, v11, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 763
    sget-object v19, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->UNKNOWN:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_13

    if-eqz v11, :cond_1e

    .line 765
    :cond_13
    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    .line 766
    const-string v11, "mreset"

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v9, v11, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 767
    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 773
    const/4 v11, 0x0

    .line 774
    :try_start_0
    invoke-virtual {v2}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v18, "device_name"

    move-object/from16 v0, v18

    invoke-static {v9, v0}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 775
    invoke-static {v9}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_1d

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_1d

    .line 776
    const/4 v11, 0x1

    .line 784
    :cond_14
    :goto_9
    if-eqz v11, :cond_15

    .line 785
    invoke-virtual {v7, v9}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 786
    const/4 v5, 0x1

    :cond_15
    move-object v9, v3

    move v3, v4

    move v4, v5

    move-object v5, v7

    .line 794
    :goto_a
    invoke-virtual {v7, v9}, Lcom/mfluent/asp/datamodel/Device;->b(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)V

    .line 798
    const-string v11, "registrationDate"

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 799
    invoke-static {v11}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_16

    .line 801
    :try_start_1
    invoke-virtual {v12, v11}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v16

    .line 805
    invoke-virtual/range {v16 .. v16}, Ljava/util/Date;->getTime()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 806
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v7, v0, v1}, Lcom/mfluent/asp/datamodel/Device;->b(J)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    .line 815
    :cond_16
    :goto_b
    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->getId()I

    move-result v11

    if-gtz v11, :cond_17

    .line 816
    if-eqz v17, :cond_17

    .line 817
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v11}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v11

    invoke-virtual {v11}, Lpcloud/net/nat/c;->h()V

    .line 822
    :cond_17
    sget-object v11, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNREGISTERED:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-ne v9, v11, :cond_18

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v9

    sget-object v11, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v9, v11, :cond_20

    .line 824
    :cond_18
    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->f()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v13, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 826
    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    move-result v7

    if-eqz v7, :cond_1f

    .line 827
    const-string v7, "INFO"

    const-string v9, "getuserdevice commitChanges OK"

    invoke-static {v7, v9}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v8

    move-object/from16 v23, v6

    move-object v6, v5

    move-object/from16 v5, v23

    goto/16 :goto_3

    .line 592
    :cond_19
    const-string v21, "N"

    const-string v22, "serviceNoti"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 593
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 646
    :cond_1a
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/mfluent/asp/datamodel/Device;->d(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 683
    :cond_1b
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/mfluent/asp/datamodel/Device;->p(Z)V

    .line 684
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/mfluent/asp/datamodel/Device;->o(Z)V

    goto/16 :goto_7

    .line 694
    :cond_1c
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNREGISTERED:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    goto/16 :goto_8

    .line 779
    :cond_1d
    :try_start_2
    invoke-virtual {v2}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v18, "device_name"

    move-object/from16 v0, v18

    invoke-static {v9, v0}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 780
    invoke-static {v9}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_14

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->p()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v18

    if-nez v18, :cond_14

    .line 781
    const/4 v11, 0x1

    goto/16 :goto_9

    .line 788
    :catch_0
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1e
    move-object v9, v3

    move v3, v4

    move v4, v5

    move-object v5, v7

    goto/16 :goto_a

    .line 808
    :catch_1
    move-exception v16

    sget-object v16, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual/range {v16 .. v16}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v16

    const/16 v18, 0x2

    move/from16 v0, v16

    move/from16 v1, v18

    if-gt v0, v1, :cond_16

    .line 809
    const-string v16, "mfl_DeviceListSyncManager"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "::processDeviceList: error while parsing registrationDate: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v16

    invoke-static {v0, v11}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 829
    :cond_1f
    const-string v7, "INFO"

    const-string v9, "getuserdevice commitChanges No"

    invoke-static {v7, v9}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v8

    move-object/from16 v23, v6

    move-object v6, v5

    move-object/from16 v5, v23

    goto/16 :goto_3

    .line 832
    :cond_20
    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v9

    sget-object v11, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_PHONE:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v9, v11, :cond_22

    .line 833
    if-eqz v6, :cond_21

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->N()J

    move-result-wide v16

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->N()J

    move-result-wide v18

    cmp-long v9, v16, v18

    if-gez v9, :cond_33

    :cond_21
    move-object v6, v5

    move-object v5, v7

    move-object v7, v8

    .line 834
    goto/16 :goto_3

    .line 836
    :cond_22
    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->n()Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    move-result-object v9

    sget-object v11, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->DEVICE_TAB:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    if-ne v9, v11, :cond_33

    .line 837
    if-eqz v8, :cond_23

    invoke-virtual {v8}, Lcom/mfluent/asp/datamodel/Device;->N()J

    move-result-wide v16

    invoke-virtual {v7}, Lcom/mfluent/asp/datamodel/Device;->N()J

    move-result-wide v18

    cmp-long v9, v16, v18

    if-gez v9, :cond_33

    :cond_23
    move-object/from16 v23, v6

    move-object v6, v5

    move-object/from16 v5, v23

    .line 838
    goto/16 :goto_3

    .line 844
    :cond_24
    if-eqz v6, :cond_26

    .line 845
    sget-object v3, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v7, 0x3

    if-gt v3, v7, :cond_25

    .line 846
    const-string v3, "mfl_DeviceListSyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v10, "::processDeviceList Latest UNREGISTERED phone: "

    invoke-direct {v7, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    :cond_25
    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    .line 849
    invoke-virtual {v6}, Lcom/mfluent/asp/datamodel/Device;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v13, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 851
    :cond_26
    if-eqz v8, :cond_28

    .line 852
    sget-object v3, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v6, 0x3

    if-gt v3, v6, :cond_27

    .line 853
    const-string v3, "mfl_DeviceListSyncManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "::processDeviceList Latest UNREGISTERED tablet: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/mfluent/asp/datamodel/Device;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    :cond_27
    invoke-virtual {v8}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    .line 856
    invoke-virtual {v8}, Lcom/mfluent/asp/datamodel/Device;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v13, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 860
    :cond_28
    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_c
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mfluent/asp/datamodel/Device;

    .line 861
    sget-object v7, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v7}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v7

    const/4 v8, 0x3

    if-gt v7, v8, :cond_29

    .line 862
    const-string v7, "mfl_DeviceListSyncManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "::processDeviceList: REMOVE (not in list): "

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    :cond_29
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->P()V

    goto :goto_c

    .line 867
    :cond_2a
    invoke-interface {v14}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mfluent/asp/datamodel/Device;

    .line 868
    sget-object v7, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v7}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v7

    const/4 v8, 0x3

    if-gt v7, v8, :cond_2b

    .line 869
    const-string v7, "mfl_DeviceListSyncManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "::processDeviceList: REMOVE (not in list, wearable devices): "

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    :cond_2b
    invoke-virtual {v3}, Lcom/mfluent/asp/datamodel/Device;->P()V

    goto :goto_d

    .line 874
    :cond_2c
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1, v5}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->a(Lcom/mfluent/asp/ASPApplication;Lcom/mfluent/asp/datamodel/t;Z)V

    .line 878
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v2

    .line 879
    invoke-static/range {p3 .. p3}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->k(Landroid/content/Intent;)Z

    move-result v3

    .line 880
    if-nez v3, :cond_2f

    .line 884
    if-nez v4, :cond_2d

    .line 886
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/a;->b(Z)V

    .line 890
    :cond_2d
    const/4 v2, 0x1

    .line 927
    :goto_e
    invoke-static/range {p3 .. p3}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 928
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->h:J

    .line 931
    :cond_2e
    return v2

    .line 893
    :cond_2f
    if-eqz v4, :cond_30

    .line 896
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/a;->b(Z)V

    .line 897
    const/4 v2, 0x0

    goto :goto_e

    .line 903
    :cond_30
    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Ljava/util/List;

    move-result-object v3

    new-instance v4, Lcom/mfluent/asp/sync/DeviceListSyncManager$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/mfluent/asp/sync/DeviceListSyncManager$3;-><init>(Lcom/mfluent/asp/sync/DeviceListSyncManager;)V

    invoke-static {v3, v4}, Lorg/apache/commons/collections/CollectionUtils;->countMatches(Ljava/util/Collection;Lorg/apache/commons/collections/Predicate;)I

    move-result v3

    .line 913
    const/4 v4, 0x6

    if-ge v3, v4, :cond_31

    .line 914
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->a(Lcom/mfluent/asp/datamodel/Device;)V

    .line 915
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/mfluent/asp/a;->b(Z)V

    .line 916
    const/4 v2, 0x0

    goto :goto_e

    .line 919
    :cond_31
    sget-object v2, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x4

    if-gt v2, v3, :cond_32

    .line 920
    const-string v2, "mfl_DeviceListSyncManager"

    const-string v3, "syncDeviceInfoList:Device list is full. Must remove a device before this device can be added."

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    :cond_32
    const/4 v2, 0x2

    goto :goto_e

    :cond_33
    move-object v7, v8

    move-object/from16 v23, v6

    move-object v6, v5

    move-object/from16 v5, v23

    goto/16 :goto_3

    :cond_34
    move-object/from16 v23, v3

    move v3, v4

    move v4, v5

    move-object v5, v9

    move-object/from16 v9, v23

    goto/16 :goto_a

    :cond_35
    move-object v7, v3

    goto/16 :goto_5

    :cond_36
    move v3, v4

    move-object v7, v8

    move v4, v5

    move-object v5, v6

    move-object v6, v9

    goto/16 :goto_3

    :cond_37
    move-object v3, v7

    goto/16 :goto_2
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/mfluent/asp/sync/DeviceListSyncManager;
    .locals 2

    .prologue
    .line 114
    const-class v1, Lcom/mfluent/asp/sync/DeviceListSyncManager;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 115
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 117
    :cond_0
    :try_start_1
    sget-object v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->i:Lcom/mfluent/asp/sync/DeviceListSyncManager;

    if-nez v0, :cond_1

    .line 118
    new-instance v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/sync/DeviceListSyncManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->i:Lcom/mfluent/asp/sync/DeviceListSyncManager;

    .line 120
    :cond_1
    sget-object v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->i:Lcom/mfluent/asp/sync/DeviceListSyncManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method private a(Lcom/mfluent/asp/ASPApplication;Lcom/mfluent/asp/datamodel/t;Z)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 935
    .line 937
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v5

    .line 938
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move v3, v2

    move v0, v2

    .line 941
    :goto_0
    invoke-virtual {p2}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/datamodel/Device;

    .line 942
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->i()Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->UNREGISTERED:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    if-eq v7, v8, :cond_3

    invoke-static {v5, v0}, Lorg/apache/commons/lang3/ObjectUtils;->notEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0a0415

    invoke-virtual {p1, v0}, Lcom/mfluent/asp/ASPApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 947
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 948
    const-string v0, ".*(\\s+\\([0-9]+\\))$"

    invoke-virtual {v1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 949
    const-string v0, " ("

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 951
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v1, v4

    move p3, v4

    .line 958
    :goto_2
    if-nez v1, :cond_4

    .line 960
    if-eqz p3, :cond_2

    .line 961
    iget-object v1, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v1

    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/mfluent/asp/b/i;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 962
    if-eqz v1, :cond_2

    .line 963
    invoke-virtual {v5, v0}, Lcom/mfluent/asp/datamodel/Device;->j(Ljava/lang/String;)V

    .line 964
    invoke-virtual {v5}, Lcom/mfluent/asp/datamodel/Device;->O()Z

    .line 965
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mfluent.asp.DataModel.REFRESH_ALL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 966
    iget-object v1, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 969
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 957
    goto/16 :goto_1

    :cond_4
    move-object v9, v0

    move v0, v1

    move-object v1, v9

    goto/16 :goto_0

    :cond_5
    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto :goto_2
.end method

.method private a(Lcom/mfluent/asp/datamodel/Device;)V
    .locals 4

    .prologue
    .line 1247
    :try_start_0
    iget-object v0, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v0

    .line 1248
    invoke-virtual {v0, p1}, Lcom/mfluent/asp/b/i;->a(Lcom/mfluent/asp/datamodel/Device;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1257
    :goto_0
    return-void

    .line 1254
    :catch_0
    move-exception v0

    .line 1255
    const-string v1, "mfl_DeviceListSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trouble saving settings to server because "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static b(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 183
    if-eqz p0, :cond_0

    const-string v0, "com.mfluent.asp.sync.DeviceListSyncManager.SYNC_ON_WAKELOCK"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->a:Lorg/slf4j/Logger;

    return-object v0
.end method

.method private d()Lorg/json/JSONArray;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x6

    const/4 v1, 0x0

    .line 304
    iget-object v0, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/a;->b()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v2

    .line 307
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/pcw/hybrid/b/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 308
    :cond_0
    sget-object v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0, v6}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->canLog(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    const-string v0, "mfl_DeviceListSyncManager"

    const-string v2, "::syncDeviceInfoListFWK: authInfo is null."

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    :cond_1
    :goto_0
    return-object v1

    .line 314
    :cond_2
    iget-object v0, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/b/i;->a(Landroid/content/Context;)Lcom/mfluent/asp/b/i;

    move-result-object v3

    .line 315
    invoke-virtual {v3}, Lcom/mfluent/asp/b/i;->b()Ljava/lang/String;

    move-result-object v0

    .line 317
    invoke-static {v0}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 323
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 355
    const-string v0, "deviceInfoList"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 356
    if-nez v2, :cond_4

    .line 357
    sget-object v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v5, :cond_1

    .line 358
    const-string v0, "mfl_DeviceListSyncManager"

    const-string v2, "FWK: No \'registeredAllDeviceInfoList\' value in device list json response"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 324
    :catch_0
    move-exception v0

    .line 325
    sget-object v4, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    if-gt v4, v5, :cond_3

    .line 326
    const-string v4, "mfl_DeviceListSyncManager"

    const-string v5, "Trouble syncing device list"

    invoke-static {v4, v5, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 328
    :cond_3
    sget-boolean v0, Lcom/mfluent/asp/common/util/AspLogLevels;->BUGSENSE_LOGGING:Z

    .line 337
    :try_start_1
    invoke-virtual {v3, v2}, Lcom/mfluent/asp/b/i;->a(Lcom/sec/pcw/hybrid/b/b;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 338
    :catch_1
    move-exception v0

    .line 339
    sget-object v2, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v6, :cond_1

    .line 340
    const-string v2, "mfl_DeviceListSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::syncDeviceInfoListFWK: failed to addOSPDevice"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 342
    :catch_2
    move-exception v0

    .line 343
    sget-object v2, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    if-gt v2, v6, :cond_1

    .line 344
    const-string v2, "mfl_DeviceListSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::syncDeviceInfoListFWK: failed to addOSPDevice"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 365
    :cond_4
    :try_start_2
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3

    .line 378
    const-string v0, "deviceInfo"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 379
    if-nez v0, :cond_5

    .line 381
    const-string v0, "deviceInfo"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 382
    if-eqz v0, :cond_8

    .line 384
    :try_start_3
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 385
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 386
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 388
    sget-object v2, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_5

    .line 389
    const-string v2, "mfl_DeviceListSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::syncDeviceInfoListFWK: NEWJSON : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_4

    :cond_5
    :goto_1
    move-object v1, v0

    .line 406
    goto/16 :goto_0

    .line 366
    :catch_3
    move-exception v0

    .line 368
    sget-object v3, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v5, :cond_6

    .line 369
    const-string v3, "mfl_DeviceListSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Trouble parsing \'registeredAllDeviceInfoList\' value: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 371
    :cond_6
    sget-boolean v0, Lcom/mfluent/asp/common/util/AspLogLevels;->BUGSENSE_LOGGING:Z

    goto/16 :goto_0

    .line 395
    :catch_4
    move-exception v0

    sget-object v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v5, :cond_7

    .line 396
    const-string v0, "mfl_DeviceListSyncManager"

    const-string v2, "FWK: No \'list\' value in device list json response"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move-object v0, v1

    .line 399
    goto :goto_1

    .line 401
    :cond_8
    sget-object v0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v5, :cond_9

    .line 402
    const-string v0, "mfl_DeviceListSyncManager"

    const-string v2, "FWK: No \'list\' value in device list json response"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move-object v0, v1

    .line 404
    goto :goto_1
.end method

.method private j(Landroid/content/Intent;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 251
    .line 253
    :try_start_0
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 255
    sget-object v2, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x4

    if-gt v2, v3, :cond_0

    .line 256
    const-string v2, "mfl_DeviceListSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Syncing device list. prev#: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->b()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->d()Lorg/json/JSONArray;

    move-result-object v2

    .line 264
    if-eqz v2, :cond_1

    .line 265
    invoke-direct {p0, v0, v2, p1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->a(Lcom/mfluent/asp/datamodel/t;Lorg/json/JSONArray;Landroid/content/Intent;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 266
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    .line 278
    :goto_0
    :try_start_2
    const-string v0, "WearableDeviceManager.getWearableList"

    .line 279
    iget-object v3, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/sdk/samsung/a/a;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/a/a;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/samsung/a/a;->a(Ljava/lang/String;)Lcom/mfluent/asp/datamodel/Device;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 285
    :goto_1
    if-eqz v1, :cond_3

    .line 286
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->h(Landroid/content/Intent;)V

    .line 288
    :goto_2
    return v2

    .line 268
    :cond_1
    :try_start_3
    const-string v0, "mfl_DeviceListSyncManager"

    const-string v2, "::syncDeviceList:syncDeviceInfoListFWK returns null "

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v2, v1

    .line 275
    goto :goto_0

    .line 271
    :catch_0
    move-exception v0

    .line 272
    :try_start_4
    sget-object v2, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x3

    if-gt v2, v3, :cond_2

    .line 273
    const-string v2, "mfl_DeviceListSyncManager"

    const-string v3, "::syncDeviceList:Trouble syncing device info list"

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move v2, v1

    goto :goto_0

    .line 280
    :catch_1
    move-exception v0

    .line 281
    const-string v3, "mfl_DeviceListSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::syncDeviceList: can\'t find WearableDeviceManager "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 285
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 286
    invoke-virtual {p0, p1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->h(Landroid/content/Intent;)V

    .line 288
    :goto_3
    throw v0

    :cond_3
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->j()V

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->j()V

    goto :goto_3
.end method

.method private static k(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 1237
    if-nez p0, :cond_0

    .line 1238
    const/4 v0, 0x0

    .line 1242
    :goto_0
    return v0

    .line 1241
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1242
    const-string v1, "com.mfluent.asp.sync.DeviceListSyncManager.SIGN_IN_SYNC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected final a()Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/IntentFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    invoke-super {p0}, Lcom/mfluent/asp/sync/j;->a()Ljava/util/Collection;

    move-result-object v0

    .line 146
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.DataModel.REFRESH_ALL"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 147
    new-instance v1, Landroid/content/IntentFilter;

    sget-object v2, Lcom/mfluent/asp/ASPApplication;->e:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 148
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.DataModel.DEVICE_LIST_REFRESH"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 149
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mfluent.asp.sync.DeviceListSyncManager.SYNC_ON_WAKELOCK"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 151
    return-object v0
.end method

.method protected final a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 197
    if-eqz p1, :cond_2

    .line 198
    const-string v2, "mfl_DeviceListSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeviceListSync due to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :goto_0
    if-eqz p1, :cond_3

    const-string v2, "com.mfluent.asp.sync.DeviceListSyncManager.DEVICE_LIST_REQUEST_VIA_PUSH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v0

    :goto_1
    if-nez v2, :cond_5

    if-eqz p1, :cond_0

    const-string v2, "com.mfluent.asp.sync.DeviceListSyncManager.SIGN_IN_SYNC"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    :cond_0
    if-nez v1, :cond_5

    .line 210
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->b()Z

    move-result v1

    if-nez v1, :cond_4

    .line 211
    const-string v0, "mfl_DeviceListSyncManager"

    const-string v1, "DeviceList skipping since no one has requested NTS Lock"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_1
    :goto_2
    return-void

    .line 200
    :cond_2
    const-string v2, "mfl_DeviceListSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeviceListSync due to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v2, v1

    .line 203
    goto :goto_1

    .line 216
    :cond_4
    invoke-static {p1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 217
    iget-wide v2, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->h:J

    sget-wide v4, Lcom/mfluent/asp/sync/DeviceListSyncManager;->g:J

    add-long/2addr v2, v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    .line 226
    :cond_5
    :try_start_0
    invoke-direct {p0, p1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->j(Landroid/content/Intent;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 232
    :cond_6
    :goto_3
    invoke-static {p1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->k(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 233
    const-string v1, "mfl_DeviceListSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Broadcasting BROADCAST_SIGN_IN_SYNC_FINISHED - EXTRA_SIGN_IN_RESULT="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.sync.DeviceListSyncManager.BROADCAST_SIGN_IN_SYNC_FINISHED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 235
    const-string v2, "com.mfluent.asp.sync.DeviceListSyncManager.EXTRA_SIGN_IN_RESULT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 236
    iget-object v0, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_2

    .line 227
    :catch_0
    move-exception v1

    .line 228
    sget-object v2, Lcom/mfluent/asp/sync/DeviceListSyncManager;->b:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x6

    if-gt v2, v3, :cond_6

    .line 229
    const-string v2, "mfl_DeviceListSyncManager"

    const-string v3, "Interrupted while syncing device list"

    invoke-static {v2, v3, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method

.method protected final c(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 160
    invoke-super {p0, p1}, Lcom/mfluent/asp/sync/j;->c(Landroid/content/Intent;)V

    .line 162
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->h:J

    .line 163
    iget-object v0, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 164
    iget-object v1, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->e:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/mfluent/asp/nts/a;->d:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 166
    iget-object v1, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->f:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/mfluent/asp/ASPApplication;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 167
    return-void
.end method

.method protected final d(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 175
    invoke-super {p0, p1}, Lcom/mfluent/asp/sync/j;->d(Landroid/content/Intent;)V

    .line 177
    iget-object v0, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 178
    iget-object v1, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 179
    iget-object v1, p0, Lcom/mfluent/asp/sync/DeviceListSyncManager;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 180
    return-void
.end method
