.class public Lcom/mfluent/common/android/log/AndroidLogger;
.super Lorg/slf4j/helpers/MarkerIgnoringBase;
.source "SourceFile"


# static fields
.field private static final serialVersionUID:J = -0x110827acc929f301L


# instance fields
.field private level:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lorg/slf4j/helpers/MarkerIgnoringBase;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    .line 90
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    :try_start_0
    invoke-static {p0, p1, p2}, Lorg/slf4j/helpers/MessageFormatter;->format(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Lorg/slf4j/helpers/FormattingTuple;

    move-result-object v0

    invoke-virtual {v0}, Lorg/slf4j/helpers/FormattingTuple;->getMessage()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 336
    :goto_0
    return-object v0

    .line 333
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/StackOverflowError;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 346
    invoke-static {p0, p1}, Lorg/slf4j/helpers/MessageFormatter;->arrayFormat(Ljava/lang/String;[Ljava/lang/Object;)Lorg/slf4j/helpers/FormattingTuple;

    move-result-object v0

    invoke-virtual {v0}, Lorg/slf4j/helpers/FormattingTuple;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 81
    iput p1, p0, Lcom/mfluent/common/android/log/AndroidLogger;->level:I

    .line 82
    return-void
.end method

.method public debug(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_0
    return-void
.end method

.method public debug(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_0
    return-void
.end method

.method public debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_0
    return-void
.end method

.method public debug(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 182
    :cond_0
    return-void
.end method

.method public varargs debug(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_0
    return-void
.end method

.method public error(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isErrorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :cond_0
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isErrorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :cond_0
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isErrorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :cond_0
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isErrorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 320
    :cond_0
    return-void
.end method

.method public varargs error(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isErrorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :cond_0
    return-void
.end method

.method public info(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_0
    return-void
.end method

.method public info(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_0
    return-void
.end method

.method public info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_0
    return-void
.end method

.method public info(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 228
    :cond_0
    return-void
.end method

.method public varargs info(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    :cond_0
    return-void
.end method

.method public isDebugEnabled()Z
    .locals 2

    .prologue
    .line 141
    iget v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->level:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isErrorEnabled()Z
    .locals 2

    .prologue
    .line 279
    iget v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->level:I

    const/4 v1, 0x6

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInfoEnabled()Z
    .locals 2

    .prologue
    .line 187
    iget v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->level:I

    const/4 v1, 0x4

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTraceEnabled()Z
    .locals 2

    .prologue
    .line 95
    iget v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->level:I

    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWarnEnabled()Z
    .locals 2

    .prologue
    .line 233
    iget v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->level:I

    const/4 v1, 0x5

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public trace(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isTraceEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_0
    return-void
.end method

.method public trace(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isTraceEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_0
    return-void
.end method

.method public trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isTraceEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_0
    return-void
.end method

.method public trace(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isTraceEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 136
    :cond_0
    return-void
.end method

.method public varargs trace(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isTraceEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_0
    return-void
.end method

.method public warn(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isWarnEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_0
    return-void
.end method

.method public warn(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isWarnEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_0
    return-void
.end method

.method public warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isWarnEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_0
    return-void
.end method

.method public warn(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isWarnEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 274
    :cond_0
    return-void
.end method

.method public varargs warn(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/mfluent/common/android/log/AndroidLogger;->isWarnEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/mfluent/common/android/log/AndroidLogger;->name:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/mfluent/common/android/log/AndroidLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :cond_0
    return-void
.end method
