.class public Lcom/mfluent/a/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final b:Ljava/io/File;

.field private static final c:Ljava/io/File;


# instance fields
.field private final d:Landroid/os/storage/StorageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/mfluent/a/a/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 30
    new-instance v0, Ljava/io/File;

    const-string v1, "/storage/sdcard0"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/mfluent/a/a/b;->b:Ljava/io/File;

    .line 31
    new-instance v0, Ljava/io/File;

    const-string v1, "/storage/emulated"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/mfluent/a/a/b;->c:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, "storage"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    iput-object v0, p0, Lcom/mfluent/a/a/b;->d:Landroid/os/storage/StorageManager;

    .line 37
    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/io/File;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 98
    invoke-static {p0}, Landroid/support/v4/content/ContextCompat;->getExternalCacheDirs(Landroid/content/Context;)[Ljava/io/File;

    move-result-object v1

    .line 100
    array-length v0, v1

    const/4 v2, 0x2

    if-lt v0, v2, :cond_2

    aget-object v0, v1, v3

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/mfluent/a/a/b;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 101
    aget-object v0, v1, v3

    .line 106
    :goto_0
    if-nez v0, :cond_0

    .line 107
    const-string v0, "mfl_MFLStorageManager"

    const-string v2, "WTF!!::getCacheDir:cacheDir is null"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    new-instance v0, Ljava/io/File;

    const-string v2, "/sdcard/Android/data/com.samsung.android.sdk.samsunglink/cache"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 111
    :cond_0
    sget-object v2, Lcom/mfluent/a/a/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x3

    if-gt v2, v3, :cond_1

    .line 112
    const-string v2, "mfl_MFLStorageManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getCacheDir:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  cacheDirs.length : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_1
    return-object v0

    .line 103
    :cond_2
    const/4 v0, 0x0

    aget-object v0, v1, v0

    goto :goto_0
.end method

.method public static a(Z)Ljava/io/File;
    .locals 3

    .prologue
    .line 123
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/mfluent/a/a/b;->d(Z)Ljava/io/File;

    move-result-object v1

    const-string v2, "Music"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Z)Ljava/io/File;
    .locals 4

    .prologue
    .line 131
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/io/File;

    invoke-static {p0}, Lcom/mfluent/a/a/b;->d(Z)Ljava/io/File;

    move-result-object v2

    const-string v3, "DCIM"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v2, "Photo"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b()Z
    .locals 2

    .prologue
    .line 66
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    .line 67
    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no local device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->r()Z

    move-result v1

    .line 72
    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->q()Z

    move-result v0

    .line 74
    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c()Ljava/io/File;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/mfluent/a/a/b;->d(Z)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static c(Z)Ljava/io/File;
    .locals 4

    .prologue
    .line 135
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/io/File;

    invoke-static {p0}, Lcom/mfluent/a/a/b;->d(Z)Ljava/io/File;

    move-result-object v2

    const-string v3, "DCIM"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v2, "Video"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static d()Ljava/io/File;
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/mfluent/a/a/b;->a(Z)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private static d(Z)Ljava/io/File;
    .locals 3

    .prologue
    .line 82
    invoke-static {}, Lcom/mfluent/a/a/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    invoke-static {}, Lcom/sec/pcw/service/d/a;->a()Ljava/io/File;

    move-result-object v0

    .line 93
    :cond_0
    :goto_0
    return-object v0

    .line 85
    :cond_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 87
    if-eqz p0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    sget-object v2, Lcom/mfluent/a/a/b;->c:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    sget-object v1, Lcom/mfluent/a/a/b;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/mfluent/a/a/b;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    sget-object v0, Lcom/mfluent/a/a/b;->b:Ljava/io/File;

    goto :goto_0
.end method

.method public static e()Ljava/io/File;
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/mfluent/a/a/b;->b(Z)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static f()Ljava/io/File;
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/mfluent/a/a/b;->c(Z)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static g()Ljava/io/File;
    .locals 3

    .prologue
    .line 143
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/mfluent/a/a/b;->d(Z)Ljava/io/File;

    move-result-object v1

    const-string v2, "files"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 144
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 145
    return-object v0
.end method

.method public static h()Ljava/io/File;
    .locals 3

    .prologue
    .line 161
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/mfluent/a/a/b;->d(Z)Ljava/io/File;

    move-result-object v1

    const-string v2, "Documents"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static i()Z
    .locals 1

    .prologue
    .line 175
    const-string v0, "EMULATED_STORAGE_TARGET"

    invoke-static {v0}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 54
    :try_start_0
    const-class v0, Landroid/os/storage/StorageManager;

    const-string v1, "getVolumeState"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/mfluent/a/a/b;->d:Landroid/os/storage/StorageManager;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    sget-object v1, Lcom/mfluent/a/a/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x5

    if-gt v1, v2, :cond_0

    .line 58
    const-string v1, "mfl_MFLStorageManager"

    const-string v2, "::getVolumeState:failed to invoke method android.os.storage.StorageManager.getVolumeState"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    :cond_0
    new-instance v0, Ljava/lang/NoSuchMethodError;

    const-string v1, "android.os.storage.StorageManager.getVolumeState"

    invoke-direct {v0, v1}, Ljava/lang/NoSuchMethodError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a()[Landroid/os/storage/StorageVolume;
    .locals 3

    .prologue
    .line 41
    :try_start_0
    const-class v0, Landroid/os/storage/StorageManager;

    const-string v1, "getVolumeList"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/mfluent/a/a/b;->d:Landroid/os/storage/StorageManager;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/storage/StorageVolume;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    sget-object v1, Lcom/mfluent/a/a/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x5

    if-gt v1, v2, :cond_0

    .line 45
    const-string v1, "mfl_MFLStorageManager"

    const-string v2, "::getVolumeList:failed to invoke method android.os.storage.StorageManager.getVolumeList"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 48
    :cond_0
    new-instance v0, Ljava/lang/NoSuchMethodError;

    const-string v1, "android.os.storage.StorageManager.getVolumeList"

    invoke-direct {v0, v1}, Ljava/lang/NoSuchMethodError;-><init>(Ljava/lang/String;)V

    throw v0
.end method
