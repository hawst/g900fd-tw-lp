.class public final enum Lcom/sec/pcw/util/Common$ServerType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/util/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ServerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/pcw/util/Common$ServerType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/pcw/util/Common$ServerType;

.field public static final enum b:Lcom/sec/pcw/util/Common$ServerType;

.field private static final synthetic c:[Lcom/sec/pcw/util/Common$ServerType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 746
    new-instance v0, Lcom/sec/pcw/util/Common$ServerType;

    const-string v1, "Production"

    invoke-direct {v0, v1, v2}, Lcom/sec/pcw/util/Common$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/Common$ServerType;->a:Lcom/sec/pcw/util/Common$ServerType;

    .line 747
    new-instance v0, Lcom/sec/pcw/util/Common$ServerType;

    const-string v1, "Staging"

    invoke-direct {v0, v1, v3}, Lcom/sec/pcw/util/Common$ServerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/Common$ServerType;->b:Lcom/sec/pcw/util/Common$ServerType;

    .line 745
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/pcw/util/Common$ServerType;

    sget-object v1, Lcom/sec/pcw/util/Common$ServerType;->a:Lcom/sec/pcw/util/Common$ServerType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/pcw/util/Common$ServerType;->b:Lcom/sec/pcw/util/Common$ServerType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/pcw/util/Common$ServerType;->c:[Lcom/sec/pcw/util/Common$ServerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 745
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/pcw/util/Common$ServerType;
    .locals 1

    .prologue
    .line 745
    const-class v0, Lcom/sec/pcw/util/Common$ServerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/util/Common$ServerType;

    return-object v0
.end method

.method public static values()[Lcom/sec/pcw/util/Common$ServerType;
    .locals 1

    .prologue
    .line 745
    sget-object v0, Lcom/sec/pcw/util/Common$ServerType;->c:[Lcom/sec/pcw/util/Common$ServerType;

    invoke-virtual {v0}, [Lcom/sec/pcw/util/Common$ServerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/pcw/util/Common$ServerType;

    return-object v0
.end method
