.class final enum Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/util/LanguageUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "LanguageBlock"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

.field public static final enum b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

.field public static final enum c:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

.field public static final enum d:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

.field public static final enum e:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

.field public static final enum f:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

.field public static final enum g:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

.field public static final enum h:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

.field public static final enum i:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

.field public static final enum j:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

.field public static final enum k:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

.field private static final synthetic l:[Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 51
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->a:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 54
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    const-string v1, "LATIN"

    invoke-direct {v0, v1, v4}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 57
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    const-string v1, "CYRILLIC"

    invoke-direct {v0, v1, v5}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->c:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 60
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    const-string v1, "GREEK"

    invoke-direct {v0, v1, v6}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->d:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 63
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    const-string v1, "HANGUL"

    invoke-direct {v0, v1, v7}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->e:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 66
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    const-string v1, "CJK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->f:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 69
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    const-string v1, "JAPANESE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->g:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 72
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    const-string v1, "ARABIC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->h:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 75
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    const-string v1, "HEBREW"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->i:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 78
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    const-string v1, "THAI"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->j:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 81
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    const-string v1, "HAF"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->k:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 48
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    sget-object v1, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->a:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->c:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->d:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->e:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->f:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->g:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->h:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->i:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->j:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->k:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->l:[Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    return-object v0
.end method

.method public static values()[Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->l:[Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v0}, [Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    return-object v0
.end method
