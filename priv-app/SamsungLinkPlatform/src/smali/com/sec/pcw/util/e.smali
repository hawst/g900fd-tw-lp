.class public final Lcom/sec/pcw/util/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 24
    sput-boolean v0, Lcom/sec/pcw/util/e;->a:Z

    .line 25
    const/4 v1, 0x2

    sput v1, Lcom/sec/pcw/util/e;->b:I

    .line 28
    invoke-static {}, Lcom/sec/pcw/util/e;->a()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    sput-boolean v0, Lcom/sec/pcw/util/e;->a:Z

    .line 29
    const-string v0, "SL_DEBUG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IS_SHIP : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/pcw/util/e;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 30
    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x2

    invoke-static {p0, p1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 99
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 48
    sget-boolean v0, Lcom/sec/pcw/util/e;->a:Z

    if-eqz v0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 51
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 60
    :pswitch_0
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 53
    :pswitch_1
    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 56
    :pswitch_2
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 63
    :pswitch_3
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 66
    :pswitch_4
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 51
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x2

    invoke-static {p0, p1, p2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 103
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V
    .locals 1

    .prologue
    .line 73
    sget-boolean v0, Lcom/sec/pcw/util/e;->a:Z

    if-eqz v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 76
    :cond_0
    packed-switch p3, :pswitch_data_0

    .line 85
    :pswitch_0
    invoke-static {p0, p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 78
    :pswitch_1
    invoke-static {p0, p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 81
    :pswitch_2
    invoke-static {p0, p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 88
    :pswitch_3
    invoke-static {p0, p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 91
    :pswitch_4
    invoke-static {p0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 130
    sget-boolean v0, Lcom/sec/pcw/util/e;->a:Z

    if-nez v0, :cond_0

    .line 131
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 133
    :cond_0
    return-void
.end method

.method public static a()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 34
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v2, "/mnt/sdcard/SL_DEBUG"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 35
    const-string v2, "SL_DEBUG"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isLoggable:: isProductShip = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    const-string v2, "SL_DEBUG"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isLoggable:: SL_DEBUG_EXIST = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v0

    .line 40
    :catch_0
    move-exception v0

    .line 41
    const-string v1, "SL_DEBUG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isLoggable:: Exception occurred : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x3

    invoke-static {p0, p1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 107
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x3

    invoke-static {p0, p1, p2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 111
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 115
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x4

    invoke-static {p0, p1, p2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 119
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x5

    invoke-static {p0, p1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 123
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x5

    invoke-static {p0, p1, p2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 127
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x6

    invoke-static {p0, p1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 137
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x6

    invoke-static {p0, p1, p2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;I)V

    .line 141
    return-void
.end method
