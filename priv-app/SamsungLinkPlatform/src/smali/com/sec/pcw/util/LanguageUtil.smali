.class public final Lcom/sec/pcw/util/LanguageUtil;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/util/LanguageUtil$1;,
        Lcom/sec/pcw/util/LanguageUtil$i;,
        Lcom/sec/pcw/util/LanguageUtil$j;,
        Lcom/sec/pcw/util/LanguageUtil$f;,
        Lcom/sec/pcw/util/LanguageUtil$a;,
        Lcom/sec/pcw/util/LanguageUtil$g;,
        Lcom/sec/pcw/util/LanguageUtil$b;,
        Lcom/sec/pcw/util/LanguageUtil$e;,
        Lcom/sec/pcw/util/LanguageUtil$d;,
        Lcom/sec/pcw/util/LanguageUtil$c;,
        Lcom/sec/pcw/util/LanguageUtil$h;,
        Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final b:Ljava/lang/Object;

.field private static c:J

.field private static d:Ljava/util/Properties;

.field private static e:Ljava/util/Timer;

.field private static f:Ljava/util/TimerTask;

.field private static g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;",
            ">;"
        }
    .end annotation
.end field

.field private static h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Character$UnicodeBlock;",
            "Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 36
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil;->b:Ljava/lang/Object;

    .line 40
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/pcw/util/LanguageUtil;->c:J

    .line 41
    sput-object v2, Lcom/sec/pcw/util/LanguageUtil;->d:Ljava/util/Properties;

    .line 42
    sput-object v2, Lcom/sec/pcw/util/LanguageUtil;->e:Ljava/util/Timer;

    .line 43
    sput-object v2, Lcom/sec/pcw/util/LanguageUtil;->f:Ljava/util/TimerTask;

    .line 251
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->g()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil;->g:Ljava/util/HashMap;

    .line 288
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->h()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil;->h:Ljava/util/HashMap;

    return-void
.end method

.method public static a(Ljava/lang/String;)C
    .locals 4

    .prologue
    const/16 v1, 0x23

    const/4 v2, 0x0

    .line 357
    .line 359
    if-nez p0, :cond_1

    move v0, v2

    .line 418
    :cond_0
    :goto_0
    return v0

    .line 363
    :cond_1
    sget-object v0, Ljava/text/Normalizer$Form;->NFD:Ljava/text/Normalizer$Form;

    invoke-static {p0, v0}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v0

    .line 364
    if-nez v0, :cond_8

    .line 367
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 368
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 370
    :cond_2
    const/16 v0, 0x7f

    if-ne v2, v0, :cond_3

    .line 371
    const-string p0, "#"

    .line 374
    :cond_3
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v3

    .line 376
    invoke-static {v2}, Lcom/sec/pcw/util/LanguageUtil;->a(C)Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    move-result-object v0

    .line 378
    sget-object v2, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->k:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v0, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 379
    const-string v0, "\u30a2"

    invoke-virtual {v3, v0, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_6

    const-string v0, "\u30ef"

    invoke-virtual {v3, v0, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_6

    .line 380
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->g:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 388
    :cond_4
    :goto_2
    sget-object v2, Lcom/sec/pcw/util/LanguageUtil$1;->a:[I

    invoke-virtual {v0}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_5
    move v0, v1

    .line 414
    :goto_3
    if-nez v0, :cond_0

    move v0, v1

    .line 415
    goto :goto_0

    .line 381
    :cond_6
    const-string v0, "\u3131"

    invoke-virtual {v3, v0, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_7

    const-string v0, "\u314e"

    invoke-virtual {v3, v0, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_7

    .line 382
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->e:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    goto :goto_2

    .line 384
    :cond_7
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    goto :goto_2

    .line 390
    :pswitch_0
    const-string v0, "a"

    invoke-virtual {v3, v0, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_5

    .line 391
    invoke-static {p0}, Lcom/sec/pcw/util/LanguageUtil;->c(Ljava/lang/String;)C

    move-result v0

    goto :goto_3

    .line 398
    :pswitch_1
    invoke-static {p0}, Lcom/sec/pcw/util/LanguageUtil;->d(Ljava/lang/String;)C

    move-result v0

    goto :goto_3

    .line 402
    :pswitch_2
    invoke-static {p0}, Lcom/sec/pcw/util/LanguageUtil;->e(Ljava/lang/String;)C

    move-result v0

    goto :goto_3

    .line 406
    :pswitch_3
    invoke-static {p0}, Lcom/sec/pcw/util/LanguageUtil;->f(Ljava/lang/String;)C

    move-result v0

    goto :goto_3

    :cond_8
    move-object p0, v0

    goto :goto_1

    .line 388
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(C)Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;
    .locals 2

    .prologue
    .line 346
    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 347
    sget-object v1, Lcom/sec/pcw/util/LanguageUtil;->h:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 349
    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->a:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    :cond_0
    return-object v0
.end method

.method static synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public static a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 762
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 763
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 764
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 765
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 766
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 768
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->i()Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    move-result-object v5

    .line 770
    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v6}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v6

    .line 773
    const-string v7, " "

    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {p1, v13, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 774
    invoke-interface {p0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 775
    const-string v8, " "

    invoke-virtual {p1, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 777
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-nez v9, :cond_0

    .line 820
    :goto_0
    return-object v0

    .line 780
    :cond_0
    :goto_1
    invoke-interface {p0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v9

    if-nez v9, :cond_6

    .line 781
    invoke-interface {p0}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    .line 782
    const/4 v10, -0x1

    if-ne v7, v10, :cond_1

    .line 784
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 805
    :goto_2
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 786
    :cond_1
    invoke-interface {p0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/pcw/util/LanguageUtil;->b(Ljava/lang/String;)C

    move-result v10

    .line 788
    invoke-static {v10}, Lcom/sec/pcw/util/LanguageUtil;->a(C)Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    move-result-object v11

    .line 789
    sget-object v12, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v12, v11}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 790
    const-string v11, "a"

    invoke-static {v10}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v11, v10}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    if-lez v10, :cond_2

    .line 791
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 793
    :cond_2
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 795
    :cond_3
    invoke-virtual {v5, v11}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 796
    sget-object v11, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->e:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v5, v11}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    const-string v11, "\u314e"

    invoke-static {v10}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v11, v10}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    if-gez v10, :cond_4

    .line 797
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 799
    :cond_4
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 802
    :cond_5
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 808
    :cond_6
    const-string v5, "ASC"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 809
    invoke-virtual {v0, v13, v1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 810
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 811
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 812
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1, v4}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    goto/16 :goto_0

    .line 814
    :cond_7
    invoke-virtual {v0, v13, v4}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 815
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v0, v4, v2}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 816
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2, v3}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 817
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    goto/16 :goto_0
.end method

.method private static a(Landroid/database/Cursor;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 867
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 868
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 869
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 870
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 871
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 873
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->i()Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    move-result-object v6

    .line 875
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v7

    .line 879
    invoke-virtual {p1, v12}, Ljava/util/ArrayList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v8

    .line 880
    :goto_0
    invoke-interface {v8}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 881
    invoke-interface {v8}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 882
    invoke-interface {p0, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 883
    const-string v9, "title"

    invoke-interface {p0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/pcw/util/LanguageUtil;->b(Ljava/lang/String;)C

    move-result v9

    .line 885
    invoke-static {v9}, Lcom/sec/pcw/util/LanguageUtil;->a(C)Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    move-result-object v10

    .line 886
    sget-object v11, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v11, v10}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 887
    const-string v10, "a"

    invoke-static {v9}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v10, v9}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    if-lez v9, :cond_0

    .line 888
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 890
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 892
    :cond_1
    invoke-virtual {v6, v10}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 893
    sget-object v10, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->e:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v6, v10}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const-string v10, "\u314e"

    invoke-static {v9}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v10, v9}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    if-gez v9, :cond_2

    .line 894
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 896
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 899
    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 903
    :cond_4
    const-string v0, "ASC"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 904
    invoke-virtual {v1, v12, v2}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 905
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0, v4}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 906
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0, v3}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 907
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0, v5}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 915
    :goto_1
    return-object v1

    .line 909
    :cond_5
    invoke-virtual {v1, v12, v5}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 910
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0, v3}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 911
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0, v4}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 912
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;)Ljava/util/Properties;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 659
    sget-object v2, Lcom/sec/pcw/util/LanguageUtil;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 660
    :try_start_0
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil;->d:Ljava/util/Properties;

    if-nez v0, :cond_0

    .line 661
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 663
    const/4 v1, 0x0

    .line 665
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    const-string v4, "unicode-to-pinyin.txt"

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 666
    invoke-virtual {v0, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 667
    sput-object v0, Lcom/sec/pcw/util/LanguageUtil;->d:Ljava/util/Properties;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 669
    :try_start_2
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    .line 673
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v4, 0x927c0

    add-long/2addr v0, v4

    sput-wide v0, Lcom/sec/pcw/util/LanguageUtil;->c:J

    .line 674
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil;->f:Ljava/util/TimerTask;

    if-nez v0, :cond_2

    .line 676
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil;->e:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 677
    new-instance v0, Ljava/util/Timer;

    const-string v1, "PinyinTableRelease"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil;->e:Ljava/util/Timer;

    .line 680
    :cond_1
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$i;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/pcw/util/LanguageUtil$i;-><init>(B)V

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil;->f:Ljava/util/TimerTask;

    .line 681
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil;->e:Ljava/util/Timer;

    sget-object v1, Lcom/sec/pcw/util/LanguageUtil;->f:Ljava/util/TimerTask;

    new-instance v3, Ljava/util/Date;

    sget-wide v4, Lcom/sec/pcw/util/LanguageUtil;->c:J

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;Ljava/util/Date;)V

    .line 684
    :cond_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 686
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil;->d:Ljava/util/Properties;

    return-object v0

    .line 669
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 684
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method static synthetic a(Ljava/util/TimerTask;)Ljava/util/TimerTask;
    .locals 0

    .prologue
    .line 33
    sput-object p0, Lcom/sec/pcw/util/LanguageUtil;->f:Ljava/util/TimerTask;

    return-object p0
.end method

.method public static b(Ljava/lang/String;)C
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/16 v3, 0x23

    .line 422
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->i()Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    move-result-object v5

    .line 426
    if-nez p0, :cond_1

    move v0, v2

    .line 539
    :cond_0
    :goto_0
    return v0

    .line 430
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 432
    sget-object v0, Ljava/text/Normalizer$Form;->NFD:Ljava/text/Normalizer$Form;

    invoke-static {v1, v0}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v0

    .line 433
    if-nez v0, :cond_2

    move-object v0, v1

    .line 436
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_11

    .line 437
    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 439
    :goto_1
    const/16 v2, 0x7f

    if-ne v4, v2, :cond_3

    .line 440
    const-string v0, "#"

    .line 443
    :cond_3
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v2}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v6

    .line 445
    invoke-static {v4}, Lcom/sec/pcw/util/LanguageUtil;->a(C)Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    move-result-object v2

    .line 447
    sget-object v7, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->k:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v7}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 448
    const-string v2, "\u30a2"

    invoke-virtual {v6, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_6

    const-string v2, "\u30ef"

    invoke-virtual {v6, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_6

    .line 449
    sget-object v2, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->g:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    .line 457
    :cond_4
    :goto_2
    sget-object v7, Lcom/sec/pcw/util/LanguageUtil$1;->a:[I

    invoke-virtual {v5}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    :cond_5
    move v0, v3

    .line 535
    :goto_3
    if-nez v0, :cond_0

    move v0, v3

    .line 536
    goto :goto_0

    .line 450
    :cond_6
    const-string v2, "\u3131"

    invoke-virtual {v6, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_7

    const-string v2, "\u314e"

    invoke-virtual {v6, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_7

    .line 451
    sget-object v2, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->e:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    goto :goto_2

    .line 453
    :cond_7
    sget-object v2, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    goto :goto_2

    .line 459
    :pswitch_0
    invoke-virtual {v5, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 460
    const-string v2, "a"

    invoke-virtual {v6, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_5

    .line 461
    invoke-static {v1}, Lcom/sec/pcw/util/LanguageUtil;->c(Ljava/lang/String;)C

    move-result v0

    goto :goto_3

    :cond_8
    move v0, v3

    .line 468
    goto :goto_3

    .line 475
    :pswitch_1
    invoke-virtual {v5, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 476
    sget-object v4, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v4, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 477
    const-string v2, "a"

    invoke-virtual {v6, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_5

    .line 478
    invoke-static {v1}, Lcom/sec/pcw/util/LanguageUtil;->c(Ljava/lang/String;)C

    move-result v0

    goto :goto_3

    :cond_9
    move v0, v3

    .line 483
    goto :goto_3

    .line 489
    :pswitch_2
    invoke-virtual {v5, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 490
    invoke-static {v0}, Lcom/sec/pcw/util/LanguageUtil;->d(Ljava/lang/String;)C

    move-result v0

    goto :goto_3

    .line 491
    :cond_a
    sget-object v4, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v4, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 492
    const-string v2, "a"

    invoke-virtual {v6, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_5

    .line 493
    invoke-static {v1}, Lcom/sec/pcw/util/LanguageUtil;->c(Ljava/lang/String;)C

    move-result v0

    goto :goto_3

    :cond_b
    move v0, v3

    .line 500
    goto :goto_3

    .line 503
    :pswitch_3
    invoke-virtual {v5, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 504
    invoke-static {v0}, Lcom/sec/pcw/util/LanguageUtil;->e(Ljava/lang/String;)C

    move-result v0

    goto :goto_3

    .line 505
    :cond_c
    sget-object v4, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v4, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 506
    const-string v2, "a"

    invoke-virtual {v6, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_5

    .line 507
    invoke-static {v1}, Lcom/sec/pcw/util/LanguageUtil;->c(Ljava/lang/String;)C

    move-result v0

    goto/16 :goto_3

    :cond_d
    move v0, v3

    .line 514
    goto/16 :goto_3

    .line 517
    :pswitch_4
    invoke-virtual {v5, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 518
    invoke-static {v0}, Lcom/sec/pcw/util/LanguageUtil;->f(Ljava/lang/String;)C

    move-result v0

    goto/16 :goto_3

    .line 519
    :cond_e
    sget-object v4, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v4, v2}, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 520
    const-string v2, "a"

    invoke-virtual {v6, v2, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_5

    .line 521
    invoke-static {v1}, Lcom/sec/pcw/util/LanguageUtil;->c(Ljava/lang/String;)C

    move-result v0

    goto/16 :goto_3

    :cond_f
    move v0, v3

    .line 528
    goto/16 :goto_3

    :cond_10
    move v0, v4

    goto/16 :goto_3

    :cond_11
    move v4, v2

    goto/16 :goto_1

    .line 457
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b()J
    .locals 2

    .prologue
    .line 33
    sget-wide v0, Lcom/sec/pcw/util/LanguageUtil;->c:J

    return-wide v0
.end method

.method public static b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 824
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 825
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 826
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 828
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 829
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 831
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v2

    .line 863
    :goto_0
    return-object v0

    .line 835
    :cond_0
    invoke-static {p0, p1}, Lcom/sec/pcw/util/LanguageUtil;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 836
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v6

    .line 838
    const/4 v1, 0x0

    .line 842
    :goto_1
    invoke-interface {v6}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 843
    invoke-interface {v6}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 844
    invoke-interface {p0, v7}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 846
    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 848
    if-nez v1, :cond_1

    .line 857
    :goto_2
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    goto :goto_1

    .line 850
    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 851
    invoke-static {p0, v3, v5}, Lcom/sec/pcw/util/LanguageUtil;->a(Landroid/database/Cursor;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 854
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    goto :goto_2

    .line 859
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 860
    invoke-static {p0, v3, v5}, Lcom/sec/pcw/util/LanguageUtil;->a(Landroid/database/Cursor;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_3
    move-object v0, v2

    .line 863
    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method private static c(Ljava/lang/String;)C
    .locals 8

    .prologue
    const/16 v1, 0xc4

    const/16 v3, 0xd8

    const/16 v4, 0xc6

    const/4 v2, 0x0

    const/16 v0, 0xc5

    .line 548
    .line 549
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 551
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    .line 553
    const-string v7, "da"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "nb"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 554
    :cond_0
    if-eq v0, v5, :cond_1

    const/16 v1, 0xe5

    if-ne v1, v5, :cond_3

    :cond_1
    move v1, v0

    .line 577
    :cond_2
    :goto_0
    if-nez v1, :cond_10

    .line 578
    sget-object v0, Ljava/text/Normalizer$Form;->NFD:Ljava/text/Normalizer$Form;

    invoke-static {p0, v0}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v0

    .line 579
    if-nez v0, :cond_11

    .line 582
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_10

    .line 583
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 587
    :goto_2
    return v0

    .line 556
    :cond_3
    if-eq v4, v5, :cond_4

    const/16 v0, 0xe6

    if-eq v0, v5, :cond_4

    const/16 v0, 0xe4

    if-ne v0, v5, :cond_5

    :cond_4
    move v1, v4

    .line 557
    goto :goto_0

    .line 558
    :cond_5
    if-eq v3, v5, :cond_6

    const/16 v0, 0xf8

    if-eq v0, v5, :cond_6

    const/16 v0, 0xf6

    if-ne v0, v5, :cond_12

    :cond_6
    move v1, v3

    .line 559
    goto :goto_0

    .line 561
    :cond_7
    const-string v7, "fi"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_8

    const-string v7, "sv"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 562
    :cond_8
    if-eq v0, v5, :cond_9

    const/16 v3, 0xe5

    if-ne v3, v5, :cond_a

    :cond_9
    move v1, v0

    .line 563
    goto :goto_0

    .line 564
    :cond_a
    if-eq v1, v5, :cond_2

    const/16 v0, 0xe4

    if-eq v0, v5, :cond_2

    .line 566
    const/16 v0, 0xd6

    if-eq v0, v5, :cond_b

    const/16 v0, 0xf6

    if-ne v0, v5, :cond_12

    .line 567
    :cond_b
    const/16 v1, 0xd6

    goto :goto_0

    .line 570
    :cond_c
    if-eq v4, v5, :cond_d

    const/16 v0, 0xe6

    if-ne v0, v5, :cond_e

    .line 571
    :cond_d
    const/16 v1, 0x41

    goto :goto_0

    .line 572
    :cond_e
    if-eq v3, v5, :cond_f

    const/16 v0, 0xf8

    if-ne v0, v5, :cond_12

    .line 573
    :cond_f
    const/16 v1, 0x4f

    goto :goto_0

    :cond_10
    move v0, v1

    goto :goto_2

    :cond_11
    move-object p0, v0

    goto :goto_1

    :cond_12
    move v1, v2

    goto :goto_0
.end method

.method static synthetic c()Ljava/util/Properties;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil;->d:Ljava/util/Properties;

    return-object v0
.end method

.method private static d(Ljava/lang/String;)C
    .locals 2

    .prologue
    .line 591
    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    .line 594
    const-string v1, "\u3131"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_0

    const-string v1, "\u3132"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    .line 595
    const/16 v0, 0x3131

    .line 636
    :goto_0
    return v0

    .line 596
    :cond_0
    const-string v1, "\u3132"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_1

    const-string v1, "\u3134"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_1

    .line 597
    const/16 v0, 0x3132

    goto :goto_0

    .line 598
    :cond_1
    const-string v1, "\u3134"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_2

    const-string v1, "\u3137"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_2

    .line 599
    const/16 v0, 0x3134

    goto :goto_0

    .line 600
    :cond_2
    const-string v1, "\u3137"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_3

    const-string v1, "\u3138"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_3

    .line 601
    const/16 v0, 0x3137

    goto :goto_0

    .line 602
    :cond_3
    const-string v1, "\u3138"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_4

    const-string v1, "\u3139"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_4

    .line 603
    const/16 v0, 0x3138

    goto :goto_0

    .line 604
    :cond_4
    const-string v1, "\u3139"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_5

    const-string v1, "\u3141"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_5

    .line 605
    const/16 v0, 0x3139

    goto :goto_0

    .line 606
    :cond_5
    const-string v1, "\u3141"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_6

    const-string v1, "\u3142"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_6

    .line 607
    const/16 v0, 0x3141

    goto :goto_0

    .line 608
    :cond_6
    const-string v1, "\u3142"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_7

    const-string v1, "\u3143"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_7

    .line 609
    const/16 v0, 0x3142

    goto/16 :goto_0

    .line 610
    :cond_7
    const-string v1, "\u3143"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_8

    const-string v1, "\u3145"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_8

    .line 611
    const/16 v0, 0x3143

    goto/16 :goto_0

    .line 612
    :cond_8
    const-string v1, "\u3145"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_9

    const-string v1, "\u3146"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_9

    .line 613
    const/16 v0, 0x3145

    goto/16 :goto_0

    .line 614
    :cond_9
    const-string v1, "\u3146"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_a

    const-string v1, "\u3147"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_a

    .line 615
    const/16 v0, 0x3146

    goto/16 :goto_0

    .line 616
    :cond_a
    const-string v1, "\u3147"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_b

    const-string v1, "\u3148"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_b

    .line 617
    const/16 v0, 0x3147

    goto/16 :goto_0

    .line 618
    :cond_b
    const-string v1, "\u3148"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_c

    const-string v1, "\u3149"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_c

    .line 619
    const/16 v0, 0x3148

    goto/16 :goto_0

    .line 620
    :cond_c
    const-string v1, "\u3149"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_d

    const-string v1, "\u314a"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_d

    .line 621
    const/16 v0, 0x3149

    goto/16 :goto_0

    .line 622
    :cond_d
    const-string v1, "\u314a"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_e

    const-string v1, "\u314b"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_e

    .line 623
    const/16 v0, 0x314a

    goto/16 :goto_0

    .line 624
    :cond_e
    const-string v1, "\u314b"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_f

    const-string v1, "\u314c"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_f

    .line 625
    const/16 v0, 0x314b

    goto/16 :goto_0

    .line 626
    :cond_f
    const-string v1, "\u314c"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_10

    const-string v1, "\u314d"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_10

    .line 627
    const/16 v0, 0x314c

    goto/16 :goto_0

    .line 628
    :cond_10
    const-string v1, "\u314d"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_11

    const-string v1, "\u314e"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_11

    .line 629
    const/16 v0, 0x314d

    goto/16 :goto_0

    .line 630
    :cond_11
    const-string v1, "\u314e"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_12

    const-string v1, "\u314f"

    invoke-virtual {v0, v1, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_12

    .line 631
    const/16 v0, 0x314e

    goto/16 :goto_0

    .line 633
    :cond_12
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto/16 :goto_0
.end method

.method static synthetic d()Ljava/util/Timer;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil;->e:Ljava/util/Timer;

    return-object v0
.end method

.method private static e(Ljava/lang/String;)C
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 690
    .line 693
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 696
    :try_start_0
    invoke-static {v0}, Lcom/sec/pcw/util/LanguageUtil;->a(Landroid/content/Context;)Ljava/util/Properties;

    move-result-object v0

    .line 698
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 699
    invoke-virtual {v0, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 701
    if-nez v0, :cond_0

    .line 720
    :goto_0
    return v1

    .line 705
    :cond_0
    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 708
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 710
    const-string v2, "none0"

    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 711
    const/16 v1, 0x23

    goto :goto_0

    .line 713
    :cond_1
    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_1
    move v1, v0

    .line 720
    goto :goto_0

    .line 714
    :catch_0
    move-exception v0

    .line 715
    sget-object v2, Lcom/sec/pcw/util/LanguageUtil;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x6

    if-gt v2, v3, :cond_2

    .line 716
    const-string v2, "mfl_LanguageUtil.LanguageBlock"

    const-string v3, "::getCjkIndexLetter:"

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static synthetic e()Ljava/util/Timer;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil;->e:Ljava/util/Timer;

    return-object v0
.end method

.method private static f(Ljava/lang/String;)C
    .locals 3

    .prologue
    .line 733
    const/4 v0, 0x0

    .line 734
    sget-object v1, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-static {v1}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v1

    .line 736
    const-string v2, "\u30a2"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_1

    const-string v2, "\u30ab"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_1

    .line 737
    const/16 v0, 0x30a2

    .line 758
    :cond_0
    :goto_0
    return v0

    .line 738
    :cond_1
    const-string v2, "\u30ab"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_2

    const-string v2, "\u30b5"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2

    .line 739
    const/16 v0, 0x30ab

    goto :goto_0

    .line 740
    :cond_2
    const-string v2, "\u30b5"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_3

    const-string v2, "\u30bf"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_3

    .line 741
    const/16 v0, 0x30b5

    goto :goto_0

    .line 742
    :cond_3
    const-string v2, "\u30bf"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_4

    const-string v2, "\u30ca"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_4

    .line 743
    const/16 v0, 0x30bf

    goto :goto_0

    .line 744
    :cond_4
    const-string v2, "\u30ca"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_5

    const-string v2, "\u30cf"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_5

    .line 745
    const/16 v0, 0x30ca

    goto :goto_0

    .line 746
    :cond_5
    const-string v2, "\u30cf"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_6

    const-string v2, "\u30de"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_6

    .line 747
    const/16 v0, 0x30cf

    goto :goto_0

    .line 748
    :cond_6
    const-string v2, "\u30de"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_7

    const-string v2, "\u30e4"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_7

    .line 749
    const/16 v0, 0x30de

    goto :goto_0

    .line 750
    :cond_7
    const-string v2, "\u30e4"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_8

    const-string v2, "\u30e9"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_8

    .line 751
    const/16 v0, 0x30e4

    goto/16 :goto_0

    .line 752
    :cond_8
    const-string v2, "\u30e9"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_9

    const-string v2, "\u30ef"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_9

    .line 753
    const/16 v0, 0x30e9

    goto/16 :goto_0

    .line 754
    :cond_9
    const-string v2, "\u30ef"

    invoke-virtual {v1, v2, p0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_0

    .line 755
    const/16 v0, 0x30ef

    goto/16 :goto_0
.end method

.method static synthetic f()Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil;->f:Ljava/util/TimerTask;

    return-object v0
.end method

.method private static g()Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 254
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 256
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$h;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 257
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 259
    :cond_0
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$c;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 260
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->c:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 262
    :cond_1
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$d;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 263
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->d:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 265
    :cond_2
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$e;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 266
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->e:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 268
    :cond_3
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$b;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_4
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 269
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->f:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 271
    :cond_4
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$g;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_5
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 272
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->g:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 274
    :cond_5
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$a;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_6
    if-ge v1, v4, :cond_6

    aget-object v5, v3, v1

    .line 275
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->h:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 277
    :cond_6
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$f;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_7
    if-ge v1, v4, :cond_7

    aget-object v5, v3, v1

    .line 278
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->i:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 280
    :cond_7
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$j;->a()[Ljava/lang/String;

    move-result-object v1

    array-length v3, v1

    :goto_8
    if-ge v0, v3, :cond_8

    aget-object v4, v1, v0

    .line 281
    sget-object v5, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->j:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 284
    :cond_8
    return-object v2
.end method

.method private static h()Ljava/util/HashMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Character$UnicodeBlock;",
            "Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 291
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 293
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$h;->b()[Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 294
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->b:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 296
    :cond_0
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$c;->b()[Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 297
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->c:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 299
    :cond_1
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$d;->b()[Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 300
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->d:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 302
    :cond_2
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$e;->b()[Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 303
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->e:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 305
    :cond_3
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$b;->b()[Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_4
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 306
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->f:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 308
    :cond_4
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$g;->b()[Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_5
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 309
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->g:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 311
    :cond_5
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$a;->b()[Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_6
    if-ge v1, v4, :cond_6

    aget-object v5, v3, v1

    .line 312
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->h:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 314
    :cond_6
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$f;->b()[Ljava/lang/Character$UnicodeBlock;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_7
    if-ge v1, v4, :cond_7

    aget-object v5, v3, v1

    .line 315
    sget-object v6, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->i:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 317
    :cond_7
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil$j;->b()[Ljava/lang/Character$UnicodeBlock;

    move-result-object v1

    array-length v3, v1

    :goto_8
    if-ge v0, v3, :cond_8

    aget-object v4, v1, v0

    .line 318
    sget-object v5, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->j:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 320
    :cond_8
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HALFWIDTH_AND_FULLWIDTH_FORMS:Ljava/lang/Character$UnicodeBlock;

    sget-object v1, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->k:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    return-object v2
.end method

.method private static i()Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;
    .locals 2

    .prologue
    .line 353
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/pcw/util/LanguageUtil;->g:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;->a:Lcom/sec/pcw/util/LanguageUtil$LanguageBlock;

    :cond_0
    return-object v0
.end method
