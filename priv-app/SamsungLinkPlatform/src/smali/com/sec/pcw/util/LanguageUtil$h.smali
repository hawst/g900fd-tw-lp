.class final Lcom/sec/pcw/util/LanguageUtil$h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/util/LanguageUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "h"
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/Character$UnicodeBlock;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 90
    const/16 v0, 0x1e

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "cs"

    aput-object v1, v0, v3

    const-string v1, "da"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "de"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "en"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "es"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "et"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "fi"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "fr"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ga"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "hr"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "hu"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "in"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "is"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "it"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "lt"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "lv"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "ms"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "nb"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "nl"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "pl"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "pt"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "ro"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "sk"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "sl"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "sq"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "sr"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "sv"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "tr"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "uz"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "vi"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$h;->a:[Ljava/lang/String;

    .line 123
    new-array v0, v4, [Ljava/lang/Character$UnicodeBlock;

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->BASIC_LATIN:Ljava/lang/Character$UnicodeBlock;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$h;->b:[Ljava/lang/Character$UnicodeBlock;

    return-void
.end method

.method static synthetic a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$h;->a:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b()[Ljava/lang/Character$UnicodeBlock;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$h;->b:[Ljava/lang/Character$UnicodeBlock;

    return-object v0
.end method
