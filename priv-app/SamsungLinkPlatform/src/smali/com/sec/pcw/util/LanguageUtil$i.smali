.class final Lcom/sec/pcw/util/LanguageUtil$i;
.super Ljava/util/TimerTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/util/LanguageUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "i"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 639
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 639
    invoke-direct {p0}, Lcom/sec/pcw/util/LanguageUtil$i;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 643
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->a()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 644
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 645
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->b()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    .line 646
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/pcw/util/LanguageUtil;->a(Ljava/util/TimerTask;)Ljava/util/TimerTask;

    .line 647
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->c()Ljava/util/Properties;

    .line 648
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->d()Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 649
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->e()Ljava/util/Timer;

    .line 654
    :goto_0
    monitor-exit v1

    return-void

    .line 651
    :cond_0
    new-instance v0, Lcom/sec/pcw/util/LanguageUtil$i;

    invoke-direct {v0}, Lcom/sec/pcw/util/LanguageUtil$i;-><init>()V

    invoke-static {v0}, Lcom/sec/pcw/util/LanguageUtil;->a(Ljava/util/TimerTask;)Ljava/util/TimerTask;

    .line 652
    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->d()Ljava/util/Timer;

    move-result-object v0

    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->f()Ljava/util/TimerTask;

    move-result-object v4

    invoke-static {}, Lcom/sec/pcw/util/LanguageUtil;->b()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-virtual {v0, v4, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 654
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
