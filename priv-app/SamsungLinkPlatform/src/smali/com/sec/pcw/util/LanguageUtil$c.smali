.class final Lcom/sec/pcw/util/LanguageUtil$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/util/LanguageUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "c"
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/Character$UnicodeBlock;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 134
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "bg"

    aput-object v1, v0, v3

    const-string v1, "kk"

    aput-object v1, v0, v4

    const-string v1, "mk"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "ru"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "uk"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$c;->a:[Ljava/lang/String;

    .line 142
    new-array v0, v5, [Ljava/lang/Character$UnicodeBlock;

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->CYRILLIC:Ljava/lang/Character$UnicodeBlock;

    aput-object v1, v0, v3

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->CYRILLIC_SUPPLEMENTARY:Ljava/lang/Character$UnicodeBlock;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$c;->b:[Ljava/lang/Character$UnicodeBlock;

    return-void
.end method

.method static synthetic a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$c;->a:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b()[Ljava/lang/Character$UnicodeBlock;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$c;->b:[Ljava/lang/Character$UnicodeBlock;

    return-object v0
.end method
