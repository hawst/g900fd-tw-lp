.class final Lcom/sec/pcw/util/LanguageUtil$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/util/LanguageUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/Character$UnicodeBlock;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 212
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "ar"

    aput-object v1, v0, v2

    const-string v1, "fa"

    aput-object v1, v0, v3

    const-string v1, "ur"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$a;->a:[Ljava/lang/String;

    .line 218
    new-array v0, v5, [Ljava/lang/Character$UnicodeBlock;

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->ARABIC:Ljava/lang/Character$UnicodeBlock;

    aput-object v1, v0, v2

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->ARABIC_PRESENTATION_FORMS_A:Ljava/lang/Character$UnicodeBlock;

    aput-object v1, v0, v3

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->ARABIC_PRESENTATION_FORMS_B:Ljava/lang/Character$UnicodeBlock;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$a;->b:[Ljava/lang/Character$UnicodeBlock;

    return-void
.end method

.method static synthetic a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$a;->a:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b()[Ljava/lang/Character$UnicodeBlock;
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$a;->b:[Ljava/lang/Character$UnicodeBlock;

    return-object v0
.end method
