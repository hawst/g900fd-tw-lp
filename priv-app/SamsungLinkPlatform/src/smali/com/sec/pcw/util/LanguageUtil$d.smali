.class final Lcom/sec/pcw/util/LanguageUtil$d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/util/LanguageUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "d"
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/Character$UnicodeBlock;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 151
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "el"

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$d;->a:[Ljava/lang/String;

    .line 155
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Character$UnicodeBlock;

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->GREEK:Ljava/lang/Character$UnicodeBlock;

    aput-object v1, v0, v2

    sget-object v1, Ljava/lang/Character$UnicodeBlock;->GREEK_EXTENDED:Ljava/lang/Character$UnicodeBlock;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/pcw/util/LanguageUtil$d;->b:[Ljava/lang/Character$UnicodeBlock;

    return-void
.end method

.method static synthetic a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$d;->a:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b()[Ljava/lang/Character$UnicodeBlock;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/sec/pcw/util/LanguageUtil$d;->b:[Ljava/lang/Character$UnicodeBlock;

    return-object v0
.end method
