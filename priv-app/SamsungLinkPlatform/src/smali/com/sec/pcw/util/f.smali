.class public final Lcom/sec/pcw/util/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:I

.field public static b:I

.field public static c:I

.field public static d:I

.field public static e:I


# instance fields
.field private final f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput v0, Lcom/sec/pcw/util/f;->a:I

    .line 27
    const/4 v0, 0x1

    sput v0, Lcom/sec/pcw/util/f;->b:I

    .line 28
    const/4 v0, 0x2

    sput v0, Lcom/sec/pcw/util/f;->c:I

    .line 29
    const/4 v0, 0x3

    sput v0, Lcom/sec/pcw/util/f;->d:I

    .line 30
    const/4 v0, 0x4

    sput v0, Lcom/sec/pcw/util/f;->e:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 11

    .prologue
    const/16 v10, 0x5c

    const/16 v9, 0x27

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 38
    invoke-static {p1}, Lorg/apache/commons/lang3/StringUtils;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 39
    const-string v0, "\\|"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 40
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 41
    :goto_0
    array-length v2, v4

    if-ge v0, v2, :cond_5

    .line 42
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 43
    aget-object v2, v4, v0

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    move v2, v1

    .line 44
    :goto_1
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 45
    invoke-virtual {v6, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 46
    const/16 v8, 0x25

    if-eq v7, v8, :cond_0

    if-eq v7, v10, :cond_0

    const/16 v8, 0x5f

    if-ne v7, v8, :cond_2

    .line 47
    :cond_0
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 51
    :cond_1
    :goto_2
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 44
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 48
    :cond_2
    if-ne v7, v9, :cond_1

    if-eqz p2, :cond_1

    .line 49
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 53
    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_4

    .line 54
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/pcw/util/f;->f:[Ljava/lang/String;

    .line 113
    return-void
.end method


# virtual methods
.method public final a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/pcw/util/f;->f:[Ljava/lang/String;

    return-object v0
.end method
