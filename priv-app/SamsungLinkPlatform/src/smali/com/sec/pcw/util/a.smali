.class public final Lcom/sec/pcw/util/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 31
    const-string v0, "com.google"

    invoke-static {p0, v0}, Lcom/sec/pcw/util/a;->b(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 35
    const-string v0, "mfl_AccountUtil"

    const-string v1, "[hasGoogleAccount] - Google Account exist"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const/4 v0, 0x1

    .line 39
    :goto_0
    return v0

    .line 38
    :cond_0
    const-string v0, "mfl_AccountUtil"

    const-string v1, "[hasGoogleAccount] - No Google Account"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 86
    const-string v0, "com.google"

    invoke-static {p0, v0}, Lcom/sec/pcw/util/a;->b(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 88
    invoke-static {p0}, Lcom/sec/pcw/util/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 89
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 90
    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 91
    const-string v0, "mfl_AccountUtil"

    const-string v2, "Google Account exist"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :goto_1
    return v1

    .line 89
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 56
    const-string v0, "com.osp.app.signin"

    invoke-static {p0, v0}, Lcom/sec/pcw/util/a;->b(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    .line 60
    const-string v0, "mfl_AccountUtil"

    const-string v1, "[hasSamungAccount] - Samsung Account exist"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const/4 v0, 0x1

    .line 64
    :goto_0
    return v0

    .line 63
    :cond_0
    const-string v0, "mfl_AccountUtil"

    const-string v1, "[hasSamungAccount] - No Samsung Account"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 128
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 129
    invoke-virtual {v0, p1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    const-string v0, "com.google"

    invoke-static {p0, v0}, Lcom/sec/pcw/util/a;->b(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 78
    invoke-static {p0}, Lcom/sec/pcw/util/a;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 81
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 108
    const-string v0, "com.osp.app.signin"

    invoke-static {p0, v0}, Lcom/sec/pcw/util/a;->b(Landroid/content/Context;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 110
    invoke-static {p0}, Lcom/sec/pcw/util/a;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 113
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
