.class public final Lcom/sec/pcw/util/Common;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/util/Common$ServerType;
    }
.end annotation


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:Landroid/net/Uri;

.field public static final c:Landroid/net/Uri;

.field public static final d:Landroid/net/Uri;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field public static final g:Landroid/net/Uri;

.field public static final h:[Ljava/lang/String;

.field public static final i:[Ljava/lang/String;

.field public static final j:[Ljava/lang/String;

.field public static final k:[Ljava/lang/String;

.field public static final l:[Ljava/lang/String;

.field public static final m:[Ljava/lang/String;

.field public static final n:[Ljava/lang/String;

.field public static final o:[Ljava/lang/String;

.field public static final p:[Ljava/lang/String;

.field public static final q:[Ljava/lang/String;

.field public static final r:[Ljava/lang/String;

.field public static final s:[Ljava/lang/String;

.field public static final t:[Ljava/lang/String;

.field public static u:Z

.field private static v:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 43
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/util/Common;->v:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 180
    sget-object v0, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/pcw/util/Common;->a:Landroid/net/Uri;

    .line 183
    sget-object v0, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/pcw/util/Common;->b:Landroid/net/Uri;

    .line 186
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/pcw/util/Common;->c:Landroid/net/Uri;

    .line 189
    sget-object v0, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/pcw/util/Common;->d:Landroid/net/Uri;

    .line 192
    sget-object v0, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/pcw/util/Common;->e:Landroid/net/Uri;

    .line 195
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/pcw/util/Common;->f:Landroid/net/Uri;

    .line 198
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/sec/pcw/util/Common;->g:Landroid/net/Uri;

    .line 205
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "album_art"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/pcw/util/Common;->h:[Ljava/lang/String;

    .line 208
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "album"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "numsongs"

    aput-object v1, v0, v6

    const-string v1, "maxyear"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "album_art"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/pcw/util/Common;->i:[Ljava/lang/String;

    .line 217
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "duration"

    aput-object v1, v0, v4

    const-string v1, "_size"

    aput-object v1, v0, v5

    const-string v1, "_data"

    aput-object v1, v0, v6

    const-string v1, "date_added"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "track"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "album"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "is_music"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/pcw/util/Common;->j:[Ljava/lang/String;

    .line 236
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "artist"

    aput-object v1, v0, v4

    const-string v1, "number_of_albums"

    aput-object v1, v0, v5

    const-string v1, "number_of_tracks"

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/pcw/util/Common;->k:[Ljava/lang/String;

    .line 243
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/pcw/util/Common;->l:[Ljava/lang/String;

    .line 246
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/pcw/util/Common;->m:[Ljava/lang/String;

    .line 249
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "_display_name"

    aput-object v1, v0, v5

    const-string v1, "date_added"

    aput-object v1, v0, v6

    const-string v1, "date_modified"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "height"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "width"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/pcw/util/Common;->n:[Ljava/lang/String;

    .line 264
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "datetaken"

    aput-object v1, v0, v4

    const-string v1, "_data"

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/pcw/util/Common;->o:[Ljava/lang/String;

    .line 267
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "_display_name"

    aput-object v1, v0, v5

    const-string v1, "date_added"

    aput-object v1, v0, v6

    const-string v1, "date_modified"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "resolution"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/pcw/util/Common;->p:[Ljava/lang/String;

    .line 283
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ".smi"

    aput-object v1, v0, v3

    const-string v1, ".srt"

    aput-object v1, v0, v4

    const-string v1, ".sub"

    aput-object v1, v0, v5

    const-string v1, ".txt"

    aput-object v1, v0, v6

    const-string v1, ".ttxt"

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/pcw/util/Common;->q:[Ljava/lang/String;

    .line 290
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v1, v0, v3

    const/4 v1, 0x0

    aput-object v1, v0, v4

    const-string v1, ".idx"

    aput-object v1, v0, v5

    const/4 v1, 0x0

    aput-object v1, v0, v6

    const/4 v1, 0x0

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/pcw/util/Common;->r:[Ljava/lang/String;

    .line 296
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "SMI"

    aput-object v1, v0, v3

    const-string v1, "SRT"

    aput-object v1, v0, v4

    const-string v1, "SUB"

    aput-object v1, v0, v5

    const-string v1, "TXT"

    aput-object v1, v0, v6

    const-string v1, "TTXT"

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/pcw/util/Common;->s:[Ljava/lang/String;

    .line 308
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "datetaken"

    aput-object v1, v0, v4

    const-string v1, "_data"

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/pcw/util/Common;->t:[Ljava/lang/String;

    .line 740
    sput-boolean v3, Lcom/sec/pcw/util/Common;->u:Z

    .line 751
    sget-object v0, Lcom/sec/pcw/util/Common$ServerType;->b:Lcom/sec/pcw/util/Common$ServerType;

    invoke-static {}, Lcom/sec/pcw/util/Common;->a()Lcom/sec/pcw/util/Common$ServerType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/pcw/util/Common$ServerType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 752
    sput-boolean v4, Lcom/sec/pcw/util/Common;->u:Z

    .line 754
    :cond_0
    const-string v0, "SL_STAGING"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IS_STAGING : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/pcw/util/Common;->u:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    return-void
.end method

.method public static a()Lcom/sec/pcw/util/Common$ServerType;
    .locals 4

    .prologue
    .line 759
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/sdcard/SL_STAGING"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 760
    const-string v1, "SL_STAGING"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getServerType:: SL_STAGING_PATH = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 763
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 764
    const-string v1, "slpf_pref_20"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/mfluent/asp/ASPApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 765
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "baseUrl"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 766
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "fwkUrl"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 768
    sget-object v0, Lcom/sec/pcw/util/Common$ServerType;->b:Lcom/sec/pcw/util/Common$ServerType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 774
    :goto_0
    return-object v0

    .line 770
    :catch_0
    move-exception v0

    .line 771
    const-string v1, "SL_STAGING"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getServerType:: Exception occurred : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    :cond_0
    sget-object v0, Lcom/sec/pcw/util/Common$ServerType;->a:Lcom/sec/pcw/util/Common$ServerType;

    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 331
    packed-switch p0, :pswitch_data_0

    .line 380
    :pswitch_0
    const-string v0, "Sorting Error"

    const-string v1, "Not Define Sorting Index"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v0, "title COLLATE LOCALIZED ASC, _id ASC"

    :goto_0
    return-object v0

    .line 334
    :pswitch_1
    const-string v0, "title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 338
    :pswitch_2
    const-string v0, "title COLLATE LOCALIZED DESC, _id DESC"

    goto :goto_0

    .line 341
    :pswitch_3
    const-string v0, "artist COLLATE LOCALIZED ASC, title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 344
    :pswitch_4
    const-string v0, "artist COLLATE LOCALIZED DESC, title COLLATE LOCALIZED ASC, _id DESC"

    goto :goto_0

    .line 347
    :pswitch_5
    const-string v0, "album COLLATE LOCALIZED ASC, title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 350
    :pswitch_6
    const-string v0, "album COLLATE LOCALIZED DESC, title COLLATE LOCALIZED ASC, _id DESC"

    goto :goto_0

    .line 353
    :pswitch_7
    const-string v0, "year ASC, title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 356
    :pswitch_8
    const-string v0, "year DESC, title COLLATE LOCALIZED ASC, _id DESC"

    goto :goto_0

    .line 359
    :pswitch_9
    const-string v0, "date_added ASC, title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 362
    :pswitch_a
    const-string v0, "date_added DESC, title COLLATE LOCALIZED ASC, _id DESC"

    goto :goto_0

    .line 365
    :pswitch_b
    const-string v0, "duration ASC, title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 368
    :pswitch_c
    const-string v0, "duration DESC, title COLLATE LOCALIZED ASC, _id DESC"

    goto :goto_0

    .line 371
    :pswitch_d
    const-string v0, "title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 374
    :pswitch_e
    const-string v0, "title COLLATE LOCALIZED DESC, _id DESC"

    goto :goto_0

    .line 377
    :pswitch_f
    const-string v0, "title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 331
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_1
        :pswitch_2
        :pswitch_7
        :pswitch_8
        :pswitch_3
        :pswitch_4
        :pswitch_d
        :pswitch_e
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static a(Lorg/json/JSONObject;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 686
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 688
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v1

    .line 696
    :try_start_0
    sget-object v1, Lcom/sec/pcw/util/Common;->c:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 697
    if-eqz v9, :cond_0

    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToLast()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 698
    const-string v1, "_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 699
    const-string v3, "musicRevision"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 701
    :cond_0
    sget-object v1, Lcom/sec/pcw/util/Common;->f:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v8

    .line 702
    if-eqz v8, :cond_1

    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToLast()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 703
    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 704
    const-string v3, "photoRevision"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 706
    :cond_1
    sget-object v1, Lcom/sec/pcw/util/Common;->g:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v7

    .line 707
    if-eqz v7, :cond_2

    :try_start_3
    invoke-interface {v7}, Landroid/database/Cursor;->moveToLast()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 708
    const-string v1, "_id"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 709
    const-string v3, "videoRevision"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 711
    :cond_2
    sget-object v1, Lcom/mfluent/asp/common/datamodel/ASPMediaStore$Documents$Media;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v1

    .line 712
    if-eqz v1, :cond_3

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 713
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 714
    const-string v2, "docRevision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 724
    :cond_3
    if-eqz v9, :cond_4

    .line 725
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 727
    :cond_4
    if-eqz v8, :cond_5

    .line 728
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 730
    :cond_5
    if-eqz v7, :cond_6

    .line 731
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 733
    :cond_6
    if-eqz v1, :cond_7

    .line 734
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 737
    :cond_7
    :goto_0
    return-void

    .line 719
    :catch_0
    move-exception v0

    move-object v1, v6

    move-object v2, v6

    move-object v3, v6

    .line 720
    :goto_1
    :try_start_5
    sget-object v4, Lcom/sec/pcw/util/Common;->v:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v5, 0x6

    if-gt v4, v5, :cond_8

    .line 721
    const-string v4, "mfl_Common"

    const-string v5, "::putMediaRevision:"

    invoke-static {v4, v5, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 724
    :cond_8
    if-eqz v3, :cond_9

    .line 725
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 727
    :cond_9
    if-eqz v2, :cond_a

    .line 728
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 730
    :cond_a
    if-eqz v6, :cond_b

    .line 731
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 733
    :cond_b
    if-eqz v1, :cond_7

    .line 734
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 724
    :catchall_0
    move-exception v0

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    :goto_2
    if-eqz v9, :cond_c

    .line 725
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 727
    :cond_c
    if-eqz v8, :cond_d

    .line 728
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 730
    :cond_d
    if-eqz v7, :cond_e

    .line 731
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 733
    :cond_e
    if-eqz v6, :cond_f

    .line 734
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_f
    throw v0

    .line 724
    :catchall_1
    move-exception v0

    move-object v7, v6

    move-object v8, v6

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v7, v6

    goto :goto_2

    :catchall_3
    move-exception v0

    goto :goto_2

    :catchall_4
    move-exception v0

    move-object v6, v1

    goto :goto_2

    :catchall_5
    move-exception v0

    move-object v7, v6

    move-object v8, v2

    move-object v9, v3

    move-object v6, v1

    goto :goto_2

    .line 719
    :catch_1
    move-exception v0

    move-object v1, v6

    move-object v2, v6

    move-object v3, v9

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v1, v6

    move-object v2, v8

    move-object v3, v9

    goto :goto_1

    :catch_3
    move-exception v0

    move-object v1, v6

    move-object v2, v8

    move-object v3, v9

    move-object v6, v7

    goto :goto_1

    :catch_4
    move-exception v0

    move-object v6, v7

    move-object v2, v8

    move-object v3, v9

    goto :goto_1
.end method

.method public static b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 393
    sparse-switch p0, :sswitch_data_0

    .line 412
    const-string v0, "Sorting Error"

    const-string v1, "Not Define Sorting Index"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    const-string v0, "album COLLATE LOCALIZED ASC"

    :goto_0
    return-object v0

    .line 396
    :sswitch_0
    const-string v0, "album COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 400
    :sswitch_1
    const-string v0, "album COLLATE LOCALIZED DESC"

    goto :goto_0

    .line 403
    :sswitch_2
    const-string v0, "maxyear ASC"

    goto :goto_0

    .line 406
    :sswitch_3
    const-string v0, "maxyear DESC"

    goto :goto_0

    .line 409
    :sswitch_4
    const-string v0, "album COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 393
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_4
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x9 -> :sswitch_0
        0xa -> :sswitch_1
        0xf -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 425
    packed-switch p0, :pswitch_data_0

    .line 466
    :pswitch_0
    const-string v0, "Sorting Error"

    const-string v1, "Not Define Sorting Index"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v0, "title COLLATE LOCALIZED ASC, _id ASC"

    :goto_0
    return-object v0

    .line 427
    :pswitch_1
    const-string v0, "_size ASC, _id ASC"

    goto :goto_0

    .line 430
    :pswitch_2
    const-string v0, "_size DESC, _id DESC"

    goto :goto_0

    .line 433
    :pswitch_3
    const-string v0, "_data COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 436
    :pswitch_4
    const-string v0, "_data COLLATE LOCALIZED DESC, _id DESC"

    goto :goto_0

    .line 439
    :pswitch_5
    const-string v0, "title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 442
    :pswitch_6
    const-string v0, "title COLLATE LOCALIZED DESC, _id DESC"

    goto :goto_0

    .line 445
    :pswitch_7
    const-string v0, "datetaken ASC, _id ASC"

    goto :goto_0

    .line 448
    :pswitch_8
    const-string v0, "datetaken DESC, _id DESC"

    goto :goto_0

    .line 451
    :pswitch_9
    const-string v0, "date_added ASC, _id ASC"

    goto :goto_0

    .line 454
    :pswitch_a
    const-string v0, "date_added DESC, _id DESC"

    goto :goto_0

    .line 457
    :pswitch_b
    const-string v0, "date_modified ASC, _id ASC"

    goto :goto_0

    .line 460
    :pswitch_c
    const-string v0, "date_modified DESC, _id DESC"

    goto :goto_0

    .line 463
    :pswitch_d
    const-string v0, "title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 425
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static d(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 479
    packed-switch p0, :pswitch_data_0

    .line 520
    :pswitch_0
    const-string v0, "Sorting Error"

    const-string v1, "Not Define Sorting Index"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    const-string v0, "title COLLATE LOCALIZED ASC, _id ASC"

    :goto_0
    return-object v0

    .line 481
    :pswitch_1
    const-string v0, "_size ASC, _id ASC"

    goto :goto_0

    .line 484
    :pswitch_2
    const-string v0, "_size DESC, _id DESC"

    goto :goto_0

    .line 487
    :pswitch_3
    const-string v0, "_data COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 490
    :pswitch_4
    const-string v0, "_data COLLATE LOCALIZED DESC, _id DESC"

    goto :goto_0

    .line 493
    :pswitch_5
    const-string v0, "_display_name COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 496
    :pswitch_6
    const-string v0, "_display_name COLLATE LOCALIZED DESC, _id DESC"

    goto :goto_0

    .line 499
    :pswitch_7
    const-string v0, "date_added ASC, _id ASC"

    goto :goto_0

    .line 502
    :pswitch_8
    const-string v0, "date_added DESC, _id DESC"

    goto :goto_0

    .line 505
    :pswitch_9
    const-string v0, "date_modified ASC, _id ASC"

    goto :goto_0

    .line 508
    :pswitch_a
    const-string v0, "date_modified DESC, _id DESC"

    goto :goto_0

    .line 511
    :pswitch_b
    const-string v0, "duration ASC, _id ASC"

    goto :goto_0

    .line 514
    :pswitch_c
    const-string v0, "duration DESC, _id DESC"

    goto :goto_0

    .line 517
    :pswitch_d
    const-string v0, "title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 479
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_9
        :pswitch_a
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static e(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 526
    packed-switch p0, :pswitch_data_0

    .line 564
    :pswitch_0
    const-string v0, "Sorting Error"

    const-string v1, "Not Define Sorting Index"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const-string v0, "title COLLATE LOCALIZED ASC, _id ASC"

    :goto_0
    return-object v0

    .line 528
    :pswitch_1
    const-string v0, "_size ASC, _id ASC"

    goto :goto_0

    .line 531
    :pswitch_2
    const-string v0, "_size DESC, _id DESC"

    goto :goto_0

    .line 534
    :pswitch_3
    const-string v0, "_data COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 537
    :pswitch_4
    const-string v0, "_data COLLATE LOCALIZED DESC, _id DESC"

    goto :goto_0

    .line 540
    :pswitch_5
    const-string v0, "_display_name COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 543
    :pswitch_6
    const-string v0, "_display_name COLLATE LOCALIZED DESC, _id DESC"

    goto :goto_0

    .line 546
    :pswitch_7
    const-string v0, "date_added ASC, _id ASC"

    goto :goto_0

    .line 549
    :pswitch_8
    const-string v0, "date_added DESC, _id DESC"

    goto :goto_0

    .line 552
    :pswitch_9
    const-string v0, "date_modified ASC, _id ASC"

    goto :goto_0

    .line 555
    :pswitch_a
    const-string v0, "date_modified DESC, _id DESC"

    goto :goto_0

    .line 558
    :pswitch_b
    const-string v0, "title COLLATE LOCALIZED ASC, _id ASC"

    goto :goto_0

    .line 561
    :pswitch_c
    sget-object v0, Lcom/mfluent/asp/c;->a:Ljava/lang/String;

    goto :goto_0

    .line 526
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_7
        :pswitch_8
        :pswitch_c
    .end packed-switch
.end method

.method public static f(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 577
    sparse-switch p0, :sswitch_data_0

    .line 590
    const-string v0, "Sorting Error"

    const-string v1, "Not Define Sorting Index"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    const-string v0, "artist COLLATE LOCALIZED ASC"

    :goto_0
    return-object v0

    .line 580
    :sswitch_0
    const-string v0, "artist COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 584
    :sswitch_1
    const-string v0, "artist COLLATE LOCALIZED DESC"

    goto :goto_0

    .line 587
    :sswitch_2
    const-string v0, "artist COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 577
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x5 -> :sswitch_0
        0x6 -> :sswitch_1
        0xf -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch
.end method

.method public static g(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 603
    sparse-switch p0, :sswitch_data_0

    .line 616
    const-string v0, "Sorting Error"

    const-string v1, "Not Define Sorting Index"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    const-string v0, "name COLLATE LOCALIZED ASC"

    :goto_0
    return-object v0

    .line 606
    :sswitch_0
    const-string v0, "name COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 610
    :sswitch_1
    const-string v0, "name COLLATE LOCALIZED DESC"

    goto :goto_0

    .line 613
    :sswitch_2
    const-string v0, "name COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 603
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x7 -> :sswitch_0
        0x8 -> :sswitch_1
        0xf -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch
.end method
