.class final Lcom/sec/pcw/service/push/spp/b$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/service/push/spp/b;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/service/push/spp/b;


# direct methods
.method constructor <init>(Lcom/sec/pcw/service/push/spp/b;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/pcw/service/push/spp/b$1;->a:Lcom/sec/pcw/service/push/spp/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b$1;->a:Lcom/sec/pcw/service/push/spp/b;

    invoke-static {v0}, Lcom/sec/pcw/service/push/spp/b;->a(Lcom/sec/pcw/service/push/spp/b;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    .line 85
    const-string v1, "mfl_SppAPI"

    const-string v2, "startService requested"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    if-eqz v0, :cond_0

    .line 92
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 93
    iget-object v1, p0, Lcom/sec/pcw/service/push/spp/b$1;->a:Lcom/sec/pcw/service/push/spp/b;

    invoke-static {v1}, Lcom/sec/pcw/service/push/spp/b;->a(Lcom/sec/pcw/service/push/spp/b;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/pcw/service/push/spp/b$1;->a:Lcom/sec/pcw/service/push/spp/b;

    invoke-static {v2}, Lcom/sec/pcw/service/push/spp/b;->b(Lcom/sec/pcw/service/push/spp/b;)Landroid/content/ServiceConnection;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 95
    const-string v1, "mfl_SppAPI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bindService(mPushClientConnection) requested. bResult="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_0
    return-void
.end method
