.class public final Lcom/sec/pcw/service/b/h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/service/b/h$a;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:Landroid/content/ContentResolver;

.field private c:I

.field private d:I

.field private final e:Z

.field private f:Lcom/sec/pcw/util/d;

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/service/b/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/service/b/h;->b:Landroid/content/ContentResolver;

    .line 58
    iput v1, p0, Lcom/sec/pcw/service/b/h;->c:I

    .line 59
    iput v1, p0, Lcom/sec/pcw/service/b/h;->d:I

    .line 60
    iput-boolean v1, p0, Lcom/sec/pcw/service/b/h;->e:Z

    .line 68
    return-void
.end method

.method private a(I)I
    .locals 3

    .prologue
    .line 168
    const/4 v0, 0x0

    .line 170
    iget-object v1, p0, Lcom/sec/pcw/service/b/h;->f:Lcom/sec/pcw/util/d;

    invoke-virtual {v1}, Lcom/sec/pcw/util/d;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 171
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    .line 173
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 175
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 179
    :cond_1
    return v1
.end method

.method private a(Lorg/json/JSONArray;ZZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 381
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 383
    :try_start_0
    const-string v0, "genres"

    invoke-virtual {v1, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 384
    const-string v0, "totalCount"

    iget v2, p0, Lcom/sec/pcw/service/b/h;->c:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 385
    const-string v0, "totalSongCount"

    iget v2, p0, Lcom/sec/pcw/service/b/h;->d:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 386
    const-string v0, "isFirstpage"

    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 387
    const-string v0, "isLastpage"

    invoke-virtual {v1, v0, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 388
    const-string v0, "mediaScan"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 389
    invoke-static {v1}, Lcom/sec/pcw/util/Common;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 390
    :catch_0
    move-exception v0

    .line 391
    sget-object v2, Lcom/sec/pcw/service/b/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 392
    const-string v2, "mfl_APIGetGenreListById"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::toJSONStringGenreItemList:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lcom/sec/pcw/service/b/h$a;I)Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 356
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 358
    :try_start_0
    const-string v0, "index"

    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 359
    const-string v0, "id"

    iget v2, p1, Lcom/sec/pcw/service/b/h$a;->a:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 360
    const-string v0, "name"

    iget-object v2, p1, Lcom/sec/pcw/service/b/h$a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 361
    const-string v0, "numOfSongs"

    iget v2, p1, Lcom/sec/pcw/service/b/h$a;->c:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 362
    const-string v0, "numOfAlbums"

    iget v2, p1, Lcom/sec/pcw/service/b/h$a;->d:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 363
    const-string v0, "thumbnailUri"

    iget v2, p1, Lcom/sec/pcw/service/b/h$a;->a:I

    invoke-direct {p0, v2}, Lcom/sec/pcw/service/b/h;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/dws/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    :cond_0
    :goto_0
    return-object v1

    .line 364
    :catch_0
    move-exception v0

    .line 365
    sget-object v2, Lcom/sec/pcw/service/b/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 366
    const-string v2, "mfl_APIGetGenreListById"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::buildGenre:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)[Lcom/sec/pcw/service/b/h$a;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 223
    .line 224
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 225
    new-instance v4, Ljava/util/ArrayList;

    const/16 v0, 0x1e

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 228
    if-nez p2, :cond_0

    .line 229
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/service/b/h;->b:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/pcw/util/Common;->e:Landroid/net/Uri;

    sget-object v2, Lcom/sec/pcw/util/Common;->m:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 247
    :goto_0
    if-eqz v1, :cond_7

    .line 248
    :try_start_1
    invoke-static {v1, p1}, Lcom/sec/pcw/util/LanguageUtil;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 249
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v3

    .line 251
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v6, v0, [Lcom/sec/pcw/service/b/h$a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move v2, v7

    .line 253
    :goto_1
    :try_start_2
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 254
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 255
    new-instance v0, Lcom/sec/pcw/service/b/h$a;

    const/4 v4, 0x0

    invoke-direct {v0, v4}, Lcom/sec/pcw/service/b/h$a;-><init>(B)V

    .line 256
    const-string v4, "_id"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v0, Lcom/sec/pcw/service/b/h$a;->a:I

    .line 257
    const-string v4, "name"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/sec/pcw/service/b/h$a;->b:Ljava/lang/String;

    .line 258
    aput-object v0, v6, v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 253
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 231
    :cond_0
    :try_start_3
    new-instance v0, Lcom/sec/pcw/util/f;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lcom/sec/pcw/util/f;-><init>(Ljava/lang/String;Z)V

    .line 232
    invoke-virtual {v0}, Lcom/sec/pcw/util/f;->a()[Ljava/lang/String;

    move-result-object v1

    move v0, v7

    .line 233
    :goto_2
    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 234
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 235
    const-string v2, " OR "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    :cond_1
    const-string v2, "name LIKE ? ESCAPE \'\\\'"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "%"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, v1, v0

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "%"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 240
    :cond_2
    iget-object v0, p0, Lcom/sec/pcw/service/b/h;->b:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/pcw/util/Common;->e:Landroid/net/Uri;

    sget-object v2, Lcom/sec/pcw/util/Common;->m:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    goto/16 :goto_0

    :cond_3
    move-object v0, v6

    .line 266
    :goto_3
    if-eqz v1, :cond_4

    .line 267
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 270
    :cond_4
    :goto_4
    return-object v0

    .line 261
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v2, v6

    move-object v0, v6

    .line 262
    :goto_5
    :try_start_4
    sget-object v3, Lcom/sec/pcw/service/b/h;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_5

    .line 263
    const-string v3, "mfl_APIGetGenreListById"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::getAllGenreList:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 266
    :cond_5
    if-eqz v2, :cond_4

    .line 267
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 266
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v6, :cond_6

    .line 267
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 266
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_6

    :catchall_2
    move-exception v0

    move-object v6, v2

    goto :goto_6

    .line 261
    :catch_1
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_5

    :catch_2
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_5

    :cond_7
    move-object v0, v6

    goto :goto_3
.end method

.method private b(I)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 190
    .line 191
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "album_id"

    aput-object v0, v2, v6

    .line 193
    :try_start_0
    const-string v0, "external"

    int-to-long v4, p1

    invoke-static {v0, v4, v5}, Landroid/provider/MediaStore$Audio$Genres$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    .line 194
    iget-object v0, p0, Lcom/sec/pcw/service/b/h;->b:Landroid/content/ContentResolver;

    const-string v3, "is_music = 1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 195
    if-eqz v1, :cond_3

    .line 196
    :try_start_1
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 197
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 198
    const-string v2, "album_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 207
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 208
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 200
    :cond_1
    :try_start_2
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    .line 207
    if-eqz v1, :cond_2

    .line 208
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_2
    return v0

    .line 202
    :cond_3
    :try_start_3
    const-string v0, "mfl_APIGetGenreListById"

    const-string v2, "getNumOfAlbumsForGenre - cursor is null"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 203
    if-eqz v1, :cond_4

    .line 208
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    move v0, v6

    goto :goto_2

    .line 207
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_1
.end method

.method private c(I)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 290
    const-string v6, ""

    .line 295
    :try_start_0
    const-string v0, "external"

    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Landroid/provider/MediaStore$Audio$Genres$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    .line 296
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "album_id"

    aput-object v3, v2, v0

    .line 297
    const/16 v0, 0x18

    invoke-static {v0}, Lcom/sec/pcw/util/Common;->a(I)Ljava/lang/String;

    move-result-object v5

    .line 298
    iget-object v0, p0, Lcom/sec/pcw/service/b/h;->b:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 299
    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_3

    .line 308
    :cond_0
    if-eqz v1, :cond_1

    .line 309
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v6

    :cond_2
    :goto_0
    return-object v0

    .line 302
    :cond_3
    :try_start_2
    const-string v0, "album_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 304
    invoke-direct {p0, v0}, Lcom/sec/pcw/service/b/h;->d(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 308
    if-eqz v1, :cond_2

    .line 309
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 308
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_1
    if-eqz v1, :cond_4

    .line 309
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 308
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private d(I)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 315
    iget-object v0, p0, Lcom/sec/pcw/service/b/h;->g:Ljava/util/Map;

    if-nez v0, :cond_4

    .line 316
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "album_art"

    aput-object v1, v2, v0

    .line 317
    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 320
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/service/b/h;->b:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 321
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 322
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/sec/pcw/service/b/h;->g:Ljava/util/Map;

    .line 323
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 324
    const-string v2, "album_art"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 325
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_3

    .line 326
    iget-object v3, p0, Lcom/sec/pcw/service/b/h;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 334
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 335
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 330
    :cond_1
    :try_start_2
    const-string v0, "mfl_APIGetGenreListById"

    const-string v2, "cacheSongThumbList - cursor is null"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 334
    if-eqz v1, :cond_2

    .line 335
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 343
    :cond_2
    :goto_2
    return-object v0

    .line 334
    :cond_3
    if-eqz v1, :cond_4

    .line 335
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 339
    :cond_4
    iget-object v0, p0, Lcom/sec/pcw/service/b/h;->g:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 340
    if-nez v0, :cond_2

    .line 341
    const-string v0, ""

    goto :goto_2

    .line 334
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method


# virtual methods
.method public final a(IIILjava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 92
    new-instance v0, Lcom/sec/pcw/util/d;

    invoke-direct {v0}, Lcom/sec/pcw/util/d;-><init>()V

    iput-object v0, p0, Lcom/sec/pcw/service/b/h;->f:Lcom/sec/pcw/util/d;

    .line 94
    invoke-static {p3}, Lcom/sec/pcw/util/Common;->g(I)Ljava/lang/String;

    move-result-object v0

    .line 95
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 96
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 98
    invoke-direct {p0, v0, p4}, Lcom/sec/pcw/service/b/h;->a(Ljava/lang/String;Ljava/lang/String;)[Lcom/sec/pcw/service/b/h$a;

    move-result-object v2

    .line 99
    if-nez v2, :cond_0

    .line 100
    invoke-direct {p0, v6, v3, v3}, Lcom/sec/pcw/service/b/h;->a(Lorg/json/JSONArray;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 157
    :goto_0
    return-object v0

    :cond_0
    move v0, v1

    .line 103
    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_2

    .line 104
    aget-object v4, v2, v0

    iget v4, v4, Lcom/sec/pcw/service/b/h$a;->a:I

    invoke-direct {p0, v4}, Lcom/sec/pcw/service/b/h;->a(I)I

    move-result v4

    .line 105
    iget v7, p0, Lcom/sec/pcw/service/b/h;->d:I

    add-int/2addr v7, v4

    iput v7, p0, Lcom/sec/pcw/service/b/h;->d:I

    .line 107
    aget-object v7, v2, v0

    iget v7, v7, Lcom/sec/pcw/service/b/h$a;->a:I

    invoke-direct {p0, v7}, Lcom/sec/pcw/service/b/h;->b(I)I

    move-result v7

    .line 108
    if-eqz v4, :cond_1

    .line 109
    aget-object v8, v2, v0

    iput v4, v8, Lcom/sec/pcw/service/b/h$a;->c:I

    .line 110
    aget-object v4, v2, v0

    iput v7, v4, Lcom/sec/pcw/service/b/h$a;->d:I

    .line 111
    aget-object v4, v2, v0

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 115
    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/pcw/service/b/h;->c:I

    .line 117
    const/4 v4, -0x1

    move v2, v1

    .line 119
    :goto_2
    iget v0, p0, Lcom/sec/pcw/service/b/h;->c:I

    if-ge v2, v0, :cond_e

    .line 122
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/service/b/h$a;

    iget v0, v0, Lcom/sec/pcw/service/b/h$a;->a:I

    if-ne v0, p1, :cond_3

    .line 128
    :goto_3
    if-lez p2, :cond_7

    .line 129
    if-ltz v2, :cond_5

    .line 130
    add-int/lit8 v2, v2, 0x1

    .line 131
    add-int v0, v2, p2

    iget v4, p0, Lcom/sec/pcw/service/b/h;->c:I

    if-ge v0, v4, :cond_4

    add-int v0, v2, p2

    :goto_4
    move p2, v0

    :goto_5
    move v4, v2

    .line 143
    :goto_6
    if-ge v4, p2, :cond_b

    .line 144
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/service/b/h$a;

    invoke-direct {p0, v0, v4}, Lcom/sec/pcw/service/b/h;->a(Lcom/sec/pcw/service/b/h$a;I)Lorg/json/JSONObject;

    move-result-object v0

    .line 145
    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 143
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_6

    .line 121
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 131
    :cond_4
    iget v0, p0, Lcom/sec/pcw/service/b/h;->c:I

    goto :goto_4

    .line 133
    :cond_5
    iget v0, p0, Lcom/sec/pcw/service/b/h;->c:I

    if-ge p2, v0, :cond_6

    :goto_7
    move v2, v1

    goto :goto_5

    :cond_6
    iget p2, p0, Lcom/sec/pcw/service/b/h;->c:I

    goto :goto_7

    .line 136
    :cond_7
    if-ltz v2, :cond_9

    .line 137
    add-int v0, v2, p2

    if-lez v0, :cond_8

    add-int v0, v2, p2

    :goto_8
    move v9, v2

    move v2, v0

    move v0, v9

    :goto_9
    move p2, v0

    .line 140
    goto :goto_5

    :cond_8
    move v0, v1

    .line 137
    goto :goto_8

    .line 140
    :cond_9
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v2, p0, Lcom/sec/pcw/service/b/h;->c:I

    if-ge v0, v2, :cond_a

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    move v2, v1

    goto :goto_9

    :cond_a
    iget v0, p0, Lcom/sec/pcw/service/b/h;->c:I

    move v2, v1

    goto :goto_9

    .line 149
    :cond_b
    if-nez v2, :cond_d

    move v0, v3

    .line 153
    :goto_a
    iget v2, p0, Lcom/sec/pcw/service/b/h;->c:I

    if-ne p2, v2, :cond_c

    .line 157
    :goto_b
    invoke-direct {p0, v6, v0, v3}, Lcom/sec/pcw/service/b/h;->a(Lorg/json/JSONArray;ZZ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_c
    move v3, v1

    goto :goto_b

    :cond_d
    move v0, v1

    goto :goto_a

    :cond_e
    move v2, v4

    goto :goto_3
.end method
