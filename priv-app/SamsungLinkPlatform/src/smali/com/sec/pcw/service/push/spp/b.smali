.class public final Lcom/sec/pcw/service/push/spp/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static b:Lcom/sec/b/a/a;


# instance fields
.field private c:Landroid/content/Context;

.field private final d:Landroid/os/ConditionVariable;

.field private final e:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/service/push/spp/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 56
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/pcw/service/push/spp/b;->b:Lcom/sec/b/a/a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->c:Landroid/content/Context;

    .line 199
    new-instance v0, Lcom/sec/pcw/service/push/spp/b$3;

    invoke-direct {v0, p0}, Lcom/sec/pcw/service/push/spp/b$3;-><init>(Lcom/sec/pcw/service/push/spp/b;)V

    iput-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->e:Landroid/content/ServiceConnection;

    .line 66
    if-nez p1, :cond_0

    .line 67
    new-instance v0, Ljava/lang/ExceptionInInitializerError;

    new-instance v1, Ljava/lang/String;

    const-string v2, "context is null"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/ExceptionInInitializerError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->c:Landroid/content/Context;

    .line 71
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->d:Landroid/os/ConditionVariable;

    .line 76
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/pcw/service/push/spp/b$1;

    invoke-direct {v1, p0}, Lcom/sec/pcw/service/push/spp/b$1;-><init>(Lcom/sec/pcw/service/push/spp/b;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 99
    return-void
.end method

.method static synthetic a(Lcom/sec/pcw/service/push/spp/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->c:Landroid/content/Context;

    return-object v0
.end method

.method private static a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 165
    const-string v0, "mfl_SppAPI"

    const-string v1, "[addIntentFlag]"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    if-eqz p0, :cond_0

    .line 168
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 170
    const/16 v1, 0xc

    if-gt v1, v0, :cond_0

    .line 185
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 189
    const-string v0, "mfl_SppAPI"

    const-string v1, "add Intent.FLAG_INCLUDE_STOPPED_PACKAGES"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :cond_0
    return-object p0
.end method

.method static synthetic a()Lcom/sec/b/a/a;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/pcw/service/push/spp/b;->b:Lcom/sec/b/a/a;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/b/a/a;)Lcom/sec/b/a/a;
    .locals 0

    .prologue
    .line 48
    sput-object p0, Lcom/sec/pcw/service/push/spp/b;->b:Lcom/sec/b/a/a;

    return-object p0
.end method

.method static synthetic b(Lcom/sec/pcw/service/push/spp/b;)Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->e:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/pcw/service/push/spp/b;)Landroid/os/ConditionVariable;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->d:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/pcw/service/push/spp/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->c:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 272
    const-string v1, "mfl_SppAPI"

    const-string v2, "[registration]"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    sget-object v1, Lcom/sec/pcw/service/push/spp/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_0

    .line 274
    const-string v1, "mfl_SppAPI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::registration:appId : ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const-string v1, "mfl_SppAPI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::registration:userData : ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_0
    sget-object v1, Lcom/sec/pcw/service/push/spp/b;->b:Lcom/sec/b/a/a;

    if-eqz v1, :cond_2

    .line 284
    :try_start_0
    sget-object v1, Lcom/sec/pcw/service/push/spp/b;->b:Lcom/sec/b/a/a;

    invoke-interface {v1, p1, v0}, Lcom/sec/b/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const-string v0, "mfl_SppAPI"

    const-string v1, "[Method1] mService.registration requested"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 315
    :cond_1
    :goto_0
    return-void

    .line 287
    :catch_0
    move-exception v0

    .line 288
    sget-object v1, Lcom/sec/pcw/service/push/spp/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_1

    .line 289
    const-string v1, "mfl_SppAPI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::registration: failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 293
    :cond_2
    iget-object v1, p0, Lcom/sec/pcw/service/push/spp/b;->c:Landroid/content/Context;

    if-eqz v1, :cond_3

    .line 299
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.action.SPP_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 300
    const-string v2, "reqType"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 301
    const-string v2, "appId"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 302
    const-string v2, "userdata"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 303
    invoke-static {v1}, Lcom/sec/pcw/service/push/spp/b;->a(Landroid/content/Intent;)Landroid/content/Intent;

    .line 305
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 307
    const-string v0, "mfl_SppAPI"

    const-string v1, "[Method2] broadcast intent"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 313
    :cond_3
    const-string v0, "mfl_SppAPI"

    const-string v1, "mService.registration() failed"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 325
    const-string v0, "mfl_SppAPI"

    const-string v1, "[deregistration]"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    sget-object v0, Lcom/sec/pcw/service/push/spp/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v3, :cond_0

    .line 327
    const-string v0, "mfl_SppAPI"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::deregistration:appId : ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    :cond_0
    sget-object v0, Lcom/sec/pcw/service/push/spp/b;->b:Lcom/sec/b/a/a;

    if-eqz v0, :cond_1

    .line 336
    :try_start_0
    sget-object v0, Lcom/sec/pcw/service/push/spp/b;->b:Lcom/sec/b/a/a;

    invoke-interface {v0, p1}, Lcom/sec/b/a/a;->a(Ljava/lang/String;)V

    .line 338
    const-string v0, "mfl_SppAPI"

    const-string v1, "[Method1] mService.deregistration requested"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 362
    :goto_0
    return-void

    .line 339
    :catch_0
    move-exception v0

    .line 340
    const-string v1, "mfl_SppAPI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mService.deregistration() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->c:Landroid/content/Context;

    if-eqz v0, :cond_2

    .line 347
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.action.SPP_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 348
    const-string v1, "reqType"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 349
    const-string v1, "appId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    invoke-static {v0}, Lcom/sec/pcw/service/push/spp/b;->a(Landroid/content/Intent;)Landroid/content/Intent;

    .line 352
    iget-object v1, p0, Lcom/sec/pcw/service/push/spp/b;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 354
    const-string v0, "mfl_SppAPI"

    const-string v1, "[Method2] broadcast intent"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 360
    :cond_2
    const-string v0, "mfl_SppAPI"

    const-string v1, "mService.deregistration() failed"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final close()V
    .locals 3

    .prologue
    .line 129
    const-string v0, "mfl_SppAPI"

    const-string v1, "[close]"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    sget-object v0, Lcom/sec/pcw/service/push/spp/b;->b:Lcom/sec/b/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b;->c:Landroid/content/Context;

    .line 137
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/pcw/service/push/spp/b$2;

    invoke-direct {v2, p0, v0}, Lcom/sec/pcw/service/push/spp/b$2;-><init>(Lcom/sec/pcw/service/push/spp/b;Landroid/content/Context;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 157
    :cond_0
    return-void
.end method

.method public final finalize()V
    .locals 4

    .prologue
    .line 113
    const-string v0, "mfl_SppAPI"

    const-string v1, "[finalize]"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    invoke-virtual {p0}, Lcom/sec/pcw/service/push/spp/b;->close()V

    .line 122
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    const-string v1, "mfl_SppAPI"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "finalize() failed - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
