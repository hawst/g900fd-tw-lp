.class final Lcom/sec/pcw/service/push/spp/b$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/service/push/spp/b;->close()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/sec/pcw/service/push/spp/b;


# direct methods
.method constructor <init>(Lcom/sec/pcw/service/push/spp/b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/pcw/service/push/spp/b$2;->b:Lcom/sec/pcw/service/push/spp/b;

    iput-object p2, p0, Lcom/sec/pcw/service/push/spp/b$2;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 141
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/pcw/service/push/spp/b$2;->b:Lcom/sec/pcw/service/push/spp/b;

    invoke-static {v1}, Lcom/sec/pcw/service/push/spp/b;->c(Lcom/sec/pcw/service/push/spp/b;)Landroid/os/ConditionVariable;

    move-result-object v1

    const-wide/32 v2, 0xea60

    invoke-virtual {v1, v2, v3}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v1

    if-ne v0, v1, :cond_0

    .line 143
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/pcw/service/push/spp/b;->a(Lcom/sec/b/a/a;)Lcom/sec/b/a/a;

    .line 144
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b$2;->b:Lcom/sec/pcw/service/push/spp/b;

    invoke-static {v0}, Lcom/sec/pcw/service/push/spp/b;->d(Lcom/sec/pcw/service/push/spp/b;)Landroid/content/Context;

    .line 146
    const-string v0, "mfl_SppAPI"

    const-string v1, "unbindService(mPushClientConnection) requested."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b$2;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/pcw/service/push/spp/b$2;->b:Lcom/sec/pcw/service/push/spp/b;

    invoke-static {v1}, Lcom/sec/pcw/service/push/spp/b;->b(Lcom/sec/pcw/service/push/spp/b;)Landroid/content/ServiceConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 150
    const-string v0, "mfl_SppAPI"

    const-string v1, "unbindService(mPushClientConnection) finished."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :goto_0
    return-void

    .line 152
    :cond_0
    const-string v0, "mfl_SppAPI"

    const-string v1, "unbindService(mPushClientConnection) failed. - Service not registered"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
