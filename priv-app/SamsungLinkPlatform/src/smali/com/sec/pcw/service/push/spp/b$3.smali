.class final Lcom/sec/pcw/service/push/spp/b$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/service/push/spp/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/service/push/spp/b;


# direct methods
.method constructor <init>(Lcom/sec/pcw/service/push/spp/b;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/pcw/service/push/spp/b$3;->a:Lcom/sec/pcw/service/push/spp/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 203
    invoke-static {p2}, Lcom/sec/b/a/a$a;->a(Landroid/os/IBinder;)Lcom/sec/b/a/a;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/pcw/service/push/spp/b;->a(Lcom/sec/b/a/a;)Lcom/sec/b/a/a;

    .line 205
    invoke-static {}, Lcom/sec/pcw/service/push/spp/b;->a()Lcom/sec/b/a/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 206
    const-string v0, "mfl_SppAPI"

    const-string v1, "[ServiceConnection] ServiceConnection - success"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b$3;->a:Lcom/sec/pcw/service/push/spp/b;

    invoke-static {v0}, Lcom/sec/pcw/service/push/spp/b;->c(Lcom/sec/pcw/service/push/spp/b;)Landroid/os/ConditionVariable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b$3;->a:Lcom/sec/pcw/service/push/spp/b;

    invoke-static {v0}, Lcom/sec/pcw/service/push/spp/b;->c(Lcom/sec/pcw/service/push/spp/b;)Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    const-string v0, "mfl_SppAPI"

    const-string v1, "[ServiceConnection] ServiceConnection - failed"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 218
    const-string v0, "mfl_SppAPI"

    const-string v1, "[ServiceConnection] onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/pcw/service/push/spp/b;->a(Lcom/sec/b/a/a;)Lcom/sec/b/a/a;

    .line 222
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b$3;->a:Lcom/sec/pcw/service/push/spp/b;

    invoke-static {v0}, Lcom/sec/pcw/service/push/spp/b;->c(Lcom/sec/pcw/service/push/spp/b;)Landroid/os/ConditionVariable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/b$3;->a:Lcom/sec/pcw/service/push/spp/b;

    invoke-static {v0}, Lcom/sec/pcw/service/push/spp/b;->c(Lcom/sec/pcw/service/push/spp/b;)Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 226
    :cond_0
    return-void
.end method
