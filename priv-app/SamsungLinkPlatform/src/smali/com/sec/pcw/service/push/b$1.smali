.class final Lcom/sec/pcw/service/push/b$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/service/push/b;->a(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/sec/pcw/service/push/b;


# direct methods
.method constructor <init>(Lcom/sec/pcw/service/push/b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/sec/pcw/service/push/b$1;->c:Lcom/sec/pcw/service/push/b;

    iput-object p2, p0, Lcom/sec/pcw/service/push/b$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/pcw/service/push/b$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 281
    iget-object v1, p0, Lcom/sec/pcw/service/push/b$1;->c:Lcom/sec/pcw/service/push/b;

    invoke-static {v1}, Lcom/sec/pcw/service/push/b;->a(Lcom/sec/pcw/service/push/b;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/service/push/util/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/pcw/service/push/b$1;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 283
    new-instance v4, Lcom/sec/pcw/service/push/a;

    iget-object v1, p0, Lcom/sec/pcw/service/push/b$1;->c:Lcom/sec/pcw/service/push/b;

    invoke-static {v1}, Lcom/sec/pcw/service/push/b;->a(Lcom/sec/pcw/service/push/b;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/sec/pcw/service/push/a;-><init>(Landroid/content/Context;)V

    .line 286
    new-instance v1, Lcom/sec/pcw/service/push/c;

    iget-object v3, p0, Lcom/sec/pcw/service/push/b$1;->c:Lcom/sec/pcw/service/push/b;

    invoke-static {v3}, Lcom/sec/pcw/service/push/b;->a(Lcom/sec/pcw/service/push/b;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/sec/pcw/service/push/c;-><init>(Landroid/content/Context;)V

    .line 288
    invoke-static {}, Lcom/sec/pcw/service/push/c;->a()Ljava/util/List;

    move-result-object v5

    .line 289
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    .line 292
    iget-object v1, p0, Lcom/sec/pcw/service/push/b$1;->c:Lcom/sec/pcw/service/push/b;

    invoke-static {v1}, Lcom/sec/pcw/service/push/b;->a(Lcom/sec/pcw/service/push/b;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v1

    .line 293
    invoke-virtual {v1}, Lcom/mfluent/asp/a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v1

    .line 295
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    move v3, v0

    move v1, v0

    .line 296
    :goto_0
    if-ge v3, v6, :cond_6

    .line 297
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/service/push/c$a;

    .line 298
    invoke-virtual {v0}, Lcom/sec/pcw/service/push/c$a;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/pcw/service/push/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 300
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-ne v2, v8, :cond_0

    .line 301
    const-string v0, "mfl_NTS_PCloudHandler"

    const-string v7, "unrecognized push type!! delivery repory ignored!!"

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 305
    :cond_0
    invoke-virtual {v0}, Lcom/sec/pcw/service/push/c$a;->b()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/pcw/service/push/b$1;->a:Ljava/lang/String;

    invoke-virtual {v4, v8, v9}, Lcom/sec/pcw/service/push/a;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 307
    if-eqz v8, :cond_4

    .line 308
    invoke-static {v8}, Lcom/sec/pcw/util/h;->a(Ljava/lang/String;)Lcom/sec/pcw/service/push/util/d;

    move-result-object v8

    .line 310
    invoke-virtual {v8}, Lcom/sec/pcw/service/push/util/d;->g()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 311
    const-string v0, "mfl_NTS_PCloudHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "[retryHttpsRequest - exception] "

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/sec/pcw/service/push/util/d;->i()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 320
    :cond_1
    const-string v9, "mfl_NTS_PCloudHandler"

    const-string v10, "Check Up!! PCloudHandler.retryHttpsRequest()"

    invoke-static {v9, v10}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    invoke-virtual {v4, v8, v0, v7}, Lcom/sec/pcw/service/push/a;->a(Lcom/sec/pcw/service/push/util/d;Lcom/sec/pcw/service/push/c$a;Ljava/lang/String;)Z

    move-result v7

    .line 329
    const-string v9, "mfl_NTS_PCloudHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Https Request: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    if-eqz v7, :cond_3

    .line 332
    invoke-virtual {v8}, Lcom/sec/pcw/service/push/util/d;->c()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 333
    const-string v7, "AUL"

    invoke-virtual {v0}, Lcom/sec/pcw/service/push/c$a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 334
    iget-object v0, p0, Lcom/sec/pcw/service/push/b$1;->c:Lcom/sec/pcw/service/push/b;

    invoke-static {v0}, Lcom/sec/pcw/service/push/b;->a(Lcom/sec/pcw/service/push/b;)Landroid/content/Context;

    invoke-static {}, Lcom/sec/pcw/service/push/util/c;->a()Z

    goto :goto_1

    .line 342
    :cond_2
    new-instance v0, Lcom/sec/pcw/service/push/d;

    iget-object v7, p0, Lcom/sec/pcw/service/push/b$1;->c:Lcom/sec/pcw/service/push/b;

    invoke-static {v7}, Lcom/sec/pcw/service/push/b;->a(Lcom/sec/pcw/service/push/b;)Landroid/content/Context;

    move-result-object v7

    invoke-direct {v0, v7}, Lcom/sec/pcw/service/push/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/pcw/service/push/d;->a()V

    goto :goto_1

    :cond_3
    move v0, v2

    :goto_2
    move v1, v0

    .line 349
    goto :goto_1

    :cond_4
    move v1, v2

    .line 350
    goto/16 :goto_1

    :cond_5
    move v1, v2

    .line 358
    :cond_6
    if-eqz v1, :cond_7

    .line 359
    iget-object v0, p0, Lcom/sec/pcw/service/push/b$1;->c:Lcom/sec/pcw/service/push/b;

    invoke-static {v0}, Lcom/sec/pcw/service/push/b;->a(Lcom/sec/pcw/service/push/b;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pcw/service/push/b$1;->c:Lcom/sec/pcw/service/push/b;

    invoke-static {v1}, Lcom/sec/pcw/service/push/b;->a(Lcom/sec/pcw/service/push/b;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/service/push/util/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/pcw/service/push/b$1;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/pcw/service/push/util/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :cond_7
    :goto_3
    return-void

    .line 361
    :cond_8
    iget-object v0, p0, Lcom/sec/pcw/service/push/b$1;->c:Lcom/sec/pcw/service/push/b;

    invoke-static {v0}, Lcom/sec/pcw/service/push/b;->a(Lcom/sec/pcw/service/push/b;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/c;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pcw/service/push/b$1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 363
    new-instance v0, Lcom/sec/pcw/service/push/d;

    iget-object v1, p0, Lcom/sec/pcw/service/push/b$1;->c:Lcom/sec/pcw/service/push/b;

    invoke-static {v1}, Lcom/sec/pcw/service/push/b;->a(Lcom/sec/pcw/service/push/b;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/pcw/service/push/d;-><init>(Landroid/content/Context;)V

    .line 364
    iget-object v1, p0, Lcom/sec/pcw/service/push/b$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/pcw/service/push/d;->b(Ljava/lang/String;)V

    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_2
.end method
