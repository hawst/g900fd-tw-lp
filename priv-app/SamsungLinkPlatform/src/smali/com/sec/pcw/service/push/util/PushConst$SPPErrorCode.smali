.class public final enum Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/service/push/util/PushConst;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SPPErrorCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum B:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum C:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum D:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field private static E:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic F:[Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum a:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum b:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum c:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum d:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum e:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum f:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum g:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum h:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum i:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum j:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum k:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum l:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum m:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum n:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum o:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum p:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum q:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum r:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum s:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum t:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum u:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum v:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum w:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum x:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum y:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

.field public static final enum z:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;


# instance fields
.field private final mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 146
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "UNDEFINED"

    invoke-direct {v1, v2, v0, v0}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->a:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 151
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "TIMEOUT"

    const/4 v3, -0x1

    invoke-direct {v1, v2, v5, v3}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->b:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 152
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "NETWORK_NOT_AVAILABLE"

    const/4 v3, -0x2

    invoke-direct {v1, v2, v6, v3}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->c:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 153
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "PROVISIONING_DATA_EXISTS"

    const/16 v3, -0x64

    invoke-direct {v1, v2, v7, v3}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->d:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 154
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "INITIALIZATION_ALREADY_COMPLETED"

    const/16 v3, -0x66

    invoke-direct {v1, v2, v8, v3}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->e:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 155
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "PROVISIONING_FAIL"

    const/4 v3, 0x5

    const/16 v4, -0x67

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->f:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 156
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "INITIALIZATION_FAIL"

    const/4 v3, 0x6

    const/16 v4, -0x68

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->g:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 157
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "APPLICATION_ALREADY_DEREGISTRATION"

    const/4 v3, 0x7

    const/16 v4, -0x69

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->h:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 158
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "SUCCESS"

    const/16 v3, 0x8

    const/16 v4, 0x3e8

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->i:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 159
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "UNKOWN_MESSAGE_TYPE"

    const/16 v3, 0x9

    const/16 v4, 0x7d0

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->j:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 160
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "UNEXPECTED_MESSAGE"

    const/16 v3, 0xa

    const/16 v4, 0x7d1

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->k:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 161
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "INTERNAL_SERVER_ERROR"

    const/16 v3, 0xb

    const/16 v4, 0x7d2

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->l:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 162
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "INTERRUPTED"

    const/16 v3, 0xc

    const/16 v4, 0x7d3

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->m:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 163
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "BAD_REQUEST_FOR_PROVISION"

    const/16 v3, 0xd

    const/16 v4, 0xbb8

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->n:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 164
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "FAIL_TO_AUTHENTICATE"

    const/16 v3, 0xe

    const/16 v4, 0xbb9

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->o:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 165
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "INVALID_DEVICE_TOKEN_TO_REPROVISION"

    const/16 v3, 0xf

    const/16 v4, 0xbba

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->p:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 166
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "PROVISION_EXCEPTION"

    const/16 v3, 0x10

    const/16 v4, 0xbbb

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->q:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 167
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "CONNECTION_MAX_EXCEEDED"

    const/16 v3, 0x11

    const/16 v4, 0xfa0

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->r:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 168
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "INVALID_STATE"

    const/16 v3, 0x12

    const/16 v4, 0xfa1

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->s:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 169
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "INVALID_DEVICE_TOKEN"

    const/16 v3, 0x13

    const/16 v4, 0xfa2

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->t:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 170
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "INVALID_APP_ID"

    const/16 v3, 0x14

    const/16 v4, 0xfa3

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->u:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 171
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "INVALID_REG_ID"

    const/16 v3, 0x15

    const/16 v4, 0xfa4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->v:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 172
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "RESET_BY_NEW_INITIALIZATION"

    const/16 v3, 0x16

    const/16 v4, 0xfa5

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->w:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 173
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "REPROVISIONING_REQUIRED"

    const/16 v3, 0x17

    const/16 v4, 0xfa6

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->x:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 174
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "REGISTRATION_FAILED"

    const/16 v3, 0x18

    const/16 v4, 0xfa7

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->y:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 175
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "DEREGISTRATION_FAILED"

    const/16 v3, 0x19

    const/16 v4, 0xfa8

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->z:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 176
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "WRONG_DEVICE_TOKEN"

    const/16 v3, 0x1a

    const/16 v4, 0xfa9

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->A:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 177
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "WRONG_APP_ID"

    const/16 v3, 0x1b

    const/16 v4, 0xfaa

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->B:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 178
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "WRONG_REG_ID"

    const/16 v3, 0x1c

    const/16 v4, 0xfab

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->C:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 179
    new-instance v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    const-string v2, "UNSUPPORTED_PING_SPECIFICATION"

    const/16 v3, 0x1d

    const/16 v4, 0xfac

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->D:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 142
    const/16 v1, 0x1e

    new-array v1, v1, [Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    sget-object v2, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->a:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v2, v1, v0

    sget-object v2, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->b:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v2, v1, v5

    sget-object v2, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->c:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v2, v1, v6

    sget-object v2, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->d:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v2, v1, v7

    sget-object v2, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->e:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->f:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->g:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->h:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->i:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->j:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->k:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->l:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->m:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->n:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->o:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->p:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->q:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->r:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x12

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->s:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->t:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->u:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x15

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->v:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x16

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->w:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x17

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->x:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x18

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->y:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x19

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->z:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->A:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->B:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->C:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    sget-object v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->D:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    aput-object v3, v1, v2

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->F:[Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 189
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    sput-object v1, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->E:Ljava/util/TreeMap;

    .line 190
    invoke-static {}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->values()[Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 191
    sget-object v4, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->E:Ljava/util/TreeMap;

    iget v5, v3, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->mValue:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 184
    iput p3, p0, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->mValue:I

    .line 185
    return-void
.end method

.method public static a(I)Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;
    .locals 2

    .prologue
    .line 200
    sget-object v0, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->E:Ljava/util/TreeMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    .line 202
    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->a:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;
    .locals 1

    .prologue
    .line 142
    const-class v0, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    return-object v0
.end method

.method public static values()[Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->F:[Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    invoke-virtual {v0}, [Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->mValue:I

    return v0
.end method
