.class final Lcom/sec/pcw/service/push/d$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/c$a;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/service/push/c$a;

.field final synthetic b:Lcom/sec/pcw/service/push/a;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/sec/pcw/service/push/d;


# direct methods
.method constructor <init>(Lcom/sec/pcw/service/push/d;Lcom/sec/pcw/service/push/c$a;Lcom/sec/pcw/service/push/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 710
    iput-object p1, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    iput-object p2, p0, Lcom/sec/pcw/service/push/d$3;->a:Lcom/sec/pcw/service/push/c$a;

    iput-object p3, p0, Lcom/sec/pcw/service/push/d$3;->b:Lcom/sec/pcw/service/push/a;

    iput-object p4, p0, Lcom/sec/pcw/service/push/d$3;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x2

    .line 715
    :try_start_0
    invoke-static {}, Lcom/sec/pcw/service/push/d;->c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_0

    .line 716
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "[requestDeliveryReport] - report."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    :cond_0
    invoke-static {}, Lcom/sec/pcw/service/push/d;->c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v4, :cond_1

    .line 719
    const-string v0, "mfl_PushMsgHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[requestDeliveryReport] - report: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/pcw/service/push/d$3;->a:Lcom/sec/pcw/service/push/c$a;

    invoke-virtual {v2}, Lcom/sec/pcw/service/push/c$a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    :cond_1
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-static {v0}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    .line 724
    invoke-virtual {v0}, Lcom/mfluent/asp/a;->b()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v0

    .line 726
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 729
    :cond_2
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "[requestDeliveryReport] AccessTokenInfo is empty"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 827
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-virtual {v0}, Lcom/sec/pcw/service/push/d;->b()V

    .line 828
    :goto_0
    return-void

    .line 734
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$3;->b:Lcom/sec/pcw/service/push/a;

    iget-object v1, p0, Lcom/sec/pcw/service/push/d$3;->a:Lcom/sec/pcw/service/push/c$a;

    invoke-virtual {v1}, Lcom/sec/pcw/service/push/c$a;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-static {v2}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/pcw/service/push/util/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/pcw/service/push/d$3;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/pcw/service/push/a;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 736
    invoke-static {}, Lcom/sec/pcw/service/push/d;->c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v4, :cond_4

    .line 737
    const-string v1, "mfl_PushMsgHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[requestDeliveryReport] msg: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    :cond_4
    if-eqz v0, :cond_a

    .line 741
    invoke-static {v0}, Lcom/sec/pcw/util/h;->a(Ljava/lang/String;)Lcom/sec/pcw/service/push/util/d;

    move-result-object v1

    .line 743
    invoke-virtual {v1}, Lcom/sec/pcw/service/push/util/d;->g()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 744
    const-string v0, "mfl_PushMsgHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[requestDeliveryReport - XML Parse Exception] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/pcw/service/push/util/d;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 827
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-virtual {v0}, Lcom/sec/pcw/service/push/d;->b()V

    goto :goto_0

    .line 746
    :cond_6
    :try_start_2
    iget-object v2, p0, Lcom/sec/pcw/service/push/d$3;->b:Lcom/sec/pcw/service/push/a;

    iget-object v3, p0, Lcom/sec/pcw/service/push/d$3;->a:Lcom/sec/pcw/service/push/c$a;

    iget-object v4, p0, Lcom/sec/pcw/service/push/d$3;->c:Ljava/lang/String;

    invoke-virtual {v2, v1, v3, v4}, Lcom/sec/pcw/service/push/a;->a(Lcom/sec/pcw/service/push/util/d;Lcom/sec/pcw/service/push/c$a;Ljava/lang/String;)Z

    move-result v2

    .line 748
    const-string v3, "mfl_PushMsgHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Https Request: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/sec/pcw/service/push/util/d;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 750
    const-string v1, "AUL"

    iget-object v2, p0, Lcom/sec/pcw/service/push/d$3;->a:Lcom/sec/pcw/service/push/c$a;

    invoke-virtual {v2}, Lcom/sec/pcw/service/push/c$a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 751
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-static {v0}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    invoke-static {}, Lcom/sec/pcw/service/push/util/c;->a()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 827
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-virtual {v1}, Lcom/sec/pcw/service/push/d;->b()V

    throw v0

    .line 752
    :cond_7
    :try_start_3
    const-string v1, "NTF"

    iget-object v2, p0, Lcom/sec/pcw/service/push/d$3;->a:Lcom/sec/pcw/service/push/c$a;

    invoke-virtual {v2}, Lcom/sec/pcw/service/push/c$a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 753
    iget-object v1, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-virtual {v1, v0}, Lcom/sec/pcw/service/push/d;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 754
    :cond_8
    const-string v0, "DLR"

    iget-object v1, p0, Lcom/sec/pcw/service/push/d$3;->a:Lcom/sec/pcw/service/push/c$a;

    invoke-virtual {v1}, Lcom/sec/pcw/service/push/c$a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 755
    invoke-static {}, Lcom/sec/pcw/service/push/d;->c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v6, :cond_9

    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "Need to send device list sync request"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mfl_PushMsgHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Checking the context : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-static {v2}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-static {v0}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-static {v0}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->a(Landroid/content/Context;)Lcom/mfluent/asp/sync/DeviceListSyncManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.sync.DeviceListSyncManager.DEVICE_LIST_REQUEST_VIA_PUSH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->f(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 815
    :cond_a
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "[requestDeliveryReport] - error in requestHttpOnSecurity"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$3;->b:Lcom/sec/pcw/service/push/a;

    iget-object v1, p0, Lcom/sec/pcw/service/push/d$3;->a:Lcom/sec/pcw/service/push/c$a;

    invoke-virtual {v0, v1}, Lcom/sec/pcw/service/push/a;->a(Lcom/sec/pcw/service/push/c$a;)V

    .line 824
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-static {v0}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pcw/service/push/d$3;->d:Lcom/sec/pcw/service/push/d;

    invoke-static {v1}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/pcw/service/push/util/c;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/pcw/service/push/d$3;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/pcw/service/push/util/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1
.end method
