.class public final Lcom/sec/pcw/service/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/service/d/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    .line 46
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    sput-object v0, Lcom/sec/pcw/service/d/a;->b:Landroid/content/Context;

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 452
    :try_start_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xa

    if-le v1, v2, :cond_2

    .line 453
    new-instance v2, Lcom/mfluent/a/a/b;

    sget-object v1, Lcom/sec/pcw/service/d/a;->b:Landroid/content/Context;

    invoke-direct {v2, v1}, Lcom/mfluent/a/a/b;-><init>(Landroid/content/Context;)V

    .line 454
    invoke-virtual {v2}, Lcom/mfluent/a/a/b;->a()[Landroid/os/storage/StorageVolume;

    move-result-object v3

    .line 456
    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 457
    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 458
    invoke-virtual {v2, p0}, Lcom/mfluent/a/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 459
    const-string v2, "mounted"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 460
    const/4 v0, 0x1

    .line 474
    :cond_0
    :goto_1
    return v0

    .line 456
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 468
    :catch_0
    move-exception v0

    .line 469
    sget-object v1, Lcom/sec/pcw/service/d/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_2

    .line 470
    const-string v1, "mfl_StorageStatus"

    const-string v2, "::isVolumeMounted:"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 474
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static a()Ljava/io/File;
    .locals 8

    .prologue
    const/16 v7, 0xa

    .line 280
    const/4 v1, 0x0

    .line 283
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v0, v7, :cond_3

    .line 284
    new-instance v0, Lcom/mfluent/a/a/b;

    sget-object v2, Lcom/sec/pcw/service/d/a;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/mfluent/a/a/b;-><init>(Landroid/content/Context;)V

    .line 285
    invoke-virtual {v0}, Lcom/mfluent/a/a/b;->a()[Landroid/os/storage/StorageVolume;

    move-result-object v2

    .line 286
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 287
    const-string v5, "sd"

    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 288
    new-instance v0, Ljava/io/File;

    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :goto_1
    if-nez v0, :cond_0

    .line 302
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v0, v7, :cond_5

    const-string v0, "/mnt/extSdCard"

    .line 303
    :goto_2
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 305
    :cond_0
    return-object v0

    .line 286
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 292
    goto :goto_1

    .line 293
    :cond_3
    :try_start_1
    invoke-static {}, Lcom/mfluent/a/a/a;->a()Ljava/io/File;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 295
    :catch_0
    move-exception v0

    .line 296
    sget-object v2, Lcom/sec/pcw/service/d/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x6

    if-gt v2, v3, :cond_4

    .line 297
    const-string v2, "mfl_StorageStatus"

    const-string v3, "::getSDCardMemeryPath:"

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_4
    move-object v0, v1

    goto :goto_1

    .line 302
    :cond_5
    const-string v0, "/mnt/sdcard/external_sd"

    goto :goto_2
.end method

.method public static b()Lorg/json/JSONObject;
    .locals 6

    .prologue
    .line 314
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 315
    invoke-static {}, Lcom/sec/pcw/service/d/a;->i()J

    move-result-wide v2

    .line 316
    invoke-static {}, Lcom/sec/pcw/service/d/a;->j()J

    move-result-wide v4

    .line 318
    :try_start_0
    const-string v0, "used"

    sub-long v2, v4, v2

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 319
    const-string v0, "total"

    invoke-virtual {v1, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    :cond_0
    :goto_0
    return-object v1

    .line 320
    :catch_0
    move-exception v0

    .line 321
    sget-object v2, Lcom/sec/pcw/service/d/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 322
    const-string v2, "mfl_StorageStatus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getExternalMemoryStatus:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static c()Lorg/json/JSONObject;
    .locals 6

    .prologue
    .line 334
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 335
    invoke-static {}, Lcom/sec/pcw/service/d/a;->k()J

    move-result-wide v2

    .line 336
    invoke-static {}, Lcom/sec/pcw/service/d/a;->m()J

    move-result-wide v4

    .line 339
    :try_start_0
    const-string v0, "used"

    sub-long v2, v4, v2

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 340
    const-string v0, "total"

    invoke-virtual {v1, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    :cond_0
    :goto_0
    return-object v1

    .line 341
    :catch_0
    move-exception v0

    .line 342
    sget-object v2, Lcom/sec/pcw/service/d/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 343
    const-string v2, "mfl_StorageStatus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getSdcardMemoryStatus:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static d()Lorg/json/JSONObject;
    .locals 10

    .prologue
    .line 355
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 356
    invoke-static {}, Lcom/sec/pcw/service/d/a;->i()J

    move-result-wide v2

    .line 357
    invoke-static {}, Lcom/sec/pcw/service/d/a;->j()J

    move-result-wide v4

    .line 358
    invoke-static {}, Lcom/sec/pcw/service/d/a;->k()J

    move-result-wide v6

    .line 359
    invoke-static {}, Lcom/sec/pcw/service/d/a;->m()J

    move-result-wide v8

    .line 362
    :try_start_0
    const-string v0, "used"

    sub-long v2, v4, v2

    add-long/2addr v2, v8

    sub-long/2addr v2, v6

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 363
    const-string v0, "total"

    add-long v2, v4, v8

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 369
    :cond_0
    :goto_0
    return-object v1

    .line 364
    :catch_0
    move-exception v0

    .line 365
    sget-object v2, Lcom/sec/pcw/service/d/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 366
    const-string v2, "mfl_StorageStatus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::getTotalMemoryStatus:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static e()J
    .locals 8

    .prologue
    .line 373
    invoke-static {}, Lcom/sec/pcw/service/d/a;->i()J

    move-result-wide v0

    .line 374
    invoke-static {}, Lcom/sec/pcw/service/d/a;->j()J

    move-result-wide v2

    .line 375
    invoke-static {}, Lcom/sec/pcw/service/d/a;->k()J

    move-result-wide v4

    .line 376
    invoke-static {}, Lcom/sec/pcw/service/d/a;->m()J

    move-result-wide v6

    .line 378
    sub-long v0, v2, v0

    add-long/2addr v0, v6

    sub-long/2addr v0, v4

    return-wide v0
.end method

.method public static f()J
    .locals 4

    .prologue
    .line 382
    invoke-static {}, Lcom/sec/pcw/service/d/a;->j()J

    move-result-wide v0

    .line 383
    invoke-static {}, Lcom/sec/pcw/service/d/a;->m()J

    move-result-wide v2

    .line 385
    add-long/2addr v0, v2

    return-wide v0
.end method

.method public static g()Z
    .locals 1

    .prologue
    .line 394
    invoke-static {}, Lcom/sec/pcw/service/d/a;->l()Landroid/os/StatFs;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static h()Z
    .locals 2

    .prologue
    .line 60
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static i()J
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 92
    :try_start_0
    invoke-static {}, Lcom/sec/pcw/service/d/a;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 93
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    .line 94
    new-instance v4, Landroid/os/StatFs;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 95
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v2

    .line 99
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v0

    .line 105
    :goto_0
    mul-long/2addr v0, v2

    .line 111
    :cond_0
    :goto_1
    return-wide v0

    .line 101
    :cond_1
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    int-to-long v2, v2

    .line 102
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 109
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private static j()J
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 121
    invoke-static {}, Lcom/sec/pcw/service/d/a;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    .line 124
    :try_start_0
    new-instance v4, Landroid/os/StatFs;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 125
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 128
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v2

    .line 129
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v0

    .line 134
    :goto_0
    mul-long/2addr v0, v2

    .line 139
    :cond_0
    :goto_1
    return-wide v0

    .line 131
    :cond_1
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    int-to-long v2, v2

    .line 132
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockCount()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    .line 136
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private static k()J
    .locals 4

    .prologue
    .line 149
    invoke-static {}, Lcom/sec/pcw/service/d/a;->l()Landroid/os/StatFs;

    move-result-object v0

    .line 150
    if-nez v0, :cond_0

    .line 151
    const-wide/16 v0, 0x0

    .line 160
    :goto_0
    return-wide v0

    .line 154
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 156
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v0

    mul-long/2addr v0, v2

    goto :goto_0

    .line 158
    :cond_1
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    mul-int/2addr v0, v1

    int-to-long v0, v0

    goto :goto_0
.end method

.method private static l()Landroid/os/StatFs;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 186
    invoke-static {}, Lcom/sec/pcw/service/d/a;->h()Z

    move-result v1

    if-nez v1, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-object v0

    .line 190
    :cond_1
    invoke-static {}, Lcom/sec/pcw/service/d/a;->a()Ljava/io/File;

    move-result-object v2

    .line 191
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    invoke-virtual {v2}, Ljava/io/File;->canWrite()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    :try_start_0
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 236
    goto :goto_0

    .line 230
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static m()J
    .locals 4

    .prologue
    .line 245
    invoke-static {}, Lcom/sec/pcw/service/d/a;->l()Landroid/os/StatFs;

    move-result-object v0

    .line 246
    if-nez v0, :cond_0

    .line 247
    const-wide/16 v0, 0x0

    .line 256
    :goto_0
    return-wide v0

    .line 250
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/util/UiUtils;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 252
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v0

    mul-long/2addr v0, v2

    goto :goto_0

    .line 254
    :cond_1
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    mul-int/2addr v0, v1

    int-to-long v0, v0

    goto :goto_0
.end method
