.class public final Lcom/sec/pcw/service/push/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_NTS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/service/push/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    .line 35
    return-void
.end method

.method static synthetic a(Lcom/sec/pcw/service/push/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 227
    const-string v0, "mfl_NTS_PCloudHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[sendBroadcastForSetting] - action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 229
    iget-object v1, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 230
    return-void
.end method

.method private d()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 196
    iget-object v1, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v1

    .line 197
    invoke-virtual {v1}, Lcom/mfluent/asp/a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v1

    .line 199
    if-eqz v1, :cond_0

    .line 200
    invoke-virtual {v1}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/commons/lang3/StringUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 201
    const/4 v0, 0x1

    .line 206
    :cond_0
    return v0
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x2

    .line 76
    const-string v2, "mfl_NTS_PCloudHandler"

    const-string v3, "[initGoogleAccountAndRequestC2DM]"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v2, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/pcw/util/a;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/pcw/service/push/util/b;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    .line 82
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/util/a;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 83
    const-string v1, "mfl_NTS_PCloudHandler"

    const-string v2, "First Google Account Registration"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    sget-object v1, Lcom/sec/pcw/service/push/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v6, :cond_0

    .line 85
    const-string v1, "mfl_NTS_PCloudHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::initGoogleAccountAndRequestC2DM:google: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/pcw/service/push/util/b;->e(Landroid/content/Context;Ljava/lang/String;)Z

    .line 90
    const-string v0, "android.intent.action.GOOGLEACCOUNT_REGISTED"

    invoke-direct {p0, v0}, Lcom/sec/pcw/service/push/b;->b(Ljava/lang/String;)V

    .line 130
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v2, v1

    .line 78
    goto :goto_0

    .line 96
    :cond_3
    iget-object v2, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/pcw/service/push/util/b;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 97
    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 98
    sget-object v3, Lcom/sec/pcw/service/push/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v6, :cond_4

    .line 99
    const-string v3, "mfl_NTS_PCloudHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::initGoogleAccountAndRequestC2DM:saved Google Account: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_4
    iget-object v3, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/sec/pcw/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 104
    const-string v3, "mfl_NTS_PCloudHandler"

    const-string v4, "Google Account Deleted."

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    sget-object v3, Lcom/sec/pcw/service/push/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v3}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v3

    if-gt v3, v6, :cond_5

    .line 106
    const-string v3, "mfl_NTS_PCloudHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::initGoogleAccountAndRequestC2DM:Deleted GoogleAccount : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_5
    iget-object v2, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/pcw/service/push/util/b;->h(Landroid/content/Context;)Z

    .line 119
    :cond_6
    iget-object v2, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/pcw/util/a;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_7

    :goto_2
    if-eqz v0, :cond_1

    .line 120
    const-string v0, "mfl_NTS_PCloudHandler"

    const-string v1, "Google Account & C2DM Unregistration"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/b;->h(Landroid/content/Context;)Z

    .line 124
    const-string v0, "android.intent.action.GOOGLEACCOUNT_REMOVED"

    invoke-direct {p0, v0}, Lcom/sec/pcw/service/push/b;->b(Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lcom/sec/pcw/service/push/util/c;->b()Z

    goto :goto_1

    :cond_7
    move v0, v1

    .line 119
    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 44
    const-string v0, "mfl_NTS_PCloudHandler"

    const-string v1, "[initSamsungAccounAndRequestToken]"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    sget-object v0, Lcom/sec/pcw/service/push/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_0

    .line 46
    const-string v0, "mfl_NTS_PCloudHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::initSamsungAccounAndRequestTokent:userId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/pcw/service/push/util/c;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    .line 52
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;I)Z

    .line 59
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/pcw/service/push/util/b;->d(Landroid/content/Context;Ljava/lang/String;)Z

    .line 61
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/c;->d(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    .line 63
    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_3

    .line 64
    iget-object v3, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    aget-object v4, v1, v0

    invoke-static {v3, v4}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 66
    if-eqz v3, :cond_1

    const-string v4, ""

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 67
    :cond_1
    iget-object v3, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    aget-object v4, v1, v0

    invoke-static {v3, v4}, Lcom/sec/pcw/service/push/d;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 63
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_3
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 275
    const-string v0, "mfl_NTS_PCloudHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[retryHttpsRequest] - uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/pcw/service/push/b$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/pcw/service/push/b$1;-><init>(Lcom/sec/pcw/service/push/b;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 369
    return-void
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 414
    .line 416
    if-nez p1, :cond_0

    .line 417
    const-string v0, "mfl_NTS_PCloudHandler"

    const-string v2, "[ensureRegistration] - Context is invalid"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :goto_0
    return v1

    .line 422
    :cond_0
    invoke-static {p1}, Lcom/sec/pcw/service/push/util/c;->d(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v4

    .line 429
    invoke-static {p1}, Lcom/sec/pcw/util/a;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 431
    array-length v6, v4

    move v3, v1

    move v0, v2

    :goto_1
    if-ge v3, v6, :cond_6

    .line 432
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 434
    const-string v7, "mfl_NTS_PCloudHandler"

    const-string v8, "[ensureRegistration] - Samsung account exist"

    invoke-static {v7, v8}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    aget-object v7, v4, v3

    invoke-static {p1, v7}, Lcom/sec/pcw/service/push/util/b;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 441
    const-string v7, "mfl_NTS_PCloudHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[ensureRegistration] - MG registered : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v9, v4, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    aget-object v7, v4, v3

    invoke-static {p1, v7}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 445
    const-string v8, ""

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 447
    const-string v0, "mfl_NTS_PCloudHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[ensureRegistration] - Re-registration ("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v8, v4, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    aget-object v0, v4, v3

    invoke-static {p1, v0}, Lcom/sec/pcw/service/push/d;->a(Landroid/content/Context;Ljava/lang/String;)V

    move v0, v1

    .line 431
    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 458
    :cond_2
    const-string v0, "mfl_NTS_PCloudHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[ensureRegistration] - MG unregistered : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v8, v4, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    invoke-direct {p0}, Lcom/sec/pcw/service/push/b;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 462
    invoke-static {p1, v2}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;I)Z

    .line 465
    invoke-static {p1}, Lcom/sec/pcw/service/push/util/c;->a(Landroid/content/Context;)V

    move v1, v2

    .line 467
    goto/16 :goto_0

    .line 470
    :cond_3
    aget-object v0, v4, v3

    invoke-static {p1, v0}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 472
    const-string v7, ""

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 474
    const-string v0, "mfl_NTS_PCloudHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[ensureRegistration] - request registration : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v8, v4, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    aget-object v0, v4, v3

    invoke-static {p1, v0}, Lcom/sec/pcw/service/push/d;->a(Landroid/content/Context;Ljava/lang/String;)V

    move v0, v1

    .line 478
    goto :goto_2

    .line 480
    :cond_4
    const-string v0, "mfl_NTS_PCloudHandler"

    const-string v7, "[ensureRegistration] - dispatchMGRegistration"

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    new-instance v0, Lcom/sec/pcw/service/push/d;

    invoke-direct {v0, p1}, Lcom/sec/pcw/service/push/d;-><init>(Landroid/content/Context;)V

    .line 485
    aget-object v7, v4, v3

    invoke-virtual {v0, v7}, Lcom/sec/pcw/service/push/d;->b(Ljava/lang/String;)V

    move v0, v1

    .line 490
    goto :goto_2

    .line 494
    :cond_5
    const-string v0, "mfl_NTS_PCloudHandler"

    const-string v7, "[ensureRegistration] - No Samsung account"

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    aget-object v0, v4, v3

    invoke-static {p1, v0}, Lcom/sec/pcw/service/push/util/b;->g(Landroid/content/Context;Ljava/lang/String;)Z

    move v0, v1

    .line 498
    goto :goto_2

    :cond_6
    move v1, v0

    .line 502
    goto/16 :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/b;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 241
    const-string v1, "mfl_NTS_PCloudHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[unregistration] - Samsung Account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    iget-object v1, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/pcw/service/push/util/b;->h(Landroid/content/Context;)Z

    .line 243
    iget-object v1, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    const-string v2, "asp_push_pref_15"

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "samsung_account"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v1}, Lcom/mfluent/asp/util/b;->a(Landroid/content/SharedPreferences$Editor;)Z

    .line 246
    iget-object v1, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/pcw/service/push/util/c;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/c;->d(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    .line 250
    const/4 v0, 0x0

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    .line 251
    iget-object v3, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    aget-object v4, v1, v0

    invoke-static {v3, v4}, Lcom/sec/pcw/service/push/util/b;->g(Landroid/content/Context;Ljava/lang/String;)Z

    .line 252
    const-string v3, ""

    iget-object v4, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    aget-object v5, v1, v0

    invoke-static {v4, v5}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 255
    iget-object v3, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    aget-object v4, v1, v0

    invoke-static {v3, v4}, Lcom/sec/pcw/service/push/util/b;->b(Landroid/content/Context;Ljava/lang/String;)Z

    .line 256
    iget-object v3, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    aget-object v4, v1, v0

    invoke-static {v3, v4}, Lcom/sec/pcw/service/push/d;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 250
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/b;->c(Landroid/content/Context;)Z

    .line 262
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;)Z

    .line 263
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/b;->b(Landroid/content/Context;)Z

    .line 264
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/b;->d(Landroid/content/Context;)Z

    .line 265
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/b;->e(Landroid/content/Context;)Z

    .line 266
    return-void
.end method

.method public final c()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 372
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/b;->i(Landroid/content/Context;)I

    move-result v0

    .line 373
    const-string v3, "mfl_NTS_PCloudHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[manageC2DM] - Push State Flag: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    and-int/lit8 v3, v0, 0x1

    if-ne v2, v3, :cond_9

    .line 376
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/c;->d(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v4

    .line 378
    array-length v5, v4

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_a

    .line 379
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    aget-object v6, v4, v3

    invoke-static {v0, v6}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 381
    iget-object v6, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    aget-object v7, v4, v3

    invoke-static {v6, v7}, Lcom/sec/pcw/service/push/util/b;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 382
    if-eqz v0, :cond_0

    const-string v6, ""

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 383
    :cond_0
    const-string v0, "mfl_NTS_PCloudHandler"

    const-string v6, "Registration id is empty or null"

    invoke-static {v0, v6}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    aget-object v6, v4, v3

    invoke-static {v0, v6}, Lcom/sec/pcw/service/push/d;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 378
    :cond_1
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 387
    :cond_2
    aget-object v6, v4, v3

    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/sec/pcw/service/push/util/b;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-direct {p0}, Lcom/sec/pcw/service/push/b;->d()Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "GCM"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/util/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/sec/pcw/service/push/util/c;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v2

    :goto_2
    const-string v7, "mfl_NTS_PCloudHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[registMGServer] - isPossible: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_7

    const-string v0, "mfl_NTS_PCloudHandler"

    const-string v7, "MessageGateway Server Registration"

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/pcw/service/push/d;

    iget-object v7, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-direct {v0, v7}, Lcom/sec/pcw/service/push/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Lcom/sec/pcw/service/push/d;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    const-string v0, "C2DM"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/util/a;->a(Landroid/content/Context;)Z

    move-result v0

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    sget-object v0, Lcom/sec/pcw/service/push/b;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v10, :cond_8

    const-string v0, "mfl_NTS_PCloudHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[registMGServer] - Google Account: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/pcw/util/a;->a(Landroid/content/Context;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mfl_NTS_PCloudHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[registMGServer] - Registrating iD: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v8, v6}, Lcom/sec/pcw/service/push/util/b;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mfl_NTS_PCloudHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[registMGServer] - Samsung Account: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/pcw/util/a;->b(Landroid/content/Context;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mfl_NTS_PCloudHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[registMGServer] - AccessToken: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/pcw/service/push/b;->d()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/sec/pcw/service/push/d;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 391
    :cond_9
    and-int/lit8 v1, v0, 0x2

    if-ne v10, v1, :cond_b

    .line 393
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    const-string v1, "asp_push_pref_15"

    invoke-virtual {v0, v1, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "delivery_report_body"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "delivery_report_action_code"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/pcw/service/push/c$a;

    invoke-direct {v2, v1, v0}, Lcom/sec/pcw/service/push/c$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    invoke-virtual {v2}, Lcom/sec/pcw/service/push/c$a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/pcw/service/push/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 396
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v2}, Lcom/sec/pcw/service/push/c$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 397
    new-instance v1, Lcom/sec/pcw/service/push/d;

    iget-object v3, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/sec/pcw/service/push/d;-><init>(Landroid/content/Context;)V

    .line 398
    invoke-virtual {v1, v2, v0}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/c$a;Ljava/lang/String;)V

    .line 406
    :cond_a
    :goto_3
    return-void

    .line 400
    :cond_b
    and-int/lit8 v0, v0, 0x4

    if-ne v6, v0, :cond_a

    .line 404
    iget-object v0, p0, Lcom/sec/pcw/service/push/b;->b:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/sec/pcw/service/push/util/b;->b(Landroid/content/Context;I)Z

    goto :goto_3

    :cond_c
    move v0, v1

    goto/16 :goto_2
.end method
