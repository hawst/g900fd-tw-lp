.class public final Lcom/sec/pcw/service/push/spp/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/service/push/spp/a$1;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/service/push/spp/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/service/push/spp/a;->b:Landroid/content/Context;

    .line 52
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 207
    const/4 v2, 0x0

    .line 210
    :try_start_0
    new-instance v1, Lcom/sec/pcw/service/push/spp/b;

    invoke-direct {v1, p0}, Lcom/sec/pcw/service/push/spp/b;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    :try_start_1
    const-string v0, "1f590c2d39fbaeb7"

    invoke-virtual {v1, v0}, Lcom/sec/pcw/service/push/spp/b;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 214
    invoke-virtual {v1}, Lcom/sec/pcw/service/push/spp/b;->close()V

    .line 216
    return-void

    .line 214
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_0

    .line 215
    invoke-virtual {v1}, Lcom/sec/pcw/service/push/spp/b;->close()V

    .line 216
    :cond_0
    throw v0

    .line 214
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 222
    const/4 v2, 0x0

    .line 225
    :try_start_0
    new-instance v1, Lcom/sec/pcw/service/push/spp/b;

    invoke-direct {v1, p0}, Lcom/sec/pcw/service/push/spp/b;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    :try_start_1
    const-string v0, "1f590c2d39fbaeb7"

    invoke-virtual {v1, v0}, Lcom/sec/pcw/service/push/spp/b;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 229
    invoke-virtual {v1}, Lcom/sec/pcw/service/push/spp/b;->close()V

    .line 231
    return-void

    .line 229
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_0

    .line 230
    invoke-virtual {v1}, Lcom/sec/pcw/service/push/spp/b;->close()V

    .line 231
    :cond_0
    throw v0

    .line 229
    :catchall_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    .line 61
    const-string v0, "mfl_SPPHandler"

    const-string v1, "[handleRegistration]"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v0, "com.sec.spp.Status"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 76
    const-string v1, "Error"

    sget-object v2, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->a:Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    invoke-virtual {v2}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->a()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 78
    const-string v2, "appId"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 79
    const-string v3, "RegistrationID"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 81
    const-string v4, "mfl_SPPHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "status = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v4, "mfl_SPPHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "errorCode = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->a(I)Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    sget-object v4, Lcom/sec/pcw/service/push/spp/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v5, 0x2

    if-gt v4, v5, :cond_0

    .line 84
    const-string v4, "mfl_SPPHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::handleRegistration:appId = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v2, "mfl_SPPHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::handleRegistration:regId = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 154
    :goto_0
    const-string v0, "mfl_SPPHandler"

    const-string v1, "ignored!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :goto_1
    return-void

    .line 91
    :pswitch_0
    const-string v0, "mfl_SPPHandler"

    const-string v1, "Config.PUSH_REGISTRATION_SUCCESS"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    if-eqz v3, :cond_1

    .line 95
    const-string v0, "mfl_SPPHandler"

    const-string v1, "Registration Completed."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/b;->c(Landroid/content/Context;)Z

    .line 99
    new-instance v0, Lcom/sec/pcw/service/push/d;

    iget-object v1, p0, Lcom/sec/pcw/service/push/spp/a;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/pcw/service/push/d;-><init>(Landroid/content/Context;)V

    const-string v1, "SPP"

    invoke-virtual {v0, v3, v1}, Lcom/sec/pcw/service/push/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 101
    :cond_1
    const-string v0, "mfl_SPPHandler"

    const-string v1, "Registration ID is null!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 110
    :pswitch_1
    const-string v0, "mfl_SPPHandler"

    const-string v2, "Config.PUSH_REGISTRATION_FAIL"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    sget-object v0, Lcom/sec/pcw/service/push/spp/a$1;->a:[I

    invoke-static {v1}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->a(I)Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/pcw/service/push/util/PushConst$SPPErrorCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 125
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SPP_REGISTRATION_RETRY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    iget-object v1, p0, Lcom/sec/pcw/service/push/spp/a;->b:Landroid/content/Context;

    const-string v2, "asp_push_pref_15"

    invoke-virtual {v1, v2, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "spp_backoff"

    const/16 v3, 0x3e8

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 127
    iget-object v2, p0, Lcom/sec/pcw/service/push/spp/a;->b:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Lcom/sec/pcw/service/push/util/c;->a(Landroid/content/Context;Landroid/content/Intent;I)I

    move-result v0

    .line 129
    iget-object v1, p0, Lcom/sec/pcw/service/push/spp/a;->b:Landroid/content/Context;

    const-string v2, "asp_push_pref_15"

    invoke-virtual {v1, v2, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "spp_backoff"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-static {v1}, Lcom/mfluent/asp/util/b;->a(Landroid/content/SharedPreferences$Editor;)Z

    goto :goto_1

    .line 120
    :pswitch_2
    const-string v0, "mfl_SPPHandler"

    const-string v1, "ignored!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 138
    :pswitch_3
    const-string v0, "mfl_SPPHandler"

    const-string v1, "Config.PUSH_DEREGISTRATION_SUCCESS"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/a;->b:Landroid/content/Context;

    const-string v1, "SPP"

    invoke-static {v0, v1}, Lcom/sec/pcw/service/push/util/b;->b(Landroid/content/Context;Ljava/lang/String;)Z

    .line 142
    iget-object v0, p0, Lcom/sec/pcw/service/push/spp/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/b;->c(Landroid/content/Context;)Z

    goto/16 :goto_1

    .line 148
    :pswitch_4
    const-string v0, "mfl_SPPHandler"

    const-string v1, "Config.PUSH_DEREGISTRATION_FAIL"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 112
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v10, 0x0

    .line 167
    const-string v0, "mfl_SPPHandler"

    const-string v1, "[handlePushMsg]"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string v0, "appId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170
    const-string v1, "notificationId"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 171
    const-string v2, "msg"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 172
    const-string v3, "ack"

    invoke-virtual {p1, v3, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 173
    const-string v4, "sender"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 174
    const-string v5, "appData"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 175
    const-string v6, "timeStamp"

    const-wide/16 v8, 0x0

    invoke-virtual {p1, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 176
    const-string v7, "sessionInfo"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 177
    const-string v8, "connectionTerm"

    invoke-virtual {p1, v8, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 179
    sget-object v9, Lcom/sec/pcw/service/push/spp/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v9}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v9

    const/4 v10, 0x2

    if-gt v9, v10, :cond_0

    .line 180
    const-string v9, "mfl_SPPHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "::handlePushMsg:appId = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v0, "mfl_SPPHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "::handlePushMsg:notiId = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v0, "mfl_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v9, "::handlePushMsg:msg = "

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v0, "mfl_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v9, "::handlePushMsg:ack = "

    invoke-direct {v1, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v0, "mfl_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::handlePushMsg:sender = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v0, "mfl_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::handlePushMsg:appData = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v0, "mfl_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::handlePushMsg:timestamp = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v0, "mfl_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::handlePushMsg:sessionInfo = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v0, "mfl_SPPHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "::handlePushMsg:connectionTerm = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 193
    :try_start_0
    new-instance v0, Lcom/sec/pcw/service/push/d;

    iget-object v1, p0, Lcom/sec/pcw/service/push/spp/a;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/pcw/service/push/d;-><init>(Landroid/content/Context;)V

    const-string v1, "SPP"

    invoke-virtual {v0, v2, v1}, Lcom/sec/pcw/service/push/d;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    :goto_0
    return-void

    .line 194
    :catch_0
    move-exception v0

    .line 195
    const-string v1, "mfl_SPPHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception is occured in PushMsgHandler.handleMsg : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 198
    :cond_1
    const-string v0, "mfl_SPPHandler"

    const-string v1, "message is null"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
