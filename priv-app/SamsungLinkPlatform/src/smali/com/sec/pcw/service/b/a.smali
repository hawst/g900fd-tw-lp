.class public final Lcom/sec/pcw/service/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private a:I

.field private b:I

.field private final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/service/b/a;->d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput v0, p0, Lcom/sec/pcw/service/b/a;->a:I

    .line 44
    iput v0, p0, Lcom/sec/pcw/service/b/a;->b:I

    .line 45
    iput-boolean v0, p0, Lcom/sec/pcw/service/b/a;->c:Z

    return-void
.end method

.method private a(Landroid/database/Cursor;IILjava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 115
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 116
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 117
    :cond_0
    invoke-direct {p0, v5, v3, v3}, Lcom/sec/pcw/service/b/a;->a(Lorg/json/JSONArray;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 119
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/sec/pcw/service/b/a;->a:I

    .line 120
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 121
    iget v0, p0, Lcom/sec/pcw/service/b/a;->b:I

    const-string v2, "numsongs"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lcom/sec/pcw/service/b/a;->b:I

    .line 122
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 125
    :cond_2
    const/4 v2, -0x1

    .line 127
    invoke-static {p1, p4}, Lcom/sec/pcw/util/LanguageUtil;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 130
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v6

    .line 132
    :goto_2
    invoke-interface {v6}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 133
    invoke-interface {v6}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 135
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, p2, :cond_4

    .line 136
    invoke-interface {v6}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    .line 142
    :cond_3
    if-lez p3, :cond_8

    .line 144
    if-ltz v2, :cond_6

    .line 145
    add-int/lit8 v2, v2, 0x1

    .line 146
    add-int v0, v2, p3

    iget v6, p0, Lcom/sec/pcw/service/b/a;->a:I

    if-ge v0, v6, :cond_5

    add-int v0, v2, p3

    :goto_3
    move p3, v0

    .line 158
    :goto_4
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v6

    move v4, v2

    .line 159
    :goto_5
    if-ge v4, p3, :cond_c

    .line 160
    invoke-interface {v6}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 161
    invoke-static {p1, v4}, Lcom/sec/pcw/service/b/a;->a(Landroid/database/Cursor;I)Lorg/json/JSONObject;

    move-result-object v0

    .line 162
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 163
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 159
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    .line 139
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_2

    .line 146
    :cond_5
    iget v0, p0, Lcom/sec/pcw/service/b/a;->a:I

    goto :goto_3

    .line 148
    :cond_6
    iget v0, p0, Lcom/sec/pcw/service/b/a;->a:I

    if-ge p3, v0, :cond_7

    :goto_6
    move v2, v1

    goto :goto_4

    :cond_7
    iget p3, p0, Lcom/sec/pcw/service/b/a;->a:I

    goto :goto_6

    .line 151
    :cond_8
    if-ltz v2, :cond_a

    .line 152
    add-int v0, v2, p3

    if-lez v0, :cond_9

    add-int v0, v2, p3

    :goto_7
    move v7, v2

    move v2, v0

    move v0, v7

    :goto_8
    move p3, v0

    .line 155
    goto :goto_4

    :cond_9
    move v0, v1

    .line 152
    goto :goto_7

    .line 155
    :cond_a
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v2, p0, Lcom/sec/pcw/service/b/a;->a:I

    if-ge v0, v2, :cond_b

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    move v2, v1

    goto :goto_8

    :cond_b
    iget v0, p0, Lcom/sec/pcw/service/b/a;->a:I

    move v2, v1

    goto :goto_8

    .line 167
    :cond_c
    if-nez v2, :cond_e

    move v0, v3

    .line 171
    :goto_9
    iget v2, p0, Lcom/sec/pcw/service/b/a;->a:I

    if-ne p3, v2, :cond_d

    .line 175
    :goto_a
    invoke-direct {p0, v5, v0, v3}, Lcom/sec/pcw/service/b/a;->a(Lorg/json/JSONArray;ZZ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_d
    move v3, v1

    goto :goto_a

    :cond_e
    move v0, v1

    goto :goto_9
.end method

.method private a(Lorg/json/JSONArray;ZZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 214
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 216
    :try_start_0
    const-string v0, "albums"

    invoke-virtual {v1, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 217
    const-string v0, "totalCount"

    iget v2, p0, Lcom/sec/pcw/service/b/a;->a:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 218
    const-string v0, "totalSongCount"

    iget v2, p0, Lcom/sec/pcw/service/b/a;->b:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 219
    const-string v0, "isFirstpage"

    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 220
    const-string v0, "isLastpage"

    invoke-virtual {v1, v0, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 221
    const-string v0, "mediaScan"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 222
    invoke-static {v1}, Lcom/sec/pcw/util/Common;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 223
    :catch_0
    move-exception v0

    .line 224
    sget-object v2, Lcom/sec/pcw/service/b/a;->d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 225
    const-string v2, "mfl_APIGetAlbumListById"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::toJSONStringGetAlbumList:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Landroid/database/Cursor;I)Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 188
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 190
    :try_start_0
    const-string v0, "index"

    invoke-virtual {v1, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 191
    const-string v0, "id"

    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 192
    const-string v0, "title"

    const-string v2, "album"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 193
    const-string v0, "artist"

    const-string v2, "artist"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 194
    const-string v0, "numOfSongs"

    const-string v2, "numsongs"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 195
    const-string v0, "thumbnailUri"

    const-string v2, "album_art"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mfluent/asp/dws/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 196
    const-string v0, "album"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/pcw/util/LanguageUtil;->b(Ljava/lang/String;)C

    move-result v0

    .line 197
    const-string v2, "indexLetter"

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :cond_0
    :goto_0
    return-object v1

    .line 198
    :catch_0
    move-exception v0

    .line 199
    sget-object v2, Lcom/sec/pcw/service/b/a;->d:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 200
    const-string v2, "mfl_APIGetAlbumListById"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::buildAlbum:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IIILjava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 66
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 68
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 72
    :try_start_0
    invoke-static {p3}, Lcom/sec/pcw/util/Common;->b(I)Ljava/lang/String;

    move-result-object v5

    .line 74
    if-nez p4, :cond_1

    .line 75
    sget-object v1, Lcom/sec/pcw/util/Common;->b:Landroid/net/Uri;

    sget-object v2, Lcom/sec/pcw/util/Common;->i:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 93
    :goto_0
    :try_start_1
    invoke-direct {p0, v1, p1, p2, v5}, Lcom/sec/pcw/service/b/a;->a(Landroid/database/Cursor;IILjava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 95
    if-eqz v1, :cond_0

    .line 96
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v0

    .line 77
    :cond_1
    :try_start_2
    new-instance v2, Lcom/sec/pcw/util/f;

    const/4 v7, 0x0

    invoke-direct {v2, p4, v7}, Lcom/sec/pcw/util/f;-><init>(Ljava/lang/String;Z)V

    .line 78
    invoke-virtual {v2}, Lcom/sec/pcw/util/f;->a()[Ljava/lang/String;

    move-result-object v2

    .line 79
    :goto_1
    array-length v7, v2

    if-ge v1, v7, :cond_3

    .line 80
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 81
    const-string v7, " OR "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    :cond_2
    const-string v7, "album LIKE ? ESCAPE \'\\\'"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "%"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v8, v2, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 86
    :cond_3
    sget-object v1, Lcom/sec/pcw/util/Common;->b:Landroid/net/Uri;

    sget-object v2, Lcom/sec/pcw/util/Common;->i:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_4

    .line 96
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 95
    :catchall_1
    move-exception v0

    goto :goto_2
.end method
