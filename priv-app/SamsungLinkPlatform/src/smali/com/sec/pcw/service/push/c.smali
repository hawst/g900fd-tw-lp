.class public final Lcom/sec/pcw/service/push/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/service/push/c$a;,
        Lcom/sec/pcw/service/push/c$b;
    }
.end annotation


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

.field private static b:Lcom/sec/pcw/service/push/c$b;

.field private static c:Ljava/util/concurrent/Semaphore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/service/push/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    sget-object v0, Lcom/sec/pcw/service/push/c;->b:Lcom/sec/pcw/service/push/c$b;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lcom/sec/pcw/service/push/c$b;

    const-string v1, "pcw.db"

    invoke-direct {v0, p1, v1}, Lcom/sec/pcw/service/push/c$b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/pcw/service/push/c;->b:Lcom/sec/pcw/service/push/c$b;

    .line 115
    :cond_0
    sget-object v0, Lcom/sec/pcw/service/push/c;->c:Ljava/util/concurrent/Semaphore;

    if-nez v0, :cond_1

    .line 116
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v2, v2}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    sput-object v0, Lcom/sec/pcw/service/push/c;->c:Ljava/util/concurrent/Semaphore;

    .line 118
    :cond_1
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 349
    .line 350
    const/4 v10, -0x1

    .line 352
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "delivery_report"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v3, v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "_id ASC"

    const-string v9, "1"

    move-object v0, p0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 354
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 355
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 361
    :goto_0
    if-eqz v1, :cond_0

    .line 362
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 366
    :cond_0
    :goto_1
    return v0

    .line 358
    :catch_0
    move-exception v0

    move-object v1, v11

    .line 359
    :goto_2
    :try_start_2
    const-string v2, "mfl_PushDBAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[selectOldestReportID] - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 361
    if-eqz v1, :cond_2

    .line 362
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v10

    goto :goto_1

    .line 361
    :catchall_0
    move-exception v0

    move-object v1, v11

    :goto_3
    if-eqz v1, :cond_1

    .line 362
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 361
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 358
    :catch_1
    move-exception v0

    goto :goto_2

    :cond_2
    move v0, v10

    goto :goto_1

    :cond_3
    move v0, v10

    goto :goto_0
.end method

.method private static a(Z)Landroid/database/sqlite/SQLiteDatabase;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 127
    .line 129
    sget-object v1, Lcom/sec/pcw/service/push/c;->c:Ljava/util/concurrent/Semaphore;

    if-eqz v1, :cond_0

    .line 131
    :try_start_0
    const-string v1, "mfl_PushDBAdapter"

    const-string v2, "dblock acquire - before"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    sget-object v1, Lcom/sec/pcw/service/push/c;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 134
    sget-object v1, Lcom/sec/pcw/service/push/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_0

    .line 135
    const-string v1, "mfl_PushDBAdapter"

    const-string v2, "::open:>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :cond_0
    sget-object v1, Lcom/sec/pcw/service/push/c;->b:Lcom/sec/pcw/service/push/c$b;

    if-eqz v1, :cond_1

    .line 148
    const/4 v1, 0x1

    if-ne v1, p0, :cond_2

    .line 149
    :try_start_1
    sget-object v1, Lcom/sec/pcw/service/push/c;->b:Lcom/sec/pcw/service/push/c$b;

    invoke-virtual {v1}, Lcom/sec/pcw/service/push/c$b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 158
    :cond_1
    :goto_0
    return-object v0

    .line 138
    :catch_0
    move-exception v1

    const-string v1, "mfl_PushDBAdapter"

    const-string v2, " InterruptedException is thrown and the current thread\'s interrupted status is cleared"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 151
    :cond_2
    :try_start_2
    sget-object v1, Lcom/sec/pcw/service/push/c;->b:Lcom/sec/pcw/service/push/c$b;

    invoke-virtual {v1}, Lcom/sec/pcw/service/push/c$b;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto :goto_0

    .line 154
    :catch_1
    move-exception v1

    const-string v1, "mfl_PushDBAdapter"

    const-string v2, "[Failed] open db"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/pcw/service/push/c$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v11, 0x0

    .line 295
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 298
    invoke-static {v0}, Lcom/sec/pcw/service/push/c;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 300
    if-nez v0, :cond_0

    move-object v0, v10

    .line 335
    :goto_0
    return-object v0

    .line 305
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "delivery_report"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "created_date"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "report"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "action_code"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 316
    if-eqz v2, :cond_3

    .line 317
    :goto_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 318
    const-string v1, "report"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 319
    const-string v3, "action_code"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 320
    new-instance v4, Lcom/sec/pcw/service/push/c$a;

    invoke-direct {v4, v1, v3}, Lcom/sec/pcw/service/push/c$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 323
    :catch_0
    move-exception v1

    .line 324
    :goto_2
    :try_start_2
    const-string v3, "mfl_PushDBAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[selectReport] - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 326
    if-eqz v2, :cond_1

    .line 327
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 330
    :cond_1
    if-eqz v0, :cond_2

    .line 331
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    :cond_2
    :goto_3
    move-object v0, v10

    .line 335
    goto :goto_0

    .line 326
    :cond_3
    if-eqz v2, :cond_4

    .line 327
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 330
    :cond_4
    if-eqz v0, :cond_2

    .line 331
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    goto :goto_3

    .line 326
    :catchall_0
    move-exception v1

    move-object v2, v11

    :goto_4
    if-eqz v2, :cond_5

    .line 327
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 330
    :cond_5
    if-eqz v0, :cond_6

    .line 331
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    :cond_6
    throw v1

    .line 326
    :catchall_1
    move-exception v1

    goto :goto_4

    .line 323
    :catch_1
    move-exception v1

    move-object v2, v11

    goto :goto_2
.end method

.method public static a(Lcom/sec/pcw/service/push/c$a;)Z
    .locals 11

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 200
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/sec/pcw/service/push/c$a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 240
    :cond_0
    :goto_0
    return v0

    .line 203
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 204
    const-string v3, "created_date"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 205
    const-string v3, "report"

    invoke-virtual {p0}, Lcom/sec/pcw/service/push/c$a;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v3, "action_code"

    invoke-virtual {p0}, Lcom/sec/pcw/service/push/c$a;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-static {v1}, Lcom/sec/pcw/service/push/c;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 211
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 215
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 217
    :try_start_0
    invoke-static {v6}, Lcom/sec/pcw/service/push/c;->b(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v3

    .line 219
    const/16 v7, 0x13

    if-ge v7, v3, :cond_2

    .line 220
    invoke-static {v6}, Lcom/sec/pcw/service/push/c;->a(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v3

    const-string v7, "delivery_report"

    const-string v8, "_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v10

    invoke-virtual {v6, v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 223
    :cond_2
    const-string v3, "delivery_report"

    const/4 v7, 0x0

    invoke-virtual {v6, v3, v7, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 225
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v7

    if-ne v1, v7, :cond_3

    .line 231
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 232
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    .line 237
    :cond_3
    :goto_1
    cmp-long v2, v4, v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 240
    goto :goto_0

    .line 228
    :catch_0
    move-exception v2

    .line 230
    if-eqz v6, :cond_5

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-ne v1, v2, :cond_5

    .line 231
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 232
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    move-wide v2, v4

    goto :goto_1

    .line 230
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-ne v1, v2, :cond_4

    .line 231
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 232
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    :cond_4
    throw v0

    :cond_5
    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 255
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    :cond_0
    :goto_0
    return v9

    .line 261
    :cond_1
    invoke-static {v9}, Lcom/sec/pcw/service/push/c;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 263
    if-eqz v0, :cond_0

    .line 268
    :try_start_0
    const-string v1, "delivery_report"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "report"

    aput-object v4, v2, v3

    const-string v3, "report=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 270
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-eqz v1, :cond_4

    move v1, v8

    .line 274
    :goto_1
    if-eqz v2, :cond_2

    .line 275
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_2
    if-eqz v0, :cond_3

    .line 280
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    :cond_3
    :goto_2
    move v9, v1

    .line 284
    goto :goto_0

    :cond_4
    move v1, v9

    .line 270
    goto :goto_1

    .line 271
    :catch_0
    move-exception v1

    move-object v2, v10

    .line 272
    :goto_3
    :try_start_2
    const-string v3, "mfl_PushDBAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[isExistReport] - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 274
    if-eqz v2, :cond_5

    .line 275
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_5
    if-eqz v0, :cond_8

    .line 280
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    move v1, v9

    goto :goto_2

    .line 274
    :catchall_0
    move-exception v1

    move-object v2, v10

    :goto_4
    if-eqz v2, :cond_6

    .line 275
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_6
    if-eqz v0, :cond_7

    .line 280
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    :cond_7
    throw v1

    .line 274
    :catchall_1
    move-exception v1

    goto :goto_4

    .line 271
    :catch_1
    move-exception v1

    goto :goto_3

    :cond_8
    move v1, v9

    goto :goto_2
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 377
    .line 381
    :try_start_0
    const-string v1, "delivery_report"

    const/4 v0, 0x0

    new-array v2, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "_id ASC"

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 383
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 387
    if-eqz v1, :cond_0

    .line 388
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 392
    :cond_0
    :goto_0
    return v0

    .line 384
    :catch_0
    move-exception v0

    move-object v1, v9

    .line 385
    :goto_1
    :try_start_2
    const-string v2, "mfl_PushDBAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[selectCount] - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387
    if-eqz v1, :cond_2

    .line 388
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v8

    goto :goto_0

    .line 387
    :catchall_0
    move-exception v0

    move-object v1, v9

    :goto_2
    if-eqz v1, :cond_1

    .line 388
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 387
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 384
    :catch_1
    move-exception v0

    goto :goto_1

    :cond_2
    move v0, v8

    goto :goto_0
.end method

.method private static b()V
    .locals 2

    .prologue
    .line 166
    :try_start_0
    sget-object v0, Lcom/sec/pcw/service/push/c;->b:Lcom/sec/pcw/service/push/c$b;

    if-eqz v0, :cond_0

    .line 167
    sget-object v0, Lcom/sec/pcw/service/push/c;->b:Lcom/sec/pcw/service/push/c$b;

    invoke-virtual {v0}, Lcom/sec/pcw/service/push/c$b;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/pcw/service/push/c;->c:Ljava/util/concurrent/Semaphore;

    if-eqz v0, :cond_2

    .line 175
    sget-object v0, Lcom/sec/pcw/service/push/c;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_1

    .line 176
    const-string v0, "mfl_PushDBAdapter"

    const-string v1, "::close:<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_1
    sget-object v0, Lcom/sec/pcw/service/push/c;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 179
    const-string v0, "mfl_PushDBAdapter"

    const-string v1, "dblock release - after"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_2
    return-void

    .line 170
    :catch_0
    move-exception v0

    const-string v0, "mfl_PushDBAdapter"

    const-string v1, "[Failed] close db"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 404
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 426
    :cond_0
    :goto_0
    return v1

    .line 410
    :cond_1
    invoke-static {v0}, Lcom/sec/pcw/service/push/c;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 412
    if-eqz v2, :cond_0

    .line 417
    :try_start_0
    const-string v3, "delivery_report"

    const-string v4, "report=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-lez v3, :cond_3

    .line 421
    :goto_1
    if-eqz v2, :cond_2

    .line 422
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    :cond_2
    :goto_2
    move v1, v0

    .line 426
    goto :goto_0

    :cond_3
    move v0, v1

    .line 417
    goto :goto_1

    .line 418
    :catch_0
    move-exception v0

    .line 419
    :try_start_1
    const-string v3, "mfl_PushDBAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[deleteReport] - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 421
    if-eqz v2, :cond_5

    .line 422
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    move v0, v1

    goto :goto_2

    .line 421
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_4

    .line 422
    invoke-static {}, Lcom/sec/pcw/service/push/c;->b()V

    :cond_4
    throw v0

    :cond_5
    move v0, v1

    goto :goto_2
.end method
