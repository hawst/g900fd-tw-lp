.class public final Lcom/sec/pcw/service/push/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/service/push/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    .line 115
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "com.sec.pcw.push.PushMsgHandler"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/service/push/d;->c:Landroid/os/PowerManager$WakeLock;

    .line 116
    return-void
.end method

.method static synthetic a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    const v4, 0x1d4c0

    const/4 v3, 0x0

    .line 174
    const-string v0, "mfl_PushMsgHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[requestPushRegistrationID] - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    const-string v0, "SPP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178
    const-string v0, "com.samsung.android.sdk.samsunglink.SPP_REGISTRATION_RETRY"

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "[requestPushRegistrationID] - spp registration is in progress, so ignore action!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.SPP_REGISTRATION_RETRY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, v4}, Lcom/sec/pcw/service/push/util/c;->a(Landroid/content/Context;Landroid/content/Intent;I)I

    .line 182
    invoke-static {p0}, Lcom/sec/pcw/service/push/spp/a;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 184
    :cond_2
    const-string v0, "GCM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 185
    invoke-static {p0}, Lcom/sec/pcw/util/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/sec/pcw/service/push/util/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    :cond_3
    const-string v0, "com.samsung.android.sdk.samsunglink.GCM_REGISTRATION_RETRY"

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 187
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "[requestPushRegistrationID] - gcm registration is in progress, so ignore action!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 189
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.GCM_REGISTRATION_RETRY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, v4}, Lcom/sec/pcw/service/push/util/c;->a(Landroid/content/Context;Landroid/content/Intent;I)I

    .line 190
    const-string v0, "GCMHandler"

    const-string v1, "certifyGCM: 541000151311"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.c2dm.intent.REGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "app"

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {p0, v3, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "sender"

    const-string v2, "541000151311"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 193
    :cond_5
    const-string v0, "C2DM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    invoke-static {p0}, Lcom/sec/pcw/util/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    const-string v0, "com.samsung.android.sdk.samsunglink.GCM_REGISTRATION_RETRY"

    invoke-static {v0}, Lcom/sec/pcw/service/push/util/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 196
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "[requestPushRegistrationID] - c2dm registration is in progress, so ignore action!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 199
    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.sdk.samsunglink.C2DM_REGISTRATION_RETRY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, v4}, Lcom/sec/pcw/service/push/util/c;->a(Landroid/content/Context;Landroid/content/Intent;I)I

    .line 200
    const-string v0, "C2DMHandler"

    const-string v1, "certifyC2DM: push.samsung@gmail.com"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.c2dm.intent.REGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "app"

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {p0, v3, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "sender"

    const-string v2, "push.samsung@gmail.com"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 218
    const-string v0, "mfl_PushMsgHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[deregisterPushRegistrationID] - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    const-string v0, "SPP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    invoke-static {p0}, Lcom/sec/pcw/service/push/spp/a;->b(Landroid/content/Context;)V

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    const-string v0, "GCM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 224
    const-string v0, "GCMHandler"

    const-string v1, "decertifyGCM"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "app"

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {p0, v3, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 225
    :cond_2
    const-string v0, "C2DM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const-string v0, "C2DMHandler"

    const-string v1, "decertifyC2DM"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "app"

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {p0, v3, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method static synthetic c()Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/pcw/service/push/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 922
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->c:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 923
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 925
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 849
    const-string v0, "Enter ::loginToPresence()"

    sget-object v1, Lcom/sec/pcw/service/push/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_0

    const-string v1, "mfl_PushMsgHandler"

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    :cond_0
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    .line 854
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/datamodel/t;->a(Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;)Ljava/util/List;

    move-result-object v0

    .line 855
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 857
    :cond_1
    sget-object v0, Lcom/sec/pcw/service/push/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_2

    .line 858
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "Need to get the latest device list since there must be new device in this Samsung account who sent me push"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    :cond_2
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.DataModel.DEVICE_LIST_REFRESH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 865
    :cond_3
    new-instance v0, Lcom/sec/pcw/service/push/b;

    iget-object v1, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/pcw/service/push/b;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/pcw/service/push/b;->a(Landroid/content/Context;)Z

    .line 870
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lpcloud/net/nat/c;->a(ZZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 876
    :cond_4
    :goto_0
    return-void

    .line 871
    :catch_0
    move-exception v0

    .line 872
    sget-object v1, Lcom/sec/pcw/service/push/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x5

    if-gt v1, v2, :cond_4

    .line 873
    const-string v1, "mfl_PushMsgHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::loginToPresence: Exception while initializing NTS: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/sec/pcw/service/push/c$a;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 707
    invoke-direct {p0}, Lcom/sec/pcw/service/push/d;->d()V

    .line 708
    new-instance v0, Lcom/sec/pcw/service/push/a;

    iget-object v1, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/pcw/service/push/a;-><init>(Landroid/content/Context;)V

    .line 710
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/pcw/service/push/d$3;

    invoke-direct {v2, p0, p1, v0, p2}, Lcom/sec/pcw/service/push/d$3;-><init>(Lcom/sec/pcw/service/push/d;Lcom/sec/pcw/service/push/c$a;Lcom/sec/pcw/service/push/a;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 844
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/16 v8, 0x10

    .line 593
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "&apos;"

    const-string v1, "\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&quot;"

    const-string v2, "\""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&gt;"

    const-string v2, ">"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&lt;"

    const-string v2, "<"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&amp;"

    const-string v2, "&"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 594
    :cond_0
    invoke-static {p1}, Lcom/sec/pcw/util/h;->b(Ljava/lang/String;)Lcom/sec/pcw/service/push/util/a;

    move-result-object v1

    .line 596
    const-string v2, "mfl_PushMsgHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "[handleAction] actionData = "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/pcw/service/push/util/a;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/sdk/samsung/c;->a(Landroid/content/Context;)Lcom/samsung/android/sdk/samsung/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsung/c;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 599
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "[handleAction] marketing push is disabled. ignore action.n"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    :cond_1
    :goto_1
    return-void

    .line 596
    :cond_2
    const-string v0, "<null>"

    goto :goto_0

    .line 603
    :cond_3
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/sec/pcw/service/push/util/a;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 604
    const-string v0, "mfl_PushMsgHandler"

    const-string v2, "[ok] valid action data!!"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 608
    invoke-virtual {v1}, Lcom/sec/pcw/service/push/util/a;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 625
    :cond_4
    :goto_2
    invoke-virtual {v1}, Lcom/sec/pcw/service/push/util/a;->c()Ljava/lang/String;

    move-result-object v0

    const-string v3, "UTF-8"

    invoke-static {v0, v3}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 626
    invoke-virtual {v1}, Lcom/sec/pcw/service/push/util/a;->d()Ljava/lang/String;

    move-result-object v0

    .line 627
    if-eqz v0, :cond_8

    .line 628
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 631
    :goto_3
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    const/4 v4, 0x0

    const/high16 v5, 0x8000000

    invoke-static {v0, v4, v2, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 633
    iget-object v4, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    const-string v0, "notification"

    invoke-virtual {v4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    new-instance v5, Landroid/app/Notification;

    invoke-direct {v5}, Landroid/app/Notification;-><init>()V

    const v6, 0x7f020104

    iput v6, v5, Landroid/app/Notification;->icon:I

    const/16 v6, 0x10

    iput v6, v5, Landroid/app/Notification;->flags:I

    iput-object v3, v5, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    iput-object v2, v5, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f030029

    invoke-direct {v2, v6, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v2, v5, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    iget-object v2, v5, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v6, 0x7f0900c7

    const v7, 0x7f020104

    invoke-virtual {v2, v6, v7}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    iget-object v2, v5, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v6, 0x7f0900c8

    invoke-virtual {v2, v6, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    if-eqz v1, :cond_5

    iget-object v2, v5, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    const v6, 0x7f0900c9

    invoke-virtual {v2, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :cond_5
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v8, :cond_6

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v6, 0x7f03002a

    invoke-direct {v2, v4, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v4, 0x7f0900c8

    invoke-virtual {v2, v4, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v3, 0x7f0900c9

    invoke-virtual {v2, v3, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v1, 0x7f0900c7

    const v3, 0x7f020104

    invoke-virtual {v2, v1, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    iput-object v2, v5, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    :cond_6
    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 637
    :catch_0
    move-exception v0

    .line 638
    sget-object v1, Lcom/sec/pcw/service/push/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_1

    .line 639
    const-string v1, "mfl_PushMsgHandler"

    const-string v2, "::handleAction:"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 611
    :pswitch_0
    :try_start_1
    const-string v0, "com.mfluent.asp.ui.MobileChargesNotificationActivity"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 612
    const/high16 v0, 0x30000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_2

    .line 616
    :pswitch_1
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 617
    invoke-virtual {v1}, Lcom/sec/pcw/service/push/util/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 619
    invoke-virtual {v1}, Lcom/sec/pcw/service/push/util/a;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "allshareplay://"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 620
    const/high16 v0, 0x30000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_2

    .line 635
    :cond_7
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "[ignored] invalid action data!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :cond_8
    move-object v1, v0

    goto/16 :goto_3

    .line 608
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 129
    const-string v0, "mfl_PushMsgHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "][handleRegistrationID]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 135
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lcom/mfluent/asp/a;->a()Lcom/sec/pcw/hybrid/b/b;

    move-result-object v3

    .line 138
    const/4 v0, 0x0

    .line 139
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 142
    :cond_0
    iget-object v3, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/pcw/util/a;->b(Landroid/content/Context;)Z

    move-result v3

    .line 144
    const-string v4, ""

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 146
    iget-object v2, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-static {v2, p1, p2}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 160
    :cond_1
    :goto_0
    if-eqz v3, :cond_2

    .line 161
    if-eqz v0, :cond_5

    .line 162
    const-string v0, "mfl_PushMsgHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]hasSamungAccount & hasSavedToken"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-virtual {p0, p2}, Lcom/sec/pcw/service/push/d;->b(Ljava/lang/String;)V

    .line 170
    :cond_2
    :goto_1
    return-void

    .line 147
    :cond_3
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 149
    const-string v4, "mfl_PushMsgHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]Registration ID has been reissued"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    sget-object v4, Lcom/sec/pcw/service/push/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v4}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v4

    const/4 v5, 0x2

    if-gt v4, v5, :cond_4

    .line 151
    const-string v4, "mfl_PushMsgHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::handleRegistrationID:["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]old Registration ID: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v2, "mfl_PushMsgHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::handleRegistrationID:["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]new Registration ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :cond_4
    iget-object v2, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-static {v2, p1, p2}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 166
    :cond_5
    const-string v0, "mfl_PushMsgHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]Need to have token. will be registered when we get token"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;I)Z

    goto/16 :goto_1
.end method

.method final b()V
    .locals 1

    .prologue
    .line 928
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->c:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 929
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 931
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 650
    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;I)Z

    .line 652
    const-string v0, "mfl_PushMsgHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "][dispatchMGRegistration]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    new-instance v0, Lcom/sec/pcw/service/push/a;

    iget-object v1, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/pcw/service/push/a;-><init>(Landroid/content/Context;)V

    .line 655
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/pcw/service/push/d$2;

    invoke-direct {v2, p0, v0, p1}, Lcom/sec/pcw/service/push/d$2;-><init>(Lcom/sec/pcw/service/push/d;Lcom/sec/pcw/service/push/a;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 682
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    .prologue
    const/4 v11, 0x4

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x3

    .line 249
    sget-object v0, Lcom/sec/pcw/service/push/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v9, :cond_0

    .line 250
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "[handlePushMsg] - Push Message."

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    :cond_0
    sget-object v0, Lcom/sec/pcw/service/push/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v10, :cond_1

    .line 253
    const-string v0, "mfl_PushMsgHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[handlePushMsg] - Push Message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_1
    const-string v0, "mfl_PushMsgHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Push type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v0, ""

    .line 260
    const-string v1, "CON"

    .line 263
    const-string v2, "|"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 264
    const-string v0, "\\|"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 266
    aget-object v2, v3, v5

    .line 267
    const-string v0, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 268
    const-string v0, "RCV-0200"

    .line 269
    const-string v3, "mfl_PushMsgHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]Message ID is empty."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v12, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v12

    .line 313
    :goto_0
    const-string v3, "mfl_PushMsgHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Enter ::shouldLoginToPresence() : actionCode:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "AUL"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "NTF"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "DLR"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    :cond_2
    move v3, v5

    :goto_1
    if-eqz v3, :cond_3

    .line 314
    invoke-direct {p0}, Lcom/sec/pcw/service/push/d;->d()V

    .line 315
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/pcw/service/push/d$1;

    invoke-direct {v4, p0}, Lcom/sec/pcw/service/push/d$1;-><init>(Lcom/sec/pcw/service/push/d;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 331
    :cond_3
    const-string v3, "mfl_PushMsgHandler"

    const-string v4, "[dispatchDeliveryReport]"

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/sec/pcw/service/push/a;

    iget-object v4, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/pcw/service/push/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1, v2, p2}, Lcom/sec/pcw/service/push/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/pcw/service/push/c$a;

    invoke-direct {v2, v1, v0}, Lcom/sec/pcw/service/push/c$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-static {v0, v10}, Lcom/sec/pcw/service/push/util/b;->a(Landroid/content/Context;I)Z

    iget-object v0, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    const-string v1, "asp_push_pref_15"

    invoke-virtual {v0, v1, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "delivery_report_body"

    invoke-virtual {v2}, Lcom/sec/pcw/service/push/c$a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "delivery_report_action_code"

    invoke-virtual {v2}, Lcom/sec/pcw/service/push/c$a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/mfluent/asp/util/b;->a(Landroid/content/SharedPreferences$Editor;)Z

    invoke-virtual {p0, v2, p2}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/c$a;Ljava/lang/String;)V

    .line 332
    return-void

    .line 271
    :cond_4
    const/16 v0, 0x1e

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v0, v6, :cond_1c

    .line 272
    aget-object v6, v3, v4

    .line 274
    const-string v0, "mfl_PushMsgHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Push: clientType = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v0, ""

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 278
    const-string v0, "RCV-0200"

    .line 279
    const-string v3, "mfl_PushMsgHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]Client Type is empty."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v12, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v12

    goto/16 :goto_0

    .line 282
    :cond_5
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v10, v0, :cond_1b

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v9, v0, :cond_1b

    .line 283
    const-string v0, "DM"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 284
    const-string v0, "mfl_PushMsgHandler"

    const-string v6, "Handling DM Msg"

    invoke-static {v0, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string v0, "mfl_PushMsgHandler"

    const-string v6, "[handleDMMsg]"

    invoke-static {v0, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "RCV-0000"

    array-length v6, v3

    if-ne v9, v6, :cond_7

    aget-object v3, v3, v10

    const-string v6, ""

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v0, "RCV-0200"

    const-string v3, "mfl_PushMsgHandler"

    const-string v6, "DM Message is empty"

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    move-object v12, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v12

    goto/16 :goto_0

    :cond_6
    const-string v6, "mfl_PushMsgHandler"

    const-string v7, "[callDMClient]"

    invoke-static {v6, v7}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.IP_PUSH_DM_NOTI_RECEIVED"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :try_start_0
    const-string v7, "pdus"

    const-string v8, "UTF8"

    invoke-virtual {v3, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    iget-object v3, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-virtual {v3, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_2

    :catch_0
    move-exception v3

    const-string v7, "mfl_PushMsgHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[callDMClient] "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    const-string v0, "RCV-0203"

    const-string v6, "mfl_PushMsgHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "DM Protocol length is incorrect. Length: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 286
    :cond_8
    const-string v0, "DS"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 287
    const-string v0, "mfl_PushMsgHandler"

    const-string v6, "Handling DS Msg"

    invoke-static {v0, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const-string v0, "mfl_PushMsgHandler"

    const-string v6, "[handleDSMsg]"

    invoke-static {v0, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "RCV-0000"

    const/4 v6, 0x5

    array-length v7, v3

    if-ne v6, v7, :cond_f

    aget-object v6, v3, v9

    const-string v7, ""

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    const-string v0, "RCV-0200"

    const-string v3, "mfl_PushMsgHandler"

    const-string v6, "DS Action Code is empty.l"

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    move-object v12, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v12

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v9, v7, :cond_e

    const-string v7, "PAY"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, "P01"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, "M01"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, "C01"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    :cond_a
    aget-object v3, v3, v11

    const-string v7, ""

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    const-string v0, "RCV-0200"

    const-string v3, "mfl_PushMsgHandler"

    const-string v6, "DS Message is empty."

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_b
    const-string v7, "mfl_PushMsgHandler"

    const-string v8, "[callDSClient]"

    invoke-static {v7, v8}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Landroid/content/Intent;

    const-string v8, "android.intent.action.IP_PUSH_DS_NOTI_RECEIVED"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v8, "PAY"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    :try_start_1
    const-string v6, "push_message"

    const-string v8, "UTF8"

    invoke-virtual {v3, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v7, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_5
    iget-object v3, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_4

    :catch_1
    move-exception v3

    const-string v6, "mfl_PushMsgHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[callDSClient] "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_c
    const-string v6, "push_message"

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v7, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_5

    :cond_d
    const-string v0, "RCV-0201"

    const-string v3, "mfl_PushMsgHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Action code is incorrect. Action code: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_e
    const-string v0, "RCV-0202"

    const-string v3, "mfl_PushMsgHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "DS ActionCode length is incorrect. Length: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_f
    const-string v0, "RCV-0203"

    const-string v6, "mfl_PushMsgHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "DS Protocol length is incorrect. Length: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 289
    :cond_10
    const-string v0, "WS"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 290
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "Handling WS Msg"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "[handleWSMsg]"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    array-length v0, v3

    if-ne v9, v0, :cond_19

    aget-object v1, v3, v10

    const-string v0, "mfl_PushMsgHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "handleWSMsg: actionCode = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "RCV-0200"

    const-string v1, "mfl_PushMsgHandler"

    const-string v6, "SV Action Code is empty"

    invoke-static {v1, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    :cond_11
    :goto_6
    aget-object v1, v3, v10

    move-object v12, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v12

    goto/16 :goto_0

    .line 291
    :cond_12
    const-string v0, "CON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "WS|CON"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "RCV-0000"

    goto :goto_6

    :cond_13
    const-string v0, "DLR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "WS|DLR"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "RCV-0000"

    sget-object v1, Lcom/sec/pcw/service/push/d;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    if-gt v1, v9, :cond_14

    const-string v1, "mfl_PushMsgHandler"

    const-string v6, "Need to send device list sync request"

    invoke-static {v1, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "mfl_PushMsgHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Checking the context : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_14
    iget-object v1, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/sec/pcw/service/push/d;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->a(Landroid/content/Context;)Lcom/mfluent/asp/sync/DeviceListSyncManager;

    move-result-object v1

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.mfluent.asp.sync.DeviceListSyncManager.DEVICE_LIST_REQUEST_VIA_PUSH"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lcom/mfluent/asp/sync/DeviceListSyncManager;->f(Landroid/content/Intent;)V

    goto :goto_6

    :cond_15
    const-string v0, "SMS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    const-string v0, "RCV-0000"

    goto :goto_6

    :cond_16
    const-string v0, "AUL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "WS|AUL"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "RCV-0000"

    goto/16 :goto_6

    :cond_17
    const-string v0, "NTF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    const-string v0, "mfl_PushMsgHandler"

    const-string v1, "WS|NTF"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "RCV-0000"

    goto/16 :goto_6

    :cond_18
    const-string v0, "RCV-0201"

    const-string v6, "mfl_PushMsgHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Action code is incorrect. Action code: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_19
    const-string v0, "RCV-0203"

    const-string v1, "mfl_PushMsgHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SV Protocol length is incorrect. Length: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v7, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 295
    :cond_1a
    const-string v0, "RCV-0201"

    .line 296
    const-string v3, "mfl_PushMsgHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]Client Type is incorrect. Client Type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v12, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v12

    goto/16 :goto_0

    .line 299
    :cond_1b
    const-string v0, "RCV-0202"

    .line 300
    const-string v3, "mfl_PushMsgHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]Client Type length is incorrect. Length: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v12, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v12

    goto/16 :goto_0

    .line 304
    :cond_1c
    const-string v0, "RCV-0202"

    .line 305
    const-string v3, "mfl_PushMsgHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]Message ID length is incorrect. Length: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v12, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v12

    .line 308
    goto/16 :goto_0

    .line 309
    :cond_1d
    const-string v2, "RCV-0204"

    .line 310
    const-string v3, "mfl_PushMsgHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]There is no delimiter."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto/16 :goto_0

    .line 313
    :cond_1e
    const-string v3, "mfl_PushMsgHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::shouldLoginToPresence() : actionCode:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " return true"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    goto/16 :goto_1
.end method
