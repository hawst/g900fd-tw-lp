.class public final Lcom/sec/pcw/service/b/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:Landroid/content/ContentResolver;

.field private c:I

.field private final d:Z

.field private final e:Lcom/sec/pcw/util/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/service/b/l;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/service/b/l;->b:Landroid/content/ContentResolver;

    .line 55
    iput v1, p0, Lcom/sec/pcw/service/b/l;->c:I

    .line 56
    iput-boolean v1, p0, Lcom/sec/pcw/service/b/l;->d:Z

    .line 58
    new-instance v0, Lcom/sec/pcw/util/d;

    invoke-direct {v0}, Lcom/sec/pcw/util/d;-><init>()V

    iput-object v0, p0, Lcom/sec/pcw/service/b/l;->e:Lcom/sec/pcw/util/d;

    return-void
.end method

.method private a(Landroid/database/Cursor;IILjava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 114
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 116
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 117
    :cond_0
    invoke-direct {p0, v5, v3, v3}, Lcom/sec/pcw/service/b/l;->a(Lorg/json/JSONArray;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 173
    :goto_0
    return-object v0

    .line 119
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/sec/pcw/service/b/l;->c:I

    .line 121
    const/4 v2, -0x1

    .line 123
    invoke-static {p1, p4}, Lcom/sec/pcw/util/LanguageUtil;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 126
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v6

    .line 128
    :goto_1
    invoke-interface {v6}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    invoke-interface {v6}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 131
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, p2, :cond_3

    .line 132
    invoke-interface {v6}, Ljava/util/ListIterator;->nextIndex()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    .line 138
    :cond_2
    if-lez p3, :cond_7

    .line 140
    if-ltz v2, :cond_5

    .line 141
    add-int/lit8 v2, v2, 0x1

    .line 142
    add-int v0, v2, p3

    iget v6, p0, Lcom/sec/pcw/service/b/l;->c:I

    if-ge v0, v6, :cond_4

    add-int v0, v2, p3

    :goto_2
    move p3, v0

    .line 155
    :goto_3
    iget-object v0, p0, Lcom/sec/pcw/service/b/l;->b:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/sec/pcw/service/b/j;->a(Landroid/content/ContentResolver;)Landroid/util/SparseArray;

    move-result-object v6

    .line 156
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v7

    move v4, v2

    .line 157
    :goto_4
    if-ge v4, p3, :cond_b

    .line 158
    invoke-interface {v7}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 159
    invoke-direct {p0, p1, v4, v6}, Lcom/sec/pcw/service/b/l;->a(Landroid/database/Cursor;ILandroid/util/SparseArray;)Lorg/json/JSONObject;

    move-result-object v0

    .line 160
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 161
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 157
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    .line 135
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 142
    :cond_4
    iget v0, p0, Lcom/sec/pcw/service/b/l;->c:I

    goto :goto_2

    .line 144
    :cond_5
    iget v0, p0, Lcom/sec/pcw/service/b/l;->c:I

    if-ge p3, v0, :cond_6

    :goto_5
    move v2, v1

    goto :goto_3

    :cond_6
    iget p3, p0, Lcom/sec/pcw/service/b/l;->c:I

    goto :goto_5

    .line 147
    :cond_7
    if-ltz v2, :cond_9

    .line 148
    add-int v0, v2, p3

    if-lez v0, :cond_8

    add-int v0, v2, p3

    :goto_6
    move v8, v2

    move v2, v0

    move v0, v8

    :goto_7
    move p3, v0

    .line 151
    goto :goto_3

    :cond_8
    move v0, v1

    .line 148
    goto :goto_6

    .line 151
    :cond_9
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v2, p0, Lcom/sec/pcw/service/b/l;->c:I

    if-ge v0, v2, :cond_a

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    move v2, v1

    goto :goto_7

    :cond_a
    iget v0, p0, Lcom/sec/pcw/service/b/l;->c:I

    move v2, v1

    goto :goto_7

    .line 165
    :cond_b
    if-nez v2, :cond_d

    move v0, v3

    .line 169
    :goto_8
    iget v2, p0, Lcom/sec/pcw/service/b/l;->c:I

    if-ne p3, v2, :cond_c

    .line 173
    :goto_9
    invoke-direct {p0, v5, v0, v3}, Lcom/sec/pcw/service/b/l;->a(Lorg/json/JSONArray;ZZ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_c
    move v3, v1

    goto :goto_9

    :cond_d
    move v0, v1

    goto :goto_8
.end method

.method private a(Lorg/json/JSONArray;ZZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 224
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 226
    :try_start_0
    const-string v0, "songs"

    invoke-virtual {v1, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 227
    const-string v0, "totalCount"

    iget v2, p0, Lcom/sec/pcw/service/b/l;->c:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 228
    const-string v0, "revision"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 229
    const-string v0, "isFirstpage"

    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 230
    const-string v0, "isLastpage"

    invoke-virtual {v1, v0, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 231
    :catch_0
    move-exception v0

    .line 232
    sget-object v2, Lcom/sec/pcw/service/b/l;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 233
    const-string v2, "mfl_APIGetSongListById"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::toJSONStringGetSongList:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;ILandroid/util/SparseArray;)Lorg/json/JSONObject;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "I",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .prologue
    .line 184
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 187
    :try_start_0
    const-string v0, "index"

    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 188
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 189
    const-string v2, "id"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 190
    const-string v2, "uri"

    const-string v3, "_data"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/pcw/service/d/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 191
    const-string v2, "songTitle"

    const-string v3, "title"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 192
    const-string v2, "artist"

    const-string v3, "artist"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 193
    const-string v2, "albumTitle"

    const-string v3, "album"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 194
    const-string v2, "playingTime"

    const-string v3, "duration"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 195
    const-string v2, "title"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/pcw/util/LanguageUtil;->b(Ljava/lang/String;)C

    move-result v2

    .line 196
    const-string v3, "indexLetter"

    invoke-static {v2}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 197
    const-string v2, "trackNo"

    const-string v3, "track"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    rem-int/lit16 v3, v3, 0x3e8

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 198
    const-string v2, "genre"

    iget-object v3, p0, Lcom/sec/pcw/service/b/l;->e:Lcom/sec/pcw/util/d;

    invoke-virtual {v3, v0}, Lcom/sec/pcw/util/d;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 199
    const-string v0, "date"

    const-string v2, "date_added"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 200
    const-string v0, "album_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 201
    const-string v2, "albumId"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 202
    const-string v2, "albumArt"

    invoke-virtual {p3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/mfluent/asp/dws/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 203
    const-string v0, "mimeType"

    const-string v2, "mime_type"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 204
    const-string v0, "size"

    const-string v2, "_size"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 205
    const-string v0, "displayName"

    const-string v2, "_display_name"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 206
    const-string v0, "mtime"

    const-string v2, "date_modified"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :cond_0
    :goto_0
    return-object v1

    .line 207
    :catch_0
    move-exception v0

    .line 208
    sget-object v2, Lcom/sec/pcw/service/b/l;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v2}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 209
    const-string v2, "mfl_APIGetSongListById"

    const-string v3, "::buildSong:"

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v2, "mfl_APIGetSongListById"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::buildSong:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IIILjava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 71
    .line 74
    :try_start_0
    invoke-static {p3}, Lcom/sec/pcw/util/Common;->a(I)Ljava/lang/String;

    move-result-object v5

    .line 75
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    const-string v1, "is_music = 1"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 78
    if-nez p4, :cond_1

    .line 79
    iget-object v0, p0, Lcom/sec/pcw/service/b/l;->b:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/pcw/util/Common;->c:Landroid/net/Uri;

    sget-object v2, Lcom/sec/pcw/util/Common;->j:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 104
    :goto_0
    :try_start_1
    invoke-direct {p0, v1, p1, p2, v5}, Lcom/sec/pcw/service/b/l;->a(Landroid/database/Cursor;IILjava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 106
    if-eqz v1, :cond_0

    .line 107
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v0

    .line 81
    :cond_1
    :try_start_2
    new-instance v1, Lcom/sec/pcw/util/f;

    const/4 v2, 0x0

    invoke-direct {v1, p4, v2}, Lcom/sec/pcw/util/f;-><init>(Ljava/lang/String;Z)V

    .line 82
    invoke-virtual {v1}, Lcom/sec/pcw/util/f;->a()[Ljava/lang/String;

    move-result-object v1

    .line 83
    const-string v2, " AND ("

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 85
    const-string v2, "title LIKE ? ESCAPE \'\\\' OR album LIKE ? ESCAPE \'\\\' OR artist LIKE ? ESCAPE \'\\\'"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "%"

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v1, v0

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "%"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 93
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "%"

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v1, v0

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "%"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 94
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "%"

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v1, v0

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "%"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 96
    :cond_2
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    iget-object v0, p0, Lcom/sec/pcw/service/b/l;->b:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/pcw/util/Common;->c:Landroid/net/Uri;

    sget-object v2, Lcom/sec/pcw/util/Common;->j:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto/16 :goto_0

    .line 106
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_3

    .line 107
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 106
    :catchall_1
    move-exception v0

    goto :goto_2
.end method
