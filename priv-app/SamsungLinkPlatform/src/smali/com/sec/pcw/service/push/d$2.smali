.class final Lcom/sec/pcw/service/push/d$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/pcw/service/push/d;->b(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/pcw/service/push/a;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/sec/pcw/service/push/d;


# direct methods
.method constructor <init>(Lcom/sec/pcw/service/push/d;Lcom/sec/pcw/service/push/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 655
    iput-object p1, p0, Lcom/sec/pcw/service/push/d$2;->c:Lcom/sec/pcw/service/push/d;

    iput-object p2, p0, Lcom/sec/pcw/service/push/d$2;->a:Lcom/sec/pcw/service/push/a;

    iput-object p3, p0, Lcom/sec/pcw/service/push/d$2;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 659
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$2;->a:Lcom/sec/pcw/service/push/a;

    iget-object v1, p0, Lcom/sec/pcw/service/push/d$2;->a:Lcom/sec/pcw/service/push/a;

    iget-object v2, p0, Lcom/sec/pcw/service/push/d$2;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/pcw/service/push/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/pcw/service/push/d$2;->c:Lcom/sec/pcw/service/push/d;

    invoke-static {v2}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/pcw/service/push/util/c;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/pcw/service/push/d$2;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/pcw/service/push/a;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 663
    if-eqz v0, :cond_0

    .line 664
    invoke-static {v0}, Lcom/sec/pcw/util/h;->a(Ljava/lang/String;)Lcom/sec/pcw/service/push/util/d;

    move-result-object v0

    .line 665
    invoke-virtual {v0}, Lcom/sec/pcw/service/push/util/d;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 666
    const-string v1, "mfl_PushMsgHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/pcw/service/push/d$2;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]XML Parse Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/pcw/service/push/util/d;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$2;->c:Lcom/sec/pcw/service/push/d;

    invoke-static {v0}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pcw/service/push/d$2;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/pcw/service/push/util/b;->g(Landroid/content/Context;Ljava/lang/String;)Z

    .line 680
    :cond_0
    :goto_0
    return-void

    .line 670
    :cond_1
    iget-object v1, p0, Lcom/sec/pcw/service/push/d$2;->a:Lcom/sec/pcw/service/push/a;

    iget-object v2, p0, Lcom/sec/pcw/service/push/d$2;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/sec/pcw/service/push/a;->a(Lcom/sec/pcw/service/push/util/d;Ljava/lang/String;)Z

    move-result v0

    .line 671
    const-string v1, "mfl_PushMsgHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/pcw/service/push/d$2;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]Https Request - isSuccess: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    if-eqz v0, :cond_2

    .line 674
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$2;->c:Lcom/sec/pcw/service/push/d;

    invoke-static {v0}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pcw/service/push/d$2;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/pcw/service/push/util/b;->f(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 676
    :cond_2
    iget-object v0, p0, Lcom/sec/pcw/service/push/d$2;->c:Lcom/sec/pcw/service/push/d;

    invoke-static {v0}, Lcom/sec/pcw/service/push/d;->a(Lcom/sec/pcw/service/push/d;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pcw/service/push/d$2;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/pcw/service/push/util/b;->g(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method
