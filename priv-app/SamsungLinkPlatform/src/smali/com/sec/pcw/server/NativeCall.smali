.class public Lcom/sec/pcw/server/NativeCall;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 12
    :try_start_0
    const-string v0, "SLPF_security"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    :goto_0
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/pcw/server/NativeCall;->a:Ljava/lang/Object;

    return-void

    .line 13
    :catch_0
    move-exception v0

    .line 14
    const-string v1, "mfl_NativeCall"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::could not load Security library: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x0

    .line 52
    :try_start_0
    sget-object v1, Lcom/sec/pcw/server/NativeCall;->a:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    if-eqz p0, :cond_0

    .line 54
    :try_start_1
    invoke-static {p0}, Lcom/sec/pcw/server/NativeCall;->encryptAESUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 56
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    :goto_0
    return-object v0

    .line 56
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_2 .. :try_end_2} :catch_0

    .line 60
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 59
    const-string v0, "ERROR"

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    const/4 v0, 0x0

    .line 67
    :try_start_0
    sget-object v1, Lcom/sec/pcw/server/NativeCall;->a:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    if-eqz p0, :cond_0

    .line 69
    :try_start_1
    invoke-static {p0}, Lcom/sec/pcw/server/NativeCall;->decryptAESUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    :goto_0
    return-object v0

    .line 71
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_2 .. :try_end_2} :catch_0

    .line 75
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->printStackTrace()V

    .line 74
    const-string v0, "ERROR"

    goto :goto_0
.end method

.method public static native decryptAES(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public static native decryptAESUrl(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public static native encryptAES(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public static native encryptAESUrl(Ljava/lang/String;)Ljava/lang/String;
.end method
