.class public final Lcom/sec/pcw/hybrid/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/hybrid/b/b$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:Ljava/lang/String;

.field private final i:I

.field private final j:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/sec/pcw/hybrid/b/b$a;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iget-object v0, p1, Lcom/sec/pcw/hybrid/b/b$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->a:Ljava/lang/String;

    .line 76
    iget-object v0, p1, Lcom/sec/pcw/hybrid/b/b$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->b:Ljava/lang/String;

    .line 77
    iget-object v0, p1, Lcom/sec/pcw/hybrid/b/b$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->c:Ljava/lang/String;

    .line 78
    iget-object v0, p1, Lcom/sec/pcw/hybrid/b/b$a;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->d:Ljava/lang/String;

    .line 79
    iget-object v0, p1, Lcom/sec/pcw/hybrid/b/b$a;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->e:Ljava/lang/String;

    .line 80
    iget-object v0, p1, Lcom/sec/pcw/hybrid/b/b$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->f:Ljava/lang/String;

    .line 81
    iget-object v0, p1, Lcom/sec/pcw/hybrid/b/b$a;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->h:Ljava/lang/String;

    .line 82
    iget v0, p1, Lcom/sec/pcw/hybrid/b/b$a;->h:I

    iput v0, p0, Lcom/sec/pcw/hybrid/b/b;->i:I

    .line 83
    iget-object v0, p1, Lcom/sec/pcw/hybrid/b/b$a;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->j:Ljava/lang/String;

    .line 85
    invoke-static {}, Lcom/sec/pcw/hybrid/b/b;->k()I

    move-result v0

    iput v0, p0, Lcom/sec/pcw/hybrid/b/b;->g:I

    .line 86
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/pcw/hybrid/b/b$a;B)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/sec/pcw/hybrid/b/b;-><init>(Lcom/sec/pcw/hybrid/b/b$a;)V

    return-void
.end method

.method private static k()I
    .locals 5

    .prologue
    .line 89
    const-string v1, ""

    .line 95
    :try_start_0
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    .line 97
    if-eqz v0, :cond_1

    .line 98
    invoke-virtual {v0}, Lcom/mfluent/asp/ASPApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "com.osp.app.signin"

    const/16 v3, 0x80

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    move-object v1, v0

    .line 106
    :goto_1
    if-eqz v1, :cond_0

    const-string v0, "1.3.053"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 107
    const/4 v0, 0x2

    .line 112
    :goto_2
    const-string v1, "mfl_AuthInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ospVersion:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    return v0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    const-string v2, "mfl_AuthInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "detectOSPVersion() failed with NameNotFoundException:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 102
    :catch_1
    move-exception v0

    .line 103
    const-string v2, "mfl_AuthInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "detectOSPVersion() failed with exc:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 109
    :cond_0
    const/4 v0, 0x1

    goto :goto_2

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/sec/pcw/hybrid/b/b;->i:I

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/pcw/hybrid/b/b;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 170
    const/4 v0, 0x2

    iget v1, p0, Lcom/sec/pcw/hybrid/b/b;->g:I

    if-ne v0, v1, :cond_0

    .line 171
    const/4 v0, 0x1

    .line 173
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AuthInfo [userID = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/pcw/hybrid/b/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", emailID = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pcw/hybrid/b/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", token = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pcw/hybrid/b/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tokenSecret = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pcw/hybrid/b/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", serverUrl = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/pcw/hybrid/b/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
