.class public final Lcom/sec/pcw/hybrid/update/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v2, p0, Lcom/sec/pcw/hybrid/update/a;->a:Ljava/lang/String;

    .line 21
    iput v3, p0, Lcom/sec/pcw/hybrid/update/a;->b:I

    .line 22
    iput-object v2, p0, Lcom/sec/pcw/hybrid/update/a;->c:Ljava/lang/String;

    .line 23
    iput-object v2, p0, Lcom/sec/pcw/hybrid/update/a;->d:Ljava/lang/String;

    .line 24
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/pcw/hybrid/update/a;->e:J

    .line 25
    iput v3, p0, Lcom/sec/pcw/hybrid/update/a;->f:I

    .line 26
    iput-object v2, p0, Lcom/sec/pcw/hybrid/update/a;->g:Ljava/lang/String;

    .line 27
    iput-object v2, p0, Lcom/sec/pcw/hybrid/update/a;->h:Ljava/lang/String;

    .line 28
    iput-object v2, p0, Lcom/sec/pcw/hybrid/update/a;->i:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/pcw/hybrid/update/a;->a:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/pcw/hybrid/update/a;->b:I

    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 45
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/pcw/hybrid/update/a;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :goto_0
    return-void

    .line 46
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/pcw/hybrid/update/a;->b:I

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/pcw/hybrid/update/a;->c:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/pcw/hybrid/update/a;->d:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/sec/pcw/hybrid/update/a;->e:J

    return-wide v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 74
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/pcw/hybrid/update/a;->e:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 77
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/pcw/hybrid/update/a;->e:J

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/pcw/hybrid/update/a;->f:I

    return v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 87
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/pcw/hybrid/update/a;->f:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/pcw/hybrid/update/a;->f:I

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/a;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/pcw/hybrid/update/a;->g:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/pcw/hybrid/update/a;->h:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/pcw/hybrid/update/a;->i:Ljava/lang/String;

    .line 116
    return-void
.end method
