.class public final Lcom/sec/pcw/hybrid/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/sec/pcw/hybrid/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_GENERAL:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lcom/sec/pcw/hybrid/c/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/pcw/hybrid/b/a;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/pcw/hybrid/b/a;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 70
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "app info is invalid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_1
    iput-object p1, p0, Lcom/sec/pcw/hybrid/c/a;->b:Landroid/content/Context;

    .line 74
    iput-object p2, p0, Lcom/sec/pcw/hybrid/c/a;->c:Lcom/sec/pcw/hybrid/b/a;

    .line 81
    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/pcw/hybrid/b/b;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 113
    iget-object v0, p0, Lcom/sec/pcw/hybrid/c/a;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 116
    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "user_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "birthday"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "email_id"

    aput-object v3, v1, v2

    const-string v2, "mcc"

    aput-object v2, v1, v5

    const/4 v2, 0x4

    const-string v3, "server_url"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "api_server_url"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "auth_server_url"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "cc"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "device_physical_address_text"

    aput-object v3, v1, v2

    .line 130
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 132
    const-string v3, "client_id"

    iget-object v4, p0, Lcom/sec/pcw/hybrid/c/a;->c:Lcom/sec/pcw/hybrid/b/a;

    invoke-virtual {v4}, Lcom/sec/pcw/hybrid/b/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    const-string v3, "client_secret"

    iget-object v4, p0, Lcom/sec/pcw/hybrid/c/a;->c:Lcom/sec/pcw/hybrid/b/a;

    invoke-virtual {v4}, Lcom/sec/pcw/hybrid/b/a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    const-string v3, "mypackage"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    const-string v0, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    const-string v0, "MODE"

    const-string v3, "SHOW_NOTIFICATION_WITH_POPUP"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    array-length v0, v1

    if-lez v0, :cond_0

    .line 140
    const-string v0, "additional"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    :cond_0
    if-eqz p1, :cond_1

    .line 145
    const-string v0, "expired_access_token"

    invoke-virtual {p1}, Lcom/sec/pcw/hybrid/b/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/sec/pcw/hybrid/c/a;->b:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 150
    sget-object v0, Lcom/sec/pcw/hybrid/c/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v0}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v0

    if-gt v0, v5, :cond_2

    .line 151
    const-string v0, "mfl_ASPSignInFacade"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "#### Sent SA token request : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_2
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/sec/pcw/hybrid/c/a;->b()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/accounts/Account;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 166
    iget-object v1, p0, Lcom/sec/pcw/hybrid/c/a;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 167
    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 168
    if-nez v3, :cond_1

    .line 176
    :cond_0
    :goto_0
    return-object v0

    .line 171
    :cond_1
    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 172
    const-string v5, "com.osp.app.signin"

    iget-object v6, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 173
    goto :goto_0

    .line 171
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public final c()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 206
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 207
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 209
    array-length v0, v0

    if-lez v0, :cond_0

    .line 210
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 211
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    const-string v1, "MODE"

    iget-object v2, p0, Lcom/sec/pcw/hybrid/c/a;->c:Lcom/sec/pcw/hybrid/b/a;

    invoke-virtual {v2}, Lcom/sec/pcw/hybrid/b/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    :goto_0
    return-object v0

    .line 218
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 219
    const-string v1, "client_id"

    const-string v2, "c7hc8m4900"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    const-string v1, "client_secret"

    const-string v2, "B5B9B48012665C4F1914C52B4B6DD2F4"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 221
    const-string v1, "mypackage"

    const-string v2, "com.samsung.android.sdk.samsunglink"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 222
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    const-string v1, "MODE"

    const-string v2, "ADD_ACCOUNT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method
