.class public final Lcom/sec/pcw/hybrid/update/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private final g:Lcom/sec/pcw/hybrid/update/e;

.field private final h:Ljava/io/File;

.field private final i:Ljava/io/File;

.field private final j:Landroid/content/Context;

.field private final k:I

.field private final l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->a:Z

    .line 46
    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->b:Z

    .line 47
    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->c:Z

    .line 48
    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->d:Z

    .line 49
    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->e:Z

    .line 50
    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->f:Z

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    .line 61
    invoke-static {p1}, Lcom/sec/pcw/hybrid/update/d;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/pcw/hybrid/update/b;->l:I

    .line 62
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/sec/pcw/hybrid/update/b;->k:I

    .line 64
    new-instance v0, Lcom/sec/pcw/hybrid/update/e;

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iget v2, p0, Lcom/sec/pcw/hybrid/update/b;->k:I

    invoke-direct {v0, v1, v2}, Lcom/sec/pcw/hybrid/update/e;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->g:Lcom/sec/pcw/hybrid/update/e;

    .line 65
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "remote_version_slpf.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->h:Ljava/io/File;

    .line 66
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "remote_samsung_apps_update.dat"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->i:Ljava/io/File;

    .line 67
    return-void
.end method

.method static synthetic a(Lcom/sec/pcw/hybrid/update/b;Z)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/pcw/hybrid/update/b;->b(Z)V

    return-void
.end method

.method private declared-synchronized b(Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 144
    monitor-enter p0

    :try_start_0
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v2, "::checkUpdate"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v2, "::init"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->a:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->b:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->c:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->d:Z

    .line 147
    if-eqz p1, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    const-string v2, "slpf_pref_20"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 149
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 150
    const-string v2, "last_ugrade_check_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 151
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/sec/pcw/hybrid/update/b;->g:Lcom/sec/pcw/hybrid/update/e;

    iget-object v4, p0, Lcom/sec/pcw/hybrid/update/b;->h:Ljava/io/File;

    if-eqz p1, :cond_3

    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v2, "::getVersionTextUrl"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    const-string v2, "slpf_pref_20"

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "baseUrl"

    iget-object v5, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    const/high16 v6, 0x7f0a0000

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "http://"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_0
    sget-boolean v0, Lcom/mfluent/asp/ASPApplication;->i:Z

    if-eqz v0, :cond_2

    const-string v0, "/tab"

    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/version_slpf2.xml"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "mfl_UpdateFromMarketManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "::getVersionTextUrl   updateCheckBase:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_2
    invoke-virtual {v3, v4, v0}, Lcom/sec/pcw/hybrid/update/e;->a(Ljava/io/File;Landroid/net/Uri;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    :goto_3
    :try_start_2
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->g:Lcom/sec/pcw/hybrid/update/e;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/e;->a()I

    move-result v0

    iget v2, p0, Lcom/sec/pcw/hybrid/update/b;->l:I

    if-le v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->a:Z

    .line 162
    const-string v0, "mfl_UpdateFromMarketManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::checkUpdate  isForce : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/pcw/hybrid/update/b;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->g:Lcom/sec/pcw/hybrid/update/e;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/e;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->b:Z

    .line 165
    const-string v0, "mfl_UpdateFromMarketManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::checkUpdate  hasModelMarket : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/pcw/hybrid/update/b;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->b:Z

    if-eqz v0, :cond_5

    .line 168
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/pcw/hybrid/update/b;->c(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->c:Z

    .line 173
    :goto_5
    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->c:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->d:Z

    if-eqz v0, :cond_6

    .line 174
    :cond_1
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "::checkUpdate  sendBroadcast(UpgradeManager.BROADCAST_PLATFORM_UPGRADE_AVAIL)"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.mfluent.asp.util.UpgradeManager.BROADCAST_PLATFORM_UPGRADE_AVAIL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ASPApplication;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 181
    :goto_6
    monitor-exit p0

    return-void

    .line 155
    :cond_2
    :try_start_3
    const-string v0, "/web"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 157
    :catch_0
    move-exception v0

    .line 158
    :try_start_4
    const-string v2, "mfl_UpdateFromMarketManager"

    const-string v3, "::checkUpdate Unable to parse data."

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v0, v1

    .line 161
    goto :goto_4

    .line 170
    :cond_5
    :try_start_5
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/pcw/hybrid/update/b;->d(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->d:Z

    goto :goto_5

    .line 178
    :cond_6
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "::checkUpdate  -- no update"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_6

    :cond_7
    move-object v2, v0

    goto/16 :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 377
    invoke-static {p0}, Lcom/sec/pcw/hybrid/update/b;->g(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 378
    const-string v1, "mfl_UpdateFromMarketManager"

    const-string v2, "::isAvailableUpdateFromSamsungApps   SamsungApps is not installed!!"

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_0
    :goto_0
    return v0

    .line 381
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 384
    :try_start_0
    invoke-static {p0, v1}, Lcom/sec/pcw/hybrid/update/d;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 389
    invoke-static {v2}, Lcom/sec/pcw/hybrid/update/c;->a(Ljava/net/URL;)Lcom/sec/pcw/hybrid/update/UpdateInfo;

    move-result-object v2

    .line 390
    const-string v3, "mfl_UpdateFromMarketManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "## Samsung Apps Update Info ## : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    invoke-virtual {v2}, Lcom/sec/pcw/hybrid/update/UpdateInfo;->c()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 392
    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lcom/sec/pcw/hybrid/update/UpdateInfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 393
    const/4 v0, 0x1

    goto :goto_0

    .line 385
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_0
.end method

.method private c()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 280
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "::isAvailableUpdateFromGooglePlay"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-direct {p0}, Lcom/sec/pcw/hybrid/update/b;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "::isAvailableUpdateFromGooglePlay   GooglePlay is not installed!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    :goto_0
    return v3

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->g:Lcom/sec/pcw/hybrid/update/e;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/e;->b()I

    move-result v2

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/e;->c()I

    move-result v1

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/e;->d()I

    move-result v0

    if-eq v0, v4, :cond_1

    .line 286
    :goto_1
    iget v1, p0, Lcom/sec/pcw/hybrid/update/b;->l:I

    if-ge v1, v0, :cond_4

    .line 287
    const-string v1, "mfl_UpdateFromMarketManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::isAvailableUpdateFromGooglePlay:: new version exist!! : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const/4 v3, 0x1

    goto :goto_0

    .line 285
    :cond_1
    if-eq v1, v4, :cond_2

    move v0, v1

    goto :goto_1

    :cond_2
    if-eq v2, v4, :cond_3

    move v0, v2

    goto :goto_1

    :cond_3
    const-string v0, "hwang"

    const-string v1, "::getGooglePlayLastestVersion :: never happen!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    goto :goto_1

    .line 290
    :cond_4
    const-string v1, "mfl_UpdateFromMarketManager"

    const-string v2, "::isAvailableUpdateFromGooglePlay:: lastest version already installed "

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const-string v1, "mfl_UpdateFromMarketManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "::isAvailableUpdateFromGooglePlay:: GooglePlay version : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v0, "mfl_UpdateFromMarketManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::isAvailableUpdateFromGooglePlay:: Local APK  version : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/pcw/hybrid/update/b;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 184
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->g:Lcom/sec/pcw/hybrid/update/e;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/e;->g()Ljava/util/Enumeration;

    move-result-object v2

    .line 185
    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 186
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 187
    const-string v3, "mfl_UpdateFromMarketManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::updateFromModelMarket MARKET : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v3, "SamsungApps"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 190
    invoke-direct {p0, p1}, Lcom/sec/pcw/hybrid/update/b;->f(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->e:Z

    .line 191
    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->e:Z

    if-eqz v0, :cond_0

    .line 192
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v2, "succeed to update through samsung apps!"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 206
    :goto_0
    return v0

    .line 195
    :cond_1
    const-string v3, "GooglePlay"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    invoke-direct {p0}, Lcom/sec/pcw/hybrid/update/b;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->f:Z

    .line 198
    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->f:Z

    if-eqz v0, :cond_0

    .line 199
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v2, "succeed to update through google play!"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 200
    goto :goto_0

    .line 205
    :cond_2
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "::updateFromModelMarket fail to update!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Lcom/sec/pcw/hybrid/update/UpdateInfo;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 339
    .line 342
    :try_start_0
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->i:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    new-instance v2, Ljava/io/ObjectInputStream;

    new-instance v0, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/sec/pcw/hybrid/update/b;->i:Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    :try_start_1
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/hybrid/update/UpdateInfo;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v2

    .line 353
    :goto_0
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    move-object v1, v0

    .line 356
    :goto_1
    return-object v1

    .line 348
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 349
    :goto_2
    :try_start_2
    const-string v3, "mfl_UpdateFromMarketManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to read downloaded update info from file: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/pcw/hybrid/update/b;->i:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 353
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    goto :goto_1

    .line 350
    :catch_1
    move-exception v0

    move-object v2, v1

    .line 351
    :goto_3
    :try_start_3
    const-string v3, "mfl_UpdateFromMarketManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to read downloaded update info from file: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/pcw/hybrid/update/b;->i:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 353
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_4
    invoke-static {v2}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/InputStream;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4

    .line 350
    :catch_2
    move-exception v0

    goto :goto_3

    .line 348
    :catch_3
    move-exception v0

    goto :goto_2

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private d(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 210
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->g:Lcom/sec/pcw/hybrid/update/e;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/e;->f()Ljava/util/Enumeration;

    move-result-object v2

    .line 211
    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 212
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 213
    const-string v3, "mfl_UpdateFromMarketManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::isAvailableUpdateFromGlobalMarket MARKET : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v3, "SamsungApps"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 216
    invoke-direct {p0, p1}, Lcom/sec/pcw/hybrid/update/b;->f(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->e:Z

    .line 217
    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->e:Z

    if-eqz v0, :cond_0

    .line 218
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v2, "succeed to update through samsung apps!"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 232
    :goto_0
    return v0

    .line 221
    :cond_1
    const-string v3, "GooglePlay"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    invoke-direct {p0}, Lcom/sec/pcw/hybrid/update/b;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->f:Z

    .line 224
    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->f:Z

    if-eqz v0, :cond_0

    .line 225
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v2, "succeed to update through google play!"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 226
    goto :goto_0

    .line 231
    :cond_2
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "::isAvailableUpdateFromGlobalMarket Update is unavailable!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->g:Lcom/sec/pcw/hybrid/update/e;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/e;->f()Ljava/util/Enumeration;

    move-result-object v1

    .line 259
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 260
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 261
    const-string v2, "mfl_UpdateFromMarketManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::updateFromGlobalMarket MARKET : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string v2, "SamsungApps"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 264
    invoke-static {p1}, Lcom/sec/pcw/hybrid/update/b;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->e:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/pcw/hybrid/update/b;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "succeed to update through samsung apps!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_1
    :goto_0
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "::updateFromGlobalMarket Update is unavailable!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    return-void

    .line 268
    :cond_2
    const-string v2, "GooglePlay"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    invoke-direct {p0}, Lcom/sec/pcw/hybrid/update/b;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->f:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/pcw/hybrid/update/b;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "succeed to update through google play!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private e()Z
    .locals 4

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/pcw/hybrid/update/d;->f(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 v0, 0x10000

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 414
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 415
    iget-object v1, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/pcw/hybrid/update/d;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private f(Landroid/content/Context;)Z
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 298
    invoke-static {p1}, Lcom/sec/pcw/hybrid/update/b;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 299
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v2, "::isAvailableUpdateFromSamsungApps   SamsungApps is not installed!!"

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 334
    :goto_0
    return v0

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 303
    invoke-direct {p0}, Lcom/sec/pcw/hybrid/update/b;->d()Lcom/sec/pcw/hybrid/update/UpdateInfo;

    move-result-object v0

    .line 310
    :try_start_0
    iget-object v3, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    invoke-static {v3, v6}, Lcom/sec/pcw/hybrid/update/d;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 315
    iget-object v4, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    const-string v5, "slpf_pref_20"

    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 316
    const-string v4, "last_remote_samsung_apps_update_check"

    const-wide/16 v8, 0x0

    invoke-interface {v7, v4, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 317
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 318
    if-eqz v3, :cond_4

    if-eqz v0, :cond_2

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/UpdateInfo;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lorg/apache/commons/lang3/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    :cond_1
    const-wide/32 v10, 0x2932e00

    add-long/2addr v4, v10

    cmp-long v4, v4, v8

    if-gez v4, :cond_4

    .line 320
    :cond_2
    invoke-static {v3}, Lcom/sec/pcw/hybrid/update/c;->a(Ljava/net/URL;)Lcom/sec/pcw/hybrid/update/UpdateInfo;

    move-result-object v0

    .line 321
    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/UpdateInfo;->c()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    .line 322
    const/4 v5, 0x0

    :try_start_1
    new-instance v4, Ljava/io/ObjectOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v10, p0, Lcom/sec/pcw/hybrid/update/b;->i:Ljava/io/File;

    invoke-direct {v3, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v4, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    move v3, v1

    :goto_1
    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/pcw/hybrid/update/b;->i:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/pcw/hybrid/update/b;->i:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 324
    :cond_3
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "last_remote_samsung_apps_update_check"

    invoke-interface {v3, v4, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 327
    :cond_4
    const-string v3, "mfl_UpdateFromMarketManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "## Samsung Apps Update Info ## : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/UpdateInfo;->c()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/UpdateInfo;->e()I

    move-result v3

    iget v4, p0, Lcom/sec/pcw/hybrid/update/b;->l:I

    if-le v3, v4, :cond_5

    .line 330
    if-eqz v6, :cond_5

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/UpdateInfo;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 331
    goto/16 :goto_0

    .line 311
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    move v0, v1

    .line 313
    goto/16 :goto_0

    .line 322
    :catch_1
    move-exception v3

    move-object v4, v5

    :goto_2
    :try_start_3
    const-string v5, "mfl_UpdateFromMarketManager"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Failed to write downloaded update info to file: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/sec/pcw/hybrid/update/b;->i:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v5, v10, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    move v3, v2

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v4, v5

    :goto_3
    invoke-static {v4}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    throw v0

    :cond_5
    move v0, v1

    .line 334
    goto/16 :goto_0

    .line 322
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v3

    goto :goto_2
.end method

.method private static g(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 406
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.sec.android.app.samsungapps"

    const-string v5, "com.sec.android.app.samsungapps.Main"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "directcall"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v2, v3, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method private h(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 419
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 420
    iget-object v1, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/pcw/hybrid/update/d;->f(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 422
    :try_start_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 427
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 423
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 425
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 129
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "::updateApkFromMarket"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->b:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->c:Z

    if-eqz v0, :cond_4

    .line 131
    iget-object v0, p0, Lcom/sec/pcw/hybrid/update/b;->g:Lcom/sec/pcw/hybrid/update/e;

    invoke-virtual {v0}, Lcom/sec/pcw/hybrid/update/e;->g()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "mfl_UpdateFromMarketManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::updateFromModelMarket MARKET : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "SamsungApps"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p1}, Lcom/sec/pcw/hybrid/update/b;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->e:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/pcw/hybrid/update/b;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "succeed to update through samsung apps!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "::updateFromModelMarket fail to update!!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_2
    :goto_1
    return-void

    .line 131
    :cond_3
    const-string v2, "GooglePlay"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/pcw/hybrid/update/b;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->f:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/pcw/hybrid/update/b;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "succeed to update through google play!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_4
    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->d:Z

    if-eqz v0, :cond_2

    .line 133
    invoke-direct {p0, p1}, Lcom/sec/pcw/hybrid/update/b;->e(Landroid/content/Context;)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 112
    :try_start_0
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "::checkUpdate  Start UpdateCheck Thread!"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/pcw/hybrid/update/b$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/pcw/hybrid/update/b$1;-><init>(Lcom/sec/pcw/hybrid/update/b;Z)V

    const-string v2, "UpdateCheckThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :goto_0
    return-void

    .line 122
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 124
    const-string v0, "mfl_UpdateFromMarketManager"

    const-string v1, "::checkUpdate  UpdateCheck Thread is running"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/pcw/hybrid/update/b;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 104
    iget-object v1, p0, Lcom/sec/pcw/hybrid/update/b;->j:Landroid/content/Context;

    const-string v2, "slpf_pref_20"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 105
    const-string v2, "last_ugrade_check_time"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 107
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x2932e00

    add-long/2addr v2, v6

    cmp-long v1, v4, v2

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
