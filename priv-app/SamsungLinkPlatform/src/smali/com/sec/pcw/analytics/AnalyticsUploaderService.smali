.class public Lcom/sec/pcw/analytics/AnalyticsUploaderService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field public static a:J


# instance fields
.field b:Lcom/sec/pcw/analytics/a;

.field private final c:Ljava/lang/String;

.field private d:Landroid/content/Context;

.field private e:Ljava/lang/String;

.field private f:Landroid/app/AlarmManager;

.field private g:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->a:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "AnalyticsUploaderService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 21
    const-string v0, "https://%s/upload"

    iput-object v0, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->c:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 61
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 62
    invoke-virtual {p0}, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->d:Landroid/content/Context;

    .line 63
    invoke-static {}, Lcom/sec/pcw/analytics/a;->a()Lcom/sec/pcw/analytics/a;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->b:Lcom/sec/pcw/analytics/a;

    .line 64
    iget-object v0, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/pcw/service/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "bss"

    invoke-static {v1, v0}, Lcom/sec/pcw/service/d/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "https://%s/upload"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->e:Ljava/lang/String;

    .line 65
    const-string v0, "mfl_AnalyticsUploaderService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Server URL : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->f:Landroid/app/AlarmManager;

    .line 67
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/pcw/analytics/AnalyticsAlarmReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 68
    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->g:Landroid/app/PendingIntent;

    .line 70
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 40
    new-instance v0, Lcom/mfluent/asp/util/u;

    iget-object v1, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->e:Ljava/lang/String;

    invoke-static {}, Lcom/sec/pcw/analytics/a;->a()Lcom/sec/pcw/analytics/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/pcw/analytics/a;->g()Ljava/lang/String;

    move-result-object v3

    const-string v4, "statistics"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/mfluent/asp/util/u;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->b:Lcom/sec/pcw/analytics/a;

    invoke-virtual {v1}, Lcom/sec/pcw/analytics/a;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/mfluent/asp/util/u;->a()V

    .line 41
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->b:Lcom/sec/pcw/analytics/a;

    invoke-virtual {v0}, Lcom/sec/pcw/analytics/a;->f()I

    move-result v0

    .line 47
    mul-int/lit8 v0, v0, 0x3c

    mul-int/lit16 v4, v0, 0x3e8

    .line 49
    iget-object v0, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->b:Lcom/sec/pcw/analytics/a;

    invoke-virtual {v0}, Lcom/sec/pcw/analytics/a;->c()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 50
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    int-to-long v2, v4

    add-long/2addr v2, v0

    .line 52
    iget-object v0, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->f:Landroid/app/AlarmManager;

    const/4 v1, 0x3

    int-to-long v4, v4

    iget-object v6, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->g:Landroid/app/PendingIntent;

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 56
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/IntentService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->f:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/sec/pcw/analytics/AnalyticsUploaderService;->g:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0
.end method
