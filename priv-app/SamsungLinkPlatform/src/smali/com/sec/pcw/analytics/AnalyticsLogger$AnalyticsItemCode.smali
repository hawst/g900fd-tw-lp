.class public final enum Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/analytics/AnalyticsLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnalyticsItemCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum B:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum C:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum D:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum E:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum F:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum G:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum H:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum I:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum J:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum K:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum L:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum M:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum N:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum O:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum P:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum Q:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field private static final synthetic R:[Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum b:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum c:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum d:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum e:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum f:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum g:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum h:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum i:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum j:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum k:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum l:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum m:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum n:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum o:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum p:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum q:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum r:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum s:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum t:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum u:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum v:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum w:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum x:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum y:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

.field public static final enum z:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 61
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_PHOTO_FROM_CURRENT_DEVICE_TO_STORAGE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 62
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_MUSIC_FROM_CURRENT_DEVICE_TO_STORAGE"

    invoke-direct {v0, v1, v4, v5}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->b:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 63
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_VIDEO_FROM_CURRENT_DEVICE_TO_STORAGE"

    invoke-direct {v0, v1, v5, v6}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->c:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 64
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_FILE_FROM_CURRENT_DEVICE_TO_STORAGE"

    invoke-direct {v0, v1, v6, v7}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->d:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 65
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_DOCUMENT_FROM_CURRENT_DEVICE_TO_STORAGE"

    invoke-direct {v0, v1, v7, v8}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->e:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 66
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_PHOTO_FROM_CURRENT_DEVICE_TO_PC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->f:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 67
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_MUSIC_FROM_CURRENT_DEVICE_TO_PC"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->g:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 68
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_VIDEO_FROM_CURRENT_DEVICE_TO_PC"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->h:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 69
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_FILE_FROM_CURRENT_DEVICE_TO_PC"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->i:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 70
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_DOCUMENT_FROM_CURRENT_DEVICE_TO_PC"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->j:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 71
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_PHOTO_FROM_CURRENT_DEVICE_TO_OTHER_DEVICE_EXCEPT_PC"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->k:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 72
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_MUSIC_FROM_CURRENT_DEVICE_TO_OTHER_DEVICE_EXCEPT_PC"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->l:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 73
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_VIDEO_FROM_CURRENT_DEVICE_TO_OTHER_DEVICE_EXCEPT_PC"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->m:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 74
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_FILE_FROM_CURRENT_DEVICE_TO_OTHER_DEVICE_EXCEPT_PC"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->n:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 75
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "SEND_DOCUMENT_FROM_CURRENT_DEVICE_TO_OTHER_DEVICE_EXCEPT_PC"

    const/16 v2, 0xe

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->o:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 77
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_PHOTO_FROM_STORAGE_TO_CURRENT_DEVICE"

    const/16 v2, 0xf

    const/16 v3, 0x65

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->p:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 78
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_MUSIC_FROM_STORAGE_TO_CURRENT_DEVICE"

    const/16 v2, 0x10

    const/16 v3, 0x66

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->q:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 79
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_VIDEO_FROM_STORAGE_TO_CURRENT_DEVICE"

    const/16 v2, 0x11

    const/16 v3, 0x67

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->r:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 80
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_FILE_FROM_STORAGE_TO_CURRENT_DEVICE"

    const/16 v2, 0x12

    const/16 v3, 0x68

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->s:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 81
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_DOCUMENT_FROM_STORAGE_TO_CURRENT_DEVICE"

    const/16 v2, 0x13

    const/16 v3, 0x69

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->t:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 82
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_PHOTO_FROM_PC_TO_CURRENT_DEVICE"

    const/16 v2, 0x14

    const/16 v3, 0x6a

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->u:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 83
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_MUSIC_FROM_PC_TO_CURRENT_DEVICE"

    const/16 v2, 0x15

    const/16 v3, 0x6b

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->v:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 84
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_VIDEO_FROM_PC_TO_CURRENT_DEVICE"

    const/16 v2, 0x16

    const/16 v3, 0x6c

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->w:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 85
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_FILE_FROM_PC_TO_CURRENT_DEVICE"

    const/16 v2, 0x17

    const/16 v3, 0x6d

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->x:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 86
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_DOCUMENT_FROM_PC_TO_CURRENT_DEVICE"

    const/16 v2, 0x18

    const/16 v3, 0x6e

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->y:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 87
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_PHOTO_FROM_OTHER_DEVICE_EXCEPT_PC_TO_CURRENT_DEVICE"

    const/16 v2, 0x19

    const/16 v3, 0x6f

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->z:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 88
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_MUSIC_FROM_OTHER_DEVICE_EXCEPT_PC_TO_CURRENT_DEVICE"

    const/16 v2, 0x1a

    const/16 v3, 0x70

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->A:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 89
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_VIDEO_FROM_OTHER_DEVICE_EXCEPT_PC_TO_CURRENT_DEVICE"

    const/16 v2, 0x1b

    const/16 v3, 0x71

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->B:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 90
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_FILE_FROM_OTHER_DEVICE_EXCEPT_PC_TO_CURRENT_DEVICE"

    const/16 v2, 0x1c

    const/16 v3, 0x72

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->C:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 91
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "DOWNLOAD_DOCUMENT_FROM_DEVICE_EXCEPT_PC_TO_CURRENT_DEVICE"

    const/16 v2, 0x1d

    const/16 v3, 0x73

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->D:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 94
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_GEOTAGGED_PHOTOS"

    const/16 v2, 0x1e

    const v3, 0x1c139

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->E:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 95
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_REVERSE_GEOCODING_CALLS_FOR_PHOTO"

    const/16 v2, 0x1f

    const v3, 0x1c13a

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->F:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 96
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_GEOTAGGED_VIDEOS"

    const/16 v2, 0x20

    const v3, 0x1c13b

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->G:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 97
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_REVERSE_GEOCODING_CALLS_FOR_VIDEO"

    const/16 v2, 0x21

    const v3, 0x1c13c

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->H:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 98
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_REVERSE_GEOCODING_CALLS_FOR_PHOTO_RETURN_SUCCESS"

    const/16 v2, 0x22

    const v3, 0x1c13d

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->I:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 99
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_REVERSE_GEOCODING_CALLS_FOR_PHOTO_RETURN_EMPTY"

    const/16 v2, 0x23

    const v3, 0x1c13e

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->J:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 100
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_REVERSE_GEOCODING_CALLS_FOR_PHOTO_EXC_SERVICE_NOT_AVAIL"

    const/16 v2, 0x24

    const v3, 0x1c13f

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->K:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 101
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_REVERSE_GEOCODING_CALLS_FOR_PHOTO_EXC_OTHERS"

    const/16 v2, 0x25

    const v3, 0x1c140

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->L:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 102
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_REVERSE_GEOCODING_CALLS_FOR_VIDEO_RETURN_SUCCESS"

    const/16 v2, 0x26

    const v3, 0x1c141

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->M:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 103
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_REVERSE_GEOCODING_CALLS_FOR_VIDEO_RETURN_EMPTY"

    const/16 v2, 0x27

    const v3, 0x1c142

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->N:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 104
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_REVERSE_GEOCODING_CALLS_FOR_VIDEO_EXC_SERVICE_NOT_AVAIL"

    const/16 v2, 0x28

    const v3, 0x1c143

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->O:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 105
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NUMBER_OF_REVERSE_GEOCODING_CALLS_FOR_VIDEO_EXC_OTHERS"

    const/16 v2, 0x29

    const v3, 0x1c144

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->P:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 107
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const-string v1, "NONE"

    const/16 v2, 0x2a

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->Q:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    .line 55
    const/16 v0, 0x2b

    new-array v0, v0, [Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->b:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->c:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->d:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->e:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->f:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->g:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->h:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->i:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->j:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->k:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->l:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->m:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->n:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->o:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->p:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->q:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->r:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->s:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->t:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->u:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->v:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->w:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->x:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->y:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->z:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->A:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->B:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->C:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->D:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->E:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->F:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->G:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->H:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->I:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->J:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->K:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->L:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->M:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->N:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->O:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->P:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->Q:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->R:[Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 112
    iput p3, p0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->value:I

    .line 113
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    return-object v0
.end method

.method public static values()[Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->R:[Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    invoke-virtual {v0}, [Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->value:I

    return v0
.end method
