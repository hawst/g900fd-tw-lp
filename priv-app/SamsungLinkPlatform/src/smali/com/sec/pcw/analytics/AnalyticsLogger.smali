.class public final Lcom/sec/pcw/analytics/AnalyticsLogger;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;,
        Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;
    }
.end annotation


# static fields
.field private static a:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger;->a:Ljava/util/concurrent/BlockingQueue;

    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/pcw/analytics/AnalyticsLogger;->b:Z

    return-void
.end method

.method public static declared-synchronized a(I)Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;
    .locals 2

    .prologue
    .line 172
    const-class v1, Lcom/sec/pcw/analytics/AnalyticsLogger;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->SLINK_UI_APP:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_0

    .line 173
    sget-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->b:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :goto_0
    monitor-exit v1

    return-object v0

    .line 174
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->GALLERY_APP:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_1

    .line 175
    sget-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->c:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    goto :goto_0

    .line 176
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->MUSIC_APP:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_2

    .line 177
    sget-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->d:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    goto :goto_0

    .line 178
    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->VIDEO_APP:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_3

    .line 179
    sget-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->e:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    goto :goto_0

    .line 180
    :cond_3
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->SLINK_PLATFORM_SAMPLE_APP:Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkConstants$ClientApp;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_4

    .line 181
    sget-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->g:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    goto :goto_0

    .line 183
    :cond_4
    sget-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->h:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a()Ljava/util/concurrent/BlockingQueue;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger;->a:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)V
    .locals 14

    .prologue
    const-wide/32 v10, 0x36ee80

    const-wide/32 v12, 0x124f80

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 22
    invoke-static {}, Lcom/sec/pcw/analytics/a;->a()Lcom/sec/pcw/analytics/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/pcw/analytics/a;->g()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v1, "mfl_AnalyticsLogger"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::checkOldFile:cannot make logs directory!"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/pcw/analytics/a;->a()Lcom/sec/pcw/analytics/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/pcw/analytics/a;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "statistics.log"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileWriter;

    const/4 v3, 0x1

    invoke-direct {v1, v0, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1, p0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/Writer;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "statistics.log"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    div-long v8, v4, v10

    mul-long/2addr v8, v10

    sub-long v8, v4, v8

    div-long v10, v8, v12

    add-long/2addr v6, v8

    div-long/2addr v6, v12

    cmp-long v6, v10, v6

    if-eqz v6, :cond_3

    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v7, "yyyyMMddHHmmss"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v6, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "statistics_"

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".log"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/io/File;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v1, "mfl_AnalyticsLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "::checkOldFile:cannot rename file to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    const-string v0, "mfl_AnalyticsLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Analytics log created : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_2
    const-string v2, "mfl_AnalyticsLogger"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::log: exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/Writer;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lorg/apache/commons/io/IOUtils;->closeQuietly(Ljava/io/Writer;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 122
    const-class v2, Lcom/sec/pcw/analytics/AnalyticsLogger;

    monitor-enter v2

    :try_start_0
    const-string v3, "mfl_AnalyticsLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Enter log() : categoryCode:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") itemCode:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    if-nez p0, :cond_0

    .line 134
    const-string v1, "mfl_AnalyticsLogger"

    const-string v3, "Initialization is not proper. context is null"

    invoke-static {v1, v3}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    :goto_0
    monitor-exit v2

    return v0

    .line 138
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sec/pcw/analytics/a;->a()Lcom/sec/pcw/analytics/a;

    move-result-object v3

    .line 139
    invoke-virtual {v3, p0}, Lcom/sec/pcw/analytics/a;->a(Landroid/content/Context;)V

    .line 141
    invoke-virtual {v3}, Lcom/sec/pcw/analytics/a;->e()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyyMMddHHmmss"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 145
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 146
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsItemCode;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149
    :try_start_2
    sget-object v3, Lcom/sec/pcw/analytics/AnalyticsLogger;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v6, 0xfa

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v6, v7, v5}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    .line 151
    sget-boolean v3, Lcom/sec/pcw/analytics/AnalyticsLogger;->b:Z

    if-nez v3, :cond_1

    .line 152
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    new-instance v4, Lcom/sec/pcw/analytics/AnalyticsLogger$1;

    invoke-direct {v4}, Lcom/sec/pcw/analytics/AnalyticsLogger$1;-><init>()V

    invoke-interface {v3, v4}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    const/4 v3, 0x1

    sput-boolean v3, Lcom/sec/pcw/analytics/AnalyticsLogger;->b:Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_1
    move v0, v1

    .line 160
    goto :goto_0

    .line 155
    :catch_0
    move-exception v3

    :try_start_3
    const-string v3, "mfl_AnalyticsLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::Queue Full for Data : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 156
    :catch_1
    move-exception v0

    .line 157
    :try_start_4
    const-string v3, "mfl_AnalyticsLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "::Logger : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 162
    :cond_2
    const-string v1, "mfl_AnalyticsLogger"

    const-string v3, "AnalyticsLogger.log() is ignored"

    invoke-static {v1, v3}, Lcom/sec/pcw/util/e;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method
