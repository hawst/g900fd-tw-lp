.class public Lcom/sec/pcw/analytics/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/net/ConnectivityManager;

.field private b:Z

.field private c:Z

.field private d:I

.field private e:I

.field private f:Landroid/content/SharedPreferences;

.field private final g:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/sec/pcw/analytics/a;->g:Landroid/content/Context;

    .line 46
    invoke-direct {p0, p1}, Lcom/sec/pcw/analytics/a;->b(Landroid/content/Context;)V

    .line 47
    const-string v0, "mfl_AnalyticsUploaderSetting"

    const-string v1, "::sendSettingsChangedIntent Starting AnalyticsUploaderService"

    invoke-static {v0, v1}, Lcom/sec/pcw/util/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/pcw/analytics/a;->g:Landroid/content/Context;

    const-class v2, Lcom/sec/pcw/analytics/AnalyticsUploaderService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "command"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/pcw/analytics/a;->g:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 48
    return-void
.end method

.method public static a()Lcom/sec/pcw/analytics/a;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/sec/pcw/analytics/a;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/analytics/a;

    return-object v0
.end method

.method private b(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 55
    const-string v0, "AnalyticsUploaderSetting"

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/pcw/analytics/a;->f:Landroid/content/SharedPreferences;

    .line 56
    iget-object v0, p0, Lcom/sec/pcw/analytics/a;->f:Landroid/content/SharedPreferences;

    const-string v1, "wifionly"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/pcw/analytics/a;->b:Z

    .line 57
    iget-object v0, p0, Lcom/sec/pcw/analytics/a;->f:Landroid/content/SharedPreferences;

    const-string v1, "logallow"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/pcw/analytics/a;->c:Z

    .line 58
    iget-object v0, p0, Lcom/sec/pcw/analytics/a;->f:Landroid/content/SharedPreferences;

    const-string v1, "upload"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/pcw/analytics/a;->d:I

    .line 59
    iget-object v0, p0, Lcom/sec/pcw/analytics/a;->f:Landroid/content/SharedPreferences;

    const-string v1, "period"

    const/16 v2, 0x2d0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/pcw/analytics/a;->e:I

    .line 60
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/sec/pcw/analytics/a;->a:Landroid/net/ConnectivityManager;

    .line 61
    return-void
.end method

.method private h()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 114
    :try_start_0
    iget-object v2, p0, Lcom/sec/pcw/analytics/a;->a:Landroid/net/ConnectivityManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 115
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 118
    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    const-string v2, "mfl_AnalyticsUploaderSetting"

    const-string v3, "::isWifiDataConnectionOn:"

    invoke-static {v2, v3, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 122
    goto :goto_0
.end method

.method private i()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 128
    :try_start_0
    iget-object v1, p0, Lcom/sec/pcw/analytics/a;->a:Landroid/net/ConnectivityManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 129
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    const/4 v0, 0x1

    .line 136
    :cond_0
    :goto_0
    return v0

    .line 134
    :catch_0
    move-exception v1

    .line 135
    const-string v2, "mfl_AnalyticsUploaderSetting"

    const-string v3, "::isMobileDataConnectionOn:"

    invoke-static {v2, v3, v1}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/pcw/analytics/a;->b(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 101
    iget-boolean v1, p0, Lcom/sec/pcw/analytics/a;->c:Z

    if-eq v1, v0, :cond_0

    .line 102
    iput-boolean v0, p0, Lcom/sec/pcw/analytics/a;->c:Z

    .line 103
    iget-object v1, p0, Lcom/sec/pcw/analytics/a;->f:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 104
    const-string v2, "logallow"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 105
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 109
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/sec/pcw/analytics/a;->d:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 149
    iget v0, p0, Lcom/sec/pcw/analytics/a;->d:I

    if-nez v0, :cond_2

    move v0, v1

    .line 151
    :goto_0
    iget-boolean v3, p0, Lcom/sec/pcw/analytics/a;->b:Z

    if-eqz v3, :cond_3

    invoke-direct {p0}, Lcom/sec/pcw/analytics/a;->h()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v1

    .line 152
    :goto_1
    iget-boolean v4, p0, Lcom/sec/pcw/analytics/a;->b:Z

    if-nez v4, :cond_4

    invoke-direct {p0}, Lcom/sec/pcw/analytics/a;->i()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/sec/pcw/analytics/a;->h()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_0
    move v4, v1

    .line 153
    :goto_2
    if-eqz v0, :cond_5

    if-nez v3, :cond_1

    if-eqz v4, :cond_5

    :cond_1
    :goto_3
    return v1

    :cond_2
    move v0, v2

    .line 149
    goto :goto_0

    :cond_3
    move v3, v2

    .line 151
    goto :goto_1

    :cond_4
    move v4, v2

    .line 152
    goto :goto_2

    :cond_5
    move v1, v2

    .line 153
    goto :goto_3
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/sec/pcw/analytics/a;->c:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/sec/pcw/analytics/a;->e:I

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/pcw/analytics/a;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 170
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/Analytics/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
