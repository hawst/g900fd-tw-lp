.class public final enum Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/pcw/analytics/AnalyticsLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnalyticsCategory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

.field public static final enum b:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

.field public static final enum c:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

.field public static final enum d:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

.field public static final enum e:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

.field public static final enum f:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

.field public static final enum g:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

.field public static final enum h:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

.field private static final synthetic i:[Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    const-string v1, "GEO_LOCATION_USAGE_STATISTICS"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    .line 36
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    const-string v1, "SLINK_UI_APP"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->b:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    .line 37
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    const-string v1, "GALLERY_APP"

    const/16 v2, 0xc9

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->c:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    .line 38
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    const-string v1, "MUSIC_APP"

    const/16 v2, 0xca

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->d:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    .line 39
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    const-string v1, "VIDEO_APP"

    const/16 v2, 0xcb

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->e:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    .line 40
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    const-string v1, "SLINK_PLATFORM_GENERAL"

    const/4 v2, 0x5

    const/16 v3, 0xcc

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->f:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    .line 41
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    const-string v1, "SLINK_PLATFORM_SAMPLE_APP"

    const/4 v2, 0x6

    const/16 v3, 0x802

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->g:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    .line 42
    new-instance v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    const-string v1, "NONE"

    const/4 v2, 0x7

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->h:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    .line 34
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->a:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->b:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->c:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->d:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->e:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->f:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->g:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->h:Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->i:[Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput p3, p0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->value:I

    .line 48
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    return-object v0
.end method

.method public static values()[Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->i:[Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    invoke-virtual {v0}, [Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/pcw/analytics/AnalyticsLogger$AnalyticsCategory;->value:I

    return v0
.end method
