.class public Lcom/sec/android/safe/wifiPke/WifiPke;
.super Ljava/lang/Object;


# static fields
# The value of this static final field might be set in the static constructor
.field private static final PASSWORD_VERIFICATION_FAILURE:Ljava/lang/String; = "\u00e7\u00e0\u00e8\u00ed\u00f4\u00f3\u00e4"

.field public static b043E043Eоо043Eо:I = 0x2

.field public static b043Eооо043Eо:I = 0x2a

.field public static bо043E043Eо043Eо:I = 0x0

.field public static bо043Eоо043Eо:I = 0x1

.field private static mWifiPke:Lcom/sec/android/safe/wifiPke/WifiPke;


# instance fields
.field private final ACCOUNT:Ljava/lang/String;

.field private final ENCRYPTEDTOKEN:Ljava/lang/String;

.field private final GENERATESECURETOKEN:Ljava/lang/String;

.field private final LOGINCOUNT:Ljava/lang/String;

.field private final PASSWORD:Ljava/lang/String;

.field private final RESULT:Ljava/lang/String;

.field private final SS_CLIENT_MSG_ACK_FROM_SERVER:Ljava/lang/String;

.field private final SS_CLIENT_MSG_ENCRYPTION_ACK_FROM_SERVER:Ljava/lang/String;

.field private final SS_CLIENT_MSG_HELLO_FROM_SERVER:Ljava/lang/String;

.field private final VERIFY_DSA_PASSWORD:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/sec/android/safe/wifiPke/WifiPke;->PASSWORD_VERIFICATION_FAILURE:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/16 v2, 0xa1

    const/4 v3, 0x3

    :try_start_1
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    :try_start_2
    sput-object v1, Lcom/sec/android/safe/wifiPke/WifiPke;->PASSWORD_VERIFICATION_FAILURE:Ljava/lang/String;

    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/safe/wifiPke/WifiPke;->mWifiPke:Lcom/sec/android/safe/wifiPke/WifiPke;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_0
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/String;->length()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    sget v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->bоо043Eо043Eо()I

    move-result v1

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_1

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v0

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v0

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    :pswitch_0
    const/16 v0, 0x4f

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    const/4 v0, 0x4

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    :pswitch_1
    return-void

    :catch_1
    move-exception v0

    :pswitch_2
    packed-switch v4, :pswitch_data_2

    :goto_1
    packed-switch v4, :pswitch_data_3

    goto :goto_1

    :pswitch_3
    throw v0

    :catch_2
    move-exception v0

    :try_start_4
    throw v0

    :catch_3
    move-exception v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private constructor <init>()V
    .locals 4

    :try_start_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "\ufd9c\ufd8f\ufd9d\ufd9f\ufd96\ufd9e"

    const/16 v1, 0xf2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/wifiPke/WifiPke;->RESULT:Ljava/lang/String;

    const-string v0, "\ufed5\ufec6\ufed8\ufed8\ufedc\ufed4\ufed7\ufec9"

    const/16 v1, 0x89

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/wifiPke/WifiPke;->PASSWORD:Ljava/lang/String;

    const-string v0, "\u0154\u0157\u014f\u0151\u0156\u012b\u0157\u015d\u0156\u015c"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0xdb

    const/16 v2, 0xd

    const/4 v3, 0x3

    :try_start_1
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/wifiPke/WifiPke;->LOGINCOUNT:Ljava/lang/String;

    const-string v0, "\u00d0\u00a0\u00ec9\u00f0Q~@\u00e3\u00a4\u00f4\u00bd\u00b8\u00ce"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v1, 0xc4

    const/4 v2, 0x1

    :try_start_2
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/wifiPke/WifiPke;->ENCRYPTEDTOKEN:Ljava/lang/String;

    const-string v0, "\u00ee\u000f"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const/16 v1, 0x7e

    const/4 v2, 0x1

    :try_start_3
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/wifiPke/WifiPke;->SS_CLIENT_MSG_ACK_FROM_SERVER:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :try_start_4
    const-string v0, "\u00ad\u00ef)\u00c3\u008a\u00e8\u009a\u00b54\u0082\u0090\u00d4{\u00f0I\u0015}[A\u00d1\u00f8\u0015\u0082-\u00fc\u0007"

    const/16 v1, 0x7f

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v0

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :pswitch_0
    :try_start_5
    iput-object v0, p0, Lcom/sec/android/safe/wifiPke/WifiPke;->ACCOUNT:Ljava/lang/String;

    const-string v0, "CE"

    const/16 v1, 0xeb

    const/16 v2, 0xd8

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/wifiPke/WifiPke;->SS_CLIENT_MSG_HELLO_FROM_SERVER:Ljava/lang/String;

    const-string v0, "\uffb7\uffbd"
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const/16 v1, 0x79

    const/4 v2, 0x4

    :try_start_6
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/wifiPke/WifiPke;->SS_CLIENT_MSG_ENCRYPTION_ACK_FROM_SERVER:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :try_start_7
    const-string v0, "\uff8c\uffbe\uffcd\uffc6\uff8c\uffcd\uffa0\uffc9\uffcc\uffd2\uffc1\uff8c\uffc1\uffc2\uffd3\uffc6\uffc0\uffc2\uff8c\uffbe\uffc0\uffc0\uffcc\uffd2\uffcb\uffd1\uff8c\uffd3\uffc2\uffcf\uffc6\uffc3\uffd6\uffad\uffbe\uffd0\uffd0\uffd4\uffcc\uffcf\uffc1"
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    const/16 v1, 0xd6

    const/16 v2, 0x33

    const/4 v3, 0x2

    :try_start_8
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/wifiPke/WifiPke;->VERIFY_DSA_PASSWORD:Ljava/lang/String;

    const-string v0, "\u01a8\u01da\u01e9\u01e2\u01a8\u01e9\u01bc\u01e5\u01e8\u01ee\u01dd\u01a8\u01dd\u01de\u01ef\u01e2\u01dc\u01de\u01a8\u01ec\u01de\u01dc\u01ee\u01eb\u01de\u01dd\u01a8\u01e0\u01de\u01e7\u01de\u01eb\u01da\u01ed\u01de\u01cd\u01e8\u01e4\u01de\u01e7"
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    const/16 v1, 0x7c

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v2

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v3

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    if-eq v2, v3, :cond_0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v2

    sput v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v2

    sput v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :cond_0
    const/16 v2, 0xfd

    const/4 v3, 0x3

    :try_start_9
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/wifiPke/WifiPke;->GENERATESECURETOKEN:Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    sget v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    if-eq v0, v1, :cond_1

    const/16 v0, 0x18

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v0

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :cond_1
    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static b043E043E043Eо043Eо()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static b043Eо043Eо043Eо()I
    .locals 1

    const/16 v0, 0x17

    return v0
.end method

.method public static bоо043Eо043Eо()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public static getWifiPkeInstance()Lcom/sec/android/safe/wifiPke/WifiPke;
    .locals 2

    :try_start_0
    sget-object v0, Lcom/sec/android/safe/wifiPke/WifiPke;->mWifiPke:Lcom/sec/android/safe/wifiPke/WifiPke;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v0

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x4

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    const/16 v0, 0x20

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :pswitch_0
    const/4 v0, 0x1

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043E043Eо043Eо()I

    move-result v1

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_2

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v0

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    const/16 v0, 0x49

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :cond_0
    :pswitch_2
    :try_start_1
    new-instance v0, Lcom/sec/android/safe/wifiPke/WifiPke;

    invoke-direct {v0}, Lcom/sec/android/safe/wifiPke/WifiPke;-><init>()V

    sput-object v0, Lcom/sec/android/safe/wifiPke/WifiPke;->mWifiPke:Lcom/sec/android/safe/wifiPke/WifiPke;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :try_start_2
    sget-object v0, Lcom/sec/android/safe/wifiPke/WifiPke;->mWifiPke:Lcom/sec/android/safe/wifiPke/WifiPke;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method private parseMsg(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 9

    const/4 v8, 0x3

    const/4 v6, 0x1

    const/4 v7, 0x6

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v0, 0x5

    invoke-virtual {p1, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\ufe68\ufe6a"

    const/16 v2, 0xe4

    const/4 v3, 0x7

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "?QNQ8SM"

    const/16 v1, 0x49

    const/16 v2, 0x31

    invoke-static {v0, v1, v2, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uff8a\uffac\uffac\uffab\uffb6\uffb0\uff5d\uffac\uffab\uff8a\uffa2\uffb0\uffb0\uff9e\uffa4\uffa2\uff8f\uffa2\uff9e\uffa1\uff77\uff5d\uff90\uffa2\uffaf\uffb3\uffa2\uffaf\uff5d\uff85\uffa2\uffa9\uffa9\uffac\uff5d\uffa6\uffb0\uff5d\uffaf\uffa2\uffa0\uffa2\uffa6\uffb3\uffa2\uffa1"

    const/16 v2, 0x41

    invoke-static {v1, v2, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v1

    array-length v2, v0

    invoke-virtual {v1, p3, v0, v2, p2}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCParseClientHello(I[BILjava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "\uff5a\uff6c\uff69\uff6c\uff53\uff6e\uff68"

    const/16 v1, 0x10

    const/16 v2, 0xed

    invoke-static {v0, v1, v2, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uff63\uff85\uff85\uff84\uff8f\uff89\uff36\uff69\uff7b\uff88\uff8c\uff7b\uff88\uff36\uff7e\uff7b\uff82\uff82\uff85\uff36\uff7f\uff89\uff36\uff8c\uff77\uff82\uff7f\uff7a\uff77\uff8a\uff7b\uff7a\uff42\uff36\uff84\uff85\uff8d\uff36\uff89\uff7b\uff84\uff7a\uff36\uff57\uff59\uff61\uff36\uff8a\uff85\uff36\uff69\uff7b\uff88\uff8c\uff7b\uff88"

    const/16 v2, 0x15

    const/16 v3, 0xd5

    invoke-static {v1, v2, v3, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v0

    invoke-virtual {v0, p3, p2}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCGeneratorServerAck(ILjava/lang/String;)[B

    move-result-object v0

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    const/16 v1, 0x58

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :cond_0
    if-nez v0, :cond_7

    const-string v1, "\u00ff\u0111\u010e\u0111\u00f8\u0113\u010d"

    const/16 v2, 0x54

    invoke-static {v1, v2, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u00dc\u0010\u001a\u00fe\u0093bS#\u0016\u00e4\u00c5\u00bb\u00b9\u0097p:\u00967TEU\u00ed\u00c1G\u00a6z\u00000\u0000\u00e29+\u00b1n\u0008\u00f6b\u00b8a\u00b4\\zO\u009e\u00d2\u00a3\u00a6I\u0099\u0002Hb\u00b4\u0099"

    const/16 v4, 0xb9

    invoke-static {v3, v4, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const-string v1, "\u01b8\u01bc"

    const/16 v2, 0xc4

    invoke-static {v1, v2, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v1

    array-length v2, v0

    invoke-virtual {v1, p3, v0, v2, p2}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCParseClientAck(I[BILjava/lang/String;)I

    :cond_2
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const-string v1, "\ufeb0\ufeb6"

    const/16 v2, 0xc0

    const/4 v3, 0x7

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v1

    array-length v2, v0

    invoke-virtual {v1, v0, v2, p2}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCParseAckMsg([BILjava/lang/String;)I

    goto :goto_1

    :cond_4
    invoke-static {v0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u0085\u0085lbel"

    const/16 v3, 0x24

    const/16 v4, 0x56

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const-string v0, "\uff46\uff58\uff55\uff58\uff3f\uff5a\uff54"

    const/16 v1, 0x5b

    invoke-static {v0, v1, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u0087\u00a5\u00a5\u00a4\u00b3\u00b9\u00ea\u0099\u00af\u00b8\u00bc\u00af\u00b8\u00ea\u00a2\u00af\u00a6\u00a6\u00a5\u00ea\u00a2\u00ab\u00b9\u00ea\u00a3\u00a4\u00bc\u00ab\u00a6\u00a3\u00ae\u00ea\u00ae\u00ab\u00be\u00ab\u00e6\u00ea\u008c\u008b\u0083\u0086\u00b9\u00e4\u00e4\u00e4"

    const/16 v2, 0xca

    invoke-static {v1, v2, v8}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v0

    invoke-virtual {v0, p3, p2}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCGeneratorServerAck(ILjava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_4

    const-string v1, "\u00b6\u00c8\u00c5\u00c8\u00af\u00ca\u00c4"

    const/16 v2, 0x5f

    const/4 v3, 0x5

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u01d9\u01fb\u01fb\u01fa\u0205\u01ff\u01c6\u01ac\u01df\u01dc\u01cf\u01d3\u01f1\u01fa\u01f1\u01fe\u01ed\u0200\u01fb\u01fe\u01df\u01f1\u01fe\u0202\u01f1\u01fe\u01cd\u01ef\u01f7\u01ac\u01d2\u01cd\u01d5\u01d8\u01ff\u01ba\u01ba\u01ac\u01ee\u0205\u0200\u01f1\u01de\u01f1\u0200\u01e2\u01ed\u01f8\u0201\u01f1\u01ac\u01f5\u01ff\u01ac"

    const/16 v4, 0x84

    sget v5, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v6, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v5, v6

    packed-switch v5, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v5

    sput v5, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v5

    sput v5, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :pswitch_0
    invoke-static {v3, v4, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043E043Eо043Eо()I

    move-result v3

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    if-eq v2, v3, :cond_6

    const/16 v2, 0x40

    sput v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v2

    sput v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_7
    invoke-static {v0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u0100\u0100\u00e7\u00dd\u00e0\u00e7"

    const/16 v3, 0x88

    const/16 v4, 0x25

    invoke-static {v2, v3, v4, v8}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private readByteArray(Ljava/io/InputStream;)Lorg/json/JSONObject;
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_12
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_11
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_a

    :cond_2
    :goto_2
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v2, v0

    :goto_3
    :try_start_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v3, v1

    mul-int/2addr v1, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v1, v3

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :pswitch_0
    if-eqz v2, :cond_0

    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    throw v0

    :catch_3
    move-exception v1

    :try_start_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_2

    :catch_5
    move-exception v1

    move-object v2, v0

    :goto_4
    :try_start_7
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v2, :cond_0

    :try_start_8
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_0

    :catch_6
    move-exception v1

    :try_start_9
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    goto :goto_0

    :catch_7
    move-exception v1

    move-object v2, v0

    :goto_5
    :try_start_a
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    if-eqz v2, :cond_0

    :try_start_b
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2

    goto :goto_0

    :catch_8
    move-exception v1

    move-object v2, v0

    :goto_6
    :try_start_c
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    if-eqz v2, :cond_0

    :try_start_d
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    const/16 v1, 0x1d

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    goto/16 :goto_0

    :catch_9
    move-exception v1

    :try_start_e
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_7
    if-eqz v2, :cond_3

    :try_start_f
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_e
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_a

    :cond_3
    :goto_8
    :try_start_10
    throw v0
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_a

    :catch_a
    move-exception v0

    throw v0

    :cond_4
    :try_start_11
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_b
    .catch Lorg/json/JSONException; {:try_start_11 .. :try_end_11} :catch_f
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_10
    .catch Ljava/lang/OutOfMemoryError; {:try_start_11 .. :try_end_11} :catch_12
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/16 :goto_1

    :catch_b
    move-exception v1

    goto/16 :goto_3

    :catch_c
    move-exception v1

    :try_start_12
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_2

    goto/16 :goto_0

    :catch_d
    move-exception v1

    move-object v2, v0

    :goto_9
    :try_start_13
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    if-eqz v2, :cond_0

    :try_start_14
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_c
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_a

    goto/16 :goto_0

    :catch_e
    move-exception v1

    :try_start_15
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_a

    goto :goto_8

    :catch_f
    move-exception v1

    goto :goto_9

    :catch_10
    move-exception v1

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_7

    :catch_11
    move-exception v1

    goto :goto_6

    :catch_12
    move-exception v1

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public SPCGeneratorTokenReq(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v1

    array-length v2, v0

    invoke-virtual {v1, v0, v2, p2}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCGeneratorTokenReq([BILjava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "As\u0082{A\u0082U~\u0081\u0087vAvw\u0088{uwA\u0085wu\u0087\u0084wvAyw\u0080w\u0084s\u0086wf\u0081}w\u0080Qw\u0080u\u0084\u008b\u0082\u0086wvf\u0081}w\u0080O"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v5

    sget v6, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    rem-int/2addr v5, v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    packed-switch v5, :pswitch_data_0

    :try_start_3
    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v5

    sput v5, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v5

    sput v5, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :pswitch_0
    add-int/2addr v3, v4

    :try_start_4
    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    sget v5, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v6, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    if-eq v5, v6, :cond_0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v5

    sput v5, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v5

    sput v5, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :cond_0
    if-eq v3, v4, :cond_1

    :try_start_5
    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v3

    sput v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    const/16 v3, 0x2b

    sput v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :cond_1
    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const/16 v2, 0xa

    :try_start_6
    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    const-string v0, "\uff09\uff36\uff36\uff33\uff36"
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    const/16 v1, 0x9e

    const/4 v2, 0x7

    :try_start_7
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u00c6\u00fa\u00f7\u00b2\u00e4\u00f3\u00fe\u00e7\u00f7\u00b2\u00fd\u00f4\u00b2\u00e6\u00fd\u00f9\u00f7\u00fc\u00b2\u00f5\u00f7\u00fc\u00f7\u00e0\u00f3\u00e6\u00fd\u00e0\u00b2\u00fb\u00e1\u00b2\u00fc\u00e7\u00fe\u00fe"

    const/16 v2, 0x92

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    :pswitch_1
    const/4 v0, 0x1

    packed-switch v0, :pswitch_data_1

    :goto_1
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_2

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public SPCStoreToken(Ljava/io/InputStream;Ljava/lang/String;)I
    .locals 5

    const/4 v0, -0x1

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :pswitch_0
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/safe/wifiPke/WifiPke;->readByteArray(Ljava/io/InputStream;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    const-string v2, "\u013b\u012e\u013c\u013e\u0135\u013d"
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v3, 0x43

    const/4 v4, 0x6

    :try_start_2
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "\u010a\u00fd\u010b\u010d\u0104\u010c"
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v3, 0x4c

    const/4 v4, 0x2

    :try_start_3
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v2

    :try_start_4
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v1

    :try_start_5
    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v2

    array-length v3, v1

    invoke-virtual {v2, v1, v3, p2}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCStoreToken([BILjava/lang/String;)I
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move-result v0

    :try_start_6
    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_1

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    const/16 v1, 0x50

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :cond_0
    :goto_0
    :pswitch_1
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public SpcIsTokenExist(Ljava/lang/String;)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x2

    const/4 v2, 0x0

    :pswitch_0
    packed-switch v5, :pswitch_data_0

    :goto_0
    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v3

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_1

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v3

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_2

    const/16 v3, 0x63

    sput v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    const/16 v3, 0x46

    sput v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :pswitch_1
    const/16 v3, 0xf

    sput v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v3

    sput v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :pswitch_2
    const/4 v3, 0x1

    packed-switch v3, :pswitch_data_3

    goto :goto_0

    :goto_1
    :pswitch_3
    :try_start_0
    div-int/2addr v0, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v0

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCIsTokenExist(Ljava/lang/String;)Z

    move-result v0

    return v0

    :catch_1
    move-exception v0

    const/16 v0, 0x5c

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    :goto_2
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v0

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    :goto_3
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public parseDSAPassword(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x6

    const/4 v4, 0x0

    if-nez p1, :cond_0

    const-string v0, "\u00f3\u0120\u0120\u011d\u0120"

    const/16 v1, 0xe9

    const/16 v2, 0x3b

    invoke-static {v0, v1, v2, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u00b0\u00d2\u00d2\u00d1\u00dc\u00d6\u009d\u0083\u00d3\u00c4\u00d5\u00d6\u00c8\u00a7\u00b6\u00a4\u00b3\u00c4\u00d6\u00d6\u00da\u00d2\u00d5\u00c7\u0083\u00a9\u00a4\u00ac\u00af\u00d6\u0091\u0091\u0083\u00d0\u00d6\u00ca\u0083\u00cc\u00d6\u0083"

    const/16 v3, 0x21

    invoke-static {v2, v3, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "\u008f\u00d5\u00d8\u00d0\u00d2\u00d7\u00ac\u00d8\u00de\u00d7\u00dd\u00a6"

    const/16 v2, 0x69

    :pswitch_1
    packed-switch v4, :pswitch_data_0

    :goto_1
    packed-switch v4, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    const/4 v3, 0x5

    invoke-static {v0, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, "\uff61\uff93\uffa2\uff9b\uff61\uffa2\uff75\uff9e\uffa1\uffa7\uff96\uff61\uff96\uff97\uffa8\uff9b\uff95\uff97\uff61\uff93\uff95\uff95\uffa1\uffa7\uffa0\uffa6\uff61\uffa8\uff97\uffa4\uff9b\uff98\uffab\uff82\uff93\uffa5\uffa5\uffa9\uffa1\uffa4\uff96"

    const/16 v1, 0xce

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    if-eq v2, v3, :cond_2

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043E043Eо043Eо()I

    move-result v3

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    if-eq v2, v3, :cond_1

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v2

    sput v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v2

    sput v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :cond_1
    const/16 v2, 0x4a

    sput v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    const/16 v2, 0x4f

    sput v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_2

    const/16 v2, 0x40

    sput v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v2

    sput v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :pswitch_3
    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "\u00c0\u00f1\u00e2\u00f4\u00f4\u00f8\u00f0\u00f3\u00e5\u00be"

    const/16 v2, 0x2b

    invoke-static {v0, v2, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    :pswitch_4
    const/4 v2, 0x1

    packed-switch v2, :pswitch_data_3

    :goto_2
    packed-switch v4, :pswitch_data_4

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public processResponse(Ljava/io/InputStream;Ljava/lang/String;I)Ljava/lang/String;
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-direct {p0, p1}, Lcom/sec/android/safe/wifiPke/WifiPke;->readByteArray(Ljava/io/InputStream;)Lorg/json/JSONObject;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    :try_start_0
    const-string v2, "\ufffa\uffed\ufffb\ufffd\ufff4\ufffc"

    const/16 v3, 0x5a

    const/16 v4, 0xd2

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "\uff1c\uff0f\uff1d\uff1f\uff16\uff1e"

    const/16 v3, 0x72

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "\u0089\u0084\u008c\u008f\u0098\u0095\u0088"

    const/16 v3, 0x2e

    const/16 v4, 0x71

    const/4 v5, 0x2

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "\u0013\u000e\u0016\u0019\"\u001f\u0012"

    :pswitch_0
    packed-switch v7, :pswitch_data_0

    :goto_0
    packed-switch v7, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    packed-switch v8, :pswitch_data_2

    :goto_1
    packed-switch v8, :pswitch_data_3

    goto :goto_1

    :pswitch_2
    const/16 v1, 0xb5

    const/16 v2, 0xe8

    sget v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_4

    const/16 v3, 0x3d

    sput v3, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v3

    sput v3, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :pswitch_3
    invoke-static {v0, v1, v2, v7}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_2
    :pswitch_4
    return-object v0

    :cond_1
    :try_start_1
    invoke-direct {p0, v1, p2, p3}, Lcom/sec/android/safe/wifiPke/WifiPke;->parseMsg(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    const-string v2, ",9969"

    const/16 v3, 0x19

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v5, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v5, v4

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v4, v5

    packed-switch v4, :pswitch_data_5

    const/16 v4, 0x14

    sput v4, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    const/16 v4, 0x27

    sput v4, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :pswitch_5
    :try_start_2
    const-string v4, "\u01b0\u01d2\u01d2\u01d1\u01dc\u01d6\u019d\u0183\u01d3\u01c4\u01d5\u01d6\u01c8\u01b0\u01d6\u01ca\u0183\u01a9\u01a4\u01ac\u01af\u01d6\u0191\u0191\u0183\u01d0\u01d6\u01ca\u0183\u01cc\u01d6\u0183"

    const/16 v5, 0x69

    const/16 v6, 0xfa

    const/4 v7, 0x3

    invoke-static {v4, v5, v6, v7}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    :pswitch_6
    :try_start_3
    const-string v5, "Eggfqk\u0018gfE]kkY_]J]Y\\2"

    const/4 v6, 0x4

    const/4 v7, 0x7

    invoke-static {v5, v6, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x6

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\u01fc\u0248\u0241\u024a\u0243\u0244\u0250\u0216"

    const/16 v6, 0xee

    const/4 v7, 0x2

    invoke-static {v5, v6, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v2, :cond_3

    const-string v1, "xzhhjxx"

    const/16 v2, 0xc0

    const/16 v3, 0x9b

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_2
    const/4 v2, 0x3

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u00b1\u00b5"

    const/16 v4, 0x81

    const/4 v5, 0x5

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v3

    array-length v4, v2

    invoke-virtual {v3, p3, v2, v4, p2}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCParseClientAck(I[BILjava/lang/String;)I

    move-result v2

    const-string v3, "\uff81\uff93\uff90\uff93\uff7a\uff95\uff8f"

    const/16 v4, 0x7c

    const/16 v5, 0x5a

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    :pswitch_7
    packed-switch v7, :pswitch_data_6

    :goto_3
    packed-switch v8, :pswitch_data_7

    goto :goto_3

    :cond_3
    const-string v1, "\ufeb7\ufeb2\ufeba\ufebd\ufec6\ufec3\ufeb6"

    const/16 v2, 0x85

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v0

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_8

    sput v9, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    goto/16 :goto_2

    :cond_4
    move-object v0, v1

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public verifyDSAPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 9

    const/4 v6, 0x0

    const/4 v2, 0x3

    const/4 v0, 0x2

    const/4 v8, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    const-string v4, "\u00068G@\u0006G\u001aCFL;\u0006;<M@:<\u00068::FLEK\u0006M<I@=P\'8JJNFI;"

    const/16 v5, 0x7f

    const/16 v7, 0x56

    invoke-static {v4, v5, v7, v0}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v7

    :goto_0
    :try_start_0
    div-int/2addr v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v1, v4

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    mul-int/2addr v1, v4

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v1, v4

    sget v4, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    if-eq v1, v4, :cond_0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v1

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    const/16 v1, 0x14

    sput v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "\u0175\u01a6\u0197\u01a9\u01a9\u01ad\u01a5\u01a8\u019a\u0173"

    const/16 v5, 0xc6

    const/16 v6, 0x70

    invoke-static {v4, v5, v6, v2}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "\uff36\uff7c\uff7f\uff77\uff79\uff7e\uff53\uff7f\uff85\uff7e\uff84\uff4d"

    const/16 v2, 0x26

    const/16 v4, 0xca

    invoke-static {v0, v2, v4, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v0

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v0

    invoke-virtual {v0, p2, p5, v3}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCCreateSecureSession(Ljava/lang/String;Ljava/lang/String;I)I

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v0

    move v1, p4

    move-object v4, p1

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCGeneratorServerHello(IIILjava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v6

    goto :goto_1

    :goto_2
    :pswitch_1
    packed-switch v8, :pswitch_data_0

    :goto_3
    sget v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043Eоо043Eо:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/wifiPke/WifiPke;->b043E043Eоо043Eо:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_1

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v0

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eооо043Eо:I

    invoke-static {}, Lcom/sec/android/safe/wifiPke/WifiPke;->b043Eо043Eо043Eо()I

    move-result v0

    sput v0, Lcom/sec/android/safe/wifiPke/WifiPke;->bо043E043Eо043Eо:I

    goto :goto_2

    :cond_1
    :pswitch_2
    packed-switch v3, :pswitch_data_2

    goto :goto_3

    :pswitch_3
    move-object v0, v6

    goto :goto_1

    :cond_2
    invoke-static {v0, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "\u00d4\u00d4\u00bb\u00b1\u00b2\u00bb"

    const/16 v5, 0x2b

    const/4 v6, 0x6

    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :pswitch_4
    packed-switch v3, :pswitch_data_3

    :goto_4
    packed-switch v8, :pswitch_data_4

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
