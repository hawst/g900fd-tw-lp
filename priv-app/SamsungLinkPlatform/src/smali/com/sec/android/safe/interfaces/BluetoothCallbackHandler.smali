.class public Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;
.super Ljava/lang/Object;


# static fields
.field public static b044Eю044Eюю044E:I = 0x1

.field public static bю044E044Eюю044E:I = 0x2

.field public static bюю044Eюю044E:I = 0x8

.field public static bююю044Eю044E:I


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mBluetoothManager:Lcom/sec/android/safe/manager/BluetoothManager;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    :try_start_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    const-string v0, "[\u0085\u008e~\u008d\u0088\u0088\u008d\u0081\\z\u0085\u0085{z|\u0084az\u0087}\u0085~\u008b"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/16 v1, 0xb2

    sget v2, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->bюю044Eюю044E:I

    invoke-static {}, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->b044Eюю044Eю044E()I

    move-result v3

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->bюю044Eюю044E:I

    mul-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->bю044Eю044Eю044E()I

    move-result v3

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->bююю044Eю044E:I

    if-eq v2, v3, :cond_0

    invoke-static {}, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->b044E044E044Eюю044E()I

    move-result v2

    sput v2, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->bюю044Eюю044E:I

    const/16 v2, 0x2d

    sput v2, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->bююю044Eю044E:I

    :cond_0
    const/16 v2, 0x99

    const/4 v3, 0x0

    :try_start_2
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    :try_start_3
    iput-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0
.end method

.method public static b044E044E044Eюю044E()I
    .locals 1

    const/16 v0, 0xa

    return v0
.end method

.method public static b044Eюю044Eю044E()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static bю044Eю044Eю044E()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method


# virtual methods
.method public onMessageDeviceName(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    :pswitch_0
    packed-switch v2, :pswitch_data_0

    :goto_0
    packed-switch v2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    check-cast v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    :pswitch_2
    packed-switch v1, :pswitch_data_2

    :goto_1
    packed-switch v1, :pswitch_data_3

    goto :goto_1

    :pswitch_3
    invoke-virtual {v0, p1}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;->onMessageDeviceName(Ljava/lang/String;)V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public onMessageRead(Ljava/lang/String;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    :pswitch_0
    const/4 v1, 0x0

    packed-switch v1, :pswitch_data_0

    :goto_0
    const/4 v1, 0x1

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, p1}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;->onMessageRead(Ljava/lang/String;)V

    :cond_0
    const-string v0, ""

    new-instance v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;

    invoke-direct {v0}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->parse(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    monitor-exit p0

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mBluetoothManager:Lcom/sec/android/safe/manager/BluetoothManager;

    invoke-virtual {v0}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->getReqType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/safe/manager/BluetoothManager;->getReqType(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->getResponseType()I

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "\u0122\u0120\u012f\u00ff\u0120\u0131\u0124\u011e\u0120\u0104\u011f"

    const/16 v3, 0xbb

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->getDeviceId()Ljava/lang/String;

    :cond_2
    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mBluetoothManager:Lcom/sec/android/safe/manager/BluetoothManager;

    if-nez v1, :cond_1

    const-string v0, "\uffab\uffd5\uffde\uffce\uffdd\uffd8\uffd8\uffdd\uffd1\uffac\uffca\uffd5\uffd5\uffcb\uffca\uffcc\uffd4\uffb1\uffca\uffd7\uffcd\uffd5\uffce\uffdb"

    const/16 v1, 0x97

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u0247\u021c\u0246\u024f\u023f\u024e\u0249\u0249\u024e\u0242\u0227\u023b\u0248\u0241\u023f\u024c\u01fa\u0243\u024d\u01fa\u0248\u024f\u0246\u0246"

    const/16 v2, 0x9e

    const/4 v3, 0x6

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onMessageToast(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    :pswitch_0
    packed-switch v1, :pswitch_data_0

    :goto_0
    packed-switch v2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    invoke-virtual {v0, p1}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;->onMessageToast(Ljava/lang/String;)V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :pswitch_2
    packed-switch v1, :pswitch_data_2

    :goto_1
    packed-switch v1, :pswitch_data_3

    goto :goto_1

    :goto_2
    :pswitch_3
    packed-switch v1, :pswitch_data_4

    :goto_3
    packed-switch v2, :pswitch_data_5

    goto :goto_3

    :pswitch_4
    throw v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onMessageWrite(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    :pswitch_0
    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_1
    packed-switch v1, :pswitch_data_1

    :goto_1
    packed-switch v0, :pswitch_data_2

    goto :goto_1

    :pswitch_2
    packed-switch v1, :pswitch_data_3

    goto :goto_0

    :pswitch_3
    packed-switch v0, :pswitch_data_4

    :goto_2
    packed-switch v1, :pswitch_data_5

    goto :goto_2

    :pswitch_4
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    invoke-virtual {v0, p1}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;->onMessageWrite(Ljava/lang/String;)V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onRequestTimeout()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;->onRequestTimeout()V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :pswitch_0
    packed-switch v2, :pswitch_data_0

    :goto_0
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    packed-switch v1, :pswitch_data_2

    :goto_1
    :pswitch_2
    packed-switch v1, :pswitch_data_3

    :goto_2
    packed-switch v2, :pswitch_data_4

    goto :goto_2

    :pswitch_3
    packed-switch v1, :pswitch_data_5

    goto :goto_1

    :pswitch_4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onStateConnected()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v0, "\u0002,5%4//4(\u0003!,,\"!#+\u0008!.$,%2"

    const/16 v1, 0x40

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u0135\u0134\u0119\u013a\u0127\u013a\u012b\u0109\u0135\u0134\u0134\u012b\u0129\u013a\u012b\u012a"

    const/16 v2, 0x42

    const/4 v3, 0x6

    :pswitch_0
    packed-switch v5, :pswitch_data_0

    :goto_0
    :pswitch_1
    packed-switch v4, :pswitch_data_1

    :goto_1
    packed-switch v4, :pswitch_data_2

    goto :goto_1

    :pswitch_2
    packed-switch v4, :pswitch_data_3

    goto :goto_0

    :pswitch_3
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :pswitch_4
    packed-switch v4, :pswitch_data_4

    :goto_2
    packed-switch v5, :pswitch_data_5

    goto :goto_2

    :pswitch_5
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;->onStateConnected()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public onStateConnecting()V
    .locals 2

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;->onStateConnecting()V

    :cond_0
    :pswitch_0
    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_1
    packed-switch v1, :pswitch_data_1

    :goto_1
    packed-switch v1, :pswitch_data_2

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    :pswitch_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public onStateDisconnected()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    :pswitch_0
    packed-switch v0, :pswitch_data_0

    :goto_0
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    packed-switch v1, :pswitch_data_2

    :goto_1
    :pswitch_2
    packed-switch v1, :pswitch_data_3

    :goto_2
    packed-switch v0, :pswitch_data_4

    goto :goto_2

    :pswitch_3
    packed-switch v0, :pswitch_data_5

    goto :goto_1

    :pswitch_4
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;->onStateDisconnected()V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public onStateFailToConnect(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    :pswitch_0
    packed-switch v1, :pswitch_data_0

    :goto_0
    packed-switch v2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    invoke-virtual {v0, p1}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;->onStateFailToConnect(Ljava/lang/String;)V

    :cond_0
    monitor-exit p0

    return-void

    :pswitch_2
    packed-switch v2, :pswitch_data_2

    :goto_1
    :pswitch_3
    packed-switch v1, :pswitch_data_3

    :goto_2
    packed-switch v1, :pswitch_data_4

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :pswitch_4
    packed-switch v1, :pswitch_data_5

    goto :goto_1

    :pswitch_5
    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public onStateListen()V
    .locals 2

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;->onStateListen()V

    :cond_0
    monitor-exit p0

    :pswitch_0
    packed-switch v1, :pswitch_data_0

    :goto_0
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onStateNone()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;->onStateNone()V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
