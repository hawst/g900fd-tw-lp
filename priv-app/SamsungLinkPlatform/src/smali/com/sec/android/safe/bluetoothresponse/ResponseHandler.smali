.class public Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;
    }
.end annotation


# static fields
.field public static final FAILED:Ljava/lang/String; = ":5=@98"

.field private static final KEY_ADMIN_STUN_ID:Ljava/lang/String; = "\u009c\u009f\u00a8\u00a4\u00a9\u0089\u00af\u00ae\u008e\u00af\u00b0\u00a9"

.field private static final KEY_BSSID_ID:Ljava/lang/String; = "\u0087\u0096\u0096\u008c\u0081"

.field private static final KEY_BT_PASSWD_ID:Ljava/lang/String; = "\uff3a\uff44\uff4d\uff3d\uff4c\uff47\uff47\uff4c\uff40\uff48\uff39\uff4b\uff4b\uff4f\uff47\uff4a\uff3c"

.field public static final KEY_CONFIRM_MSG_OK:Ljava/lang/String; = "\u0085\u0081"

.field public static final KEY_CONFIRM_MSG_TIME_OUT:Ljava/lang/String; = "\uffe3\uffd8\uffdc\uffd4\uffee\uffde\uffe4\uffe3"

.field public static final KEY_CONFIRM_RESULT_FAIL:Ljava/lang/String; = "\u0160\u015b\u0163\u0166"

.field public static final KEY_CONFIRM_RESULT_SUCCESS:Ljava/lang/String; = "\uff05\uff07\ufef5\ufef5\ufef7\uff05\uff05"

.field private static final KEY_DETAILED_STATUS_ID:Ljava/lang/String; = "QRaNVYRQ@aNab`"

.field private static final KEY_DEVICE_ID:Ljava/lang/String; = "\u00ec\u00ed\u00fe\u00f1\u00eb\u00ed\u00d1\u00ec"

.field public static final KEY_ERROR_AUTHENTICATION:Ljava/lang/String; = "\u0012\u001f\u001f\u001c\u001f,\u000e\"!\u0015\u0012\u001b!\u0016\u0010\u000e!\u0016\u001c\u001b"

.field public static final KEY_ERROR_CONNECTIONFAIL:Ljava/lang/String; = "\u00db\u00e8\u00e8\u00e5\u00e8\u00f5\u00d9\u00e5\u00e4\u00e4\u00db\u00d9\u00ea\u00df\u00e5\u00e4\u00dc\u00d7\u00df\u00e2"

.field private static final KEY_ERROR_ID:Ljava/lang/String; = "Y\u000b_\u0090\u00b9/\u00c1\u00a1s"

.field private static final KEY_ERROR_REASON_ID:Ljava/lang/String; = "\ufed6\ufec9\ufec5\ufed7\ufed3\ufed2"

.field private static final KEY_IS_ETHERNETUP_ID:Ljava/lang/String; = "\u009e\u00a8z\u00a9\u009d\u009a\u00a7\u00a3\u009a\u00a9\u008a\u00a5"

.field private static final KEY_IS_READY_ID:Ljava/lang/String; = " \u0018u\u00bd\u0096\u00ad\u00cfk\u0006\u00d2\u00e4\u00ac\u008a\'F\u009a"

.field private static final KEY_MESSAGE_TYPE_ID:Ljava/lang/String; = "#\u001b))\u0017\u001d\u001b\n/&\u001b"

.field private static final KEY_NETWORK_TYPE_ID:Ljava/lang/String; = "\u00e8\u00df\u00ee\u00f1\u00e9\u00ec\u00e5\u00ce\u00f3\u00ea\u00df"

.field private static final KEY_NOTIFY_RESULT_ID:Ljava/lang/String; = "\uffa3\uffa4\uffa9\uff9e\uff9b\uffae\uff87\uff9a\uffa8\uffaa\uffa1\uffa9"

.field private static final KEY_NOTIFY_TYPE_ID:Ljava/lang/String; = "\u00ac\u00ad\u00b6\u00ab\u00a4\u00bb\u0096\u00bb\u00b2\u00a7"

.field private static final KEY_PAYLOAD:Ljava/lang/String; = "\u00a8\u0099\u00b1\u0084\u00a7\u0099\u009c"

.field private static final KEY_REQEST_ID:Ljava/lang/String; = "\uff60\uff53\uff5f\uff63\uff53\uff61\uff62\uff37\uff52"

.field private static final KEY_RESPONSE_MSG:Ljava/lang/String; = "\uffca\uffbd\uffcb\uffc8\uffc7\uffc6\uffcb\uffbd\uffa5\uffcb\uffbf"

.field private static final KEY_RESPONSE_RESULT:Ljava/lang/String; = "F9GDCBG9&9GI@H"

.field private static final KEY_SESSION_KEY_ID:Ljava/lang/String; = "\u009b\u008d\u009b\u009b\u0091\u0097\u0096s\u008d\u00a1"

.field private static final KEY_TRY_REMAINED_ID:Ljava/lang/String; = "\u0218\u0216\u021d\u01f6\u0209\u0211\u0205\u020d\u0212\u0209\u0208"

.field private static final KEY_WIFI_MAC_ID:Ljava/lang/String; = "\uff0d\ufeff\ufefc\ufeff\uff03\ufef7\ufef9"

.field public static final RES_TYPE_ERROR:I = 0x2

.field public static final RES_TYPE_MESSAGE:I = 0x1

.field public static final RES_TYPE_REQUEST:I = 0x0

.field public static final SUCCESS:Ljava/lang/String; = "\u00e3\u00e5\u00d3\u00d3\u00d5\u00e3\u00e3"

.field private static final TAG:Ljava/lang/String; = "\u0014\'52105\'\n#0&.\'4"

.field public static final TYPE_AUTHCODE_VALIDATION:Ljava/lang/String; = "\u01ee\u0202\u0201\u01f5\u01f0\u01fc\u01f1\u01f2\u020c\u0203\u01ee\u01f9\u01f6\u01f1\u01ee\u0201\u01f6\u01fc\u01fb"

.field public static final TYPE_CONNECTING_TO_AP:Ljava/lang/String; = "\u0198\u01a4\u01a3\u01a3\u019a\u0198\u01a9\u019e\u01a3\u019c\u01b4\u01a9\u01a4\u01b4\u0196\u01a5"

.field public static final TYPE_NTS_LOGIN:Ljava/lang/String; = "\u00ec\u00f6\u00f1\u00fd\u00ee\u00ed\u00e5\u00eb\u00ec"

.field public static final TYPE_USER_CONFIRM:Ljava/lang/String; = "64&3@$0/\'*3."

.field public static b041104110411ББ0411:I = 0x0

.field public static b0411Б0411ББ0411:I = 0x1

.field public static bБ04110411ББ0411:I = 0x2

.field public static bББ0411ББ0411:I = 0x5f


# instance fields
.field private mDetailedStatus:Ljava/lang/String;

.field private mMessageType:Ljava/lang/String;

.field private mNotifyResult:Ljava/lang/String;

.field private mNotifyType:Ljava/lang/String;

.field private mPayLoad:Ljava/lang/String;

.field private mRequestId:I

.field private mResponseMsg:Ljava/lang/String;

.field private mResponseResult:Ljava/lang/String;

.field private mResponseType:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mResponseType:I

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    :pswitch_0
    packed-switch v3, :pswitch_data_0

    :goto_0
    packed-switch v2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_2

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x2a

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_0
    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_3

    const/16 v0, 0x63

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v0, 0x3f

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_2
    packed-switch v2, :pswitch_data_4

    :goto_1
    packed-switch v2, :pswitch_data_5

    goto :goto_1

    :pswitch_3
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b04110411Б0411Б0411()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static b0411ББ0411Б0411()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public static bБ0411Б0411Б0411()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static bБББ0411Б0411()I
    .locals 1

    const/16 v0, 0xd

    return v0
.end method


# virtual methods
.method public getBSSID()Ljava/lang/String;
    .locals 6

    const/4 v5, 0x1

    const/4 v0, 0x0

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v1, :cond_0

    :pswitch_1
    const/4 v1, 0x0

    packed-switch v1, :pswitch_data_1

    :goto_0
    packed-switch v5, :pswitch_data_2

    goto :goto_0

    :goto_1
    :pswitch_2
    return-object v0

    :cond_0
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "\u014b\u015c\u015c\u0152\u014d"

    const/16 v3, 0xe9

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ0411Б0411Б0411()I

    move-result v4

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_3

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_3
    :try_start_2
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto :goto_1

    :pswitch_4
    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b04110411Б0411Б0411()I

    move-result v3

    if-eq v2, v3, :cond_1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_1
    :try_start_3
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    throw v0

    :goto_2
    packed-switch v5, :pswitch_data_4

    goto :goto_2

    :goto_3
    :pswitch_5
    packed-switch v5, :pswitch_data_5

    goto :goto_2

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public getDetailedStatus()Ljava/lang/String;
    .locals 5

    const/4 v4, 0x0

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v0, v1, :cond_1

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    :pswitch_0
    packed-switch v4, :pswitch_data_0

    :goto_0
    packed-switch v4, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411ББ0411Б0411()I

    move-result v1

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_2

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v2, 0x3c

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_2
    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_0
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v0, 0xc

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :pswitch_3
    packed-switch v4, :pswitch_data_3

    :goto_1
    packed-switch v4, :pswitch_data_4

    goto :goto_1

    :pswitch_4
    return-object v0

    :catch_0
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x0

    :pswitch_0
    const/4 v1, 0x1

    packed-switch v1, :pswitch_data_0

    :goto_0
    packed-switch v5, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_2

    const/16 v3, 0x25

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v3, 0x53

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_2
    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_3

    const/16 v1, 0x38

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v1, 0x2a

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_3
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v1, :cond_0

    :goto_1
    :pswitch_4
    return-object v0

    :catch_0
    move-exception v0

    throw v0

    :cond_0
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iget-object v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    :pswitch_5
    packed-switch v5, :pswitch_data_4

    :goto_2
    packed-switch v5, :pswitch_data_5

    goto :goto_2

    :pswitch_6
    const-string v2, "\u021c\u021d\u022e\u0221\u021b\u021d\u0201\u021c"

    const/16 v3, 0xdc

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_6

    const/16 v1, 0x5c

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    goto :goto_1

    :catch_1
    move-exception v1

    :try_start_3
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public getErrorLog()Ljava/lang/String;
    .locals 9

    const/4 v0, 0x0

    const/4 v7, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v1, :cond_0

    :goto_0
    :pswitch_0
    return-object v0

    :cond_0
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "\u00b8\u00c5\u00c5\u00c2\u00c5\u00b6\u00c2\u00b7\u00b8"
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v3, v4

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411ББ0411Б0411()I

    move-result v4

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v3, v4, :cond_1

    const/16 v3, 0x26

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v3, 0x62

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_1
    const/16 v3, 0x8d

    const/16 v4, 0x3a

    const/4 v5, 0x0

    :try_start_2
    invoke-static {v1, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\u00c1"

    const/16 v4, 0x8e

    const/4 v5, 0x7

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\u01cd\u01c0\u01bc\u01ce\u01ca\u01c9"
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    const/16 v4, 0x6a

    :try_start_4
    sget v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    if-eq v5, v6, :cond_2

    const/16 v5, 0x59

    :try_start_5
    sput v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v5

    sput v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    :cond_2
    const/16 v5, 0xf1

    const/4 v6, 0x3

    :try_start_6
    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    move-result-object v0

    :try_start_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    move-result-object v0

    :pswitch_1
    packed-switch v7, :pswitch_data_0

    :goto_1
    packed-switch v7, :pswitch_data_1

    goto :goto_1

    :catch_0
    move-exception v1

    :goto_2
    :try_start_8
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    throw v0

    :catch_3
    move-exception v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getMacaddr()Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;

    if-nez v1, :cond_1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v1

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v1, v2, :cond_0

    const/16 v1, 0x11

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v1, 0x40

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "\uffd0\uffc2\uffbf\uffc2\uffc6\uffba\uffbc"

    const/16 v3, 0x3e

    :pswitch_0
    packed-switch v4, :pswitch_data_0

    :goto_1
    packed-switch v4, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    const/16 v4, 0x69

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getMessageType()Ljava/lang/String;
    .locals 2

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b04110411Б0411Б0411()I

    move-result v1

    if-eq v0, v1, :cond_1

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x8

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_0
    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mMessageType:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    throw v0
.end method

.method public getNetworkType()Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;
    .locals 7

    const/4 v6, 0x6

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;

    if-nez v1, :cond_2

    sget-object v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->NONE:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "\u02c1\u02d0\u02c4\u02c1\u02ce\u02ca\u02c1\u02d0"

    const/16 v2, 0xd4

    invoke-static {v1, v2, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->ETHERNET:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->NONE:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    goto :goto_0

    :cond_2
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ0411Б0411Б0411()I

    move-result v3

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v2, v3, :cond_3

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_3
    :try_start_1
    const-string v2, "\u02c3\u02ba\u02c9\u02cc\u02c4\u02c7\u02c0\u02a9\u02ce\u02c5\u02ba"

    const/16 v3, 0xc7

    const/4 v4, 0x6

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    :goto_1
    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->NONE:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    goto :goto_0

    :pswitch_0
    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->WIFI:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    goto :goto_0

    :cond_4
    const-string v1, "6\u00af\u00dc;"

    const/16 v2, 0xfd

    invoke-static {v1, v2, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :pswitch_1
    packed-switch v5, :pswitch_data_0

    :goto_2
    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ0411Б0411Б0411()I

    move-result v2

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/4 v1, 0x3

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_2
    packed-switch v5, :pswitch_data_2

    goto :goto_2

    :catch_0
    move-exception v1

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_3

    const/16 v2, 0x28

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v2, 0x40

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_3
    packed-switch v5, :pswitch_data_4

    :goto_3
    const/4 v2, 0x0

    packed-switch v2, :pswitch_data_5

    goto :goto_3

    :pswitch_4
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public getNotifyResult()Ljava/lang/String;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mNotifyResult:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    throw v0
.end method

.method public getNotifyType()Ljava/lang/String;
    .locals 2

    :pswitch_0
    const/4 v0, 0x1

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    :try_start_0
    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v0, v1, :cond_0

    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_2

    const/16 v0, 0xf

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v0, 0x43

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_2
    const/16 v0, 0x4d

    :try_start_2
    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mNotifyType:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method public getPassword()Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_0
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "@JSCRMMRFN?QQUMPB"

    const/16 v3, 0xe

    const/16 v4, 0x14

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_3
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public getReqType()I
    .locals 2

    :pswitch_0
    const/4 v0, 0x1

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_2

    const/16 v0, 0x24

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_2
    iget v0, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mRequestId:I

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method public getResponseMsg()Ljava/lang/String;
    .locals 4

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v2, v3, :cond_0

    const/16 v2, 0x5a

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v2, 0x54

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ0411Б0411Б0411()I

    move-result v3

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v2, 0x2e

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_0
    :pswitch_0
    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v0, 0x9

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_1
    :pswitch_1
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_1

    :goto_0
    const/4 v0, 0x1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mResponseMsg:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getResponseResult()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v0, v1, :cond_1

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b04110411Б0411Б0411()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v0, 0x1d

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_0
    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v0, 0x3d

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_1
    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411ББ0411Б0411()I

    move-result v1

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v0, v1, :cond_2

    const/16 v0, 0x5d

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v0, 0x15

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_2
    :pswitch_0
    packed-switch v3, :pswitch_data_0

    :goto_0
    packed-switch v3, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mResponseResult:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getResponseType()I
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_0
    :pswitch_0
    packed-switch v2, :pswitch_data_0

    :goto_0
    packed-switch v3, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    packed-switch v2, :pswitch_data_2

    :goto_1
    packed-switch v3, :pswitch_data_3

    goto :goto_1

    :pswitch_2
    iget v0, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mResponseType:I

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getSessionKey()Ljava/lang/String;
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x2

    :pswitch_0
    packed-switch v5, :pswitch_data_0

    :goto_0
    packed-switch v5, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x0

    :try_start_0
    const-string v0, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v3, v4, :cond_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ0411Б0411Б0411()I

    move-result v4

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v3, v4

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411ББ0411Б0411()I

    move-result v4

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v3, v4, :cond_0

    const/16 v3, 0x24

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v3, 0x10

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    if-nez v3, :cond_1

    :goto_1
    return-object v0

    :catch_0
    move-exception v1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    :try_start_2
    const-string v1, "\ufe91\ufe83\ufe91\ufe91\ufe87\ufe8d\ufe8c\ufe69\ufe83\ufe97"

    const/16 v2, 0xf1

    const/4 v4, 0x7

    invoke-static {v1, v2, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    new-instance v3, Lorg/json/JSONObject;

    iget-object v4, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :pswitch_2
    packed-switch v5, :pswitch_data_2

    :goto_2
    const/4 v4, 0x1

    packed-switch v4, :pswitch_data_3

    goto :goto_2

    :goto_3
    :pswitch_3
    :try_start_3
    div-int/2addr v1, v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    :catch_1
    move-exception v1

    :try_start_4
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    throw v0

    :catch_3
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getTryRemained()I
    .locals 9

    const/4 v0, -0x1

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;

    if-nez v1, :cond_1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v2, "\u021b\u022e\u023c\u0239\u0238\u0237\u023c\u022e\u0211\u022a\u0237\u022d\u0235\u022e\u023b"

    const/16 v3, 0xe1

    const/16 v4, 0xe8

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u0142\u0133\u0144\u0145\u0137\u0124\u0137\u0145\u0142\u0141\u0140\u0145\u0137"

    const/16 v4, 0x69

    const/4 v5, 0x2

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v5

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v5, v6

    packed-switch v5, :pswitch_data_0

    const/16 v5, 0x54

    sput v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v5

    sput v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :pswitch_0
    :try_start_1
    const-string v5, "\u001bHHEH\ufff6F7HI?D=\ufff6:7J7\ufff6"

    const/16 v6, 0xbb

    const/16 v7, 0xe5

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    :try_start_2
    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result v5

    :try_start_3
    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411ББ0411Б0411()I

    move-result v6

    rem-int/2addr v5, v6
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    packed-switch v5, :pswitch_data_1

    const/16 v5, 0x24

    :try_start_4
    sput v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    const/16 v5, 0x5c

    :try_start_5
    sput v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    sget v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v5, v6
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :try_start_6
    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b04110411Б0411Б0411()I

    move-result v6

    if-eq v5, v6, :cond_0

    const/16 v5, 0x37

    sput v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v5, 0x39

    sput v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    :cond_0
    :pswitch_1
    :try_start_7
    invoke-virtual {v1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    throw v0

    :cond_1
    :try_start_8
    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "\u0144\u0142\u0149\u0122\u0135\u013d\u0131\u0139\u013e\u0135\u0134"

    const/16 v3, 0xcc

    const/4 v4, 0x4

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    move-result v0

    goto/16 :goto_0

    :catch_2
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public isEthernetUp()Z
    .locals 7

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;

    if-nez v1, :cond_1

    :pswitch_0
    packed-switch v0, :pswitch_data_0

    :goto_0
    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v1, 0x1b

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_0
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :goto_1
    :pswitch_1
    return v0

    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "\u01e6\u01f0\u01c2\u01f1\u01e5\u01e2\u01ef\u01eb\u01e2\u01f1\u01d2\u01ed"
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v3, 0x88

    const/16 v4, 0xf5

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v5

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v5, v6

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v6

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v5, v6, :cond_2

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v5

    sput v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v5, 0x1d

    sput v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_2
    const/4 v5, 0x3

    :try_start_1
    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_1

    :catch_0
    move-exception v1

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v2, v3, :cond_3

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_3
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isReadyEasySetup()Z
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v1, :cond_0

    :goto_0
    :pswitch_0
    return v0

    :cond_0
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :try_start_2
    const-string v2, "8\u0007u2\u0007*V\u001f>\u00f8\u00b0\u00c9\u00e8#8\u000c"

    const/16 v3, 0x47

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result v0

    :try_start_3
    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v1

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411ББ0411Б0411()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    move-result v2

    :try_start_4
    rem-int/2addr v1, v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    packed-switch v1, :pswitch_data_0

    const/16 v1, 0x2c

    :try_start_5
    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v1, 0x58

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :pswitch_1
    :try_start_6
    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v1

    :pswitch_2
    packed-switch v5, :pswitch_data_1

    :goto_1
    packed-switch v5, :pswitch_data_2

    goto :goto_1

    :pswitch_3
    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_3

    const/16 v1, 0x4a

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v3

    mul-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411ББ0411Б0411()I

    move-result v3

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v2, v3, :cond_1

    const/16 v2, 0xe

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_1
    :try_start_7
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_8
    throw v0

    :catch_3
    move-exception v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    :catch_4
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public parse(Ljava/lang/String;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_3

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    const-string v2, "\u00e3\u00db\u00e9\u00e9\u00d7\u00dd\u00db\u00ca\u00ef\u00e6\u00db"

    const/16 v3, 0x62

    const/16 v4, 0x14

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mResponseType:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "\u0205\u01fd\u020b\u020b\u01f9\u01ff\u01fd\u01ec\u0211\u0208\u01fd"

    const/16 v4, 0xcc

    const/4 v5, 0x2

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mMessageType:Ljava/lang/String;

    const-string v3, "\u011a\u011b\u0120\u0115\u0112\u0125\u0100\u0125\u011c\u0111"
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v4, 0x56

    const/4 v5, 0x2

    :try_start_2
    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mNotifyType:Ljava/lang/String;

    const-string v3, "\u014a\u014b\u0150\u0145\u0142\u0155\u012e\u0141\u014f\u0151\u0148\u0150"

    const/16 v4, 0xdc

    const/4 v5, 0x5

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mNotifyResult:Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    :try_start_3
    const-string v3, "\u01c2\u01c3\u01d2\u01bf\u01c7\u01ca\u01c3\u01c2\u01b1\u01d2\u01bf\u01d2\u01d3\u01d1"

    const/16 v4, 0xd4

    const/16 v5, 0x8a

    const/4 v6, 0x3

    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "@AP=EHA@/P=PQO"

    const/16 v4, 0xc

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mDetailedStatus:Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    :try_start_4
    const-string v2, "\uff4c\uff3f\uff4b\uff4f\uff3f\uff4d\uff4e\uff23\uff3e"

    const/16 v3, 0x93

    const/4 v4, 0x7

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mResponseType:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :try_start_5
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "\ufed2\ufec5\ufed1\ufed5\ufec5\ufed3\ufed4\ufea9\ufec4"

    const/16 v4, 0xd0

    const/4 v5, 0x7

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mRequestId:I
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :try_start_6
    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v3

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v3, v4
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    packed-switch v3, :pswitch_data_0

    :try_start_7
    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    :pswitch_0
    :try_start_8
    const-string v3, "\u00cf\u00e6\u00e5-\u001cU\\\u00fc\u00c0\u00cb\u0005[\u00e1K"

    const/16 v4, 0xb8

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mResponseResult:Ljava/lang/String;

    const-string v3, "gZhedchZBh\\"
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    :try_start_9
    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v4, v5

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411ББ0411Б0411()I

    move-result v5

    rem-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    if-eq v4, v5, :cond_4

    const/16 v4, 0xb

    :try_start_a
    sput v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v4, 0x23

    sput v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    :cond_4
    const/16 v4, 0xc1

    const/16 v5, 0xcc

    const/4 v6, 0x0

    :try_start_b
    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mResponseMsg:Ljava/lang/String;

    const-string v3, "\u0122\u0113\u012b\u00fe\u0121\u0113\u0116"

    const/16 v4, 0x59

    const/4 v5, 0x2

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_b
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1

    move-result v3

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ0411Б0411Б0411()I

    move-result v5

    add-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v4, v5

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411ББ0411Б0411()I

    move-result v5

    rem-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v4, v5, :cond_5

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v4

    sput v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v4, 0x13

    sput v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_5
    if-eqz v3, :cond_6

    :try_start_c
    const-string v3, "\u0011\u0002\u001a\uffed\u0010\u0002\u0005"

    const/4 v4, 0x7

    const/16 v5, 0x58

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;
    :try_end_c
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1

    :cond_6
    move v0, v1

    goto/16 :goto_0

    :catch_0
    move-exception v2

    :try_start_d
    const-string v3, "\u0343\u0356\u0364\u0361\u0360\u035f\u0364\u0356\u0339\u0352\u035f\u0355\u035d\u0356\u0363"

    const/16 v4, 0xfb

    const/4 v5, 0x6

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\uffc5\uffb6\uffc7\uffc8\uffba\uffa7\uffba\uffc8\uffc5\uffc4\uffc3\uffc8\uffba"
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1

    const/16 v5, 0xab

    const/4 v6, 0x4

    :try_start_e
    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\u00d1\u00fe\u00fe\u00fb\u00fe\u00ac\u00fc\u00ed\u00fe\u00ff\u00f5\u00fa\u00f3\u00ac\u00f0\u00ed\u0100\u00ed\u00ac"
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4

    const/16 v7, 0x46

    :pswitch_1
    packed-switch v1, :pswitch_data_1

    :goto_1
    packed-switch v0, :pswitch_data_2

    goto :goto_1

    :pswitch_2
    const/4 v1, 0x2

    :try_start_f
    invoke-static {v6, v7, v1}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v4, v1}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_10
    throw v0
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_2

    :catch_2
    move-exception v0

    throw v0

    :catch_3
    move-exception v1

    :try_start_11
    const-string v2, "aV@C\\]@V{R]W_VA"

    const/16 v3, 0x33

    const/4 v4, 0x3

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u01db\u01cc\u01dd\u01de\u01d0\u01bd\u01d0\u01de\u01db\u01da\u01d9\u01de\u01d0"

    const/16 v4, 0x79

    const/4 v5, 0x6

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\ufe68\ufe95\ufe95\ufe92\ufe95\ufe43\ufe93\ufe84\ufe95\ufe96\ufe8c\ufe91\ufe8a\ufe43\ufe87\ufe84\ufe97\ufe84\ufe43"

    const/16 v6, 0x9f

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v0

    :try_start_12
    throw v0
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_2

    :cond_7
    :try_start_13
    const-string v2, "\u00a8\u00b5\u00b5\u00b2\u00b5\u00a6\u00b2\u00a7\u00a8"

    const/16 v3, 0x43

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mResponseType:I

    iput-object p1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_4

    move v0, v1

    goto/16 :goto_0

    :catch_5
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public stunServerOfAdmin()Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    add-int/2addr v2, v1

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ0411Б0411Б0411()I

    move-result v4

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    if-eq v3, v4, :cond_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v3, 0x25

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I

    :cond_0
    mul-int/2addr v1, v2

    :try_start_1
    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    const/16 v1, 0x5a

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v1, 0x4b

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :pswitch_0
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    :try_start_3
    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->mPayLoad:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "\u009f\u009a\u0093\u0097\u0090\u00b0\u008a\u008d\u00ad\u008a\u008b\u0090"
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    const/16 v3, 0xfe

    const/4 v4, 0x3

    :try_start_4
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v0

    :try_start_5
    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v1

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b0411Б0411ББ0411:I

    add-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБББ0411Б0411()I

    move-result v2

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bБ04110411ББ0411:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    if-eq v1, v2, :cond_1

    const/16 v1, 0x56

    :try_start_6
    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->bББ0411ББ0411:I

    const/16 v1, 0x13

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->b041104110411ББ0411:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v1

    :try_start_7
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    :catch_3
    move-exception v0

    throw v0

    :catch_4
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
