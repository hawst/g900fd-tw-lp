.class public Lcom/sec/android/safe/manager/BluetoothManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/safe/manager/BluetoothManager$AcceptThread;,
        Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;,
        Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;,
        Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;,
        Lcom/sec/android/safe/manager/BluetoothManager$IBluetoothEnableListener;,
        Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;,
        Lcom/sec/android/safe/manager/BluetoothManager$Request;
    }
.end annotation


# static fields
# The value of this static final field might be set in the static constructor
.field public static final DEVICE_NAME:Ljava/lang/String; = "\uff9d\uff9e\uffaf\uffa2\uff9c\uff9e\uff98\uffa7\uff9a\uffa6\uff9e"

# The value of this static final field might be set in the static constructor
.field public static final FAIL_MSG:Ljava/lang/String; = "\ufe71\ufe6c\ufe74\ufe77\ufe6a\ufe78\ufe70\ufe7e\ufe7e\ufe6c\ufe72\ufe70"

.field public static final MESSAGE_DEVICE_NAME:I = 0x4

.field public static final MESSAGE_EXCUTE_MIRROR_APK:I = 0x8

.field public static final MESSAGE_READ:I = 0x2

.field public static final MESSAGE_STATE_CHANGE:I = 0x1

.field public static final MESSAGE_TOAST:I = 0x5

.field public static final MESSAGE_WRITE:I = 0x3

.field private static final MY_UUID_INSECURE:Ljava/util/UUID;

.field private static final MY_UUID_SECURE:Ljava/util/UUID;

# The value of this static final field might be set in the static constructor
.field private static final NAME_INSECURE:Ljava/lang/String; = "\ufe84\ufeae\ufeb7\ufea7\ufeb6\ufeb1\ufeb1\ufeb6\ufeaa\ufe8a\ufeb1\ufeaf\ufea7\ufe95\ufebb\ufeb0\ufea5\ufe8b\ufeb0\ufe95\ufea7\ufea5\ufeb7\ufeb4\ufea7"

# The value of this static final field might be set in the static constructor
.field private static final NAME_SECURE:Ljava/lang/String; = "\u023a\u0264\u026d\u025d\u026c\u0267\u0267\u026c\u0260\u0240\u0267\u0265\u025d\u024b\u0271\u0266\u025b\u024b\u025d\u025b\u026d\u026a\u025d"

.field private static REQ_ID:I = 0x0

.field public static final STATE_CONNECTED:I = 0x3

.field public static final STATE_CONNECTING:I = 0x2

.field public static final STATE_DISCONNECTED:I = 0x4

.field public static final STATE_FAIL_CONNECT:I = 0x5

.field public static final STATE_LISTEN:I = 0x1

.field public static final STATE_NONE:I = 0x0

# The value of this static final field might be set in the static constructor
.field private static final TAG:Ljava/lang/String; = "6`iYhcch\\AUbU[Yf"

# The value of this static final field might be set in the static constructor
.field public static final TOAST:Ljava/lang/String; = "G\u00a9\u009d\u009f\u00fd"

.field public static b044C044C044C044Cьь:I = 0x1

.field public static b044C044Cь044Cьь:I = 0x0

.field public static b044Cь044C044Cьь:I = 0x2

.field public static bь044C044C044Cьь:I = 0x2d

.field private static mInstance:Lcom/sec/android/safe/manager/BluetoothManager;

.field private static mReqMap:Ljava/util/HashMap;


# instance fields
.field private isBTListenerRegistered:Z

.field private final mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBTCallbackHandler:Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;

.field private mBluetoothHandler:Landroid/os/Handler;

.field private mBluetoothListener:Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;

.field private mConnectThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;

.field private mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

.field private mContext:Landroid/content/Context;

.field private mOriginalHandler:Landroid/os/Handler;

.field private mProfileReceiver:Landroid/content/BroadcastReceiver;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mState:I

.field private mTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x7

    const/4 v6, 0x4

    const/4 v5, 0x0

    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->TAG:Ljava/lang/String;

    invoke-static {v0, v6, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->TAG:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->NAME_SECURE:Ljava/lang/String;

    const/16 v1, 0xfc

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->NAME_SECURE:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->NAME_INSECURE:Ljava/lang/String;

    const/16 v1, 0xdf

    invoke-static {v0, v1, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->NAME_INSECURE:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->DEVICE_NAME:Ljava/lang/String;

    const/16 v1, 0xc7

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v3

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v2, v3, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    invoke-static {v0, v1, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->DEVICE_NAME:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->TOAST:Ljava/lang/String;

    const/16 v1, 0xa3

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->TOAST:Ljava/lang/String;

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v1

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x33

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->FAIL_MSG:Ljava/lang/String;

    const/16 v1, 0xa7

    invoke-static {v0, v1, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->FAIL_MSG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->mInstance:Lcom/sec/android/safe/manager/BluetoothManager;

    const-string v0, "\u017d\u0178\u014f\u014a\u017a\u0147\u017b\u0147\u0144\u0178\u017d\u0178\u017a\u0144\u0148\u0147\u017b\u017c\u0144\u014f\u0179\u0147\u0147\u0144\u0149\u0147\u0148\u014a\u0147\u014b\u0148\u014c\u0148\u014b\u014c\u014b"

    const/16 v1, 0x64

    const/16 v2, 0xb3

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->MY_UUID_SECURE:Ljava/util/UUID;

    const-string v0, "lg>9i6j63glgi376jk3>h67386796:7;7:;:"

    const/16 v1, 0xd

    invoke-static {v0, v1, v7, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->MY_UUID_INSECURE:Ljava/util/UUID;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->mReqMap:Ljava/util/HashMap;

    sput v5, Lcom/sec/android/safe/manager/BluetoothManager;->REQ_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    :try_start_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mOriginalHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->isBTListenerRegistered:Z

    new-instance v0, Lcom/sec/android/safe/manager/BluetoothManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/safe/manager/BluetoothManager$1;-><init>(Lcom/sec/android/safe/manager/BluetoothManager;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result v2

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    :try_start_2
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    const/16 v1, 0x56

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v1, 0xd

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    packed-switch v1, :pswitch_data_1

    const/4 v1, 0x1

    :try_start_3
    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :pswitch_0
    :try_start_4
    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    new-instance v0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :try_start_5
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBTCallbackHandler:Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mState:I

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothHandler:Landroid/os/Handler;

    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mOriginalHandler:Landroid/os/Handler;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    return-void

    :catch_0
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic access$0(Lcom/sec/android/safe/manager/BluetoothManager;)Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;
    .locals 4

    const/16 v3, 0x54

    const/4 v1, -0x1

    const/4 v0, 0x2

    const/4 v2, 0x0

    :goto_0
    :try_start_0
    div-int/2addr v0, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v0, 0x54

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    const/16 v1, 0xc

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    :try_start_1
    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBTCallbackHandler:Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-object v0

    :catch_1
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    throw v0

    :catch_3
    move-exception v0

    const/16 v0, 0x42

    :try_start_4
    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :cond_0
    :goto_1
    :try_start_5
    new-array v0, v1, [I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    :try_start_6
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    mul-int/2addr v0, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v2
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    :try_start_7
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x29

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_1

    :catch_4
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic access$1(Lcom/sec/android/safe/manager/BluetoothManager;)V
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/safe/manager/BluetoothManager;->unRegisterBluetoothEnableReceiver()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x45

    :try_start_2
    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x31

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    :pswitch_0
    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic access$10(Lcom/sec/android/safe/manager/BluetoothManager;)Landroid/os/Handler;
    .locals 5

    const/4 v2, 0x1

    :pswitch_0
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v1

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_1
    packed-switch v2, :pswitch_data_1

    :goto_0
    packed-switch v2, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothHandler:Landroid/os/Handler;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    if-eq v3, v4, :cond_0

    :try_start_3
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v3, 0x5c

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :cond_0
    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    :try_start_4
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    packed-switch v1, :pswitch_data_3

    const/16 v1, 0x12

    :try_start_5
    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v1, 0x3e

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    :pswitch_3
    return-object v0

    :catch_0
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    :catch_3
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic access$11(Lcom/sec/android/safe/manager/BluetoothManager;)V
    .locals 2

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x1e

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/safe/manager/BluetoothManager;->connectionLost()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    throw v0
.end method

.method static synthetic access$12(Lcom/sec/android/safe/manager/BluetoothManager;ILjava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/safe/manager/BluetoothManager;->setState(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    if-eq v0, v1, :cond_1

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0xa

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    const/4 v0, 0x1

    :try_start_2
    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    throw v0
.end method

.method static synthetic access$13(Lcom/sec/android/safe/manager/BluetoothManager;)V
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/safe/manager/BluetoothManager;->unRegisterBTDiscoveryListener()V

    :pswitch_0
    const/4 v0, 0x1

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v1

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v0, v1, :cond_0

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_2

    const/16 v0, 0x49

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_2
    const/16 v0, 0x43

    :try_start_1
    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cььь044Cь()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/16 v0, 0x58

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :cond_0
    return-void

    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_3
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic access$14(Lcom/sec/android/safe/manager/BluetoothManager;)V
    .locals 3

    :try_start_0
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    mul-int/2addr v1, v2

    :try_start_2
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-eq v1, v2, :cond_0

    :try_start_3
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_0

    const/16 v1, 0x57

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v1, 0x5d

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    :try_start_4
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    if-eq v0, v1, :cond_1

    const/16 v0, 0x26

    :try_start_5
    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    :cond_1
    :try_start_6
    invoke-direct {p0}, Lcom/sec/android/safe/manager/BluetoothManager;->unRegisterProfileReceiver()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    return-void

    :catch_0
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    :catch_3
    move-exception v0

    throw v0
.end method

.method static synthetic access$2(Lcom/sec/android/safe/manager/BluetoothManager;)Landroid/bluetooth/BluetoothAdapter;
    .locals 2

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v1

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_1

    const/16 v0, 0x4a

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x56

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    :pswitch_2
    const/4 v1, 0x0

    packed-switch v1, :pswitch_data_2

    :goto_0
    const/4 v1, 0x1

    packed-switch v1, :pswitch_data_3

    goto :goto_0

    :pswitch_3
    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic access$3()Ljava/util/UUID;
    .locals 3

    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->MY_UUID_SECURE:Ljava/util/UUID;

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_0

    const/16 v1, 0x25

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v1, 0x18

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    return-object v0
.end method

.method static synthetic access$4()Ljava/util/UUID;
    .locals 5

    const/4 v4, 0x1

    :try_start_0
    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->MY_UUID_INSECURE:Ljava/util/UUID;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eq v2, v3, :cond_0

    const/16 v2, 0x10

    :try_start_2
    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v2, v3, :cond_0

    const/16 v2, 0x5c

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v2, 0x1f

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    :pswitch_0
    packed-switch v4, :pswitch_data_0

    :goto_0
    packed-switch v4, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    :try_start_3
    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :cond_1
    return-object v0

    :catch_0
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$5(Lcom/sec/android/safe/manager/BluetoothManager;)I
    .locals 3

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v1

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_0

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v1

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0xf

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    const/16 v0, 0x51

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    :try_start_0
    iget v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mState:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v2

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_1

    const/16 v1, 0x11

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_1
    return v0

    :catch_0
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$6(Lcom/sec/android/safe/manager/BluetoothManager;Ljava/lang/String;)V
    .locals 4

    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/safe/manager/BluetoothManager;->connectionFailed(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v3

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v3

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v2, v3, :cond_0

    const/16 v2, 0x1b

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    const/4 v2, 0x1

    packed-switch v2, :pswitch_data_0

    :goto_0
    const/4 v2, 0x0

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x26

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x1a

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    :pswitch_2
    return-void

    :catch_0
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic access$7(Lcom/sec/android/safe/manager/BluetoothManager;Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;)V
    .locals 4

    const/4 v3, 0x1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x3f

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    :pswitch_1
    packed-switch v3, :pswitch_data_1

    :goto_0
    packed-switch v3, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x24

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    iput-object p1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;

    :pswitch_3
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    packed-switch v3, :pswitch_data_3

    :goto_1
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_4

    goto :goto_1

    :pswitch_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic access$8(Lcom/sec/android/safe/manager/BluetoothManager;Ljava/util/Timer;)V
    .locals 2

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mTimer:Ljava/util/Timer;

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v0, v1, :cond_0

    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_0

    :goto_0
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_1

    const/16 v0, 0x4d

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x5a

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_1
    const/4 v0, 0x1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic access$9(Lcom/sec/android/safe/manager/BluetoothManager;)Ljava/util/Timer;
    .locals 4

    :try_start_0
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    packed-switch v1, :pswitch_data_0

    :try_start_2
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :pswitch_0
    :try_start_3
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    add-int/2addr v1, v0

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v2, v3, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    mul-int/2addr v0, v1

    :try_start_4
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v1

    rem-int/2addr v0, v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    packed-switch v0, :pswitch_data_1

    :try_start_5
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :pswitch_1
    :try_start_6
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mTimer:Ljava/util/Timer;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    :catch_1
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    :catch_2
    move-exception v0

    throw v0

    :catch_3
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    :catch_4
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public static b044Cььь044Cь()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static bь044Cь044Cьь()I
    .locals 1

    const/16 v0, 0x39

    return v0
.end method

.method public static bьь044C044Cьь()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static bьььь044Cь()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method private connectionFailed(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :try_start_1
    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x17

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :pswitch_0
    const/4 v0, 0x5

    :pswitch_1
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v2

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v1, 0x45

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v2

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_1

    const/16 v1, 0x63

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v1, 0x1c

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_2
    packed-switch v3, :pswitch_data_2

    :goto_0
    packed-switch v3, :pswitch_data_3

    goto :goto_0

    :pswitch_3
    :try_start_2
    invoke-direct {p0, v0, p1}, Lcom/sec/android/safe/manager/BluetoothManager;->setState(ILjava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/safe/manager/BluetoothManager;->unRegisterProfileReceiver()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private connectionLost()V
    .locals 7

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v3

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v4, v5

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cььь044Cь()I

    move-result v5

    if-eq v4, v5, :cond_0

    const/4 v4, 0x3

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v4

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    mul-int/2addr v2, v3

    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v3

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v2, v3, :cond_1

    const/16 v2, 0x55

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/4 v2, 0x6

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v3

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v2, v3, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    :try_start_2
    const-string v2, "\u00d6\u00d1\u00c3\u00d5\u00d6"

    const/16 v3, 0x31

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\uff33\uff54\uff65\uff58\uff52\uff54\uff0f\uff52\uff5e\uff5d\uff5d\uff54\uff52\uff63\uff58\uff5e\uff5d\uff0f\uff66\uff50\uff62\uff0f\uff5b\uff5e\uff62\uff63"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    const/16 v4, 0x59

    const/16 v5, 0xb8

    const/4 v6, 0x1

    :try_start_3
    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v3

    :try_start_4
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :try_start_5
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/safe/manager/BluetoothManager;
    .locals 3

    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->mInstance:Lcom/sec/android/safe/manager/BluetoothManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/safe/manager/BluetoothManager;

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    const/16 v1, 0x4e

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v1, 0x5d

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/safe/manager/BluetoothManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->mInstance:Lcom/sec/android/safe/manager/BluetoothManager;

    :cond_0
    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->mInstance:Lcom/sec/android/safe/manager/BluetoothManager;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private getState(I)Ljava/lang/String;
    .locals 6

    const/16 v3, 0x37

    const/4 v1, 0x2

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "\u00ca\u00cb\u00b8\u00cb\u00bc\u00d6\u00c5\u00c6\u00c5\u00bc"

    const/16 v3, 0x77

    const/4 v4, 0x5

    invoke-static {v0, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    :goto_1
    :try_start_0
    div-int/2addr v1, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :pswitch_1
    const-string v0, "\uffb7\uffb8\uffa5\uffb8\uffa9\uffc3\uffa7\uffb3\uffb2\uffb2\uffa9\uffa7\uffb8\uffa9\uffa8"

    const/16 v1, 0x4e

    const/4 v2, 0x7

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const-string v0, "\u00b9\u00ba\u00a7\u00ba\u00ab\u00c5\u00aa\u00af\u00b9\u00a9\u00b5\u00b4\u00b4\u00ab\u00a9\u00ba\u00ab\u00aa"

    const/16 v2, 0x94

    const/16 v3, 0xfa

    invoke-static {v0, v2, v3, v1}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const-string v0, "{|i|m\u0087niqt\u0087kwvvmk|"

    const/16 v1, 0xec

    const/16 v2, 0xc4

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_1

    const/16 v3, 0x38

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v3, 0xd

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_4
    invoke-static {v0, v1, v2, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    const-string v0, "n\u0096\u00179\u0001.\u00e3]\u00ef\u00a3\u00c5.o\u00ba\u00caE"

    const/16 v1, 0xc9

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    goto :goto_0

    :catch_0
    move-exception v1

    const/16 v1, 0x3b

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    goto :goto_0

    :pswitch_6
    const-string v0, "\u00f8\u00f9\u00e6\u00f9\u00ea\u0104\u00f1\u00ee\u00f8\u00f9\u00ea\u00f3"

    const/4 v1, 0x6

    invoke-static {v0, v3, v1}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method private registerBluetoothEnableReceiver(Lcom/sec/android/safe/manager/BluetoothManager$IBluetoothEnableListener;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    :pswitch_0
    packed-switch v5, :pswitch_data_0

    :goto_0
    packed-switch v6, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v2, v3, :cond_0

    const/16 v2, 0x4e

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v1

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/4 v0, 0x6

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_1

    const/16 v0, 0x36

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x2d

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothListener:Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;

    if-nez v0, :cond_2

    new-instance v0, Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;-><init>(Lcom/sec/android/safe/manager/BluetoothManager;Lcom/sec/android/safe/manager/BluetoothManager$IBluetoothEnableListener;)V

    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothListener:Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;

    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "\u0200\u020d\u0203\u0211\u020e\u0208\u0203\u01cd\u0201\u020b\u0214\u0204\u0213\u020e\u020e\u0213\u0207\u01cd\u0200\u0203\u0200\u020f\u0213\u0204\u0211\u01cd\u0200\u0202\u0213\u0208\u020e\u020d\u01cd\u01f2\u01f3\u01e0\u01f3\u01e4\u01fe\u01e2\u01e7\u01e0\u01ed\u01e6\u01e4\u01e3"

    const/16 v2, 0xc4

    const/16 v3, 0xdb

    const/4 v4, 0x3

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothListener:Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;

    :pswitch_2
    packed-switch v5, :pswitch_data_2

    :goto_1
    :pswitch_3
    packed-switch v5, :pswitch_data_3

    :goto_2
    packed-switch v6, :pswitch_data_4

    goto :goto_2

    :pswitch_4
    packed-switch v5, :pswitch_data_5

    goto :goto_1

    :pswitch_5
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method private declared-synchronized registerProfileReceiver()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mProfileReceiver:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Lcom/sec/android/safe/manager/BluetoothManager$3;

    invoke-direct {v0, p0}, Lcom/sec/android/safe/manager/BluetoothManager$3;-><init>(Lcom/sec/android/safe/manager/BluetoothManager;)V

    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mProfileReceiver:Landroid/content/BroadcastReceiver;

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v1

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x51

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "\u012b\u0138\u012e\u013c\u0139\u0133\u012e\u00f8\u012c\u0136\u013f\u012f\u013e\u0139\u0139\u013e\u0132\u00f8\u012e\u012f\u0140\u0133\u012d\u012f\u00f8\u012b\u012d\u013e\u0133\u0139\u0138\u00f8\u011f\u011f\u0113\u010e"

    const/16 v2, 0x65

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mContext:Landroid/content/Context;

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mProfileReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v0, "%OXHWRRWK0DQDJHU"

    const/16 v1, 0x1d

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u0091\r\u0008\u008e1L\u00b45\u008d\u0003\u0019\u00e3\u00c4\\\u001e\u00dd\u00d6VX\u0089\u00feQ\u00a0"

    const/16 v2, 0xaa

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u009a\u009a\u009a\u009a\u009a\u009a\u009a\u009a\u009a\u009a\u0090\u00e2\u00f5\u00f7\u00f9\u00e3\u00e4\u00f5\u00e2\u0090\u00e0\u00e2\u00ff\u00f6\u00f9\u00fc\u00f5\u0090\u00e2\u00f5\u00f3\u00f5\u00f9\u00e6\u00f5\u00e2\u0090\u009a\u009a\u009a\u009a\u009a\u009a\u009a\u009a\u009a\u009a"

    const/16 v3, 0xb0

    const/4 v4, 0x3

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private declared-synchronized setState(ILjava/lang/String;)V
    .locals 7

    const/4 v6, 0x1

    monitor-enter p0

    :try_start_0
    const-string v0, "\ufe0b\ufe35\ufe3e\ufe2e\ufe3d\ufe38\ufe38\ufe3d\ufe31\ufe16\ufe2a\ufe37\ufe2a\ufe30\ufe2e\ufe3b"

    const/16 v1, 0xbd

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\ufdd3\ufdc5\ufdd4\ufdb3\ufdd4\ufdc1\ufdd4\ufdc5"

    const/16 v2, 0xe0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mState:I

    invoke-direct {p0, v3}, Lcom/sec/android/safe/manager/BluetoothManager;->getState(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\u009f\u00ac\u00bd\u009f"

    const/16 v4, 0x7f

    const/4 v5, 0x5

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    :pswitch_0
    packed-switch v6, :pswitch_data_0

    :goto_0
    packed-switch v6, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v4

    rem-int/2addr v3, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v3, :pswitch_data_2

    const/16 v3, 0x1f

    :try_start_1
    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :pswitch_2
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/android/safe/manager/BluetoothManager;->getState(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_3

    const/4 v0, 0x4

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x3e

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :pswitch_3
    :try_start_4
    iput p1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mState:I

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothHandler:Landroid/os/Handler;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v1, 0x1

    const/4 v2, -0x1

    :try_start_5
    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    packed-switch p1, :pswitch_data_4

    :goto_1
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return-void

    :pswitch_4
    :try_start_6
    new-instance v1, Landroid/os/Bundle;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "\u008d\u0088\u0090\u0093\u0086\u0094\u008c\u009a\u009a\u0088\u008e\u008c"

    const/4 v3, 0x5

    const/16 v4, 0x22

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    :pswitch_5
    packed-switch v6, :pswitch_data_5

    :goto_2
    packed-switch v6, :pswitch_data_6

    goto :goto_2

    :pswitch_6
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_7

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v2, 0x43

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :pswitch_7
    :try_start_9
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x5
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x0
        :pswitch_7
    .end packed-switch
.end method

.method private unRegisterBTDiscoveryListener()V
    .locals 7

    :try_start_0
    const-string v0, "\u00bd\u00e7\u00f0\u00e0\u00ef\u00ea\u00ea\u00ef\u00e3\u00c8\u00dc\u00e9\u00dc\u00e2\u00e0\u00ed"

    const/16 v1, 0xcf

    const/16 v2, 0x54

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\ufee3\ufedc\ufec0\ufed3\ufed5\ufed7\ufee1\ufee2\ufed3\ufee0\ufeb0\ufec2\ufeb2\ufed7\ufee1\ufed1\ufedd\ufee4\ufed3\ufee0\ufee7\ufeba\ufed7\ufee1\ufee2\ufed3\ufedc\ufed3\ufee0"

    const/16 v2, 0x86

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\ufef5\ufeff\ufece\ufee0\ufed8\ufef5\ufeff\uff00\ufef1\ufefa\ufef1\ufefe\ufede\ufef1\ufef3\ufef5\ufeff\uff00\ufef1\ufefe\ufef1\ufef0\ufeac\ufec6\ufeac"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v4, 0xba

    const/4 v5, 0x7

    :try_start_1
    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/safe/manager/BluetoothManager;->isBTListenerRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    if-eqz v0, :cond_0

    :try_start_2
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->isBTListenerRegistered:Z

    if-nez v0, :cond_3

    const-string v0, "\u00b8\u00e2\u00eb\u00db\u00ea\u00e5\u00e5\u00ea\u00de\u00c3\u00d7\u00e4\u00d7\u00dd\u00db\u00e8"

    const/16 v1, 0x3b

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "]V:MOQ[\\MZ*<,Q[KW^MZa4Q[\\MVMZ"

    const/16 v2, 0x18

    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\uff3c\uff2f\uff3e\uff3f\uff3c\uff38\ufeea\uff08\ufeea\uff33\uff3d\uff0c\uff1e\uff16\uff33\uff3d\uff3e\uff2f\uff38\uff2f\uff3c\uff1c\uff2f\uff31\uff33\uff3d\uff3e\uff2f\uff3c\uff2f\uff2e\ufeea\uff04\ufeea"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v5

    add-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v4, v5, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v4

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v4

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    const/16 v4, 0x45

    const/16 v5, 0xf1

    const/4 v6, 0x1

    :try_start_3
    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/safe/manager/BluetoothManager;->isBTListenerRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mReceiver:Landroid/content/BroadcastReceiver;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    if-eqz v0, :cond_2

    :try_start_4
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->isBTListenerRegistered:Z

    const-string v0, "CmvfuppuiNbobhfs"

    const/4 v1, 0x1

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\t\u0002\uffe6\ufff9\ufffb\ufffd\u0007\u0008\ufff9\u0006\uffd6\uffe8\uffd8\ufffd\u0007\ufff7\u0003\n\ufff9\u0006\r\uffe0\ufffd\u0007\u0008\ufff9\u0002\ufff9\u0006"

    const/16 v2, 0x42

    const/16 v3, 0xae

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u00bd\u00bf\u00ad\u00ad\u00af\u00bd\u00bd\u008a\u00be\u00b9\u008a\u00bf\u00b8\u00bc\u00af\u00b1\u00b3\u00bd\u00be\u00af\u00bc\u00af\u00ae\u008a\u0097\u0097\u0097\u00a8\u008a\u00d3\u00dd\u00ac\u00be\u00b6\u00d3\u00dd\u00de\u00cf\u00d8\u00cf\u00dc\u00bc\u00cf\u00d1\u00d3\u00dd\u00de\u00cf\u00dc\u00cf\u00ce\u008a\u00a4\u008a"
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v5, v4

    mul-int/2addr v4, v5

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v5

    rem-int/2addr v4, v5

    packed-switch v4, :pswitch_data_0

    const/16 v4, 0x4e

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    move-result v4

    :try_start_6
    sput v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    :pswitch_0
    const/16 v4, 0x48

    const/16 v5, 0x22

    const/4 v6, 0x3

    :try_start_7
    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/safe/manager/BluetoothManager;->isBTListenerRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const/4 v0, 0x0

    :try_start_8
    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mReceiver:Landroid/content/BroadcastReceiver;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    :catch_1
    move-exception v0

    :try_start_a
    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    :catch_2
    move-exception v0

    throw v0

    :catch_3
    move-exception v0

    :try_start_b
    const-string v1, "\u00f5\u011f\u0128\u0118\u0127\u0122\u0122\u0127\u011b\u0100\u0114\u0121\u0114\u011a\u0118\u0125"
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    const/16 v2, 0x45

    const/16 v3, 0xf8

    const/4 v4, 0x2

    :try_start_c
    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v6, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v5, v6
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    packed-switch v5, :pswitch_data_1

    const/16 v5, 0x2b

    :try_start_d
    sput v5, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v5

    sput v5, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1

    :pswitch_1
    :try_start_e
    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\uffc6\uffbf\uffa3\uffb6\uffb8\uffba\uffc4\uffc5\uffb6\uffc3\uff93\uffa5\uff95\uffba\uffc4\uffb4\uffc0\uffc7\uffb6\uffc3\uffca\uff9d\uffba\uffc4\uffc5\uffb6\uffbf\uffb6\uffc3"

    const/16 v3, 0xaf

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "i|gitxmsr$>$"

    const/4 v5, 0x2

    const/4 v6, 0x2

    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    const/4 v0, 0x0

    :try_start_f
    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mReceiver:Landroid/content/BroadcastReceiver;

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mReceiver:Landroid/content/BroadcastReceiver;

    throw v0
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_0

    :catch_4
    move-exception v0

    :try_start_10
    throw v0
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_1

    :catch_5
    move-exception v0

    :try_start_11
    throw v0
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_2

    :catch_6
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method private unRegisterBluetoothEnableReceiver()V
    .locals 7

    const/4 v5, 0x6

    const/4 v4, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothListener:Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v2

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v2, v3, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v2, 0x56

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_1

    sput v5, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    if-eqz v0, :cond_2

    :try_start_0
    const-string v0, "\uffe0\n\u0013\u0003\u0012\r\r\u0012\u0006\uffeb\uffff\u000c\uffff\u0005\u0003\u0010"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    const/16 v1, 0x3b

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    const/16 v1, 0x62

    const/4 v2, 0x4

    :try_start_1
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    :pswitch_1
    packed-switch v4, :pswitch_data_1

    :goto_0
    packed-switch v4, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    const-string v1, "\u0081z^qsu\u007f\u0080q~Nx\u0081q\u0080{{\u0080tQzmnxq^qoqu\u0082q~"

    const/4 v2, 0x4

    const/4 v3, 0x6

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u016d\u0166\u016a\u015d\u015f\u0161\u016b\u016c\u015d\u016a\u014a\u015d\u015b\u015d\u0161\u016e\u015d\u016a\u0119\u0119\u0119"

    const/16 v3, 0xd8

    const/16 v4, 0x20

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothListener:Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-object v6, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothListener:Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    iput-object v6, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothListener:Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;

    goto :goto_1

    :catchall_0
    move-exception v0

    iput-object v6, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothListener:Lcom/sec/android/safe/manager/BluetoothManager$BluetoothEnableReceiver;

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private unRegisterProfileReceiver()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mProfileReceiver:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_3

    :try_start_1
    const-string v0, "-Y\u00f9uMx8&\u00ce\u00d4UH\u00eb\u0018\u0006 "
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v3, v4, :cond_0

    const/16 v3, 0x27

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v3, 0x23

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    if-eq v1, v2, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    const/16 v1, 0xbd

    const/4 v2, 0x1

    :try_start_2
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uff28\uff21\uff05\uff18\uff1a\uff1c\uff26\uff27\uff18\uff25\uff03\uff25\uff22\uff19\uff1c\uff1f\uff18\uff05\uff18\uff16\uff18\uff1c\uff29\uff18\uff25"

    const/16 v2, 0x6f

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0123\u0123\u0123\u0123\u0123\u0123\u0123\u0123\u0123\u0123\u0119\u014e\u0147\u014b\u013e\u0140\u0142\u014c\u014d\u013e\u014b\u0119\u0149\u014b\u0148\u013f\u0142\u0145\u013e\u0119\u014b\u013e\u013c\u013e\u0142\u014f\u013e\u014b\u0119\u0123\u0123\u0123\u0123\u0123\u0123\u0123\u0123\u0123\u0123"

    const/16 v3, 0x53

    const/4 v4, 0x6

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mContext:Landroid/content/Context;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    :pswitch_0
    packed-switch v5, :pswitch_data_0

    :goto_0
    packed-switch v5, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    :pswitch_2
    packed-switch v5, :pswitch_data_2

    :goto_1
    packed-switch v6, :pswitch_data_3

    goto :goto_1

    :pswitch_3
    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_2
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mProfileReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v0, 0x0

    :try_start_4
    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mProfileReceiver:Landroid/content/BroadcastReceiver;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    :goto_2
    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    const/4 v0, 0x0

    :try_start_5
    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mProfileReceiver:Landroid/content/BroadcastReceiver;

    goto :goto_2

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mProfileReceiver:Landroid/content/BroadcastReceiver;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public cancelDiscovery()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v1

    rem-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cььь044Cь()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    :pswitch_0
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_1

    const/16 v1, 0x14

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    packed-switch v3, :pswitch_data_0

    :goto_0
    packed-switch v3, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_2

    const/16 v1, 0x62

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    :cond_3
    :pswitch_2
    packed-switch v4, :pswitch_data_2

    :goto_1
    packed-switch v4, :pswitch_data_3

    goto :goto_1

    :pswitch_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public changeBluetoothCallbackHandler(Landroid/content/Context;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    :try_start_0
    new-instance v0, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :pswitch_0
    packed-switch v5, :pswitch_data_0

    :goto_0
    packed-switch v5, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    packed-switch v6, :pswitch_data_2

    :goto_1
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v2

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v3, v4, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    packed-switch v6, :pswitch_data_3

    goto :goto_1

    :pswitch_2
    :try_start_1
    invoke-direct {v0, p1}, Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;-><init>(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBTCallbackHandler:Lcom/sec/android/safe/interfaces/BluetoothCallbackHandler;

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_2

    const/16 v0, 0x32

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_2
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mOriginalHandler:Landroid/os/Handler;

    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothHandler:Landroid/os/Handler;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public changeBluetoothHandler(Landroid/os/Handler;)V
    .locals 5

    const/4 v3, 0x1

    :try_start_0
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v1, 0x2f

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    :try_start_1
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v1

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x14

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x24

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :try_start_2
    const-string v0, "?b\u00c8\u00e0\u00f2Jq\u00ce>_\u0086\u009e\u00cfs\u0004\u00ca"

    const/16 v1, 0x2e

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    :pswitch_1
    packed-switch v3, :pswitch_data_1

    :goto_0
    const/4 v1, 0x0

    packed-switch v1, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    :try_start_3
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result v2

    :try_start_4
    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_3

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :pswitch_3
    :try_start_5
    const-string v1, "\u017b\u0150\u017a\u0183\u0173\u0182\u017d\u017d\u0182\u0176\u0156\u016f\u017c\u0172\u017a\u0173\u0180"
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const/16 v2, 0x52

    const/16 v3, 0xbc

    const/4 v4, 0x3

    :try_start_6
    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\uff66\uff8b\uff84\uff91\uff8a\uff88\uff43\uff6b\uff84\uff91\uff87\uff8f\uff88\uff95"
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    const/16 v3, 0xdd

    const/4 v4, 0x4

    :try_start_7
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    :try_start_8
    iput-object p1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothHandler:Landroid/os/Handler;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    return-void

    :catch_0
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_a
    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    :catch_3
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized connect(Landroid/bluetooth/BluetoothDevice;ZZ)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    monitor-enter p0

    :try_start_0
    const-string v0, "\u0154\u017e\u0187\u0177\u0186\u0181\u0181\u0186\u017a\u015f\u0173\u0180\u0173\u0179\u0177\u0184"

    const/16 v1, 0x89

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "^RSSX^I"

    const/16 v2, 0x3d

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u00de\u00ec\u00f83\u0016X[\u00b9\u00e6\u00c1L\u000e"

    const/16 v4, 0xb8

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    :try_start_2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u0100\u00f4\u013d\u0147\u0126\u0139\u0148\u0146\u014d\u00f4\u010e\u00f4"

    const/16 v4, 0xd4

    const/4 v5, 0x5

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    mul-int/2addr v1, v2

    :try_start_3
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_0

    const/16 v2, 0x63

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v2, 0x2c

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :pswitch_0
    :try_start_4
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eq v1, v2, :cond_1

    :try_start_5
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v2

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_0

    const/16 v1, 0x1f

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v1, 0x5f

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_0
    :try_start_6
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v1, 0x59

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_1
    :pswitch_1
    packed-switch v6, :pswitch_data_1

    :goto_0
    packed-switch v6, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    :try_start_7
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;-><init>(Lcom/sec/android/safe/manager/BluetoothManager;Landroid/bluetooth/BluetoothDevice;ZZ)V

    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->start()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const/4 v0, 0x2

    :pswitch_3
    packed-switch v7, :pswitch_data_3

    :goto_1
    packed-switch v6, :pswitch_data_4

    goto :goto_1

    :pswitch_4
    const/4 v1, 0x0

    :try_start_8
    invoke-direct {p0, v0, v1}, Lcom/sec/android/safe/manager/BluetoothManager;->setState(ILjava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;ZZZ)V
    .locals 2

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cььь044Cь()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/16 v0, 0x10

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/safe/manager/BluetoothManager;->cancelDiscovery()V

    if-eqz p4, :cond_1

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x5f

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/safe/manager/BluetoothManager;->stop()V

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/safe/manager/BluetoothManager;->connect(Landroid/bluetooth/BluetoothDevice;ZZ)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized connected(Landroid/bluetooth/BluetoothSocket;Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    monitor-enter p0

    :try_start_0
    const-string v0, "\ufe89\ufeb3\ufebc\ufeac\ufebb\ufeb6\ufeb6\ufebb\ufeaf\ufe94\ufea8\ufeb5\ufea8\ufeae\ufeac\ufeb9"

    const/16 v1, 0x93

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u02c5\u02f1\u02f0\u02f0\u02e7\u02e5\u02f6\u02e7\u02e6\u02d6\u02ea\u02f4\u02e7\u02e3\u02e6"

    const/16 v2, 0xd6

    const/4 v3, 0x6

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\uff99\uffa5\uffa4\uffa4\uff9b\uff99\uffaa\uff9b\uff9a\uff62\uff56\uff89\uffa5\uff99\uffa1\uff9b\uffaa\uff56\uff8a\uffaf\uffa6\uff9b\uff70"

    const/16 v4, 0xca

    const/4 v5, 0x4

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

    :goto_0
    new-instance v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

    invoke-direct {v0, p0, p1, p3}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;-><init>(Lcom/sec/android/safe/manager/BluetoothManager;Landroid/bluetooth/BluetoothSocket;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->start()V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    :pswitch_0
    packed-switch v7, :pswitch_data_0

    :goto_1
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v3

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cььь044Cь()I

    move-result v3

    if-eq v2, v3, :cond_0

    const/16 v2, 0x3f

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v2, 0x4e

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    packed-switch v7, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    const-string v2, "\u013a\u013b\u014c\u013f\u0139\u013b\u0135\u0144\u0137\u0143\u013b"

    const/16 v3, 0x6b

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mBluetoothHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x3

    const/4 v1, 0x0

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v3

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v2, 0x22

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_2
    invoke-direct {p0, v0, v1}, Lcom/sec/android/safe/manager/BluetoothManager;->setState(ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :pswitch_3
    const/16 v1, 0xa5

    const/16 v2, 0x3e

    const/4 v3, 0x2

    :try_start_1
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "LXWWNL]NM"

    const/16 v2, 0x17

    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0241\u0217\u0243\u0242\u0242\u0239\u0237\u0248\u0239\u0238\u0228\u023c\u0246\u0239\u0235\u0238\u01f4\u023d\u0247\u01f4\u0242\u0249\u0240\u0240\u01f5\u01f5\u01f5"

    const/16 v3, 0xea

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v4, v5

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cььь044Cь()I

    move-result v5

    if-eq v4, v5, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v4

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v4, 0x1b

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    const-string v0, "\uffdb\u0005\u000e\ufffe\r\u0008\u0008\r\u0001\uffe6\ufffa\u0007\ufffa\u0000\ufffe\u000b"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :pswitch_4
    packed-switch v6, :pswitch_data_3

    :goto_2
    packed-switch v6, :pswitch_data_4

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public discovery(Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;)V
    .locals 6

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/safe/manager/BluetoothManager;->registerBTDiscoveryListener(Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    :cond_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    const-string v0, "\u017a\u01a4\u01ad\u019d\u01ac\u01a7\u01a7\u01ac\u01a0\u0185\u0199\u01a6\u0199\u019f\u019d\u01aa"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x68

    const/4 v2, 0x6

    :try_start_1
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uffc5\uffca\uffd4\uffc4\uffd0\uffd7\uffc6\uffd3\uffda"

    const/16 v2, 0x9f

    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    :try_start_2
    const-string v2, "\uffad\uffae\uff9b\uffac\uffae\uff7e\uffa3\uffad\uff9d\uffa9\uffb0\uff9f\uffac\uffb3\uff5a\uff5b\uff5b\uff5b"

    const/16 v3, 0x42

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v2

    :pswitch_0
    const/4 v3, 0x1

    packed-switch v3, :pswitch_data_0

    :goto_0
    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v3, v4, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v4

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v4

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_1

    const/16 v3, 0x3c

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_1
    packed-switch v5, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    :try_start_3
    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x10

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public enable()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v1, 0x5e

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    :try_start_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_1

    const/16 v1, 0x2e

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/16 v1, 0x3c

    :try_start_2
    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :pswitch_1
    :try_start_3
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x35

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x4e

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public enable(Lcom/sec/android/safe/manager/BluetoothManager$IBluetoothEnableListener;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x1a

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    packed-switch v2, :pswitch_data_1

    :goto_0
    packed-switch v3, :pswitch_data_2

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x31

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    :pswitch_2
    packed-switch v3, :pswitch_data_3

    :goto_1
    packed-switch v2, :pswitch_data_4

    goto :goto_1

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/android/safe/manager/BluetoothManager;->registerBluetoothEnableReceiver(Lcom/sec/android/safe/manager/BluetoothManager$IBluetoothEnableListener;)V

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v1

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_5

    const/16 v0, 0x4c

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x5d

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public getReqType(I)Ljava/lang/String;
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->mReqMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    packed-switch v2, :pswitch_data_0

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v2, 0x4c

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_0
    const/16 v2, 0xf

    :try_start_2
    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v2, 0x40

    :try_start_3
    sput v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :pswitch_1
    :try_start_4
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v2

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_2
    return-object v0

    :catch_0
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    :pswitch_3
    packed-switch v4, :pswitch_data_3

    :goto_0
    packed-switch v4, :pswitch_data_4

    goto :goto_0

    :pswitch_4
    throw v0

    :catch_2
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized getState()I
    .locals 2

    monitor-enter p0

    :try_start_0
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_1

    :try_start_1
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v0, v1, :cond_0

    const/16 v0, 0x37

    :try_start_2
    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v1

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cььь044Cь()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0xa

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_0
    const/16 v0, 0x48

    :try_start_4
    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_1
    :try_start_5
    iget v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mState:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return v0

    :catch_0
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_2
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_3
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_4
    move-exception v0

    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0
.end method

.method public isEnabled()Z
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    :pswitch_0
    packed-switch v2, :pswitch_data_0

    :goto_0
    packed-switch v3, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    packed-switch v3, :pswitch_data_2

    :goto_1
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_3

    const/16 v0, 0xb

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x1d

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_2
    packed-switch v2, :pswitch_data_4

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_5

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_4
    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public makeRequest(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x1

    :try_start_0
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->REQ_ID:I

    add-int/lit8 v0, v1, 0x1

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->REQ_ID:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    const-string v0, "\u0090\u0083\u008f\u0093\u0083\u0091\u0092g\u0082"
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v3, 0x1e

    const/4 v4, 0x5

    :try_start_3
    invoke-static {v0, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :try_start_4
    const-string v0, "\ufdc7\ufdbf\ufdce\ufdc2\ufdc9\ufdbe"

    const/16 v3, 0xe2

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v0

    :try_start_5
    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    if-eq v3, v4, :cond_0

    const/4 v3, 0x2

    :try_start_6
    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v3, 0x36

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :cond_0
    :try_start_7
    invoke-virtual {v2, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    :try_start_8
    const-string v0, "\ufea9\ufeb8\ufeb1\ufe96\ufea9\ufeb5\ufead"

    const/16 v3, 0xdc

    const/4 v4, 0x7

    invoke-static {v0, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    if-eqz p3, :cond_2

    const-string v0, "xi\u0081Twil"
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    const/4 v3, 0x4

    const/4 v4, 0x2

    :try_start_9
    invoke-static {v0, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    move-result-object v0

    :try_start_a
    invoke-virtual {v2, v0, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    :try_start_b
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2

    if-eq v0, v3, :cond_2

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v3

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v3

    rem-int/2addr v0, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v3, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    const/16 v0, 0x61

    :try_start_c
    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x5f

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_2
    :goto_0
    sget-object v0, Lcom/sec/android/safe/manager/BluetoothManager;->mReqMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1

    move-result-object v1

    :pswitch_0
    packed-switch v5, :pswitch_data_0

    :goto_1
    packed-switch v5, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    :try_start_d
    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_e
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public registerBTDiscoveryListener(Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;)V
    .locals 10

    const/16 v9, 0x16

    const/4 v8, 0x7

    const/4 v7, 0x3

    const/4 v5, 0x1

    const/4 v6, 0x2

    const-string v0, "\u00ab\u0085\u009c\u008c\u009d\u0086\u0086\u009d\u0081\u00a4\u0088\u0087\u0088\u008e\u008c\u009b"

    const/16 v1, 0xe9

    invoke-static {v0, v1, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\ufe90\ufe83\ufe85\ufe87\ufe91\ufe92\ufe83\ufe90\ufe60\ufe72\ufe62\ufe87\ufe91\ufe81\ufe8d\ufe94\ufe83\ufe90\ufe97\ufe6a\ufe87\ufe91\ufe92\ufe83\ufe8c\ufe83\ufe90"

    const/16 v2, 0xf1

    invoke-static {v1, v2, v8}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\uff5b\uff65\uff34\uff46\uff3e\uff5b\uff65\uff66\uff57\uff60\uff57\uff64\uff44\uff57\uff59\uff5b\uff65\uff66\uff57\uff64\uff57\uff56\uff12\uff2c\uff12"

    const/16 v4, 0x87

    invoke-static {v3, v4, v8}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/safe/manager/BluetoothManager;->isBTListenerRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/sec/android/safe/manager/BluetoothManager$2;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/safe/manager/BluetoothManager$2;-><init>(Lcom/sec/android/safe/manager/BluetoothManager;Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;)V

    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "\u00cd\u00da\u00d0\u00de\u00db\u00d5\u00d0\u009a\u00ce\u00d8\u00e1\u00d1\u00e0\u00db\u00db\u00e0\u00d4\u009a\u00d0\u00d1\u00e2\u00d5\u00cf\u00d1\u009a\u00cd\u00cf\u00e0\u00d5\u00db\u00da\u009a\u00b2\u00bb\u00c1\u00ba\u00b0"

    const/16 v2, 0x20

    const/16 v3, 0x8c

    invoke-static {v1, v2, v3, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "\u00ff\u00f0\u00fa\u00ec\u00f1\u00f7\u00fa\u00b0\u00fc\u00f2\u00eb\u00fb\u00ea\u00f1\u00f1\u00ea\u00f6\u00b0\u00ff\u00fa\u00ff\u00ee\u00ea\u00fb\u00ec\u00b0\u00ff\u00fd\u00ea\u00f7\u00f1\u00f0\u00b0\u00da\u00d7\u00cd\u00dd\u00d1\u00c8\u00db\u00cc\u00c7\u00c1\u00d8\u00d7\u00d0\u00d7\u00cd\u00d6\u00db\u00da"

    const/16 v2, 0x9e

    invoke-static {v1, v2, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v5, p0, Lcom/sec/android/safe/manager/BluetoothManager;->isBTListenerRegistered:Z

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v1

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_1

    sput v9, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_1
    const-string v0, "\uffb8\uffe2\uffeb\uffdb\uffea\uffe5\uffe5\uffea\uffde\uffc3\uffd7\uffe4\uffd7\uffdd\uffdb\uffe8"

    const/16 v1, 0xcc

    const/16 v2, 0x42

    :pswitch_0
    packed-switch v5, :pswitch_data_0

    :goto_1
    const/4 v3, 0x0

    packed-switch v3, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    invoke-static {v0, v1, v2, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "I<>@JK<I\u0019+\u001b@J:FM<IP#@JK<E<I"

    const/16 v2, 0x29

    const/4 v3, 0x4

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v4

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v4, v5

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v5

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v4, v5, :cond_2

    sput v9, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v4

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_2
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u0217\u0217\u0217\u0228\u020a\u0253\u025d\u022c\u023e\u0236\u0253\u025d\u025e\u024f\u0258\u024f\u025c\u023c\u024f\u0251\u0253\u025d\u025e\u024f\u025c\u024f\u024e\u020a\u0224\u020a"

    const/16 v4, 0xf5

    invoke-static {v3, v4, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/android/safe/manager/BluetoothManager;->isBTListenerRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v4

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_2

    const/4 v3, 0x6

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :pswitch_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method public sendMessage(Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x1

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьььь044Cь()I

    move-result v1

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/safe/manager/BluetoothManager;->getState()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    :try_start_1
    const-string v0, "\uffdb\u0005\u000e\ufffe\r\u0008\u0008\r\u0001\uffe6\ufffa\u0007\ufffa\u0000\ufffe\u000b"

    const/16 v1, 0x67

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u0016\u0008\u0011\u0007\ufff0\u0008\u0016\u0016\u0004\n\u0008"

    const/16 v2, 0x5d

    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\ufe8f\ufe8d\ufe9c\ufe48\ufe6a\ufe7c\ufe48\ufe8b\ufe97\ufe96\ufe96\ufe8d\ufe8b\ufe9c\ufe91\ufe97\ufe96\ufe48\ufe9b\ufe9c\ufe89\ufe9c\ufe8d\ufe48"

    const/16 v3, 0xec

    const/4 v4, 0x7

    :pswitch_0
    packed-switch v6, :pswitch_data_0

    :goto_0
    const/4 v5, 0x0

    packed-switch v5, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result v0

    :try_start_2
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bьь044C044Cьь()I

    move-result v2

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :try_start_4
    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :pswitch_2
    if-lez v0, :cond_1

    :try_start_5
    const-string v0, "-W`P_ZZ_S8LYLRP]"

    const/16 v1, 0x15

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    move-result-object v0

    :try_start_6
    const-string v1, "\u01ff\u01f1\u01fa\u01f0\u01d9\u01f1\u01ff\u01ff\u01ed\u01f3\u01f1"

    const/16 v2, 0xc6

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\uffc1\uffb9\uffc7\uffc7\uffb5\uffbb\uffb9\uff74\uffc8\uffbc\uffb5\uffc8\uff74\uffcb\uffbd\uffc0\uffc0\uff74\uffb6\uffb9\uff74\uffc7\uffb9\uffc2\uffc8\uff8e\uff74"
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    const/16 v4, 0x13

    const/16 v5, 0x99

    const/4 v6, 0x1

    :try_start_7
    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    :try_start_8
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    :try_start_9
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    packed-switch v0, :pswitch_data_3

    const/16 v0, 0x50

    :try_start_a
    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x30

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    :pswitch_3
    :try_start_b
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/manager/BluetoothManager;->write([B)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_c
    throw v0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_d
    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1

    :catch_3
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized stop()V
    .locals 7

    const/4 v6, 0x0

    monitor-enter p0

    :try_start_0
    const-string v0, "\u02af\u02d9\u02e2\u02d2\u02e1\u02dc\u02dc\u02e1\u02d5\u02ba\u02ce\u02db\u02ce\u02d4\u02d2\u02df"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v1, 0xcf

    const/4 v2, 0x6

    :try_start_1
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MNIJ"

    const/16 v2, 0x26

    :pswitch_0
    packed-switch v6, :pswitch_data_0

    :goto_0
    const/4 v3, 0x1

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\uff92\uff90\uff9b\uff9b\uff94\uff93\uff4f\uff50\uff50\uff50"

    const/16 v3, 0xd8

    const/4 v4, 0x7

    const/4 v5, 0x2

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->cancel()V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->interrupt()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->cancel()V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->interrupt()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v0, 0x0

    :try_start_4
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v1, v2, :cond_1

    const/16 v1, 0x57

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_1
    :try_start_5
    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_6
    invoke-direct {p0, v0, v1}, Lcom/sec/android/safe/manager/BluetoothManager;->setState(ILjava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    :pswitch_2
    packed-switch v6, :pswitch_data_2

    :goto_1
    packed-switch v6, :pswitch_data_3

    goto :goto_1

    :pswitch_3
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_4

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044C044C044Cьь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044Cь044C044Cьь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    if-eq v0, v1, :cond_3

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0x19

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I

    :cond_3
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->bь044Cь044Cьь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->bь044C044C044Cьь:I

    const/16 v0, 0xd

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager;->b044C044Cь044Cьь:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_4
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public write([B)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    :pswitch_0
    packed-switch v2, :pswitch_data_0

    :goto_0
    packed-switch v2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

    if-nez v0, :cond_1

    :cond_0
    monitor-exit p0

    :goto_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :pswitch_2
    packed-switch v3, :pswitch_data_2

    :goto_2
    packed-switch v3, :pswitch_data_3

    goto :goto_2

    :pswitch_3
    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager;->mConnectedThread:Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v0, p1}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->write([B)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
