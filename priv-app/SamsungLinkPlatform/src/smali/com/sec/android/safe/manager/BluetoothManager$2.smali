.class Lcom/sec/android/safe/manager/BluetoothManager$2;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/safe/manager/BluetoothManager;->registerBTDiscoveryListener(Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final SPC_FILTER:Ljava/lang/String; = "\uff6b\uff68\uff5b\uff77"

.field private static final SPC_FILTER2:Ljava/lang/String; = "\uff62\uff89\uff87\uff7f\uff6d\uff93\uff88\uff7d\uff79"

.field public static b043Aк043A043A043Aк:I = 0x1

.field public static bк043A043A043A043Aк:I = 0x2

.field public static bкк043A043A043Aк:I = 0x6

.field public static bккккк043A:I


# instance fields
.field final synthetic this$0:Lcom/sec/android/safe/manager/BluetoothManager;

.field private final synthetic val$listener:Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;


# direct methods
.method constructor <init>(Lcom/sec/android/safe/manager/BluetoothManager;Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;)V
    .locals 2

    iput-object p1, p0, Lcom/sec/android/safe/manager/BluetoothManager$2;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    iput-object p2, p0, Lcom/sec/android/safe/manager/BluetoothManager$2;->val$listener:Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043Aк043A043A043Aк:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$2;->bк043A043A043A043Aк:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x42

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043A043A043A043A043Aк()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043Aк043A043A043Aк:I

    :pswitch_0
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static b043A043A043A043A043Aк()I
    .locals 1

    const/16 v0, 0x39

    return v0
.end method

.method public static b043Aкккк043A()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method private filterSPCDevice(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043Aк043A043A043Aк:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$2;->bк043A043A043A043Aк:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$2;->bккккк043A:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043A043A043A043A043Aк()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043A043A043A043A043Aк()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$2;->bккккк043A:I

    :cond_0
    :pswitch_0
    const/4 v1, 0x1

    packed-switch v1, :pswitch_data_0

    :goto_0
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    :try_start_0
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043Aк043A043A043Aк:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    :try_start_1
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$2;->bк043A043A043A043Aк:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043A043A043A043A043Aк()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043A043A043A043A043Aк()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$2;->bккккк043A:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :pswitch_2
    return v0

    :cond_1
    :try_start_2
    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    goto :goto_1

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10

    const/4 v9, 0x7

    const/4 v6, 0x0

    const/4 v8, 0x3

    const/4 v5, 0x2

    const/4 v7, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uff83\uff90\uff86\uff94\uff91\uff8b\uff86\uff50\uff84\uff8e\uff97\uff87\uff96\uff91\uff91\uff96\uff8a\uff50\uff86\uff87\uff98\uff8b\uff85\uff87\uff50\uff83\uff85\uff96\uff8b\uff91\uff90\uff50\uff68\uff71\uff77\uff70\uff66"

    const/16 v2, 0xde

    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v0, "&3)74.)\ufff3\'1:*9449-\ufff3)*;.(*\ufff3*=97&\ufff3\t\n\u001b\u000e\u0008\n"

    const/16 v1, 0xec

    const/16 v2, 0xb1

    invoke-static {v0, v1, v2, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    const-string v1, "!\u000f\u0016\u0006\u0017\u000c\u000c\u0017\u000b.\u0002\r\u0002\u0004\u0006\u0011"

    const/16 v2, 0x63

    invoke-static {v1, v2, v8}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u011a\u010d\u010f\u0111\u011b\u011c\u010d\u011a\u00ea\u00fc\u00ec\u0111\u011b\u010b\u0117\u011e\u010d\u011a\u0121\u00f4\u0111\u011b\u011c\u010d\u0116\u010d\u011a"

    const/16 v3, 0x3e

    const/16 v4, 0xe6

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\ufef1\ufef0\ufed4\ufee7\ufee5\ufee7\ufeeb\ufef8\ufee7\ufea2\ufec0\ufea2\ufed0\ufee3\ufeef\ufee7\ufea2\ufebc\ufea2"

    const/16 v5, 0xbf

    :pswitch_0
    packed-switch v7, :pswitch_data_0

    :goto_0
    packed-switch v7, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    packed-switch v7, :pswitch_data_2

    :goto_1
    packed-switch v6, :pswitch_data_3

    goto :goto_1

    :pswitch_2
    invoke-static {v4, v5, v9}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "am,))?(>>mwm"

    const/16 v5, 0x4d

    invoke-static {v4, v5, v8}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043Aк043A043A043Aк:I

    add-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$2;->bк043A043A043A043Aк:I

    rem-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$2;->bккккк043A:I

    if-eq v4, v5, :cond_0

    sput v9, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    const/16 v4, 0x45

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager$2;->bккккк043A:I

    :cond_0
    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0103\u0100\u00f3\u010f"

    const/16 v3, 0xb0

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/safe/manager/BluetoothManager$2;->filterSPCDevice(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\{yqGmzwK"

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043Aк043A043A043Aк:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043Aкккк043A()I

    move-result v4

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_4

    const/16 v3, 0x49

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    sput v7, Lcom/sec/android/safe/manager/BluetoothManager$2;->bккккк043A:I

    :pswitch_3
    const/16 v3, 0x14

    invoke-static {v2, v3, v8}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/safe/manager/BluetoothManager$2;->filterSPCDevice(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043Aк043A043A043Aк:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$2;->bк043A043A043A043Aк:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$2;->bккккк043A:I

    if-eq v2, v3, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043A043A043A043A043Aк()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager$2;->bкк043A043A043Aк:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$2;->b043A043A043A043A043Aк()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager$2;->bккккк043A:I

    :cond_1
    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager$2;->val$listener:Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;

    invoke-interface {v1, v0}, Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;->onDiscovery(Landroid/bluetooth/BluetoothDevice;)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    const-string v1, ";H>LIC>\u0008<FO?NIINB\u0008;>;JN?L\u0008;=NCIH\u0008\u001e#-\u001d)0\u001f,39 #(#-\"\u001f\u001e"

    const/16 v2, 0x22

    const/16 v3, 0x48

    invoke-static {v1, v2, v3, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "\u00f4\u0016?h\u00fe\u0093&\u009f\u00f3\u009a\u00a5\u00b2:.\u00e1W"

    const/16 v1, 0x35

    invoke-static {v0, v1, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u020a\u01fd\u01ff\u0201\u020b\u020c\u01fd\u020a\u01da\u01ec\u01dc\u0201\u020b\u01fb\u0207\u020e\u01fd\u020a\u0211\u01e4\u0201\u020b\u020c\u01fd\u0206\u01fd\u020a"

    const/16 v2, 0xcc

    invoke-static {v1, v2, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0105\u0104\u00e8\u00fb\u00f9\u00fb\u00ff\u010c\u00fb\u00b6\u00d4\u00b6\u00fc\u00ff\u0104\u00ff\u0109\u00fe"

    const/16 v3, 0x4b

    invoke-static {v2, v3, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$2;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    invoke-static {v0}, Lcom/sec/android/safe/manager/BluetoothManager;->access$13(Lcom/sec/android/safe/manager/BluetoothManager;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$2;->val$listener:Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;

    invoke-interface {v0}, Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;->onFinish()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method
