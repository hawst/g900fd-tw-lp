.class public interface abstract Lcom/sec/android/safe/interfaces/IBluetoothHandlerInterface;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onMessageDeviceName(Ljava/lang/String;)V
.end method

.method public abstract onMessageRead(Ljava/lang/String;)V
.end method

.method public abstract onMessageToast(Ljava/lang/String;)V
.end method

.method public abstract onMessageWrite(Ljava/lang/String;)V
.end method

.method public abstract onRequestTimeout()V
.end method

.method public abstract onStateConnected()V
.end method

.method public abstract onStateConnecting()V
.end method

.method public abstract onStateDisconnected()V
.end method

.method public abstract onStateListen()V
.end method

.method public abstract onStateNone()V
.end method
