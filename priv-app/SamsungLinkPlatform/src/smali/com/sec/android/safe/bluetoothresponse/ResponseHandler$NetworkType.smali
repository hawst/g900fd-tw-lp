.class public final enum Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NetworkType"
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

.field public static final enum ETHERNET:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

.field public static final enum NONE:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

.field public static final enum WIFI:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

.field public static b044C044Cь044C044Cь:I = 0x0

.field public static b044Cьь044C044Cь:I = 0x1

.field public static bь044Cь044C044Cь:I = 0x2

.field public static bььь044C044Cь:I = 0x19


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    new-instance v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    const-string v1, "\uffc0\uffcf\uffc3\uffc0\uffcd\uffc9\uffc0\uffcf"

    const/16 v2, 0x5e

    const/16 v3, 0x27

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->ETHERNET:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    new-instance v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    const-string v1, "\u00a9\u009b\u0098\u009b"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, 0x85

    const/16 v3, 0xd7

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bьь044C044C044Cь()I

    move-result v5

    add-int/2addr v5, v4

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bь044Cь044C044Cь:I

    rem-int/2addr v4, v5

    packed-switch v4, :pswitch_data_0

    const/16 v4, 0x45

    sput v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    const/16 v4, 0x5f

    sput v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    :pswitch_0
    const/4 v4, 0x2

    :try_start_1
    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->WIFI:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    new-instance v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    const-string v1, "\u0128\u0129\u0128\u011f"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/16 v2, 0x6d

    sget v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bьь044C044C044Cь()I

    move-result v4

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bь044Cь044C044Cь:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    if-eq v3, v4, :cond_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v3

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    const/4 v3, 0x6

    sput v3, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    :cond_0
    const/4 v3, 0x2

    :try_start_2
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;-><init>(Ljava/lang/String;I)V

    :pswitch_1
    packed-switch v6, :pswitch_data_1

    :goto_0
    packed-switch v6, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    sput-object v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->NONE:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const/4 v0, 0x3

    :try_start_3
    new-array v0, v0, [Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->ETHERNET:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    aput-object v2, v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cьь044C044Cь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bь044Cь044C044Cь:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_3

    const/16 v1, 0x55

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    const/16 v1, 0x14

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cьь044C044Cь:I

    :pswitch_3
    const/4 v1, 0x1

    sget-object v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->WIFI:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->NONE:Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->ENUM$VALUES:[Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 3

    :try_start_0
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cьь044C044Cь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bь044Cь044C044Cь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x4e

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cьь044C044Cь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bь044Cь044C044Cь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v0

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cьь044C044Cь:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bь044Cь044C044Cь:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    const/16 v0, 0x2a

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    :pswitch_0
    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    :cond_0
    :pswitch_1
    return-void

    :catch_0
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static b044Cь044C044C044Cь()I
    .locals 1

    const/16 v0, 0x62

    return v0
.end method

.method public static bь044C044C044C044Cь()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public static bьь044C044C044Cь()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;
    .locals 3

    :try_start_0
    const-class v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :try_start_1
    check-cast v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v1

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cьь044C044Cь:I

    add-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v2

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bь044Cь044C044Cь:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    const/16 v1, 0x26

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cьь044C044Cь:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bь044Cь044C044Cь:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v1

    sput v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    :cond_0
    :pswitch_0
    return-object v0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static values()[Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;
    .locals 5

    :try_start_0
    sget-object v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->ENUM$VALUES:[Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    const/4 v1, 0x0

    array-length v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-array v3, v2, [Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;

    const/4 v4, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cьь044C044Cь:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bь044Cь044C044Cь:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    sget v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cьь044C044Cь:I

    add-int/2addr v2, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    mul-int/2addr v2, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bь044Cь044C044Cь:I

    rem-int/2addr v2, v4

    sget v4, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    if-eq v2, v4, :cond_0

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v2

    sput v2, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I

    :cond_0
    if-eq v0, v1, :cond_1

    :try_start_2
    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->bььь044C044Cь:I

    invoke-static {}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044Cь044C044C044Cь()I

    move-result v0

    sput v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler$NetworkType;->b044C044Cь044C044Cь:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    return-object v3

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0
.end method
