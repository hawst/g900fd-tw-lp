.class public interface abstract Lcom/sec/android/safe/manager/BluetoothManager$IDeviceDiscoveryListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/safe/manager/BluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IDeviceDiscoveryListener"
.end annotation


# virtual methods
.method public abstract onDiscovery(Landroid/bluetooth/BluetoothDevice;)V
.end method

.method public abstract onFinish()V
.end method
