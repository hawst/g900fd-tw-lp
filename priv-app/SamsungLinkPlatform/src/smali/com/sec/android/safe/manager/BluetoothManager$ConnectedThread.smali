.class Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/safe/manager/BluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectedThread"
.end annotation


# static fields
.field public static b041104110411Б0411Б:I = 0x1

.field public static bБ04110411Б0411Б:I = 0x2b

.field public static bБ0411Б04110411Б:I = 0x0

.field public static bБББ04110411Б:I = 0x2


# instance fields
.field private final mmInStream:Ljava/io/InputStream;

.field private final mmOutStream:Ljava/io/OutputStream;

.field private final mmSocket:Landroid/bluetooth/BluetoothSocket;

.field final synthetic this$0:Lcom/sec/android/safe/manager/BluetoothManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/safe/manager/BluetoothManager;Landroid/bluetooth/BluetoothSocket;Ljava/lang/String;)V
    .locals 9

    const/4 v2, 0x0

    :try_start_0
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b041104110411Б0411Б:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБББ04110411Б:I

    rem-int/2addr v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    packed-switch v0, :pswitch_data_0

    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b0411ББ04110411Б()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    const/16 v0, 0xf

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b041104110411Б0411Б:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    :pswitch_0
    :try_start_2
    iput-object p1, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const-string v0, "%OXHWRRWK0DQDJHU"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    const/16 v1, 0x1d

    const/4 v3, 0x4

    :try_start_4
    invoke-static {v0, v1, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uff7a\uffa6\uffa5\uffa5\uff9c\uff9a\uffab\uff9c\uff9b\uff8b\uff9f\uffa9\uff9c\uff98\uff9b"

    const/16 v3, 0xc9

    const/4 v4, 0x4

    invoke-static {v1, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\u023a\u0249\u023c\u0238\u024b\u023c\u01f7\u021a\u0246\u0245\u0245\u023c\u023a\u024b\u023c\u023b\u022b\u023f\u0249\u023c\u0238\u023b\u0211\u01f7"

    const/16 v5, 0x9d

    const/4 v6, 0x6

    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :try_start_5
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    move-result-object v1

    :try_start_6
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    move-result-object v2

    :try_start_7
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b041104110411Б0411Б:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    add-int/2addr v3, v0

    mul-int/2addr v0, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b041104110411Б0411Б:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБББ04110411Б:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I

    if-eq v3, v4, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b0411ББ04110411Б()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b0411ББ04110411Б()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I

    :cond_0
    :try_start_8
    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБББ04110411Б:I

    rem-int/2addr v0, v3

    packed-switch v0, :pswitch_data_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b0411ББ04110411Б()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    const/16 v0, 0xd

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b041104110411Б0411Б:I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :goto_0
    :pswitch_1
    :try_start_9
    iput-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->mmInStream:Ljava/io/InputStream;

    iput-object v2, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->mmOutStream:Ljava/io/OutputStream;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    return-void

    :catch_0
    move-exception v0

    :try_start_a
    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_b
    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1

    :catch_3
    move-exception v0

    move-object v1, v2

    :goto_1
    :try_start_c
    const-string v3, "\u000c\";+:!!:&\u0003/ /)+<"

    const/16 v4, 0x4e

    const/4 v5, 0x3

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\u01a4\u01d0\u01cf\u01cf\u01c6\u01c4\u01d5\u01c6\u01c5\u01b5\u01c9\u01d3\u01c6\u01c2\u01c5"
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    const/16 v5, 0x62

    const/16 v6, 0xff

    const/4 v7, 0x3

    :try_start_d
    invoke-static {v4, v5, v6, v7}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "r\u0093O\u00e0\u0000-%}\u00e2>\u00a7\u00e2\u008f\u00e5\u00c0\u0016t\u00d5\u00ae5\u00bf^\u00ff*\u0007\u00ad0"

    const/16 v7, 0xd3

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2

    goto :goto_0

    :catch_4
    move-exception v0

    throw v0

    :catch_5
    move-exception v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public static b0411Б041104110411Б()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static b0411ББ04110411Б()I
    .locals 1

    const/16 v0, 0x28

    return v0
.end method

.method public static bБ0411041104110411Б()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public cancel()V
    .locals 7

    const/4 v5, 0x5

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    if-eqz v0, :cond_1

    const-string v0, "\ufffe(1!0++0$\t\u001d*\u001d#!."
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/16 v1, 0xe6

    :try_start_1
    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    :try_start_2
    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b041104110411Б0411Б:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБББ04110411Б:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I

    if-eq v2, v3, :cond_0

    const/16 v2, 0x2d

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v2, 0x56

    :try_start_3
    sput v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    :cond_0
    const/16 v2, 0xa2

    const/4 v3, 0x2

    :try_start_4
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u00d9HF\u0094\u00d5%=\u00eeM\u0093\u0013\u00cfR\u00ef\u00a6"
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    const/16 v2, 0xc1

    const/4 v3, 0x1

    :try_start_5
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, ";\u0001E<\u00bae\u00d2)\u00ac\u00fdW]\u0085\u00cc\u0000\u0007"

    const/16 v3, 0xa8

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :try_start_6
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b0411ББ04110411Б()I

    move-result v1

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b041104110411Б0411Б:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБББ04110411Б:I

    rem-int/2addr v1, v2
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    packed-switch v1, :pswitch_data_0

    const/16 v1, 0x15

    :try_start_7
    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b0411ББ04110411Б()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    :pswitch_0
    :try_start_8
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    :try_start_9
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    const/4 v1, 0x4

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b041104110411Б0411Б:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБББ04110411Б:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_1

    sput v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b0411ББ04110411Б()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I

    :pswitch_1
    const/4 v2, 0x0

    :try_start_a
    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/manager/BluetoothManager;->access$12(Lcom/sec/android/safe/manager/BluetoothManager;ILjava/lang/String;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_b
    const-string v1, "r\u009c\u00a5\u0095\u00a4\u009f\u009f\u00a4\u0098}\u0091\u009e\u0091\u0097\u0095\u00a2"
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3

    const/16 v2, 0x30

    const/4 v3, 0x5

    :try_start_c
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\ufe75\ufea1\ufea0\ufea0\ufe97\ufe95\ufea6\ufe97\ufe96\ufe86\ufe9a\ufea4\ufe97\ufe93\ufe96"

    const/16 v3, 0x9a

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "LC@\\J\u0007\u0006\u000f@I\u000fL@AAJL[\u000f\\@LDJ[\u000fINFCJK\u000f\u0015\u000f"

    const/16 v5, 0x2f

    const/4 v6, 0x3

    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_d
    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2

    :catch_2
    move-exception v0

    throw v0

    :catch_3
    move-exception v0

    :try_start_e
    throw v0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2

    :catch_4
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public run()V
    .locals 7

    :try_start_0
    const-string v0, "_\u00a3\u00c1\u001a\r2\u0096\u00d2M\u008e\u00b0`\u00a4\u00c4D\u00c2"

    const/16 v1, 0xe

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "w\u00a3\u00a2\u00a2\u0099\u0097\u00a8\u0099\u0098\u0088\u009c\u00a6\u0099\u0095\u0098"

    const/16 v2, 0x99

    const/16 v3, 0x65

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\ufd5a\ufd5d\ufd5f\ufd61\ufd66\ufd38\ufd85\ufd5b\ufd87\ufd86\ufd86\ufd7d\ufd7b\ufd8c\ufd7d\ufd7c\ufd6c\ufd80\ufd8a\ufd7d\ufd79\ufd7c"

    const/16 v3, 0xf8

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x400

    new-array v0, v0, [B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->mmInStream:Ljava/io/InputStream;

    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    invoke-static {v2}, Lcom/sec/android/safe/manager/BluetoothManager;->access$10(Lcom/sec/android/safe/manager/BluetoothManager;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v1, v4, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "\uff22\uff4c\uff55\uff45\uff54\uff4f\uff4f\uff54\uff48\uff2d\uff41\uff4e\uff41\uff47\uff45\uff52"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    const/16 v2, 0x60

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b041104110411Б0411Б:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБББ04110411Б:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I

    if-eq v3, v4, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b0411ББ04110411Б()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    const/4 v3, 0x3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I

    :cond_0
    const/4 v3, 0x0

    :try_start_3
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u011b\u0147\u0146\u0146\u013d\u013b\u014c\u013d\u013c\u012c\u0140\u014a\u013d\u0139\u013c"

    const/16 v3, 0x6c

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\u0094\u0099\u00a3\u0093\u009f\u009e\u009e\u0095\u0093\u00a4\u0095\u0094PjP"

    const/16 v5, 0x10

    const/4 v6, 0x6

    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :try_start_4
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    invoke-static {v0}, Lcom/sec/android/safe/manager/BluetoothManager;->access$11(Lcom/sec/android/safe/manager/BluetoothManager;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    return-void

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    throw v0
.end method

.method public write([B)V
    .locals 7

    const/4 v6, 0x1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->mmOutStream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "\uffeb\u0015\u001e\u000e\u001d\u0018\u0018\u001d\u0011\ufff6\n\u0017\n\u0010\u000e\u001b"

    const/16 v2, 0x3d

    const/16 v3, 0x1a

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\uff9f\uffcb\uffca\uffca\uffc1\uffbf\uffd0\uffc1\uffc0\uffb0\uffc4\uffce\uffc1\uffbd\uffc0"

    const/16 v3, 0xba

    const/16 v4, 0x16

    const/4 v5, 0x2

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v2

    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b041104110411Б0411Б:I

    add-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБББ04110411Б:I

    rem-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I

    if-eq v4, v5, :cond_0

    const/16 v4, 0x32

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b0411ББ04110411Б()I

    move-result v4

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I

    :cond_0
    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411041104110411Б()I

    move-result v5

    add-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБББ04110411Б:I

    rem-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I

    :pswitch_0
    packed-switch v6, :pswitch_data_0

    :goto_1
    packed-switch v6, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    if-eq v4, v5, :cond_1

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b041104110411Б0411Б:I

    add-int/2addr v5, v4

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБББ04110411Б:I

    rem-int/2addr v4, v5

    packed-switch v4, :pswitch_data_2

    const/16 v4, 0x19

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b0411ББ04110411Б()I

    move-result v4

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I

    :pswitch_2
    const/16 v4, 0x4a

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ04110411Б0411Б:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->b0411ББ04110411Б()I

    move-result v4

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectedThread;->bБ0411Б04110411Б:I

    :cond_1
    :try_start_3
    const-string v4, "\uff04\uff37\uff22\uff24\uff2f\uff33\uff28\uff2e\uff2d\ufedf\uff23\uff34\uff31\uff28\uff2d\uff26\ufedf\uff36\uff31\uff28\uff33\uff24"

    const/16 v5, 0x6b

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v4

    :try_start_4
    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method
