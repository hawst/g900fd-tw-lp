.class Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/safe/manager/BluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectThread"
.end annotation


# static fields
.field public static b043A043Aк043Aкк:I = 0x2

.field public static b043Aкк043Aкк:I = 0x0

.field public static bк043Aк043Aкк:I = 0x1

.field public static bккк043Aкк:I = 0x1c


# instance fields
.field private isFetchUuidsWithSdp:Z

.field private isUnabled:Z

.field private mSocketType:Ljava/lang/String;

.field private mmDevice:Landroid/bluetooth/BluetoothDevice;

.field private mmSocket:Landroid/bluetooth/BluetoothSocket;

.field final synthetic this$0:Lcom/sec/android/safe/manager/BluetoothManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/safe/manager/BluetoothManager;Landroid/bluetooth/BluetoothDevice;ZZ)V
    .locals 8

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->isFetchUuidsWithSdp:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    sget v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bк043Aк043Aкк:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043A043Aк043Aкк:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x4

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    const/16 v0, 0x4e

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I

    :pswitch_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->isUnabled:Z

    iput-object p2, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmDevice:Landroid/bluetooth/BluetoothDevice;

    const/4 v0, 0x0

    if-eqz p3, :cond_3

    const-string v1, "\ufd5f\ufd71\ufd6f\ufd81\ufd7e\ufd71"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/16 v2, 0xfc

    const/4 v3, 0x0

    :try_start_2
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    :goto_0
    :try_start_3
    iput-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mSocketType:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    if-eqz p3, :cond_1

    :try_start_4
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->access$3()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v0

    :cond_0
    :goto_1
    :try_start_5
    iput-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    return-void

    :cond_1
    :try_start_6
    const-string v1, "\u00ca\u00f4\u00fd\u00ed\u00fc\u00f7\u00f7\u00fc\u00f0\u00d5\u00e9\u00f6\u00e9\u00ef\u00ed\u00fa"

    const/16 v2, 0x88

    const/4 v3, 0x5

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "5555555555+Nzyypn\u007f_s}plo"

    const/16 v3, 0xd6

    const/16 v4, 0xcb

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\uffe7\ufff6\uffe9\uffe5\ufff8\uffe9\uffc4\uffed\ufff2\ufff7\uffe9\uffe7\ufff9\ufff6\uffe9\uffc4\ufff6\uffea\uffe7\ufff3\ufff1\ufff1\uffc4\ufff7\ufff3\uffe7\uffef\uffe9\ufff8\uffc4\uffde\uffc4\n\u0013\u0016\u0007\t\uffc4\u0007\u0013\u0012\u0012\t\u0007\u0018\uffc4\u001b\r\u0018\u000c\u0013\u0019\u0018\uffc4\n\r\u0012\u0008\uffc4\u0017\u0014\u0014\uffc4\u0019\u0019\r\u0008\uffc4\uffce\uffce\uffce\uffce\uffce\uffce\uffce\uffce\uffce\uffce"

    const/16 v4, 0xbf

    const/16 v5, 0x63

    const/4 v6, 0x2

    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager;->access$4()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/bluetooth/BluetoothDevice;->createInsecureRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    move-result-object v0

    :try_start_7
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bк043Aк043Aкк:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043A043Aк043Aкк:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    :try_start_8
    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    :try_start_9
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v1

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bк043Aк043Aкк:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043A043Aк043Aкк:I

    rem-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aк043A043Aкк()I

    move-result v3

    if-eq v2, v3, :cond_2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v2

    sput v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I

    :cond_2
    :try_start_a
    const-string v2, "x\u00a2\u00ab\u009b\u00aa\u00a5\u00a5\u00aa\u009e\u0083\u0097\u00a4\u0097\u009d\u009b\u00a8"
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    const/16 v3, 0x5a

    const/16 v4, 0x90

    const/4 v5, 0x2

    :try_start_b
    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2

    move-result-object v2

    :try_start_c
    const-string v3, "\u009f\u00cb\u00ca\u00ca\u00c1\u00bf\u00d0\u00b0\u00c4\u00ce\u00c1\u00bd\u00c0"

    const/16 v4, 0x2e

    const/4 v5, 0x2

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\uff69\uff85\uff79\uff81\uff7b\uff8a\uff36\uff6a\uff8f\uff86\uff7b\uff50\uff36"

    const/16 v6, 0x75

    const/4 v7, 0x7

    invoke-static {v5, v6, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mSocketType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\uffa0\uffaf\uffa2\uff9e\uffb1\uffa2\uff65\uff66\uff5d\uffa3\uff9e\uffa6\uffa9\uffa2\uffa1\uff5d\uff77\uff5d"

    const/16 v6, 0x41

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    move-result-object v1

    :try_start_d
    invoke-static {v2, v3, v1}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v0

    throw v0

    :cond_3
    :try_start_e
    const-string v1, "\ufd6d\ufd92\ufd97\ufd89\ufd87\ufd99\ufd96\ufd89"

    const/16 v2, 0xf4

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0

    move-result-object v1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic access$0(Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;)Lcom/sec/android/safe/manager/BluetoothManager;
    .locals 3

    :pswitch_0
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    :try_start_0
    sget v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bк043Aк043Aкк:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043A043Aк043Aкк:I

    rem-int/2addr v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bк043Aк043Aкк:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043A043Aк043Aкк:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_2

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I

    :pswitch_2
    :try_start_1
    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eq v0, v1, :cond_0

    :try_start_2
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v0

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    const/16 v0, 0x46

    sput v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    sget v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bк043Aк043Aкк:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043A043Aк043Aкк:I

    rem-int/2addr v1, v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    packed-switch v1, :pswitch_data_3

    :try_start_4
    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v1

    sput v1, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :pswitch_3
    return-object v0

    :catch_0
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method

.method public static b043Aк043A043Aкк()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static bк043A043A043Aкк()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public static bкк043A043Aкк()I
    .locals 1

    const/16 v0, 0x35

    return v0
.end method

.method private setfetchUuidsWithSdpTimer(Landroid/bluetooth/BluetoothDevice;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/android/safe/manager/BluetoothManager;->access$8(Lcom/sec/android/safe/manager/BluetoothManager;Ljava/util/Timer;)V

    new-instance v0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread$1;-><init>(Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;Landroid/bluetooth/BluetoothDevice;)V

    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    invoke-static {v1}, Lcom/sec/android/safe/manager/BluetoothManager;->access$9(Lcom/sec/android/safe/manager/BluetoothManager;)Ljava/util/Timer;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    throw v0
.end method


# virtual methods
.method public cancel()V
    .locals 8

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    if-eqz v0, :cond_0

    const-string v0, "\uff4e\uff78\uff81\uff71\uff80\uff7b\uff7b\uff80\uff74\uff59\uff6d\uff7a\uff6d\uff73\uff71\uff7e"

    const/16 v1, 0x7a

    const/4 v2, 0x7

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "l\u0098\u0097\u0097\u008e\u008c\u009d}\u0091\u009b\u008e\u008a\u008d"

    const/16 v2, 0x29

    const/4 v3, 0x5

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u01b6\u01b6\u019c\u01b8\u01ac\u01b4\u01ae\u01bd\u0177\u01ac\u01b5\u01b8\u01bc\u01ae\u0171\u0172"

    const/16 v3, 0x56

    const/16 v4, 0xf3

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :cond_0
    :try_start_1
    const-string v0, "\u00d1\u00ff\u00e6\u00f6\u00e7\u00fc\u00fc\u00e7\u00fb\u00de\u00f2\u00fd\u00f2\u00f4\u00f6\u00e1"

    const/16 v1, 0x93

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0\\[[RPaAU_RNQ"
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    const/16 v2, 0xdf

    const/16 v3, 0xcc

    const/4 v4, 0x2

    :try_start_2
    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\uffea\uffea\uffd0\uffec\uffe0\uffe8\uffe2\ufff1\uff9d\uffe6\ufff0\uff9d\uffeb\ufff2\uffe9\uffe9\uff9e\uff9e\uff9e"
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    const/16 v3, 0x97

    const/16 v4, 0x14

    const/4 v5, 0x2

    :try_start_3
    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v2

    :try_start_4
    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bк043Aк043Aкк:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043A043Aк043Aкк:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :try_start_5
    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I

    if-eq v3, v4, :cond_1

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    :cond_1
    :try_start_6
    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_7
    const-string v1, "0ZcSb]]bV;O\\OUS`"
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    const/16 v2, 0x12

    const/4 v3, 0x4

    :try_start_8
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0184\u01b0\u01af\u01af\u01a6\u01a4\u01b5\u0195\u01a9\u01b3\u01a6\u01a2\u01a5"

    const/16 v3, 0xef

    const/16 v4, 0x52

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bк043Aк043Aкк:I

    add-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043A043Aк043Aкк:I

    rem-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I

    if-eq v4, v5, :cond_2

    const/16 v4, 0x59

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v4

    sput v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I

    :cond_2
    :try_start_9
    const-string v4, "\uff4a\uff53\uff56\uff5a\uff4c\uff0f\uff10\uff07\uff56\uff4d\uff07\uff4a\uff56\uff55\uff55\uff4c\uff4a\uff5b\uff07"

    const/16 v5, 0x7f

    const/16 v6, 0x9a

    const/4 v7, 0x1

    invoke-static {v4, v5, v6, v7}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mSocketType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\uffc9\u001c\u0018\u000c\u0014\u000e\u001d\uffc9\u000f\n\u0012\u0015\u000e\r"

    const/16 v5, 0x57

    const/4 v6, 0x4

    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    move-result-object v0

    sget v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bк043Aк043Aкк:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    mul-int/2addr v3, v4

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bк043A043A043Aкк()I

    move-result v4

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I

    if-eq v3, v4, :cond_3

    const/16 v3, 0xb

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bккк043Aкк:I

    invoke-static {}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->bкк043A043Aкк()I

    move-result v3

    sput v3, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->b043Aкк043Aкк:I

    :cond_3
    :try_start_a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    throw v0
.end method

.method public run()V
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x7

    const/4 v6, 0x6

    const/4 v7, 0x0

    iget-boolean v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->isUnabled:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    const-string v1, "0)e\u00b2\u009d\u00bdH\u00ba\u0090i\u0091\u001f"

    const/16 v2, 0x6d

    invoke-static {v1, v2, v9}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/safe/manager/BluetoothManager;->access$6(Lcom/sec/android/safe/manager/BluetoothManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_0
    const-string v1, "\uff01\uff2b\uff34\uff24\uff33\uff2e\uff2e\uff33\uff27\uff0c\uff20\uff2d\uff20\uff26\uff24\uff31"

    const/16 v2, 0xc9

    const/16 v3, 0x78

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0121\u0121\u0121\u0121\u0121\u0121\u0121\u0121\u0121\u0121\u0117\u013a\u0166\u0165\u0165\u015c\u015a\u016b\u014b\u015f\u0169\u015c\u0158\u015b"

    const/16 v3, 0xf7

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\uff78\uff73\uff7b\uff7e\uff52\uff86\uff81\uff52\uff75\uff81\uff80\uff80\uff77\uff75\uff86\uff5e\uff52\uff75\uff7e\uff81\uff85\uff77\uff52\uff86\uff7a\uff77\uff52\uff85\uff81\uff75\uff7d\uff77\uff86\uff52\uff5c\uff5c\uff5c\uff5c\uff5c\uff5c\uff5c\uff5c\uff5c\uff5c"

    const/16 v4, 0xce

    const/4 v5, 0x4

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "\ufeda\uff04\uff0d\ufefd\uff0c\uff07\uff07\uff0c\uff00\ufee5\ufef9\uff06\ufef9\ufeff\ufefd\uff0a"

    const/16 v2, 0xed

    const/16 v3, 0x7b

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u014d\u014d\u014d\u014d\u014d\u014d\u014d\u014d\u014d\u014d\u0143\u0166\u0192\u0191\u0191\u0188\u0186\u0197\u0177\u018b\u0195\u0188\u0184\u0187"

    const/16 v3, 0x61

    const/4 v4, 0x6

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\uffd4\uffcf\uffd7\uffda\uffae\uffe2\uffdd\uffae\uffd1\uffdd\uffdc\uffdc\uffd3\uffd1\uffe2\uffba\uffae\uffd7\uffe1\uffae\uffd1\uffdd\uffdc\uffdc\uffd3\uffd1\uffe2\uffd3\uffd2\uffae\uffc8\uffae"

    const/16 v5, 0x39

    const/4 v6, 0x7

    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothSocket;->isConnected()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\u020f\u0219\u0219\u0219\u0219\u0219\u0219\u0219\u0219\u0219\u0219"

    const/16 v5, 0xa5

    const/4 v6, 0x6

    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "\u00cf\u00f9\u0102\u00f2\u0101\u00fc\u00fc\u0101\u00f5\u00da\u00ee\u00fb\u00ee\u00f4\u00f2\u00ff"

    const/16 v2, 0x53

    const/16 v3, 0xe0

    const/4 v4, 0x2

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0008\u0008\u0008\u0008\u0008\u0008\u0008\u0008\u0008\u0008\ufffe!MLLCAR2FPC?B"

    const/16 v3, 0x22

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\uff11\uff0c\uff14\uff17\ufeeb\uff1f\uff1a\ufeeb\uff0e\uff1a\uff19\uff19\uff10\uff0e\uff1f\ufef7\ufeeb\uff30\ufeeb\uff05\ufeeb"

    const/16 v5, 0x67

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\uff60\uff6a\uff6a\uff6a\uff6a\uff6a\uff6a\uff6a\uff6a\uff6a"

    const/16 v4, 0xba

    const/4 v5, 0x6

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    const-string v1, "\u00d8\u00df\u00d7\u00d2\u00e1\u00d3\u00db\u00cd\u00cd\u00df\u00d9\u00db"

    const/16 v2, 0xbe

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/safe/manager/BluetoothManager;->access$6(Lcom/sec/android/safe/manager/BluetoothManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_0
    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    invoke-static {v0}, Lcom/sec/android/safe/manager/BluetoothManager;->access$2(Lcom/sec/android/safe/manager/BluetoothManager;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    move-result v0

    const-string v1, "\u00d4\u00fe\u0107\u00f7\u0106\u0101\u0101\u0106\u00fa\u00df\u00f3\u0100\u00f3\u00f9\u00f7\u0104"

    const/16 v2, 0xa9

    const/16 v3, 0x17

    invoke-static {v1, v2, v3, v7}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u000c877.,=\u001d1;.*-"

    const/16 v3, 0x58

    const/16 v4, 0x21

    invoke-static {v2, v3, v4, v10}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\uff05\uff03\uff10\uff05\uff07\uff0e\ufee6\uff0b\uff15\uff05\uff11\uff18\uff07\uff14\uff1b\ufec2\ufedc\ufec2"

    const/16 v5, 0xaf

    invoke-static {v4, v5, v8}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :try_start_1
    const-string v0, "\u01f5\u021f\u0228\u0218\u0227\u0222\u0222\u0227\u021b\u0200\u0214\u0221\u0214\u021a\u0218\u0225"

    const/16 v1, 0x91

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uff7f\uff7f\uff7f\uff7f\uff7f\uff7f\uff7f\uff7f\uff7f\uff7f\uff75\uff98\uffc4\uffc3\uffc3\uffba\uffb8\uffc9\uffa9\uffbd\uffc7\uffba\uffb6\uffb9"

    const/16 v2, 0x39

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\uffe3\uffe4\uffd1\uffe2\uffe4\uffb0\uffca\uffb0\uffe4\uffe2\uffe9\uffb0\uffe4\uffdf\uffb0\uffd3\uffdf\uffde\uffde\uffd5\uffd3\uffe4\uffb0\uffce\uffb0"

    const/16 v4, 0xd7

    const/16 v5, 0x67

    const/4 v6, 0x2

    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".8888888888"

    const/16 v4, 0xe

    const/4 v5, 0x5

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->connect()V

    const-string v0, "\ufefe\uff28\uff31\uff21\uff30\uff2b\uff2b\uff30\uff24\uff09\uff1d\uff2a\uff1d\uff23\uff21\uff2e"

    const/16 v1, 0xa2

    const/4 v2, 0x7

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uff7e\uff7e\uff7e\uff7e\uff7e\uff7e\uff7e\uff7e\uff7e\uff7e\uff74\uff97\uffc3\uffc2\uffc2\uffb9\uffb7\uffc8\uffa8\uffbc\uffc6\uffb9\uffb5\uffb8"

    :pswitch_1
    packed-switch v7, :pswitch_data_0

    :goto_2
    packed-switch v7, :pswitch_data_1

    goto :goto_2

    :pswitch_2
    const/16 v2, 0x30

    const/16 v3, 0xdc

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u0116\u0118\u0106\u0106\u0108\u0116\u0116\u00e3\u0117\u0112\u00e3\u0106\u0112\u0111\u0111\u0108\u0106\u0117\u00ef\u00e3\u010c\u0116\u00e3\u0106\u0112\u0111\u0111\u0108\u0106\u0117\u0108\u0107\u00e3\u00fd\u00e3"

    const/16 v4, 0x41

    const/4 v5, 0x6

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothSocket;->isConnected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\uff90\uff9a\uff9a\uff9a\uff9a\uff9a\uff9a\uff9a\uff9a\uff9a\uff9a"

    const/16 v4, 0x48

    const/4 v5, 0x7

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    monitor-enter v1

    :pswitch_3
    packed-switch v9, :pswitch_data_2

    :goto_3
    packed-switch v7, :pswitch_data_3

    goto :goto_3

    :pswitch_4
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/android/safe/manager/BluetoothManager;->access$7(Lcom/sec/android/safe/manager/BluetoothManager;Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;)V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmSocket:Landroid/bluetooth/BluetoothSocket;

    iget-object v2, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mmDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v3, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mSocketType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/safe/manager/BluetoothManager;->connected(Landroid/bluetooth/BluetoothSocket;Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    const-string v0, "\ufffe(1!0++0$\t\u001d*\u001d#!."

    const/16 v1, 0x22

    invoke-static {v0, v1, v8}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".ZYYPN_?S]PLO"

    invoke-static {v1, v8, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u0098\u009b\u009d\u009f\u00a4v\u00c3\u0099\u00c5\u00c4\u00c4\u00bb\u00b9\u00ca\u00aa\u00be\u00c8\u00bb\u00b7\u00bav\u00a9\u00c5\u00b9\u00c1\u00bb\u00ca\u00aa\u00cf\u00c6\u00bb\u0090"

    const/16 v4, 0x56

    const/4 v5, 0x5

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mSocketType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\u012e\u015a\u0159\u0159\u0150\u014e\u015f\u013f\u0153\u015d\u0150\u014c\u014f"

    const/16 v2, 0x60

    const/16 v3, 0x8b

    const/4 v4, 0x3

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mSocketType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->this$0:Lcom/sec/android/safe/manager/BluetoothManager;

    invoke-static {v0}, Lcom/sec/android/safe/manager/BluetoothManager;->access$2(Lcom/sec/android/safe/manager/BluetoothManager;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v0

    const-string v1, "\u00d7\u00f9\u00e0\u00f0\u00e1\u00fa\u00fa\u00e1\u00fd\u00d8\u00f4\u00fb\u00f4\u00f2\u00f0\u00e7"

    const/16 v2, 0x95

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0\\[[RPaAU_RNQ"

    const/16 v3, 0x35

    const/16 v4, 0x48

    invoke-static {v2, v3, v4, v7}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\u0208\u020d\u0217\u0207\u0213\u021a\u0209\u0216\u020d\u0212\u020b\u01c4\u01de\u01c4"

    const/16 v5, 0x8c

    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :pswitch_5
    packed-switch v7, :pswitch_data_4

    :goto_4
    packed-switch v7, :pswitch_data_5

    goto :goto_4

    :pswitch_6
    invoke-static {v2, v3, v4, v10}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\ufda5\ufd9e\ufd91\ufd92\ufd9c\ufd95\ufd50\ufda4\ufd9f\ufd50\ufd93\ufd9c\ufd9f\ufda3\ufd95\ufd58\ufd59\ufd50"

    const/16 v5, 0xf0

    invoke-static {v4, v5, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/safe/manager/BluetoothManager$ConnectThread;->mSocketType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\uff81\uffd4\uffd0\uffc4\uffcc\uffc6\uffd5\uff81\uffc5\uffd6\uffd3\uffca\uffcf\uffc8\uff81\uffc4\uffd0\uffcf\uffcf\uffc6\uffc4\uffd5\uffca\uffd0\uffcf\uff81\uffc7\uffc2\uffca\uffcd\uffd6\uffd3\uffc6"

    const/16 v5, 0xce

    const/16 v6, 0x2f

    invoke-static {v4, v5, v6, v10}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :goto_5
    packed-switch v7, :pswitch_data_6

    goto :goto_5

    :goto_6
    :pswitch_7
    packed-switch v7, :pswitch_data_7

    goto :goto_5

    :catch_1
    move-exception v0

    const-string v1, "\uffd6\u0000\t\ufff9\u0008\u0003\u0003\u0008\ufffc\uffe1\ufff5\u0002\ufff5\ufffb\ufff9\u0006"

    const/16 v2, 0x24

    invoke-static {v1, v2, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "BnmmdbsSgqd`c"

    const/16 v3, 0xad

    const/16 v4, 0xac

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
