.class public Lcom/sec/android/safe/util/Logger;
.super Ljava/lang/Object;


# static fields
.field public static final IS_DIVIDER:Z = false

# The value of this static final field might be set in the static constructor
.field public static final TAG:Ljava/lang/String; = "\uffd3\uffd0\uffc3\uffc3\uffec\uffe9\uffe5\uffee\ufff4"

.field public static b043E043Eо043Eо043E:I = 0x0

.field public static b043Eо043Eоо043E:I = 0x1

.field public static bо043E043Eоо043E:I = 0x2

.field public static bоо043Eоо043E:I = 0x36

.field private static isLogActivated:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    :try_start_0
    sget-object v0, Lcom/sec/android/safe/util/Logger;->TAG:Ljava/lang/String;

    const/16 v1, 0x40

    const/4 v2, 0x7

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :try_start_1
    sput-object v0, Lcom/sec/android/safe/util/Logger;->TAG:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :try_start_2
    sget v0, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    sget v1, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/util/Logger;->bо043E043Eоо043E:I

    rem-int/2addr v0, v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    sget v1, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    invoke-static {}, Lcom/sec/android/safe/util/Logger;->bооо043Eо043E()I

    move-result v2

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/util/Logger;->bо043E043Eоо043E:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    const/16 v1, 0x5d

    sput v1, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    const/16 v1, 0x27

    sput v1, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I

    :pswitch_0
    packed-switch v0, :pswitch_data_1

    :try_start_3
    sget v0, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    sget v1, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/util/Logger;->bо043E043Eоо043E:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_2

    const/16 v0, 0x3c

    sput v0, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    const/16 v0, 0x2b

    sput v0, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :pswitch_1
    :try_start_4
    invoke-static {}, Lcom/sec/android/safe/util/Logger;->b043E043E043Eоо043E()I

    move-result v0

    sput v0, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    const/16 v0, 0x1a

    sput v0, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    :pswitch_2
    const/4 v0, 0x1

    :try_start_5
    sput-boolean v0, Lcom/sec/android/safe/util/Logger;->isLogActivated:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    return-void

    :catch_0
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    :catch_2
    move-exception v0

    throw v0

    :catch_3
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    :catch_4
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, -0x1

    :try_start_0
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    new-array v1, v0, [I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Lcom/sec/android/safe/util/Logger;->b043E043E043Eоо043E()I

    move-result v0

    sput v0, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-void

    :catch_1
    move-exception v0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    throw v0
.end method

.method public static b043E043E043Eоо043E()I
    .locals 1

    const/16 v0, 0x1d

    return v0
.end method

.method public static b043Eо043E043Eо043E()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public static bоо043E043Eо043E()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static bооо043Eо043E()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    :try_start_0
    sget-boolean v0, Lcom/sec/android/safe/util/Logger;->isLogActivated:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_1
    const-string v0, "\ufe40\ufe3d\ufe30\ufe30\ufe59\ufe56\ufe52\ufe5b\ufe61"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    sget v1, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    sget v2, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/util/Logger;->bо043E043Eоо043E:I

    rem-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/util/Logger;->bоо043E043Eо043E()I

    move-result v2

    if-eq v1, v2, :cond_1

    :pswitch_0
    packed-switch v3, :pswitch_data_0

    :goto_1
    packed-switch v3, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    const/16 v1, 0x39

    sput v1, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    invoke-static {}, Lcom/sec/android/safe/util/Logger;->b043E043E043Eоо043E()I

    move-result v1

    sput v1, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    sget v1, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    sget v2, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    mul-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/util/Logger;->b043Eо043E043Eо043E()I

    move-result v2

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    if-eq v1, v2, :cond_1

    invoke-static {}, Lcom/sec/android/safe/util/Logger;->b043E043E043Eоо043E()I

    move-result v1

    sput v1, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    sput v3, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    :cond_1
    const/16 v1, 0xb1

    const/4 v2, 0x0

    :try_start_2
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lcom/sec/android/safe/util/Logger;->makeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    :try_start_0
    sget-boolean v0, Lcom/sec/android/safe/util/Logger;->isLogActivated:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget v0, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    sget v1, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/util/Logger;->bо043E043Eоо043E:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    const/16 v0, 0x3c

    sput v0, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    :pswitch_0
    :try_start_1
    const-string v0, "\uff5f\uff5c\uff4f\uff4f\uff78\uff75\uff71\uff7a\uff80"

    const/16 v1, 0x1e

    const/16 v2, 0xd6

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lcom/sec/android/safe/util/Logger;->makeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const/4 v1, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    move v0, v1

    :goto_0
    :try_start_0
    div-int/2addr v0, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const-string v0, "\uff89\uff86\uff79\uff79\uffa2\uff9f\uff9b\uffa4\uffaa"

    const/16 v2, 0xca

    invoke-static {v0, v2, v1}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    :pswitch_0
    packed-switch v3, :pswitch_data_0

    :goto_1
    packed-switch v3, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    invoke-static {p0, p1, p2}, Lcom/sec/android/safe/util/Logger;->makeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x49

    sput v0, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    sget-boolean v0, Lcom/sec/android/safe/util/Logger;->isLogActivated:Z

    :pswitch_2
    packed-switch v4, :pswitch_data_2

    :goto_3
    packed-switch v4, :pswitch_data_3

    goto :goto_3

    :pswitch_3
    if-nez v0, :cond_0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static makeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\u01c1"

    const/16 v2, 0xb0

    sget v3, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    sget v4, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/util/Logger;->bо043E043Eоо043E:I

    rem-int/2addr v3, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v3, :pswitch_data_0

    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/util/Logger;->b043E043E043Eоо043E()I

    move-result v3

    sput v3, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    invoke-static {}, Lcom/sec/android/safe/util/Logger;->b043E043E043Eоо043E()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    :try_start_2
    sput v3, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    :pswitch_0
    const/16 v3, 0xb6

    const/4 v4, 0x3

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u0010\uffd3"

    :pswitch_1
    packed-switch v6, :pswitch_data_1

    :goto_0
    packed-switch v5, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    const/16 v2, 0x26

    const/16 v3, 0x27

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    :try_start_3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\uff6c\uff8a\uff8a\uff6c"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    const/16 v2, 0x3c

    sget v3, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    invoke-static {}, Lcom/sec/android/safe/util/Logger;->bооо043Eо043E()I

    move-result v4

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/util/Logger;->bо043E043Eоо043E:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    if-eq v3, v4, :cond_0

    const/16 v3, 0x59

    sput v3, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    const/16 v3, 0x35

    sput v3, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    :cond_0
    const/4 v3, 0x0

    :try_start_4
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    sget-boolean v0, Lcom/sec/android/safe/util/Logger;->isLogActivated:Z

    if-nez v0, :cond_2

    :goto_0
    return-void

    :pswitch_0
    sget v2, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/util/Logger;->bо043E043Eоо043E:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    if-eq v1, v2, :cond_0

    const/16 v1, 0x4d

    sget v2, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    sget v3, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/util/Logger;->bо043E043Eоо043E:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/util/Logger;->b043E043E043Eоо043E()I

    move-result v2

    sput v2, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    invoke-static {}, Lcom/sec/android/safe/util/Logger;->b043E043E043Eоо043E()I

    move-result v2

    sput v2, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    :pswitch_1
    sput v1, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    const/16 v1, 0x53

    sput v1, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    :cond_0
    const/16 v1, 0x21

    const/16 v2, 0xa3

    :pswitch_2
    packed-switch v5, :pswitch_data_1

    :goto_1
    sget v3, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    sget v4, Lcom/sec/android/safe/util/Logger;->b043Eо043Eоо043E:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/util/Logger;->bо043E043Eоо043E:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    if-eq v3, v4, :cond_1

    const/16 v3, 0x1c

    sput v3, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    const/16 v3, 0x54

    sput v3, Lcom/sec/android/safe/util/Logger;->b043E043Eо043Eо043E:I

    :cond_1
    packed-switch v5, :pswitch_data_2

    goto :goto_1

    :pswitch_3
    packed-switch v5, :pswitch_data_3

    :goto_2
    packed-switch v6, :pswitch_data_4

    goto :goto_2

    :pswitch_4
    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lcom/sec/android/safe/util/Logger;->makeString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "\u0117\u0114\u0107\u0107\u0130\u012d\u0129\u0132\u0138"

    sget v1, Lcom/sec/android/safe/util/Logger;->bоо043Eоо043E:I

    :pswitch_5
    packed-switch v6, :pswitch_data_5

    :goto_3
    packed-switch v6, :pswitch_data_6

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method
