.class public Lcom/sec/android/safe/SecureStorageManagerClient;
.super Lcom/sec/android/safe/manager/EasySetupCallbackActivity;


# static fields
.field private static final DEVICEID_ERROR:I = 0x8

.field private static final DEVICE_CHANGED_FAIL:I = 0x6

.field private static final DEVICE_CHANGED_SUCCESS:I = 0x5

.field private static final FAIL_CONNECT:I = 0x7

# The value of this static final field might be set in the static constructor
.field private static final LOGIN_COUNT:Ljava/lang/String; = "\ufef1\uff14\uff0c\uff0e\uff13\ufee8\uff14\uff1a\uff13\uff19"

# The value of this static final field might be set in the static constructor
.field private static final MESSAGE:Ljava/lang/String; = "\u00be\u00d6\u00e4\u00e4\u00d2\u00d8\u00d6"

# The value of this static final field might be set in the static constructor
.field private static final PEER_ID:Ljava/lang/String; = "\u00fc\u0111\u0111\u011e\u00f5\u0110"

.field private static final PW_CHANGE:I = 0x1

.field private static final PW_DEVICE_CHANGED:I = 0x3

.field private static final PW_RECOVERY:I = 0x2

.field private static final PW_SETUP:I = 0x0

.field private static final RECOVERY_FAIL:I = 0x3

.field private static final RECOVERY_SUCCESS:I = 0x2

.field private static final SAFE_BTPKE_FAIL:I = 0x1

.field private static final SAFE_BTPKE_SUCCESS:I = 0x0

# The value of this static final field might be set in the static constructor
.field private static final SAMSUNG_ACCOUNT_ID:Ljava/lang/String; = "\u00d0\u0006E\u00b8\u00df\u00a4\u0092(\u00b0\u00a3\u00fbv\u0098Uy\u00b2"

.field private static final SS_CLIENT_MODE:I = 0x1

# The value of this static final field might be set in the static constructor
.field private static final SS_CLIENT_MSG_ACK_FROM_SERVER:Ljava/lang/String; = "\u00e5\u00d9"

# The value of this static final field might be set in the static constructor
.field private static final SS_CLIENT_MSG_CHECKTOKEN_ACK_FROM_SERVER:Ljava/lang/String; = "\u0013\u0014"

# The value of this static final field might be set in the static constructor
.field private static final SS_CLIENT_MSG_ENCRYPTION_ACK_FROM_SERVER:Ljava/lang/String; = "\u017d\u0183"

# The value of this static final field might be set in the static constructor
.field private static final SS_CLIENT_MSG_HELLO_FROM_SERVER:Ljava/lang/String; = "\u013b\u013d"

# The value of this static final field might be set in the static constructor
.field private static final SS_CLIENT_MSG_IMEI_ACK_FROM_SERVER:Ljava/lang/String; = "\uff38\uff40"

# The value of this static final field might be set in the static constructor
.field private static final SS_CLIENT_MSG_INITIAL_ACK_FROM_SERVER:Ljava/lang/String; = "\ufde1\ufde1"

# The value of this static final field might be set in the static constructor
.field private static final SS_SERVER_MSG_ACK_FROM_CLIENT:Ljava/lang/String; = "\uffb0\uffb3"

# The value of this static final field might be set in the static constructor
.field private static final SS_SERVER_MSG_ENCRYPTION_DATA_FROM_CLIENT:Ljava/lang/String; = "\u00d9\u00de"

# The value of this static final field might be set in the static constructor
.field private static final SS_SERVER_MSG_HELLO_CHANGEPW_FROM_CLIENT:Ljava/lang/String; = "PP"

# The value of this static final field might be set in the static constructor
.field private static final SS_SERVER_MSG_HELLO_FROM_CLIENT:Ljava/lang/String; = "\uff94\uff95"

# The value of this static final field might be set in the static constructor
.field private static final SS_SERVER_MSG_IMEI_DATA_FROM_CLIENT:Ljava/lang/String; = "\uff5c\uff63"

# The value of this static final field might be set in the static constructor
.field private static final SS_SERVER_MSG_INITIAL_SETUP_FROM_CLIENT:Ljava/lang/String; = "\uff73\uff73"

# The value of this static final field might be set in the static constructor
.field private static final SS_SERVER_MSG_RECOVERY_HELLO_FROM_CLIENT:Ljava/lang/String; = "\u0093\u0091"

# The value of this static final field might be set in the static constructor
.field private static final SS_SERVER_MSG_RESTORE_PW_FROM_CLIENT:Ljava/lang/String; = "\uff4a\uff4e"

# The value of this static final field might be set in the static constructor
.field private static final SS_SERVER_MSG_TOKENVAL_REQ_FROM_CLIENT:Ljava/lang/String; = "\uff31\uff3a"

# The value of this static final field might be set in the static constructor
.field private static final TAG:Ljava/lang/String; = "`}\u0093!\u000c\u0097\u0088\u0014a\u00e9.\u00b6Y\u00b0bl\u00f2"

.field private static final USER_BUSY:I = 0x4

.field public static b04110411ББ04110411:I = 0x2

.field public static b0411БББ04110411:I = 0x0

.field public static bБ0411ББ04110411:I = 0x1

.field public static bББББ04110411:I = 0x8

.field private static mBtAddr:Ljava/lang/String;

.field private static mInitMode:I

.field private static mLoginCount:I

.field private static mOldPW:Ljava/lang/String;

.field private static mPassword:Ljava/lang/String;

.field private static mPeerIDByte:[B

.field private static mPeerId:Ljava/lang/String;

.field private static mSamsungAccountId:Ljava/lang/String;


# instance fields
.field private final SS_CTX_OTHERS:I

.field private final SS_CTX_SETUP:I

.field private final SS_PKE_OTHERS:I

.field private final SS_PKE_SETUP:I

.field SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

.field private mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothManager:Lcom/sec/android/safe/manager/BluetoothManager;

.field private mCTX:I

.field protected mContext:Landroid/content/Context;

.field mHandlerSecureStorage:Landroid/os/Handler;

.field private mPKEMode:I

.field private mTargetDevice:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    :try_start_0
    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->TAG:Ljava/lang/String;

    const/16 v1, 0x15

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->TAG:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->PEER_ID:Ljava/lang/String;

    const/16 v1, 0x4b

    const/16 v2, 0x61

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->PEER_ID:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SAMSUNG_ACCOUNT_ID:Ljava/lang/String;

    :pswitch_0
    packed-switch v6, :pswitch_data_0

    :goto_0
    packed-switch v5, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const/16 v1, 0xeb

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SAMSUNG_ACCOUNT_ID:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->MESSAGE:Ljava/lang/String;

    const/16 v1, 0xde

    const/16 v2, 0x6d

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->MESSAGE:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->LOGIN_COUNT:Ljava/lang/String;

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eq v1, v2, :cond_0

    const/16 v1, 0x18

    :try_start_1
    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    const/16 v1, 0x8a

    const/16 v2, 0xd1

    const/4 v3, 0x1

    :try_start_2
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->LOGIN_COUNT:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_INITIAL_ACK_FROM_SERVER:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    const/16 v1, 0xc5

    const/4 v2, 0x0

    :try_start_3
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_INITIAL_ACK_FROM_SERVER:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_HELLO_FROM_SERVER:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const/16 v1, 0x11

    const/16 v2, 0xfa

    const/4 v3, 0x3

    :try_start_4
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_HELLO_FROM_SERVER:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_ACK_FROM_SERVER:Ljava/lang/String;

    const/16 v1, 0x20

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_ACK_FROM_SERVER:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_ENCRYPTION_ACK_FROM_SERVER:Ljava/lang/String;

    const/16 v1, 0x6f

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_ENCRYPTION_ACK_FROM_SERVER:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :try_start_5
    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_IMEI_ACK_FROM_SERVER:Ljava/lang/String;

    const/16 v1, 0xf8

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_IMEI_ACK_FROM_SERVER:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_CHECKTOKEN_ACK_FROM_SERVER:Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const/16 v1, 0x1e

    const/4 v2, 0x4

    :try_start_6
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CLIENT_MSG_CHECKTOKEN_ACK_FROM_SERVER:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_INITIAL_SETUP_FROM_CLIENT:Ljava/lang/String;

    const/16 v1, 0x9

    const/16 v2, 0xc6

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_INITIAL_SETUP_FROM_CLIENT:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_HELLO_FROM_CLIENT:Ljava/lang/String;

    const/16 v1, 0x5b

    const/16 v2, 0x41

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_HELLO_FROM_CLIENT:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_ACK_FROM_CLIENT:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    const/16 v1, 0x80

    const/4 v2, 0x4

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v3, v4

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v4

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v3, v4, :cond_1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v3, 0x4d

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_1
    :try_start_7
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_ACK_FROM_CLIENT:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_ENCRYPTION_DATA_FROM_CLIENT:Ljava/lang/String;

    const/16 v1, 0xa9

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_ENCRYPTION_DATA_FROM_CLIENT:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_IMEI_DATA_FROM_CLIENT:Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    :pswitch_2
    packed-switch v5, :pswitch_data_2

    :goto_1
    packed-switch v6, :pswitch_data_3

    goto :goto_1

    :pswitch_3
    const/16 v1, 0x60

    const/16 v2, 0x74

    const/4 v3, 0x1

    :try_start_8
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_IMEI_DATA_FROM_CLIENT:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_TOKENVAL_REQ_FROM_CLIENT:Ljava/lang/String;

    const/16 v1, 0x55

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_TOKENVAL_REQ_FROM_CLIENT:Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    :try_start_9
    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_HELLO_CHANGEPW_FROM_CLIENT:Ljava/lang/String;

    const/16 v1, 0x1f

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_HELLO_CHANGEPW_FROM_CLIENT:Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v0, v1, :cond_2

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sput v7, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_2
    :try_start_a
    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_RECOVERY_HELLO_FROM_CLIENT:Ljava/lang/String;

    const/16 v1, 0xa2

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_RECOVERY_HELLO_FROM_CLIENT:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_RESTORE_PW_FROM_CLIENT:Ljava/lang/String;

    const/16 v1, 0x4d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_SERVER_MSG_RESTORE_PW_FROM_CLIENT:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mPassword:Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    const/4 v0, 0x0

    :try_start_b
    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mSamsungAccountId:Ljava/lang/String;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1

    const/4 v0, 0x0

    :try_start_c
    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0

    const/4 v0, 0x0

    :try_start_d
    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBtAddr:Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mLoginCount:I

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mOldPW:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerIDByte:[B

    const/4 v0, -0x1

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1

    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v0, 0x2a

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_1

    const/16 v1, 0x43

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_1
    iput-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    iput v3, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CTX_SETUP:I

    const/4 v0, 0x3

    :pswitch_2
    packed-switch v3, :pswitch_data_2

    :goto_0
    packed-switch v3, :pswitch_data_3

    goto :goto_0

    :pswitch_3
    iput v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CTX_OTHERS:I

    iput v3, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_PKE_SETUP:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_PKE_OTHERS:I

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/sec/android/safe/manager/EasySetupCallbackActivity;-><init>()V

    const/4 v0, 0x0

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v2

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v1, 0x3e

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    iput-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    iput v4, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CTX_SETUP:I

    iput v6, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_CTX_OTHERS:I

    iput v4, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_PKE_SETUP:I

    :pswitch_0
    packed-switch v4, :pswitch_data_0

    :goto_0
    packed-switch v5, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v5, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SS_PKE_OTHERS:I

    invoke-static {}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->getInstance()Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    const-string v0, "\u0111\u00ff\u0104\u0103\u011d\u0100\u0112\u010e\u0109\u0103\u011d\u0101\u010a\u0107\u0103\u010c\u0112"

    const/16 v1, 0x5f

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uff67\uff79\uff77\uff89\uff86\uff79\uff67\uff88\uff83\uff86\uff75\uff7b\uff79\uff61\uff75\uff82\uff75\uff7b\uff79\uff86\uff57\uff80\uff7d\uff79\uff82\uff88"

    const/16 v2, 0x6e

    const/16 v3, 0x7e

    invoke-static {v1, v2, v3, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_2

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_2
    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v2, v3, :cond_1

    :pswitch_3
    packed-switch v4, :pswitch_data_3

    :goto_1
    packed-switch v4, :pswitch_data_4

    goto :goto_1

    :pswitch_4
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v2, 0x57

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_1
    const-string v2, "\u0119\u0148\u013b\u0137\u014a\u013b\u00f6\u0129\u013b\u0139\u014b\u0148\u013b\u0129\u014a\u0145\u0148\u0137\u013d\u013b\u0123\u0137\u0144\u0137\u013d\u013b\u0148"

    const/16 v3, 0x52

    const/16 v4, 0x84

    invoke-static {v2, v3, v4, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mContext:Landroid/content/Context;

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public static b041104110411Б04110411()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static b0411Б0411Б04110411()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public static bБ04110411Б04110411()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static bББ0411Б04110411()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method


# virtual methods
.method public SendJsonMsg([BLjava/lang/String;)V
    .locals 9

    const/16 v8, 0x1e

    const/4 v7, 0x3

    const/4 v3, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-static {p1, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\u00b0\u00c2\u00cb\u00c1\u00a7\u00d0\u00cc\u00cb\u00aa\u00d0\u00c4\u0097}\u00a2\u00cb\u00c0\u00c1\u00cc\u00c6\u00cb\u00c4}\u00aa\u00d0\u00c4}\u00a3\u009e\u00a6\u00a9\u00d0\u008b\u008b\u008b\u0085\u00cd\u00cf\u00cc\u00d1\u00cc\u00c0\u00cc\u00c9}\u00cb\u00d2\u00ca\u00bf\u00c2\u00cf\u0097}"

    const/16 v2, 0x5f

    invoke-static {v1, v3, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "8"

    invoke-static {v1, v8, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u00b0"

    const/16 v3, 0x8a

    invoke-static {v2, v3, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    :pswitch_1
    packed-switch v5, :pswitch_data_0

    :goto_1
    packed-switch v5, :pswitch_data_1

    goto :goto_1

    :pswitch_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    :pswitch_3
    packed-switch v6, :pswitch_data_2

    :goto_2
    packed-switch v5, :pswitch_data_3

    goto :goto_2

    :pswitch_4
    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_4

    const/16 v1, 0x4e

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u0086tyx\u0092u\u0087\u0083~x\u0092v\u007f|x\u0081\u0087"

    const/16 v2, 0x11

    const/4 v3, 0x6

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "M_h^DmihGma"

    const/4 v3, 0x7

    invoke-static {v2, v7, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u0162\u0154\u015f\u0153\u010b\u0130\u0159\u014e\u014f\u015a\u0154\u0159\u0152\u010b\u0138\u015e\u0152\u0119\u0119\u0119"

    const/16 v4, 0xeb

    const/4 v5, 0x5

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string v2, "c\u00c4\u0011\u0090\u001c\u00bf"

    const/16 v3, 0xd5

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "\u0105\u0113\u011f\u0125\u0127\u0120\u0119\u00f3\u0115\u0115\u0121\u0127\u0120\u0126\u00fb\u0116"

    const/16 v3, 0x59

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/safe/SecureStorageManagerClient;->mSamsungAccountId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "\u014e\u0166\u0174\u0174\u0162\u0168\u0166"

    const/16 v3, 0xd0

    const/16 v4, 0x31

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "\u00f0\u0113\u010b\u010d\u0112\u00e7\u0113\u0119\u0112\u0118"
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, 0x52

    const/4 v3, 0x2

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v5, v4

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v4, v5

    packed-switch v4, :pswitch_data_5

    const/16 v4, 0x3d

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sput v8, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_6
    :try_start_1
    invoke-static {v0, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->mLoginCount:I

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_3
    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBluetoothManager:Lcom/sec/android/safe/manager/BluetoothManager;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/safe/manager/BluetoothManager;->sendMessage(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "!!\u0008"

    const/16 v3, 0x32

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v3

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_6

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_7
    packed-switch v6, :pswitch_data_7

    :goto_4
    packed-switch v6, :pswitch_data_8

    goto :goto_4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_6
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_7
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x0
        :pswitch_0
        :pswitch_7
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public changePasswd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    :pswitch_0
    packed-switch v1, :pswitch_data_0

    :goto_0
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    packed-switch v1, :pswitch_data_2

    :goto_1
    packed-switch v0, :pswitch_data_3

    goto :goto_1

    :pswitch_2
    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_4

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x36

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v0, 0x61

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    const/16 v0, 0x56

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_5

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v0, 0x38

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_3
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_4
    :try_start_0
    sput-object p1, Lcom/sec/android/safe/SecureStorageManagerClient;->mPassword:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    sput-object p2, Lcom/sec/android/safe/SecureStorageManagerClient;->mOldPW:Ljava/lang/String;

    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mCTX:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mPKEMode:I

    const/4 v0, 0x1

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->connect()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method

.method public completeProcess()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    :try_start_0
    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "\u00b6\u00a4\u00a9\u00a8\u00c2\u00a5\u00b7\u00b3\u00ae\u00a8\u00c2\u00a6\u00af\u00ac\u00a8\u00b1\u00b7"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x52

    const/16 v2, 0xb5

    const/4 v3, 0x2

    :try_start_1
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u010e\u011a\u0118\u011b\u0117\u0110\u011f\u0110\u00fb\u011d\u011a\u010e\u0110\u011e\u011e"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v2, 0xab

    const/4 v3, 0x5

    :try_start_2
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u000b\u001c..2*-\u001f\uffdb\u000e\u0000\u000f\u0010\u000b\uffdb\ufffe\n\u0008\u000b\u0007\u0000\u000f\u0000\uffdc\uffdc\uffdc"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const/16 v3, 0x45

    const/4 v4, 0x4

    :try_start_3
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v2

    :try_start_4
    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendMsgToHandler(Landroid/os/Handler;I)V

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->stopBT()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    const/4 v0, -0x1

    :try_start_5
    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    return-void

    :pswitch_0
    :try_start_6
    const-string v0, "\u00b7\u00a5\u00a2\u00a1\u00bb\u00a6\u00b0\u00b4\u00af\u00a1\u00bb\u00a7\u00a8\u00ad\u00a1\u00aa\u00b0"

    const/16 v1, 0xe4

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u0135\u0141\u013f\u0142\u013e\u0137\u0146\u0137\u0122\u0144\u0141\u0135\u0137\u0145\u0145"

    const/16 v2, 0x69

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    if-eq v2, v3, :cond_0

    const/16 v2, 0x2f

    :try_start_7
    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/4 v2, 0x2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    :cond_0
    :try_start_8
    const-string v2, "\u00d2\u00e3\u00f5\u00f5\u00f9\u00f1\u00f4\u00e6\u00a2\u00c6\u00c7\u00d8\u00cb\u00c5\u00c7\u00e1\u00c5\u00ca\u00c3\u00d0\u00c9\u00c7\u00c6\u00a2\u00d4\u00c7\u00d5\u00d6\u00d1\u00d4\u00c7\u00a2\u00c5\u00d1\u00cf\u00d2\u00ce\u00c7\u00d6\u00c7\u00a3\u00a3\u00a3"
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    const/16 v3, 0x82

    const/4 v4, 0x5

    :try_start_9
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v3, v4, :cond_1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    const/16 v3, 0x4d

    :try_start_a
    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_1
    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendMsgToHandler(Landroid/os/Handler;I)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :pswitch_1
    :try_start_b
    const-string v0, "(\u0016\u001b\u001a4\u0017)% \u001a4\u0018!\u001e\u001a#)"

    const/16 v1, 0x9

    const/16 v2, 0x34

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u007f\u008b\u0089\u008c\u0088\u0081\u0090\u0081l\u008e\u008b\u007f\u0081\u008f\u008f"

    const/16 v2, 0xe

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Tewwskv`$GLEJCA$GKITHAPA%%%"

    const/4 v3, 0x4

    const/4 v4, 0x3

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendMsgToHandler(Landroid/os/Handler;I)V

    goto/16 :goto_0

    :pswitch_2
    const-string v0, "\u0215\u0203\u0208\u0207\u0221\u0204\u0216\u0212\u020d\u0207\u0221\u0205\u020e\u020b\u0207\u0210\u0216"
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    const/16 v1, 0xe1

    const/4 v2, 0x2

    :try_start_c
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\ufef6\uff02\uff00\uff03\ufeff\ufef8\uff07\ufef8\ufee3\uff05\uff02\ufef6\ufef8\uff06\uff06"

    const/16 v2, 0xbd

    const/16 v3, 0xb0

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u020c\u021d\u022f\u022f\u0233\u022b\u022e\u0220\u01dc\u020e\u0201\u01ff\u020b\u0212\u0201\u020e\u0215\u01dc\u01ff\u020b\u0209\u020c\u0208\u0201\u0210\u0201\u01dd\u01dd\u01dd"
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1

    const/16 v3, 0x94

    :pswitch_3
    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v5, v4

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v4, v5

    packed-switch v4, :pswitch_data_1

    const/16 v4, 0x9

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v4, 0x4f

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_4
    packed-switch v6, :pswitch_data_2

    :goto_1
    packed-switch v7, :pswitch_data_3

    goto :goto_1

    :pswitch_5
    const/4 v4, 0x6

    :try_start_d
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendMsgToHandler(Landroid/os/Handler;I)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public connect()V
    .locals 6

    :try_start_0
    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBtAddr:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->getBTDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/safe/manager/BluetoothManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/safe/manager/BluetoothManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBluetoothManager:Lcom/sec/android/safe/manager/BluetoothManager;

    iget-object v1, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBluetoothManager:Lcom/sec/android/safe/manager/BluetoothManager;

    invoke-virtual {v1, p0}, Lcom/sec/android/safe/manager/BluetoothManager;->changeBluetoothCallbackHandler(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBluetoothManager:Lcom/sec/android/safe/manager/BluetoothManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_1

    :try_start_1
    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x40

    :try_start_2
    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :try_start_3
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :pswitch_0
    :try_start_4
    const-string v0, "\u00f0\u00de\u00e3\u00e2\u00fc\u00df\u00f1\u00ed\u00e8\u00e2\u00fc\u00e0\u00e9\u00e6\u00e2\u00eb\u00f1"

    const/16 v1, 0x9d

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v0

    :try_start_5
    const-string v1, "\u0171\u017d\u017c\u017c\u0173\u0171\u0182"
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const/16 v2, 0x75

    const/16 v3, 0x99

    const/4 v4, 0x3

    :try_start_6
    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\ufd6f\ufd99\ufda2\ufd92\ufda1\ufd9c\ufd9c\ufda1\ufd95\ufd7a\ufd8e\ufd9b\ufd8e\ufd94\ufd92\ufd9f\ufd4d\ufd96\ufda0\ufd4d\ufd9b\ufda2\ufd99\ufd99\ufd59\ufd4d\ufd73\ufd6e\ufd76\ufd79\ufda0\ufd5b\ufd5b\ufd5b"
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    const/16 v3, 0xf1

    const/4 v4, 0x0

    :try_start_7
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v0, 0x57

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    :try_start_8
    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendMsgToHandler(Landroid/os/Handler;I)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_a
    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    :catch_3
    move-exception v0

    throw v0

    :cond_1
    :try_start_b
    iget-object v1, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBluetoothManager:Lcom/sec/android/safe/manager/BluetoothManager;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/sec/android/safe/manager/BluetoothManager;->connect(Landroid/bluetooth/BluetoothDevice;ZZZ)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public createSecureSession()V
    .locals 8

    const/4 v7, 0x7

    const/4 v3, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x0

    const-string v0, "&\u0014\u0019\u00182\u0015\'#\u001e\u00182\u0016\u001f\u001c\u0018!\'"

    const/16 v1, 0x2d

    invoke-static {v0, v1, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uffff\uffd4\uffd5\uffd1\uffd5\u0001\uffc4\u0007\u0016\t\u0005\u0018\t\ufff7\t\u0007\u0019\u0016\t\ufff7\t\u0017\u0017\r\u0013\u0012"

    const/16 v2, 0x5c

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u01da\u01fb\u0203\u01ac\u01ef\u01fe\u01f1\u01ed\u0200\u01f1\u01ac\u01df\u01f1\u01ef\u0201\u01fe\u01f1\u01ac\u01df\u01f1\u01ff\u01ff\u01f5\u01fb\u01fa\u01b8\u01ac"

    const/16 v3, 0xc6

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "\ufee2\ufed0\ufed5\ufed4\ufeee\ufed1\ufee3\ufedf\ufeda\ufed4\ufeee\ufed2\ufedb\ufed8\ufed4\ufedd\ufee3"

    const/16 v1, 0x7b

    invoke-static {v0, v1, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    const-string v1, "\uff85\uff94\uff87\uff83\uff96\uff87\uff75\uff87\uff85\uff97\uff94\uff87\uff75\uff87\uff95\uff95\uff8b\uff91\uff90"

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v3

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_0
    const/16 v2, 0x6f

    invoke-static {v1, v2, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "zwjj\u0099\u008c\u0088\u009b\u008cz\u008c\u008a\u009c\u0099\u008cz\u008c\u009a\u009a\u0090\u0096\u0095SG\u009at\u0096\u008b\u008cO\u008a\u0093\u0090\u008c\u0095\u009bG\u0088\u0093\u009e\u0088\u00a0\u009aGXPaGX"

    const/16 v3, 0x27

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    sget-object v1, Lcom/sec/android/safe/SecureStorageManagerClient;->mSamsungAccountId:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCCreateSecureSession(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v2

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v1, v2, :cond_1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v1, 0x41

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_1
    if-eqz v0, :cond_2

    const-string v0, "\u015c\u016b\u015e\u015a\u016d\u015e\u014c\u015e\u015c\u016e\u016b\u015e\u014c\u015e\u016c\u016c\u0162\u0168\u0167\u0133\u0119\u014c\u0149\u013c\u013c\u016b\u015e\u015a\u016d\u015e\u014c\u015e\u015c\u016e\u016b\u015e\u014c\u015e\u016c\u016c\u0162\u0168\u0167\u0119\u0162\u016c\u0119\u0147\u014e\u0145\u0145\u0119\u011a\u011a\u011a"

    const/16 v1, 0x53

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    const-string v0, "\ufe95\ufe83\ufe88\ufe87\ufea1\ufe84\ufe96\ufe92\ufe8d\ufe87\ufea1\ufe85\ufe8e\ufe8b\ufe87\ufe90\ufe96"

    const/16 v1, 0xdf

    invoke-static {v0, v1, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uff2b\uff3a\uff2d\uff29\uff3c\uff2d\uff1b\uff2d\uff2b\uff3d\uff3a\uff2d\uff1b\uff2d\uff3b\uff3b\uff31\uff37\uff36"

    const/16 v2, 0x68

    invoke-static {v1, v2, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u014e\u0160\u015e\u0170\u016d\u0160\u011b\u014e\u0160\u016e\u016e\u0164\u016a\u0169\u011b\u013e\u016d\u0160\u015c\u016f\u0160\u015f\u011b\u014e\u0170\u015e\u015e\u0160\u016e\u016e\u0161\u0170\u0167\u0167\u0174"

    const/16 v3, 0xa4

    const/16 v4, 0x57

    invoke-static {v2, v3, v4, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public deviceChanged(Ljava/lang/String;)V
    .locals 3

    :try_start_0
    sput-object p1, Lcom/sec/android/safe/SecureStorageManagerClient;->mPassword:Ljava/lang/String;

    const/4 v0, 0x3

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v1, :pswitch_data_0

    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :pswitch_0
    :try_start_2
    iput v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mCTX:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mPKEMode:I

    const/4 v0, 0x3

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->connect()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public getBTDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    .locals 5

    :try_start_0
    const-string v0, "\u00f4\u00e6\u00e1\u00e2\u00f8\u00e5\u00f3\u00f7\u00ec\u00e2\u00f8\u00e4\u00eb\u00ee\u00e2\u00e9\u00f3"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0xa7

    const/4 v2, 0x3

    :try_start_1
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u0090\u008e\u009dk}m\u008e\u009f\u0092\u008c\u008e"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v2, 0xb8

    const/16 v3, 0xe1

    const/4 v4, 0x2

    :try_start_2
    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    add-int/2addr v2, v3

    :try_start_3
    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b041104110411Б04110411()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result v3

    if-eq v2, v3, :cond_0

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v3

    add-int/2addr v3, v2

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v2, v3

    packed-switch v2, :pswitch_data_0

    const/16 v2, 0x55

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_0
    :try_start_4
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v2, v3

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v3

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v2, v3, :cond_0

    const/16 v2, 0x5a

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v2, 0x13

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    :try_start_5
    const-string v2, "\u00d6\u00d8\u00c1\u00d1\u00c0\u00db\u00db\u00c0\u00dc\u00fd\u00f0\u009a\u009a\u009a"

    const/16 v3, 0xb4

    const/4 v4, 0x3

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBTAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public getPeerIDByte()Z
    .locals 8

    const/4 v7, 0x2

    const/4 v0, 0x0

    const-string v1, "-\u001b \u001f9\u001c.*%\u001f9\u001d&#\u001f(."

    const/16 v2, 0x71

    :pswitch_0
    packed-switch v0, :pswitch_data_0

    :goto_0
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const/16 v3, 0x97

    invoke-static {v1, v2, v3, v0}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0133\u0131\u0140\u011c\u0131\u0131\u013e\u0115\u0110\u010e\u0145\u0140\u0131"

    const/16 v3, 0x66

    invoke-static {v2, v3, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u029e\u0292\u029c\u0296\u0251\u0281\u0296\u0296\u02a3\u027a\u0275\u0251\u0284\u02a5\u02a3\u029a\u029f\u0298\u0251\u02a5\u02a0\u0251\u0273\u02aa\u02a5\u0296"

    const/16 v4, 0xbb

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v5, v6

    packed-switch v5, :pswitch_data_2

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v5

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v5

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_2
    const/4 v5, 0x6

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    sput-object v1, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerIDByte:[B

    sget-object v1, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerIDByte:[B

    if-nez v1, :cond_1

    const-string v1, "\u0179\u0167\u016c\u016b\u0185\u0168\u017a\u0176\u0171\u016b\u0185\u0169\u0172\u016f\u016b\u0174\u017a"

    const/16 v2, 0x93

    invoke-static {v1, v2, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0123\u0121\u0130\u010c\u0121\u0121\u012e\u0105\u0100\u00fe\u0135\u0130\u0121\u00e4\u00e5"

    const/16 v3, 0x5e

    invoke-static {v2, v3, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "K.CCP\'\" WRC\ufffeGQ\ufffeLSJJ\u000c\u000c\u000c"

    const/16 v4, 0x11

    const/4 v5, 0x7

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v5

    add-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v4, v5, :cond_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v4

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v5

    add-int/2addr v5, v4

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v4, v5

    packed-switch v4, :pswitch_data_3

    const/16 v4, 0x25

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v4, 0x2d

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_3
    const/16 v4, 0xc

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v4

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method

.method public initPasswd(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    :try_start_0
    sput-object p1, Lcom/sec/android/safe/SecureStorageManagerClient;->mPassword:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mCTX:I

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v1, v2, :cond_0

    const/16 v1, 0x8

    :try_start_1
    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v1, 0x4a

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    :try_start_2
    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v1

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-eq v0, v1, :cond_1

    :pswitch_0
    packed-switch v4, :pswitch_data_0

    :goto_0
    packed-switch v4, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xb

    :try_start_3
    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_1
    const/4 v0, 0x0

    :try_start_4
    iput v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mPKEMode:I

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    :pswitch_2
    packed-switch v3, :pswitch_data_2

    :goto_1
    packed-switch v3, :pswitch_data_3

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->connect()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public initValues(Ljava/lang/String;Landroid/os/Handler;ILjava/lang/String;Ljava/lang/String;)Z
    .locals 8

    const/4 v5, 0x7

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    sput-object p1, Lcom/sec/android/safe/SecureStorageManagerClient;->mSamsungAccountId:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    sput p3, Lcom/sec/android/safe/SecureStorageManagerClient;->mLoginCount:I

    sput-object p4, Lcom/sec/android/safe/SecureStorageManagerClient;->mBtAddr:Ljava/lang/String;

    sput-object p5, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/safe/SecureStorageManagerClient;->mSamsungAccountId:Ljava/lang/String;

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v3, v4

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b041104110411Б04110411()I

    move-result v4

    if-eq v3, v4, :cond_0

    const/16 v3, 0x42

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    if-nez v2, :cond_2

    const-string v2, "\uff54\uff42\uff47\uff46\uff60\uff43\uff55\uff51\uff4c\uff46\uff60\uff44\uff4d\uff4a\uff46\uff4f\uff55"

    const/16 v3, 0x19

    const/16 v4, 0xe6

    invoke-static {v2, v3, v4, v1}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u02b5\u02ba\u02b5\u02c0\u02a2\u02ad\u02b8\u02c1\u02b1\u02bf"

    const/16 v3, 0xc4

    const/4 v4, 0x6

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "{ao{\u0081\u0083|uOqq}\u0083|\u0082Wr.w\u0081.|\u0083zz..///"

    const/16 v4, 0x73

    const/16 v5, 0x81

    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_1
    sget-object v2, Lcom/sec/android/safe/SecureStorageManagerClient;->mBtAddr:Ljava/lang/String;

    if-nez v2, :cond_5

    const-string v2, "?-21K.@<71K/851:@"

    const/16 v3, 0xd

    invoke-static {v2, v3, v5, v1}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\uff2a\uff2f\uff2a\uff35\uff17\uff22\uff2d\uff36\uff26\uff34"

    const/16 v4, 0xbb

    const/16 v5, 0x84

    invoke-static {v3, v4, v5, v1}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\u009a\u008a\u0081\u00f6\u001f\u00a9\u00ac^\u00ef\u0013\u008b\u00b6t\u001a\u00a7\u00eb\u00ecS\u000cA\u00f4\u00dd\u00cc\u0081\u00c6\u00a5\u00e3\u0012"

    const/16 v5, 0xb5

    invoke-static {v4, v5, v1}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    :pswitch_0
    packed-switch v1, :pswitch_data_0

    :goto_1
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    invoke-static {v2, v3, v4}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    if-nez v2, :cond_1

    const-string v1, "\u0019\u0007\u000c\u000b%\u0008\u001a\u0016\u0011\u000b%\t\u0012\u000f\u000b\u0014\u001a"

    const/16 v2, 0xf4

    const/16 v3, 0xba

    invoke-static {v1, v2, v3, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0089\u008e\u0089\u0094v\u0081\u008c\u0095\u0085\u0093"

    const/16 v3, 0x10

    invoke-static {v2, v3, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u00e2\u00c7\u00ee\u00e1\u00eb\u00e3\u00ea\u00fd\u00dc\u00ea\u00ec\u00fa\u00fd\u00ea\u00dc\u00fb\u00e0\u00fd\u00ee\u00e8\u00ea\u00af\u00e6\u00fc\u00af\u00e1\u00fa\u00e3\u00e3\u00af\u00af\u00ae\u00ae\u00ae"

    const/16 v4, 0x8f

    const/4 v5, 0x3

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v6, v7

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v6, v7

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v6, v7

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v6, v7, :cond_3

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v6

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v6, 0x4a

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_3
    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->getPeerIDByte()Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "\uffb1\uff9f\uffa4\uffa3\uffbd\uffa0\uffb2\uffae\uffa9\uffa3\uffbd\uffa1\uffaa\uffa7\uffa3\uffac\uffb2"

    const/16 v3, 0x51

    invoke-static {v2, v3, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_2

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v1, 0x22

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_2
    const-string v1, "\ufe2c\ufe31\ufe2c\ufe37\ufe19\ufe24\ufe2f\ufe38\ufe28\ufe36"

    const/16 v3, 0xbf

    invoke-static {v1, v3, v0}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v3, "\u0017,,9\u0010\u000b\t@;,\uffe70:\uffe75<33\uffe7\uffe7\uffe8\uffe8\uffe8"

    const/16 v4, 0x9e

    const/16 v5, 0xd7

    invoke-static {v3, v4, v5, v0}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    sget-object v2, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    if-nez v2, :cond_4

    const-string v1, "\uffca\uffb8\uffbd\uffbc\uffd6\uffb9\uffcb\uffc7\uffc2\uffbc\uffd6\uffba\uffc3\uffc0\uffbc\uffc5\uffcb"

    const/16 v2, 0x90

    invoke-static {v1, v5, v2, v0}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\uffb1\uffb6\uffb1\uffbc\uff9e\uffa9\uffb4\uffbd\uffad\uffbb"

    const/16 v3, 0xb8

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "}`uu\u0082Yt0y\u00830~\u0085||00111"

    const/16 v4, 0x8

    invoke-static {v3, v4, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method public onMessageRead(Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v2, 0x2

    :try_start_0
    new-instance v0, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;

    invoke-direct {v0}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->parse(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->getResponseType()I

    move-result v1

    if-ne v1, v2, :cond_4

    const-string v1, "\uff99\uff87\uff8c\uff8b\uffa5\uff88\uff9a\uff96\uff91\uff8b\uffa5\uff89\uff92\uff8f\uff8b\uff94\uff9a"

    const/16 v2, 0x5d

    const/4 v3, 0x7

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u022e\u022d\u020c\u0224\u0232\u0232\u0220\u0226\u0224\u0211\u0224\u0220\u0223"

    const/16 v3, 0xfa

    const/16 v4, 0xc5

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/safe/bluetoothresponse/ResponseHandler;->getErrorLog()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendMsgToHandler(Landroid/os/Handler;I)V

    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->stopBT()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "\u0096\u0084\u0083\u0080\u009a\u0087\u0091\u0095\u008e\u0080\u009a\u0086\u0089\u008c\u0080\u008b\u0091"

    const/16 v1, 0xc5

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u0090\u0091\u00b2\u009a\u008c\u008c\u009e\u0098\u009a\u00ad\u009a\u009e\u009b"

    const/16 v2, 0xff

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u00b1\u00ac\u00a8\u00a2\u00ab]\u00ab\u00ac\u00b1]\u00a2\u00b5\u00a6\u00b0\u00b1i]\u00a4\u00ac]\u00b1\u00ac]\u00b0\u00a2\u00ab\u00a1\u0091\u00ac\u00a8\u00a2\u00ab\u008f\u00a2\u00ae\u0091\u00ac\u0090\u00a2\u00af\u00b3\u00a2\u00afef"

    const/16 v3, 0x73

    const/16 v4, 0xb0

    const/4 v5, 0x2

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "\u00ef\u00f6"

    const/16 v1, 0x16

    const/16 v2, 0xd5

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendTokenReqToServer(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :cond_2
    :try_start_1
    const-string v2, "\u00d4\u00d6"

    const/16 v3, 0xe4

    const/4 v4, 0x3

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->parseHelloFromServer([B)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "lo"

    const/16 v1, 0x3c

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    :try_start_2
    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendAckToServer(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    throw v0

    :cond_3
    :try_start_3
    const-string v2, "\uffef\ufff7"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const/16 v3, 0x92

    const/16 v4, 0x51

    const/4 v5, 0x2

    :try_start_4
    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->storeToken([B)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v0, "\u008d\u008ck\u0083\u0091\u0091\u007f\u0085\u0083p\u0083\u007f\u0082X>qq}ajgclr}kqe}gkcg}_ai}dpmk}qcptcpJ>\u0081\u007f\u0091\u0083>\u0082\u0083\u0084\u007f\u0093\u008a\u0092>\u0083\u0090\u0090\u008d\u0090J>d_gj\u0091LLL"

    const/16 v1, 0x1e

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :cond_4
    :try_start_5
    const-string v0, "\u00d7\u00c5\u00ca\u00c9\u00e3\u00c6\u00d8\u00d4\u00cf\u00c9\u00e3\u00c7\u00d0\u00cd\u00c9\u00d2\u00d8"

    const/16 v1, 0x84

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u00a4\u00a3\u0082\u009a\u00a8\u00a8\u0096\u009c\u009a\u0087\u009a\u0096\u0099"

    const/16 v2, 0x27

    const/16 v3, 0x5c

    const/4 v4, 0x2

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\uffe8\uffdb\uffd7\uffda\uff96\uffda\uffd7\uffea\uffd7\uffa4\uffa4\uffa4"

    const/16 v3, 0x8a

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    const-string v2, "\u00ec\u009d"

    const/16 v3, 0x7b

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    move-result v2

    if-eqz v2, :cond_2

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b041104110411Б04110411()I

    move-result v1

    if-eq v0, v1, :cond_5

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v0, 0x2c

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_5
    :try_start_6
    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    packed-switch v0, :pswitch_data_1

    const-string v0, "\uff91\uff92"

    const/16 v1, 0x35

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendHelloToServer(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_1
    const-string v0, "\u00e0\u00ce\u00d3\u00d2\u00ec\u00cf\u00e1\u00dd\u00d8\u00d2\u00ec\u00d0\u00d9\u00d6\u00d2\u00db\u00e1"

    const/16 v1, 0x2f

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-,\u000b#11\u001f%#\u0010#\u001f\""

    const/16 v2, 0x46

    const/4 v3, 0x4

    const/4 v4, 0x2

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0082\u0083\u008b4w|yw\u007f4}z4\u0088\u0083\u007fy\u00824y\u008c}\u0087\u0088"

    const/16 v3, 0x14

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    sget-object v1, Lcom/sec/android/safe/SecureStorageManagerClient;->mSamsungAccountId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCIsTokenExist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "\u01a0\u01a5"

    const/16 v1, 0xb8

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendEncPWToServer(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_2
    const-string v0, "\u00e8\u00f1"

    const/16 v1, 0xd3

    const/16 v2, 0x1b

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendTokenCheckReqToServer(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    :cond_6
    :try_start_7
    const-string v2, "\uffd9\uffdd"

    const/16 v3, 0x57

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    move-result-object v2

    :try_start_8
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->parseAckFromServer([B)Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    :try_start_9
    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    packed-switch v0, :pswitch_data_2

    const-string v0, "\u0249\u0248\u0227\u023f\u024d\u024d\u023b\u0241\u023f\u022c\u023f\u023b\u023e\u0214\u01fa\u022d\u022d\u0239\u021d\u0226\u0223\u021f\u0228\u022e\u0239\u0227\u022d\u0221\u0239\u021b\u021d\u0225\u0239\u0220\u022c\u0229\u0227\u0239\u022d\u021f\u022c\u0230\u021f\u022c\u0206\u01fa\u023d\u023b\u024d\u023f\u01fa\u023e\u023f\u0240\u023b\u024f\u0246\u024e\u01fa\u023f\u024c\u024c\u0249\u024c\u0206\u01fa\u0220\u021b\u0223\u0226\u024d\u0208\u0208\u0208"

    const/16 v1, 0xed

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v2, "\u009f\u00a0"

    const/16 v3, 0x6e

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    move-result-object v2

    :try_start_a
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    move-result v0

    if-eqz v0, :cond_9

    :pswitch_3
    packed-switch v6, :pswitch_data_3

    :goto_1
    const/4 v0, 0x1

    packed-switch v0, :pswitch_data_4

    goto :goto_1

    :pswitch_4
    :try_start_b
    invoke-virtual {p0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->parseEncAckFromServer([B)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->completeProcess()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v1

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v0, v1, :cond_0

    const/16 v0, 0x3a

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v0, 0x3c

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    goto/16 :goto_0

    :cond_8
    :try_start_c
    const-string v2, "\u00ff\u0105"

    const/16 v3, 0xf1

    const/16 v4, 0x22

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->parseEncAckFromServer([B)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1

    packed-switch v0, :pswitch_data_5

    :pswitch_5
    :try_start_d
    const-string v0, "\u0003\u0002\uffe1\ufff9\u0007\u0007\ufff5\ufffb\ufff9\uffe6\ufff9\ufff5\ufff8\uffce\uffb4\uffe7\uffe7\ufff3\uffd7\uffe0\uffdd\uffd9\uffe2\uffe8\ufff3\uffe1\uffe7\uffdb\ufff3\uffd9\uffe2\uffd7\uffe6\uffed\uffe4\uffe8\uffdd\uffe3\uffe2\ufff3\uffd5\uffd7\uffdf\ufff3\uffda\uffe6\uffe3\uffe1\ufff3\uffe7\uffd9\uffe6\uffea\uffd9\uffe6\uffc0\uffb4\ufff7\ufff5\u0007\ufff9\uffb4\ufff8\ufff9\ufffa\ufff5\t\u0000\u0008\uffb4\ufff9\u0006\u0006\u0003\u0006\uffc0\uffb4\uffda\uffd5\uffdd\uffe0\u0007\uffc2\uffc2\uffc2"

    const/16 v1, 0x43

    const/16 v2, 0xaf

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_6
    const-string v0, "\ufe8d\ufe94"

    const/16 v1, 0xbc

    const/16 v2, 0xe7

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0

    move-result-object v0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v2

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v2

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_6

    const/16 v1, 0x5b

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_7
    :try_start_e
    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendTokenReqToServer(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_8
    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->completeProcess()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0

    goto/16 :goto_0

    :cond_9
    :try_start_f
    const-string v0, "\u019f\u019e\u017d\u0195\u01a3\u01a3\u0191\u0197\u0195\u0182\u0195\u0191\u0194\u016a\u0150\u0179\u019e\u01a6\u0191\u019c\u0199\u0194\u0150\u0183\u0183\u0150\u0194\u0191\u01a4\u0191\u015c\u0150\u0176\u0171\u0179\u017c\u01a3\u015e\u015e\u015e"
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_7

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v1, 0x45

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_9
    const/16 v1, 0xbd

    const/16 v2, 0x73

    const/4 v3, 0x3

    :try_start_10
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_a
    const-string v0, "\u0175\u0179"

    const/16 v1, 0x6c

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendEncPWToServer(Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_1

    goto/16 :goto_0

    :pswitch_b
    :try_start_11
    const-string v0, "\u0155\u0157"

    const/16 v1, 0xd5

    const/16 v2, 0x4f

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendHelloToServer(Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_0

    goto/16 :goto_0

    :pswitch_c
    :try_start_12
    const-string v0, "y~"
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_1

    const/16 v1, 0x49

    const/4 v2, 0x5

    :try_start_13
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendEncPWToServer(Ljava/lang/String;)V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_0

    goto/16 :goto_0

    :pswitch_d
    :try_start_14
    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->completeProcess()V

    goto/16 :goto_0

    :pswitch_e
    const-string v0, "\u00eb\u00ee"

    const/16 v1, 0xdb

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendEncPWToServer(Ljava/lang/String;)V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_d
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_b
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_c
        :pswitch_1
        :pswitch_2
        :pswitch_a
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_6
        :pswitch_8
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_7
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x0
        :pswitch_9
    .end packed-switch
.end method

.method public onMessageWrite(Ljava/lang/String;)V
    .locals 1

    return-void
.end method

.method public onStateConnected()V
    .locals 9

    const/16 v8, 0xb6

    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v0, "\u0131\u011f\u0124\u0123\u013d\u0120\u0132\u012e\u0129\u0123\u013d\u0121\u012a\u0127\u0123\u012c\u0132"

    const/16 v1, 0xde

    invoke-static {v0, v1, v7}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u01db\u01da\u01bf\u01e0\u01cd\u01e0\u01d1\u01af\u01db\u01da\u01da\u01d1\u01cf\u01e0\u01d1\u01d0"

    const/4 v2, 0x2

    invoke-static {v1, v8, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\ufedf\uff00\uff11\uff04\ufefe\uff00\ufebb\ufefe\uff0a\uff09\uff09\uff00\ufefe\uff0f\uff00\ufeff"

    const/16 v3, 0x77

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->createSecureSession()V

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "HG,M:M>\u001cHGG><M>=\u0013\ufff9<:L>\ufff9=>?:NEM\ufff9>KKHK\u0005\ufff9\u001f\u001a\"%L\u0007\u0007\u0007"

    const/16 v1, 0x2c

    invoke-static {v0, v7, v1, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    :goto_0
    :pswitch_0
    packed-switch v4, :pswitch_data_1

    :goto_1
    packed-switch v5, :pswitch_data_2

    goto :goto_1

    :pswitch_1
    return-void

    :pswitch_2
    const-string v0, "\u015f\u015f"

    const/16 v1, 0x7d

    const/16 v2, 0xb2

    invoke-static {v0, v1, v2, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendReqInitialSetup(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v2

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_3

    const/16 v1, 0x34

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_3
    packed-switch v0, :pswitch_data_4

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    goto :goto_0

    :pswitch_4
    const-string v0, "\u017f\u017f"

    const/16 v1, 0x98

    invoke-static {v0, v1, v8, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendHelloToServer(Ljava/lang/String;)V

    :pswitch_5
    packed-switch v4, :pswitch_data_5

    :goto_2
    packed-switch v5, :pswitch_data_6

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method

.method public onStateDisconnected()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    :try_start_0
    const-string v0, "\u013f\u012d\u0132\u0131\u014b\u012e\u0140\u013c\u0137\u0131\u014b\u012f\u0138\u0135\u0131\u013a\u0140"

    const/16 v1, 0xa4

    const/16 v2, 0x48

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    :try_start_1
    const-string v1, "\u00a8\u00a7\u008c\u00ad\u009a\u00ad\u009e}\u00a2\u00ac\u009c\u00a8\u00a7\u00a7\u009e\u009c\u00ad\u009e\u009d"

    const/16 v2, 0xef

    const/16 v3, 0xb6

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v5, v4

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v4, v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    packed-switch v4, :pswitch_data_0

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v4, v5

    :pswitch_0
    packed-switch v6, :pswitch_data_1

    :goto_0
    packed-switch v7, :pswitch_data_2

    goto :goto_0

    :pswitch_1
    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v4, v5

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b041104110411Б04110411()I

    move-result v5

    if-eq v4, v5, :cond_1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v4

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/4 v4, 0x6

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_1
    :try_start_2
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v4

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v4

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :pswitch_2
    const/4 v4, 0x0

    :try_start_3
    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u018f\u01b0\u01c1\u01b4\u01ae\u01b0\u016b\u018f\u01b4\u01be\u01ae\u01ba\u01b9\u01b9\u01b0\u01ae\u01bf\u01b0\u01af\u0179\u0179\u0179"

    const/16 v3, 0x81

    const/16 v4, 0xca

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v2

    :pswitch_3
    packed-switch v7, :pswitch_data_3

    :goto_1
    packed-switch v6, :pswitch_data_4

    goto :goto_1

    :pswitch_4
    :try_start_4
    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    return-void

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public onStateFailToConnect(Ljava/lang/String;)V
    .locals 6

    :try_start_0
    const-string v0, "\u0083qvu\u008fr\u0084\u0080{u\u008fs|yu~\u0084"

    const/16 v1, 0x30

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u00f1\u00f0\u00cd\u00ea\u00ff\u00ea\u00fb\u00d8\u00ff\u00f7\u00f2\u00ca\u00f1\u00dd\u00f1\u00f0\u00f0\u00fb\u00fd\u00ea"

    const/16 v2, 0x9e

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v3, 0x26

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_0
    :try_start_1
    const-string v3, "\u00db\u00fc\u010d\u0100\u00fa\u00fc\u00b7\u00dd\u00f8\u0100\u0103\u00b7\u010b\u0106\u00b7\u00da\u0106\u0105\u0105\u00fc\u00fa\u010b\u00c5\u00c5\u00c5\u00c3\u00b7\u0104\u00e0\u0105\u0100\u010b\u00e4\u0106\u00fb\u00fc\u00d1\u00b7"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/16 v4, 0x97

    const/4 v5, 0x5

    :try_start_2
    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :try_start_3
    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    if-eq v0, v1, :cond_1

    :pswitch_1
    const/4 v0, 0x1

    packed-switch v0, :pswitch_data_1

    :goto_0
    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    :try_start_4
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :try_start_5
    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v1

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v1

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    if-eq v0, v1, :cond_0

    :try_start_6
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v0, 0x13

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    :cond_1
    :try_start_7
    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendMsgToHandler(Landroid/os/Handler;I)V

    const/4 v0, -0x1

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    return-void

    :catch_0
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    :catch_3
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public parseAckFromServer([B)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v0, 0x0

    :try_start_0
    const-string v2, "\u00db\u00c9\u00ce\u00cd\u00e7\u00ca\u00dc\u00d8\u00d3\u00cd\u00e7\u00cb\u00d4\u00d1\u00cd\u00d6\u00dc"

    const/16 v3, 0x44

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\uff14\ufee9\ufeee\uff16\ufed9\ufee1\ufee9\ufeee\ufef3\uff2c\uff1e\uff2d\uff2e\uff29\ufee5\uff1c\uff21\uff1a\uff27\uff20\uff1e\ufee2\ufed9\uff29\uff1a\uff2b\uff2c\uff1e\ufefa\uff1c\uff24\ufeff\uff2b\uff28\uff26\uff0c\uff1e\uff2b\uff2f\uff1e\uff2b"

    const/16 v4, 0x6d

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\uff30\uff42\uff4f\uff53\uff42\uff4f\ufefd\uff1e\uff40\uff48\ufefd\uff2f\uff42\uff40\uff46\uff42\uff53\uff42\uff41"

    const/16 v5, 0xa9

    const/16 v6, 0x7a

    const/4 v7, 0x1

    invoke-static {v4, v5, v6, v7}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :pswitch_0
    packed-switch v0, :pswitch_data_0

    :goto_0
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->mLoginCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :pswitch_2
    packed-switch v1, :pswitch_data_2

    :goto_1
    packed-switch v0, :pswitch_data_3

    goto :goto_1

    :pswitch_3
    :try_start_1
    array-length v4, p1

    sget-object v5, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    invoke-virtual {v2, v3, p1, v4, v5}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCParseClientAck(I[BILjava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "Gz\u00f9\u0083g\u0098h\u00034\u00af\u00fd\u009c(F\n\u00eb7\u0095yR\u001b^E\u00e2I(\u00c2\u00be%\u0013B\u00b7\u00d8\u0014\u00a8\u00de\u00a4\u00e6\u0010l\u00e1p\u00a2_s\u0016\u0083\u00f6\u00fb"

    const/16 v2, 0x1b

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    return v0

    :cond_0
    :try_start_2
    const-string v0, "0\u001e#\"<\u001f1-(\"< )&\"+1"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const/16 v2, 0xd8

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_4

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v3, 0x29

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_4
    const/16 v3, 0xfb

    const/4 v4, 0x0

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v5, v6, :cond_1

    const/16 v5, 0x3e

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v5

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_1
    :try_start_3
    invoke-static {v0, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\u0117\u0108\u0119\u011a\u010c\u00e8\u010a\u0112\u00ed\u0119\u0116\u0114\u00fa\u010c\u0119\u011d\u010c\u0119"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const/16 v3, 0x2e

    const/16 v4, 0xd5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v5, v6, :cond_2

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v5

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v5

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_2
    const/4 v5, 0x2

    :try_start_4
    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u01ac\u01be\u01cb\u01cf\u01be\u01cb\u0179\u019a\u01bc\u01c4\u0179\u01c2\u01cc\u0179\u01cf\u01ba\u01c5\u01c2\u01bd\u01ba\u01cd\u01be\u01bd\u0185\u0179\u01c7\u01c8\u01d0\u0179\u01cc\u01be\u01c7\u01bd\u0179\u019a\u019c\u01a4\u0179\u01cd\u01c8\u0179\u01ac\u01be\u01cb\u01cf\u01be\u01cb"

    const/16 v4, 0x73

    const/4 v5, 0x6

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v3

    :try_start_5
    invoke-static {v0, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move v0, v1

    goto :goto_2

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public parseEncAckFromServer([B)Z
    .locals 9

    const/16 v8, 0x7b

    const/4 v6, 0x6

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, "\ufeb8\ufea6\ufeab\ufeaa\ufec4\ufea7\ufeb9\ufeb5\ufeb0\ufeaa\ufec4\ufea8\ufeb1\ufeae\ufeaa\ufeb3\ufeb9"

    const/16 v3, 0x89

    invoke-static {v2, v3, v0}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u00d1\u00a6\u00ad\u00a5\u00a6\u00af\u00d3\u0096\u009e\u00a6\u00ad\u00b0\u00d9\u00de\u00d7\u00e4\u00dd\u00db\u00a2\u00e8\u00db\u00d9\u00e5\u00ec\u00db\u00e8\u00ef\u00bc\u00df\u00e4\u00d7\u00e2\u00a5\u00a6\u00af\u00b0\u00d9\u00de\u00d7\u00e4\u00dd\u00db\u0096\u00ed\u00df\u00ea\u00de\u00e5\u00eb\u00ea\u0096\u00ca\u00e5\u00e1\u00db\u00e4\u0096\u00bc\u00df\u00e4\u00d7\u00e2\u009f\u0096\u00e6\u00d7\u00e8\u00e9\u00db\u00bb\u00e4\u00d9\u00b7\u00d9\u00e1\u00bc\u00e8\u00e5\u00e3\u00c9\u00db\u00e8\u00ec\u00db\u00e8"

    const/16 v4, 0x70

    const/16 v5, 0xe6

    invoke-static {v3, v4, v5, v7}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\u01b6\u01df\u01d4\u01e3\u01ea\u01e1\u01e5\u01d6\u01d5\u0191\u01c1\u01d2\u01e4\u01e4\u01e8\u01e0\u01e3\u01d5\u0191\u01d2\u01d4\u01dc\u01d6\u01d5"

    :pswitch_0
    packed-switch v0, :pswitch_data_0

    :goto_0
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v4, v8, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    array-length v3, p1

    sget-object v4, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    invoke-virtual {v2, p1, v3, v4}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCParseAckMsg([BILjava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "\u0082s\u0084\u0085wW\u0080uSu}X\u0084\u0081\u007few\u0084\u0088w\u0084L2ebUbs\u0084\u0085wSu}_\u0085y2{\u00852`g^^2333"

    const/16 v3, 0x5c

    const/16 v4, 0x6e

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v5, v6

    packed-switch v5, :pswitch_data_2

    const/16 v5, 0x34

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v5

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_2
    packed-switch v1, :pswitch_data_3

    :goto_1
    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v5, v6

    packed-switch v5, :pswitch_data_4

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v5, v6

    packed-switch v5, :pswitch_data_5

    const/16 v5, 0x24

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v5, 0x5f

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_3
    const/16 v5, 0x40

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v5, 0x5b

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_4
    packed-switch v0, :pswitch_data_6

    goto :goto_1

    :pswitch_5
    invoke-static {v2, v3, v4, v7}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    :goto_2
    return v0

    :cond_0
    const-string v2, "\u022a\u0218\u021d\u021c\u0236\u0219\u022b\u0227\u0222\u021c\u0236\u021a\u0223\u0220\u021c\u0225\u022b"

    const/16 v3, 0x9d

    invoke-static {v2, v3, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\ufee7\ufed8\ufee9\ufeea\ufedc\ufebc\ufee5\ufeda\ufeb8\ufeda\ufee2\ufebd\ufee9\ufee6\ufee4\ufeca\ufedc\ufee9\ufeed\ufedc\ufee9"

    const/16 v4, 0x83

    invoke-static {v3, v4, v0}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v3, "\u00c4\u00d6\u00e3\u00e7\u00d6\u00e3\u0091\u00b2\u00d4\u00dc\u0091\u00da\u00e4\u0091\u00e7\u00d2\u00dd\u00da\u00d5\u00d2\u00e5\u00d6\u00d5"

    const/16 v4, 0xec

    invoke-static {v3, v8, v4, v7}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method

.method public parseHelloFromServer([B)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v0, 0x0

    :try_start_0
    const-string v2, "\u0226\u0214\u0219\u0218\u0232\u0215\u0227\u0223\u021e\u0218\u0232\u0216\u021f\u021c\u0218\u0221\u0227"

    const/16 v3, 0xdc

    const/16 v4, 0xf7

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\ufec6\ufe9b\ufe9e\ufec8\ufe8b\ufedb\ufecc\ufedd\ufede\ufed0\ufeb3\ufed0\ufed7\ufed7\ufeda\ufeb1\ufedd\ufeda\ufed8\ufebe\ufed0\ufedd\ufee1\ufed0\ufedd"

    const/16 v4, 0x87

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\uff72\uff84\uff91\uff95\uff84\uff91\uff3f\uff67\uff84\uff8b\uff8b\uff8e\uff3f\uff88\uff92\uff3f\uff91\uff84\uff82\uff84\uff88\uff95\uff84\uff83"

    const/16 v5, 0x64

    const/16 v6, 0x7d

    const/4 v7, 0x1

    invoke-static {v4, v5, v6, v7}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->mLoginCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v4

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    add-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v5, v6

    packed-switch v5, :pswitch_data_0

    const/16 v5, 0x49

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v5, 0x56

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_0
    :try_start_2
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v5

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-eq v4, v5, :cond_0

    const/16 v4, 0x22

    :try_start_3
    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v4

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_0
    :try_start_4
    array-length v4, p1

    sget-object v5, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    invoke-virtual {v2, v3, p1, v4, v5}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCParseClientHello(I[BILjava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "\u00d6\u00c7\u00d8\u00d9\u00cb\u00ae\u00cb\u00d2\u00d2\u00d5\u00ac\u00d8\u00d5\u00d3\u00b9\u00cb\u00d8\u00dc\u00cb\u00d8\u00a0\u0086\u00b9\u00b6\u00a9\u00b6\u00c7\u00d8\u00d9\u00cb\u00a9\u00d2\u00cf\u00cb\u00d4\u00da\u00ae\u00cb\u00d2\u00d2\u00d5\u0086\u00cf\u00d9\u0086\u00b4\u00bb\u00b2\u00b2\u0086\u0087\u0087\u0087"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    const/16 v2, 0xda

    const/16 v3, 0x74

    const/4 v4, 0x0

    :try_start_5
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v5

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v5, v6
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    packed-switch v5, :pswitch_data_1

    const/16 v5, 0x46

    :try_start_6
    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v5

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :pswitch_1
    :try_start_7
    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_1
    const-string v0, "\uff29\uff17\uff1c\uff1b\uff35\uff18\uff2a\uff26\uff21\uff1b\uff35\uff19\uff22\uff1f\uff1b\uff24\uff2a"

    const/16 v2, 0x95

    const/4 v3, 0x7

    invoke-static {v0, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v2, "ufwxjMjqqtKwtrXjw{jw"

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cUBFUB\u0010XU\\\\_\u0010YC\u0010FQ\\YTQDUT\u001c\u0010^_G\u0010CU^T\u0010qs{\u0010D_\u0010cUBFUB"

    const/16 v4, 0x30

    const/4 v5, 0x3

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    :catch_3
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public recoveryPasswd()V
    .locals 3

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b041104110411Б04110411()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v0, 0x18

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    const/4 v0, 0x3

    :try_start_0
    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :pswitch_0
    :try_start_1
    iput v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mCTX:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    const/4 v0, 0x0

    :try_start_2
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v2

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    const/16 v1, 0x27

    :try_start_3
    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :pswitch_1
    :try_start_4
    iput v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mPKEMode:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    const/4 v0, 0x2

    :try_start_5
    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->connect()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    return-void

    :catch_0
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_7
    throw v0

    :catch_3
    move-exception v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    :catch_4
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public sendAckToServer(Ljava/lang/String;)V
    .locals 7

    const/4 v3, 0x0

    const/4 v6, 0x1

    :try_start_0
    const-string v0, "\uff7b\uff69\uff6e\uff6d\uff87\uff6a\uff7c\uff78\uff73\uff6d\uff87\uff6b\uff74\uff71\uff6d\uff76\uff7c"

    const/16 v1, 0x48

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    :pswitch_0
    packed-switch v3, :pswitch_data_0

    :goto_0
    packed-switch v6, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const-string v1, "1\u0006\n3\ufff6I;D:\u00179A*E);HL;H"

    const/16 v2, 0xfc

    const/16 v3, 0xd2

    const/4 v4, 0x2

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u033e\u033b\u032e\u0332\u0350\u0359\u0350\u035d\u034c\u035f\u035a\u035d\u033e\u0350\u035d\u0361\u0350\u035d\u032c\u034e\u0356"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v3, 0xf9

    const/4 v4, 0x6

    :try_start_1
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->mLoginCount:I

    sget-object v2, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCGeneratorServerAck(ILjava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "\u020b\u01fd\u0206\u01fc\u01d9\u01fb\u0203\u01ec\u0207\u01eb\u01fd\u020a\u020e\u01fd\u020a\u01d2\u01b8\u01eb\u01e8\u01db\u01df\u01fd\u0206\u01fd\u020a\u01f9\u020c\u0207\u020a\u01eb\u01fd\u020a\u020e\u01fd\u020a\u01d9\u01fb\u0203\u01b8\u0201\u020b\u01b8\u01e6\u01ed\u01e4\u01e4\u01b8\u01b9\u01b9\u01b9"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    const/16 v1, 0x88

    const/4 v2, 0x6

    :try_start_2
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const-string v1, "\uffbe\uffac\uffb1\uffb0\uffca\uffad\uffbf\uffbb\uffb6\uffb0\uffca\uffae\uffb7\uffb4\uffb0\uffb9\uffbf"

    const/16 v2, 0x6c

    const/16 v3, 0x29

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\uffb0\uffa2\uffab\uffa1\uff7e\uffa0\uffa8\uff91\uffac\uff90\uffa2\uffaf\uffb3\uffa2\uffaf"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v4, v3

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v3, v4

    packed-switch v3, :pswitch_data_2

    const/4 v3, 0x7

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v4

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v5, v4

    mul-int/2addr v4, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v4, v5

    packed-switch v4, :pswitch_data_3

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v4

    sput v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_2
    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_3
    const/16 v3, 0xc3

    const/4 v4, 0x4

    :try_start_3
    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v6

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v5, v6
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    packed-switch v5, :pswitch_data_4

    const/4 v5, 0x2

    :try_start_4
    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v5, 0x3b

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :pswitch_4
    :try_start_5
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u0110\u0122\u012b\u0121\u0126\u012b\u0124\u00dd\u00f7\u00dd\u0110\u0122\u012f\u0133\u0122\u012f\u00dd\u00fe\u0120\u0128\u00dd\u012a\u0122\u0130\u0130\u011e\u0124\u0122"
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const/16 v4, 0x3f

    const/4 v5, 0x6

    :try_start_6
    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    :try_start_7
    invoke-virtual {p0, v0, p1}, Lcom/sec/android/safe/SecureStorageManagerClient;->SendJsonMsg([BLjava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    :catch_3
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public sendEncPWToServer(Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    :try_start_0
    const-string v0, "\u0099\u008b\u008c\u008f\u0095\u0088\u009e\u009a\u0081\u008f\u0095\u0089\u0086\u0083\u008f\u0084\u009e"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0xca

    const/4 v2, 0x3

    :try_start_1
    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v3, v4, :cond_0

    const/16 v3, 0x27

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :try_start_2
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u00ac\u00c7\u00c1\u00d8\u00c7\u00cf\u00aa\u00d7\u00df\u00c7\u00c1\u00d8\u00c7\u00cf\u00cd\u0094\u009f\u0096\u0099\u0090\u0092\u00d7\u0080\u009e\u0083\u009f\u0098\u0082\u0083\u00d7\u0083\u0098\u009c\u0092\u0099\u00de\u00d7\u0084\u0092\u0099\u0093\u00b2\u0099\u0094\u00a7\u00a0\u00a3\u0098\u00a4\u0092\u0085\u0081\u0092\u0085"

    const/16 v2, 0xf7

    :pswitch_0
    packed-switch v6, :pswitch_data_0

    :goto_0
    packed-switch v7, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\ufe37\ufe34\ufe27\ufe2b\ufe49\ufe52\ufe49\ufe56\ufe45\ufe58\ufe53\ufe56\ufe36\ufe49\ufe47\ufe53\ufe56\ufe48\ufe31\ufe57\ufe4b"

    const/16 v3, 0xb4

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mPassword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v1, v2, :cond_1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v1

    :try_start_3
    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v1, 0x58

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_1
    :try_start_4
    const-string v1, "\ufff0\uffde\uffe3\uffe2\ufffc\uffdf\ufff1\uffed\uffe8\uffe2\ufffc\uffe0\uffe9\uffe6\uffe2\uffeb\ufff1"

    const/16 v2, 0x7a

    const/16 v3, 0x17

    const/4 v4, 0x2

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\ufe5a\ufe4c\ufe55\ufe4b\ufe2c\ufe55\ufe4a\ufe37\ufe3e\ufe3b\ufe56\ufe3a\ufe4c\ufe59\ufe5d\ufe4c\ufe59"

    const/16 v3, 0xb3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v2

    :try_start_5
    const-string v3, "IHP\u0007TBIC\u0007bIDU^WSBC\u0007CFSF\u0007SH\u0007tBUQBU"
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    const/16 v4, 0x27

    const/4 v5, 0x3

    :try_start_6
    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    array-length v2, v0

    sget-object v3, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    :try_start_7
    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCGeneratorRecordMsg([BILjava/lang/String;)[B
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    move-result-object v0

    if-nez v0, :cond_3

    :try_start_8
    const-string v0, "B4=3\u0014=2\u001f&#>\"4AE4A\t\uffef\"\u001f\u0012\u00164=4A0C>A!42>A3\u001cB6\uffef8B\uffef\u001d$\u001b\u001b\uffef\ufff0\ufff0\ufff0"
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    const/16 v1, 0x31

    const/4 v2, 0x4

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v3, v4, :cond_2

    const/16 v3, 0x35

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_2
    :try_start_9
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    :goto_1
    return-void

    :cond_3
    :try_start_a
    const-string v1, "\uff7c\uff6a\uff6f\uff6e\uff88\uff6b\uff7d\uff79\uff74\uff6e\uff88\uff6c\uff75\uff72\uff6e\uff77\uff7d"

    const/16 v2, 0xc7

    const/16 v3, 0x10

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u009a\u008c\u0095\u008bl\u0095\u008aw~{\u0096z\u008c\u0099\u009d\u008c\u0099"

    const/16 v3, 0xd

    :pswitch_2
    packed-switch v6, :pswitch_data_2

    :goto_2
    packed-switch v7, :pswitch_data_3

    goto :goto_2

    :pswitch_3
    const/4 v4, 0x6

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "?QZPUZS\u000c&\u000c1ZO<c\u000cYQ__MSQ"

    const/16 v4, 0xa

    const/4 v5, 0x7

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/safe/SecureStorageManagerClient;->SendJsonMsg([BLjava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public sendErrorToMain(Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x6

    :try_start_0
    const-string v0, "\u0131\u011f\u0124\u0123\u013d\u0120\u0132\u012e\u0129\u0123\u013d\u0121\u012a\u0127\u0123\u012c\u0132"

    const/16 v1, 0xce

    const/16 v2, 0x10

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "h-\t.)4\r/$%z`"

    const/16 v3, 0x40

    const/4 v4, 0x3

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "R"

    const/16 v3, 0x29

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/sec/android/safe/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendMsgToHandler(Landroid/os/Handler;I)V

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    invoke-virtual {p0}, Lcom/sec/android/safe/SecureStorageManagerClient;->stopBT()V

    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendMsgToHandler(Landroid/os/Handler;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :try_start_2
    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    :try_start_3
    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    packed-switch v0, :pswitch_data_1

    :try_start_4
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v0, 0x37

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :pswitch_2
    :try_start_5
    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mHandlerSecureStorage:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendMsgToHandler(Landroid/os/Handler;I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :try_start_6
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    if-eq v0, v1, :cond_0

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_2

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_3
    :try_start_7
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    :catch_2
    move-exception v0

    throw v0

    :catch_3
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch
.end method

.method public sendHelloToServer(Ljava/lang/String;)V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x1

    :try_start_0
    const-string v0, "\uffd3\uffc1\uffc6\uffc5\uffdf\uffc2\uffd4\uffd0\uffcb\uffc5\uffdf\uffc3\uffcc\uffc9\uffc5\uffce\uffd4"

    const/16 v1, 0x80

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "L!#N\u0011dV_U9V]]`E`DVcgVc"

    const/16 v2, 0xe7

    const/16 v3, 0xd8

    const/4 v4, 0x2

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u014e\u0160\u0169\u015f\u0164\u0169\u0162\u011b\u0135\u011b\u014e\u0160\u016d\u0171\u0160\u016d\u0143\u0160\u0167\u0167\u016a\u011b\u0168\u0160\u016e\u016e\u015c\u0162\u0160"

    const/16 v3, 0x1a

    const/16 v4, 0xe1

    const/4 v5, 0x3

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->mInitMode:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "\u000f\u0004nzR"

    const/16 v1, 0x88

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    :goto_0
    const-string v0, "\u00eb\u00f9\u00fe\u00fd\u00e7\u00fa\u00ec\u00e8\u00f3\u00fd\u00e7\u00fb\u00f4\u00f1\u00fd\u00f6\u00ec"

    const/16 v1, 0xb8

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u00b0\u00a6\u00ad\u00a7\u008b\u00a6\u00af\u00af\u00ac\u0097\u00ac\u0090\u00a6\u00b1\u00b5\u00a6\u00b1"

    const/16 v2, 0xc3

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v5

    add-int/2addr v3, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v3, v5

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v5

    rem-int/2addr v3, v5

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v3, v5, :cond_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    :try_start_1
    const-string v3, "\u00a7\u00d0\u00b9\u00a9\u0090\u00c4=\u00cd<\u0084S\u00e0!\u00c4\u00db3\u00d1R\u00a9=\u00e3\u00e0@,\u00db\u00e4\u0010l\u0086\u0083\u0015cX\u00e5\u001d>gM\u0080\u0003\u0081o\u0004\u00a6\u00ca\u0019X\u00ae"

    const/16 v5, 0xcf

    const/4 v6, 0x1

    invoke-static {v3, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mCTX:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "i\u0099\u0094\u008e\u0096\u0098\u008d\u008eq\u00bc\u00ae\u00bd\u00be\u00b9iyx\u00b8\u00bd\u00b1\u00ae\u00bb\u00bcizr\u0083i"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v5, 0x49

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v6, v7

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v6, v7

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v7

    rem-int/2addr v6, v7

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b041104110411Б04110411()I

    move-result v7

    if-eq v6, v7, :cond_1

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v7, v6

    mul-int/2addr v6, v7

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v6, v7

    packed-switch v6, :pswitch_data_1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v6

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v6, 0x24

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_0
    const/16 v6, 0x1e

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v6, 0x34

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_1
    const/4 v6, 0x5

    :try_start_2
    invoke-static {v3, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mPKEMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->mLoginCount:I

    iget v2, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mCTX:I

    iget v3, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mPKEMode:I

    sget-object v5, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCGeneratorServerHello(IIILjava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    :pswitch_1
    packed-switch v8, :pswitch_data_2

    :goto_1
    packed-switch v9, :pswitch_data_3

    goto :goto_1

    :pswitch_2
    if-nez v0, :cond_2

    const-string v0, "(\u001a#\u0019\ufffd\u001a!!$\t$\u0008\u001a\'+\u001a\'\uffef\uffd5\u0008\u0005\ufff8\ufffc\u001a#\u001a\'\u0016)$\'\u0008\u001a\'+\u001a\'\ufffd\u001a!!$\uffd5\u001e(\uffd5\u0003\n\u0001\u0001\uffd5\uffd6\uffd6\uffd6"

    const/16 v1, 0x4b

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    :goto_2
    return-void

    :pswitch_3
    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/safe/SecureStorageManagerClient;->SendJsonMsg([BLjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    throw v0

    :cond_2
    :try_start_4
    const-string v1, "\u009c\u008a\u008f\u008e\u00a8\u008b\u009d\u0099\u0094\u008e\u00a8\u008c\u0095\u0092\u008e\u0097\u009d"

    const/16 v2, 0x3d

    const/16 v3, 0xc

    const/4 v4, 0x3

    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u01d3\u01c5\u01ce\u01c4\u01a8\u01c5\u01cc\u01cc\u01cf\u01b4\u01cf\u01b3\u01c5\u01d2\u01d6\u01c5\u01d2"

    const/16 v3, 0xb0

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u0003\u00a4I{\u0090\u00a6\u0010\u00d0J4\u0097L\u00c0\u00b7\u0081\u00cc\u00c5m\u00fbQ2\u0007H\u00aa,8QX\u0010"
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    const/16 v4, 0xa5

    :pswitch_4
    packed-switch v9, :pswitch_data_4

    :goto_3
    packed-switch v9, :pswitch_data_5

    goto :goto_3

    :pswitch_5
    :try_start_5
    sget-object v4, Lcom/sec/android/safe/SecureStorageManagerClient;->mOldPW:Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public sendMsgToHandler(Landroid/os/Handler;I)V
    .locals 5

    :try_start_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput p2, v0, Landroid/os/Message;->what:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v3, v4

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v4

    mul-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v3, v4

    sget v4, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v3, v4, :cond_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v3

    sput v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    :try_start_2
    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-eq v1, v2, :cond_1

    :try_start_3
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    :try_start_4
    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :pswitch_0
    const/4 v1, 0x0

    :try_start_5
    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    const/16 v1, 0x26

    :try_start_6
    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_1
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    return-void

    :catch_0
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    :catch_3
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public sendReqInitialSetup(Ljava/lang/String;)V
    .locals 6

    :try_start_0
    const-string v0, "safe\u007fbtpke\u007fclient"

    const/16 v1, 0x20

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\uff8e\uff63\uff64\uff60\uff65\uff90\uff53\uffa6\uff98\uffa1\uff97\uff85\uff98\uffa4\uff7c\uffa1\uff9c\uffa7\uff9c\uff94\uff9f\uff86\uff98\uffa7\uffa8\uffa3"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/16 v2, 0xcd

    const/4 v3, 0x4

    :try_start_1
    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    :try_start_2
    const-string v2, "\uff82\uffa3\uffab\uff54\uffa7\uffa8\uff95\uffa6\uffa8\uff54\uff7d\uffa2\uff9d\uffa8\uff9d\uff95\uffa0\uff54\uff87\uff99\uffa8\uffa9\uffa4"

    const/16 v3, 0xcc

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "\u01ef\u01dd\u01e2\u01e1\u01fb\u01de\u01f0\u01ec\u01e7\u01e1\u01fb\u01df\u01e8\u01e5\u01e1\u01ea\u01f0"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v1, 0xce

    const/4 v2, 0x2

    :try_start_3
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u0088z\u0083ygz\u0086^\u0083~\u0089~v\u0081hz\u0089\u008a\u0085"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const/16 v2, 0xc2

    const/16 v3, 0xad

    const/4 v4, 0x0

    :try_start_4
    invoke-static {v1, v2, v3, v4}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0000\ufffd\ufff0\ufff4\u0012\u001b\u0012\u001f\u000e!\u0012\ufff6\u001b\u0016!\u0016\u000e\u0019\u0000\u0012!\"\u001d\uffd9\uffcd\ufff0\u0001\u0005\uffd5\u0010\u0019\u0016\u0012\u001b!\uffcd\u000e\u0019$\u000e& \uffcd\uffdd\uffd6\uffe7\uffcd\uffdd"

    const/16 v3, 0x53

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->mLoginCount:I

    const/4 v2, 0x0

    sget-object v3, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCGenerateInitialSetup(IILjava/lang/String;)[B
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v0

    if-nez v0, :cond_2

    :try_start_5
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    if-eq v0, v1, :cond_1

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_0
    const/16 v0, 0x17

    :try_start_6
    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v0, 0x17

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :cond_1
    :try_start_7
    const-string v0, "\u0117\u0109\u0112\u0108\u00f6\u0109\u0115\u00ed\u0112\u010d\u0118\u010d\u0105\u0110\u00f7\u0109\u0118\u0119\u0114\u00de\u00c4\u00f7\u00f4\u00e7\u00eb\u0109\u0112\u0109\u0116\u0105\u0118\u0109\u00ed\u0112\u010d\u0118\u010d\u0105\u0110\u00f7\u0109\u0118\u0119\u0114\u00c4\u010d\u0117\u00c4\u00f2\u00f9\u00f0\u00f0\u00c4\u00c5\u00c5\u00c5"

    const/16 v1, 0x52

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    const-string v1, "\u027b\u0269\u026e\u026d\u0287\u026a\u027c\u0278\u0273\u026d\u0287\u026b\u0274\u0271\u026d\u0276\u027c"

    const/16 v2, 0xb8

    const/4 v3, 0x6

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    move-result-object v1

    :try_start_8
    const-string v2, "]\u00aa:@\u00e6\u00c54\u0005\rdM\u00be}\\\u00e5- \u000c\u00b5"

    const/16 v3, 0xd8

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\ufea6\ufeb8\ufec1\ufeb7\ufebc\ufec1\ufeba\ufe73\ufe8d\ufe73\ufe9c\ufec1\ufebc\ufec7\ufebc\ufeb4\ufebf\ufe73\ufec6\ufeb8\ufec7\ufec8\ufec3\ufe73\ufec0\ufeb8\ufec6\ufec6\ufeb4\ufeba\ufeb8"

    const/16 v4, 0x8f

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/safe/SecureStorageManagerClient;->SendJsonMsg([BLjava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_a
    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    :catch_3
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public sendTokenCheckReqToServer(Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    const-string v0, "\uff98\uff86\uff8b\uff8a\uffa4\uff87\uff99\uff95\uff90\uff8a\uffa4\uff88\uff91\uff8e\uff8a\uff93\uff99"

    const/16 v1, 0xbb

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u00f5\u00b0\u00cb\u00ca\u00af\n\u00cc8\u00b5\u0091\u0087\u00e9\u00bdd\u00ad\u00e5A~Lq\u00e5\u00f5\u00d9\u00f4&bD`\u00d5:"

    const/16 v2, 0x71

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\uff88\uff85\uff78\uff7c\uff9a\uffa3\uff9a\uffa7\uff96\uffa9\uffa4\uffa7\uff89\uffa4\uffa0\uff9a\uffa3\uff8b\uff96\uffa1\uff9e\uff99\uff96\uffa9\uff9e\uffa4\uffa3\uff87\uff9a\uffa6"

    const/16 v3, 0xa0

    const/16 v4, 0x2b

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    sget-object v1, Lcom/sec/android/safe/SecureStorageManagerClient;->mSamsungAccountId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCIsTokenExist(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eq v0, v1, :cond_0

    sget v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v1

    add-int/2addr v1, v0

    mul-int/2addr v0, v1

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v0, v1

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x1d

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_0
    :try_start_1
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v0

    sput v0, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :try_start_2
    const-string v0, "\u02c2\u02b4\u02bd\u02b3\u02a3\u02be\u02ba\u02b4\u02bd\u0292\u02b7\u02b4\u02b2\u02ba\u02a1\u02b4\u02c0\u02a3\u02be\u02a2\u02b4\u02c1\u02c5\u02b4\u02c1\u0289\u026f\u02c3\u02be\u02ba\u02b4\u02bd\u026f\u02bd\u02be\u02c3\u026f\u02b4\u02c7\u02b8\u02c2\u02c3\u026f\u0270\u0270\u0270"

    const/16 v1, 0xc5

    const/4 v2, 0x6

    :pswitch_1
    packed-switch v6, :pswitch_data_1

    :goto_0
    packed-switch v6, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    const-string v1, "\uffac\uff9a\uff9f\uff9e\uffb8\uff9b\uffad\uffa9\uffa4\uff9e\uffb8\uff9c\uffa5\uffa2\uff9e\uffa7\uffad"

    const/16 v2, 0xa7

    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    :try_start_3
    const-string v2, "\u01f3\u01e5\u01ee\u01e4\u01d4\u01ef\u01eb\u01e5\u01ee\u01c3\u01e8\u01e5\u01e3\u01eb\u01d2\u01e5\u01f1\u01d4\u01ef\u01d3\u01e5\u01f2\u01f6\u01e5\u01f2"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const/16 v3, 0xbe

    const/16 v4, 0xc2

    const/4 v5, 0x3

    :try_start_4
    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v2

    :try_start_5
    const-string v3, "\ufd80\ufd92\ufd9b\ufd91\ufd96\ufd9b\ufd94\ufd4d\ufd67\ufd4d\ufd80\ufd7d\ufd70\ufd74\ufd92\ufd9b\ufd92\ufd9f\ufd8e\ufda1\ufd9c\ufd9f\ufd81\ufd9c\ufd98\ufd92\ufd9b\ufd83\ufd8e\ufd99\ufd96\ufd91\ufd8e\ufda1\ufd96\ufd9c\ufd9b\ufd7f\ufd92\ufd9e"
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const/16 v4, 0xf1

    const/4 v5, 0x0

    :try_start_6
    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v2

    rem-int/2addr v1, v2

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    if-eq v1, v2, :cond_2

    :try_start_7
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v1, 0x49

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_2
    invoke-virtual {p0, v0, p1}, Lcom/sec/android/safe/SecureStorageManagerClient;->SendJsonMsg([BLjava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    throw v0

    :cond_3
    :try_start_8
    const-string v0, "\uffcd\uffbb\uffc0\uffbf\uffd9\uffbc\uffce\uffca\uffc5\uffbf\uffd9\uffbd\uffc6\uffc3\uffbf\uffc8\uffce"

    const/16 v1, 0x43

    const/4 v2, 0x7

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u0007\ufff9\u0002\ufff8\uffe8\u0003\uffff\ufff9\u0002\uffd7\ufffc\ufff9\ufff7\uffff\uffe6\ufff9\u0005\uffe8\u0003\uffe7\ufff9\u0006\n\ufff9\u0006"

    const/16 v2, 0x24

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u00a2\u0094\u009f\u0093K\u0098{\u0090\u0090\u009dt\u008fYYY"

    const/16 v3, 0xb8

    const/16 v4, 0xe3

    const/4 v5, 0x2

    invoke-static {v2, v3, v4, v5}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    sget-object v1, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerIDByte:[B

    sget-object v2, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerIDByte:[B

    array-length v2, v2

    sget-object v3, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCGeneratorTokenValidationReq([BILjava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "\u00e4\u00d6\u00df\u00d5\u00c5\u00e0\u00dc\u00d6\u00df\u00b4\u00d9\u00d6\u00d4\u00dc\u00c3\u00d6\u00e2\u00c5\u00e0\u00c4\u00d6\u00e3\u00e7\u00d6\u00e3\u00ab\u0091\u00c4\u00c1\u00b4\u00b8\u00d6\u00df\u00d6\u00e3\u00d2\u00e5\u00e0\u00e3\u00c5\u00e0\u00dc\u00d6\u00df\u00c7\u00d2\u00dd\u00da\u00d5\u00d2\u00e5\u00da\u00e0\u00df\u00c3\u00d6\u00e2\u0091\u00da\u00e4\u0091\u00bf\u00c6\u00bd\u00bd\u0091\u0092\u0092\u0092"

    const/16 v1, 0x71

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public sendTokenReqToServer(Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    :try_start_0
    const-string v0, "\uffa6\uff94\uff99\uff98\uffb2\uff95\uffa7\uffa3\uff9e\uff98\uffb2\uff96\uff9f\uff9c\uff98\uffa1\uffa7"

    const/16 v1, 0x94

    const/16 v2, 0x19

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u02c8\u029d\u02a5\u029c\u029d\u02a3\u02ca\u028d\u0295\u029d\u02a5\u029c\u029d\u02a3\u02a7\u02d0\u02d5\u02ce\u02db\u02d4\u02d2\u0296\u028d\u02e0\u02d2\u02db\u02d1\u02c1\u02dc\u02d8\u02d2\u02db\u02bf\u02d2\u02de\u02c1\u02dc\u02c0\u02d2\u02df\u02e3\u02d2\u02df"

    const/16 v2, 0xcf

    const/4 v3, 0x6

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u0185\u0197\u01a4\u01a8\u0197\u01a4\u0152\u0173\u0195\u019d\u0152\u019b\u01a5\u0152\u01a8\u0193\u019e\u019b\u0196\u0193\u01a6\u0197\u0196\u015e\u0152\u01a0\u01a1\u01a9\u0152\u01a5\u0197\u01a0\u0196\u0152\u017b\u017f\u0177\u017b\u0152\u0196\u0193\u01a6\u0193\u0152\u01a6\u01a1\u0152\u0185\u0197\u01a4\u01a8\u0197\u01a4"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/16 v3, 0x66

    const/4 v4, 0x6

    :try_start_1
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "\uffab\uff99\uff9e\uff9d\uffb7\uff9a\uffac\uffa8\uffa3\uff9d\uffb7\uff9b\uffa4\uffa1\uff9d\uffa6\uffac"

    const/16 v1, 0xa8

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    const-string v1, "w\u00cbX|\u0093\u007f8\u00b3`\u00bd&\u007f\tT\'\u00da\u0090C\u00d2\u00a9"

    const/16 v2, 0x78

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    :try_start_2
    const-string v2, "\u01c1\u01b3\u01be\u01b2\u016a\u01b7\u019a\u01af\u01af\u01bc\u0193\u018e\u018c\u01c3\u01be\u01af\u0178\u0178\u0178"

    const/16 v3, 0xa5

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    sget-object v1, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerIDByte:[B

    sget-object v2, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerIDByte:[B

    array-length v2, v2

    sget-object v3, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCGeneratorTokenReq([BILjava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "\u01c9\u01bb\u01c4\u01ba\u01aa\u01c5\u01c1\u01bb\u01c4\u01a8\u01bb\u01c7\u01aa\u01c5\u01a9\u01bb\u01c8\u01cc\u01bb\u01c8\u0190\u0176\u01a9\u01a6\u0199\u019d\u01bb\u01c4\u01bb\u01c8\u01b7\u01ca\u01c5\u01c8\u01aa\u01c5\u01c1\u01bb\u01c4\u01a8\u01bb\u01c7\u0176\u01bf\u01c9\u0176\u01a4\u01ab\u01a2\u01a2\u0176\u0177\u0177\u0177"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :pswitch_0
    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ04110411Б04110411()I

    move-result v2

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v2

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v1, 0x2f

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_1
    packed-switch v6, :pswitch_data_1

    :goto_0
    packed-switch v5, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    const/16 v1, 0xde

    const/16 v2, 0x78

    const/4 v3, 0x3

    :try_start_3
    invoke-static {v0, v1, v2, v3}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v0

    sget v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v2, v1

    mul-int/2addr v1, v2

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411Б0411Б04110411()I

    move-result v2

    rem-int/2addr v1, v2

    packed-switch v1, :pswitch_data_3

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v1

    sput v1, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_3
    :try_start_4
    invoke-virtual {p0, v0}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const-string v1, "\u00c1\u00af\u00b4\u00b3\u00cd\u00b0\u00c2\u00be\u00b9\u00b3\u00cd\u00b1\u00ba\u00b7\u00b3\u00bc\u00c2"

    const/16 v2, 0x6e

    const/4 v3, 0x5

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v1

    :try_start_5
    const-string v2, "\ufd9d\ufd8f\ufd98\ufd8e\ufd7e\ufd99\ufd95\ufd8f\ufd98\ufd7c\ufd8f\ufd9b\ufd7e\ufd99\ufd7d\ufd8f\ufd9c\ufda0\ufd8f\ufd9c"
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    const/16 v3, 0xf2

    const/4 v4, 0x0

    sget v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v6, v5

    mul-int/2addr v5, v6

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v5, v6

    packed-switch v5, :pswitch_data_4

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v5

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    const/16 v5, 0x2c

    sput v5, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :pswitch_4
    :try_start_6
    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "n\u0080\u0089\u007f\u0084\u0089\u0082;U;dh`d;o\u008a\u0086\u0080\u0089;\u0088\u0080\u008e\u008e|\u0082\u0080"

    const/16 v4, 0x9

    const/16 v5, 0x24

    const/4 v6, 0x2

    invoke-static {v3, v4, v5, v6}, Lkkkkkk/nkkkkk;->b044C044C044Cььь(Ljava/lang/String;CCC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/safe/SecureStorageManagerClient;->SendJsonMsg([BLjava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public stopBT()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBluetoothManager:Lcom/sec/android/safe/manager/BluetoothManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->mBluetoothManager:Lcom/sec/android/safe/manager/BluetoothManager;

    invoke-virtual {v0}, Lcom/sec/android/safe/manager/BluetoothManager;->stop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    throw v0
.end method

.method public storeToken([B)Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "\ufe59\ufe47\ufe4c\ufe4b\ufe65\ufe48\ufe5a\ufe56\ufe51\ufe4b\ufe65\ufe49\ufe52\ufe4f\ufe4b\ufe54\ufe5a"

    const/16 v3, 0xfd

    const/4 v4, 0x7

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "h\u000c\u00ecKc\u00e2\u0081d\u0002\u009cH\u00cfS\u0090IC\u00ca\u0013R\u00ab\u0092R\r\u00cdP\u001bt\u00a6\u00cf6\u00f7BG\u00fcT\u001eP\u0007\u0017`\u00cc\n\u0083`3\u00aa2i+\u00df+\u00dd\u00cc"

    const/16 v4, 0x15

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\u017c\u018e\u019b\u019f\u018e\u019b\u0149\u016a\u018c\u0194\u0149\u01a0\u0192\u019d\u0191\u0149\u017d\u0198\u0194\u018e\u0197\u0176\u018e\u019c\u019c\u018a\u0190\u018e\u0149\u0192\u019c\u0149\u019b\u018e\u018c\u018e\u0192\u019f\u018e\u018d"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/16 v5, 0x63

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v6

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v6, v7

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v7

    mul-int/2addr v6, v7

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v6, v7

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    if-eq v6, v7, :cond_0

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v6

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v6

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    sget v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v6, v7

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v6, v7

    sget v7, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v6, v7

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->b041104110411Б04110411()I

    move-result v7

    if-eq v6, v7, :cond_0

    const/16 v6, 0x4a

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v6

    sput v6, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I

    :cond_0
    const/4 v6, 0x6

    :try_start_1
    invoke-static {v4, v5, v6}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/safe/SecureStorageManagerClient;->SecureClientJNI:Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;

    array-length v3, p1

    sget-object v4, Lcom/sec/android/safe/SecureStorageManagerClient;->mPeerId:Ljava/lang/String;

    invoke-virtual {v2, p1, v3, v4}, Lcom/sec/android/safe/SecSecureFileTransferClientUnitJNI;->SPCStoreToken([BILjava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result v2

    if-eqz v2, :cond_2

    :try_start_2
    sget v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bБ0411ББ04110411:I

    add-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    mul-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b04110411ББ04110411:I

    rem-int/2addr v2, v3

    sget v3, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-eq v2, v3, :cond_1

    const/16 v2, 0x23

    :try_start_3
    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->bББББ04110411:I

    invoke-static {}, Lcom/sec/android/safe/SecureStorageManagerClient;->bББ0411Б04110411()I

    move-result v2

    sput v2, Lcom/sec/android/safe/SecureStorageManagerClient;->b0411БББ04110411:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_1
    :try_start_4
    const-string v2, "\ufe7e\ufe7f\ufe7a\ufe7d\ufe70\ufe5f\ufe7a\ufe76\ufe70\ufe79\ufe45\ufe2b\ufe5e\ufe5b\ufe4e\ufe5e\ufe7f\ufe7a\ufe7d\ufe70\ufe5f\ufe7a\ufe76\ufe70\ufe79\ufe2b\ufe74\ufe7e\ufe2b\ufe59\ufe60\ufe57\ufe57\ufe2b\ufe2c\ufe2c\ufe2c"

    const/16 v3, 0xa7

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    :pswitch_0
    packed-switch v1, :pswitch_data_0

    :goto_0
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/sec/android/safe/SecureStorageManagerClient;->sendErrorToMain(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move v0, v1

    :goto_1
    return v0

    :cond_2
    :try_start_5
    const-string v1, "@RUVLQGCXVLP_ZV]G"

    const/16 v2, 0x13

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\uff2f\uff30\uff2b\uff2e\uff21\uff10\uff2b\uff27\uff21\uff2a"

    const/16 v3, 0xa2

    const/4 v4, 0x7

    invoke-static {v2, v3, v4}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\u00cb\u00e6\u00e2\u00dc\u00e5\u0097\u00ca\u00eb\u00e6\u00e9\u00dc\u00db\u0097\u00ca\u00ec\u00da\u00da\u00dc\u00ea\u00ea\u00dd\u00ec\u00e3\u00e3\u00f0"

    const/16 v4, 0x77

    const/4 v5, 0x5

    invoke-static {v3, v4, v5}, Lkkkkkk/nkkkkk;->bь044C044Cььь(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/safe/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    :catch_3
    move-exception v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
