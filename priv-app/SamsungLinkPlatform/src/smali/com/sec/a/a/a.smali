.class public final Lcom/sec/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/a/a/a$a;,
        Lcom/sec/a/a/a$b;
    }
.end annotation


# instance fields
.field public final a:I

.field b:Ljava/lang/String;

.field c:I

.field private d:Lcom/sec/a/a/a$b;

.field private e:Landroid/content/pm/PackageManager;

.field private f:Ljava/lang/reflect/Method;

.field private g:Ljava/lang/reflect/Method;

.field private h:Lcom/sec/a/a/c;

.field private i:Lcom/sec/a/a/d;

.field private j:Landroid/os/Handler;

.field private k:Landroid/os/Handler;

.field private l:Ljava/lang/reflect/Method;

.field private m:Lcom/sec/a/a/a$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;,
            Ljava/lang/NoSuchMethodException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput v4, p0, Lcom/sec/a/a/a;->a:I

    .line 360
    new-instance v0, Lcom/sec/a/a/a$1;

    invoke-direct {v0, p0}, Lcom/sec/a/a/a$1;-><init>(Lcom/sec/a/a/a;)V

    iput-object v0, p0, Lcom/sec/a/a/a;->j:Landroid/os/Handler;

    .line 368
    new-instance v0, Lcom/sec/a/a/a$2;

    invoke-direct {v0, p0}, Lcom/sec/a/a/a$2;-><init>(Lcom/sec/a/a/a;)V

    iput-object v0, p0, Lcom/sec/a/a/a;->k:Landroid/os/Handler;

    .line 386
    new-instance v0, Lcom/sec/a/a/a$b;

    invoke-direct {v0, p0}, Lcom/sec/a/a/a$b;-><init>(Lcom/sec/a/a/a;)V

    iput-object v0, p0, Lcom/sec/a/a/a;->d:Lcom/sec/a/a/a$b;

    .line 387
    new-instance v0, Lcom/sec/a/a/a$a;

    invoke-direct {v0, p0}, Lcom/sec/a/a/a$a;-><init>(Lcom/sec/a/a/a;)V

    iput-object v0, p0, Lcom/sec/a/a/a;->m:Lcom/sec/a/a/a$a;

    .line 388
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a;->e:Landroid/content/pm/PackageManager;

    .line 390
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Landroid/net/Uri;

    aput-object v1, v0, v2

    const-class v1, Landroid/content/pm/IPackageInstallObserver;

    aput-object v1, v0, v3

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v4

    const-class v1, Ljava/lang/String;

    aput-object v1, v0, v5

    .line 391
    iget-object v1, p0, Lcom/sec/a/a/a;->e:Landroid/content/pm/PackageManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "installPackage"

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a;->f:Ljava/lang/reflect/Method;

    .line 394
    :try_start_0
    iget-object v0, p0, Lcom/sec/a/a/a;->e:Landroid/content/pm/PackageManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "installExistingPackage"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a;->g:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    :goto_0
    const/4 v0, 0x3

    :try_start_1
    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Landroid/content/pm/IPackageDeleteObserver;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    .line 401
    iget-object v1, p0, Lcom/sec/a/a/a;->e:Landroid/content/pm/PackageManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "deletePackage"

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/a/a/a;->l:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 406
    :goto_1
    return-void

    .line 396
    :catch_0
    move-exception v0

    iput-object v6, p0, Lcom/sec/a/a/a;->g:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 404
    :catch_1
    move-exception v0

    iput-object v6, p0, Lcom/sec/a/a/a;->l:Ljava/lang/reflect/Method;

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/a/a/a;)Lcom/sec/a/a/c;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/a/a/a;->h:Lcom/sec/a/a/c;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/a/a/a;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/a/a/a;->j:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/a/a/a;)Lcom/sec/a/a/d;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/a/a/a;->i:Lcom/sec/a/a/d;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/a/a/a;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/a/a/a;->k:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 434
    iget-object v0, p0, Lcom/sec/a/a/a;->f:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/sec/a/a/a;->e:Landroid/content/pm/PackageManager;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/a/a/a;->d:Lcom/sec/a/a/a$b;

    aput-object v4, v2, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    return-void
.end method

.method public final a(Lcom/sec/a/a/c;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/sec/a/a/a;->h:Lcom/sec/a/a/c;

    .line 410
    return-void
.end method
