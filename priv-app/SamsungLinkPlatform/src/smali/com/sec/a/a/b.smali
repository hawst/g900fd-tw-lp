.class public final Lcom/sec/a/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/io/File;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/a/a/b;->e:Z

    .line 17
    iput-object p1, p0, Lcom/sec/a/a/b;->a:Landroid/content/Context;

    .line 18
    iput-object p2, p0, Lcom/sec/a/a/b;->c:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public final a()Ljava/io/File;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/a/a/b;->b:Ljava/io/File;

    return-object v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 58
    iget-boolean v1, p0, Lcom/sec/a/a/b;->e:Z

    if-eqz v1, :cond_0

    .line 66
    :goto_0
    return v0

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/sec/a/a/b;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 62
    :cond_1
    iget-object v1, p0, Lcom/sec/a/a/b;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/a/a/b;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/a/a/b;->b:Ljava/io/File;

    .line 63
    iget-object v1, p0, Lcom/sec/a/a/b;->b:Ljava/io/File;

    if-nez v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :cond_2
    iget-object v1, p0, Lcom/sec/a/a/b;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/a/a/b;->d:Ljava/lang/String;

    .line 65
    iput-boolean v0, p0, Lcom/sec/a/a/b;->e:Z

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/a/a/b;->c:Ljava/lang/String;

    return-object v0
.end method
