.class final Lpcloud/net/c;
.super Ljava/io/OutputStream;
.source "SourceFile"


# instance fields
.field private final a:Lpcloud/net/nat/c;

.field private final b:I

.field private final c:Lpcloud/net/nat/a;

.field private d:Lpcloud/net/e;

.field private final e:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Lpcloud/net/nat/a;ILpcloud/net/e;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/c;->e:Landroid/content/Context;

    .line 29
    iget-object v0, p0, Lpcloud/net/c;->e:Landroid/content/Context;

    invoke-static {v0}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/c;->a:Lpcloud/net/nat/c;

    .line 30
    iput-object p2, p0, Lpcloud/net/c;->c:Lpcloud/net/nat/a;

    .line 31
    iput p3, p0, Lpcloud/net/c;->b:I

    .line 32
    iput-object p4, p0, Lpcloud/net/c;->d:Lpcloud/net/e;

    .line 33
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lpcloud/net/c;->d:Lpcloud/net/e;

    .line 90
    return-void
.end method

.method protected final finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 94
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lpcloud/net/c;->d:Lpcloud/net/e;

    .line 96
    return-void
.end method

.method public final flush()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-super {p0}, Ljava/io/OutputStream;->flush()V

    .line 102
    return-void
.end method

.method public final write(I)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 38
    iget-object v0, p0, Lpcloud/net/c;->d:Lpcloud/net/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lpcloud/net/c;->d:Lpcloud/net/e;

    invoke-virtual {v0}, Lpcloud/net/e;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 39
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lpcloud/net/c;->d:Lpcloud/net/e;

    .line 40
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Socket already closed."

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_1
    new-array v2, v5, [B

    .line 44
    const/4 v0, 0x0

    int-to-byte v1, p1

    aput-byte v1, v2, v0

    .line 45
    iget-object v0, p0, Lpcloud/net/c;->a:Lpcloud/net/nat/c;

    iget-object v1, p0, Lpcloud/net/c;->c:Lpcloud/net/nat/a;

    array-length v3, v2

    iget v4, p0, Lpcloud/net/c;->b:I

    iget-object v6, p0, Lpcloud/net/c;->d:Lpcloud/net/e;

    invoke-virtual {v6}, Lpcloud/net/e;->a()Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lpcloud/net/nat/c;->a(Lpcloud/net/nat/a;[BIIZZ)I

    .line 46
    return-void
.end method

.method public final write([B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lpcloud/net/c;->write([BII)V

    .line 51
    return-void
.end method

.method public final write([BII)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lpcloud/net/c;->d:Lpcloud/net/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lpcloud/net/c;->d:Lpcloud/net/e;

    invoke-virtual {v0}, Lpcloud/net/e;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lpcloud/net/c;->d:Lpcloud/net/e;

    .line 59
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Socket already closed."

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_1
    if-nez p1, :cond_2

    .line 63
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 64
    :cond_2
    if-ltz p2, :cond_3

    array-length v0, p1

    if-gt p2, v0, :cond_3

    if-ltz p3, :cond_3

    add-int v0, p2, p3

    array-length v1, p1

    if-gt v0, v1, :cond_3

    add-int v0, p2, p3

    if-gez v0, :cond_4

    .line 65
    :cond_3
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 66
    :cond_4
    if-nez p3, :cond_5

    .line 79
    :goto_0
    return-void

    .line 73
    :cond_5
    if-lez p2, :cond_6

    .line 75
    new-array v2, p3, [B

    .line 76
    const/4 v0, 0x0

    invoke-static {p1, p2, v2, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 78
    :goto_1
    iget-object v0, p0, Lpcloud/net/c;->a:Lpcloud/net/nat/c;

    iget-object v1, p0, Lpcloud/net/c;->c:Lpcloud/net/nat/a;

    iget v4, p0, Lpcloud/net/c;->b:I

    const/4 v5, 0x1

    iget-object v3, p0, Lpcloud/net/c;->d:Lpcloud/net/e;

    invoke-virtual {v3}, Lpcloud/net/e;->a()Z

    move-result v6

    move v3, p3

    invoke-virtual/range {v0 .. v6}, Lpcloud/net/nat/c;->a(Lpcloud/net/nat/a;[BIIZZ)I

    goto :goto_0

    :cond_6
    move-object v2, p1

    goto :goto_1
.end method
