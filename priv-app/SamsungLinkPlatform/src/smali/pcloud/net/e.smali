.class public final Lpcloud/net/e;
.super Ljava/net/Socket;
.source "SourceFile"


# static fields
.field private static a:Lorg/slf4j/Logger;


# instance fields
.field private final b:Lpcloud/net/nat/c;

.field private final c:Ljava/lang/Object;

.field private d:Z

.field private e:Lpcloud/net/b;

.field private f:Lpcloud/net/c;

.field private final g:Ljava/lang/String;

.field private h:I

.field private final i:I

.field private final j:I

.field private final k:Z

.field private final l:Z

.field private final m:Lcom/mfluent/asp/util/t$d;

.field private n:Lpcloud/net/nat/a;

.field private final o:Landroid/content/Context;

.field private p:Lpcloud/net/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lpcloud/net/nat/c;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lpcloud/net/e;->a:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 77
    invoke-direct {p0}, Ljava/net/Socket;-><init>()V

    .line 31
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lpcloud/net/e;->c:Ljava/lang/Object;

    .line 32
    iput-boolean v1, p0, Lpcloud/net/e;->d:Z

    .line 41
    iput-boolean v2, p0, Lpcloud/net/e;->l:Z

    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/e;->o:Landroid/content/Context;

    .line 79
    iget-object v0, p0, Lpcloud/net/e;->o:Landroid/content/Context;

    invoke-static {v0}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/e;->b:Lpcloud/net/nat/c;

    .line 80
    new-instance v0, Lpcloud/net/nat/a;

    invoke-direct {v0, v1}, Lpcloud/net/nat/a;-><init>(Z)V

    iput-object v0, p0, Lpcloud/net/e;->n:Lpcloud/net/nat/a;

    .line 84
    iget-object v0, p0, Lpcloud/net/e;->n:Lpcloud/net/nat/a;

    invoke-virtual {v0, p3}, Lpcloud/net/nat/a;->setConn_id(I)V

    .line 85
    iput-object p2, p0, Lpcloud/net/e;->g:Ljava/lang/String;

    .line 86
    iput-boolean v2, p0, Lpcloud/net/e;->d:Z

    .line 87
    const v0, 0xea60

    iput v0, p0, Lpcloud/net/e;->i:I

    .line 88
    const/16 v0, 0x1f90

    iput v0, p0, Lpcloud/net/e;->j:I

    .line 89
    iput-boolean v1, p0, Lpcloud/net/e;->k:Z

    .line 90
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lpcloud/net/e;->m:Lcom/mfluent/asp/util/t$d;

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZLpcloud/net/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/net/Socket;-><init>()V

    .line 31
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lpcloud/net/e;->c:Ljava/lang/Object;

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpcloud/net/e;->d:Z

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lpcloud/net/e;->l:Z

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/e;->o:Landroid/content/Context;

    .line 62
    invoke-static {p1}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/e;->b:Lpcloud/net/nat/c;

    .line 63
    iput-object p2, p0, Lpcloud/net/e;->g:Ljava/lang/String;

    .line 64
    const/16 v0, 0x7530

    iput v0, p0, Lpcloud/net/e;->i:I

    .line 65
    const/16 v0, 0x1f90

    iput v0, p0, Lpcloud/net/e;->j:I

    .line 66
    iput-boolean p3, p0, Lpcloud/net/e;->k:Z

    .line 67
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lpcloud/net/e;->m:Lcom/mfluent/asp/util/t$d;

    .line 72
    iput-object p4, p0, Lpcloud/net/e;->p:Lpcloud/net/a;

    .line 73
    return-void
.end method

.method static synthetic a(Lpcloud/net/e;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lpcloud/net/e;->o:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lpcloud/net/e;)Lpcloud/net/nat/a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lpcloud/net/e;->n:Lpcloud/net/nat/a;

    return-object v0
.end method

.method static synthetic c(Lpcloud/net/e;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lpcloud/net/e;->i:I

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 115
    iput p1, p0, Lpcloud/net/e;->h:I

    .line 117
    iget-object v0, p0, Lpcloud/net/e;->g:Ljava/lang/String;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 118
    iget-object v1, p0, Lpcloud/net/e;->b:Lpcloud/net/nat/c;

    aget-object v0, v0, v4

    iget v2, p0, Lpcloud/net/e;->j:I

    iget v3, p0, Lpcloud/net/e;->h:I

    invoke-virtual {v1, v0, v2, v3}, Lpcloud/net/nat/c;->a(Ljava/lang/String;II)Lpcloud/net/nat/a;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/e;->n:Lpcloud/net/nat/a;

    .line 119
    iget-object v0, p0, Lpcloud/net/e;->m:Lcom/mfluent/asp/util/t$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lpcloud/net/e;->n:Lpcloud/net/nat/a;

    invoke-virtual {v0}, Lpcloud/net/nat/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    sget-object v0, Lpcloud/net/e;->a:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "::connect got a server connection. Lock:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lpcloud/net/e;->m:Lcom/mfluent/asp/util/t$d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 122
    :cond_0
    iput-boolean v4, p0, Lpcloud/net/e;->d:Z

    .line 123
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lpcloud/net/e;->k:Z

    return v0
.end method

.method public final b()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    const/16 v0, 0x3a98

    invoke-virtual {p0, v0}, Lpcloud/net/e;->a(I)V

    .line 111
    return-void
.end method

.method public final bind(Ljava/net/SocketAddress;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Function is not available does not support"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lpcloud/net/e;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 337
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lpcloud/net/e;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 338
    :try_start_1
    invoke-virtual {p0}, Lpcloud/net/e;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 354
    :goto_0
    monitor-exit p0

    return-void

    .line 342
    :cond_0
    :try_start_2
    iget-object v0, p0, Lpcloud/net/e;->p:Lpcloud/net/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lpcloud/net/e;->p:Lpcloud/net/a;

    invoke-virtual {v0}, Lpcloud/net/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lpcloud/net/e;->n:Lpcloud/net/nat/a;

    if-eqz v0, :cond_1

    .line 343
    sget-object v0, Lpcloud/net/e;->a:Lorg/slf4j/Logger;

    const-string v2, "destroying connection {} because destroySignal is signalled"

    iget-object v3, p0, Lpcloud/net/e;->n:Lpcloud/net/nat/a;

    invoke-interface {v0, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 344
    iget-object v0, p0, Lpcloud/net/e;->n:Lpcloud/net/nat/a;

    invoke-virtual {v0}, Lpcloud/net/nat/a;->b()V

    .line 346
    :cond_1
    sget-object v0, Lpcloud/net/e;->a:Lorg/slf4j/Logger;

    const-string v2, "returning connection {} to the pool"

    iget-object v3, p0, Lpcloud/net/e;->n:Lpcloud/net/nat/a;

    invoke-interface {v0, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 347
    iget-object v0, p0, Lpcloud/net/e;->b:Lpcloud/net/nat/c;

    iget-object v2, p0, Lpcloud/net/e;->n:Lpcloud/net/nat/a;

    invoke-virtual {v0, v2}, Lpcloud/net/nat/c;->a(Lpcloud/net/nat/a;)V

    .line 348
    invoke-virtual {p0}, Lpcloud/net/e;->shutdownInput()V

    .line 349
    invoke-virtual {p0}, Lpcloud/net/e;->shutdownOutput()V

    .line 350
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpcloud/net/e;->d:Z

    .line 351
    iget-object v0, p0, Lpcloud/net/e;->m:Lcom/mfluent/asp/util/t$d;

    if-eqz v0, :cond_2

    .line 352
    iget-object v0, p0, Lpcloud/net/e;->m:Lcom/mfluent/asp/util/t$d;

    invoke-virtual {v0}, Lcom/mfluent/asp/util/t$d;->a()V

    .line 354
    :cond_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 337
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final connect(Ljava/net/SocketAddress;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Function is not available does not support"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final connect(Ljava/net/SocketAddress;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Function is not available does not support"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getChannel()Ljava/nio/channels/SocketChannel;
    .locals 1

    .prologue
    .line 170
    invoke-super {p0}, Ljava/net/Socket;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v0

    return-object v0
.end method

.method public final getInetAddress()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    invoke-virtual {p0}, Lpcloud/net/e;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Socket is closed"

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_0
    invoke-virtual {p0}, Lpcloud/net/e;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 179
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Socket is not connected"

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_1
    invoke-virtual {p0}, Lpcloud/net/e;->isInputShutdown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 182
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Socket input is shutdown"

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_2
    iget-object v0, p0, Lpcloud/net/e;->e:Lpcloud/net/b;

    if-nez v0, :cond_3

    .line 188
    :try_start_0
    new-instance v0, Lpcloud/net/e$1;

    invoke-direct {v0, p0, p0}, Lpcloud/net/e$1;-><init>(Lpcloud/net/e;Lpcloud/net/e;)V

    invoke-static {v0}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedExceptionAction;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpcloud/net/b;

    iput-object v0, p0, Lpcloud/net/e;->e:Lpcloud/net/b;
    :try_end_0
    .catch Ljava/security/PrivilegedActionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :cond_3
    iget-object v0, p0, Lpcloud/net/e;->e:Lpcloud/net/b;

    return-object v0

    .line 195
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/security/PrivilegedActionException;->getException()Ljava/lang/Exception;

    move-result-object v0

    check-cast v0, Ljava/io/IOException;

    throw v0
.end method

.method public final getLocalAddress()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method public final getLocalPort()I
    .locals 1

    .prologue
    .line 152
    invoke-super {p0}, Ljava/net/Socket;->getLocalPort()I

    move-result v0

    return v0
.end method

.method public final getLocalSocketAddress()Ljava/net/SocketAddress;
    .locals 1

    .prologue
    .line 164
    invoke-super {p0}, Ljava/net/Socket;->getLocalSocketAddress()Ljava/net/SocketAddress;

    move-result-object v0

    return-object v0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 205
    invoke-virtual {p0}, Lpcloud/net/e;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Socket is closed"

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 208
    :cond_0
    invoke-virtual {p0}, Lpcloud/net/e;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Socket is not connected"

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_1
    invoke-virtual {p0}, Lpcloud/net/e;->isOutputShutdown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 212
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Socket output is shutdown"

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :cond_2
    iget-object v0, p0, Lpcloud/net/e;->f:Lpcloud/net/c;

    if-nez v0, :cond_3

    .line 218
    :try_start_0
    new-instance v0, Lpcloud/net/e$2;

    invoke-direct {v0, p0, p0}, Lpcloud/net/e$2;-><init>(Lpcloud/net/e;Lpcloud/net/e;)V

    invoke-static {v0}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedExceptionAction;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpcloud/net/c;

    iput-object v0, p0, Lpcloud/net/e;->f:Lpcloud/net/c;
    :try_end_0
    .catch Ljava/security/PrivilegedActionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :cond_3
    iget-object v0, p0, Lpcloud/net/e;->f:Lpcloud/net/c;

    return-object v0

    .line 225
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/security/PrivilegedActionException;->getException()Ljava/lang/Exception;

    move-result-object v0

    check-cast v0, Ljava/io/IOException;

    throw v0
.end method

.method public final getPort()I
    .locals 1

    .prologue
    .line 146
    invoke-super {p0}, Ljava/net/Socket;->getPort()I

    move-result v0

    return v0
.end method

.method public final declared-synchronized getReceiveBufferSize()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 302
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Ljava/net/Socket;->getReceiveBufferSize()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getRemoteSocketAddress()Ljava/net/SocketAddress;
    .locals 1

    .prologue
    .line 158
    invoke-super {p0}, Ljava/net/Socket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    move-result-object v0

    return-object v0
.end method

.method public final getReuseAddress()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 332
    invoke-super {p0}, Ljava/net/Socket;->getReuseAddress()Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized getSendBufferSize()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 290
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Ljava/net/Socket;->getSendBufferSize()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getSoLinger()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 256
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Function is not available does not support."

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final declared-synchronized getSoTimeout()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 278
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lpcloud/net/e;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getTcpNoDelay()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 242
    invoke-super {p0}, Ljava/net/Socket;->getTcpNoDelay()Z

    move-result v0

    return v0
.end method

.method public final getTrafficClass()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 320
    invoke-super {p0}, Ljava/net/Socket;->getTrafficClass()I

    move-result v0

    return v0
.end method

.method public final isBound()Z
    .locals 1

    .prologue
    .line 388
    invoke-super {p0}, Ljava/net/Socket;->isBound()Z

    move-result v0

    return v0
.end method

.method public final isClosed()Z
    .locals 2

    .prologue
    .line 393
    iget-object v1, p0, Lpcloud/net/e;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 394
    :try_start_0
    iget-boolean v0, p0, Lpcloud/net/e;->d:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 395
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isConnected()Z
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0}, Lpcloud/net/e;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInputShutdown()Z
    .locals 1

    .prologue
    .line 400
    invoke-virtual {p0}, Lpcloud/net/e;->isClosed()Z

    move-result v0

    return v0
.end method

.method public final isOutputShutdown()Z
    .locals 1

    .prologue
    .line 405
    invoke-virtual {p0}, Lpcloud/net/e;->isClosed()Z

    move-result v0

    return v0
.end method

.method public final sendUrgentData(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 262
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Function is not available does not support."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setKeepAlive(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 308
    invoke-super {p0, p1}, Ljava/net/Socket;->setKeepAlive(Z)V

    .line 309
    return-void
.end method

.method public final setOOBInline(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 268
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Function is not available does not support."

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setPerformancePreferences(III)V
    .locals 0

    .prologue
    .line 411
    invoke-super {p0, p1, p2, p3}, Ljava/net/Socket;->setPerformancePreferences(III)V

    .line 412
    return-void
.end method

.method public final declared-synchronized setReceiveBufferSize(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 296
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Ljava/net/Socket;->setReceiveBufferSize(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    monitor-exit p0

    return-void

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setReuseAddress(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 326
    invoke-super {p0, p1}, Ljava/net/Socket;->setReuseAddress(Z)V

    .line 327
    return-void
.end method

.method public final declared-synchronized setSendBufferSize(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 284
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Ljava/net/Socket;->setSendBufferSize(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    monitor-exit p0

    return-void

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setSoLinger(ZI)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 250
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Function is not available does not support."

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final declared-synchronized setSoTimeout(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 273
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lpcloud/net/e;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    monitor-exit p0

    return-void

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setTcpNoDelay(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 235
    invoke-super {p0, p1}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    .line 238
    return-void
.end method

.method public final setTrafficClass(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 314
    invoke-super {p0, p1}, Ljava/net/Socket;->setTrafficClass(I)V

    .line 315
    return-void
.end method

.method public final shutdownInput()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 359
    iget-object v0, p0, Lpcloud/net/e;->e:Lpcloud/net/b;

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lpcloud/net/e;->e:Lpcloud/net/b;

    .line 361
    const/4 v1, 0x0

    iput-object v1, p0, Lpcloud/net/e;->e:Lpcloud/net/b;

    .line 362
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 364
    :cond_0
    return-void
.end method

.method public final shutdownOutput()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 368
    iget-object v0, p0, Lpcloud/net/e;->f:Lpcloud/net/c;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lpcloud/net/e;->f:Lpcloud/net/c;

    .line 370
    const/4 v1, 0x0

    iput-object v1, p0, Lpcloud/net/e;->f:Lpcloud/net/c;

    .line 371
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 373
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 377
    invoke-super {p0}, Ljava/net/Socket;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
