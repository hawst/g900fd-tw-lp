.class final Lpcloud/net/b;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field private final a:Lpcloud/net/nat/c;

.field private final b:I

.field private final c:Lpcloud/net/nat/a;

.field private d:Lpcloud/net/e;

.field private final e:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Lpcloud/net/nat/a;ILpcloud/net/e;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lpcloud/net/b;->d:Lpcloud/net/e;

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/b;->e:Landroid/content/Context;

    .line 32
    iget-object v0, p0, Lpcloud/net/b;->e:Landroid/content/Context;

    invoke-static {v0}, Lpcloud/net/nat/c;->a(Landroid/content/Context;)Lpcloud/net/nat/c;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/b;->a:Lpcloud/net/nat/c;

    .line 33
    iput-object p2, p0, Lpcloud/net/b;->c:Lpcloud/net/nat/a;

    .line 34
    iput p3, p0, Lpcloud/net/b;->b:I

    .line 35
    iput-object p4, p0, Lpcloud/net/b;->d:Lpcloud/net/e;

    .line 36
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-super {p0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lpcloud/net/b;->d:Lpcloud/net/e;

    .line 99
    return-void
.end method

.method protected final finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 103
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lpcloud/net/b;->d:Lpcloud/net/e;

    .line 105
    return-void
.end method

.method public final read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    const/4 v0, 0x1

    new-array v0, v0, [B

    .line 41
    invoke-virtual {p0, v0}, Lpcloud/net/b;->read([B)I

    move-result v1

    if-gez v1, :cond_0

    .line 42
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Socket closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    const/4 v1, 0x0

    aget-byte v0, v0, v1

    return v0
.end method

.method public final read([B)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lpcloud/net/b;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 55
    iget-object v1, p0, Lpcloud/net/b;->d:Lpcloud/net/e;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lpcloud/net/b;->d:Lpcloud/net/e;

    invoke-virtual {v1}, Lpcloud/net/e;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lpcloud/net/b;->d:Lpcloud/net/e;

    .line 57
    new-instance v0, Ljava/net/SocketException;

    const-string v1, "Socket already closed."

    invoke-direct {v0, v1}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_1
    if-nez p1, :cond_2

    .line 61
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 62
    :cond_2
    if-ltz p2, :cond_3

    if-ltz p3, :cond_3

    array-length v1, p1

    sub-int/2addr v1, p2

    if-le p3, v1, :cond_4

    .line 63
    :cond_3
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 64
    :cond_4
    if-nez p3, :cond_6

    .line 81
    :cond_5
    :goto_0
    return v0

    .line 71
    :cond_6
    if-lez p2, :cond_7

    .line 73
    new-array v2, p3, [B

    .line 76
    :goto_1
    :try_start_0
    iget-object v0, p0, Lpcloud/net/b;->a:Lpcloud/net/nat/c;

    iget-object v1, p0, Lpcloud/net/b;->c:Lpcloud/net/nat/a;

    iget v4, p0, Lpcloud/net/b;->b:I

    iget-object v3, p0, Lpcloud/net/b;->d:Lpcloud/net/e;

    invoke-virtual {v3}, Lpcloud/net/e;->a()Z

    move-result v5

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lpcloud/net/nat/c;->a(Lpcloud/net/nat/a;[BIIZ)I

    move-result v0

    .line 77
    if-lez p2, :cond_5

    if-lez v0, :cond_5

    .line 78
    const/4 v1, 0x0

    invoke-static {v2, v1, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trouble reading data on "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lpcloud/net/b;->c:Lpcloud/net/nat/a;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_7
    move-object v2, p1

    goto :goto_1
.end method
