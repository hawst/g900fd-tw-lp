.class final Lpcloud/net/nat/c$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lpcloud/net/nat/c;->h()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lpcloud/net/nat/c;


# direct methods
.method constructor <init>(Lpcloud/net/nat/c;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lpcloud/net/nat/c$1;->a:Lpcloud/net/nat/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 167
    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::initializeInBackground: requested"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 169
    :try_start_0
    iget-object v0, p0, Lpcloud/net/nat/c$1;->a:Lpcloud/net/nat/c;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lpcloud/net/nat/c;->a(ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    iget-object v0, p0, Lpcloud/net/nat/c$1;->a:Lpcloud/net/nat/c;

    iget-object v0, v0, Lpcloud/net/nat/c;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 174
    :goto_0
    return-void

    .line 170
    :catch_0
    move-exception v0

    .line 171
    :try_start_1
    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::initializeInBackground: trouble with initialize: "

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173
    iget-object v0, p0, Lpcloud/net/nat/c$1;->a:Lpcloud/net/nat/c;

    iget-object v0, v0, Lpcloud/net/nat/c;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lpcloud/net/nat/c$1;->a:Lpcloud/net/nat/c;

    iget-object v1, v1, Lpcloud/net/nat/c;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method
