.class final Lpcloud/net/nat/c$4;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lpcloud/net/nat/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/concurrent/CountDownLatch;

.field final synthetic b:Lpcloud/net/nat/c;


# direct methods
.method constructor <init>(Lpcloud/net/nat/c;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 405
    iput-object p1, p0, Lpcloud/net/nat/c$4;->b:Lpcloud/net/nat/c;

    iput-object p2, p0, Lpcloud/net/nat/c$4;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 413
    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::terminateCore: requested"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 415
    :try_start_0
    invoke-static {}, Lpcloud/net/nat/c;->m()Lcom/msc/seclib/SecLibJNI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/msc/seclib/SecLibJNI;->terminateCore()V

    .line 416
    iget-object v0, p0, Lpcloud/net/nat/c$4;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 421
    :goto_0
    return-void

    .line 417
    :catch_0
    move-exception v0

    .line 418
    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::terminateCore: trouble with terminateCore: "

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
