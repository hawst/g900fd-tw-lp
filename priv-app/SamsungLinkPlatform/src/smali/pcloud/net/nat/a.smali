.class public final Lpcloud/net/nat/a;
.super Lcom/msc/seclib/ConnInfo;
.source "SourceFile"


# instance fields
.field private a:J

.field private b:Z

.field private c:Z

.field private final d:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/msc/seclib/ConnInfo;-><init>()V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpcloud/net/nat/a;->b:Z

    .line 30
    iput-boolean p1, p0, Lpcloud/net/nat/a;->d:Z

    .line 31
    return-void
.end method

.method public static c()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lpcloud/net/nat/a;->d:Z

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lpcloud/net/nat/a;->c:Z

    .line 62
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 68
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lpcloud/net/nat/a;->a:J

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lpcloud/net/nat/a;->b:Z

    .line 70
    return-void
.end method

.method public final e()Z
    .locals 8

    .prologue
    const-wide/16 v2, 0x3a98

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 73
    iget-boolean v4, p0, Lpcloud/net/nat/a;->c:Z

    if-eqz v4, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    iget-boolean v4, p0, Lpcloud/net/nat/a;->b:Z

    if-eqz v4, :cond_2

    move v0, v1

    .line 79
    goto :goto_0

    .line 82
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lpcloud/net/nat/a;->a:J

    sub-long/2addr v4, v6

    .line 85
    invoke-virtual {p0}, Lpcloud/net/nat/a;->getConn_type()C

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 87
    :pswitch_1
    const-wide/32 v2, 0xafc8

    .line 100
    :pswitch_2
    cmp-long v2, v4, v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 35
    new-instance v0, Lorg/apache/commons/lang3/builder/ToStringBuilder;

    invoke-direct {v0, p0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;-><init>(Ljava/lang/Object;)V

    const-string v1, "connectionId"

    invoke-virtual {p0}, Lpcloud/net/nat/a;->getConn_id()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;I)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v0

    const-string v1, "remotePeerId"

    invoke-virtual {p0}, Lpcloud/net/nat/a;->getRemote_peer_id()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v0

    const-string v1, "isServerConnection"

    iget-boolean v2, p0, Lpcloud/net/nat/a;->d:Z

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;Z)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v1

    const-string v2, "type"

    invoke-virtual {p0}, Lpcloud/net/nat/a;->getConn_type()C

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "UNKNOWN ("

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lpcloud/net/nat/a;->getConn_type()C

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->append(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/ToStringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/lang3/builder/ToStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_1
    const-string v0, "TCP (1)"

    goto :goto_0

    :pswitch_2
    const-string v0, "UDP (2)"

    goto :goto_0

    :pswitch_3
    const-string v0, "TURN (4)"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
