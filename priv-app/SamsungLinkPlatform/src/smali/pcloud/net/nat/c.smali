.class public abstract Lpcloud/net/nat/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/msc/seclib/SecLibCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpcloud/net/nat/c$a;
    }
.end annotation


# static fields
.field private static final a:Lorg/slf4j/Logger;

.field private static b:Lcom/msc/seclib/SecLibJNI;

.field private static c:Lpcloud/net/nat/c;

.field private static final e:[B


# instance fields
.field private final d:Lorg/apache/commons/pool/KeyedObjectPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<",
            "Ljava/lang/String;",
            "Lpcloud/net/nat/a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected final g:Landroid/content/Context;

.field protected final h:Landroid/net/wifi/WifiManager$WifiLock;

.field protected final i:Ljava/util/concurrent/Semaphore;

.field private final j:Ljava/util/concurrent/ExecutorService;

.field private k:Z

.field private l:Z

.field private m:Ljava/lang/String;

.field private final n:Ljava/util/concurrent/ScheduledExecutorService;

.field private o:I

.field private p:Ljava/lang/Object;

.field private q:Lpcloud/net/nat/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    const-class v0, Lpcloud/net/nat/c;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    .line 51
    sput-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "NetworkTraversal static init"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 54
    invoke-static {}, Lcom/msc/seclib/SecLibJNI;->getInstance()Lcom/msc/seclib/SecLibJNI;

    move-result-object v0

    sput-object v0, Lpcloud/net/nat/c;->b:Lcom/msc/seclib/SecLibJNI;

    .line 55
    const/4 v0, 0x0

    sput-object v0, Lpcloud/net/nat/c;->c:Lpcloud/net/nat/c;

    .line 62
    const/16 v0, 0xa

    new-array v0, v0, [B

    sput-object v0, Lpcloud/net/nat/c;->e:[B

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lpcloud/net/nat/c;->f:Ljava/util/HashMap;

    .line 68
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/nat/c;->j:Ljava/util/concurrent/ExecutorService;

    .line 70
    iput-boolean v4, p0, Lpcloud/net/nat/c;->k:Z

    .line 72
    iput-boolean v4, p0, Lpcloud/net/nat/c;->l:Z

    .line 82
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/nat/c;->n:Ljava/util/concurrent/ScheduledExecutorService;

    .line 84
    iput-object v1, p0, Lpcloud/net/nat/c;->p:Ljava/lang/Object;

    .line 86
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v6}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lpcloud/net/nat/c;->i:Ljava/util/concurrent/Semaphore;

    .line 449
    iput-object v1, p0, Lpcloud/net/nat/c;->q:Lpcloud/net/nat/b;

    .line 97
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "enter ctor"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 98
    if-nez p1, :cond_0

    .line 99
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Network Traversal may not be constructed with a null context"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/nat/c;->g:Landroid/content/Context;

    .line 103
    iget-object v0, p0, Lpcloud/net/nat/c;->g:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 104
    iget-object v0, p0, Lpcloud/net/nat/c;->g:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 107
    const/4 v1, 0x3

    const-string v2, "mfl_NetworkTraversal_HiPerf_WiFi_Lock"

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/nat/c;->h:Landroid/net/wifi/WifiManager$WifiLock;

    .line 108
    iget-object v0, p0, Lpcloud/net/nat/c;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0, v4}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 111
    :try_start_0
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.msc.nts.android.proxy.NTSCProxyService"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/ASPApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_0
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "registering callback"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 118
    invoke-static {p0}, Lcom/msc/seclib/SecLibJNI;->registerCallback(Lcom/msc/seclib/SecLibCallbacks;)V

    .line 119
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "registered callback"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 122
    new-instance v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    new-instance v1, Lpcloud/net/nat/c$a;

    invoke-direct {v1, p0, v4}, Lpcloud/net/nat/c$a;-><init>(Lpcloud/net/nat/c;B)V

    invoke-direct {v0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V

    .line 125
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMaxActive(I)V

    .line 126
    invoke-virtual {v0, v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMaxIdle(I)V

    .line 127
    invoke-virtual {v0, v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMaxTotal(I)V

    .line 128
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMaxWait(J)V

    .line 129
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMinEvictableIdleTimeMillis(J)V

    .line 130
    invoke-virtual {v0, v4}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMinIdle(I)V

    .line 131
    invoke-virtual {v0, v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setNumTestsPerEvictionRun(I)V

    .line 132
    invoke-virtual {v0, v6}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setTestOnBorrow(Z)V

    .line 133
    invoke-virtual {v0, v4}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setTestOnReturn(Z)V

    .line 134
    invoke-virtual {v0, v4}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setTestWhileIdle(Z)V

    .line 135
    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setTimeBetweenEvictionRunsMillis(J)V

    .line 137
    iput-object v0, p0, Lpcloud/net/nat/c;->d:Lorg/apache/commons/pool/KeyedObjectPool;

    .line 138
    return-void

    .line 112
    :catch_0
    move-exception v0

    .line 113
    sget-object v1, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v2, "Failed to startService NTSCProxyService : "

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic a(Lpcloud/net/nat/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lpcloud/net/nat/c;->m:Ljava/lang/String;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lpcloud/net/nat/c;
    .locals 3

    .prologue
    .line 141
    const-class v1, Lpcloud/net/nat/c;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 143
    :try_start_0
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v2, "::getInstance: context is null"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 145
    :cond_0
    sget-object v0, Lpcloud/net/nat/c;->c:Lpcloud/net/nat/c;

    if-nez v0, :cond_1

    .line 146
    new-instance v0, Lcom/mfluent/asp/nts/a;

    invoke-direct {v0, p0}, Lcom/mfluent/asp/nts/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lpcloud/net/nat/c;->c:Lpcloud/net/nat/c;

    .line 148
    :cond_1
    sget-object v0, Lpcloud/net/nat/c;->c:Lpcloud/net/nat/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 920
    iget-boolean v0, p0, Lpcloud/net/nat/c;->k:Z

    if-eq v0, p1, :cond_0

    .line 921
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "setInitialized {}"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 922
    iput-boolean p1, p0, Lpcloud/net/nat/c;->k:Z

    .line 923
    invoke-virtual {p0}, Lpcloud/net/nat/c;->c()V

    .line 926
    :cond_0
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "::setInitialized: updating resource locks"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 927
    iget-boolean v0, p0, Lpcloud/net/nat/c;->k:Z

    if-eqz v0, :cond_2

    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "::updateCPULockState: Acquiring CPU lock"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lpcloud/net/nat/c;->p:Ljava/lang/Object;

    iget-object v1, p0, Lpcloud/net/nat/c;->g:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/util/t;->a(Landroid/content/Context;)Lcom/mfluent/asp/util/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mfluent/asp/util/t;->a()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lpcloud/net/nat/c;->p:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lpcloud/net/nat/c;->g:Landroid/content/Context;

    invoke-static {v1}, Lcom/mfluent/asp/util/t;->a(Landroid/content/Context;)Lcom/mfluent/asp/util/t;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/mfluent/asp/util/t;->a(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lpcloud/net/nat/c;->k()V

    .line 928
    return-void

    .line 927
    :cond_2
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "::updateCPULockState: Releasing CPU lock"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lpcloud/net/nat/c;->p:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lpcloud/net/nat/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/util/t;->a(Landroid/content/Context;)Lcom/mfluent/asp/util/t;

    move-result-object v0

    iget-object v1, p0, Lpcloud/net/nat/c;->p:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/mfluent/asp/util/t;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lpcloud/net/nat/c;->p:Ljava/lang/Object;

    goto :goto_0
.end method

.method static synthetic b(Lpcloud/net/nat/c;)Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lpcloud/net/nat/c;->j:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method private static b(Lpcloud/net/nat/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 687
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 688
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "Invalidating connection {} due to interrupt"

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 689
    invoke-virtual {p0}, Lpcloud/net/nat/a;->b()V

    .line 690
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "Interrupted while attempting to read or write to/from nts"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 692
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 982
    iget-boolean v0, p0, Lpcloud/net/nat/c;->l:Z

    if-eq v0, p1, :cond_0

    .line 983
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "setPresenceConnected {}"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 984
    iput-boolean p1, p0, Lpcloud/net/nat/c;->l:Z

    .line 985
    invoke-virtual {p0}, Lpcloud/net/nat/c;->d()V

    .line 987
    :cond_0
    return-void
.end method

.method public static g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    const-string v0, "c7hc8m4900"

    return-object v0
.end method

.method static synthetic l()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic m()Lcom/msc/seclib/SecLibJNI;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lpcloud/net/nat/c;->b:Lcom/msc/seclib/SecLibJNI;

    return-object v0
.end method

.method static synthetic n()[B
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lpcloud/net/nat/c;->e:[B

    return-object v0
.end method


# virtual methods
.method public a(Lpcloud/net/nat/a;[BIIZ)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x3e8

    .line 695
    invoke-virtual {p1}, Lpcloud/net/nat/a;->getConn_id()I

    move-result v2

    .line 697
    const/4 v1, 0x0

    .line 699
    invoke-static {p1}, Lpcloud/net/nat/c;->b(Lpcloud/net/nat/a;)V

    .line 700
    sget-object v0, Lpcloud/net/nat/c;->b:Lcom/msc/seclib/SecLibJNI;

    invoke-virtual {v0, v2, p2, p3, v4}, Lcom/msc/seclib/SecLibJNI;->recv(I[BII)I

    move-result v0

    .line 701
    :goto_0
    const/4 v3, -0x3

    if-ne v0, v3, :cond_0

    if-ge v1, p4, :cond_0

    .line 702
    add-int/lit16 v1, v1, 0x3e8

    .line 703
    invoke-static {p1}, Lpcloud/net/nat/c;->b(Lpcloud/net/nat/a;)V

    .line 704
    sget-object v0, Lpcloud/net/nat/c;->b:Lcom/msc/seclib/SecLibJNI;

    invoke-virtual {v0, v2, p2, p3, v4}, Lcom/msc/seclib/SecLibJNI;->recv(I[BII)I

    move-result v0

    goto :goto_0

    .line 707
    :cond_0
    if-gez v0, :cond_1

    .line 708
    sparse-switch v0, :sswitch_data_0

    .line 742
    const-string v0, "Unkown"

    .line 745
    :goto_1
    invoke-virtual {p1}, Lpcloud/net/nat/a;->b()V

    .line 746
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 711
    :sswitch_0
    const-string v0, "-1: Invalid Parameter : \uc798\ubabb\ub41c conn_id \uac12 \ub610\ub294 Object Not Found (\uc774\ubbf8 \uc885\ub8cc\ub418\uc5c8\uac70\ub098 \uc798\ubabb\ub41c id\uac12\uc784) \ub610\ub294 buf_size <= 0"

    goto :goto_1

    .line 714
    :sswitch_1
    const-string v0, "-2: Insufficient memmory"

    goto :goto_1

    .line 717
    :sswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "-3: Wait timeout "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 718
    invoke-virtual {p1}, Lpcloud/net/nat/a;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 719
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "SeverSocket(cid: {}) (time_out:{}): Apache Server will close this socket silently::{}"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 720
    const/4 v0, -0x1

    .line 748
    :cond_1
    return v0

    .line 722
    :cond_2
    sget-object v1, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v3, "ClientSocket Recv Error(cid: {}): {}"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v3, v2, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 726
    :sswitch_3
    const-string v0, "-4: Disconnected socket"

    .line 727
    sget-object v1, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v3, "Recv Error(cid: {}): {}"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v3, v2, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 730
    :sswitch_4
    const-string v0, "-5: Socket IO error"

    goto :goto_1

    .line 733
    :sswitch_5
    const-string v0, "-6: Etc error (serialize, \ub610\ub294 \uc0c1\ud0dc \uc624\ub958 \ub4f1)"

    goto :goto_1

    .line 736
    :sswitch_6
    const-string v0, "-7: Parameter error buf_size <= 0 \ub610\ub294 buf_size > MAX_PACKET_SIZE(1GB)"

    goto :goto_1

    .line 739
    :sswitch_7
    const-string v0, "-99: Off-Line"

    goto :goto_1

    .line 708
    nop

    :sswitch_data_0
    .sparse-switch
        -0x63 -> :sswitch_7
        -0x7 -> :sswitch_6
        -0x6 -> :sswitch_5
        -0x5 -> :sswitch_4
        -0x4 -> :sswitch_3
        -0x3 -> :sswitch_2
        -0x2 -> :sswitch_1
        -0x1 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lpcloud/net/nat/a;[BIIZZ)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 638
    if-nez p1, :cond_0

    .line 639
    new-instance v0, Ljava/io/IOException;

    const-string v1, "connectionInfo is null"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 642
    :cond_0
    invoke-virtual {p1}, Lpcloud/net/nat/a;->getConn_id()I

    move-result v1

    .line 644
    invoke-static {p1}, Lpcloud/net/nat/c;->b(Lpcloud/net/nat/a;)V

    .line 646
    sget-object v0, Lpcloud/net/nat/c;->b:Lcom/msc/seclib/SecLibJNI;

    move-object v2, p2

    move v3, p3

    move v4, p5

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/msc/seclib/SecLibJNI;->send(I[BIZI)I

    move-result v0

    .line 648
    if-gez v0, :cond_1

    .line 650
    sparse-switch v0, :sswitch_data_0

    .line 676
    const-string v0, "Unkown"

    .line 679
    :goto_0
    invoke-virtual {p1}, Lpcloud/net/nat/a;->b()V

    .line 680
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 654
    :sswitch_0
    const-string v0, "-1: \uc798\ubabb\ub41c conn_id \uac12 \ub610\ub294 Object Not Found (\uc774\ubbf8 \uc885\ub8cc\ub418\uc5c8\uac70\ub098 \uc798\ubabb\ub41c id\uac12\uc784)"

    goto :goto_0

    .line 657
    :sswitch_1
    const-string v0, "-2: \ud30c\ub77c\ubbf8\ud130 \uc624\ub958 buf_size <= 0 \ub610\ub294 buf_size > MAX_PACKET_SIZE(1GB)"

    goto :goto_0

    .line 660
    :sswitch_2
    const-string v0, "-3: Timeout"

    goto :goto_0

    .line 664
    :sswitch_3
    const-string v0, "-4: Disconnected socket"

    goto :goto_0

    .line 667
    :sswitch_4
    const-string v0, "-5: Socket IO error.. (send() res\uac00 < 0 \uc77c \uacbd\uc6b0)"

    goto :goto_0

    .line 670
    :sswitch_5
    const-string v0, "-6: Etc error (serialize, \ub610\ub294 \uc0c1\ud0dc \uc624\ub958 \ub4f1)"

    goto :goto_0

    .line 673
    :sswitch_6
    const-string v0, "-99: Off-Line"

    goto :goto_0

    .line 683
    :cond_1
    return v0

    .line 650
    nop

    :sswitch_data_0
    .sparse-switch
        -0x63 -> :sswitch_6
        -0x6 -> :sswitch_5
        -0x5 -> :sswitch_4
        -0x4 -> :sswitch_3
        -0x3 -> :sswitch_2
        -0x2 -> :sswitch_1
        -0x1 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Ljava/lang/String;II)Lpcloud/net/nat/a;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 793
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 801
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v2

    int-to-long v4, p3

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    .line 803
    :try_start_0
    iget-object v0, p0, Lpcloud/net/nat/c;->d:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->borrowObject(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpcloud/net/nat/a;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 808
    :catch_0
    move-exception v0

    .line 809
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trouble getting a connection from the pool for peerId "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 813
    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Timed out waiting for a connection from the pool for peerId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 810
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public declared-synchronized a()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 402
    monitor-enter p0

    :try_start_0
    sget-object v2, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v3, "terminateCore"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 404
    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 405
    new-instance v3, Lpcloud/net/nat/c$4;

    invoke-direct {v3, p0, v2}, Lpcloud/net/nat/c$4;-><init>(Lpcloud/net/nat/c;Ljava/util/concurrent/CountDownLatch;)V

    .line 424
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    const-wide/16 v4, 0x5

    :try_start_1
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 427
    :goto_0
    if-eqz v0, :cond_0

    .line 428
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "Time elapsed waiting for terminateCore to complete"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 434
    :cond_0
    :goto_1
    const/4 v0, 0x0

    :try_start_2
    invoke-direct {p0, v0}, Lpcloud/net/nat/c;->a(Z)V

    .line 435
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lpcloud/net/nat/c;->b(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 437
    monitor-exit p0

    return-void

    :cond_1
    move v0, v1

    .line 426
    goto :goto_0

    .line 431
    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 402
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized a(Lcom/msc/seclib/CoreConfig;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/ConnectException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 182
    monitor-enter p0

    :try_start_0
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v3, "initializeCore"

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 183
    invoke-virtual {p1}, Lcom/msc/seclib/CoreConfig;->getGroup_id()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpcloud/net/nat/c;->m:Ljava/lang/String;

    .line 186
    sget-object v0, Lpcloud/net/nat/c;->b:Lcom/msc/seclib/SecLibJNI;

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Ljava/net/ConnectException;

    const-string v1, "no stub library"

    invoke-direct {v0, v1}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 189
    :cond_0
    :try_start_1
    sget-object v0, Lpcloud/net/nat/c;->b:Lcom/msc/seclib/SecLibJNI;

    invoke-virtual {v0, p1}, Lcom/msc/seclib/SecLibJNI;->initializeCore(Lcom/msc/seclib/CoreConfig;)I

    move-result v3

    .line 190
    const/4 v0, -0x8

    if-eq v3, v0, :cond_1

    const/16 v0, -0x19

    if-eq v3, v0, :cond_1

    .line 191
    const/4 v0, 0x0

    iput v0, p0, Lpcloud/net/nat/c;->o:I

    .line 193
    :cond_1
    if-gez v3, :cond_5

    .line 195
    sparse-switch v3, :sswitch_data_0

    .line 381
    const-string v0, "Unkown"

    move v1, v2

    .line 385
    :goto_0
    sget-object v2, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v4, "initializeCore: {}"

    invoke-interface {v2, v4, v0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Object;)V

    .line 387
    if-eqz v1, :cond_2

    .line 388
    invoke-virtual {p0}, Lpcloud/net/nat/c;->a()V

    .line 391
    :cond_2
    iget-boolean v0, p0, Lpcloud/net/nat/c;->l:Z

    if-nez v0, :cond_6

    .line 392
    new-instance v0, Ljava/net/ConnectException;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :sswitch_0
    const-string v0, "-1 : Local IP \uc870\ud68c \uc2e4\ud328 (\ub2e8\ub9d0\ub85c\ubd80\ud130 IP\ub97c \uc5bb\uc5b4\uc62c \uc218 \uc5c6\uc74c)"

    move v1, v2

    .line 202
    goto :goto_0

    .line 204
    :sswitch_1
    const-string v0, "-2 : P2P \uc5f0\uacb0\uc744 \uc704\ud55c \uc11c\ube44\uc2a4 \ud3ec\ud2b8(5050:default) \ubc14\uc778\ub529 \uc2e4\ud328.(\uc774\ubbf8 \ub2e4\ub978 \ud504\ub85c\uc138\uc2a4\uac00 \uc0ac\uc6a9\uc911\uc774\uac70\ub098 android\uc758 \uacbd\uc6b0 \uc18c\ucf13 \uc5d1\uc138\uc2a4 \uad8c\ud55c \uc788\ub294\uc9c0 \ud655\uc778 \ud544\uc694)"

    move v1, v2

    .line 205
    goto :goto_0

    .line 207
    :sswitch_2
    const-string v0, "-3 : Presence/STUN server address is not correct - \uc11c\ubc84\uc758 \uc8fc\uc18c\uac00 \uc62c\ubc14\ub974\uc9c0 \uc54a\uc74c (SCoreConfig\uc758 svr_domain, stun_domain \ud655\uc778)"

    move v1, v2

    .line 208
    goto :goto_0

    .line 210
    :sswitch_3
    const-string v1, "-4 : Unable to connect to presence server - Presence \uc11c\ubc84 \uc5f0\uacb0 \uc2e4\ud328 (STUN \uc11c\ubc84\ub294 \uc5f0\uacb0 \uc2e4\ud328\uc77c \uacbd\uc6b0 skip)"

    .line 211
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mfluent/asp/ASPApplication;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.msc.nts.android.proxy.NTSCProxyService"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/mfluent/asp/ASPApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-object v0, v1

    move v1, v2

    .line 212
    goto :goto_0

    .line 214
    :sswitch_4
    const-string v0, "-5 : \ub85c\uadf8\uc778 \uc2e4\ud328 (Presence \uc11c\ubc84\ub85c\ubd80\ud130 "

    move v1, v2

    .line 215
    goto :goto_0

    .line 217
    :sswitch_5
    const-string v0, "-6 : NAT/\ubc29\ud654\ubcbd \uc815\ubcf4 \ucd94\ucd9c \uc2e4\ud328 (\uc815\ud655\ud558\uac8c\ub294 \uc4f0\ub808\ub4dc \uc0dd\uc131 \uc2e4\ud328)"

    move v1, v2

    .line 218
    goto :goto_0

    .line 220
    :sswitch_6
    const-string v0, "-7 : Misc initialize error - \uae30\ud0c0 \ucd08\uae30\ud654 \uc5d0\ub7ec"

    move v1, v2

    .line 221
    goto :goto_0

    .line 223
    :sswitch_7
    const-string v1, "-8 : Group ID, Peer ID \uac80\uc99d \uc2e4\ud328 (ID\uac00 \uc720\ud6a8\ud558\uc9c0 \uc54a\uac70\ub098 \uc6f9 \uc11c\ube44\uc2a4 \uc811\uc18d \uc2e4\ud328)"

    .line 224
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/a;->c()V

    .line 225
    iget v0, p0, Lpcloud/net/nat/c;->o:I

    if-lt v0, v4, :cond_3

    .line 226
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v4, "::initializeCore: Received -8 - but auto retry count ({}) has been exceeded"

    iget v5, p0, Lpcloud/net/nat/c;->o:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    move-object v0, v1

    move v1, v2

    goto :goto_0

    .line 228
    :cond_3
    iget v0, p0, Lpcloud/net/nat/c;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lpcloud/net/nat/c;->o:I

    .line 229
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v4, "::initializeCore: Received -8 - Will try to initializeCore in {} seconds"

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 231
    iget-object v0, p0, Lpcloud/net/nat/c;->n:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v4, Lpcloud/net/nat/c$2;

    invoke-direct {v4, p0}, Lpcloud/net/nat/c$2;-><init>(Lpcloud/net/nat/c;)V

    const-wide/16 v6, 0xa

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v4, v6, v7, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-object v0, v1

    move v1, v2

    .line 248
    goto/16 :goto_0

    .line 250
    :sswitch_8
    const-string v0, "-9 : Server error, DB Query failed or web service call failed - \uc11c\ubc84 \uc5d0\ub7ec(\uc11c\ubc84\uc5d0\uc11c \uc5d0\ub7ec\ub97c \ubc18\ud658) - DB Query \uc2e4\ud328, \uc6f9 \uc11c\ube44\uc2a4 \ud638\ucd9c \uc2e4\ud328 \ub4f1"

    move v1, v2

    .line 251
    goto/16 :goto_0

    .line 253
    :sswitch_9
    const-string v0, "-10 : Able to login but could not get peer list - \ub85c\uadf8\uc778\uc5d0\ub294 \uc131\uacf5\ud588\uc73c\ub098, \uadf8\ub8f9 \ud53c\uc5b4 \ubaa9\ub85d \uc870\ud68c\uc5d0 \uc2e4\ud328\ud55c \uacbd\uc6b0 \ub9ac\ud134\ud55c\ub2e4. \ubc18\ud658\ud558\uae30 \uc804 Presence \uc11c\ubc84\uc640\uc758 \uc5f0\uacb0\uc744 \uc885\ub8cc\ud55c\ub2e4"

    move v1, v2

    .line 254
    goto/16 :goto_0

    .line 256
    :sswitch_a
    const-string v0, "-20 : SCA \uae30\ud0c0 \uc5d0\ub7ec"

    move v1, v2

    .line 257
    goto/16 :goto_0

    .line 259
    :sswitch_b
    const-string v0, "-21 : SCA API \uc11c\ubc84 GetHostbyname \uc2e4\ud328"

    move v1, v2

    .line 260
    goto/16 :goto_0

    .line 262
    :sswitch_c
    const-string v0, "-22 : SCA API \uc11c\ubc84 TCP \uc5f0\uacb0 \uc2e4\ud328"

    move v1, v2

    .line 263
    goto/16 :goto_0

    .line 265
    :sswitch_d
    const-string v0, "-23 : SCA API \uc11c\ubc84\uc640 \uc554\ud638\ud654(SSL) \uc5f0\uacb0 \uc2e4\ud328"

    move v1, v2

    .line 266
    goto/16 :goto_0

    .line 268
    :sswitch_e
    const-string v0, "-24 : \uc785\ub825\ub41c MCC, CC \uac12\uc774 \ubaa8\ub450 \uc5c6\ub294 \uacbd\uc6b0"

    move v1, v2

    .line 269
    goto/16 :goto_0

    .line 271
    :sswitch_f
    const-string v1, "-25 : SCA \uc11c\ubc84 \uc751\ub2f5 - SSO \uc778\uc99d \uc5d0\ub7ec(1401)"

    .line 272
    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/mfluent/asp/a;->a(Landroid/content/Context;)Lcom/mfluent/asp/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/a;->c()V

    .line 273
    iget v0, p0, Lpcloud/net/nat/c;->o:I

    if-lt v0, v4, :cond_4

    .line 274
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v4, "::initializeCore: Received -25 - but auto retry count ({}) has been exceeded"

    iget v5, p0, Lpcloud/net/nat/c;->o:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    move-object v0, v1

    move v1, v2

    goto/16 :goto_0

    .line 276
    :cond_4
    iget v0, p0, Lpcloud/net/nat/c;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lpcloud/net/nat/c;->o:I

    .line 277
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v4, "::initializeCore: Received -25 - Will try to initializeCore in {} seconds"

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 279
    iget-object v0, p0, Lpcloud/net/nat/c;->n:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v4, Lpcloud/net/nat/c$3;

    invoke-direct {v4, p0}, Lpcloud/net/nat/c$3;-><init>(Lpcloud/net/nat/c;)V

    const-wide/16 v6, 0xa

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v4, v6, v7, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-object v0, v1

    move v1, v2

    .line 297
    goto/16 :goto_0

    .line 299
    :sswitch_10
    const-string v0, "-26 : SCA \uc11c\ubc84 \uc751\ub2f5 - X-SCS-SSO \ud5e4\ub354 \uc5d0\ub7ec (1402)"

    move v1, v2

    .line 300
    goto/16 :goto_0

    .line 302
    :sswitch_11
    const-string v0, "-27 : SCA \uc11c\ubc84 \uc751\ub2f5 - HTTP Body \uba54\uc2dc\uc9c0 \uc5d0\ub7ec (1403)"

    move v1, v2

    .line 303
    goto/16 :goto_0

    .line 305
    :sswitch_12
    const-string v0, "-28 : SCA \uc11c\ubc84 \uc751\ub2f5 - MCC, CC\uac12\uc774 \ubaa8\ub450 \uc5c6\ub294 \uacbd\uc6b0 (1404)"

    move v1, v2

    .line 306
    goto/16 :goto_0

    .line 308
    :sswitch_13
    const-string v0, "-29 : SCA \uc11c\ubc84 \uc751\ub2f5 - SSO \uac12\uc774 \uc5c6\ub294 \uacbd\uc6b0 \uc5d0\ub7ec\ucf54\ub4dc (1405)"

    move v1, v2

    .line 309
    goto/16 :goto_0

    .line 311
    :sswitch_14
    const-string v0, "-30 : SCA \uc11c\ubc84 \uc751\ub2f5 - Http\ub85c \uc694\uccad\ud55c \uacbd\uc6b0 (1406)"

    move v1, v2

    .line 312
    goto/16 :goto_0

    .line 314
    :sswitch_15
    const-string v0, "-31 : SCA \uc798\ubabb\ub41c Parameter"

    move v1, v2

    .line 315
    goto/16 :goto_0

    .line 317
    :sswitch_16
    const-string v0, "-32 : SCA \uc11c\ubc84 \uc1a1\uc2de \uc2e4\ud328"

    move v1, v2

    .line 318
    goto/16 :goto_0

    .line 320
    :sswitch_17
    const-string v0, "-33 : SCA \uc11c\ubc84 \uc751\ub2f5 \uc218\uc2de \uc2e4\ud328"

    move v1, v2

    .line 321
    goto/16 :goto_0

    .line 323
    :sswitch_18
    const-string v0, "-34 : \uc798\ubabb\ub41c Server Type"

    move v1, v2

    .line 324
    goto/16 :goto_0

    .line 326
    :sswitch_19
    const-string v0, "-35 : SCA \uc11c\ubc84 \uc751\ub2f5 \u2013 \uc785\ub825\ub41c MCC, CC \uac12\uc758 \ud574\ub2f9\ub418\ub294 \uac12\uc774 DB\uc5d0 \uc5c6\ub294 \uacbd\uc6b0 (1407)"

    move v1, v2

    .line 327
    goto/16 :goto_0

    .line 329
    :sswitch_1a
    const-string v0, "-36 : SCA \uc11c\ubc84 \uc751\ub2f5 \u2013 \ub4f1\ub85d\ub418\uc9c0 \uc54a\uc740 PeerID, GroupID (1408)"

    move v1, v2

    .line 330
    goto/16 :goto_0

    .line 332
    :sswitch_1b
    const-string v0, "-37 : SSL Initialize\uc911 \uc5d0\ub7ec"

    move v1, v2

    .line 333
    goto/16 :goto_0

    .line 336
    :sswitch_1c
    const-string v0, "-100 : SCS_Proxy \uc5f0\uacb0 \uc2e4\ud328..(Stub)"

    move v1, v2

    .line 337
    goto/16 :goto_0

    .line 339
    :sswitch_1d
    const-string v0, "-101 : lock \ud68d\ub4dd \uc2e4\ud328.(Stub)"

    move v1, v2

    .line 340
    goto/16 :goto_0

    .line 342
    :sswitch_1e
    const-string v0, "-102 : \uc4f0\ub808\ub4dc \uc0dd\uc131\uc2e4\ud328 (Stub)"

    move v1, v2

    .line 343
    goto/16 :goto_0

    .line 345
    :sswitch_1f
    const-string v0, "-103 : \uc18c\ucf13 IO \uc5d0\ub7ec. (Stub)\ud328"

    move v1, v2

    .line 346
    goto/16 :goto_0

    .line 348
    :sswitch_20
    const-string v0, "-104 : \uba54\uc138\uc9c0 serialize/deserialize \uc2e4\ud328. (Stub)"

    move v1, v2

    .line 349
    goto/16 :goto_0

    .line 351
    :sswitch_21
    const-string v0, "-105 : \uc751\ub2f5\uba54\uc2dc\uc9c0 \ub300\uae30 \ud0c0\uc784\uc544\uc6c3. (Stub)"

    move v1, v2

    .line 352
    goto/16 :goto_0

    .line 354
    :sswitch_22
    const-string v0, "-106 : \uba54\uc2dc\uc9c0 \ub9f5 \ubc14\uc778\ub4dc \uc624\ub958. (Stub)"

    move v1, v2

    .line 355
    goto/16 :goto_0

    .line 357
    :sswitch_23
    const-string v0, "-110 : \uc798\ubabb\ub41c \ud53c\uc5b4 ID - \ub2e4\ub978 app\uc758 peer id\uc640 \ub2e4\ub984. (Stub)"

    move v1, v2

    .line 358
    goto/16 :goto_0

    .line 360
    :sswitch_24
    const-string v0, "-121 : \ub3d9\uc77c \uadf8\ub8f9\uc5d0 \ub3d9\uc77c\ud55c instance_id\uac00 \uc774\ubbf8 \uc788\uc74c. (Stub)"

    move v1, v2

    .line 361
    goto/16 :goto_0

    .line 364
    :sswitch_25
    const-string v0, "-98 : Online already - \uc774\ubbf8 \uc628\ub77c\uc784 \uc784"

    .line 370
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lpcloud/net/nat/c;->a(Z)V

    .line 371
    sget-object v2, Lpcloud/net/nat/c;->b:Lcom/msc/seclib/SecLibJNI;

    invoke-virtual {v2}, Lcom/msc/seclib/SecLibJNI;->getIsLoggedin()Z

    move-result v2

    invoke-direct {p0, v2}, Lpcloud/net/nat/c;->b(Z)V

    goto/16 :goto_0

    .line 375
    :sswitch_26
    const-string v0, "-99 : Cannot call this API in current state (core initialized already) - \ud604\uc7ac status\uc5d0\uc11c\ub294 \ud574\ub2f9 API\ub97c \uc2e4\ud589\ud560 \uc218 \uc5c6\uc74c (\uc774\ubbf8 \ucd08\uae30\ud654 \ud55c \uc0c1\ud0dc\uc5d0\uc11c \ub2e4\uc2dc \ucd08\uae30\ud654\ub97c \ud558\ub294 \uacbd\uc6b0 \ub4f1)"

    goto/16 :goto_0

    .line 395
    :cond_5
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lpcloud/net/nat/c;->a(Z)V

    .line 396
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lpcloud/net/nat/c;->b(Z)V

    .line 397
    invoke-static {p1}, Lcom/mfluent/asp/util/r;->a(Lcom/msc/seclib/CoreConfig;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 399
    :cond_6
    monitor-exit p0

    return-void

    .line 195
    :sswitch_data_0
    .sparse-switch
        -0x79 -> :sswitch_24
        -0x6e -> :sswitch_23
        -0x6a -> :sswitch_22
        -0x69 -> :sswitch_21
        -0x68 -> :sswitch_20
        -0x67 -> :sswitch_1f
        -0x66 -> :sswitch_1e
        -0x65 -> :sswitch_1d
        -0x64 -> :sswitch_1c
        -0x63 -> :sswitch_26
        -0x62 -> :sswitch_25
        -0x25 -> :sswitch_1b
        -0x24 -> :sswitch_1a
        -0x23 -> :sswitch_19
        -0x22 -> :sswitch_18
        -0x21 -> :sswitch_17
        -0x20 -> :sswitch_16
        -0x1f -> :sswitch_15
        -0x1e -> :sswitch_14
        -0x1d -> :sswitch_13
        -0x1c -> :sswitch_12
        -0x1b -> :sswitch_11
        -0x1a -> :sswitch_10
        -0x19 -> :sswitch_f
        -0x18 -> :sswitch_e
        -0x17 -> :sswitch_d
        -0x16 -> :sswitch_c
        -0x15 -> :sswitch_b
        -0x14 -> :sswitch_a
        -0xa -> :sswitch_9
        -0x9 -> :sswitch_8
        -0x8 -> :sswitch_7
        -0x7 -> :sswitch_6
        -0x6 -> :sswitch_5
        -0x5 -> :sswitch_4
        -0x4 -> :sswitch_3
        -0x3 -> :sswitch_2
        -0x2 -> :sswitch_1
        -0x1 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Ljava/util/Vector;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Lcom/msc/seclib/PeerInfo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 540
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enter ::getInstancePeerList(): PeerInfoList:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 542
    sget-object v0, Lpcloud/net/nat/c;->b:Lcom/msc/seclib/SecLibJNI;

    iget-object v1, p0, Lpcloud/net/nat/c;->m:Ljava/lang/String;

    const-string v2, "c7hc8m4900"

    invoke-virtual {v0, v1, v2, p1}, Lcom/msc/seclib/SecLibJNI;->getInstancePeerList(Ljava/lang/String;Ljava/lang/String;Ljava/util/Vector;)I

    move-result v0

    .line 544
    if-gez v0, :cond_0

    .line 545
    sparse-switch v0, :sswitch_data_0

    .line 555
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : Unkown"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 558
    :goto_0
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 549
    :sswitch_0
    const-string v0, "-2 : Insufficient memmory (malloc error)"

    goto :goto_0

    .line 552
    :sswitch_1
    const-string v0, "-99 : Off-Line"

    goto :goto_0

    .line 560
    :cond_0
    return-void

    .line 545
    nop

    :sswitch_data_0
    .sparse-switch
        -0x63 -> :sswitch_1
        -0x2 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Lpcloud/net/nat/a;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 817
    if-nez p1, :cond_1

    .line 818
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "returnConnection: ConnectionToken is null"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 833
    :cond_0
    :goto_0
    return-void

    .line 822
    :cond_1
    invoke-virtual {p1}, Lpcloud/net/nat/a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 824
    :try_start_0
    invoke-virtual {p1}, Lpcloud/net/nat/a;->getConn_id()I

    move-result v1

    iget-object v0, p0, Lpcloud/net/nat/c;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lpcloud/net/nat/c$6;

    invoke-direct {v2, p0, v1}, Lpcloud/net/nat/c$6;-><init>(Lpcloud/net/nat/c;I)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const-wide/16 v2, 0x1

    :try_start_1
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v4}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    :try_start_2
    sget-object v2, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v3, "Close the Socket (ID: {})"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    if-gez v0, :cond_0

    sparse-switch v0, :sswitch_data_0

    const-string v0, "Unkown"

    :goto_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 826
    :catch_0
    move-exception v0

    goto :goto_0

    .line 824
    :catch_1
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    new-instance v0, Ljava/io/InterruptedIOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Interrupted while attempting to close "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_2
    move-exception v0

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Trouble closing "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :sswitch_0
    const-string v0, "-1: Object Not Found (\uc774\ubbf8 \uc885\ub8cc\ub418\uc5c8\uac70\ub098 \uc798\ubabb\ub41c id\uac12\uc784)"

    goto :goto_1

    :sswitch_1
    const-string v0, "-2: Error in close"

    goto :goto_1

    :sswitch_2
    const-string v0, "-99: Off-Line"
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 829
    :cond_2
    :try_start_3
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "returnConnection::ClientSocket returning to the pool::{}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 830
    iget-object v0, p0, Lpcloud/net/nat/c;->d:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-virtual {p1}, Lpcloud/net/nat/a;->getRemote_peer_id()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->returnObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 832
    :catch_3
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "returnConnection::Trouble returning "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to the pool"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 824
    nop

    :sswitch_data_0
    .sparse-switch
        -0x63 -> :sswitch_2
        -0x2 -> :sswitch_1
        -0x1 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Lpcloud/net/nat/b;)V
    .locals 0

    .prologue
    .line 452
    iput-object p1, p0, Lpcloud/net/nat/c;->q:Lpcloud/net/nat/b;

    .line 453
    return-void
.end method

.method public abstract a(ZZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract b()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 979
    return-void
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 990
    return-void
.end method

.method public groupPeerStatusNotify(Lcom/msc/seclib/PeerInfo;)I
    .locals 1

    .prologue
    .line 459
    const/4 v0, 0x0

    return v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 161
    iget-boolean v0, p0, Lpcloud/net/nat/c;->k:Z

    if-nez v0, :cond_0

    .line 162
    iget-object v0, p0, Lpcloud/net/nat/c;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lpcloud/net/nat/c;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lpcloud/net/nat/c$1;

    invoke-direct {v1, p0}, Lpcloud/net/nat/c$1;-><init>(Lpcloud/net/nat/c;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 179
    :cond_0
    return-void
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 440
    iget-boolean v0, p0, Lpcloud/net/nat/c;->k:Z

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 444
    iget-boolean v0, p0, Lpcloud/net/nat/c;->l:Z

    return v0
.end method

.method protected final k()V
    .locals 2

    .prologue
    .line 968
    invoke-static {}, Lcom/mfluent/asp/datamodel/t;->a()Lcom/mfluent/asp/datamodel/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/t;->c()Lcom/mfluent/asp/datamodel/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mfluent/asp/datamodel/Device;->k()Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 969
    :goto_0
    iget-boolean v1, p0, Lpcloud/net/nat/c;->k:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 970
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "::updateWifiLockState: Acquiring Wifi Lock"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 971
    iget-object v0, p0, Lpcloud/net/nat/c;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 976
    :goto_1
    return-void

    .line 968
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 973
    :cond_1
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "::updateWifiLockState: Releasing Wifi Lock"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 974
    iget-object v0, p0, Lpcloud/net/nat/c;->h:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_1
.end method

.method public peerConnNotify(Lcom/msc/seclib/PeerInfo;IC)I
    .locals 2

    .prologue
    .line 464
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "peerConnNotify {}"

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    .line 466
    invoke-virtual {p0, p1}, Lpcloud/net/nat/c;->groupPeerStatusNotify(Lcom/msc/seclib/PeerInfo;)I

    .line 468
    iget-object v0, p0, Lpcloud/net/nat/c;->q:Lpcloud/net/nat/b;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lpcloud/net/nat/c;->q:Lpcloud/net/nat/b;

    invoke-interface {v0, p1, p2, p3}, Lpcloud/net/nat/b;->a(Lcom/msc/seclib/PeerInfo;IC)I

    move-result v0

    .line 471
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public presConnNotify()V
    .locals 2

    .prologue
    .line 485
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "presConnNotify"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 486
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lpcloud/net/nat/c;->b(Z)V

    .line 487
    return-void
.end method

.method public presDisconnNotify(I)I
    .locals 5

    .prologue
    .line 491
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "presDisconnNotify {}"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Object;)V

    .line 493
    invoke-virtual {p0}, Lpcloud/net/nat/c;->a()V

    .line 497
    iget-object v0, p0, Lpcloud/net/nat/c;->n:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lpcloud/net/nat/c$5;

    invoke-direct {v1, p0}, Lpcloud/net/nat/c$5;-><init>(Lpcloud/net/nat/c;)V

    const-wide/16 v2, 0x2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 509
    const/4 v0, -0x1

    return v0
.end method

.method public terminateNotify()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 476
    sget-object v0, Lpcloud/net/nat/c;->a:Lorg/slf4j/Logger;

    const-string v1, "terminateNotify"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 478
    invoke-direct {p0, v2}, Lpcloud/net/nat/c;->a(Z)V

    .line 479
    invoke-direct {p0, v2}, Lpcloud/net/nat/c;->b(Z)V

    .line 480
    return v2
.end method
