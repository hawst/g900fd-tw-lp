.class final Lpcloud/net/nat/c$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/KeyedPoolableObjectFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpcloud/net/nat/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
        "<",
        "Ljava/lang/String;",
        "Lpcloud/net/nat/a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lpcloud/net/nat/c;


# direct methods
.method private constructor <init>(Lpcloud/net/nat/c;)V
    .locals 0

    .prologue
    .line 847
    iput-object p1, p0, Lpcloud/net/nat/c$a;->a:Lpcloud/net/nat/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lpcloud/net/nat/c;B)V
    .locals 0

    .prologue
    .line 847
    invoke-direct {p0, p1}, Lpcloud/net/nat/c$a;-><init>(Lpcloud/net/nat/c;)V

    return-void
.end method


# virtual methods
.method public final synthetic activateObject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 847
    check-cast p2, Lpcloud/net/nat/a;

    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "pool activateObject {}"

    invoke-interface {v0, v1, p2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-static {}, Lpcloud/net/nat/a;->c()V

    return-void
.end method

.method public final synthetic destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 847
    check-cast p2, Lpcloud/net/nat/a;

    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "pool destroyObject {}"

    invoke-interface {v0, v1, p2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lpcloud/net/nat/c$a;->a:Lpcloud/net/nat/c;

    invoke-static {v0}, Lpcloud/net/nat/c;->b(Lpcloud/net/nat/c;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lpcloud/net/nat/c$a$1;

    invoke-direct {v1, p0, p2}, Lpcloud/net/nat/c$a$1;-><init>(Lpcloud/net/nat/c$a;Lpcloud/net/nat/a;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final synthetic makeObject(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 847
    check-cast p1, Ljava/lang/String;

    new-instance v0, Lpcloud/net/nat/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lpcloud/net/nat/a;-><init>(Z)V

    invoke-virtual {v0, p1}, Lpcloud/net/nat/a;->setRemote_peer_id(Ljava/lang/String;)V

    invoke-static {}, Lpcloud/net/nat/c;->m()Lcom/msc/seclib/SecLibJNI;

    move-result-object v1

    iget-object v2, p0, Lpcloud/net/nat/c$a;->a:Lpcloud/net/nat/c;

    invoke-static {v2}, Lpcloud/net/nat/c;->a(Lpcloud/net/nat/c;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "c7hc8m4900"

    invoke-virtual {v0}, Lpcloud/net/nat/a;->getRemote_peer_id()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/msc/seclib/SecLibJNI;->connect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/seclib/ConnInfo;)I

    move-result v1

    const/4 v2, -0x6

    if-ne v1, v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create connection for peerId "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": result was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-gez v1, :cond_1

    new-instance v0, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create connection for peerId "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": result was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "pool makeObject {}"

    invoke-interface {v1, v2, v0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final synthetic passivateObject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 847
    check-cast p2, Lpcloud/net/nat/a;

    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "pool passivateObject {}"

    invoke-interface {v0, v1, p2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p2}, Lpcloud/net/nat/a;->d()V

    return-void
.end method

.method public final synthetic validateObject(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 847
    check-cast p2, Lpcloud/net/nat/a;

    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "pool validateObject {}"

    invoke-interface {v2, v3, p2}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p2}, Lpcloud/net/nat/a;->e()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "pool Invalidating {} because isValid returned false"

    invoke-interface {v1, v2, p2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lpcloud/net/nat/c;->m()Lcom/msc/seclib/SecLibJNI;

    move-result-object v2

    invoke-virtual {p2}, Lpcloud/net/nat/a;->getConn_id()I

    move-result v3

    invoke-static {}, Lpcloud/net/nat/c;->n()[B

    move-result-object v4

    invoke-static {}, Lpcloud/net/nat/c;->n()[B

    move-result-object v5

    array-length v5, v5

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/msc/seclib/SecLibJNI;->recv(I[BII)I

    move-result v2

    const/4 v3, -0x3

    if-eq v2, v3, :cond_1

    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v3, "pool Invalidating {} because recv check returned {}"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v3, p2, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v3, "pool Validated {} because recv check returned {}"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v3, p2, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0
.end method
