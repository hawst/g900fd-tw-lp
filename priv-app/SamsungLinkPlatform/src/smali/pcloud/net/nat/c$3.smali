.class final Lpcloud/net/nat/c$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lpcloud/net/nat/c;->a(Lcom/msc/seclib/CoreConfig;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lpcloud/net/nat/c;


# direct methods
.method constructor <init>(Lpcloud/net/nat/c;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lpcloud/net/nat/c$3;->a:Lpcloud/net/nat/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 283
    invoke-static {}, Lcom/mfluent/asp/NTSLockService;->a()Z

    move-result v0

    .line 284
    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "::run: Trying to reinitialize core after -25 if still bound isBound={}"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 286
    if-eqz v0, :cond_0

    .line 287
    :try_start_0
    iget-object v0, p0, Lpcloud/net/nat/c$3;->a:Lpcloud/net/nat/c;

    invoke-virtual {v0}, Lpcloud/net/nat/c;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 290
    :catch_0
    move-exception v0

    invoke-static {}, Lpcloud/net/nat/c;->l()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "::run: Trouble initializing core in -25 retry runnable"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method
