.class public final Lpcloud/net/a/b/a;
.super Lorg/apache/http/impl/conn/DefaultClientConnectionOperator;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lorg/apache/http/conn/scheme/SchemeRegistry;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lorg/apache/http/impl/conn/DefaultClientConnectionOperator;-><init>(Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 27
    return-void
.end method


# virtual methods
.method public final openConnection(Lorg/apache/http/conn/OperatedClientConnection;Lorg/apache/http/HttpHost;Ljava/net/InetAddress;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/params/HttpParams;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    if-nez p1, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Connection may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    if-nez p2, :cond_1

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Target host may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_1
    if-nez p5, :cond_2

    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parameters may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_2
    invoke-interface {p1}, Lorg/apache/http/conn/OperatedClientConnection;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 46
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Connection must not be open"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_3
    iget-object v0, p0, Lpcloud/net/a/b/a;->schemeRegistry:Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-virtual {p2}, Lorg/apache/http/HttpHost;->getSchemeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->getScheme(Ljava/lang/String;)Lorg/apache/http/conn/scheme/Scheme;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lorg/apache/http/conn/scheme/Scheme;->getSocketFactory()Lorg/apache/http/conn/scheme/SocketFactory;

    move-result-object v0

    check-cast v0, Lpcloud/net/a/b/a/a;

    .line 53
    const-string v1, "markActivity"

    const/4 v2, 0x1

    invoke-interface {p5, v1, v2}, Lorg/apache/http/params/HttpParams;->getBooleanParameter(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 55
    const-string v1, "destroySignal"

    invoke-interface {p5, v1}, Lorg/apache/http/params/HttpParams;->getParameter(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpcloud/net/a;

    .line 56
    const-string v2, "peerId"

    invoke-interface {p5, v2}, Lorg/apache/http/params/HttpParams;->getParameter(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lpcloud/net/a/b/a/a;->a(Ljava/lang/String;Ljava/lang/Boolean;Lpcloud/net/a;)Ljava/net/Socket;

    move-result-object v2

    .line 57
    invoke-interface {p1, v2, p2}, Lorg/apache/http/conn/OperatedClientConnection;->opening(Ljava/net/Socket;Lorg/apache/http/HttpHost;)V

    .line 60
    :try_start_0
    invoke-static {v2, p5}, Lpcloud/net/a/b/a/a;->a(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)Ljava/net/Socket;
    :try_end_0
    .catch Lpcloud/net/nat/TurnNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v1

    .line 61
    if-eq v2, v1, :cond_9

    .line 63
    :try_start_1
    invoke-interface {p1, v1, p2}, Lorg/apache/http/conn/OperatedClientConnection;->opening(Ljava/net/Socket;Lorg/apache/http/HttpHost;)V

    .line 65
    :goto_0
    invoke-virtual {p0, v1, p4, p5}, Lpcloud/net/a/b/a;->prepareSocket(Ljava/net/Socket;Lorg/apache/http/protocol/HttpContext;Lorg/apache/http/params/HttpParams;)V

    .line 66
    invoke-virtual {v0, v1}, Lpcloud/net/a/b/a/a;->isSecure(Ljava/net/Socket;)Z

    move-result v0

    invoke-interface {p1, v0, p5}, Lorg/apache/http/conn/OperatedClientConnection;->openCompleted(ZLorg/apache/http/params/HttpParams;)V
    :try_end_1
    .catch Lpcloud/net/nat/TurnNotSupportedException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/net/ConnectException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    .line 92
    return-void

    .line 67
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 68
    :goto_1
    if-eqz v1, :cond_4

    .line 69
    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 71
    :cond_4
    throw v0

    .line 72
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 73
    :goto_2
    if-eqz v1, :cond_5

    .line 74
    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 76
    :cond_5
    new-instance v1, Lorg/apache/http/conn/HttpHostConnectException;

    invoke-direct {v1, p2, v0}, Lorg/apache/http/conn/HttpHostConnectException;-><init>(Lorg/apache/http/HttpHost;Ljava/net/ConnectException;)V

    throw v1

    .line 77
    :catch_2
    move-exception v0

    move-object v1, v2

    .line 78
    :goto_3
    if-eqz v1, :cond_6

    .line 79
    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 81
    :cond_6
    throw v0

    .line 82
    :catch_3
    move-exception v0

    move-object v1, v2

    .line 83
    :goto_4
    if-eqz v1, :cond_7

    .line 84
    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 86
    :cond_7
    throw v0

    .line 88
    :catch_4
    move-exception v0

    move-object v1, v2

    :goto_5
    if-eqz v1, :cond_8

    .line 89
    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 91
    :cond_8
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unknown"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :catch_5
    move-exception v0

    goto :goto_5

    .line 82
    :catch_6
    move-exception v0

    goto :goto_4

    .line 77
    :catch_7
    move-exception v0

    goto :goto_3

    .line 72
    :catch_8
    move-exception v0

    goto :goto_2

    .line 67
    :catch_9
    move-exception v0

    goto :goto_1

    :cond_9
    move-object v1, v2

    goto :goto_0
.end method
