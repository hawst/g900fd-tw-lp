.class public final Lpcloud/net/a/b/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/conn/scheme/SocketFactory;


# static fields
.field private static a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/mfluent/asp/common/util/AspLogLevels;->LOGLEVEL_NTS:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    sput-object v0, Lpcloud/net/a/b/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Boolean;Lpcloud/net/a;)Ljava/net/Socket;
    .locals 3

    .prologue
    .line 83
    :try_start_0
    new-instance v1, Lpcloud/net/e;

    const-class v0, Lcom/mfluent/asp/ASPApplication;

    invoke-static {v0}, Lcom/mfluent/asp/b;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-nez p1, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-direct {v1, v0, p0, v2, p2}, Lpcloud/net/e;-><init>(Landroid/content/Context;Ljava/lang/String;ZLpcloud/net/a;)V

    move-object v0, v1

    .line 94
    :goto_1
    return-object v0

    .line 83
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    sget-object v1, Lpcloud/net/a/b/a/a;->a:Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;

    invoke-virtual {v1}, Lcom/mfluent/asp/common/util/AspLogLevels$LogLevel;->value()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_1

    .line 92
    const-string v1, "mfl_nts_NtclSchemeSocketFactory"

    const-string v2, "::createSocket:"

    invoke-static {v1, v2, v0}, Lcom/sec/pcw/util/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)Ljava/net/Socket;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-static {p1}, Lorg/apache/http/params/HttpConnectionParams;->getConnectionTimeout(Lorg/apache/http/params/HttpParams;)I

    move-result v2

    .line 143
    invoke-static {p1}, Lorg/apache/http/params/HttpConnectionParams;->getSoTimeout(Lorg/apache/http/params/HttpParams;)I

    move-result v1

    .line 146
    :try_start_0
    invoke-virtual {p0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 147
    if-lez v2, :cond_0

    .line 148
    move-object v0, p0

    check-cast v0, Lpcloud/net/e;

    move-object v1, v0

    invoke-virtual {v1, v2}, Lpcloud/net/e;->a(I)V

    .line 165
    :goto_0
    return-object p0

    .line 150
    :cond_0
    move-object v0, p0

    check-cast v0, Lpcloud/net/e;

    move-object v1, v0

    invoke-virtual {v1}, Lpcloud/net/e;->b()V
    :try_end_0
    .catch Lpcloud/net/nat/TurnNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 152
    :catch_0
    move-exception v1

    throw v1

    .line 154
    :catch_1
    move-exception v1

    throw v1

    .line 157
    :catch_2
    move-exception v1

    .line 159
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 160
    :catch_3
    move-exception v1

    throw v1
.end method

.method public static a()Lpcloud/net/a/b/a/a;
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lpcloud/net/a/b/a/a;

    invoke-direct {v0}, Lpcloud/net/a/b/a/a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final connectSocket(Ljava/net/Socket;Ljava/lang/String;ILjava/net/InetAddress;ILorg/apache/http/params/HttpParams;)Ljava/net/Socket;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/UnknownHostException;,
            Lorg/apache/http/conn/ConnectTimeoutException;
        }
    .end annotation

    .prologue
    .line 202
    const/4 v0, 0x0

    return-object v0
.end method

.method public final createSocket()Ljava/net/Socket;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return-object v0
.end method

.method public final isSecure(Ljava/net/Socket;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 182
    if-nez p1, :cond_0

    .line 183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Socket may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    invoke-virtual {p1}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Socket is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
