.class public Lorg/apache/commons/pool/impl/GenericObjectPool;
.super Lorg/apache/commons/pool/BaseObjectPool;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/ObjectPool;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/pool/impl/GenericObjectPool$1;,
        Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;,
        Lorg/apache/commons/pool/impl/GenericObjectPool$Config;,
        Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/commons/pool/BaseObjectPool",
        "<TT;>;",
        "Lorg/apache/commons/pool/ObjectPool",
        "<TT;>;"
    }
.end annotation


# static fields
.field public static final DEFAULT_LIFO:Z = true

.field public static final DEFAULT_MAX_ACTIVE:I = 0x8

.field public static final DEFAULT_MAX_IDLE:I = 0x8

.field public static final DEFAULT_MAX_WAIT:J = -0x1L

.field public static final DEFAULT_MIN_EVICTABLE_IDLE_TIME_MILLIS:J = 0x1b7740L

.field public static final DEFAULT_MIN_IDLE:I = 0x0

.field public static final DEFAULT_NUM_TESTS_PER_EVICTION_RUN:I = 0x3

.field public static final DEFAULT_SOFT_MIN_EVICTABLE_IDLE_TIME_MILLIS:J = -0x1L

.field public static final DEFAULT_TEST_ON_BORROW:Z = false

.field public static final DEFAULT_TEST_ON_RETURN:Z = false

.field public static final DEFAULT_TEST_WHILE_IDLE:Z = false

.field public static final DEFAULT_TIME_BETWEEN_EVICTION_RUNS_MILLIS:J = -0x1L

.field public static final DEFAULT_WHEN_EXHAUSTED_ACTION:B = 0x1t

.field public static final WHEN_EXHAUSTED_BLOCK:B = 0x1t

.field public static final WHEN_EXHAUSTED_FAIL:B = 0x0t

.field public static final WHEN_EXHAUSTED_GROW:B = 0x2t


# instance fields
.field private final _allocationQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/commons/pool/impl/GenericObjectPool$Latch",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private _evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<TT;>;>.Cursor;"
        }
    .end annotation
.end field

.field private _evictor:Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/GenericObjectPool",
            "<TT;>.Evictor;"
        }
    .end annotation
.end field

.field private _factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;"
        }
    .end annotation
.end field

.field private _lifo:Z

.field private _maxActive:I

.field private _maxIdle:I

.field private _maxWait:J

.field private _minEvictableIdleTimeMillis:J

.field private _minIdle:I

.field private _numActive:I

.field private _numInternalProcessing:I

.field private _numTestsPerEvictionRun:I

.field private _pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private _softMinEvictableIdleTimeMillis:J

.field private volatile _testOnBorrow:Z

.field private volatile _testOnReturn:Z

.field private _testWhileIdle:Z

.field private _timeBetweenEvictionRunsMillis:J

.field private _whenExhaustedAction:B


# direct methods
.method public constructor <init>()V
    .locals 16

    .prologue
    .line 344
    const/4 v1, 0x0

    const/16 v2, 0x8

    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 347
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 354
    const/16 v2, 0x8

    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 357
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;I)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 377
    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 380
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJ)V"
        }
    .end annotation

    .prologue
    .line 391
    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 394
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJI)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJI)V"
        }
    .end annotation

    .prologue
    .line 425
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 428
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJIIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 503
    const-wide/16 v16, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-wide/from16 v10, p10

    move/from16 v12, p12

    move-wide/from16 v13, p13

    move/from16 v15, p15

    invoke-direct/range {v0 .. v17}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJ)V

    .line 506
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJ)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJIIZZJIJZJ)V"
        }
    .end annotation

    .prologue
    .line 538
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-wide/from16 v10, p10

    move/from16 v12, p12

    move-wide/from16 v13, p13

    move/from16 v15, p15

    move-wide/from16 v16, p16

    invoke-direct/range {v0 .. v18}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJZ)V

    .line 541
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJIIZZJIJZJZ)V"
        }
    .end annotation

    .prologue
    .line 574
    invoke-direct {p0}, Lorg/apache/commons/pool/BaseObjectPool;-><init>()V

    .line 1918
    const/16 v2, 0x8

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxIdle:I

    .line 1925
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_minIdle:I

    .line 1932
    const/16 v2, 0x8

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxActive:I

    .line 1950
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxWait:J

    .line 1964
    const/4 v2, 0x1

    iput-byte v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_whenExhaustedAction:B

    .line 1977
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testOnBorrow:Z

    .line 1988
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testOnReturn:Z

    .line 2001
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testWhileIdle:Z

    .line 2012
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_timeBetweenEvictionRunsMillis:J

    .line 2027
    const/4 v2, 0x3

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numTestsPerEvictionRun:I

    .line 2041
    const-wide/32 v2, 0x1b7740

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_minEvictableIdleTimeMillis:J

    .line 2054
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_softMinEvictableIdleTimeMillis:J

    .line 2057
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_lifo:Z

    .line 2060
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    .line 2063
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 2066
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 2072
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    .line 2077
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;

    .line 2084
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 2091
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    .line 575
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 576
    iput p2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxActive:I

    .line 577
    move/from16 v0, p18

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_lifo:Z

    .line 578
    packed-switch p3, :pswitch_data_0

    .line 585
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "whenExhaustedAction "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not recognized."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 582
    :pswitch_0
    iput-byte p3, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_whenExhaustedAction:B

    .line 587
    iput-wide p4, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxWait:J

    .line 588
    iput p6, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxIdle:I

    .line 589
    iput p7, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_minIdle:I

    .line 590
    iput-boolean p8, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testOnBorrow:Z

    .line 591
    iput-boolean p9, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testOnReturn:Z

    .line 592
    move-wide/from16 v0, p10

    iput-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_timeBetweenEvictionRunsMillis:J

    .line 593
    move/from16 v0, p12

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numTestsPerEvictionRun:I

    .line 594
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_minEvictableIdleTimeMillis:J

    .line 595
    move-wide/from16 v0, p16

    iput-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_softMinEvictableIdleTimeMillis:J

    .line 596
    move/from16 v0, p15

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testWhileIdle:Z

    .line 598
    new-instance v2, Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-direct {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;-><init>()V

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    .line 599
    iget-wide v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_timeBetweenEvictionRunsMillis:J

    invoke-virtual {p0, v2, v3}, Lorg/apache/commons/pool/impl/GenericObjectPool;->startEvictor(J)V

    .line 600
    return-void

    .line 578
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIZZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJIZZ)V"
        }
    .end annotation

    .prologue
    .line 445
    const/4 v7, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 448
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIZZJIJZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 474
    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-wide/from16 v10, p9

    move/from16 v12, p11

    move-wide/from16 v13, p12

    move/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 476
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJZZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJZZ)V"
        }
    .end annotation

    .prologue
    .line 410
    const/16 v6, 0x8

    const/4 v7, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 413
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;Lorg/apache/commons/pool/impl/GenericObjectPool$Config;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;",
            "Lorg/apache/commons/pool/impl/GenericObjectPool$Config;",
            ")V"
        }
    .end annotation

    .prologue
    .line 365
    move-object/from16 v0, p2

    iget v4, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->maxActive:I

    move-object/from16 v0, p2

    iget-byte v5, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->whenExhaustedAction:B

    move-object/from16 v0, p2

    iget-wide v6, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->maxWait:J

    move-object/from16 v0, p2

    iget v8, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->maxIdle:I

    move-object/from16 v0, p2

    iget v9, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->minIdle:I

    move-object/from16 v0, p2

    iget-boolean v10, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->testOnBorrow:Z

    move-object/from16 v0, p2

    iget-boolean v11, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->testOnReturn:Z

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->timeBetweenEvictionRunsMillis:J

    move-object/from16 v0, p2

    iget v14, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->numTestsPerEvictionRun:I

    move-object/from16 v0, p2

    iget-wide v15, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->minEvictableIdleTimeMillis:J

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->testWhileIdle:Z

    move/from16 v17, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->softMinEvictableIdleTimeMillis:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->lifo:Z

    move/from16 v20, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v20}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJZ)V

    .line 369
    return-void
.end method

.method static synthetic access$700(Lorg/apache/commons/pool/impl/GenericObjectPool;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 192
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->ensureMinIdle()V

    return-void
.end method

.method private addObjectToPool(Ljava/lang/Object;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1426
    .line 1427
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testOnReturn:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->validateObject(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    move v3, v2

    .line 1433
    :goto_0
    if-nez v3, :cond_4

    move v0, v1

    .line 1438
    :goto_1
    monitor-enter p0

    .line 1439
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1458
    :cond_0
    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1459
    if-eqz v2, :cond_1

    .line 1460
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 1464
    :cond_1
    if-eqz v1, :cond_2

    .line 1466
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1471
    :goto_3
    if-eqz p2, :cond_2

    .line 1472
    monitor-enter p0

    .line 1473
    :try_start_2
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    .line 1474
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1475
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 1479
    :cond_2
    return-void

    .line 1430
    :cond_3
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->passivateObject(Ljava/lang/Object;)V

    move v3, v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1433
    goto :goto_1

    .line 1442
    :cond_5
    :try_start_3
    iget v4, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxIdle:I

    if-ltz v4, :cond_6

    iget-object v4, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v4}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v4

    iget v5, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxIdle:I

    if-ge v4, v5, :cond_0

    .line 1444
    :cond_6
    if-eqz v3, :cond_9

    .line 1447
    iget-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_lifo:Z

    if-eqz v2, :cond_8

    .line 1448
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    new-instance v3, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    invoke-direct {v3, p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->addFirst(Ljava/lang/Object;)Z

    .line 1452
    :goto_4
    if-eqz p2, :cond_7

    .line 1453
    iget v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    :cond_7
    move v2, v1

    move v1, v0

    .line 1455
    goto :goto_2

    .line 1450
    :cond_8
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    new-instance v3, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    invoke-direct {v3, p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->addLast(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 1458
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1474
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_3

    :cond_9
    move v1, v0

    goto :goto_2
.end method

.method private declared-synchronized allocate()V
    .locals 2

    .prologue
    .line 1249
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->isClosed()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 1274
    :cond_0
    monitor-exit p0

    return-void

    .line 1253
    :cond_1
    :goto_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1254
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;

    .line 1255
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->setPair(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V
    invoke-static {v0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$300(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V

    .line 1256
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1257
    monitor-enter v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1258
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1259
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1249
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1260
    :cond_2
    :goto_1
    :try_start_4
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxActive:I

    if-ltz v0, :cond_3

    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/2addr v0, v1

    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxActive:I

    if-ge v0, v1, :cond_0

    .line 1268
    :cond_3
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;

    .line 1269
    const/4 v1, 0x1

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->setMayCreate(Z)V
    invoke-static {v0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$500(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;Z)V

    .line 1270
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1271
    monitor-enter v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1272
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1273
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_1

    :catchall_2
    move-exception v1

    :try_start_6
    monitor-exit v0

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1
.end method

.method private declared-synchronized calculateDeficit(Z)I
    .locals 4

    .prologue
    .line 1669
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getMinIdle()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getNumIdle()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1670
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxActive:I

    if-lez v1, :cond_0

    .line 1671
    const/4 v1, 0x0

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getMaxActive()I

    move-result v2

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getNumActive()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getNumIdle()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1673
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1675
    :cond_0
    if-eqz p1, :cond_1

    if-lez v0, :cond_1

    .line 1676
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678
    :cond_1
    monitor-exit p0

    return v0

    .line 1669
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private destroy(Ljava/util/Collection;Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<TT;>;>;",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1337
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1339
    :try_start_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    iget-object v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {p2, v0}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1343
    monitor-enter p0

    .line 1344
    :try_start_1
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1345
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1346
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    goto :goto_0

    .line 1345
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1343
    :catch_0
    move-exception v0

    monitor-enter p0

    .line 1344
    :try_start_2
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1345
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1346
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    goto :goto_0

    .line 1345
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1343
    :catchall_2
    move-exception v0

    monitor-enter p0

    .line 1344
    :try_start_3
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1345
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1346
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    throw v0

    .line 1345
    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1349
    :cond_0
    return-void
.end method

.method private ensureMinIdle()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1645
    invoke-direct {p0, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->calculateDeficit(Z)I

    move-result v1

    .line 1646
    :goto_0
    if-ge v0, v1, :cond_0

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lorg/apache/commons/pool/impl/GenericObjectPool;->calculateDeficit(Z)I

    move-result v2

    if-lez v2, :cond_0

    .line 1648
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->addObject()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1650
    monitor-enter p0

    .line 1651
    :try_start_1
    iget v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1652
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1653
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 1646
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1652
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1650
    :catchall_1
    move-exception v0

    monitor-enter p0

    .line 1651
    :try_start_2
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1652
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1653
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    throw v0

    .line 1652
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1656
    :cond_0
    return-void
.end method

.method private getNumTests()I
    .locals 4

    .prologue
    .line 1754
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numTestsPerEvictionRun:I

    if-ltz v0, :cond_0

    .line 1755
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numTestsPerEvictionRun:I

    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1757
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v0

    int-to-double v0, v0

    iget v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numTestsPerEvictionRun:I

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public addObject()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1687
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->assertOpen()V

    .line 1688
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-nez v0, :cond_0

    .line 1689
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add objects without a factory."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1691
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0}, Lorg/apache/commons/pool/PoolableObjectFactory;->makeObject()Ljava/lang/Object;

    move-result-object v1

    .line 1693
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->assertOpen()V

    .line 1694
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->addObjectToPool(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1702
    return-void

    .line 1695
    :catch_0
    move-exception v0

    .line 1697
    :try_start_1
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v2, v1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1701
    :goto_0
    throw v0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public borrowObject()Ljava/lang/Object;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1059
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1060
    new-instance v5, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;-><init>(Lorg/apache/commons/pool/impl/GenericObjectPool$1;)V

    .line 1063
    monitor-enter p0

    .line 1067
    :try_start_0
    iget-byte v8, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_whenExhaustedAction:B

    .line 1068
    iget-wide v10, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxWait:J

    .line 1071
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1072
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1075
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 1078
    :cond_0
    monitor-enter p0

    .line 1079
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->assertOpen()V

    .line 1080
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1083
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1085
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1089
    packed-switch v8, :pswitch_data_0

    .line 1179
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WhenExhaustedAction property "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not recognized."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1072
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1080
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1092
    :pswitch_0
    monitor-enter p0

    .line 1095
    :try_start_2
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_1

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1096
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1097
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1099
    :cond_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1186
    :cond_2
    :goto_0
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_f

    .line 1188
    :try_start_3
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0}, Lorg/apache/commons/pool/PoolableObjectFactory;->makeObject()Ljava/lang/Object;

    move-result-object v0

    .line 1189
    new-instance v1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    invoke-direct {v1, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;-><init>(Ljava/lang/Object;)V

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->setPair(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V
    invoke-static {v5, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$300(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_7

    move v2, v3

    .line 1204
    :goto_1
    :try_start_4
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/PoolableObjectFactory;->activateObject(Ljava/lang/Object;)V

    .line 1205
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testOnBorrow:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/PoolableObjectFactory;->validateObject(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 1207
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "ValidateObject failed"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 1215
    :catch_0
    move-exception v0

    .line 1216
    invoke-static {v0}, Lorg/apache/commons/pool/PoolUtils;->checkRethrow(Ljava/lang/Throwable;)V

    .line 1219
    :try_start_5
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v9

    iget-object v9, v9, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v1, v9}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    .line 1224
    :goto_2
    monitor-enter p0

    .line 1225
    :try_start_6
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1226
    if-nez v2, :cond_3

    .line 1227
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->reset()V
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$400(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)V

    .line 1228
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    const/4 v9, 0x0

    invoke-virtual {v1, v9, v5}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 1230
    :cond_3
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_a

    .line 1231
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 1232
    if-eqz v2, :cond_0

    .line 1233
    new-instance v1, Ljava/util/NoSuchElementException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not create a validated object, cause: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1099
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1100
    :pswitch_1
    monitor-enter p0

    .line 1105
    :try_start_7
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_4

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1106
    :cond_4
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto/16 :goto_0

    .line 1109
    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1108
    :cond_5
    :try_start_8
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1109
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 1110
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Pool exhausted"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1113
    :pswitch_2
    :try_start_9
    monitor-enter v5
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_1

    .line 1116
    :try_start_a
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_9

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1117
    cmp-long v0, v10, v12

    if-gtz v0, :cond_8

    .line 1118
    invoke-virtual {v5}, Ljava/lang/Object;->wait()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 1132
    :cond_6
    :goto_3
    :try_start_b
    monitor-exit v5

    .line 1134
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->isClosed()Z

    move-result v0

    if-ne v0, v3, :cond_c

    .line 1135
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Pool closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_1

    .line 1137
    :catch_1
    move-exception v0

    .line 1139
    monitor-enter p0

    .line 1141
    :try_start_c
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v1

    if-nez v1, :cond_a

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1144
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v1, v5}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1156
    :goto_4
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    .line 1157
    if-eqz v4, :cond_7

    .line 1158
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 1160
    :cond_7
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 1161
    throw v0

    .line 1122
    :cond_8
    :try_start_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v6

    .line 1123
    sub-long v0, v10, v0

    .line 1124
    cmp-long v2, v0, v12

    if-lez v2, :cond_6

    .line 1126
    invoke-virtual {v5, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    goto :goto_3

    .line 1132
    :catchall_4
    move-exception v0

    :try_start_e
    monitor-exit v5

    throw v0
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_1

    .line 1130
    :cond_9
    :try_start_f
    monitor-exit v5
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    goto/16 :goto_0

    .line 1145
    :cond_a
    :try_start_10
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v1

    if-nez v1, :cond_b

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1148
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    move v4, v3

    .line 1149
    goto :goto_4

    .line 1152
    :cond_b
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1153
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    .line 1154
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool;->returnObject(Ljava/lang/Object;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    goto :goto_4

    .line 1156
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1163
    :cond_c
    cmp-long v0, v10, v12

    if-lez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v6

    cmp-long v0, v0, v10

    if-ltz v0, :cond_0

    .line 1164
    monitor-enter p0

    .line 1167
    :try_start_11
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_d

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1169
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    .line 1173
    monitor-exit p0

    .line 1174
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Timeout waiting for idle object"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1171
    :cond_d
    :try_start_12
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_6

    goto/16 :goto_0

    .line 1173
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1194
    :catchall_7
    move-exception v0

    monitor-enter p0

    .line 1195
    :try_start_13
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1197
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_8

    .line 1198
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    throw v0

    .line 1197
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1209
    :cond_e
    :try_start_14
    monitor-enter p0
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_0

    .line 1210
    :try_start_15
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1211
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    .line 1212
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_9

    .line 1213
    :try_start_16
    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    return-object v0

    .line 1212
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_0

    .line 1220
    :catch_2
    move-exception v1

    invoke-static {v1}, Lorg/apache/commons/pool/PoolUtils;->checkRethrow(Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 1230
    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_f
    move v2, v4

    goto/16 :goto_1

    .line 1089
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public clear()V
    .locals 3

    .prologue
    .line 1317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1319
    monitor-enter p0

    .line 1320
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1321
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    iget v2, v2, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1322
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->clear()V

    .line 1323
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1324
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool;->destroy(Ljava/util/Collection;Lorg/apache/commons/pool/PoolableObjectFactory;)V

    .line 1325
    return-void

    .line 1323
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1493
    invoke-super {p0}, Lorg/apache/commons/pool/BaseObjectPool;->close()V

    .line 1494
    monitor-enter p0

    .line 1495
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->clear()V

    .line 1496
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool;->startEvictor(J)V

    .line 1498
    :goto_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1499
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;

    .line 1501
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1503
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1504
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1505
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    return-void
.end method

.method declared-synchronized debugInfo()Ljava/lang/String;
    .locals 8

    .prologue
    .line 1732
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1733
    const-string v0, "Active: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getNumActive()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1734
    const-string v0, "Idle: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getNumIdle()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1735
    const-string v0, "Idle Objects:\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1736
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1737
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1738
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1739
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    .line 1740
    const-string v3, "\t"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v6, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v6, "\t"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-wide v6, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->tstamp:J

    sub-long v6, v4, v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1732
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1742
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public evict()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1554
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->assertOpen()V

    .line 1555
    monitor-enter p0

    .line 1556
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1557
    monitor-exit p0

    .line 1631
    :goto_0
    return-void

    .line 1559
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-nez v0, :cond_1

    .line 1560
    iget-object v3, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_lifo:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v0

    :goto_1
    invoke-virtual {v3, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->cursor(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 1562
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1564
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getNumTests()I

    move-result v6

    move v5, v2

    :goto_2
    if-ge v5, v6, :cond_d

    .line 1566
    monitor-enter p0

    .line 1567
    :try_start_1
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_lifo:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_lifo:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1569
    :cond_3
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->close()V

    .line 1570
    iget-object v3, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_lifo:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v0

    :goto_3
    invoke-virtual {v3, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->cursor(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 1573
    :cond_4
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_lifo:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-object v4, v0

    .line 1577
    :goto_4
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->remove()V

    .line 1578
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1579
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1582
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-wide v10, v4, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->tstamp:J

    sub-long/2addr v8, v10

    .line 1583
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getMinEvictableIdleTimeMillis()J

    move-result-wide v10

    cmp-long v0, v10, v12

    if-lez v0, :cond_b

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getMinEvictableIdleTimeMillis()J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-lez v0, :cond_b

    move v0, v1

    .line 1591
    :goto_5
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getTestWhileIdle()Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez v0, :cond_5

    .line 1594
    :try_start_2
    iget-object v3, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    iget-object v7, v4, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v3, v7}, Lorg/apache/commons/pool/PoolableObjectFactory;->activateObject(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move v3, v1

    .line 1599
    :goto_6
    if-eqz v3, :cond_5

    .line 1600
    iget-object v3, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    iget-object v7, v4, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v3, v7}, Lorg/apache/commons/pool/PoolableObjectFactory;->validateObject(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v0, v1

    .line 1612
    :cond_5
    :goto_7
    if-eqz v0, :cond_6

    .line 1614
    :try_start_3
    iget-object v3, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    iget-object v7, v4, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v3, v7}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 1619
    :cond_6
    :goto_8
    monitor-enter p0

    .line 1620
    if-nez v0, :cond_7

    .line 1621
    :try_start_4
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0, v4}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->add(Ljava/lang/Object;)V

    .line 1622
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_lifo:Z

    if-eqz v0, :cond_7

    .line 1624
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->previous()Ljava/lang/Object;

    .line 1627
    :cond_7
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1628
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1564
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto/16 :goto_2

    :cond_8
    move v0, v2

    .line 1560
    goto/16 :goto_1

    .line 1562
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_9
    move v0, v2

    .line 1570
    goto/16 :goto_3

    .line 1573
    :cond_a
    :try_start_5
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v4, v0

    goto :goto_4

    .line 1579
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1586
    :cond_b
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getSoftMinEvictableIdleTimeMillis()J

    move-result-wide v10

    cmp-long v0, v10, v12

    if-lez v0, :cond_e

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getSoftMinEvictableIdleTimeMillis()J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-lez v0, :cond_e

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getNumIdle()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getMinIdle()I

    move-result v3

    if-le v0, v3, :cond_e

    move v0, v1

    .line 1589
    goto :goto_5

    .line 1597
    :catch_0
    move-exception v0

    move v3, v2

    move v0, v1

    goto :goto_6

    .line 1604
    :cond_c
    :try_start_6
    iget-object v3, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    iget-object v7, v4, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v3, v7}, Lorg/apache/commons/pool/PoolableObjectFactory;->passivateObject(Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_7

    .line 1606
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_7

    .line 1628
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1630
    :cond_d
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    goto/16 :goto_0

    :catch_2
    move-exception v3

    goto :goto_8

    :cond_e
    move v0, v2

    goto/16 :goto_5
.end method

.method public declared-synchronized getLifo()Z
    .locals 1

    .prologue
    .line 986
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_lifo:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMaxActive()I
    .locals 1

    .prologue
    .line 616
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxActive:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMaxIdle()I
    .locals 1

    .prologue
    .line 721
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxIdle:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMaxWait()J
    .locals 2

    .prologue
    .line 690
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxWait:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMinEvictableIdleTimeMillis()J
    .locals 2

    .prologue
    .line 898
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_minEvictableIdleTimeMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMinIdle()I
    .locals 1

    .prologue
    .line 772
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_minIdle:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumActive()I
    .locals 1

    .prologue
    .line 1358
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumIdle()I
    .locals 1

    .prologue
    .line 1368
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumTestsPerEvictionRun()I
    .locals 1

    .prologue
    .line 867
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numTestsPerEvictionRun:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSoftMinEvictableIdleTimeMillis()J
    .locals 2

    .prologue
    .line 927
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_softMinEvictableIdleTimeMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTestOnBorrow()Z
    .locals 1

    .prologue
    .line 787
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testOnBorrow:Z

    return v0
.end method

.method public getTestOnReturn()Z
    .locals 1

    .prologue
    .line 815
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testOnReturn:Z

    return v0
.end method

.method public declared-synchronized getTestWhileIdle()Z
    .locals 1

    .prologue
    .line 958
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testWhileIdle:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTimeBetweenEvictionRunsMillis()J
    .locals 2

    .prologue
    .line 841
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_timeBetweenEvictionRunsMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getWhenExhaustedAction()B
    .locals 1

    .prologue
    .line 645
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_whenExhaustedAction:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public invalidateObject(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1289
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-eqz v0, :cond_0

    .line 1290
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1293
    :cond_0
    monitor-enter p0

    .line 1294
    :try_start_1
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    .line 1295
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1296
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 1297
    return-void

    .line 1295
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1293
    :catchall_1
    move-exception v0

    monitor-enter p0

    .line 1294
    :try_start_2
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    .line 1295
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1296
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    throw v0

    .line 1295
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public returnObject(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1393
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->addObjectToPool(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1410
    :cond_0
    :goto_0
    return-void

    .line 1395
    :catch_0
    move-exception v0

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-eqz v0, :cond_0

    .line 1397
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1404
    :goto_1
    monitor-enter p0

    .line 1405
    :try_start_2
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numActive:I

    .line 1406
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1407
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    goto :goto_0

    .line 1406
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public setConfig(Lorg/apache/commons/pool/impl/GenericObjectPool$Config;)V
    .locals 2

    .prologue
    .line 1010
    monitor-enter p0

    .line 1011
    :try_start_0
    iget v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->maxIdle:I

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setMaxIdle(I)V

    .line 1012
    iget v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->minIdle:I

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setMinIdle(I)V

    .line 1013
    iget v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->maxActive:I

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setMaxActive(I)V

    .line 1014
    iget-wide v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->maxWait:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setMaxWait(J)V

    .line 1015
    iget-byte v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->whenExhaustedAction:B

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setWhenExhaustedAction(B)V

    .line 1016
    iget-boolean v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->testOnBorrow:Z

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setTestOnBorrow(Z)V

    .line 1017
    iget-boolean v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->testOnReturn:Z

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setTestOnReturn(Z)V

    .line 1018
    iget-boolean v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->testWhileIdle:Z

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setTestWhileIdle(Z)V

    .line 1019
    iget v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->numTestsPerEvictionRun:I

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setNumTestsPerEvictionRun(I)V

    .line 1020
    iget-wide v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->minEvictableIdleTimeMillis:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setMinEvictableIdleTimeMillis(J)V

    .line 1021
    iget-wide v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->timeBetweenEvictionRunsMillis:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setTimeBetweenEvictionRunsMillis(J)V

    .line 1022
    iget-wide v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->softMinEvictableIdleTimeMillis:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setSoftMinEvictableIdleTimeMillis(J)V

    .line 1023
    iget-boolean v0, p1, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->lifo:Z

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->setLifo(Z)V

    .line 1024
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1025
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 1026
    return-void

    .line 1024
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setFactory(Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1524
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1525
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 1526
    monitor-enter p0

    .line 1527
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->assertOpen()V

    .line 1528
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->getNumActive()I

    move-result v2

    if-lez v2, :cond_0

    .line 1529
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Objects are already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1536
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1531
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1532
    iget v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    iget-object v3, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    iget v3, v3, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numInternalProcessing:I

    .line 1533
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_pool:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->clear()V

    .line 1535
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 1536
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1537
    invoke-direct {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool;->destroy(Ljava/util/Collection;Lorg/apache/commons/pool/PoolableObjectFactory;)V

    .line 1538
    return-void
.end method

.method public declared-synchronized setLifo(Z)V
    .locals 1

    .prologue
    .line 1000
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_lifo:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1001
    monitor-exit p0

    return-void

    .line 1000
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMaxActive(I)V
    .locals 1

    .prologue
    .line 630
    monitor-enter p0

    .line 631
    :try_start_0
    iput p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxActive:I

    .line 632
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 633
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 634
    return-void

    .line 632
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMaxIdle(I)V
    .locals 1

    .prologue
    .line 738
    monitor-enter p0

    .line 739
    :try_start_0
    iput p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxIdle:I

    .line 740
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 741
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 742
    return-void

    .line 740
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMaxWait(J)V
    .locals 1

    .prologue
    .line 709
    monitor-enter p0

    .line 710
    :try_start_0
    iput-wide p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_maxWait:J

    .line 711
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 712
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 713
    return-void

    .line 711
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setMinEvictableIdleTimeMillis(J)V
    .locals 1

    .prologue
    .line 913
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_minEvictableIdleTimeMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 914
    monitor-exit p0

    return-void

    .line 913
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMinIdle(I)V
    .locals 1

    .prologue
    .line 757
    monitor-enter p0

    .line 758
    :try_start_0
    iput p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_minIdle:I

    .line 759
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 760
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 761
    return-void

    .line 759
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setNumTestsPerEvictionRun(I)V
    .locals 1

    .prologue
    .line 885
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_numTestsPerEvictionRun:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 886
    monitor-exit p0

    return-void

    .line 885
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setSoftMinEvictableIdleTimeMillis(J)V
    .locals 1

    .prologue
    .line 944
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_softMinEvictableIdleTimeMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 945
    monitor-exit p0

    return-void

    .line 944
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setTestOnBorrow(Z)V
    .locals 0

    .prologue
    .line 802
    iput-boolean p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testOnBorrow:Z

    .line 803
    return-void
.end method

.method public setTestOnReturn(Z)V
    .locals 0

    .prologue
    .line 828
    iput-boolean p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testOnReturn:Z

    .line 829
    return-void
.end method

.method public declared-synchronized setTestWhileIdle(Z)V
    .locals 1

    .prologue
    .line 972
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_testWhileIdle:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 973
    monitor-exit p0

    return-void

    .line 972
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setTimeBetweenEvictionRunsMillis(J)V
    .locals 3

    .prologue
    .line 854
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_timeBetweenEvictionRunsMillis:J

    .line 855
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_timeBetweenEvictionRunsMillis:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool;->startEvictor(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 856
    monitor-exit p0

    return-void

    .line 854
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setWhenExhaustedAction(B)V
    .locals 3

    .prologue
    .line 659
    monitor-enter p0

    .line 660
    packed-switch p1, :pswitch_data_0

    .line 667
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "whenExhaustedAction "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not recognized."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 669
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 664
    :pswitch_0
    :try_start_1
    iput-byte p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_whenExhaustedAction:B

    .line 669
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 670
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->allocate()V

    .line 671
    return-void

    .line 660
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected declared-synchronized startEvictor(J)V
    .locals 3

    .prologue
    .line 1715
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;

    if-eqz v0, :cond_0

    .line 1716
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;

    invoke-static {v0}, Lorg/apache/commons/pool/impl/EvictionTimer;->cancel(Ljava/util/TimerTask;)V

    .line 1717
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;

    .line 1719
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 1720
    new-instance v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;-><init>(Lorg/apache/commons/pool/impl/GenericObjectPool;Lorg/apache/commons/pool/impl/GenericObjectPool$1;)V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;

    .line 1721
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;

    invoke-static {v0, p1, p2, p1, p2}, Lorg/apache/commons/pool/impl/EvictionTimer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1723
    :cond_1
    monitor-exit p0

    return-void

    .line 1715
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
