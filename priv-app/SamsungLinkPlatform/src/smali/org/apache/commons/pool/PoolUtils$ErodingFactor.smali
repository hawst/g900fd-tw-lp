.class Lorg/apache/commons/pool/PoolUtils$ErodingFactor;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/PoolUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ErodingFactor"
.end annotation


# instance fields
.field private final factor:F

.field private volatile transient idleHighWaterMark:I

.field private volatile transient nextShrink:J


# direct methods
.method public constructor <init>(F)V
    .locals 4

    .prologue
    .line 2031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2032
    iput p1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->factor:F

    .line 2033
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const v2, 0x495bba00    # 900000.0f

    mul-float/2addr v2, p1

    float-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->nextShrink:J

    .line 2034
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->idleHighWaterMark:I

    .line 2035
    return-void
.end method


# virtual methods
.method public getNextShrink()J
    .locals 2

    .prologue
    .line 2066
    iget-wide v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->nextShrink:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2074
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ErodingFactor{factor="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->factor:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", idleHighWaterMark="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->idleHighWaterMark:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public update(I)V
    .locals 2

    .prologue
    .line 2043
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1, p1}, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->update(JI)V

    .line 2044
    return-void
.end method

.method public update(JI)V
    .locals 5

    .prologue
    .line 2053
    const/4 v0, 0x0

    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2054
    iget v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->idleHighWaterMark:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->idleHighWaterMark:I

    .line 2055
    const/high16 v1, 0x41700000    # 15.0f

    const/high16 v2, -0x3ea00000    # -14.0f

    iget v3, p0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->idleHighWaterMark:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    int-to-float v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    .line 2057
    const v1, 0x476a6000    # 60000.0f

    mul-float/2addr v0, v1

    iget v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->factor:F

    mul-float/2addr v0, v1

    float-to-long v0, v0

    add-long/2addr v0, p1

    iput-wide v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->nextShrink:J

    .line 2058
    return-void
.end method
