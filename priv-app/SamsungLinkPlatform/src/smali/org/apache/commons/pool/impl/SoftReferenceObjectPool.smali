.class public Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;
.super Lorg/apache/commons/pool/BaseObjectPool;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/ObjectPool;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/commons/pool/BaseObjectPool",
        "<TT;>;",
        "Lorg/apache/commons/pool/ObjectPool",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private _factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;"
        }
    .end annotation
.end field

.field private _numActive:I

.field private final _pool:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/SoftReference",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final refQueue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Lorg/apache/commons/pool/BaseObjectPool;-><init>()V

    .line 368
    iput-object v1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 375
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->refQueue:Ljava/lang/ref/ReferenceQueue;

    .line 378
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_numActive:I

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    .line 57
    iput-object v1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 58
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0}, Lorg/apache/commons/pool/BaseObjectPool;-><init>()V

    .line 368
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 375
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->refQueue:Ljava/lang/ref/ReferenceQueue;

    .line 378
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_numActive:I

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    .line 67
    iput-object p1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 68
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0}, Lorg/apache/commons/pool/BaseObjectPool;-><init>()V

    .line 368
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 375
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->refQueue:Ljava/lang/ref/ReferenceQueue;

    .line 378
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_numActive:I

    .line 82
    if-nez p1, :cond_0

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "factory required to prefill the pool."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    .line 86
    iput-object p1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 87
    invoke-static {p0, p2}, Lorg/apache/commons/pool/PoolUtils;->prefill(Lorg/apache/commons/pool/ObjectPool;I)V

    .line 88
    return-void
.end method

.method private pruneClearedReferences()V
    .locals 2

    .prologue
    .line 345
    :goto_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->refQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 347
    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 350
    :catch_0
    move-exception v0

    goto :goto_0

    .line 352
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized addObject()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 230
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->assertOpen()V

    .line 231
    iget-object v2, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-nez v2, :cond_0

    .line 232
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add objects without a factory."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 234
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v2}, Lorg/apache/commons/pool/PoolableObjectFactory;->makeObject()Ljava/lang/Object;

    move-result-object v3

    .line 237
    iget-object v2, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v2, v3}, Lorg/apache/commons/pool/PoolableObjectFactory;->validateObject(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v1

    .line 243
    :goto_0
    if-nez v2, :cond_4

    .line 244
    :goto_1
    if-eqz v2, :cond_1

    .line 245
    iget-object v1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/SoftReference;

    iget-object v4, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->refQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v2, v3, v4}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249
    :cond_1
    if-eqz v0, :cond_2

    .line 251
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, v3}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 256
    :cond_2
    :goto_2
    monitor-exit p0

    return-void

    .line 240
    :cond_3
    :try_start_3
    iget-object v2, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v2, v3}, Lorg/apache/commons/pool/PoolableObjectFactory;->passivateObject(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v2, v0

    goto :goto_0

    :cond_4
    move v0, v1

    .line 243
    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public declared-synchronized borrowObject()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 113
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->assertOpen()V

    .line 115
    const/4 v0, 0x0

    move v1, v0

    move-object v0, v3

    .line 116
    :goto_0
    if-nez v0, :cond_4

    .line 117
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 121
    :cond_0
    const/4 v2, 0x1

    .line 122
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0}, Lorg/apache/commons/pool/PoolableObjectFactory;->makeObject()Ljava/lang/Object;

    move-result-object v1

    .line 129
    :goto_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    .line 131
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/PoolableObjectFactory;->activateObject(Ljava/lang/Object;)V

    .line 132
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/PoolableObjectFactory;->validateObject(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 133
    new-instance v0, Ljava/lang/Exception;

    const-string v4, "ValidateObject failed"

    invoke-direct {v0, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 135
    :catch_0
    move-exception v0

    .line 136
    :try_start_3
    invoke-static {v0}, Lorg/apache/commons/pool/PoolUtils;->checkRethrow(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 138
    :try_start_4
    iget-object v4, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v4, v1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 145
    :goto_2
    if-eqz v2, :cond_3

    .line 146
    :try_start_5
    new-instance v1, Ljava/util/NoSuchElementException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not create a validated object, cause: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 125
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 126
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    .line 127
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->clear()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v5, v1

    move-object v1, v2

    move v2, v5

    goto :goto_1

    :cond_2
    move-object v0, v1

    move v1, v2

    .line 150
    goto :goto_0

    .line 139
    :catch_1
    move-exception v1

    :try_start_6
    invoke-static {v1}, Lorg/apache/commons/pool/PoolUtils;->checkRethrow(Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    .line 143
    :catchall_1
    move-exception v0

    :try_start_7
    throw v0

    :cond_3
    move v1, v2

    move-object v0, v3

    .line 150
    goto/16 :goto_0

    .line 153
    :cond_4
    iget v1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_numActive:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_numActive:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 154
    monitor-exit p0

    return-object v0

    :cond_5
    move-object v0, v1

    move v1, v2

    goto/16 :goto_0
.end method

.method public declared-synchronized clear()V
    .locals 3

    .prologue
    .line 284
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-eqz v0, :cond_1

    .line 285
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 286
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 288
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_0

    .line 290
    iget-object v2, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v2, v0}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 294
    :catch_0
    move-exception v0

    goto :goto_0

    .line 297
    :cond_1
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 298
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->pruneClearedReferences()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 299
    monitor-exit p0

    return-void

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 313
    invoke-super {p0}, Lorg/apache/commons/pool/BaseObjectPool;->close()V

    .line 314
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->clear()V

    .line 315
    return-void
.end method

.method public declared-synchronized getFactory()Lorg/apache/commons/pool/PoolableObjectFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 361
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumActive()I
    .locals 1

    .prologue
    .line 276
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_numActive:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumIdle()I
    .locals 1

    .prologue
    .line 265
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->pruneClearedReferences()V

    .line 266
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized invalidateObject(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 207
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_numActive:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_numActive:I

    .line 208
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V

    .line 211
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    monitor-exit p0

    return-void

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized returnObject(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 173
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->isClosed()Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v0

    .line 174
    :goto_0
    iget-object v3, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-eqz v3, :cond_0

    .line 175
    iget-object v3, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v3, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->validateObject(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v2, v1

    .line 186
    :cond_0
    :goto_1
    if-nez v2, :cond_5

    .line 187
    :goto_2
    iget v1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_numActive:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_numActive:I

    .line 188
    if-eqz v2, :cond_1

    .line 189
    iget-object v1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_pool:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/SoftReference;

    iget-object v3, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->refQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v2, p1, v3}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 193
    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    .line 195
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    :cond_2
    :goto_3
    monitor-exit p0

    return-void

    :cond_3
    move v2, v1

    .line 173
    goto :goto_0

    .line 179
    :cond_4
    :try_start_2
    iget-object v3, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v3, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->passivateObject(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 181
    :catch_0
    move-exception v2

    move v2, v1

    goto :goto_1

    :cond_5
    move v0, v1

    .line 186
    goto :goto_2

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public declared-synchronized setFactory(Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 330
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->assertOpen()V

    .line 331
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->getNumActive()I

    move-result v0

    if-lez v0, :cond_0

    .line 332
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Objects are already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 334
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->clear()V

    .line 335
    iput-object p1, p0, Lorg/apache/commons/pool/impl/SoftReferenceObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 337
    monitor-exit p0

    return-void
.end method
