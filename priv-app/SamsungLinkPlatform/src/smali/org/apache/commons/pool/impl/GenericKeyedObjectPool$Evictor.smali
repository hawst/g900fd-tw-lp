.class Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;
.super Ljava/util/TimerTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Evictor"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;


# direct methods
.method private constructor <init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)V
    .locals 0

    .prologue
    .line 2378
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$1;)V
    .locals 0

    .prologue
    .line 2378
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;-><init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 2387
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->evict()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2397
    :goto_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->ensureMinIdle()V
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->access$1600(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2401
    :goto_1
    return-void

    .line 2390
    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v0, v1}, Ljava/lang/OutOfMemoryError;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_0

    .line 2401
    :catch_1
    move-exception v0

    goto :goto_1

    .line 2394
    :catch_2
    move-exception v0

    goto :goto_0
.end method
