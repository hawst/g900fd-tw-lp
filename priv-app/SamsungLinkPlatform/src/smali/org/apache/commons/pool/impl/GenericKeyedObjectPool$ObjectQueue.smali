.class Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ObjectQueue"
.end annotation


# instance fields
.field private activeCount:I

.field private internalProcessingCount:I

.field private final queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;


# direct methods
.method private constructor <init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2231
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2233
    iput v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I

    .line 2236
    new-instance v0, Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-direct {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    .line 2239
    iput v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$1;)V
    .locals 0

    .prologue
    .line 2231
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;-><init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)V

    return-void
.end method

.method static synthetic access$1000(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I
    .locals 1

    .prologue
    .line 2231
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I

    return v0
.end method

.method static synthetic access$1100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I
    .locals 1

    .prologue
    .line 2231
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I

    return v0
.end method

.method static synthetic access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;
    .locals 1

    .prologue
    .line 2231
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    return-object v0
.end method


# virtual methods
.method decrementActiveCount()V
    .locals 2

    .prologue
    .line 2251
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    monitor-enter v1

    .line 2252
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    # operator-- for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalActive:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->access$1410(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)I

    .line 2253
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2254
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I

    if-lez v0, :cond_0

    .line 2255
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I

    .line 2257
    :cond_0
    return-void

    .line 2253
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method decrementInternalProcessingCount()V
    .locals 2

    .prologue
    .line 2269
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    monitor-enter v1

    .line 2270
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    # operator-- for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->access$1510(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)I

    .line 2271
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2272
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I

    .line 2273
    return-void

    .line 2271
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method incrementActiveCount()V
    .locals 2

    .prologue
    .line 2243
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    monitor-enter v1

    .line 2244
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    # operator++ for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalActive:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->access$1408(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)I

    .line 2245
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2246
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I

    .line 2247
    return-void

    .line 2245
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method incrementInternalProcessingCount()V
    .locals 2

    .prologue
    .line 2261
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    monitor-enter v1

    .line 2262
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    # operator++ for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->access$1508(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)I

    .line 2263
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2264
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I

    .line 2265
    return-void

    .line 2263
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
