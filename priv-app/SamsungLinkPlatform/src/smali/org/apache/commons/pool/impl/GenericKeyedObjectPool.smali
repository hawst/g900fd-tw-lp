.class public Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;
.super Lorg/apache/commons/pool/BaseKeyedObjectPool;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/KeyedObjectPool;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$1;,
        Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;,
        Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;,
        Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;,
        Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;,
        Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/commons/pool/BaseKeyedObjectPool",
        "<TK;TV;>;",
        "Lorg/apache/commons/pool/KeyedObjectPool",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field public static final DEFAULT_LIFO:Z = true

.field public static final DEFAULT_MAX_ACTIVE:I = 0x8

.field public static final DEFAULT_MAX_IDLE:I = 0x8

.field public static final DEFAULT_MAX_TOTAL:I = -0x1

.field public static final DEFAULT_MAX_WAIT:J = -0x1L

.field public static final DEFAULT_MIN_EVICTABLE_IDLE_TIME_MILLIS:J = 0x1b7740L

.field public static final DEFAULT_MIN_IDLE:I = 0x0

.field public static final DEFAULT_NUM_TESTS_PER_EVICTION_RUN:I = 0x3

.field public static final DEFAULT_TEST_ON_BORROW:Z = false

.field public static final DEFAULT_TEST_ON_RETURN:Z = false

.field public static final DEFAULT_TEST_WHILE_IDLE:Z = false

.field public static final DEFAULT_TIME_BETWEEN_EVICTION_RUNS_MILLIS:J = -0x1L

.field public static final DEFAULT_WHEN_EXHAUSTED_ACTION:B = 0x1t

.field public static final WHEN_EXHAUSTED_BLOCK:B = 0x1t

.field public static final WHEN_EXHAUSTED_FAIL:B = 0x0t

.field public static final WHEN_EXHAUSTED_GROW:B = 0x2t


# instance fields
.field private _allocationQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool",
            "<TK;TV;>.",
            "Latch",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field private _evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<TV;>;>.Cursor;"
        }
    .end annotation
.end field

.field private _evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<TK;>.Cursor;"
        }
    .end annotation
.end field

.field private _evictor:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool",
            "<TK;TV;>.Evictor;"
        }
    .end annotation
.end field

.field private _factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private _lifo:Z

.field private _maxActive:I

.field private _maxIdle:I

.field private _maxTotal:I

.field private _maxWait:J

.field private _minEvictableIdleTimeMillis:J

.field private volatile _minIdle:I

.field private _numTestsPerEvictionRun:I

.field private _poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<TK;>;"
        }
    .end annotation
.end field

.field private _poolMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool",
            "<TK;TV;>.ObjectQueue;>;"
        }
    .end annotation
.end field

.field private volatile _testOnBorrow:Z

.field private volatile _testOnReturn:Z

.field private _testWhileIdle:Z

.field private _timeBetweenEvictionRunsMillis:J

.field private _totalActive:I

.field private _totalIdle:I

.field private _totalInternalProcessing:I

.field private _whenExhaustedAction:B


# direct methods
.method public constructor <init>()V
    .locals 15

    .prologue
    const-wide/16 v4, -0x1

    const/16 v2, 0x8

    const/4 v7, 0x0

    .line 364
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v11, 0x3

    const-wide/32 v12, 0x1b7740

    move-object v0, p0

    move v6, v2

    move v8, v7

    move-wide v9, v4

    move v14, v7

    invoke-direct/range {v0 .. v14}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZJIJZ)V

    .line 367
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 375
    const/16 v2, 0x8

    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, -0x1

    const/4 v11, 0x3

    const-wide/32 v12, 0x1b7740

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    invoke-direct/range {v0 .. v14}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZJIJZ)V

    .line 378
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;I)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;I)V"
        }
    .end annotation

    .prologue
    .line 399
    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, -0x1

    const/4 v11, 0x3

    const-wide/32 v12, 0x1b7740

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct/range {v0 .. v14}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZJIJZ)V

    .line 402
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJ)V"
        }
    .end annotation

    .prologue
    .line 415
    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, -0x1

    const/4 v11, 0x3

    const-wide/32 v12, 0x1b7740

    const/4 v14, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    invoke-direct/range {v0 .. v14}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZJIJZ)V

    .line 418
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJI)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJI)V"
        }
    .end annotation

    .prologue
    .line 453
    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, -0x1

    const/4 v11, 0x3

    const-wide/32 v12, 0x1b7740

    const/4 v14, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    invoke-direct/range {v0 .. v14}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZJIJZ)V

    .line 456
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIIZZJIJZ)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJIIIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 575
    const/16 v17, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move-wide/from16 v11, p11

    move/from16 v13, p13

    move-wide/from16 v14, p14

    move/from16 v16, p16

    invoke-direct/range {v0 .. v17}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIIZZJIJZZ)V

    .line 578
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIIZZJIJZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJIIIZZJIJZZ)V"
        }
    .end annotation

    .prologue
    .line 610
    invoke-direct {p0}, Lorg/apache/commons/pool/BaseKeyedObjectPool;-><init>()V

    .line 2571
    const/16 v2, 0x8

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxIdle:I

    .line 2578
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_minIdle:I

    .line 2585
    const/16 v2, 0x8

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxActive:I

    .line 2592
    const/4 v2, -0x1

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxTotal:I

    .line 2610
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxWait:J

    .line 2624
    const/4 v2, 0x1

    iput-byte v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_whenExhaustedAction:B

    .line 2637
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testOnBorrow:Z

    .line 2648
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testOnReturn:Z

    .line 2661
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testWhileIdle:Z

    .line 2672
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_timeBetweenEvictionRunsMillis:J

    .line 2687
    const/4 v2, 0x3

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_numTestsPerEvictionRun:I

    .line 2701
    const-wide/32 v2, 0x1b7740

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_minEvictableIdleTimeMillis:J

    .line 2704
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    .line 2707
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalActive:I

    .line 2710
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    .line 2717
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    .line 2720
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    .line 2725
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;

    .line 2731
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    .line 2734
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 2737
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 2740
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z

    .line 2747
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    .line 611
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    .line 612
    iput p2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxActive:I

    .line 613
    move/from16 v0, p17

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z

    .line 614
    packed-switch p3, :pswitch_data_0

    .line 621
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "whenExhaustedAction "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not recognized."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 618
    :pswitch_0
    iput-byte p3, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_whenExhaustedAction:B

    .line 623
    iput-wide p4, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxWait:J

    .line 624
    iput p6, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxIdle:I

    .line 625
    iput p7, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxTotal:I

    .line 626
    iput p8, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_minIdle:I

    .line 627
    iput-boolean p9, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testOnBorrow:Z

    .line 628
    move/from16 v0, p10

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testOnReturn:Z

    .line 629
    move-wide/from16 v0, p11

    iput-wide v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_timeBetweenEvictionRunsMillis:J

    .line 630
    move/from16 v0, p13

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_numTestsPerEvictionRun:I

    .line 631
    move-wide/from16 v0, p14

    iput-wide v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_minEvictableIdleTimeMillis:J

    .line 632
    move/from16 v0, p16

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testWhileIdle:Z

    .line 634
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    .line 635
    new-instance v2, Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-direct {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;-><init>()V

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    .line 637
    iget-wide v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_timeBetweenEvictionRunsMillis:J

    invoke-virtual {p0, v2, v3}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->startEvictor(J)V

    .line 638
    return-void

    .line 614
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIZZJIJZ)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJIIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 540
    const/4 v8, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-wide/from16 v11, p10

    move/from16 v13, p12

    move-wide/from16 v14, p13

    move/from16 v16, p15

    invoke-direct/range {v0 .. v16}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIIZZJIJZ)V

    .line 543
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJIZZ)V"
        }
    .end annotation

    .prologue
    .line 475
    const-wide/16 v9, -0x1

    const/4 v11, 0x3

    const-wide/32 v12, 0x1b7740

    const/4 v14, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v14}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZJIJZ)V

    .line 478
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZJIJZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 507
    const/4 v7, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-wide/from16 v10, p9

    move/from16 v12, p11

    move-wide/from16 v13, p12

    move/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIZZJIJZ)V

    .line 510
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJZZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJZZ)V"
        }
    .end annotation

    .prologue
    .line 435
    const/16 v6, 0x8

    const-wide/16 v9, -0x1

    const/4 v11, 0x3

    const-wide/32 v12, 0x1b7740

    const/4 v14, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v14}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZJIJZ)V

    .line 438
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;",
            ")V"
        }
    .end annotation

    .prologue
    .line 387
    move-object/from16 v0, p2

    iget v4, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxActive:I

    move-object/from16 v0, p2

    iget-byte v5, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->whenExhaustedAction:B

    move-object/from16 v0, p2

    iget-wide v6, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxWait:J

    move-object/from16 v0, p2

    iget v8, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxIdle:I

    move-object/from16 v0, p2

    iget v9, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxTotal:I

    move-object/from16 v0, p2

    iget v10, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->minIdle:I

    move-object/from16 v0, p2

    iget-boolean v11, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->testOnBorrow:Z

    move-object/from16 v0, p2

    iget-boolean v12, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->testOnReturn:Z

    move-object/from16 v0, p2

    iget-wide v13, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->timeBetweenEvictionRunsMillis:J

    move-object/from16 v0, p2

    iget v15, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->numTestsPerEvictionRun:I

    move-object/from16 v0, p2

    iget-wide v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->minEvictableIdleTimeMillis:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->testWhileIdle:Z

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->lifo:Z

    move/from16 v19, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v19}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIIZZJIJZZ)V

    .line 390
    return-void
.end method

.method static synthetic access$1408(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)I
    .locals 2

    .prologue
    .line 207
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalActive:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalActive:I

    return v0
.end method

.method static synthetic access$1410(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)I
    .locals 2

    .prologue
    .line 207
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalActive:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalActive:I

    return v0
.end method

.method static synthetic access$1508(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)I
    .locals 2

    .prologue
    .line 207
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    return v0
.end method

.method static synthetic access$1510(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)I
    .locals 2

    .prologue
    .line 207
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    return v0
.end method

.method static synthetic access$1600(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 207
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->ensureMinIdle()V

    return-void
.end method

.method private addObjectToPool(Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1633
    .line 1634
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testOnReturn:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->validateObject(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v4, v3

    .line 1640
    :goto_0
    if-nez v4, :cond_6

    move v1, v2

    .line 1646
    :goto_1
    monitor-enter p0

    .line 1648
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1650
    if-nez v0, :cond_0

    .line 1651
    new-instance v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    const/4 v5, 0x0

    invoke-direct {v0, p0, v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;-><init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$1;)V

    .line 1652
    iget-object v5, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v5, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1653
    iget-object v5, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v5, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->add(Ljava/lang/Object;)Z

    .line 1655
    :cond_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->isClosed()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1677
    :cond_1
    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678
    if-eqz v3, :cond_2

    .line 1679
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 1683
    :cond_2
    if-eqz v2, :cond_4

    .line 1685
    :try_start_1
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v1, p1, p2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1690
    :goto_3
    if-eqz p3, :cond_4

    .line 1691
    monitor-enter p0

    .line 1692
    :try_start_2
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementActiveCount()V

    .line 1693
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1000(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v1

    if-nez v1, :cond_3

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v0

    if-nez v0, :cond_3

    .line 1696
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1697
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->remove(Ljava/lang/Object;)Z

    .line 1699
    :cond_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1700
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 1703
    :cond_4
    return-void

    .line 1637
    :cond_5
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->passivateObject(Ljava/lang/Object;Ljava/lang/Object;)V

    move v4, v2

    goto :goto_0

    :cond_6
    move v1, v3

    .line 1640
    goto :goto_1

    .line 1660
    :cond_7
    :try_start_3
    iget v5, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxIdle:I

    if-ltz v5, :cond_8

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v5

    iget v6, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxIdle:I

    if-ge v5, v6, :cond_1

    .line 1662
    :cond_8
    if-eqz v4, :cond_b

    .line 1665
    iget-boolean v3, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z

    if-eqz v3, :cond_a

    .line 1666
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v3

    new-instance v4, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    invoke-direct {v4, p2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->addFirst(Ljava/lang/Object;)Z

    .line 1670
    :goto_4
    iget v3, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    .line 1671
    if-eqz p3, :cond_9

    .line 1672
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementActiveCount()V

    :cond_9
    move v3, v2

    move v2, v1

    .line 1674
    goto :goto_2

    .line 1668
    :cond_a
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v3

    new-instance v4, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    invoke-direct {v4, p2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->addLast(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 1677
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1699
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v1

    goto :goto_3

    :cond_b
    move v2, v1

    goto/16 :goto_2
.end method

.method private allocate()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 1282
    const/4 v3, 0x0

    .line 1284
    monitor-enter p0

    .line 1285
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit p0

    .line 1354
    :cond_0
    :goto_0
    return-void

    .line 1287
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 1289
    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1291
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;

    .line 1292
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getkey()Ljava/lang/Object;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$400(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1293
    if-nez v1, :cond_9

    .line 1294
    new-instance v1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v4}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;-><init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$1;)V

    .line 1295
    iget-object v4, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getkey()Ljava/lang/Object;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$400(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1296
    iget-object v4, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getkey()Ljava/lang/Object;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$400(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->add(Ljava/lang/Object;)Z

    move-object v4, v1

    .line 1298
    :goto_2
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->setPool(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)V
    invoke-static {v0, v4}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$800(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)V

    .line 1299
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v4}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1300
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 1301
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v4}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->setPair(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V
    invoke-static {v0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$500(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V

    .line 1303
    invoke-virtual {v4}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->incrementInternalProcessingCount()V

    .line 1304
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    .line 1305
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1306
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1307
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1338
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1309
    :cond_3
    :try_start_3
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxTotal:I

    if-lez v1, :cond_4

    iget v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalActive:I

    iget v6, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    add-int/2addr v1, v6

    iget v6, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    add-int/2addr v1, v6

    iget v6, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxTotal:I

    if-lt v1, v6, :cond_4

    move v0, v2

    .line 1338
    :goto_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1343
    if-eqz v0, :cond_0

    .line 1352
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->clearOldest()V

    goto/16 :goto_0

    .line 1321
    :cond_4
    :try_start_4
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxActive:I

    if-ltz v1, :cond_5

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I
    invoke-static {v4}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1000(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v1

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I
    invoke-static {v4}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v6

    add-int/2addr v1, v6

    iget v6, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxActive:I

    if-ge v1, v6, :cond_7

    :cond_5
    iget v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxTotal:I

    if-ltz v1, :cond_6

    iget v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalActive:I

    iget v6, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    add-int/2addr v1, v6

    iget v6, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    add-int/2addr v1, v6

    iget v6, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxTotal:I

    if-ge v1, v6, :cond_7

    .line 1324
    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 1325
    const/4 v1, 0x1

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->setMayCreate(Z)V
    invoke-static {v0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$1200(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;Z)V

    .line 1326
    invoke-virtual {v4}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->incrementInternalProcessingCount()V

    .line 1327
    monitor-enter v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1328
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1329
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto/16 :goto_1

    :catchall_2
    move-exception v1

    :try_start_6
    monitor-exit v0

    throw v1

    .line 1331
    :cond_7
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxActive:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-gez v0, :cond_2

    :cond_8
    move v0, v3

    goto :goto_3

    :cond_9
    move-object v4, v1

    goto/16 :goto_2
.end method

.method private declared-synchronized calculateDeficit(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;Z)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool",
            "<TK;TV;>.ObjectQueue;Z)I"
        }
    .end annotation

    .prologue
    .line 2204
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->getMinIdle()I

    move-result v0

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2209
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->getMaxActive()I

    move-result v1

    if-lez v1, :cond_0

    .line 2210
    const/4 v1, 0x0

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->getMaxActive()I

    move-result v2

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I
    invoke-static {p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1000(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v3

    sub-int/2addr v2, v3

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v3

    sub-int/2addr v2, v3

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I
    invoke-static {p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2211
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2215
    :cond_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->getMaxTotal()I

    move-result v1

    if-lez v1, :cond_1

    .line 2216
    const/4 v1, 0x0

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->getMaxTotal()I

    move-result v2

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->getNumActive()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->getNumIdle()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2217
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2220
    :cond_1
    if-eqz p2, :cond_2

    if-lez v0, :cond_2

    .line 2221
    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->incrementInternalProcessingCount()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2223
    :cond_2
    monitor-exit p0

    return v0

    .line 2204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private destroy(Ljava/util/Map;Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<TV;>;>;>;",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1488
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1489
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1490
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 1491
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1492
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1494
    :try_start_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    iget-object v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {p2, v2, v0}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1499
    monitor-enter p0

    .line 1500
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1502
    if-eqz v0, :cond_7

    .line 1503
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementInternalProcessingCount()V

    .line 1504
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v4

    if-nez v4, :cond_1

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1000(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v4

    if-nez v4, :cond_1

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1507
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1508
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->remove(Ljava/lang/Object;)Z

    .line 1513
    :cond_1
    :goto_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 1514
    :goto_2
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    goto :goto_0

    .line 1499
    :catchall_0
    move-exception v0

    move-object v1, v0

    monitor-enter p0

    .line 1500
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1502
    if-eqz v0, :cond_6

    .line 1503
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementInternalProcessingCount()V

    .line 1504
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v3

    if-nez v3, :cond_2

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1000(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v3

    if-nez v3, :cond_2

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1507
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1508
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->remove(Ljava/lang/Object;)Z

    .line 1513
    :cond_2
    :goto_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1514
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 1499
    throw v1

    .line 1519
    :cond_3
    return-void

    :catch_0
    move-exception v0

    .line 1499
    monitor-enter p0

    .line 1500
    :try_start_3
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1502
    if-eqz v0, :cond_5

    .line 1503
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementInternalProcessingCount()V

    .line 1504
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v4

    if-nez v4, :cond_4

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1000(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v4

    if-nez v4, :cond_4

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1507
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1508
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->remove(Ljava/lang/Object;)Z

    .line 1513
    :cond_4
    :goto_4
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1511
    :cond_5
    :try_start_4
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_4

    .line 1513
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1511
    :cond_6
    :try_start_5
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_3

    .line 1513
    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1511
    :cond_7
    :try_start_6
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto/16 :goto_1
.end method

.method private ensureMinIdle()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 2080
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_minIdle:I

    if-lez v0, :cond_0

    .line 2082
    monitor-enter p0

    .line 2084
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    .line 2085
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2090
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 2092
    aget-object v2, v1, v0

    invoke-direct {p0, v2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->ensureMinIdle(Ljava/lang/Object;)V

    .line 2090
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2085
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2095
    :cond_0
    return-void
.end method

.method private ensureMinIdle(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2111
    monitor-enter p0

    .line 2112
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 2113
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2114
    if-nez v0, :cond_1

    .line 2135
    :cond_0
    return-void

    .line 2113
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2123
    :cond_1
    invoke-direct {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->calculateDeficit(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;Z)I

    move-result v2

    .line 2125
    :goto_0
    if-ge v1, v2, :cond_0

    const/4 v3, 0x1

    invoke-direct {p0, v0, v3}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->calculateDeficit(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;Z)I

    move-result v3

    if-lez v3, :cond_0

    .line 2127
    :try_start_1
    invoke-virtual {p0, p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->addObject(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2129
    monitor-enter p0

    .line 2130
    :try_start_2
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementInternalProcessingCount()V

    .line 2131
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 2132
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 2125
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2129
    :catchall_1
    move-exception v1

    monitor-enter p0

    .line 2130
    :try_start_3
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementInternalProcessingCount()V

    .line 2131
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2132
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 2129
    throw v1

    .line 2131
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getNumTests()I
    .locals 4

    .prologue
    .line 2184
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_numTestsPerEvictionRun:I

    if-ltz v0, :cond_0

    .line 2185
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_numTestsPerEvictionRun:I

    iget v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2187
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    int-to-double v0, v0

    iget v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_numTestsPerEvictionRun:I

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0

    .line 2184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private resetEvictionKeyCursor()V
    .locals 1

    .prologue
    .line 2041
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-eqz v0, :cond_0

    .line 2042
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->close()V

    .line 2044
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->cursor()Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 2045
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-eqz v0, :cond_1

    .line 2046
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->close()V

    .line 2047
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 2049
    :cond_1
    return-void
.end method

.method private resetEvictionObjectCursor(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2057
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-eqz v0, :cond_0

    .line 2058
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->close()V

    .line 2060
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 2068
    :cond_1
    :goto_0
    return-void

    .line 2063
    :cond_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 2064
    if-eqz v0, :cond_1

    .line 2065
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v1

    .line 2066
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v0

    :goto_1
    invoke-virtual {v1, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->cursor(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public addObject(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1744
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->assertOpen()V

    .line 1745
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    if-nez v0, :cond_0

    .line 1746
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add objects without a factory."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1748
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->makeObject(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1750
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->assertOpen()V

    .line 1751
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->addObjectToPool(Ljava/lang/Object;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1759
    return-void

    .line 1752
    :catch_0
    move-exception v0

    .line 1754
    :try_start_1
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v2, p1, v1}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1758
    :goto_0
    throw v0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public borrowObject(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1093
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1094
    new-instance v5, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;

    const/4 v0, 0x0

    invoke-direct {v5, p0, p1, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;-><init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Ljava/lang/Object;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$1;)V

    .line 1097
    monitor-enter p0

    .line 1101
    :try_start_0
    iget-byte v8, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_whenExhaustedAction:B

    .line 1102
    iget-wide v10, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxWait:J

    .line 1105
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1106
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1109
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 1112
    :cond_0
    monitor-enter p0

    .line 1113
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->assertOpen()V

    .line 1114
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1116
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1118
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1122
    packed-switch v8, :pswitch_data_0

    .line 1211
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "whenExhaustedAction "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not recognized."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1106
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1114
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1125
    :pswitch_0
    monitor-enter p0

    .line 1128
    :try_start_2
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_1

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1129
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1130
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPool()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$300(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->incrementInternalProcessingCount()V

    .line 1132
    :cond_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1218
    :cond_2
    :goto_0
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_f

    .line 1220
    :try_start_3
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->makeObject(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1221
    new-instance v1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    invoke-direct {v1, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;-><init>(Ljava/lang/Object;)V

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->setPair(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V
    invoke-static {v5, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$500(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_7

    move v2, v3

    .line 1237
    :goto_1
    :try_start_4
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v0, p1, v1}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->activateObject(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1238
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testOnBorrow:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v0, p1, v1}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->validateObject(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 1239
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "ValidateObject failed"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 1246
    :catch_0
    move-exception v0

    .line 1247
    invoke-static {v0}, Lorg/apache/commons/pool/PoolUtils;->checkRethrow(Ljava/lang/Throwable;)V

    .line 1250
    :try_start_5
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v9

    iget-object v9, v9, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v1, p1, v9}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    .line 1255
    :goto_2
    monitor-enter p0

    .line 1256
    :try_start_6
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPool()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$300(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementInternalProcessingCount()V

    .line 1257
    if-nez v2, :cond_3

    .line 1258
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->reset()V
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$600(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)V

    .line 1259
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    const/4 v9, 0x0

    invoke-virtual {v1, v9, v5}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 1261
    :cond_3
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_a

    .line 1262
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 1263
    if-eqz v2, :cond_0

    .line 1264
    new-instance v1, Ljava/util/NoSuchElementException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not create a validated object, cause: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1132
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1133
    :pswitch_1
    monitor-enter p0

    .line 1138
    :try_start_7
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_4

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1139
    :cond_4
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto/16 :goto_0

    .line 1142
    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1141
    :cond_5
    :try_start_8
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1142
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 1143
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Pool exhausted"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1146
    :pswitch_2
    :try_start_9
    monitor-enter v5
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_1

    .line 1149
    :try_start_a
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_9

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1150
    cmp-long v0, v10, v12

    if-gtz v0, :cond_8

    .line 1151
    invoke-virtual {v5}, Ljava/lang/Object;->wait()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 1165
    :cond_6
    :goto_3
    :try_start_b
    monitor-exit v5

    .line 1167
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->isClosed()Z

    move-result v0

    if-ne v0, v3, :cond_c

    .line 1168
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Pool closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_1

    .line 1170
    :catch_1
    move-exception v0

    .line 1172
    monitor-enter p0

    .line 1174
    :try_start_c
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v1

    if-nez v1, :cond_a

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1177
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v1, v5}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1189
    :goto_4
    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    .line 1190
    if-eqz v4, :cond_7

    .line 1191
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 1193
    :cond_7
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 1194
    throw v0

    .line 1155
    :cond_8
    :try_start_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v6

    .line 1156
    sub-long v0, v10, v0

    .line 1157
    cmp-long v2, v0, v12

    if-lez v2, :cond_6

    .line 1159
    invoke-virtual {v5, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    goto :goto_3

    .line 1165
    :catchall_4
    move-exception v0

    :try_start_e
    monitor-exit v5

    throw v0
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_1

    .line 1163
    :cond_9
    :try_start_f
    monitor-exit v5
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    goto/16 :goto_0

    .line 1178
    :cond_a
    :try_start_10
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v1

    if-nez v1, :cond_b

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1181
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPool()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$300(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementInternalProcessingCount()V

    move v4, v3

    .line 1182
    goto :goto_4

    .line 1185
    :cond_b
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPool()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$300(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementInternalProcessingCount()V

    .line 1186
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPool()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$300(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->incrementActiveCount()V

    .line 1187
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getkey()Ljava/lang/Object;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$400(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Ljava/lang/Object;

    move-result-object v1

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->returnObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    goto :goto_4

    .line 1189
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1196
    :cond_c
    cmp-long v0, v10, v12

    if-lez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v6

    cmp-long v0, v0, v10

    if-ltz v0, :cond_0

    .line 1197
    monitor-enter p0

    .line 1200
    :try_start_11
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    if-nez v0, :cond_d

    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->mayCreate()Z
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$200(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1201
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    .line 1205
    monitor-exit p0

    .line 1206
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Timeout waiting for idle object"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1203
    :cond_d
    :try_start_12
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_6

    goto/16 :goto_0

    .line 1205
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1226
    :catchall_7
    move-exception v0

    monitor-enter p0

    .line 1227
    :try_start_13
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPool()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$300(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementInternalProcessingCount()V

    .line 1229
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_8

    .line 1230
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    throw v0

    .line 1229
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1241
    :cond_e
    :try_start_14
    monitor-enter p0
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_0

    .line 1242
    :try_start_15
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPool()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$300(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementInternalProcessingCount()V

    .line 1243
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPool()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$300(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->incrementActiveCount()V

    .line 1244
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_9

    .line 1245
    :try_start_16
    # invokes: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    invoke-static {v5}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    return-object v0

    .line 1244
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_0

    .line 1251
    :catch_2
    move-exception v1

    invoke-static {v1}, Lorg/apache/commons/pool/PoolUtils;->checkRethrow(Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 1261
    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_f
    move v2, v4

    goto/16 :goto_1

    .line 1122
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public clear()V
    .locals 6

    .prologue
    .line 1373
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1374
    monitor-enter p0

    .line 1375
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1376
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1377
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1380
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1381
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1382
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1383
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 1384
    iget-object v4, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v4, v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->remove(Ljava/lang/Object;)Z

    .line 1385
    iget v3, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    .line 1386
    iget v3, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    .line 1388
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1390
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1391
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-direct {p0, v1, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->destroy(Ljava/util/Map;Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V

    .line 1392
    return-void
.end method

.method public clear(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    .line 1457
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1460
    monitor-enter p0

    .line 1461
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1462
    if-nez v0, :cond_0

    .line 1463
    monitor-exit p0

    .line 1478
    :goto_0
    return-void

    .line 1465
    :cond_0
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v2, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->remove(Ljava/lang/Object;)Z

    .line 1469
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1470
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1471
    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1472
    iget v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    .line 1473
    iget v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    .line 1475
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->clear()V

    .line 1476
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1477
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-direct {p0, v1, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->destroy(Ljava/util/Map;Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V

    goto :goto_0

    .line 1476
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public clearOldest()V
    .locals 10

    .prologue
    .line 1402
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1405
    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    .line 1406
    monitor-enter p0

    .line 1407
    :try_start_0
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1408
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 1409
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v2

    .line 1410
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1414
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v3, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1446
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1419
    :cond_1
    :try_start_1
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    .line 1420
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v2

    int-to-double v2, v2

    const-wide v8, 0x3fc3333333333333L    # 0.15

    mul-double/2addr v2, v8

    double-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    .line 1422
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v5, v2

    .line 1423
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    if-lez v5, :cond_3

    .line 1424
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1428
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    .line 1429
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-object v4, v0

    .line 1430
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1431
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v8

    .line 1432
    invoke-interface {v8, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1434
    invoke-interface {v6, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1435
    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1441
    :goto_2
    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->incrementInternalProcessingCount()V

    .line 1442
    iget v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    .line 1443
    add-int/lit8 v2, v5, -0x1

    move v5, v2

    .line 1444
    goto :goto_1

    .line 1437
    :cond_2
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1438
    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1439
    invoke-interface {v6, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1446
    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1447
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-direct {p0, v6, v2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->destroy(Ljava/util/Map;Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V

    .line 1448
    return-void
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1805
    invoke-super {p0}, Lorg/apache/commons/pool/BaseKeyedObjectPool;->close()V

    .line 1806
    monitor-enter p0

    .line 1807
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->clear()V

    .line 1808
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-eqz v0, :cond_0

    .line 1809
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->close()V

    .line 1810
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 1812
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-eqz v0, :cond_1

    .line 1813
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->close()V

    .line 1814
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 1816
    :cond_1
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->startEvictor(J)V

    .line 1818
    :goto_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 1819
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_allocationQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;

    .line 1821
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1823
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1824
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1825
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    return-void
.end method

.method declared-synchronized debugInfo()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2164
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 2165
    const-string v1, "Active: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->getNumActive()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2166
    const-string v1, "Idle: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->getNumIdle()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2167
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2168
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2169
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2170
    const-string v3, "\t"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2172
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public evict()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1888
    const/4 v1, 0x0

    .line 1892
    monitor-enter p0

    .line 1896
    :try_start_0
    iget-boolean v5, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testWhileIdle:Z

    .line 1897
    iget-wide v6, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_minEvictableIdleTimeMillis:J

    .line 1900
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    iget-object v0, v0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_lastReturned:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    if-eqz v0, :cond_0

    .line 1902
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    iget-object v0, v0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_lastReturned:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v1

    .line 1904
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1906
    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->getNumTests()I

    move-result v8

    move v4, v0

    move-object v0, v1

    :goto_0
    if-ge v4, v8, :cond_14

    .line 1908
    monitor-enter p0

    .line 1910
    :try_start_1
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 1911
    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1906
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 1904
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1915
    :cond_2
    :try_start_2
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-nez v1, :cond_3

    .line 1916
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->resetEvictionKeyCursor()V

    .line 1917
    const/4 v0, 0x0

    .line 1921
    :cond_3
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-nez v1, :cond_4

    .line 1923
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1924
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1925
    invoke-direct {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->resetEvictionObjectCursor(Ljava/lang/Object;)V

    .line 1938
    :cond_4
    :goto_2
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-nez v1, :cond_6

    .line 1939
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 1976
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1928
    :cond_5
    :try_start_3
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->resetEvictionKeyCursor()V

    .line 1929
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-eqz v1, :cond_4

    .line 1930
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1931
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1932
    invoke-direct {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->resetEvictionObjectCursor(Ljava/lang/Object;)V

    goto :goto_2

    .line 1944
    :cond_6
    iget-boolean v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    iget-boolean v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->hasNext()Z

    move-result v1

    if-nez v1, :cond_c

    .line 1946
    :cond_8
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-eqz v1, :cond_c

    .line 1947
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1948
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1949
    invoke-direct {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->resetEvictionObjectCursor(Ljava/lang/Object;)V

    move-object v1, v0

    .line 1962
    :goto_3
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->hasNext()Z

    move-result v0

    if-nez v0, :cond_d

    .line 1964
    :cond_a
    monitor-exit p0

    move-object v0, v1

    goto/16 :goto_1

    .line 1951
    :cond_b
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->resetEvictionKeyCursor()V

    .line 1952
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    if-eqz v1, :cond_c

    .line 1953
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1954
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionKeyCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1955
    invoke-direct {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->resetEvictionObjectCursor(Ljava/lang/Object;)V

    :cond_c
    move-object v1, v0

    goto :goto_3

    .line 1969
    :cond_d
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-object v3, v0

    .line 1972
    :goto_4
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->remove()V

    .line 1973
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1974
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->incrementInternalProcessingCount()V

    .line 1975
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    .line 1976
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1978
    const/4 v0, 0x0

    .line 1979
    const-wide/16 v10, 0x0

    cmp-long v2, v6, v10

    if-lez v2, :cond_e

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iget-wide v12, v3, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->tstamp:J

    sub-long/2addr v10, v12

    cmp-long v2, v10, v6

    if-lez v2, :cond_e

    .line 1982
    const/4 v0, 0x1

    .line 1984
    :cond_e
    if-eqz v5, :cond_15

    if-nez v0, :cond_15

    .line 1985
    const/4 v2, 0x0

    .line 1987
    :try_start_4
    iget-object v9, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    iget-object v10, v3, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v9, v1, v10}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->activateObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 1988
    const/4 v2, 0x1

    .line 1992
    :goto_5
    if-eqz v2, :cond_15

    .line 1993
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    iget-object v9, v3, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v2, v1, v9}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->validateObject(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 1994
    const/4 v0, 0x1

    move v2, v0

    .line 2005
    :goto_6
    if-eqz v2, :cond_f

    .line 2007
    :try_start_5
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    iget-object v9, v3, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v0, v1, v9}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 2012
    :cond_f
    :goto_7
    monitor-enter p0

    .line 2013
    :try_start_6
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 2015
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementInternalProcessingCount()V

    .line 2016
    if-eqz v2, :cond_13

    .line 2017
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_10

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1000(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v2

    if-nez v2, :cond_10

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v0

    if-nez v0, :cond_10

    .line 2020
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2021
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->remove(Ljava/lang/Object;)Z

    .line 2031
    :cond_10
    :goto_8
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-object v0, v1

    goto/16 :goto_1

    .line 1969
    :cond_11
    :try_start_7
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v3, v0

    goto/16 :goto_4

    .line 1990
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    goto :goto_5

    .line 1997
    :cond_12
    :try_start_8
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    iget-object v9, v3, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;->value:Ljava/lang/Object;

    invoke-interface {v2, v1, v9}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->passivateObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    move v2, v0

    .line 2000
    goto :goto_6

    .line 1999
    :catch_1
    move-exception v0

    const/4 v0, 0x1

    move v2, v0

    goto :goto_6

    .line 2024
    :cond_13
    :try_start_9
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0, v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->add(Ljava/lang/Object;)V

    .line 2025
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    .line 2026
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z

    if-eqz v0, :cond_10

    .line 2028
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictionCursor:Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->previous()Ljava/lang/Object;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_8

    .line 2031
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2033
    :cond_14
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 2034
    return-void

    :catch_2
    move-exception v0

    goto :goto_7

    :cond_15
    move v2, v0

    goto :goto_6
.end method

.method public declared-synchronized getLifo()Z
    .locals 1

    .prologue
    .line 1041
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMaxActive()I
    .locals 1

    .prologue
    .line 653
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxActive:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMaxIdle()I
    .locals 1

    .prologue
    .line 786
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxIdle:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMaxTotal()I
    .locals 1

    .prologue
    .line 677
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxTotal:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMaxWait()J
    .locals 2

    .prologue
    .line 754
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxWait:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMinEvictableIdleTimeMillis()J
    .locals 2

    .prologue
    .line 963
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_minEvictableIdleTimeMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMinIdle()I
    .locals 1

    .prologue
    .line 836
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_minIdle:I

    return v0
.end method

.method public declared-synchronized getNumActive()I
    .locals 1

    .prologue
    .line 1528
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalActive:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumActive(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1550
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1551
    if-eqz v0, :cond_0

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1000(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1550
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumIdle()I
    .locals 1

    .prologue
    .line 1538
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumIdle(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1562
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1563
    if-eqz v0, :cond_0

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1562
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumTestsPerEvictionRun()I
    .locals 1

    .prologue
    .line 931
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_numTestsPerEvictionRun:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTestOnBorrow()Z
    .locals 1

    .prologue
    .line 851
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testOnBorrow:Z

    return v0
.end method

.method public getTestOnReturn()Z
    .locals 1

    .prologue
    .line 879
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testOnReturn:Z

    return v0
.end method

.method public declared-synchronized getTestWhileIdle()Z
    .locals 1

    .prologue
    .line 993
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testWhileIdle:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTimeBetweenEvictionRunsMillis()J
    .locals 2

    .prologue
    .line 905
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_timeBetweenEvictionRunsMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getWhenExhaustedAction()B
    .locals 1

    .prologue
    .line 709
    monitor-enter p0

    :try_start_0
    iget-byte v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_whenExhaustedAction:B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public invalidateObject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1717
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1719
    monitor-enter p0

    .line 1720
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1721
    if-nez v0, :cond_0

    .line 1722
    new-instance v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;-><init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$1;)V

    .line 1723
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1724
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v1, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->add(Ljava/lang/Object;)Z

    .line 1726
    :cond_0
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementActiveCount()V

    .line 1727
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1728
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 1729
    return-void

    .line 1719
    :catchall_0
    move-exception v0

    move-object v1, v0

    monitor-enter p0

    .line 1720
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1721
    if-nez v0, :cond_1

    .line 1722
    new-instance v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;-><init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$1;)V

    .line 1723
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1724
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v2, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->add(Ljava/lang/Object;)Z

    .line 1726
    :cond_1
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementActiveCount()V

    .line 1727
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1728
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 1719
    throw v1

    .line 1727
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized preparePool(Ljava/lang/Object;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)V"
        }
    .end annotation

    .prologue
    .line 1775
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1776
    if-nez v0, :cond_0

    .line 1777
    new-instance v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;-><init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$1;)V

    .line 1778
    iget-object v1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1779
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1782
    :cond_0
    if-eqz p2, :cond_1

    .line 1785
    :try_start_1
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->ensureMinIdle(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1791
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 1775
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public returnObject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1587
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->addObjectToPool(Ljava/lang/Object;Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1613
    :cond_0
    :goto_0
    return-void

    .line 1589
    :catch_0
    move-exception v0

    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    if-eqz v0, :cond_0

    .line 1591
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1598
    :goto_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1599
    if-eqz v0, :cond_0

    .line 1600
    monitor-enter p0

    .line 1601
    :try_start_2
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->decrementActiveCount()V

    .line 1602
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->activeCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1000(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v1

    if-nez v1, :cond_1

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->internalProcessingCount:I
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$1100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1605
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1606
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->remove(Ljava/lang/Object;)Z

    .line 1608
    :cond_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1609
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    goto :goto_0

    .line 1608
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public declared-synchronized setConfig(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;)V
    .locals 2

    .prologue
    .line 1016
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxIdle:I

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMaxIdle(I)V

    .line 1017
    iget v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxActive:I

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMaxActive(I)V

    .line 1018
    iget v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxTotal:I

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMaxTotal(I)V

    .line 1019
    iget v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->minIdle:I

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMinIdle(I)V

    .line 1020
    iget-wide v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxWait:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMaxWait(J)V

    .line 1021
    iget-byte v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->whenExhaustedAction:B

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setWhenExhaustedAction(B)V

    .line 1022
    iget-boolean v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->testOnBorrow:Z

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setTestOnBorrow(Z)V

    .line 1023
    iget-boolean v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->testOnReturn:Z

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setTestOnReturn(Z)V

    .line 1024
    iget-boolean v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->testWhileIdle:Z

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setTestWhileIdle(Z)V

    .line 1025
    iget v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->numTestsPerEvictionRun:I

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setNumTestsPerEvictionRun(I)V

    .line 1026
    iget-wide v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->minEvictableIdleTimeMillis:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setMinEvictableIdleTimeMillis(J)V

    .line 1027
    iget-wide v0, p1, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->timeBetweenEvictionRunsMillis:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->setTimeBetweenEvictionRunsMillis(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1028
    monitor-exit p0

    return-void

    .line 1016
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setFactory(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1843
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1844
    iget-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    .line 1845
    monitor-enter p0

    .line 1846
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->assertOpen()V

    .line 1847
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->getNumActive()I

    move-result v0

    if-lez v0, :cond_0

    .line 1848
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Objects are already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1869
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1850
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1851
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 1852
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolMap:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    .line 1853
    if-eqz v0, :cond_1

    .line 1856
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1857
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1858
    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1859
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 1860
    iget-object v5, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_poolList:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v5, v4}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->remove(Ljava/lang/Object;)Z

    .line 1861
    iget v4, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalIdle:I

    .line 1862
    iget v4, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_totalInternalProcessing:I

    .line 1864
    # getter for: Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->queue:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;->access$900(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)Lorg/apache/commons/pool/impl/CursorableLinkedList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->clear()V

    goto :goto_0

    .line 1867
    :cond_2
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    .line 1869
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1870
    invoke-direct {p0, v1, v2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->destroy(Ljava/util/Map;Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V

    .line 1871
    return-void
.end method

.method public declared-synchronized setLifo(Z)V
    .locals 1

    .prologue
    .line 1055
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_lifo:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1056
    monitor-exit p0

    return-void

    .line 1055
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMaxActive(I)V
    .locals 1

    .prologue
    .line 664
    monitor-enter p0

    .line 665
    :try_start_0
    iput p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxActive:I

    .line 666
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 668
    return-void

    .line 666
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMaxIdle(I)V
    .locals 1

    .prologue
    .line 804
    monitor-enter p0

    .line 805
    :try_start_0
    iput p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxIdle:I

    .line 806
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 807
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 808
    return-void

    .line 806
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMaxTotal(I)V
    .locals 1

    .prologue
    .line 693
    monitor-enter p0

    .line 694
    :try_start_0
    iput p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxTotal:I

    .line 695
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 696
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 697
    return-void

    .line 695
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMaxWait(J)V
    .locals 1

    .prologue
    .line 773
    monitor-enter p0

    .line 774
    :try_start_0
    iput-wide p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_maxWait:J

    .line 775
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 776
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 777
    return-void

    .line 775
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setMinEvictableIdleTimeMillis(J)V
    .locals 1

    .prologue
    .line 979
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_minEvictableIdleTimeMillis:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 980
    monitor-exit p0

    return-void

    .line 979
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMinIdle(I)V
    .locals 0

    .prologue
    .line 822
    iput p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_minIdle:I

    .line 823
    return-void
.end method

.method public declared-synchronized setNumTestsPerEvictionRun(I)V
    .locals 1

    .prologue
    .line 950
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_numTestsPerEvictionRun:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 951
    monitor-exit p0

    return-void

    .line 950
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setTestOnBorrow(Z)V
    .locals 0

    .prologue
    .line 866
    iput-boolean p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testOnBorrow:Z

    .line 867
    return-void
.end method

.method public setTestOnReturn(Z)V
    .locals 0

    .prologue
    .line 892
    iput-boolean p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testOnReturn:Z

    .line 893
    return-void
.end method

.method public declared-synchronized setTestWhileIdle(Z)V
    .locals 1

    .prologue
    .line 1007
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_testWhileIdle:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1008
    monitor-exit p0

    return-void

    .line 1007
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setTimeBetweenEvictionRunsMillis(J)V
    .locals 3

    .prologue
    .line 918
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_timeBetweenEvictionRunsMillis:J

    .line 919
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_timeBetweenEvictionRunsMillis:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->startEvictor(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 920
    monitor-exit p0

    return-void

    .line 918
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setWhenExhaustedAction(B)V
    .locals 3

    .prologue
    .line 723
    monitor-enter p0

    .line 724
    packed-switch p1, :pswitch_data_0

    .line 731
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "whenExhaustedAction "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not recognized."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 733
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 728
    :pswitch_0
    :try_start_1
    iput-byte p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_whenExhaustedAction:B

    .line 733
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 734
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->allocate()V

    .line 735
    return-void

    .line 724
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected declared-synchronized startEvictor(J)V
    .locals 3

    .prologue
    .line 2147
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;

    if-eqz v0, :cond_0

    .line 2148
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;

    invoke-static {v0}, Lorg/apache/commons/pool/impl/EvictionTimer;->cancel(Ljava/util/TimerTask;)V

    .line 2149
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;

    .line 2151
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 2152
    new-instance v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;-><init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$1;)V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;

    .line 2153
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;->_evictor:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Evictor;

    invoke-static {v0, p1, p2, p1, p2}, Lorg/apache/commons/pool/impl/EvictionTimer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2155
    :cond_1
    monitor-exit p0

    return-void

    .line 2147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
