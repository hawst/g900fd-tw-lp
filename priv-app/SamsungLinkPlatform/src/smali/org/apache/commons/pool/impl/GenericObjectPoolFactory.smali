.class public Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/ObjectPoolFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/commons/pool/ObjectPoolFactory",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected _factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _lifo:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _maxActive:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _maxIdle:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _maxWait:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _minEvictableIdleTimeMillis:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _minIdle:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _numTestsPerEvictionRun:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _softMinEvictableIdleTimeMillis:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _testOnBorrow:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _testOnReturn:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _testWhileIdle:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _timeBetweenEvictionRunsMillis:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _whenExhaustedAction:B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    const/16 v2, 0x8

    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 45
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;I)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 67
    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 68
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJ)V"
        }
    .end annotation

    .prologue
    .line 80
    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 81
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJI)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJI)V"
        }
    .end annotation

    .prologue
    .line 109
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 110
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJIIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 166
    const-wide/16 v16, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-wide/from16 v10, p10

    move/from16 v12, p12

    move-wide/from16 v13, p13

    move/from16 v15, p15

    invoke-direct/range {v0 .. v17}, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJ)V

    .line 167
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJ)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJIIZZJIJZJ)V"
        }
    .end annotation

    .prologue
    .line 189
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-wide/from16 v10, p10

    move/from16 v12, p12

    move-wide/from16 v13, p13

    move/from16 v15, p15

    move-wide/from16 v16, p16

    invoke-direct/range {v0 .. v18}, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJZ)V

    .line 190
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJIIZZJIJZJZ)V"
        }
    .end annotation

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 360
    const/16 v2, 0x8

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxIdle:I

    .line 367
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_minIdle:I

    .line 374
    const/16 v2, 0x8

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxActive:I

    .line 381
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxWait:J

    .line 389
    const/4 v2, 0x1

    iput-byte v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_whenExhaustedAction:B

    .line 396
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testOnBorrow:Z

    .line 403
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testOnReturn:Z

    .line 410
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testWhileIdle:Z

    .line 418
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_timeBetweenEvictionRunsMillis:J

    .line 426
    const/4 v2, 0x3

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_numTestsPerEvictionRun:I

    .line 434
    const-wide/32 v2, 0x1b7740

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_minEvictableIdleTimeMillis:J

    .line 442
    const-wide/32 v2, 0x1b7740

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_softMinEvictableIdleTimeMillis:J

    .line 449
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_lifo:Z

    .line 456
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 213
    iput p6, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxIdle:I

    .line 214
    iput p7, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_minIdle:I

    .line 215
    iput p2, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxActive:I

    .line 216
    iput-wide p4, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxWait:J

    .line 217
    iput-byte p3, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_whenExhaustedAction:B

    .line 218
    iput-boolean p8, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testOnBorrow:Z

    .line 219
    iput-boolean p9, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testOnReturn:Z

    .line 220
    move/from16 v0, p15

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testWhileIdle:Z

    .line 221
    iput-wide p10, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_timeBetweenEvictionRunsMillis:J

    .line 222
    move/from16 v0, p12

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_numTestsPerEvictionRun:I

    .line 223
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_minEvictableIdleTimeMillis:J

    .line 224
    move-wide/from16 v0, p16

    iput-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_softMinEvictableIdleTimeMillis:J

    .line 225
    move/from16 v0, p18

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_lifo:Z

    .line 226
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 227
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIZZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJIZZ)V"
        }
    .end annotation

    .prologue
    .line 125
    const/4 v7, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 126
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIZZJIJZ)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 145
    const/4 v7, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-wide/from16 v10, p9

    move/from16 v12, p11

    move-wide/from16 v13, p12

    move/from16 v15, p14

    invoke-direct/range {v0 .. v17}, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJ)V

    .line 146
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJZZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;IBJZZ)V"
        }
    .end annotation

    .prologue
    .line 95
    const/16 v6, 0x8

    const/4 v7, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZ)V

    .line 96
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;Lorg/apache/commons/pool/impl/GenericObjectPool$Config;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;",
            "Lorg/apache/commons/pool/impl/GenericObjectPool$Config;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 56
    move-object/from16 v0, p2

    iget v4, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->maxActive:I

    move-object/from16 v0, p2

    iget-byte v5, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->whenExhaustedAction:B

    move-object/from16 v0, p2

    iget-wide v6, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->maxWait:J

    move-object/from16 v0, p2

    iget v8, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->maxIdle:I

    move-object/from16 v0, p2

    iget v9, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->minIdle:I

    move-object/from16 v0, p2

    iget-boolean v10, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->testOnBorrow:Z

    move-object/from16 v0, p2

    iget-boolean v11, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->testOnReturn:Z

    move-object/from16 v0, p2

    iget-wide v12, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->timeBetweenEvictionRunsMillis:J

    move-object/from16 v0, p2

    iget v14, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->numTestsPerEvictionRun:I

    move-object/from16 v0, p2

    iget-wide v15, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->minEvictableIdleTimeMillis:J

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->testWhileIdle:Z

    move/from16 v17, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->softMinEvictableIdleTimeMillis:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericObjectPool$Config;->lifo:Z

    move/from16 v20, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v20}, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJZ)V

    .line 57
    return-void
.end method


# virtual methods
.method public createPool()Lorg/apache/commons/pool/ObjectPool;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 233
    new-instance v2, Lorg/apache/commons/pool/impl/GenericObjectPool;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxActive:I

    move-object/from16 v0, p0

    iget-byte v5, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_whenExhaustedAction:B

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxWait:J

    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxIdle:I

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_minIdle:I

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testOnBorrow:Z

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testOnReturn:Z

    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_timeBetweenEvictionRunsMillis:J

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_numTestsPerEvictionRun:I

    move-object/from16 v0, p0

    iget-wide v15, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_minEvictableIdleTimeMillis:J

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testWhileIdle:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_softMinEvictableIdleTimeMillis:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_lifo:Z

    move/from16 v20, v0

    invoke-direct/range {v2 .. v20}, Lorg/apache/commons/pool/impl/GenericObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;IBJIIZZJIJZJZ)V

    return-object v2
.end method

.method public getFactory()Lorg/apache/commons/pool/PoolableObjectFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 353
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    return-object v0
.end method

.method public getLifo()Z
    .locals 1

    .prologue
    .line 346
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_lifo:Z

    return v0
.end method

.method public getMaxActive()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxActive:I

    return v0
.end method

.method public getMaxIdle()I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxIdle:I

    return v0
.end method

.method public getMaxWait()J
    .locals 2

    .prologue
    .line 266
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_maxWait:J

    return-wide v0
.end method

.method public getMinEvictableIdleTimeMillis()J
    .locals 2

    .prologue
    .line 329
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_minEvictableIdleTimeMillis:J

    return-wide v0
.end method

.method public getMinIdle()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_minIdle:I

    return v0
.end method

.method public getNumTestsPerEvictionRun()I
    .locals 1

    .prologue
    .line 320
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_numTestsPerEvictionRun:I

    return v0
.end method

.method public getSoftMinEvictableIdleTimeMillis()J
    .locals 2

    .prologue
    .line 338
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_softMinEvictableIdleTimeMillis:J

    return-wide v0
.end method

.method public getTestOnBorrow()Z
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testOnBorrow:Z

    return v0
.end method

.method public getTestOnReturn()Z
    .locals 1

    .prologue
    .line 293
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testOnReturn:Z

    return v0
.end method

.method public getTestWhileIdle()Z
    .locals 1

    .prologue
    .line 302
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_testWhileIdle:Z

    return v0
.end method

.method public getTimeBetweenEvictionRunsMillis()J
    .locals 2

    .prologue
    .line 311
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_timeBetweenEvictionRunsMillis:J

    return-wide v0
.end method

.method public getWhenExhaustedAction()B
    .locals 1

    .prologue
    .line 275
    iget-byte v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPoolFactory;->_whenExhaustedAction:B

    return v0
.end method
