.class Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;
.super Ljava/util/TimerTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/PoolUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KeyedObjectPoolMinIdleTimerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/TimerTask;"
    }
.end annotation


# instance fields
.field private final key:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field private final keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final minIdle:I


# direct methods
.method constructor <init>(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Object;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;TK;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1475
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 1476
    if-nez p1, :cond_0

    .line 1477
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyedPool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1479
    :cond_0
    iput-object p1, p0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    .line 1480
    iput-object p2, p0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->key:Ljava/lang/Object;

    .line 1481
    iput p3, p0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->minIdle:I

    .line 1482
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1489
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->key:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/KeyedObjectPool;->getNumIdle(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->minIdle:I

    if-ge v0, v1, :cond_0

    .line 1492
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->key:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/KeyedObjectPool;->addObject(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1501
    :cond_0
    :goto_0
    return-void

    .line 1497
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {p0}, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->cancel()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1501
    invoke-virtual {p0}, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->cancel()Z

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 1502
    invoke-virtual {p0}, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->cancel()Z

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1512
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1513
    const-string v1, "KeyedObjectPoolMinIdleTimerTask"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1514
    const-string v1, "{minIdle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->minIdle:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1515
    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->key:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 1516
    const-string v1, ", keyedPool="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 1517
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1518
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
