.class public Lorg/apache/commons/pool/impl/StackObjectPoolFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/ObjectPoolFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/commons/pool/ObjectPoolFactory",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected _factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _initCapacity:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _maxSleeping:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 45
    const/4 v0, 0x0

    const/16 v1, 0x8

    const/4 v2, 0x4

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V

    .line 46
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 57
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V

    .line 58
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V

    .line 72
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 81
    const/16 v0, 0x8

    const/4 v1, 0x4

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V

    .line 82
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 91
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V

    .line 92
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;II)V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 128
    const/16 v0, 0x8

    iput v0, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_maxSleeping:I

    .line 135
    const/4 v0, 0x4

    iput v0, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_initCapacity:I

    .line 103
    iput-object p1, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 104
    iput p2, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_maxSleeping:I

    .line 105
    iput p3, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_initCapacity:I

    .line 106
    return-void
.end method


# virtual methods
.method public createPool()Lorg/apache/commons/pool/ObjectPool;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lorg/apache/commons/pool/impl/StackObjectPool;

    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    iget v2, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_maxSleeping:I

    iget v3, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_initCapacity:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/commons/pool/impl/StackObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V

    return-object v0
.end method

.method public getFactory()Lorg/apache/commons/pool/PoolableObjectFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    return-object v0
.end method

.method public getInitCapacity()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_initCapacity:I

    return v0
.end method

.method public getMaxSleeping()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lorg/apache/commons/pool/impl/StackObjectPoolFactory;->_maxSleeping:I

    return v0
.end method
