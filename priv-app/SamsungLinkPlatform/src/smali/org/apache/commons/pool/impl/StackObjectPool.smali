.class public Lorg/apache/commons/pool/impl/StackObjectPool;
.super Lorg/apache/commons/pool/BaseObjectPool;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/ObjectPool;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/commons/pool/BaseObjectPool",
        "<TT;>;",
        "Lorg/apache/commons/pool/ObjectPool",
        "<TT;>;"
    }
.end annotation


# static fields
.field protected static final DEFAULT_INIT_SLEEPING_CAPACITY:I = 0x4

.field protected static final DEFAULT_MAX_SLEEPING:I = 0x8


# instance fields
.field protected _factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _maxSleeping:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _numActive:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _pool:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 62
    const/4 v0, 0x0

    const/16 v1, 0x8

    const/4 v2, 0x4

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/commons/pool/impl/StackObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V

    .line 63
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 78
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/commons/pool/impl/StackObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V

    .line 79
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/commons/pool/impl/StackObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V

    .line 97
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 105
    const/16 v0, 0x8

    const/4 v1, 0x4

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/commons/pool/impl/StackObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V

    .line 106
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 116
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/commons/pool/impl/StackObjectPool;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V

    .line 117
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;II)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/16 v0, 0x8

    .line 134
    invoke-direct {p0}, Lorg/apache/commons/pool/BaseObjectPool;-><init>()V

    .line 426
    iput-object v1, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    .line 433
    iput-object v1, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 440
    iput v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_maxSleeping:I

    .line 447
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_numActive:I

    .line 135
    iput-object p1, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 136
    if-gez p2, :cond_0

    move p2, v0

    :cond_0
    iput p2, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_maxSleeping:I

    .line 137
    if-gtz p3, :cond_1

    const/4 p3, 0x4

    .line 138
    :cond_1
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    .line 139
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    iget v1, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_maxSleeping:I

    if-le p3, v1, :cond_2

    iget p3, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_maxSleeping:I

    :cond_2
    invoke-virtual {v0, p3}, Ljava/util/Stack;->ensureCapacity(I)V

    .line 140
    return-void
.end method


# virtual methods
.method public declared-synchronized addObject()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 353
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackObjectPool;->assertOpen()V

    .line 354
    iget-object v2, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-nez v2, :cond_0

    .line 355
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add objects without a factory."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 357
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v2}, Lorg/apache/commons/pool/PoolableObjectFactory;->makeObject()Ljava/lang/Object;

    move-result-object v2

    .line 360
    iget-object v3, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v3, v2}, Lorg/apache/commons/pool/PoolableObjectFactory;->validateObject(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v0

    .line 366
    :goto_0
    if-nez v3, :cond_1

    move v0, v1

    .line 368
    :cond_1
    if-eqz v3, :cond_5

    .line 369
    const/4 v3, 0x0

    .line 370
    iget-object v4, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->size()I

    move-result v4

    iget v5, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_maxSleeping:I

    if-lt v4, v5, :cond_4

    .line 372
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;

    move-result-object v0

    move v6, v1

    move-object v1, v0

    move v0, v6

    .line 374
    :goto_1
    iget-object v3, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    invoke-virtual {v3, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    :goto_2
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 379
    if-eqz v0, :cond_2

    .line 381
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 386
    :cond_2
    :goto_3
    monitor-exit p0

    return-void

    .line 363
    :cond_3
    :try_start_3
    iget-object v3, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v3, v2}, Lorg/apache/commons/pool/PoolableObjectFactory;->passivateObject(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v3, v1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_3

    :cond_4
    move-object v1, v3

    goto :goto_1

    :cond_5
    move-object v1, v2

    goto :goto_2
.end method

.method public declared-synchronized borrowObject()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 166
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackObjectPool;->assertOpen()V

    .line 168
    const/4 v0, 0x0

    move-object v1, v3

    .line 169
    :goto_0
    if-nez v1, :cond_5

    .line 170
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 171
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    move v2, v0

    .line 183
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    .line 185
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/PoolableObjectFactory;->activateObject(Ljava/lang/Object;)V

    .line 186
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/PoolableObjectFactory;->validateObject(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 187
    new-instance v0, Ljava/lang/Exception;

    const-string v4, "ValidateObject failed"

    invoke-direct {v0, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    :try_start_2
    invoke-static {v0}, Lorg/apache/commons/pool/PoolUtils;->checkRethrow(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 192
    :try_start_3
    iget-object v4, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v4, v1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 199
    :goto_1
    if-eqz v2, :cond_4

    .line 200
    :try_start_4
    new-instance v1, Ljava/util/NoSuchElementException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not create a validated object, cause: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 173
    :cond_1
    :try_start_5
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-nez v0, :cond_2

    .line 174
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 176
    :cond_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0}, Lorg/apache/commons/pool/PoolableObjectFactory;->makeObject()Ljava/lang/Object;

    move-result-object v1

    .line 177
    const/4 v2, 0x1

    .line 178
    if-nez v1, :cond_0

    .line 179
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "PoolableObjectFactory.makeObject() returned null."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    move v0, v2

    .line 204
    goto :goto_0

    .line 193
    :catch_1
    move-exception v1

    :try_start_6
    invoke-static {v1}, Lorg/apache/commons/pool/PoolUtils;->checkRethrow(Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 197
    :catchall_1
    move-exception v0

    :try_start_7
    throw v0

    :cond_4
    move v0, v2

    move-object v1, v3

    .line 204
    goto :goto_0

    .line 207
    :cond_5
    iget v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_numActive:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_numActive:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 208
    monitor-exit p0

    return-object v1

    :cond_6
    move v0, v2

    goto/16 :goto_0
.end method

.method public declared-synchronized clear()V
    .locals 3

    .prologue
    .line 304
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 306
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 308
    :try_start_1
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 311
    :catch_0
    move-exception v1

    goto :goto_0

    .line 314
    :cond_0
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 315
    monitor-exit p0

    return-void

    .line 304
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 329
    invoke-super {p0}, Lorg/apache/commons/pool/BaseObjectPool;->close()V

    .line 330
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackObjectPool;->clear()V

    .line 331
    return-void
.end method

.method public declared-synchronized getFactory()Lorg/apache/commons/pool/PoolableObjectFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 457
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMaxSleeping()I
    .locals 1

    .prologue
    .line 467
    iget v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_maxSleeping:I

    return v0
.end method

.method public declared-synchronized getNumActive()I
    .locals 1

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_numActive:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumIdle()I
    .locals 1

    .prologue
    .line 285
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized invalidateObject(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 270
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_numActive:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_numActive:I

    .line 271
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V

    .line 274
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    monitor-exit p0

    return-void

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized returnObject(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 229
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackObjectPool;->isClosed()Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v1

    .line 230
    :goto_0
    iget-object v3, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    if-eqz v3, :cond_0

    .line 231
    iget-object v3, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v3, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->validateObject(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v2, v0

    .line 242
    :cond_0
    :goto_1
    if-nez v2, :cond_1

    move v0, v1

    .line 244
    :cond_1
    iget v3, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_numActive:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_numActive:I

    .line 245
    if-eqz v2, :cond_2

    .line 246
    const/4 v2, 0x0

    .line 247
    iget-object v3, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v3

    iget v4, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_maxSleeping:I

    if-lt v3, v4, :cond_6

    .line 249
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;

    move-result-object v0

    move v5, v1

    move-object v1, v0

    move v0, v5

    .line 251
    :goto_2
    iget-object v2, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_pool:Ljava/util/Stack;

    invoke-virtual {v2, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-object p1, v1

    .line 254
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    if-eqz v0, :cond_3

    .line 258
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 263
    :cond_3
    :goto_3
    monitor-exit p0

    return-void

    :cond_4
    move v2, v0

    .line 229
    goto :goto_0

    .line 235
    :cond_5
    :try_start_2
    iget-object v3, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v3, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->passivateObject(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 237
    :catch_0
    move-exception v2

    move v2, v0

    goto :goto_1

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    goto :goto_3

    :cond_6
    move-object v1, v2

    goto :goto_2
.end method

.method public declared-synchronized setFactory(Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 401
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackObjectPool;->assertOpen()V

    .line 402
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackObjectPool;->getNumActive()I

    move-result v0

    if-lez v0, :cond_0

    .line 403
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Objects are already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 405
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackObjectPool;->clear()V

    .line 406
    iput-object p1, p0, Lorg/apache/commons/pool/impl/StackObjectPool;->_factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408
    monitor-exit p0

    return-void
.end method
