.class final Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/impl/GenericObjectPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Latch"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private _mayCreate:Z

.field private _pair:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1866
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->_mayCreate:Z

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/commons/pool/impl/GenericObjectPool$1;)V
    .locals 0

    .prologue
    .line 1860
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    .locals 1

    .prologue
    .line 1860
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)Z
    .locals 1

    .prologue
    .line 1860
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->mayCreate()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V
    .locals 0

    .prologue
    .line 1860
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->setPair(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V

    return-void
.end method

.method static synthetic access$400(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;)V
    .locals 0

    .prologue
    .line 1860
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->reset()V

    return-void
.end method

.method static synthetic access$500(Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;Z)V
    .locals 0

    .prologue
    .line 1860
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->setMayCreate(Z)V

    return-void
.end method

.method private declared-synchronized getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1873
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->_pair:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized mayCreate()Z
    .locals 1

    .prologue
    .line 1889
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->_mayCreate:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized reset()V
    .locals 1

    .prologue
    .line 1905
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->_pair:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    .line 1906
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->_mayCreate:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1907
    monitor-exit p0

    return-void

    .line 1905
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setMayCreate(Z)V
    .locals 1

    .prologue
    .line 1897
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->_mayCreate:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1898
    monitor-exit p0

    return-void

    .line 1897
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setPair(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1881
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool$Latch;->_pair:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1882
    monitor-exit p0

    return-void

    .line 1881
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
