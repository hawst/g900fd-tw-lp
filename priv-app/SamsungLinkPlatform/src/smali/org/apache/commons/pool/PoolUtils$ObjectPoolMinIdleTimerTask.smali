.class Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;
.super Ljava/util/TimerTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/PoolUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ObjectPoolMinIdleTimerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/TimerTask;"
    }
.end annotation


# instance fields
.field private final minIdle:I

.field private final pool:Lorg/apache/commons/pool/ObjectPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/apache/commons/pool/ObjectPool;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1406
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 1407
    if-nez p1, :cond_0

    .line 1408
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1410
    :cond_0
    iput-object p1, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;->pool:Lorg/apache/commons/pool/ObjectPool;

    .line 1411
    iput p2, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;->minIdle:I

    .line 1412
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1419
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/ObjectPool;->getNumIdle()I

    move-result v0

    iget v1, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;->minIdle:I

    if-ge v0, v1, :cond_0

    .line 1422
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/ObjectPool;->addObject()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1431
    :cond_0
    :goto_0
    return-void

    .line 1427
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {p0}, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;->cancel()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1431
    invoke-virtual {p0}, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;->cancel()Z

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 1432
    invoke-virtual {p0}, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;->cancel()Z

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1442
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1443
    const-string v1, "ObjectPoolMinIdleTimerTask"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1444
    const-string v1, "{minIdle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;->minIdle:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1445
    const-string v1, ", pool="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 1446
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1447
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
