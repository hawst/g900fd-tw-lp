.class public Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;
.super Lorg/apache/commons/pool/impl/CursorableLinkedList$ListIter;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/impl/CursorableLinkedList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Cursor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/commons/pool/impl/CursorableLinkedList",
        "<TE;>.",
        "ListIter;",
        "Ljava/util/ListIterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field _valid:Z

.field final synthetic this$0:Lorg/apache/commons/pool/impl/CursorableLinkedList;


# direct methods
.method constructor <init>(Lorg/apache/commons/pool/impl/CursorableLinkedList;I)V
    .locals 1

    .prologue
    .line 1125
    iput-object p1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->this$0:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    .line 1126
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$ListIter;-><init>(Lorg/apache/commons/pool/impl/CursorableLinkedList;I)V

    .line 1123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_valid:Z

    .line 1127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_valid:Z

    .line 1128
    invoke-virtual {p1, p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->registerCursor(Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;)V

    .line 1129
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 1143
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->checkForComod()V

    .line 1144
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->this$0:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->insertListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    .line 1145
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1146
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1147
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_lastReturned:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 1148
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_nextIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_nextIndex:I

    .line 1149
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_expectedModCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_expectedModCount:I

    .line 1150
    return-void
.end method

.method protected checkForComod()V
    .locals 1

    .prologue
    .line 1190
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_valid:Z

    if-nez v0, :cond_0

    .line 1191
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 1193
    :cond_0
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 1208
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_valid:Z

    if-eqz v0, :cond_0

    .line 1209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_valid:Z

    .line 1210
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->this$0:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0, p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->unregisterCursor(Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;)V

    .line 1212
    :cond_0
    return-void
.end method

.method public bridge synthetic hasNext()Z
    .locals 1

    .prologue
    .line 1122
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$ListIter;->hasNext()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic hasPrevious()Z
    .locals 1

    .prologue
    .line 1122
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$ListIter;->hasPrevious()Z

    move-result v0

    return v0
.end method

.method protected invalidate()V
    .locals 1

    .prologue
    .line 1196
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_valid:Z

    .line 1197
    return-void
.end method

.method protected listableChanged(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1183
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_lastReturned:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    if-ne v0, p1, :cond_0

    .line 1184
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_lastReturned:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 1186
    :cond_0
    return-void
.end method

.method protected listableInserted(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1169
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1170
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1174
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 1175
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1177
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_lastReturned:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    if-ne v0, p1, :cond_2

    .line 1178
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_lastReturned:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 1180
    :cond_2
    return-void

    .line 1171
    :cond_3
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1172
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    goto :goto_0
.end method

.method protected listableRemoved(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1153
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->this$0:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    iget-object v0, v0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1154
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1158
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->this$0:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    iget-object v0, v0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1159
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1163
    :cond_1
    :goto_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_lastReturned:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    if-ne v0, p1, :cond_2

    .line 1164
    iput-object v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_lastReturned:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 1166
    :cond_2
    return-void

    .line 1155
    :cond_3
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1156
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    goto :goto_0

    .line 1160
    :cond_4
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 1161
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->_cur:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    goto :goto_1
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1122
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$ListIter;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public nextIndex()I
    .locals 1

    .prologue
    .line 1138
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1122
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$ListIter;->previous()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .locals 1

    .prologue
    .line 1133
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic remove()V
    .locals 0

    .prologue
    .line 1122
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$ListIter;->remove()V

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1122
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$ListIter;->set(Ljava/lang/Object;)V

    return-void
.end method
