.class Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/KeyedObjectPool;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/PoolUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ErodingKeyedObjectPool"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/commons/pool/KeyedObjectPool",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final erodingFactor:Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

.field private final keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/commons/pool/KeyedObjectPool;F)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;F)V"
        }
    .end annotation

    .prologue
    .line 2238
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    invoke-direct {v0, p2}, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;-><init>(F)V

    invoke-direct {p0, p1, v0}, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedObjectPool;Lorg/apache/commons/pool/PoolUtils$ErodingFactor;)V

    .line 2239
    return-void
.end method

.method protected constructor <init>(Lorg/apache/commons/pool/KeyedObjectPool;Lorg/apache/commons/pool/PoolUtils$ErodingFactor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;",
            "Lorg/apache/commons/pool/PoolUtils$ErodingFactor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2249
    if-nez p1, :cond_0

    .line 2250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyedPool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2252
    :cond_0
    iput-object p1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    .line 2253
    iput-object p2, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->erodingFactor:Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    .line 2254
    return-void
.end method


# virtual methods
.method public addObject(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2334
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->addObject(Ljava/lang/Object;)V

    .line 2335
    return-void
.end method

.method public borrowObject(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/util/NoSuchElementException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 2260
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->borrowObject(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2369
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/KeyedObjectPool;->clear()V

    .line 2370
    return-void
.end method

.method public clear(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2376
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->clear(Ljava/lang/Object;)V

    .line 2377
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 2384
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/KeyedObjectPool;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2388
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected getErodingFactor(Ljava/lang/Object;)Lorg/apache/commons/pool/PoolUtils$ErodingFactor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lorg/apache/commons/pool/PoolUtils$ErodingFactor;"
        }
    .end annotation

    .prologue
    .line 2316
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->erodingFactor:Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    return-object v0
.end method

.method protected getKeyedPool()Lorg/apache/commons/pool/KeyedObjectPool;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2405
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    return-object v0
.end method

.method public getNumActive()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2355
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/KeyedObjectPool;->getNumActive()I

    move-result v0

    return v0
.end method

.method public getNumActive(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2362
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->getNumActive(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getNumIdle()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2341
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/KeyedObjectPool;->getNumIdle()I

    move-result v0

    return v0
.end method

.method public getNumIdle(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2348
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->getNumIdle(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public invalidateObject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 2324
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedObjectPool;->invalidateObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2328
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected numIdle(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    .line 2307
    invoke-virtual {p0}, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->getKeyedPool()Lorg/apache/commons/pool/KeyedObjectPool;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/commons/pool/KeyedObjectPool;->getNumIdle()I

    move-result v0

    return v0
.end method

.method public returnObject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 2274
    const/4 v0, 0x0

    .line 2275
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2276
    invoke-virtual {p0, p1}, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->getErodingFactor(Ljava/lang/Object;)Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    move-result-object v1

    .line 2277
    iget-object v4, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    monitor-enter v4

    .line 2278
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->getNextShrink()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-gez v5, :cond_1

    .line 2279
    invoke-virtual {p0, p1}, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->numIdle(Ljava/lang/Object;)I

    move-result v5

    .line 2280
    if-lez v5, :cond_0

    .line 2281
    const/4 v0, 0x1

    .line 2284
    :cond_0
    invoke-virtual {v1, v2, v3, v5}, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->update(JI)V

    .line 2286
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2288
    if-eqz v0, :cond_2

    .line 2289
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedObjectPool;->invalidateObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2296
    :goto_0
    return-void

    .line 2286
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 2291
    :cond_2
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedObjectPool;->returnObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 2296
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setFactory(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2396
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->setFactory(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V

    .line 2397
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2413
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ErodingKeyedObjectPool{erodingFactor="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->erodingFactor:Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", keyedPool="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
