.class final Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Latch"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "LK:Ljava/lang/Object;",
        "LV:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final _key:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "LK;"
        }
    .end annotation
.end field

.field private _mayCreate:Z

.field private _pair:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<T",
            "LV;",
            ">;"
        }
    .end annotation
.end field

.field private _pool:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool",
            "<TK;TV;>.ObjectQueue;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;


# direct methods
.method private constructor <init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "LK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2492
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->this$0:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2486
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->_mayCreate:Z

    .line 2493
    iput-object p2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->_key:Ljava/lang/Object;

    .line 2494
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Ljava/lang/Object;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$1;)V
    .locals 0

    .prologue
    .line 2474
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;-><init>(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$100(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    .locals 1

    .prologue
    .line 2474
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;Z)V
    .locals 0

    .prologue
    .line 2474
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->setMayCreate(Z)V

    return-void
.end method

.method static synthetic access$200(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Z
    .locals 1

    .prologue
    .line 2474
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->mayCreate()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    .locals 1

    .prologue
    .line 2474
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getPool()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2474
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->getkey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V
    .locals 0

    .prologue
    .line 2474
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->setPair(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V

    return-void
.end method

.method static synthetic access$600(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;)V
    .locals 0

    .prologue
    .line 2474
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->reset()V

    return-void
.end method

.method static synthetic access$800(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)V
    .locals 0

    .prologue
    .line 2474
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->setPool(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)V

    return-void
.end method

.method private declared-synchronized getPair()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<T",
            "LV;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2526
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->_pair:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getPool()Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool",
            "<TK;TV;>.ObjectQueue;"
        }
    .end annotation

    .prologue
    .line 2509
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->_pool:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getkey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()T",
            "LK;"
        }
    .end annotation

    .prologue
    .line 2501
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->_key:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized mayCreate()Z
    .locals 1

    .prologue
    .line 2542
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->_mayCreate:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized reset()V
    .locals 1

    .prologue
    .line 2559
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->_pair:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;

    .line 2560
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->_mayCreate:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2561
    monitor-exit p0

    return-void

    .line 2559
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setMayCreate(Z)V
    .locals 1

    .prologue
    .line 2551
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->_mayCreate:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2552
    monitor-exit p0

    return-void

    .line 2551
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setPair(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair",
            "<T",
            "LV;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2534
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->_pair:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectTimestampPair;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2535
    monitor-exit p0

    return-void

    .line 2534
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setPool(Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool",
            "<TK;TV;>.ObjectQueue;)V"
        }
    .end annotation

    .prologue
    .line 2517
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Latch;->_pool:Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$ObjectQueue;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2518
    monitor-exit p0

    return-void

    .line 2517
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
