.class Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/ObjectPool;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/PoolUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ErodingObjectPool"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/commons/pool/ObjectPool",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final factor:Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

.field private final pool:Lorg/apache/commons/pool/ObjectPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/commons/pool/ObjectPool;F)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;F)V"
        }
    .end annotation

    .prologue
    .line 2101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2102
    iput-object p1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    .line 2103
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    invoke-direct {v0, p2}, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;-><init>(F)V

    iput-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->factor:Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    .line 2104
    return-void
.end method


# virtual methods
.method public addObject()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2161
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/ObjectPool;->addObject()V

    .line 2162
    return-void
.end method

.method public borrowObject()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/util/NoSuchElementException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 2110
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/ObjectPool;->borrowObject()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2182
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/ObjectPool;->clear()V

    .line 2183
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 2190
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/ObjectPool;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2194
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getNumActive()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2175
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/ObjectPool;->getNumActive()I

    move-result v0

    return v0
.end method

.method public getNumIdle()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 2168
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/ObjectPool;->getNumIdle()I

    move-result v0

    return v0
.end method

.method public invalidateObject(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 2151
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/ObjectPool;->invalidateObject(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2155
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public returnObject(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 2123
    const/4 v0, 0x0

    .line 2124
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2125
    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    monitor-enter v1

    .line 2126
    :try_start_0
    iget-object v4, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->factor:Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    invoke-virtual {v4}, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->getNextShrink()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-gez v4, :cond_1

    .line 2127
    iget-object v4, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v4}, Lorg/apache/commons/pool/ObjectPool;->getNumIdle()I

    move-result v4

    .line 2128
    if-lez v4, :cond_0

    .line 2129
    const/4 v0, 0x1

    .line 2132
    :cond_0
    iget-object v5, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->factor:Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    invoke-virtual {v5, v2, v3, v4}, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;->update(JI)V

    .line 2134
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2136
    if-eqz v0, :cond_2

    .line 2137
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/ObjectPool;->invalidateObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2144
    :goto_0
    return-void

    .line 2134
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 2139
    :cond_2
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/ObjectPool;->returnObject(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 2144
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setFactory(Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2202
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/ObjectPool;->setFactory(Lorg/apache/commons/pool/PoolableObjectFactory;)V

    .line 2203
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2210
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ErodingObjectPool{factor="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->factor:Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pool="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;->pool:Lorg/apache/commons/pool/ObjectPool;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
