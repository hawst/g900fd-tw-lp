.class Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/PoolableObjectFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/PoolUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SynchronizedPoolableObjectFactory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/commons/pool/PoolableObjectFactory",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final factory:Lorg/apache/commons/pool/PoolableObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final lock:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1855
    if-nez p1, :cond_0

    .line 1856
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "factory must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1858
    :cond_0
    iput-object p1, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    .line 1859
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->lock:Ljava/lang/Object;

    .line 1860
    return-void
.end method


# virtual methods
.method public activateObject(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1893
    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 1894
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->activateObject(Ljava/lang/Object;)V

    .line 1895
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public destroyObject(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1875
    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 1876
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->destroyObject(Ljava/lang/Object;)V

    .line 1877
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public makeObject()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1866
    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 1867
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0}, Lorg/apache/commons/pool/PoolableObjectFactory;->makeObject()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1868
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public passivateObject(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1902
    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 1903
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->passivateObject(Ljava/lang/Object;)V

    .line 1904
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1912
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1913
    const-string v1, "SynchronizedPoolableObjectFactory"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1914
    const-string v1, "{factory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 1915
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1916
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public validateObject(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 1884
    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 1885
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;->factory:Lorg/apache/commons/pool/PoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/PoolableObjectFactory;->validateObject(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 1886
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
