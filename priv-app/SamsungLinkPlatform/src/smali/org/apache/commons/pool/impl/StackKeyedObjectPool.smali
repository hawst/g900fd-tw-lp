.class public Lorg/apache/commons/pool/impl/StackKeyedObjectPool;
.super Lorg/apache/commons/pool/BaseKeyedObjectPool;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/KeyedObjectPool;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/commons/pool/BaseKeyedObjectPool",
        "<TK;TV;>;",
        "Lorg/apache/commons/pool/KeyedObjectPool",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field protected static final DEFAULT_INIT_SLEEPING_CAPACITY:I = 0x4

.field protected static final DEFAULT_MAX_SLEEPING:I = 0x8


# instance fields
.field protected _activeCount:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<TK;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _initSleepingCapacity:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _maxSleeping:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _pools:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<TK;",
            "Ljava/util/Stack",
            "<TV;>;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _totActive:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _totIdle:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 64
    const/4 v0, 0x0

    const/16 v1, 0x8

    const/4 v2, 0x4

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;II)V

    .line 65
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 78
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;II)V

    .line 79
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;II)V

    .line 95
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 104
    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;I)V

    .line 105
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;I)V"
        }
    .end annotation

    .prologue
    .line 116
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;II)V

    .line 117
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;II)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;II)V"
        }
    .end annotation

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 131
    invoke-direct {p0}, Lorg/apache/commons/pool/BaseKeyedObjectPool;-><init>()V

    .line 609
    iput-object v2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    .line 616
    iput-object v2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    .line 623
    iput v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_maxSleeping:I

    .line 630
    iput v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_initSleepingCapacity:I

    .line 637
    iput v3, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totActive:I

    .line 644
    iput v3, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    .line 651
    iput-object v2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    .line 132
    iput-object p1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    .line 133
    if-gez p2, :cond_0

    move p2, v0

    :cond_0
    iput p2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_maxSleeping:I

    .line 134
    if-gtz p3, :cond_1

    move p3, v1

    :cond_1
    iput p3, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_initSleepingCapacity:I

    .line 135
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    .line 136
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    .line 137
    return-void
.end method

.method private decrementActiveCount(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    .line 536
    iget v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totActive:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totActive:I

    .line 537
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 538
    if-nez v0, :cond_0

    .line 545
    :goto_0
    return-void

    .line 540
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_1

    .line 541
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 543
    :cond_1
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private declared-synchronized destroyStack(Ljava/lang/Object;Ljava/util/Stack;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/Stack",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 413
    monitor-enter p0

    if-nez p2, :cond_0

    .line 430
    :goto_0
    monitor-exit p0

    return-void

    .line 416
    :cond_0
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    if-eqz v0, :cond_1

    .line 417
    invoke-virtual {p2}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 418
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 420
    :try_start_1
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 423
    :catch_0
    move-exception v1

    goto :goto_1

    .line 426
    :cond_1
    :try_start_2
    iget v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    invoke-virtual {p2}, Ljava/util/Stack;->size()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    .line 427
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    invoke-virtual {p2}, Ljava/util/Stack;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getActiveCount(Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 505
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 509
    :goto_0
    return v0

    .line 507
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 509
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method private incrementActiveCount(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    .line 520
    iget v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totActive:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totActive:I

    .line 521
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 522
    if-nez v0, :cond_0

    .line 523
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    :goto_0
    return-void

    .line 525
    :cond_0
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized addObject(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 283
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->assertOpen()V

    .line 284
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    if-nez v0, :cond_0

    .line 285
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add objects without a factory."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 287
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->makeObject(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 289
    :try_start_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, v2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->validateObject(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 330
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    .line 294
    :try_start_3
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, v2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 298
    :catch_1
    move-exception v0

    goto :goto_0

    .line 300
    :cond_2
    :try_start_4
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, v2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->passivateObject(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 302
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 303
    if-nez v0, :cond_3

    .line 304
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    .line 305
    iget v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_initSleepingCapacity:I

    iget v3, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_maxSleeping:I

    if-le v1, v3, :cond_4

    iget v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_maxSleeping:I

    :goto_1
    invoke-virtual {v0, v1}, Ljava/util/Stack;->ensureCapacity(I)V

    .line 306
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    :cond_3
    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v1

    .line 310
    iget v3, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_maxSleeping:I

    if-lt v1, v3, :cond_6

    .line 312
    if-lez v1, :cond_5

    .line 313
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .line 314
    iget v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v1, v0

    .line 319
    :goto_2
    :try_start_5
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, v1}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 320
    :catch_2
    move-exception v0

    .line 322
    if-ne v2, v1, :cond_1

    .line 323
    :try_start_6
    throw v0

    .line 305
    :cond_4
    iget v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_initSleepingCapacity:I

    goto :goto_1

    :cond_5
    move-object v1, v2

    .line 316
    goto :goto_2

    .line 327
    :cond_6
    invoke-virtual {v0, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    iget v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized borrowObject(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 148
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->assertOpen()V

    .line 149
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 150
    if-nez v0, :cond_0

    .line 151
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    .line 152
    iget v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_initSleepingCapacity:I

    iget v2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_maxSleeping:I

    if-le v1, v2, :cond_1

    iget v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_maxSleeping:I

    :goto_0
    invoke-virtual {v0, v1}, Ljava/util/Stack;->ensureCapacity(I)V

    .line 153
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    :cond_0
    const/4 v1, 0x0

    .line 158
    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 159
    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    .line 160
    iget v4, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    move v4, v1

    .line 169
    :goto_1
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_6

    if-eqz v2, :cond_6

    .line 171
    :try_start_1
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v1, p1, v2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->activateObject(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 172
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v1, p1, v2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->validateObject(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 173
    new-instance v1, Ljava/lang/Exception;

    const-string v5, "ValidateObject failed"

    invoke-direct {v1, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175
    :catch_0
    move-exception v1

    .line 176
    :try_start_2
    invoke-static {v1}, Lorg/apache/commons/pool/PoolUtils;->checkRethrow(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 178
    :try_start_3
    iget-object v5, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v5, p1, v2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 185
    :goto_2
    if-eqz v4, :cond_5

    .line 186
    :try_start_4
    new-instance v0, Ljava/util/NoSuchElementException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not create a validated object, cause: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 152
    :cond_1
    :try_start_5
    iget v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_initSleepingCapacity:I

    goto :goto_0

    .line 162
    :cond_2
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    if-nez v1, :cond_3

    .line 163
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "pools without a factory cannot create new objects as needed."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_3
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v1, p1}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->makeObject(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 166
    const/4 v1, 0x1

    move v4, v1

    goto :goto_1

    :cond_4
    move-object v1, v2

    .line 192
    :goto_3
    if-eqz v1, :cond_0

    .line 193
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->incrementActiveCount(Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 194
    monitor-exit p0

    return-object v1

    .line 179
    :catch_1
    move-exception v2

    :try_start_6
    invoke-static {v2}, Lorg/apache/commons/pool/PoolUtils;->checkRethrow(Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    .line 183
    :catchall_1
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_5
    move-object v1, v3

    goto :goto_3

    :cond_6
    move-object v1, v2

    goto :goto_3
.end method

.method public declared-synchronized clear()V
    .locals 3

    .prologue
    .line 384
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 385
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 387
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 388
    invoke-direct {p0, v2, v0}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->destroyStack(Ljava/lang/Object;Ljava/util/Stack;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 384
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 390
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    .line 391
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 392
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 393
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized clear(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    .line 402
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 403
    invoke-direct {p0, p1, v0}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->destroyStack(Ljava/lang/Object;Ljava/util/Stack;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    monitor-exit p0

    return-void

    .line 402
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 464
    invoke-super {p0}, Lorg/apache/commons/pool/BaseKeyedObjectPool;->close()V

    .line 465
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->clear()V

    .line 466
    return-void
.end method

.method public getActiveCount()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_activeCount:Ljava/util/HashMap;

    return-object v0
.end method

.method public declared-synchronized getFactory()Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 494
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getInitSleepingCapacity()I
    .locals 1

    .prologue
    .line 569
    iget v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_initSleepingCapacity:I

    return v0
.end method

.method public getMaxSleeping()I
    .locals 1

    .prologue
    .line 561
    iget v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_maxSleeping:I

    return v0
.end method

.method public declared-synchronized getNumActive()I
    .locals 1

    .prologue
    .line 349
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totActive:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumActive(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    .line 361
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->getActiveCount(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumIdle()I
    .locals 1

    .prologue
    .line 339
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumIdle(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    .line 373
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 375
    :goto_0
    monitor-exit p0

    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPools()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Stack",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 553
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    return-object v0
.end method

.method public getTotActive()I
    .locals 1

    .prologue
    .line 576
    iget v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totActive:I

    return v0
.end method

.method public getTotIdle()I
    .locals 1

    .prologue
    .line 583
    iget v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    return v0
.end method

.method public declared-synchronized invalidateObject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 265
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->decrementActiveCount(Ljava/lang/Object;)V

    .line 266
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 269
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    monitor-exit p0

    return-void

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized returnObject(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->decrementActiveCount(Ljava/lang/Object;)V

    .line 209
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->validateObject(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->passivateObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 222
    :cond_0
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_1

    .line 225
    :try_start_3
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 258
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 214
    :catch_0
    move-exception v0

    :try_start_4
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v0, p1, p2}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 233
    :cond_2
    :try_start_5
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 234
    if-nez v0, :cond_6

    .line 235
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    .line 236
    iget v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_initSleepingCapacity:I

    iget v2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_maxSleeping:I

    if-le v1, v2, :cond_4

    iget v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_maxSleeping:I

    :goto_1
    invoke-virtual {v0, v1}, Ljava/util/Stack;->ensureCapacity(I)V

    .line 237
    iget-object v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 239
    :goto_2
    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v0

    .line 240
    iget v2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_maxSleeping:I

    if-lt v0, v2, :cond_3

    .line 242
    if-lez v0, :cond_5

    .line 243
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .line 244
    iget v2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    .line 248
    :goto_3
    iget-object v2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v2, :cond_3

    .line 250
    :try_start_6
    iget-object v2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    invoke-interface {v2, p1, v0}, Lorg/apache/commons/pool/KeyedPoolableObjectFactory;->destroyObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 256
    :cond_3
    :goto_4
    :try_start_7
    invoke-virtual {v1, p2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    iget v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_totIdle:I

    goto :goto_0

    .line 236
    :cond_4
    iget v1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_initSleepingCapacity:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :cond_5
    move-object v0, p2

    .line 246
    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_4

    :catch_2
    move-exception v0

    goto :goto_0

    :cond_6
    move-object v1, v0

    goto :goto_2
.end method

.method public declared-synchronized setFactory(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 481
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->getNumActive()I

    move-result v0

    if-lez v0, :cond_0

    .line 482
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Objects are already active"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 484
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->clear()V

    .line 485
    iput-object p1, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 487
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 440
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 441
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 442
    const-string v0, " contains "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, " distinct pools: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 443
    iget-object v0, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 444
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 446
    const-string v3, " |"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "|="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 447
    iget-object v3, p0, Lorg/apache/commons/pool/impl/StackKeyedObjectPool;->_pools:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    .line 448
    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 440
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 450
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method
