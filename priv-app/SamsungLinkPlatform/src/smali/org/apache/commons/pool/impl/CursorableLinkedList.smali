.class Lorg/apache/commons/pool/impl/CursorableLinkedList;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/List;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;,
        Lorg/apache/commons/pool/impl/CursorableLinkedList$ListIter;,
        Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/util/List",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x7aa12cb4b4f67ac1L


# instance fields
.field protected transient _cursors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<TE;>.Cursor;>;>;"
        }
    .end annotation
.end field

.field protected transient _head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;"
        }
    .end annotation
.end field

.field protected transient _modCount:I

.field protected transient _size:I


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 945
    iput v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    .line 959
    new-instance v0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-direct {v0, v1, v1, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;-><init>(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 962
    iput v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_modCount:I

    .line 968
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_cursors:Ljava/util/List;

    .line 1122
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 931
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 932
    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    .line 933
    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_modCount:I

    .line 934
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_cursors:Ljava/util/List;

    .line 935
    new-instance v1, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-direct {v1, v2, v2, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;-><init>(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)V

    iput-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 936
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    .line 937
    :goto_0
    if-ge v0, v1, :cond_0

    .line 938
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->add(Ljava/lang/Object;)Z

    .line 937
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 940
    :cond_0
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 920
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 921
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 922
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    .line 923
    :goto_0
    if-eqz v0, :cond_0

    .line 924
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 925
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    goto :goto_0

    .line 927
    :cond_0
    return-void
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 98
    iget v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-ne p1, v1, :cond_0

    .line 99
    invoke-virtual {p0, p2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->add(Ljava/lang/Object;)Z

    .line 108
    :goto_0
    return-void

    .line 101
    :cond_0
    if-ltz p1, :cond_1

    iget v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-le p1, v1, :cond_2

    .line 102
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < 0 or "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_2
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v0

    .line 105
    :goto_1
    if-nez v1, :cond_4

    .line 106
    :goto_2
    invoke-virtual {p0, v0, v1, p2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->insertListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    goto :goto_0

    .line 104
    :cond_3
    invoke-virtual {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->getListableAt(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    goto :goto_1

    .line 105
    :cond_4
    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    goto :goto_2
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->insertListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 79
    const/4 v0, 0x1

    return v0
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 164
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    const/4 v0, 0x0

    .line 175
    :goto_0
    return v0

    .line 166
    :cond_0
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-eq v0, p1, :cond_1

    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-nez v0, :cond_2

    .line 167
    :cond_1
    invoke-virtual {p0, p2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->addAll(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0

    .line 169
    :cond_2
    invoke-virtual {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->getListableAt(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    .line 170
    if-nez v1, :cond_3

    const/4 v0, 0x0

    .line 171
    :goto_1
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 172
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 173
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->insertListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    goto :goto_2

    .line 170
    :cond_3
    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    goto :goto_1

    .line 175
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 128
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 135
    :goto_0
    return v0

    .line 131
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 132
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 133
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->insertListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    goto :goto_1

    .line 135
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public addFirst(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 187
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->insertListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 188
    const/4 v0, 0x1

    return v0
.end method

.method public addLast(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->insertListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 200
    const/4 v0, 0x1

    return v0
.end method

.method protected broadcastListableChanged(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 873
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_cursors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 874
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 875
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 876
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 877
    if-nez v0, :cond_0

    .line 878
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 880
    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->listableChanged(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    goto :goto_0

    .line 883
    :cond_1
    return-void
.end method

.method protected broadcastListableInserted(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 907
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_cursors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 908
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 909
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 910
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 911
    if-nez v0, :cond_0

    .line 912
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 914
    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->listableInserted(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    goto :goto_0

    .line 917
    :cond_1
    return-void
.end method

.method protected broadcastListableRemoved(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 890
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_cursors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 891
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 892
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 893
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 894
    if-nez v0, :cond_0

    .line 895
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 897
    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->listableRemoved(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    goto :goto_0

    .line 900
    :cond_1
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 217
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 218
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 220
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 222
    :cond_0
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 234
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_3

    iget-object v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v2

    if-eq v0, v2, :cond_3

    .line 235
    if-nez p1, :cond_0

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 237
    :cond_1
    const/4 v0, 0x1

    .line 240
    :goto_1
    return v0

    .line 234
    :cond_2
    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_0

    .line 240
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 252
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 253
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 254
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 255
    const/4 v0, 0x0

    .line 258
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cursor()Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<TE;>.Cursor;"
        }
    .end annotation

    .prologue
    .line 287
    new-instance v0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;-><init>(Lorg/apache/commons/pool/impl/CursorableLinkedList;I)V

    return-object v0
.end method

.method public cursor(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<TE;>.Cursor;"
        }
    .end annotation

    .prologue
    .line 307
    new-instance v0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;-><init>(Lorg/apache/commons/pool/impl/CursorableLinkedList;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 326
    if-ne p1, p0, :cond_1

    .line 337
    :cond_0
    :goto_0
    return v0

    .line 328
    :cond_1
    instance-of v2, p1, Ljava/util/List;

    if-nez v2, :cond_2

    move v0, v1

    .line 329
    goto :goto_0

    .line 331
    :cond_2
    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v4

    .line 332
    iget-object v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v3

    const/4 v2, 0x0

    :goto_1
    if-eqz v3, :cond_6

    iget-object v5, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v5}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v5

    if-eq v2, v5, :cond_6

    .line 333
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    :cond_3
    move v0, v1

    .line 334
    goto :goto_0

    .line 333
    :cond_4
    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 332
    :cond_5
    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v2

    move-object v6, v3

    move-object v3, v2

    move-object v2, v6

    goto :goto_1

    .line 337
    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 350
    invoke-virtual {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->getListableAt(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getFirst()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 358
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 360
    :catch_0
    move-exception v0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public getLast()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 369
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 371
    :catch_0
    move-exception v0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method protected getListableAt(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 792
    if-ltz p1, :cond_0

    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-lt p1, v0, :cond_1

    .line 793
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < 0 or "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 795
    :cond_1
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    div-int/lit8 v0, v0, 0x2

    if-gt p1, v0, :cond_2

    .line 796
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    .line 797
    const/4 v0, 0x0

    move v3, v0

    move-object v0, v1

    move v1, v3

    :goto_0
    if-ge v1, p1, :cond_3

    .line 798
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v2

    .line 797
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_0

    .line 802
    :cond_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    .line 803
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move-object v0, v1

    move v1, v3

    :goto_1
    if-le v1, p1, :cond_3

    .line 804
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v2

    .line 803
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    move-object v0, v2

    goto :goto_1

    .line 806
    :cond_3
    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 398
    const/4 v2, 0x1

    .line 399
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v3, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v3

    if-eq v0, v3, :cond_1

    .line 400
    mul-int/lit8 v2, v2, 0x1f

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v2, v0

    .line 399
    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    .line 400
    :cond_0
    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    .line 402
    :cond_1
    return v2
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 417
    const/4 v1, 0x0

    .line 421
    if-nez p1, :cond_2

    .line 422
    iget-object v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v2

    move-object v4, v0

    move v0, v1

    move-object v1, v4

    :goto_0
    if-eqz v2, :cond_3

    iget-object v3, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v3

    if-eq v1, v3, :cond_3

    .line 423
    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 437
    :cond_0
    :goto_1
    return v0

    .line 426
    :cond_1
    add-int/lit8 v1, v0, 0x1

    .line 422
    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    move-object v4, v2

    move-object v2, v0

    move v0, v1

    move-object v1, v4

    goto :goto_0

    .line 430
    :cond_2
    iget-object v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v2

    move-object v4, v0

    move v0, v1

    move-object v1, v4

    :goto_2
    if-eqz v2, :cond_3

    iget-object v3, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v3

    if-eq v1, v3, :cond_3

    .line 431
    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 434
    add-int/lit8 v1, v0, 0x1

    .line 430
    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    move-object v4, v2

    move-object v2, v0

    move v0, v1

    move-object v1, v4

    goto :goto_2

    .line 437
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected insertListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;TE;)",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 742
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_modCount:I

    .line 743
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    .line 744
    new-instance v0, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;-><init>(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)V

    .line 745
    if-eqz p1, :cond_0

    .line 746
    invoke-virtual {p1, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 751
    :goto_0
    if-eqz p2, :cond_1

    .line 752
    invoke-virtual {p2, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 756
    :goto_1
    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->broadcastListableInserted(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 757
    return-object v0

    .line 748
    :cond_0
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    goto :goto_0

    .line 754
    :cond_1
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    goto :goto_1
.end method

.method protected invalidateCursors()V
    .locals 3

    .prologue
    .line 854
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_cursors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 855
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 856
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 857
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 858
    if-eqz v1, :cond_0

    .line 860
    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;->invalidate()V

    .line 861
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 863
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 865
    :cond_1
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 445
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 453
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 468
    iget v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    add-int/lit8 v1, v1, -0x1

    .line 472
    if-nez p1, :cond_2

    .line 473
    iget-object v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v2

    move-object v4, v0

    move v0, v1

    move-object v1, v4

    :goto_0
    if-eqz v2, :cond_3

    iget-object v3, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v3

    if-eq v1, v3, :cond_3

    .line 474
    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 487
    :cond_0
    :goto_1
    return v0

    .line 477
    :cond_1
    add-int/lit8 v1, v0, -0x1

    .line 473
    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    move-object v4, v2

    move-object v2, v0

    move v0, v1

    move-object v1, v4

    goto :goto_0

    .line 480
    :cond_2
    iget-object v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v2

    move-object v4, v0

    move v0, v1

    move-object v1, v4

    :goto_2
    if-eqz v2, :cond_3

    iget-object v3, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v3

    if-eq v1, v3, :cond_3

    .line 481
    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 484
    add-int/lit8 v1, v0, -0x1

    .line 480
    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    move-object v4, v2

    move-object v2, v0

    move v0, v1

    move-object v1, v4

    goto :goto_2

    .line 487
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 495
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 503
    if-ltz p1, :cond_0

    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-le p1, v0, :cond_1

    .line 504
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < 0 or > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 506
    :cond_1
    new-instance v0, Lorg/apache/commons/pool/impl/CursorableLinkedList$ListIter;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$ListIter;-><init>(Lorg/apache/commons/pool/impl/CursorableLinkedList;I)V

    return-object v0
.end method

.method protected registerCursor(Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<TE;>.Cursor;)V"
        }
    .end annotation

    .prologue
    .line 817
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_cursors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 818
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 819
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 820
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 824
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_cursors:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 825
    return-void
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 545
    invoke-virtual {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->getListableAt(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    .line 546
    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v1

    .line 547
    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->removeListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 548
    return-object v1
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 520
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v2

    const/4 v1, 0x0

    :goto_0
    if-eqz v2, :cond_2

    iget-object v3, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v3

    if-eq v1, v3, :cond_2

    .line 521
    if-nez p1, :cond_0

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 522
    invoke-virtual {p0, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->removeListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 529
    :goto_1
    return v0

    .line 524
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 525
    invoke-virtual {p0, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->removeListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    goto :goto_1

    .line 520
    :cond_1
    invoke-virtual {v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    move-object v4, v2

    move-object v2, v1

    move-object v1, v4

    goto :goto_0

    .line 529
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 560
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-nez v1, :cond_1

    .line 571
    :cond_0
    return v0

    .line 564
    :cond_1
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 565
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 566
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 567
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 568
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeFirst()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 579
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v0

    .line 581
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->removeListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 582
    return-object v0

    .line 584
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public removeLast()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 592
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v0

    .line 594
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->removeListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 595
    return-object v0

    .line 597
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method protected removeListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 766
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_modCount:I

    .line 767
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    .line 768
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 769
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 771
    :cond_0
    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 772
    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 774
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-ne v0, p1, :cond_2

    .line 775
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 777
    :cond_2
    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 778
    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 780
    :cond_3
    invoke-virtual {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->broadcastListableRemoved(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 781
    return-void
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 612
    const/4 v0, 0x0

    .line 613
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 614
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 615
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 616
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 617
    const/4 v0, 0x1

    goto :goto_0

    .line 620
    :cond_1
    return v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .prologue
    .line 639
    invoke-virtual {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->getListableAt(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    .line 640
    invoke-virtual {v0, p2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 641
    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->broadcastListableChanged(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 642
    return-object v1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 650
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    return v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 722
    if-ltz p1, :cond_0

    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-gt p2, v0, :cond_0

    if-le p1, p2, :cond_1

    .line 723
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 724
    :cond_1
    if-nez p1, :cond_2

    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-ne p2, v0, :cond_2

    .line 727
    :goto_0
    return-object p0

    :cond_2
    new-instance v0, Lorg/apache/commons/pool/impl/CursorableSubList;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/commons/pool/impl/CursorableSubList;-><init>(Lorg/apache/commons/pool/impl/CursorableLinkedList;II)V

    move-object p0, v0

    goto :goto_0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 6

    .prologue
    .line 661
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    new-array v4, v0, [Ljava/lang/Object;

    .line 662
    const/4 v2, 0x0

    .line 663
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_0

    iget-object v3, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v3

    if-eq v0, v3, :cond_0

    .line 664
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v4, v2

    .line 663
    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    move v2, v3

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_0

    .line 666
    :cond_0
    return-object v4
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 686
    array-length v0, p1

    iget v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-ge v0, v1, :cond_2

    .line 687
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    iget v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 689
    :goto_0
    const/4 v1, 0x0

    .line 690
    iget-object v3, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v3

    move v4, v1

    move-object v1, v2

    :goto_1
    if-eqz v3, :cond_0

    iget-object v5, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v5}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v5

    if-eq v1, v5, :cond_0

    .line 691
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v0, v4

    .line 690
    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    move v4, v5

    move-object v6, v1

    move-object v1, v3

    move-object v3, v6

    goto :goto_1

    .line 693
    :cond_0
    array-length v1, v0

    iget v3, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    if-le v1, v3, :cond_1

    .line 694
    iget v1, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_size:I

    aput-object v2, v0, v1

    .line 696
    :cond_1
    return-object v0

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 705
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 706
    const-string v0, "["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 707
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v3, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v3}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v3

    if-eq v0, v3, :cond_1

    .line 708
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-eq v0, v1, :cond_0

    .line 709
    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 711
    :cond_0
    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->value()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 707
    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    .line 713
    :cond_1
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 714
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected unregisterCursor(Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<TE;>.Cursor;)V"
        }
    .end annotation

    .prologue
    .line 832
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_cursors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 833
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 834
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/commons/pool/impl/CursorableLinkedList$Cursor;

    .line 835
    if-nez v1, :cond_1

    .line 839
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 841
    :cond_1
    if-ne v1, p1, :cond_0

    .line 842
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 843
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 847
    :cond_2
    return-void
.end method
