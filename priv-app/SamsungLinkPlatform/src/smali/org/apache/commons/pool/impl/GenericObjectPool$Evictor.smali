.class Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;
.super Ljava/util/TimerTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/impl/GenericObjectPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Evictor"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/commons/pool/impl/GenericObjectPool;


# direct methods
.method private constructor <init>(Lorg/apache/commons/pool/impl/GenericObjectPool;)V
    .locals 0

    .prologue
    .line 1767
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;->this$0:Lorg/apache/commons/pool/impl/GenericObjectPool;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/commons/pool/impl/GenericObjectPool;Lorg/apache/commons/pool/impl/GenericObjectPool$1;)V
    .locals 0

    .prologue
    .line 1767
    invoke-direct {p0, p1}, Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;-><init>(Lorg/apache/commons/pool/impl/GenericObjectPool;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1775
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;->this$0:Lorg/apache/commons/pool/impl/GenericObjectPool;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->evict()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1784
    :goto_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericObjectPool$Evictor;->this$0:Lorg/apache/commons/pool/impl/GenericObjectPool;

    # invokes: Lorg/apache/commons/pool/impl/GenericObjectPool;->ensureMinIdle()V
    invoke-static {v0}, Lorg/apache/commons/pool/impl/GenericObjectPool;->access$700(Lorg/apache/commons/pool/impl/GenericObjectPool;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1788
    :goto_1
    return-void

    .line 1778
    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v0, v1}, Ljava/lang/OutOfMemoryError;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_0

    .line 1788
    :catch_1
    move-exception v0

    goto :goto_1

    .line 1782
    :catch_2
    move-exception v0

    goto :goto_0
.end method
