.class public final Lorg/apache/commons/pool/PoolUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;,
        Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;,
        Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;,
        Lorg/apache/commons/pool/PoolUtils$ErodingFactor;,
        Lorg/apache/commons/pool/PoolUtils$SynchronizedKeyedPoolableObjectFactory;,
        Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;,
        Lorg/apache/commons/pool/PoolUtils$SynchronizedKeyedObjectPool;,
        Lorg/apache/commons/pool/PoolUtils$SynchronizedObjectPool;,
        Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;,
        Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;,
        Lorg/apache/commons/pool/PoolUtils$CheckedKeyedObjectPool;,
        Lorg/apache/commons/pool/PoolUtils$CheckedObjectPool;,
        Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolAdaptor;,
        Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;,
        Lorg/apache/commons/pool/PoolUtils$KeyedPoolableObjectFactoryAdaptor;,
        Lorg/apache/commons/pool/PoolUtils$PoolableObjectFactoryAdaptor;
    }
.end annotation


# static fields
.field private static MIN_IDLE_TIMER:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method public static adapt(Lorg/apache/commons/pool/ObjectPool;)Lorg/apache/commons/pool/KeyedObjectPool;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TV;>;)",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 164
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolAdaptor;

    invoke-direct {v0, p0}, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolAdaptor;-><init>(Lorg/apache/commons/pool/ObjectPool;)V

    return-object v0
.end method

.method public static adapt(Lorg/apache/commons/pool/PoolableObjectFactory;)Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TV;>;)",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 118
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$KeyedPoolableObjectFactoryAdaptor;

    invoke-direct {v0, p0}, Lorg/apache/commons/pool/PoolUtils$KeyedPoolableObjectFactoryAdaptor;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;)V

    return-object v0
.end method

.method public static adapt(Lorg/apache/commons/pool/KeyedObjectPool;)Lorg/apache/commons/pool/ObjectPool;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<",
            "Ljava/lang/Object;",
            "TV;>;)",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 133
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    invoke-static {p0, v0}, Lorg/apache/commons/pool/PoolUtils;->adapt(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Object;)Lorg/apache/commons/pool/ObjectPool;

    move-result-object v0

    return-object v0
.end method

.method public static adapt(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Object;)Lorg/apache/commons/pool/ObjectPool;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<",
            "Ljava/lang/Object;",
            "TV;>;",
            "Ljava/lang/Object;",
            ")",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 149
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;-><init>(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static adapt(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)Lorg/apache/commons/pool/PoolableObjectFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<",
            "Ljava/lang/Object;",
            "TV;>;)",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    invoke-static {p0, v0}, Lorg/apache/commons/pool/PoolUtils;->adapt(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;Ljava/lang/Object;)Lorg/apache/commons/pool/PoolableObjectFactory;

    move-result-object v0

    return-object v0
.end method

.method public static adapt(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;Ljava/lang/Object;)Lorg/apache/commons/pool/PoolableObjectFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;TK;)",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 103
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$PoolableObjectFactoryAdaptor;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/pool/PoolUtils$PoolableObjectFactoryAdaptor;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static checkMinIdle(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/util/Collection;IJ)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;",
            "Ljava/util/Collection",
            "<+TK;>;IJ)",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/TimerTask;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 287
    if-nez p1, :cond_0

    .line 288
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keys must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 291
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 292
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 293
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 294
    invoke-static {p0, v2, p2, p3, p4}, Lorg/apache/commons/pool/PoolUtils;->checkMinIdle(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Object;IJ)Ljava/util/TimerTask;

    move-result-object v3

    .line 295
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 297
    :cond_1
    return-object v0
.end method

.method public static checkMinIdle(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Object;IJ)Ljava/util/TimerTask;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;TK;IJ)",
            "Ljava/util/TimerTask;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 254
    if-nez p0, :cond_0

    .line 255
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyedPool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_0
    if-nez p1, :cond_1

    .line 258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 260
    :cond_1
    if-gez p2, :cond_2

    .line 261
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minIdle must be non-negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_2
    new-instance v1, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;

    invoke-direct {v1, p0, p1, p2}, Lorg/apache/commons/pool/PoolUtils$KeyedObjectPoolMinIdleTimerTask;-><init>(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Object;I)V

    .line 264
    invoke-static {}, Lorg/apache/commons/pool/PoolUtils;->getMinIdleTimer()Ljava/util/Timer;

    move-result-object v0

    const-wide/16 v2, 0x0

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 265
    return-object v1
.end method

.method public static checkMinIdle(Lorg/apache/commons/pool/ObjectPool;IJ)Ljava/util/TimerTask;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;IJ)",
            "Ljava/util/TimerTask;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 224
    if-nez p0, :cond_0

    .line 225
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyedPool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_0
    if-gez p1, :cond_1

    .line 228
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minIdle must be non-negative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 230
    :cond_1
    new-instance v1, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;

    invoke-direct {v1, p0, p1}, Lorg/apache/commons/pool/PoolUtils$ObjectPoolMinIdleTimerTask;-><init>(Lorg/apache/commons/pool/ObjectPool;I)V

    .line 231
    invoke-static {}, Lorg/apache/commons/pool/PoolUtils;->getMinIdleTimer()Ljava/util/Timer;

    move-result-object v0

    const-wide/16 v2, 0x0

    move-wide v4, p2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 232
    return-object v1
.end method

.method public static checkRethrow(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 64
    instance-of v0, p0, Ljava/lang/ThreadDeath;

    if-eqz v0, :cond_0

    .line 65
    check-cast p0, Ljava/lang/ThreadDeath;

    throw p0

    .line 67
    :cond_0
    instance-of v0, p0, Ljava/lang/VirtualMachineError;

    if-eqz v0, :cond_1

    .line 68
    check-cast p0, Ljava/lang/VirtualMachineError;

    throw p0

    .line 71
    :cond_1
    return-void
.end method

.method public static checkedPool(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Class;)Lorg/apache/commons/pool/KeyedObjectPool;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;",
            "Ljava/lang/Class",
            "<TV;>;)",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 199
    if-nez p0, :cond_0

    .line 200
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyedPool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_0
    if-nez p1, :cond_1

    .line 203
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "type must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_1
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$CheckedKeyedObjectPool;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/pool/PoolUtils$CheckedKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static checkedPool(Lorg/apache/commons/pool/ObjectPool;Ljava/lang/Class;)Lorg/apache/commons/pool/ObjectPool;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 178
    if-nez p0, :cond_0

    .line 179
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_0
    if-nez p1, :cond_1

    .line 182
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "type must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_1
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$CheckedObjectPool;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/pool/PoolUtils$CheckedObjectPool;-><init>(Lorg/apache/commons/pool/ObjectPool;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static erodingPool(Lorg/apache/commons/pool/KeyedObjectPool;)Lorg/apache/commons/pool/KeyedObjectPool;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;)",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 521
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, v0}, Lorg/apache/commons/pool/PoolUtils;->erodingPool(Lorg/apache/commons/pool/KeyedObjectPool;F)Lorg/apache/commons/pool/KeyedObjectPool;

    move-result-object v0

    return-object v0
.end method

.method public static erodingPool(Lorg/apache/commons/pool/KeyedObjectPool;F)Lorg/apache/commons/pool/KeyedObjectPool;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;F)",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 548
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/commons/pool/PoolUtils;->erodingPool(Lorg/apache/commons/pool/KeyedObjectPool;FZ)Lorg/apache/commons/pool/KeyedObjectPool;

    move-result-object v0

    return-object v0
.end method

.method public static erodingPool(Lorg/apache/commons/pool/KeyedObjectPool;FZ)Lorg/apache/commons/pool/KeyedObjectPool;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;FZ)",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 583
    if-nez p0, :cond_0

    .line 584
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyedPool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 586
    :cond_0
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    .line 587
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "factor must be positive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 589
    :cond_1
    if-eqz p2, :cond_2

    .line 590
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedObjectPool;F)V

    .line 592
    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedObjectPool;F)V

    goto :goto_0
.end method

.method public static erodingPool(Lorg/apache/commons/pool/ObjectPool;)Lorg/apache/commons/pool/ObjectPool;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;)",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 471
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, v0}, Lorg/apache/commons/pool/PoolUtils;->erodingPool(Lorg/apache/commons/pool/ObjectPool;F)Lorg/apache/commons/pool/ObjectPool;

    move-result-object v0

    return-object v0
.end method

.method public static erodingPool(Lorg/apache/commons/pool/ObjectPool;F)Lorg/apache/commons/pool/ObjectPool;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;F)",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 496
    if-nez p0, :cond_0

    .line 497
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 499
    :cond_0
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    .line 500
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "factor must be positive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502
    :cond_1
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;

    invoke-direct {v0, p0, p1}, Lorg/apache/commons/pool/PoolUtils$ErodingObjectPool;-><init>(Lorg/apache/commons/pool/ObjectPool;F)V

    return-object v0
.end method

.method private static declared-synchronized getMinIdleTimer()Ljava/util/Timer;
    .locals 3

    .prologue
    .line 603
    const-class v1, Lorg/apache/commons/pool/PoolUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/apache/commons/pool/PoolUtils;->MIN_IDLE_TIMER:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 604
    new-instance v0, Ljava/util/Timer;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/Timer;-><init>(Z)V

    sput-object v0, Lorg/apache/commons/pool/PoolUtils;->MIN_IDLE_TIMER:Ljava/util/Timer;

    .line 606
    :cond_0
    sget-object v0, Lorg/apache/commons/pool/PoolUtils;->MIN_IDLE_TIMER:Ljava/util/Timer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 603
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static prefill(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Object;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;TK;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 333
    if-nez p0, :cond_0

    .line 334
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyedPool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 336
    :cond_0
    if-nez p1, :cond_1

    .line 337
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 339
    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_2

    .line 340
    invoke-interface {p0, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->addObject(Ljava/lang/Object;)V

    .line 339
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 342
    :cond_2
    return-void
.end method

.method public static prefill(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/util/Collection;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;",
            "Ljava/util/Collection",
            "<+TK;>;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 361
    if-nez p1, :cond_0

    .line 362
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keys must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 365
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 366
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {p0, v1, p2}, Lorg/apache/commons/pool/PoolUtils;->prefill(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Object;I)V

    goto :goto_0

    .line 368
    :cond_1
    return-void
.end method

.method public static prefill(Lorg/apache/commons/pool/ObjectPool;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 311
    if-nez p0, :cond_0

    .line 312
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_1

    .line 315
    invoke-interface {p0}, Lorg/apache/commons/pool/ObjectPool;->addObject()V

    .line 314
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 317
    :cond_1
    return-void
.end method

.method public static synchronizedPool(Lorg/apache/commons/pool/KeyedObjectPool;)Lorg/apache/commons/pool/KeyedObjectPool;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;)",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 419
    if-nez p0, :cond_0

    .line 420
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyedPool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 430
    :cond_0
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$SynchronizedKeyedObjectPool;

    invoke-direct {v0, p0}, Lorg/apache/commons/pool/PoolUtils$SynchronizedKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedObjectPool;)V

    return-object v0
.end method

.method public static synchronizedPool(Lorg/apache/commons/pool/ObjectPool;)Lorg/apache/commons/pool/ObjectPool;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;)",
            "Lorg/apache/commons/pool/ObjectPool",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 386
    if-nez p0, :cond_0

    .line 387
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 399
    :cond_0
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$SynchronizedObjectPool;

    invoke-direct {v0, p0}, Lorg/apache/commons/pool/PoolUtils$SynchronizedObjectPool;-><init>(Lorg/apache/commons/pool/ObjectPool;)V

    return-object v0
.end method

.method public static synchronizedPoolableFactory(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;)",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 455
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$SynchronizedKeyedPoolableObjectFactory;

    invoke-direct {v0, p0}, Lorg/apache/commons/pool/PoolUtils$SynchronizedKeyedPoolableObjectFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V

    return-object v0
.end method

.method public static synchronizedPoolableFactory(Lorg/apache/commons/pool/PoolableObjectFactory;)Lorg/apache/commons/pool/PoolableObjectFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;)",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 442
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;

    invoke-direct {v0, p0}, Lorg/apache/commons/pool/PoolUtils$SynchronizedPoolableObjectFactory;-><init>(Lorg/apache/commons/pool/PoolableObjectFactory;)V

    return-object v0
.end method
