.class public Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/KeyedObjectPoolFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/commons/pool/KeyedObjectPoolFactory",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field protected _factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _lifo:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _maxActive:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _maxIdle:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _maxTotal:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _maxWait:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _minEvictableIdleTimeMillis:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _minIdle:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _numTestsPerEvictionRun:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _testOnBorrow:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _testOnReturn:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _testWhileIdle:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _timeBetweenEvictionRunsMillis:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected _whenExhaustedAction:B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    const/16 v2, 0x8

    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    const/16 v6, 0x8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, -0x1

    const/4 v11, 0x3

    const-wide/32 v12, 0x1b7740

    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    invoke-direct/range {v0 .. v14}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZJIJZ)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;I)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;I)V"
        }
    .end annotation

    .prologue
    .line 69
    const/4 v3, 0x1

    const-wide/16 v4, -0x1

    const/16 v6, 0x8

    const/4 v7, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIZZJIJZ)V

    .line 70
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJ)V"
        }
    .end annotation

    .prologue
    .line 82
    const/16 v6, 0x8

    const/4 v7, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIZZJIJZ)V

    .line 83
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJI)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJI)V"
        }
    .end annotation

    .prologue
    .line 111
    const/4 v7, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIZZJIJZ)V

    .line 112
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJII)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJII)V"
        }
    .end annotation

    .prologue
    .line 125
    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIZZJIJZ)V

    .line 126
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIIZZJIJZ)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJIIIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 205
    const/16 v17, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move-wide/from16 v11, p11

    move/from16 v13, p13

    move-wide/from16 v14, p14

    move/from16 v16, p16

    invoke-direct/range {v0 .. v17}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIIZZJIJZZ)V

    .line 206
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIIZZJIJZZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJIIIZZJIJZZ)V"
        }
    .end annotation

    .prologue
    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376
    const/16 v2, 0x8

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxIdle:I

    .line 383
    const/16 v2, 0x8

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxActive:I

    .line 390
    const/4 v2, -0x1

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxTotal:I

    .line 397
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_minIdle:I

    .line 404
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxWait:J

    .line 411
    const/4 v2, 0x1

    iput-byte v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_whenExhaustedAction:B

    .line 418
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testOnBorrow:Z

    .line 425
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testOnReturn:Z

    .line 432
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testWhileIdle:Z

    .line 440
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_timeBetweenEvictionRunsMillis:J

    .line 448
    const/4 v2, 0x3

    iput v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_numTestsPerEvictionRun:I

    .line 456
    const-wide/32 v2, 0x1b7740

    iput-wide v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_minEvictableIdleTimeMillis:J

    .line 463
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    .line 470
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_lifo:Z

    .line 229
    iput p6, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxIdle:I

    .line 230
    iput p2, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxActive:I

    .line 231
    iput p7, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxTotal:I

    .line 232
    iput p8, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_minIdle:I

    .line 233
    iput-wide p4, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxWait:J

    .line 234
    iput-byte p3, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_whenExhaustedAction:B

    .line 235
    iput-boolean p9, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testOnBorrow:Z

    .line 236
    iput-boolean p10, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testOnReturn:Z

    .line 237
    move/from16 v0, p16

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testWhileIdle:Z

    .line 238
    iput-wide p11, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_timeBetweenEvictionRunsMillis:J

    .line 239
    move/from16 v0, p13

    iput v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_numTestsPerEvictionRun:I

    .line 240
    move-wide/from16 v0, p14

    iput-wide v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_minEvictableIdleTimeMillis:J

    .line 241
    iput-object p1, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    .line 242
    move/from16 v0, p17

    iput-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_lifo:Z

    .line 243
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIZZJIJZ)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJIIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 182
    const/4 v8, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-wide/from16 v11, p10

    move/from16 v13, p12

    move-wide/from16 v14, p13

    move/from16 v16, p15

    invoke-direct/range {v0 .. v16}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIIZZJIJZ)V

    .line 183
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJIZZ)V"
        }
    .end annotation

    .prologue
    .line 141
    const/4 v7, -0x1

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIZZJIJZ)V

    .line 142
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIZZJIJZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJIZZJIJZ)V"
        }
    .end annotation

    .prologue
    .line 161
    const/4 v7, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-wide/from16 v10, p9

    move/from16 v12, p11

    move-wide/from16 v13, p12

    move/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIZZJIJZ)V

    .line 162
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJZZ)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;IBJZZ)V"
        }
    .end annotation

    .prologue
    .line 97
    const/16 v6, 0x8

    const/4 v7, -0x1

    const-wide/16 v10, -0x1

    const/4 v12, 0x3

    const-wide/32 v13, 0x1b7740

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v15}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIZZJIJZ)V

    .line 98
    return-void
.end method

.method public constructor <init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;",
            "Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 58
    move-object/from16 v0, p2

    iget v4, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxActive:I

    move-object/from16 v0, p2

    iget-byte v5, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->whenExhaustedAction:B

    move-object/from16 v0, p2

    iget-wide v6, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxWait:J

    move-object/from16 v0, p2

    iget v8, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxIdle:I

    move-object/from16 v0, p2

    iget v9, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->maxTotal:I

    move-object/from16 v0, p2

    iget v10, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->minIdle:I

    move-object/from16 v0, p2

    iget-boolean v11, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->testOnBorrow:Z

    move-object/from16 v0, p2

    iget-boolean v12, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->testOnReturn:Z

    move-object/from16 v0, p2

    iget-wide v13, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->timeBetweenEvictionRunsMillis:J

    move-object/from16 v0, p2

    iget v15, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->numTestsPerEvictionRun:I

    move-object/from16 v0, p2

    iget-wide v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->minEvictableIdleTimeMillis:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->testWhileIdle:Z

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool$Config;->lifo:Z

    move/from16 v19, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v19}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIIZZJIJZZ)V

    .line 59
    return-void
.end method


# virtual methods
.method public createPool()Lorg/apache/commons/pool/KeyedObjectPool;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 252
    new-instance v2, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxActive:I

    move-object/from16 v0, p0

    iget-byte v5, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_whenExhaustedAction:B

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxWait:J

    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxIdle:I

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxTotal:I

    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_minIdle:I

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testOnBorrow:Z

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testOnReturn:Z

    move-object/from16 v0, p0

    iget-wide v13, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_timeBetweenEvictionRunsMillis:J

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_numTestsPerEvictionRun:I

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_minEvictableIdleTimeMillis:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testWhileIdle:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_lifo:Z

    move/from16 v19, v0

    invoke-direct/range {v2 .. v19}, Lorg/apache/commons/pool/impl/GenericKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;IBJIIIZZJIJZZ)V

    return-object v2
.end method

.method public getFactory()Lorg/apache/commons/pool/KeyedPoolableObjectFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/commons/pool/KeyedPoolableObjectFactory",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 359
    iget-object v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_factory:Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    return-object v0
.end method

.method public getLifo()Z
    .locals 1

    .prologue
    .line 367
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_lifo:Z

    return v0
.end method

.method public getMaxActive()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxActive:I

    return v0
.end method

.method public getMaxIdle()I
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxIdle:I

    return v0
.end method

.method public getMaxTotal()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxTotal:I

    return v0
.end method

.method public getMaxWait()J
    .locals 2

    .prologue
    .line 292
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_maxWait:J

    return-wide v0
.end method

.method public getMinEvictableIdleTimeMillis()J
    .locals 2

    .prologue
    .line 351
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_minEvictableIdleTimeMillis:J

    return-wide v0
.end method

.method public getMinIdle()I
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_minIdle:I

    return v0
.end method

.method public getNumTestsPerEvictionRun()I
    .locals 1

    .prologue
    .line 342
    iget v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_numTestsPerEvictionRun:I

    return v0
.end method

.method public getTestOnBorrow()Z
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testOnBorrow:Z

    return v0
.end method

.method public getTestOnReturn()Z
    .locals 1

    .prologue
    .line 316
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testOnReturn:Z

    return v0
.end method

.method public getTestWhileIdle()Z
    .locals 1

    .prologue
    .line 324
    iget-boolean v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_testWhileIdle:Z

    return v0
.end method

.method public getTimeBetweenEvictionRunsMillis()J
    .locals 2

    .prologue
    .line 333
    iget-wide v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_timeBetweenEvictionRunsMillis:J

    return-wide v0
.end method

.method public getWhenExhaustedAction()B
    .locals 1

    .prologue
    .line 300
    iget-byte v0, p0, Lorg/apache/commons/pool/impl/GenericKeyedObjectPoolFactory;->_whenExhaustedAction:B

    return v0
.end method
