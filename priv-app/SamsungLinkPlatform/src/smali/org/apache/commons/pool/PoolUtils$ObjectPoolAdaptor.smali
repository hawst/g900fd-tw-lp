.class Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/pool/ObjectPool;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/PoolUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ObjectPoolAdaptor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/commons/pool/ObjectPool",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final key:Ljava/lang/Object;

.field private final keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<",
            "Ljava/lang/Object;",
            "TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/apache/commons/pool/KeyedObjectPool;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<",
            "Ljava/lang/Object;",
            "TV;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807
    if-nez p1, :cond_0

    .line 808
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyedPool must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 810
    :cond_0
    if-nez p2, :cond_1

    .line 811
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 813
    :cond_1
    iput-object p1, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    .line 814
    iput-object p2, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->key:Ljava/lang/Object;

    .line 815
    return-void
.end method


# virtual methods
.method public addObject()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 850
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->key:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/KeyedObjectPool;->addObject(Ljava/lang/Object;)V

    .line 851
    return-void
.end method

.method public borrowObject()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/util/NoSuchElementException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 821
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->key:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/KeyedObjectPool;->borrowObject(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 871
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/KeyedObjectPool;->clear()V

    .line 872
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 879
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-interface {v0}, Lorg/apache/commons/pool/KeyedObjectPool;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 883
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getNumActive()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 864
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->key:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/KeyedObjectPool;->getNumActive(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getNumIdle()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 857
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->key:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/KeyedObjectPool;->getNumIdle(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public invalidateObject(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 840
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->key:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->invalidateObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 844
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public returnObject(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 829
    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->key:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->returnObject(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 833
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setFactory(Lorg/apache/commons/pool/PoolableObjectFactory;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/PoolableObjectFactory",
            "<TV;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 893
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-static {p1}, Lorg/apache/commons/pool/PoolUtils;->adapt(Lorg/apache/commons/pool/PoolableObjectFactory;)Lorg/apache/commons/pool/KeyedPoolableObjectFactory;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/apache/commons/pool/KeyedObjectPool;->setFactory(Lorg/apache/commons/pool/KeyedPoolableObjectFactory;)V

    .line 894
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 901
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 902
    const-string v1, "ObjectPoolAdaptor"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 903
    const-string v1, "{key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->key:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 904
    const-string v1, ", keyedPool="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/pool/PoolUtils$ObjectPoolAdaptor;->keyedPool:Lorg/apache/commons/pool/KeyedObjectPool;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 905
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 906
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
