.class Lorg/apache/commons/pool/impl/CursorableSubList;
.super Lorg/apache/commons/pool/impl/CursorableLinkedList;
.source "SourceFile"

# interfaces
.implements Ljava/util/List;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/commons/pool/impl/CursorableLinkedList",
        "<TE;>;",
        "Ljava/util/List",
        "<TE;>;"
    }
.end annotation


# instance fields
.field protected _list:Lorg/apache/commons/pool/impl/CursorableLinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<TE;>;"
        }
    .end annotation
.end field

.field protected _post:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;"
        }
    .end annotation
.end field

.field protected _pre:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/apache/commons/pool/impl/CursorableLinkedList;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList",
            "<TE;>;II)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1221
    invoke-direct {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;-><init>()V

    .line 1508
    iput-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_list:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    .line 1511
    iput-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_pre:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 1514
    iput-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_post:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 1222
    if-ltz p2, :cond_0

    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v0

    if-ge v0, p3, :cond_1

    .line 1223
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1224
    :cond_1
    if-le p2, p3, :cond_2

    .line 1225
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1227
    :cond_2
    iput-object p1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_list:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    .line 1228
    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v0

    if-ge p2, v0, :cond_4

    .line 1229
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    iget-object v2, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_list:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v2, p2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->getListableAt(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1230
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_pre:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 1234
    :goto_1
    if-ne p2, p3, :cond_6

    .line 1235
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1236
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1237
    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v0

    if-ge p3, v0, :cond_5

    .line 1238
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_list:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0, p3}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->getListableAt(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_post:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    .line 1246
    :goto_2
    sub-int v0, p3, p2

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_size:I

    .line 1247
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_list:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    iget v0, v0, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_modCount:I

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_modCount:I

    .line 1248
    return-void

    .line 1230
    :cond_3
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    goto :goto_0

    .line 1232
    :cond_4
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_list:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    add-int/lit8 v2, p2, -0x1

    invoke-virtual {v0, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->getListableAt(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_pre:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    goto :goto_1

    .line 1240
    :cond_5
    iput-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_post:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    goto :goto_2

    .line 1243
    :cond_6
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_list:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    add-int/lit8 v2, p3, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->getListableAt(I)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1244
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_post:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    goto :goto_2
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    .line 1402
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1403
    invoke-super {p0, p1, p2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->add(ILjava/lang/Object;)V

    .line 1404
    return-void
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 1324
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1325
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 1354
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1355
    invoke-super {p0, p1, p2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->addAll(ILjava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 1318
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1319
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public addFirst(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 1330
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1331
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->addFirst(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addLast(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 1336
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1337
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->addLast(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected checkForComod()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/ConcurrentModificationException;
        }
    .end annotation

    .prologue
    .line 1500
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_modCount:I

    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_list:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    iget v1, v1, Lorg/apache/commons/pool/impl/CursorableLinkedList;->_modCount:I

    if-eq v0, v1, :cond_0

    .line 1501
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 1503
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 1254
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1255
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1256
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1257
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 1258
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1260
    :cond_0
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1294
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1295
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 1348
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1349
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1378
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1379
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 1384
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1385
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getFirst()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1390
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1391
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getLast()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1396
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1397
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1360
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1361
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->hashCode()I

    move-result v0

    return v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1420
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1421
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected insertListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;TE;)",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1453
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_modCount:I

    .line 1454
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_size:I

    .line 1455
    iget-object v2, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_list:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    if-nez p1, :cond_3

    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_pre:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-object v1, v0

    :goto_0
    if-nez p2, :cond_4

    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_post:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    :goto_1
    invoke-virtual {v2, v1, v0, p3}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->insertListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;Ljava/lang/Object;)Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    .line 1456
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1457
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1458
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1460
    :cond_0
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    if-ne p1, v1, :cond_1

    .line 1461
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1463
    :cond_1
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    if-ne p2, v1, :cond_2

    .line 1464
    iget-object v1, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v1, v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1466
    :cond_2
    invoke-virtual {p0, v0}, Lorg/apache/commons/pool/impl/CursorableSubList;->broadcastListableInserted(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1467
    return-object v0

    :cond_3
    move-object v1, p1

    .line 1455
    goto :goto_0

    :cond_4
    move-object v0, p2

    goto :goto_1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 1276
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1277
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1264
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1265
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1426
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1427
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1432
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1433
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1408
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1409
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 1414
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1415
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1300
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1301
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 1342
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1343
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public removeFirst()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1306
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1307
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public removeLast()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 1312
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1313
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected removeListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1475
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_modCount:I

    .line 1476
    iget v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_size:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_size:I

    .line 1477
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1478
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1479
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1481
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 1482
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->next()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setNext(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1484
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {v0}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v0

    if-ne v0, p1, :cond_2

    .line 1485
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_head:Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    invoke-virtual {p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->prev()Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;->setPrev(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1487
    :cond_2
    iget-object v0, p0, Lorg/apache/commons/pool/impl/CursorableSubList;->_list:Lorg/apache/commons/pool/impl/CursorableLinkedList;

    invoke-virtual {v0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->removeListable(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1488
    invoke-virtual {p0, p1}, Lorg/apache/commons/pool/impl/CursorableSubList;->broadcastListableRemoved(Lorg/apache/commons/pool/impl/CursorableLinkedList$Listable;)V

    .line 1489
    return-void
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 1366
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1367
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .prologue
    .line 1372
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1373
    invoke-super {p0, p1, p2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1270
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1271
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->size()I

    move-result v0

    return v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1438
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1439
    invoke-super {p0, p1, p2}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1282
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1283
    invoke-super {p0}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 1288
    invoke-virtual {p0}, Lorg/apache/commons/pool/impl/CursorableSubList;->checkForComod()V

    .line 1289
    invoke-super {p0, p1}, Lorg/apache/commons/pool/impl/CursorableLinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
