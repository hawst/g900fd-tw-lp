.class Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;
.super Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/pool/PoolUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ErodingPerKeyKeyedObjectPool"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final factor:F

.field private final factors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Lorg/apache/commons/pool/PoolUtils$ErodingFactor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/commons/pool/KeyedObjectPool;F)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/pool/KeyedObjectPool",
            "<TK;TV;>;F)V"
        }
    .end annotation

    .prologue
    .line 2438
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/commons/pool/PoolUtils$ErodingKeyedObjectPool;-><init>(Lorg/apache/commons/pool/KeyedObjectPool;Lorg/apache/commons/pool/PoolUtils$ErodingFactor;)V

    .line 2429
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;->factors:Ljava/util/Map;

    .line 2439
    iput p2, p0, Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;->factor:F

    .line 2440
    return-void
.end method


# virtual methods
.method protected getErodingFactor(Ljava/lang/Object;)Lorg/apache/commons/pool/PoolUtils$ErodingFactor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lorg/apache/commons/pool/PoolUtils$ErodingFactor;"
        }
    .end annotation

    .prologue
    .line 2455
    iget-object v0, p0, Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;->factors:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    .line 2458
    if-nez v0, :cond_0

    .line 2459
    new-instance v0, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;

    iget v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;->factor:F

    invoke-direct {v0, v1}, Lorg/apache/commons/pool/PoolUtils$ErodingFactor;-><init>(F)V

    .line 2460
    iget-object v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;->factors:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2462
    :cond_0
    return-object v0
.end method

.method protected numIdle(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    .line 2447
    invoke-virtual {p0}, Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;->getKeyedPool()Lorg/apache/commons/pool/KeyedObjectPool;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/apache/commons/pool/KeyedObjectPool;->getNumIdle(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2470
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ErodingPerKeyKeyedObjectPool{factor="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;->factor:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", keyedPool="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/commons/pool/PoolUtils$ErodingPerKeyKeyedObjectPool;->getKeyedPool()Lorg/apache/commons/pool/KeyedObjectPool;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
