.class Lorg/apache/commons/collections/MultiHashMap$ValueIterator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/commons/collections/MultiHashMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ValueIterator"
.end annotation


# instance fields
.field private backedIterator:Ljava/util/Iterator;

.field private tempIterator:Ljava/util/Iterator;

.field private final this$0:Lorg/apache/commons/collections/MultiHashMap;


# direct methods
.method private constructor <init>(Lorg/apache/commons/collections/MultiHashMap;)V
    .locals 1

    .prologue
    .line 422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->this$0:Lorg/apache/commons/collections/MultiHashMap;

    .line 423
    invoke-virtual {p1}, Lorg/apache/commons/collections/MultiHashMap;->superValuesIterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->backedIterator:Ljava/util/Iterator;

    .line 424
    return-void
.end method

.method constructor <init>(Lorg/apache/commons/collections/MultiHashMap;Lorg/apache/commons/collections/MultiHashMap$1;)V
    .locals 0

    .prologue
    .line 418
    invoke-direct {p0, p1}, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;-><init>(Lorg/apache/commons/collections/MultiHashMap;)V

    return-void
.end method

.method private searchNextIterator()Z
    .locals 1

    .prologue
    .line 427
    :goto_0
    iget-object v0, p0, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->tempIterator:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->tempIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 428
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->backedIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 429
    const/4 v0, 0x0

    .line 433
    :goto_1
    return v0

    .line 431
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->backedIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->tempIterator:Ljava/util/Iterator;

    goto :goto_0

    .line 433
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 437
    invoke-direct {p0}, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->searchNextIterator()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 441
    invoke-direct {p0}, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->searchNextIterator()Z

    move-result v0

    if-nez v0, :cond_0

    .line 442
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 444
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->tempIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->tempIterator:Ljava/util/Iterator;

    if-nez v0, :cond_0

    .line 449
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 451
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/collections/MultiHashMap$ValueIterator;->tempIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 452
    return-void
.end method
