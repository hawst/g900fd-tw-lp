.class Lorg/apache/commons/collections/BinaryHeap$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private index:I

.field private lastReturnedIndex:I

.field private final this$0:Lorg/apache/commons/collections/BinaryHeap;


# direct methods
.method constructor <init>(Lorg/apache/commons/collections/BinaryHeap;)V
    .locals 1

    .prologue
    .line 482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    .line 468
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->index:I

    .line 469
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 472
    iget v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->index:I

    iget-object v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget v1, v1, Lorg/apache/commons/collections/BinaryHeap;->m_size:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 476
    invoke-virtual {p0}, Lorg/apache/commons/collections/BinaryHeap$1;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 477
    :cond_0
    iget v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->index:I

    iput v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    .line 478
    iget v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->index:I

    .line 479
    iget-object v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget-object v0, v0, Lorg/apache/commons/collections/BinaryHeap;->m_elements:[Ljava/lang/Object;

    iget v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public remove()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 483
    iget v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    if-ne v0, v5, :cond_0

    .line 484
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 486
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget-object v0, v0, Lorg/apache/commons/collections/BinaryHeap;->m_elements:[Ljava/lang/Object;

    iget v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    iget-object v2, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget-object v2, v2, Lorg/apache/commons/collections/BinaryHeap;->m_elements:[Ljava/lang/Object;

    iget-object v3, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget v3, v3, Lorg/apache/commons/collections/BinaryHeap;->m_size:I

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    .line 487
    iget-object v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget-object v0, v0, Lorg/apache/commons/collections/BinaryHeap;->m_elements:[Ljava/lang/Object;

    iget-object v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget v1, v1, Lorg/apache/commons/collections/BinaryHeap;->m_size:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 488
    iget-object v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget v1, v0, Lorg/apache/commons/collections/BinaryHeap;->m_size:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lorg/apache/commons/collections/BinaryHeap;->m_size:I

    .line 489
    iget-object v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget v0, v0, Lorg/apache/commons/collections/BinaryHeap;->m_size:I

    if-eqz v0, :cond_2

    iget v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    iget-object v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget v1, v1, Lorg/apache/commons/collections/BinaryHeap;->m_size:I

    if-gt v0, v1, :cond_2

    .line 490
    const/4 v0, 0x0

    .line 491
    iget v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    if-le v1, v4, :cond_1

    .line 492
    iget-object v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget-object v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget-object v1, v1, Lorg/apache/commons/collections/BinaryHeap;->m_elements:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    aget-object v1, v1, v2

    iget-object v2, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget-object v2, v2, Lorg/apache/commons/collections/BinaryHeap;->m_elements:[Ljava/lang/Object;

    iget v3, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    div-int/lit8 v3, v3, 0x2

    aget-object v2, v2, v3

    invoke-static {v0, v1, v2}, Lorg/apache/commons/collections/BinaryHeap;->access$000(Lorg/apache/commons/collections/BinaryHeap;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 495
    :cond_1
    iget-object v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget-boolean v1, v1, Lorg/apache/commons/collections/BinaryHeap;->m_isMinHeap:Z

    if-eqz v1, :cond_4

    .line 496
    iget v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    if-le v1, v4, :cond_3

    if-gez v0, :cond_3

    .line 497
    iget-object v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    invoke-virtual {v0, v1}, Lorg/apache/commons/collections/BinaryHeap;->percolateUpMinHeap(I)V

    .line 509
    :cond_2
    :goto_0
    iget v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->index:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->index:I

    .line 510
    iput v5, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    .line 511
    return-void

    .line 499
    :cond_3
    iget-object v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    invoke-virtual {v0, v1}, Lorg/apache/commons/collections/BinaryHeap;->percolateDownMinHeap(I)V

    goto :goto_0

    .line 502
    :cond_4
    iget v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    if-le v1, v4, :cond_5

    if-lez v0, :cond_5

    .line 503
    iget-object v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    invoke-virtual {v0, v1}, Lorg/apache/commons/collections/BinaryHeap;->percolateUpMaxHeap(I)V

    goto :goto_0

    .line 505
    :cond_5
    iget-object v0, p0, Lorg/apache/commons/collections/BinaryHeap$1;->this$0:Lorg/apache/commons/collections/BinaryHeap;

    iget v1, p0, Lorg/apache/commons/collections/BinaryHeap$1;->lastReturnedIndex:I

    invoke-virtual {v0, v1}, Lorg/apache/commons/collections/BinaryHeap;->percolateDownMaxHeap(I)V

    goto :goto_0
.end method
