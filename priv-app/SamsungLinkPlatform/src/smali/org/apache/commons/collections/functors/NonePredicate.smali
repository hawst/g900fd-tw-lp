.class public final Lorg/apache/commons/collections/functors/NonePredicate;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/apache/commons/collections/Predicate;
.implements Lorg/apache/commons/collections/functors/PredicateDecorator;


# static fields
.field private static final serialVersionUID:J = 0x1bdc79727f17bb61L


# instance fields
.field private final iPredicates:[Lorg/apache/commons/collections/Predicate;


# direct methods
.method public constructor <init>([Lorg/apache/commons/collections/Predicate;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-object p1, p0, Lorg/apache/commons/collections/functors/NonePredicate;->iPredicates:[Lorg/apache/commons/collections/Predicate;

    .line 92
    return-void
.end method

.method public static getInstance(Ljava/util/Collection;)Lorg/apache/commons/collections/Predicate;
    .locals 2

    .prologue
    .line 76
    invoke-static {p0}, Lorg/apache/commons/collections/functors/FunctorUtils;->validate(Ljava/util/Collection;)[Lorg/apache/commons/collections/Predicate;

    move-result-object v1

    .line 77
    array-length v0, v1

    if-nez v0, :cond_0

    .line 78
    sget-object v0, Lorg/apache/commons/collections/functors/TruePredicate;->INSTANCE:Lorg/apache/commons/collections/Predicate;

    .line 80
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/commons/collections/functors/NonePredicate;

    invoke-direct {v0, v1}, Lorg/apache/commons/collections/functors/NonePredicate;-><init>([Lorg/apache/commons/collections/Predicate;)V

    goto :goto_0
.end method

.method public static getInstance([Lorg/apache/commons/collections/Predicate;)Lorg/apache/commons/collections/Predicate;
    .locals 2

    .prologue
    .line 57
    invoke-static {p0}, Lorg/apache/commons/collections/functors/FunctorUtils;->validate([Lorg/apache/commons/collections/Predicate;)V

    .line 58
    array-length v0, p0

    if-nez v0, :cond_0

    .line 59
    sget-object v0, Lorg/apache/commons/collections/functors/TruePredicate;->INSTANCE:Lorg/apache/commons/collections/Predicate;

    .line 62
    :goto_0
    return-object v0

    .line 61
    :cond_0
    invoke-static {p0}, Lorg/apache/commons/collections/functors/FunctorUtils;->copy([Lorg/apache/commons/collections/Predicate;)[Lorg/apache/commons/collections/Predicate;

    move-result-object v1

    .line 62
    new-instance v0, Lorg/apache/commons/collections/functors/NonePredicate;

    invoke-direct {v0, v1}, Lorg/apache/commons/collections/functors/NonePredicate;-><init>([Lorg/apache/commons/collections/Predicate;)V

    goto :goto_0
.end method


# virtual methods
.method public final evaluate(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 101
    move v0, v1

    :goto_0
    iget-object v2, p0, Lorg/apache/commons/collections/functors/NonePredicate;->iPredicates:[Lorg/apache/commons/collections/Predicate;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 102
    iget-object v2, p0, Lorg/apache/commons/collections/functors/NonePredicate;->iPredicates:[Lorg/apache/commons/collections/Predicate;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Lorg/apache/commons/collections/Predicate;->evaluate(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    :goto_1
    return v1

    .line 101
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final getPredicates()[Lorg/apache/commons/collections/Predicate;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/commons/collections/functors/NonePredicate;->iPredicates:[Lorg/apache/commons/collections/Predicate;

    return-object v0
.end method
