.class public final Lorg/apache/commons/collections/functors/AnyPredicate;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/apache/commons/collections/Predicate;
.implements Lorg/apache/commons/collections/functors/PredicateDecorator;


# static fields
.field private static final serialVersionUID:J = 0x671ca93522adbaf6L


# instance fields
.field private final iPredicates:[Lorg/apache/commons/collections/Predicate;


# direct methods
.method public constructor <init>([Lorg/apache/commons/collections/Predicate;)V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lorg/apache/commons/collections/functors/AnyPredicate;->iPredicates:[Lorg/apache/commons/collections/Predicate;

    .line 99
    return-void
.end method

.method public static getInstance(Ljava/util/Collection;)Lorg/apache/commons/collections/Predicate;
    .locals 3

    .prologue
    .line 80
    invoke-static {p0}, Lorg/apache/commons/collections/functors/FunctorUtils;->validate(Ljava/util/Collection;)[Lorg/apache/commons/collections/Predicate;

    move-result-object v1

    .line 81
    array-length v0, v1

    if-nez v0, :cond_0

    .line 82
    sget-object v0, Lorg/apache/commons/collections/functors/FalsePredicate;->INSTANCE:Lorg/apache/commons/collections/Predicate;

    .line 87
    :goto_0
    return-object v0

    .line 84
    :cond_0
    array-length v0, v1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 85
    const/4 v0, 0x0

    aget-object v0, v1, v0

    goto :goto_0

    .line 87
    :cond_1
    new-instance v0, Lorg/apache/commons/collections/functors/AnyPredicate;

    invoke-direct {v0, v1}, Lorg/apache/commons/collections/functors/AnyPredicate;-><init>([Lorg/apache/commons/collections/Predicate;)V

    goto :goto_0
.end method

.method public static getInstance([Lorg/apache/commons/collections/Predicate;)Lorg/apache/commons/collections/Predicate;
    .locals 2

    .prologue
    .line 58
    invoke-static {p0}, Lorg/apache/commons/collections/functors/FunctorUtils;->validate([Lorg/apache/commons/collections/Predicate;)V

    .line 59
    array-length v0, p0

    if-nez v0, :cond_0

    .line 60
    sget-object v0, Lorg/apache/commons/collections/functors/FalsePredicate;->INSTANCE:Lorg/apache/commons/collections/Predicate;

    .line 65
    :goto_0
    return-object v0

    .line 62
    :cond_0
    array-length v0, p0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 63
    const/4 v0, 0x0

    aget-object v0, p0, v0

    goto :goto_0

    .line 65
    :cond_1
    new-instance v0, Lorg/apache/commons/collections/functors/AnyPredicate;

    invoke-static {p0}, Lorg/apache/commons/collections/functors/FunctorUtils;->copy([Lorg/apache/commons/collections/Predicate;)[Lorg/apache/commons/collections/Predicate;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/commons/collections/functors/AnyPredicate;-><init>([Lorg/apache/commons/collections/Predicate;)V

    goto :goto_0
.end method


# virtual methods
.method public final evaluate(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 108
    move v0, v1

    :goto_0
    iget-object v2, p0, Lorg/apache/commons/collections/functors/AnyPredicate;->iPredicates:[Lorg/apache/commons/collections/Predicate;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 109
    iget-object v2, p0, Lorg/apache/commons/collections/functors/AnyPredicate;->iPredicates:[Lorg/apache/commons/collections/Predicate;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Lorg/apache/commons/collections/Predicate;->evaluate(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110
    const/4 v1, 0x1

    .line 113
    :cond_0
    return v1

    .line 108
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getPredicates()[Lorg/apache/commons/collections/Predicate;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lorg/apache/commons/collections/functors/AnyPredicate;->iPredicates:[Lorg/apache/commons/collections/Predicate;

    return-object v0
.end method
