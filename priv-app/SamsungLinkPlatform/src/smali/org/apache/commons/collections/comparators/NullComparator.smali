.class public Lorg/apache/commons/collections/comparators/NullComparator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# static fields
.field private static final serialVersionUID:J = -0x50c789d15007a6d3L


# instance fields
.field private nonNullComparator:Ljava/util/Comparator;

.field private nullsAreHigh:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 55
    invoke-static {}, Lorg/apache/commons/collections/comparators/ComparableComparator;->getInstance()Lorg/apache/commons/collections/comparators/ComparableComparator;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/collections/comparators/NullComparator;-><init>(Ljava/util/Comparator;Z)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/commons/collections/comparators/NullComparator;-><init>(Ljava/util/Comparator;Z)V

    .line 73
    return-void
.end method

.method public constructor <init>(Ljava/util/Comparator;Z)V
    .locals 2

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object p1, p0, Lorg/apache/commons/collections/comparators/NullComparator;->nonNullComparator:Ljava/util/Comparator;

    .line 112
    iput-boolean p2, p0, Lorg/apache/commons/collections/comparators/NullComparator;->nullsAreHigh:Z

    .line 114
    if-nez p1, :cond_0

    .line 115
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null nonNullComparator"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_0
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 88
    invoke-static {}, Lorg/apache/commons/collections/comparators/ComparableComparator;->getInstance()Lorg/apache/commons/collections/comparators/ComparableComparator;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lorg/apache/commons/collections/comparators/NullComparator;-><init>(Ljava/util/Comparator;Z)V

    .line 89
    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 137
    if-ne p1, p2, :cond_1

    const/4 v0, 0x0

    .line 140
    :cond_0
    :goto_0
    return v0

    .line 138
    :cond_1
    if-nez p1, :cond_2

    iget-boolean v2, p0, Lorg/apache/commons/collections/comparators/NullComparator;->nullsAreHigh:Z

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 139
    :cond_2
    if-nez p2, :cond_3

    iget-boolean v2, p0, Lorg/apache/commons/collections/comparators/NullComparator;->nullsAreHigh:Z

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 140
    :cond_3
    iget-object v0, p0, Lorg/apache/commons/collections/comparators/NullComparator;->nonNullComparator:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 166
    if-nez p1, :cond_1

    .line 172
    :cond_0
    :goto_0
    return v0

    .line 167
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    goto :goto_0

    .line 168
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    check-cast p1, Lorg/apache/commons/collections/comparators/NullComparator;

    .line 172
    iget-boolean v2, p0, Lorg/apache/commons/collections/comparators/NullComparator;->nullsAreHigh:Z

    iget-boolean v3, p1, Lorg/apache/commons/collections/comparators/NullComparator;->nullsAreHigh:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/commons/collections/comparators/NullComparator;->nonNullComparator:Ljava/util/Comparator;

    iget-object v3, p1, Lorg/apache/commons/collections/comparators/NullComparator;->nonNullComparator:Ljava/util/Comparator;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 151
    iget-boolean v0, p0, Lorg/apache/commons/collections/comparators/NullComparator;->nullsAreHigh:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iget-object v1, p0, Lorg/apache/commons/collections/comparators/NullComparator;->nonNullComparator:Ljava/util/Comparator;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    mul-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
