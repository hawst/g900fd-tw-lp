.class Lorg/apache/commons/collections/SequencedHashMap$3;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field private final this$0:Lorg/apache/commons/collections/SequencedHashMap;


# direct methods
.method constructor <init>(Lorg/apache/commons/collections/SequencedHashMap;)V
    .locals 0

    .prologue
    .line 677
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    iput-object p1, p0, Lorg/apache/commons/collections/SequencedHashMap$3;->this$0:Lorg/apache/commons/collections/SequencedHashMap;

    return-void
.end method

.method private findEntry(Ljava/lang/Object;)Lorg/apache/commons/collections/SequencedHashMap$Entry;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 642
    if-nez p1, :cond_1

    move-object v0, v1

    .line 652
    :cond_0
    :goto_0
    return-object v0

    .line 644
    :cond_1
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_2

    move-object v0, v1

    .line 645
    goto :goto_0

    .line 647
    :cond_2
    check-cast p1, Ljava/util/Map$Entry;

    .line 648
    iget-object v0, p0, Lorg/apache/commons/collections/SequencedHashMap$3;->this$0:Lorg/apache/commons/collections/SequencedHashMap;

    invoke-static {v0}, Lorg/apache/commons/collections/SequencedHashMap;->access$200(Lorg/apache/commons/collections/SequencedHashMap;)Ljava/util/HashMap;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/commons/collections/SequencedHashMap$Entry;

    .line 649
    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lorg/apache/commons/collections/SequencedHashMap$Entry;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move-object v0, v1

    .line 652
    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lorg/apache/commons/collections/SequencedHashMap$3;->this$0:Lorg/apache/commons/collections/SequencedHashMap;

    invoke-virtual {v0}, Lorg/apache/commons/collections/SequencedHashMap;->clear()V

    .line 670
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 678
    invoke-direct {p0, p1}, Lorg/apache/commons/collections/SequencedHashMap$3;->findEntry(Ljava/lang/Object;)Lorg/apache/commons/collections/SequencedHashMap$Entry;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lorg/apache/commons/collections/SequencedHashMap$3;->this$0:Lorg/apache/commons/collections/SequencedHashMap;

    invoke-virtual {v0}, Lorg/apache/commons/collections/SequencedHashMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 3

    .prologue
    .line 657
    new-instance v0, Lorg/apache/commons/collections/SequencedHashMap$OrderedIterator;

    iget-object v1, p0, Lorg/apache/commons/collections/SequencedHashMap$3;->this$0:Lorg/apache/commons/collections/SequencedHashMap;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lorg/apache/commons/collections/SequencedHashMap$OrderedIterator;-><init>(Lorg/apache/commons/collections/SequencedHashMap;I)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 660
    invoke-direct {p0, p1}, Lorg/apache/commons/collections/SequencedHashMap$3;->findEntry(Ljava/lang/Object;)Lorg/apache/commons/collections/SequencedHashMap$Entry;

    move-result-object v1

    .line 661
    if-nez v1, :cond_1

    .line 664
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lorg/apache/commons/collections/SequencedHashMap$3;->this$0:Lorg/apache/commons/collections/SequencedHashMap;

    invoke-virtual {v1}, Lorg/apache/commons/collections/SequencedHashMap$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v2, v1}, Lorg/apache/commons/collections/SequencedHashMap;->access$000(Lorg/apache/commons/collections/SequencedHashMap;Ljava/lang/Object;)Lorg/apache/commons/collections/SequencedHashMap$Entry;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lorg/apache/commons/collections/SequencedHashMap$3;->this$0:Lorg/apache/commons/collections/SequencedHashMap;

    invoke-virtual {v0}, Lorg/apache/commons/collections/SequencedHashMap;->size()I

    move-result v0

    return v0
.end method
