.class public Lorg/apache/commons/collections/set/CompositeSet;
.super Lorg/apache/commons/collections/collection/CompositeCollection;
.source "SourceFile"

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/collections/set/CompositeSet$SetMutator;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/commons/collections/collection/CompositeCollection;-><init>()V

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lorg/apache/commons/collections/collection/CompositeCollection;-><init>(Ljava/util/Collection;)V

    .line 52
    return-void
.end method

.method public constructor <init>([Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lorg/apache/commons/collections/collection/CompositeCollection;-><init>([Ljava/util/Collection;)V

    .line 59
    return-void
.end method


# virtual methods
.method public declared-synchronized addComposited(Ljava/util/Collection;)V
    .locals 6

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    instance-of v1, p1, Ljava/util/Set;

    if-nez v1, :cond_0

    .line 74
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Collections added must implement java.util.Set"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 77
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/commons/collections/set/CompositeSet;->getCollections()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 78
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 79
    invoke-static {v1, p1}, Lorg/apache/commons/collections/CollectionUtils;->intersection(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v5

    .line 80
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 81
    iget-object v2, p0, Lorg/apache/commons/collections/set/CompositeSet;->mutator:Lorg/apache/commons/collections/collection/CompositeCollection$CollectionMutator;

    if-nez v2, :cond_2

    .line 82
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Collision adding composited collection with no SetMutator set"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 85
    :cond_2
    iget-object v2, p0, Lorg/apache/commons/collections/set/CompositeSet;->mutator:Lorg/apache/commons/collections/collection/CompositeCollection$CollectionMutator;

    instance-of v2, v2, Lorg/apache/commons/collections/set/CompositeSet$SetMutator;

    if-nez v2, :cond_3

    .line 86
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Collision adding composited collection to a CompositeSet with a CollectionMutator instead of a SetMutator"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 89
    :cond_3
    iget-object v2, p0, Lorg/apache/commons/collections/set/CompositeSet;->mutator:Lorg/apache/commons/collections/collection/CompositeCollection$CollectionMutator;

    check-cast v2, Lorg/apache/commons/collections/set/CompositeSet$SetMutator;

    move-object v0, p1

    check-cast v0, Ljava/util/Set;

    move-object v3, v0

    invoke-interface {v2, p0, v1, v3, v5}, Lorg/apache/commons/collections/set/CompositeSet$SetMutator;->resolveCollision(Lorg/apache/commons/collections/set/CompositeSet;Ljava/util/Set;Ljava/util/Set;Ljava/util/Collection;)V

    .line 90
    invoke-static {v1, p1}, Lorg/apache/commons/collections/CollectionUtils;->intersection(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 91
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Attempt to add illegal entry unresolved by SetMutator.resolveCollision()"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 96
    :cond_4
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/util/Collection;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-super {p0, v1}, Lorg/apache/commons/collections/collection/CompositeCollection;->addComposited([Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized addComposited(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 2

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    instance-of v0, p1, Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument must implement java.util.Set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 106
    :cond_0
    :try_start_1
    instance-of v0, p2, Ljava/util/Set;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument must implement java.util.Set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/Set;

    const/4 v1, 0x0

    check-cast p1, Ljava/util/Set;

    aput-object p1, v0, v1

    const/4 v1, 0x1

    check-cast p2, Ljava/util/Set;

    aput-object p2, v0, v1

    invoke-virtual {p0, v0}, Lorg/apache/commons/collections/set/CompositeSet;->addComposited([Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized addComposited([Ljava/util/Collection;)V
    .locals 2

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 117
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lorg/apache/commons/collections/set/CompositeSet;->addComposited(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 119
    :cond_0
    monitor-exit p0

    return-void

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 154
    instance-of v0, p1, Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 155
    check-cast p1, Ljava/util/Set;

    .line 156
    invoke-interface {p1, p0}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/commons/collections/set/CompositeSet;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 157
    const/4 v0, 0x1

    .line 160
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 167
    .line 168
    invoke-virtual {p0}, Lorg/apache/commons/collections/set/CompositeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v0, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 169
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 170
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_1
    add-int/2addr v0, v2

    .line 171
    goto :goto_0

    :cond_0
    move v2, v1

    .line 170
    goto :goto_1

    .line 172
    :cond_1
    return v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 142
    invoke-virtual {p0}, Lorg/apache/commons/collections/set/CompositeSet;->getCollections()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 144
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 146
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMutator(Lorg/apache/commons/collections/collection/CompositeCollection$CollectionMutator;)V
    .locals 0

    .prologue
    .line 129
    invoke-super {p0, p1}, Lorg/apache/commons/collections/collection/CompositeCollection;->setMutator(Lorg/apache/commons/collections/collection/CompositeCollection$CollectionMutator;)V

    .line 130
    return-void
.end method
