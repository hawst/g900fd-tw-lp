.class public abstract Lorg/apache/commons/collections/DefaultMapBag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/collections/Bag;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/collections/DefaultMapBag$BagIterator;
    }
.end annotation


# instance fields
.field private _map:Ljava/util/Map;

.field private _mods:I

.field private _total:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    .line 50
    iput v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    .line 51
    iput v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_mods:I

    .line 59
    return-void
.end method

.method protected constructor <init>(Ljava/util/Map;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    .line 50
    iput v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    .line 51
    iput v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_mods:I

    .line 68
    invoke-virtual {p0, p1}, Lorg/apache/commons/collections/DefaultMapBag;->setMap(Ljava/util/Map;)V

    .line 69
    return-void
.end method

.method static access$000(Lorg/apache/commons/collections/DefaultMapBag;)I
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/commons/collections/DefaultMapBag;->modCount()I

    move-result v0

    return v0
.end method

.method private extractList()Ljava/util/List;
    .locals 4

    .prologue
    .line 412
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 413
    invoke-virtual {p0}, Lorg/apache/commons/collections/DefaultMapBag;->uniqueSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 414
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 416
    invoke-virtual {p0, v3}, Lorg/apache/commons/collections/DefaultMapBag;->getCount(Ljava/lang/Object;)I

    move-result v0

    :goto_0
    if-lez v0, :cond_0

    .line 417
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 420
    :cond_1
    return-object v1
.end method

.method private modCount()I
    .locals 1

    .prologue
    .line 429
    iget v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_mods:I

    return v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lorg/apache/commons/collections/DefaultMapBag;->add(Ljava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method public add(Ljava/lang/Object;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 90
    iget v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_mods:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_mods:I

    .line 91
    if-lez p2, :cond_0

    .line 92
    invoke-virtual {p0, p1}, Lorg/apache/commons/collections/DefaultMapBag;->getCount(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v1, p2

    .line 93
    iget-object v2, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget v2, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    add-int/2addr v2, p2

    iput v2, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    .line 95
    if-ne v1, p2, :cond_0

    const/4 v0, 0x1

    .line 97
    :cond_0
    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 108
    .line 109
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v0, v1

    .line 110
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 111
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/commons/collections/DefaultMapBag;->add(Ljava/lang/Object;)Z

    move-result v3

    .line 112
    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 114
    :cond_2
    return v0
.end method

.method protected calcTotalSize()I
    .locals 1

    .prologue
    .line 383
    invoke-direct {p0}, Lorg/apache/commons/collections/DefaultMapBag;->extractList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    .line 384
    iget v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_mods:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_mods:I

    .line 122
    iget-object v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 123
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    .line 124
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lorg/apache/commons/collections/HashBag;

    invoke-direct {v0, p1}, Lorg/apache/commons/collections/HashBag;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lorg/apache/commons/collections/DefaultMapBag;->containsAll(Lorg/apache/commons/collections/Bag;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Lorg/apache/commons/collections/Bag;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 155
    .line 156
    invoke-interface {p1}, Lorg/apache/commons/collections/Bag;->uniqueSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v0, v1

    .line 157
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 158
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 159
    invoke-virtual {p0, v3}, Lorg/apache/commons/collections/DefaultMapBag;->getCount(Ljava/lang/Object;)I

    move-result v5

    invoke-interface {p1, v3}, Lorg/apache/commons/collections/Bag;->getCount(Ljava/lang/Object;)I

    move-result v3

    if-lt v5, v3, :cond_0

    move v3, v1

    .line 160
    :goto_1
    if-eqz v0, :cond_1

    if-eqz v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_0
    move v3, v2

    .line 159
    goto :goto_1

    :cond_1
    move v0, v2

    .line 160
    goto :goto_0

    .line 162
    :cond_2
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 174
    if-ne p1, p0, :cond_1

    .line 190
    :cond_0
    :goto_0
    return v0

    .line 177
    :cond_1
    instance-of v2, p1, Lorg/apache/commons/collections/Bag;

    if-nez v2, :cond_2

    move v0, v1

    .line 178
    goto :goto_0

    .line 180
    :cond_2
    check-cast p1, Lorg/apache/commons/collections/Bag;

    .line 181
    invoke-interface {p1}, Lorg/apache/commons/collections/Bag;->size()I

    move-result v2

    invoke-virtual {p0}, Lorg/apache/commons/collections/DefaultMapBag;->size()I

    move-result v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 182
    goto :goto_0

    .line 184
    :cond_3
    iget-object v2, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 185
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 186
    invoke-interface {p1, v3}, Lorg/apache/commons/collections/Bag;->getCount(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p0, v3}, Lorg/apache/commons/collections/DefaultMapBag;->getCount(Ljava/lang/Object;)I

    move-result v3

    if-eq v4, v3, :cond_4

    move v0, v1

    .line 187
    goto :goto_0
.end method

.method public getCount(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 350
    const/4 v0, 0x0

    .line 351
    iget-object v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    invoke-static {v1, p1}, Lorg/apache/commons/collections/MapUtils;->getInteger(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v1

    .line 352
    if-eqz v1, :cond_0

    .line 353
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 355
    :cond_0
    return v0
.end method

.method protected getMap()Ljava/util/Map;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 212
    new-instance v0, Lorg/apache/commons/collections/DefaultMapBag$BagIterator;

    invoke-direct {p0}, Lorg/apache/commons/collections/DefaultMapBag;->extractList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/apache/commons/collections/DefaultMapBag$BagIterator;-><init>(Lorg/apache/commons/collections/DefaultMapBag;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 251
    invoke-virtual {p0, p1}, Lorg/apache/commons/collections/DefaultMapBag;->getCount(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/commons/collections/DefaultMapBag;->remove(Ljava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method public remove(Ljava/lang/Object;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 255
    iget v2, p0, Lorg/apache/commons/collections/DefaultMapBag;->_mods:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/commons/collections/DefaultMapBag;->_mods:I

    .line 256
    invoke-virtual {p0, p1}, Lorg/apache/commons/collections/DefaultMapBag;->getCount(Ljava/lang/Object;)I

    move-result v2

    .line 258
    if-gtz p2, :cond_0

    move v0, v1

    .line 269
    :goto_0
    return v0

    .line 260
    :cond_0
    if-le v2, p2, :cond_1

    .line 261
    iget-object v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    new-instance v3, Ljava/lang/Integer;

    sub-int/2addr v2, p2

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v1, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    iget v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    sub-int/2addr v1, p2

    iput v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    goto :goto_0

    .line 266
    :cond_1
    iget-object v3, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 267
    :goto_1
    iget v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    sub-int/2addr v1, v2

    iput v1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 266
    goto :goto_1
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 273
    .line 274
    if-eqz p1, :cond_2

    .line 275
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v0, v1

    .line 276
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 277
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4, v2}, Lorg/apache/commons/collections/DefaultMapBag;->remove(Ljava/lang/Object;I)Z

    move-result v4

    .line 278
    if-nez v0, :cond_0

    if-eqz v4, :cond_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 281
    :cond_3
    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 292
    new-instance v0, Lorg/apache/commons/collections/HashBag;

    invoke-direct {v0, p1}, Lorg/apache/commons/collections/HashBag;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lorg/apache/commons/collections/DefaultMapBag;->retainAll(Lorg/apache/commons/collections/Bag;)Z

    move-result v0

    return v0
.end method

.method public retainAll(Lorg/apache/commons/collections/Bag;)Z
    .locals 6

    .prologue
    .line 304
    const/4 v0, 0x0

    .line 305
    new-instance v1, Lorg/apache/commons/collections/HashBag;

    invoke-direct {v1}, Lorg/apache/commons/collections/HashBag;-><init>()V

    .line 306
    invoke-virtual {p0}, Lorg/apache/commons/collections/DefaultMapBag;->uniqueSet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 307
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 308
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 309
    invoke-virtual {p0, v3}, Lorg/apache/commons/collections/DefaultMapBag;->getCount(Ljava/lang/Object;)I

    move-result v4

    .line 310
    invoke-interface {p1, v3}, Lorg/apache/commons/collections/Bag;->getCount(Ljava/lang/Object;)I

    move-result v5

    .line 311
    if-lez v5, :cond_0

    if-gt v5, v4, :cond_0

    .line 312
    sub-int/2addr v4, v5

    invoke-interface {v1, v3, v4}, Lorg/apache/commons/collections/Bag;->add(Ljava/lang/Object;I)Z

    goto :goto_0

    .line 314
    :cond_0
    invoke-interface {v1, v3, v4}, Lorg/apache/commons/collections/Bag;->add(Ljava/lang/Object;I)Z

    goto :goto_0

    .line 317
    :cond_1
    invoke-interface {v1}, Lorg/apache/commons/collections/Bag;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 318
    invoke-virtual {p0, v1}, Lorg/apache/commons/collections/DefaultMapBag;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    .line 320
    :cond_2
    return v0
.end method

.method protected setMap(Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 393
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 394
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The map must be non-null and empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 396
    :cond_1
    iput-object p1, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    .line 397
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 373
    iget v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_total:I

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 329
    invoke-direct {p0}, Lorg/apache/commons/collections/DefaultMapBag;->extractList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 339
    invoke-direct {p0}, Lorg/apache/commons/collections/DefaultMapBag;->extractList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 438
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 439
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 440
    invoke-virtual {p0}, Lorg/apache/commons/collections/DefaultMapBag;->uniqueSet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 441
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 442
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 443
    invoke-virtual {p0, v2}, Lorg/apache/commons/collections/DefaultMapBag;->getCount(Ljava/lang/Object;)I

    move-result v3

    .line 444
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 445
    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 446
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 447
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 448
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 451
    :cond_1
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 452
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public uniqueSet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lorg/apache/commons/collections/DefaultMapBag;->_map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/commons/collections/set/UnmodifiableSet;->decorate(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
