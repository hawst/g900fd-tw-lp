.class public Lorg/apache/commons/collections/map/CompositeMap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Map;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/collections/map/CompositeMap$MapMutator;
    }
.end annotation


# instance fields
.field private composite:[Ljava/util/Map;

.field private mutator:Lorg/apache/commons/collections/map/CompositeMap$MapMutator;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 58
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/util/Map;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/collections/map/CompositeMap;-><init>([Ljava/util/Map;Lorg/apache/commons/collections/map/CompositeMap$MapMutator;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 69
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/Map;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/commons/collections/map/CompositeMap;-><init>([Ljava/util/Map;Lorg/apache/commons/collections/map/CompositeMap$MapMutator;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Ljava/util/Map;Lorg/apache/commons/collections/map/CompositeMap$MapMutator;)V
    .locals 2

    .prologue
    .line 80
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/Map;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    invoke-direct {p0, v0, p3}, Lorg/apache/commons/collections/map/CompositeMap;-><init>([Ljava/util/Map;Lorg/apache/commons/collections/map/CompositeMap$MapMutator;)V

    .line 81
    return-void
.end method

.method public constructor <init>([Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/commons/collections/map/CompositeMap;-><init>([Ljava/util/Map;Lorg/apache/commons/collections/map/CompositeMap$MapMutator;)V

    .line 92
    return-void
.end method

.method public constructor <init>([Ljava/util/Map;Lorg/apache/commons/collections/map/CompositeMap$MapMutator;)V
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object p2, p0, Lorg/apache/commons/collections/map/CompositeMap;->mutator:Lorg/apache/commons/collections/map/CompositeMap$MapMutator;

    .line 103
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/util/Map;

    iput-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    .line 104
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 105
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lorg/apache/commons/collections/map/CompositeMap;->addComposited(Ljava/util/Map;)V

    .line 104
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 107
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized addComposited(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    .line 128
    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v1, v1, v0

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/apache/commons/collections/CollectionUtils;->intersection(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    .line 129
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    if-eqz v2, :cond_1

    .line 130
    iget-object v2, p0, Lorg/apache/commons/collections/map/CompositeMap;->mutator:Lorg/apache/commons/collections/map/CompositeMap$MapMutator;

    if-nez v2, :cond_0

    .line 131
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Key collision adding Map to CompositeMap"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 134
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/commons/collections/map/CompositeMap;->mutator:Lorg/apache/commons/collections/map/CompositeMap$MapMutator;

    iget-object v3, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v3, v3, v0

    invoke-interface {v2, p0, v3, p1, v1}, Lorg/apache/commons/collections/map/CompositeMap$MapMutator;->resolveCollision(Lorg/apache/commons/collections/map/CompositeMap;Ljava/util/Map;Ljava/util/Map;Ljava/util/Collection;)V

    .line 127
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 138
    :cond_2
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/util/Map;

    .line 139
    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v4, v4

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 140
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1

    .line 141
    iput-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    monitor-exit p0

    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 172
    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v1, v1, v0

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 171
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 174
    :cond_0
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 194
    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v1, v1, v0

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    const/4 v0, 0x1

    .line 198
    :goto_1
    return v0

    .line 193
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 198
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 219
    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v1, v1, v0

    invoke-interface {v1, p1}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    const/4 v0, 0x1

    .line 223
    :goto_1
    return v0

    .line 218
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 223
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public entrySet()Ljava/util/Set;
    .locals 3

    .prologue
    .line 244
    new-instance v1, Lorg/apache/commons/collections/set/CompositeSet;

    invoke-direct {v1}, Lorg/apache/commons/collections/set/CompositeSet;-><init>()V

    .line 245
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 246
    iget-object v2, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v2, v2, v0

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/commons/collections/set/CompositeSet;->addComposited(Ljava/util/Collection;)V

    .line 245
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 248
    :cond_0
    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 459
    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 460
    check-cast p1, Ljava/util/Map;

    .line 461
    invoke-virtual {p0}, Lorg/apache/commons/collections/map/CompositeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 463
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 277
    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v1, v1, v0

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 278
    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v0, v1, v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 281
    :goto_1
    return-object v0

    .line 276
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 281
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 470
    const/4 v0, 0x0

    .line 471
    invoke-virtual {p0}, Lorg/apache/commons/collections/map/CompositeMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 472
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 473
    goto :goto_0

    .line 474
    :cond_0
    return v0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 291
    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v1, v1, v0

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 292
    const/4 v0, 0x0

    .line 295
    :goto_1
    return v0

    .line 290
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 295
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public keySet()Ljava/util/Set;
    .locals 3

    .prologue
    .line 314
    new-instance v1, Lorg/apache/commons/collections/set/CompositeSet;

    invoke-direct {v1}, Lorg/apache/commons/collections/set/CompositeSet;-><init>()V

    .line 315
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 316
    iget-object v2, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v2, v2, v0

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/commons/collections/set/CompositeSet;->addComposited(Ljava/util/Collection;)V

    .line 315
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 318
    :cond_0
    return-object v1
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->mutator:Lorg/apache/commons/collections/map/CompositeMap$MapMutator;

    if-nez v0, :cond_0

    .line 348
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No mutator specified"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 350
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->mutator:Lorg/apache/commons/collections/map/CompositeMap$MapMutator;

    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    invoke-interface {v0, p0, v1, p1, p2}, Lorg/apache/commons/collections/map/CompositeMap$MapMutator;->put(Lorg/apache/commons/collections/map/CompositeMap;[Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->mutator:Lorg/apache/commons/collections/map/CompositeMap$MapMutator;

    if-nez v0, :cond_0

    .line 377
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No mutator specified"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 379
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->mutator:Lorg/apache/commons/collections/map/CompositeMap$MapMutator;

    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    invoke-interface {v0, p0, v1, p1}, Lorg/apache/commons/collections/map/CompositeMap$MapMutator;->putAll(Lorg/apache/commons/collections/map/CompositeMap;[Ljava/util/Map;Ljava/util/Map;)V

    .line 380
    return-void
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 408
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 409
    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v1, v1, v0

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 410
    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v0, v1, v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 413
    :goto_1
    return-object v0

    .line 408
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 413
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public declared-synchronized removeComposited(Ljava/util/Map;)Ljava/util/Map;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 151
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v1, v1

    .line 152
    :goto_0
    if-ge v0, v1, :cond_1

    .line 153
    iget-object v2, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    add-int/lit8 v2, v1, -0x1

    new-array v2, v2, [Ljava/util/Map;

    .line 155
    iget-object v3, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v2, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 156
    iget-object v3, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    add-int/lit8 v4, v0, 0x1

    sub-int/2addr v1, v0

    add-int/lit8 v1, v1, -0x1

    invoke-static {v3, v4, v2, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157
    iput-object v2, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :goto_1
    monitor-exit p0

    return-object p1

    .line 152
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setMutator(Lorg/apache/commons/collections/map/CompositeMap$MapMutator;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lorg/apache/commons/collections/map/CompositeMap;->mutator:Lorg/apache/commons/collections/map/CompositeMap$MapMutator;

    .line 117
    return-void
.end method

.method public size()I
    .locals 3

    .prologue
    .line 424
    const/4 v1, 0x0

    .line 425
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 426
    iget-object v2, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v2, v2, v0

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 425
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 428
    :cond_0
    return v1
.end method

.method public values()Ljava/util/Collection;
    .locals 3

    .prologue
    .line 445
    new-instance v1, Lorg/apache/commons/collections/collection/CompositeCollection;

    invoke-direct {v1}, Lorg/apache/commons/collections/collection/CompositeCollection;-><init>()V

    .line 446
    iget-object v0, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 447
    iget-object v2, p0, Lorg/apache/commons/collections/map/CompositeMap;->composite:[Ljava/util/Map;

    aget-object v2, v2, v0

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/commons/collections/collection/CompositeCollection;->addComposited(Ljava/util/Collection;)V

    .line 446
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 449
    :cond_0
    return-object v1
.end method
