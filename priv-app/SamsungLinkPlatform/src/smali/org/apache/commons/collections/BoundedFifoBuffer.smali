.class public Lorg/apache/commons/collections/BoundedFifoBuffer;
.super Ljava/util/AbstractCollection;
.source "SourceFile"

# interfaces
.implements Lorg/apache/commons/collections/BoundedCollection;
.implements Lorg/apache/commons/collections/Buffer;


# instance fields
.field private final m_elements:[Ljava/lang/Object;

.field private m_end:I

.field private m_full:Z

.field private m_start:I

.field private final maxElements:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lorg/apache/commons/collections/BoundedFifoBuffer;-><init>(I)V

    .line 70
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 59
    iput v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    .line 60
    iput v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    .line 61
    iput-boolean v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_full:Z

    .line 80
    if-gtz p1, :cond_0

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The size must be greater than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_elements:[Ljava/lang/Object;

    .line 84
    iget-object v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_elements:[Ljava/lang/Object;

    array-length v0, v0

    iput v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->maxElements:I

    .line 85
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 96
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/commons/collections/BoundedFifoBuffer;-><init>(I)V

    .line 97
    invoke-virtual {p0, p1}, Lorg/apache/commons/collections/BoundedFifoBuffer;->addAll(Ljava/util/Collection;)Z

    .line 98
    return-void
.end method

.method static access$000(Lorg/apache/commons/collections/BoundedFifoBuffer;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    return v0
.end method

.method static access$100(Lorg/apache/commons/collections/BoundedFifoBuffer;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_full:Z

    return v0
.end method

.method static access$102(Lorg/apache/commons/collections/BoundedFifoBuffer;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_full:Z

    return p1
.end method

.method static access$200(Lorg/apache/commons/collections/BoundedFifoBuffer;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    return v0
.end method

.method static access$202(Lorg/apache/commons/collections/BoundedFifoBuffer;I)I
    .locals 0

    .prologue
    .line 55
    iput p1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    return p1
.end method

.method static access$300(Lorg/apache/commons/collections/BoundedFifoBuffer;I)I
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lorg/apache/commons/collections/BoundedFifoBuffer;->increment(I)I

    move-result v0

    return v0
.end method

.method static access$400(Lorg/apache/commons/collections/BoundedFifoBuffer;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_elements:[Ljava/lang/Object;

    return-object v0
.end method

.method static access$500(Lorg/apache/commons/collections/BoundedFifoBuffer;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->maxElements:I

    return v0
.end method

.method static access$600(Lorg/apache/commons/collections/BoundedFifoBuffer;I)I
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lorg/apache/commons/collections/BoundedFifoBuffer;->decrement(I)I

    move-result v0

    return v0
.end method

.method private decrement(I)I
    .locals 1

    .prologue
    .line 247
    add-int/lit8 v0, p1, -0x1

    .line 248
    if-gez v0, :cond_0

    .line 249
    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->maxElements:I

    add-int/lit8 v0, v0, -0x1

    .line 251
    :cond_0
    return v0
.end method

.method private increment(I)I
    .locals 2

    .prologue
    .line 233
    add-int/lit8 v0, p1, 0x1

    .line 234
    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->maxElements:I

    if-lt v0, v1, :cond_0

    .line 235
    const/4 v0, 0x0

    .line 237
    :cond_0
    return v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 165
    if-nez p1, :cond_0

    .line 166
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Attempted to add null object to buffer"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    iget-boolean v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_full:Z

    if-eqz v0, :cond_1

    .line 170
    new-instance v0, Lorg/apache/commons/collections/BufferOverflowException;

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "The buffer cannot hold more than "

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->maxElements:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " objects."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/commons/collections/BufferOverflowException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_elements:[Ljava/lang/Object;

    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    aput-object p1, v0, v1

    .line 175
    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->maxElements:I

    if-lt v0, v1, :cond_2

    .line 176
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    .line 179
    :cond_2
    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    if-ne v0, v1, :cond_3

    .line 180
    iput-boolean v3, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_full:Z

    .line 183
    :cond_3
    return v3
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 150
    iput-boolean v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_full:Z

    .line 151
    iput v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    .line 152
    iput v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    .line 153
    iget-object v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_elements:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 154
    return-void
.end method

.method public get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 193
    invoke-virtual {p0}, Lorg/apache/commons/collections/BoundedFifoBuffer;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    new-instance v0, Lorg/apache/commons/collections/BufferUnderflowException;

    const-string v1, "The buffer is already empty"

    invoke-direct {v0, v1}, Lorg/apache/commons/collections/BufferUnderflowException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_elements:[Ljava/lang/Object;

    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0}, Lorg/apache/commons/collections/BoundedFifoBuffer;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFull()Z
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p0}, Lorg/apache/commons/collections/BoundedFifoBuffer;->size()I

    move-result v0

    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->maxElements:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 260
    new-instance v0, Lorg/apache/commons/collections/BoundedFifoBuffer$1;

    invoke-direct {v0, p0}, Lorg/apache/commons/collections/BoundedFifoBuffer$1;-><init>(Lorg/apache/commons/collections/BoundedFifoBuffer;)V

    return-object v0
.end method

.method public maxSize()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->maxElements:I

    return v0
.end method

.method public remove()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 207
    invoke-virtual {p0}, Lorg/apache/commons/collections/BoundedFifoBuffer;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    new-instance v0, Lorg/apache/commons/collections/BufferUnderflowException;

    const-string v1, "The buffer is already empty"

    invoke-direct {v0, v1}, Lorg/apache/commons/collections/BufferUnderflowException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_elements:[Ljava/lang/Object;

    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    aget-object v0, v0, v1

    .line 213
    if-eqz v0, :cond_2

    .line 214
    iget-object v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_elements:[Ljava/lang/Object;

    iget v2, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    const/4 v3, 0x0

    aput-object v3, v1, v2

    .line 216
    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    iget v2, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->maxElements:I

    if-lt v1, v2, :cond_1

    .line 217
    iput v4, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    .line 220
    :cond_1
    iput-boolean v4, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_full:Z

    .line 223
    :cond_2
    return-object v0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 106
    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    if-ge v0, v1, :cond_0

    .line 109
    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->maxElements:I

    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    sub-int/2addr v0, v1

    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    add-int/2addr v0, v1

    .line 116
    :goto_0
    return v0

    .line 110
    :cond_0
    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    if-ne v0, v1, :cond_2

    .line 111
    iget-boolean v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_full:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->maxElements:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 113
    :cond_2
    iget v0, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_end:I

    iget v1, p0, Lorg/apache/commons/collections/BoundedFifoBuffer;->m_start:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method
