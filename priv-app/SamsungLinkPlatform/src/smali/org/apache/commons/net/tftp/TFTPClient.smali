.class public Lorg/apache/commons/net/tftp/TFTPClient;
.super Lorg/apache/commons/net/tftp/TFTP;
.source "SourceFile"


# static fields
.field public static final DEFAULT_MAX_TIMEOUTS:I = 0x5


# instance fields
.field private __maxTimeouts:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/apache/commons/net/tftp/TFTP;-><init>()V

    .line 75
    const/4 v0, 0x5

    iput v0, p0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    .line 76
    return-void
.end method


# virtual methods
.method public getMaxTimeouts()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    return v0
.end method

.method public receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/lang/String;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 346
    invoke-static {p4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    const/16 v5, 0x45

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/net/InetAddress;I)I

    move-result v0

    return v0
.end method

.method public receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/lang/String;I)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    invoke-static {p4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/net/InetAddress;I)I

    move-result v0

    return v0
.end method

.method public receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/net/InetAddress;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 328
    const/16 v5, 0x45

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/net/InetAddress;I)I

    move-result v0

    return v0
.end method

.method public receiveFile(Ljava/lang/String;ILjava/io/OutputStream;Ljava/net/InetAddress;I)I
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    const/4 v5, 0x0

    .line 131
    new-instance v12, Lorg/apache/commons/net/tftp/TFTPAckPacket;

    const/4 v4, 0x0

    move-object/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {v12, v0, v1, v4}, Lorg/apache/commons/net/tftp/TFTPAckPacket;-><init>(Ljava/net/InetAddress;II)V

    .line 133
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->beginBufferedOps()V

    .line 135
    const/4 v6, 0x0

    .line 136
    const/4 v7, 0x1

    .line 138
    if-nez p2, :cond_0

    .line 139
    new-instance v4, Lorg/apache/commons/net/io/FromNetASCIIOutputStream;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Lorg/apache/commons/net/io/FromNetASCIIOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 p3, v4

    .line 141
    :cond_0
    new-instance v4, Lorg/apache/commons/net/tftp/TFTPReadRequestPacket;

    move-object/from16 v0, p4

    move/from16 v1, p5

    move-object/from16 v2, p1

    move/from16 v3, p2

    invoke-direct {v4, v0, v1, v2, v3}, Lorg/apache/commons/net/tftp/TFTPReadRequestPacket;-><init>(Ljava/net/InetAddress;ILjava/lang/String;I)V

    move v8, v6

    move v9, v6

    move-object v10, v4

    move-object v4, v5

    move v5, v6

    .line 147
    :cond_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    .line 152
    :goto_0
    const/4 v11, 0x0

    .line 153
    :cond_2
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    if-ge v11, v13, :cond_3

    .line 157
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedReceive()Lorg/apache/commons/net/tftp/TFTPPacket;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/commons/net/tftp/TFTPPacketException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v4

    .line 187
    :cond_3
    if-nez v8, :cond_4

    .line 189
    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v6

    .line 190
    invoke-virtual {v12, v6}, Lorg/apache/commons/net/tftp/TFTPAckPacket;->setPort(I)V

    .line 191
    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v11

    move-object/from16 v0, p4

    invoke-virtual {v0, v11}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 193
    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object p4

    .line 194
    move-object/from16 v0, p4

    invoke-virtual {v12, v0}, Lorg/apache/commons/net/tftp/TFTPAckPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 195
    move-object/from16 v0, p4

    invoke-virtual {v10, v0}, Lorg/apache/commons/net/tftp/TFTPPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 201
    :cond_4
    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v11

    move-object/from16 v0, p4

    invoke-virtual {v0, v11}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v11

    if-ne v11, v6, :cond_8

    .line 205
    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPPacket;->getType()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 255
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 256
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Received unexpected packet type."

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 162
    :catch_0
    move-exception v13

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    if-lt v11, v13, :cond_2

    .line 164
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 165
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Connection timed out."

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 171
    :catch_1
    move-exception v13

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    if-lt v11, v13, :cond_2

    .line 173
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 174
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Connection timed out."

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 178
    :catch_2
    move-exception v4

    .line 180
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 181
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Bad packet: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPPacketException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 208
    :pswitch_1
    check-cast v4, Lorg/apache/commons/net/tftp/TFTPErrorPacket;

    .line 209
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 210
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error code "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;->getError()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " received: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :pswitch_2
    move-object v5, v4

    .line 213
    check-cast v5, Lorg/apache/commons/net/tftp/TFTPDataPacket;

    .line 214
    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->getDataLength()I

    move-result v8

    .line 216
    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->getBlockNumber()I

    move-result v11

    .line 218
    if-ne v11, v7, :cond_6

    .line 222
    :try_start_1
    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->getData()[B

    move-result-object v10

    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->getDataOffset()I

    move-result v5

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v5, v8}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 234
    add-int/lit8 v5, v7, 0x1

    .line 235
    const v7, 0xffff

    if-le v5, v7, :cond_5

    .line 238
    const/4 v5, 0x0

    .line 274
    :cond_5
    invoke-virtual {v12, v11}, Lorg/apache/commons/net/tftp/TFTPAckPacket;->setBlockNumber(I)V

    .line 276
    add-int/2addr v9, v8

    move v7, v5

    move-object v10, v12

    move v5, v8

    move v8, v11

    .line 279
    :goto_1
    const/16 v11, 0x200

    if-eq v5, v11, :cond_1

    .line 281
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    .line 282
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 284
    return v9

    .line 225
    :catch_3
    move-exception v4

    .line 227
    new-instance v5, Lorg/apache/commons/net/tftp/TFTPErrorPacket;

    const/4 v7, 0x3

    const-string v8, "File write failed."

    move-object/from16 v0, p4

    invoke-direct {v5, v0, v6, v7, v8}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;-><init>(Ljava/net/InetAddress;IILjava/lang/String;)V

    .line 230
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    .line 231
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 232
    throw v4

    .line 245
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->discardPackets()V

    .line 247
    if-nez v7, :cond_7

    const v5, 0xffff

    :goto_2
    if-ne v11, v5, :cond_9

    move v5, v8

    move v8, v11

    .line 248
    goto :goto_1

    .line 247
    :cond_7
    add-int/lit8 v5, v7, -0x1

    goto :goto_2

    .line 261
    :cond_8
    new-instance v11, Lorg/apache/commons/net/tftp/TFTPErrorPacket;

    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v13

    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v14

    const/4 v15, 0x5

    const-string v16, "Unexpected host or port."

    move-object/from16 v0, v16

    invoke-direct {v11, v13, v14, v15, v0}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;-><init>(Ljava/net/InetAddress;IILjava/lang/String;)V

    .line 265
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    goto :goto_1

    :cond_9
    move v5, v8

    move v8, v11

    goto/16 :goto_0

    .line 205
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 606
    invoke-static {p4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    const/16 v5, 0x45

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/net/InetAddress;I)V

    .line 608
    return-void
.end method

.method public sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/lang/String;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 569
    invoke-static {p4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/net/InetAddress;I)V

    .line 570
    return-void
.end method

.method public sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/net/InetAddress;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 588
    const/16 v5, 0x45

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/commons/net/tftp/TFTPClient;->sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/net/InetAddress;I)V

    .line 589
    return-void
.end method

.method public sendFile(Ljava/lang/String;ILjava/io/InputStream;Ljava/net/InetAddress;I)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 371
    const/4 v11, 0x0

    .line 373
    new-instance v4, Lorg/apache/commons/net/tftp/TFTPDataPacket;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/commons/net/tftp/TFTPClient;->_sendBuffer:[B

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object/from16 v5, p4

    move/from16 v6, p5

    invoke-direct/range {v4 .. v10}, Lorg/apache/commons/net/tftp/TFTPDataPacket;-><init>(Ljava/net/InetAddress;II[BII)V

    .line 377
    const/4 v7, 0x1

    .line 379
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->beginBufferedOps()V

    .line 381
    const/4 v8, 0x0

    .line 382
    const/4 v9, 0x0

    .line 383
    const/4 v6, 0x0

    .line 385
    if-nez p2, :cond_0

    .line 386
    new-instance v5, Lorg/apache/commons/net/io/ToNetASCIIInputStream;

    move-object/from16 v0, p3

    invoke-direct {v5, v0}, Lorg/apache/commons/net/io/ToNetASCIIInputStream;-><init>(Ljava/io/InputStream;)V

    move-object/from16 p3, v5

    .line 388
    :cond_0
    new-instance v5, Lorg/apache/commons/net/tftp/TFTPWriteRequestPacket;

    move-object/from16 v0, p4

    move/from16 v1, p5

    move-object/from16 v2, p1

    move/from16 v3, p2

    invoke-direct {v5, v0, v1, v2, v3}, Lorg/apache/commons/net/tftp/TFTPWriteRequestPacket;-><init>(Ljava/net/InetAddress;ILjava/lang/String;I)V

    move-object v12, v5

    move v5, v7

    move v7, v6

    move-object v6, v11

    move v11, v9

    move v9, v8

    .line 396
    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    move v10, v8

    move v8, v5

    .line 404
    :goto_1
    const/4 v5, 0x0

    .line 405
    :cond_1
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    if-ge v5, v13, :cond_d

    .line 409
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedReceive()Lorg/apache/commons/net/tftp/TFTPPacket;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/commons/net/tftp/TFTPPacketException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v5

    .line 439
    :goto_2
    if-eqz v8, :cond_2

    .line 441
    const/4 v8, 0x0

    .line 442
    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v10

    .line 443
    invoke-virtual {v4, v10}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->setPort(I)V

    .line 444
    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 446
    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object p4

    .line 447
    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 448
    move-object/from16 v0, p4

    invoke-virtual {v12, v0}, Lorg/apache/commons/net/tftp/TFTPPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 454
    :cond_2
    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v6

    if-ne v6, v10, :cond_7

    .line 458
    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPPacket;->getType()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 498
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 499
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Received unexpected packet type."

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 414
    :catch_0
    move-exception v13

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    if-lt v5, v13, :cond_1

    .line 416
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 417
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Connection timed out."

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 423
    :catch_1
    move-exception v13

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    if-lt v5, v13, :cond_1

    .line 425
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 426
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Connection timed out."

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 430
    :catch_2
    move-exception v4

    .line 432
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 433
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Bad packet: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPPacketException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :pswitch_0
    move-object v4, v5

    .line 461
    check-cast v4, Lorg/apache/commons/net/tftp/TFTPErrorPacket;

    .line 462
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 463
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error code "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;->getError()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " received: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :pswitch_1
    move-object v6, v5

    .line 466
    check-cast v6, Lorg/apache/commons/net/tftp/TFTPAckPacket;

    .line 468
    invoke-virtual {v6}, Lorg/apache/commons/net/tftp/TFTPAckPacket;->getBlockNumber()I

    move-result v13

    .line 470
    if-ne v13, v11, :cond_4

    .line 472
    add-int/lit8 v6, v11, 0x1

    .line 473
    const v9, 0xffff

    if-le v6, v9, :cond_c

    .line 476
    const/4 v6, 0x0

    move v12, v6

    .line 478
    :goto_3
    if-eqz v7, :cond_8

    .line 544
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->endBufferedOps()V

    .line 545
    return-void

    .line 488
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/commons/net/tftp/TFTPClient;->discardPackets()V

    .line 490
    if-nez v11, :cond_6

    const v6, 0xffff

    :goto_4
    if-ne v13, v6, :cond_b

    move v6, v7

    move v7, v9

    move v9, v11

    move-object v11, v12

    .line 539
    :goto_5
    if-gtz v7, :cond_5

    if-eqz v6, :cond_3

    :cond_5
    move-object v12, v11

    move v11, v9

    move v9, v7

    move v7, v6

    move-object v6, v5

    move v5, v8

    move v8, v10

    goto/16 :goto_0

    .line 490
    :cond_6
    add-int/lit8 v6, v11, -0x1

    goto :goto_4

    .line 504
    :cond_7
    new-instance v6, Lorg/apache/commons/net/tftp/TFTPErrorPacket;

    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v13

    invoke-virtual {v5}, Lorg/apache/commons/net/tftp/TFTPPacket;->getPort()I

    move-result v14

    const/4 v15, 0x5

    const-string v16, "Unexpected host or port."

    move-object/from16 v0, v16

    invoke-direct {v6, v13, v14, v15, v0}, Lorg/apache/commons/net/tftp/TFTPErrorPacket;-><init>(Ljava/net/InetAddress;IILjava/lang/String;)V

    .line 508
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lorg/apache/commons/net/tftp/TFTPClient;->bufferedSend(Lorg/apache/commons/net/tftp/TFTPPacket;)V

    move v6, v7

    move v7, v9

    move v9, v11

    move-object v11, v12

    .line 509
    goto :goto_5

    .line 520
    :cond_8
    const/16 v9, 0x200

    .line 521
    const/4 v11, 0x4

    .line 522
    const/4 v6, 0x0

    .line 524
    :goto_6
    if-lez v9, :cond_9

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/commons/net/tftp/TFTPClient;->_sendBuffer:[B

    move-object/from16 v0, p3

    invoke-virtual {v0, v13, v11, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v13

    if-lez v13, :cond_9

    .line 526
    add-int/2addr v11, v13

    .line 527
    sub-int/2addr v9, v13

    .line 528
    add-int/2addr v6, v13

    goto :goto_6

    .line 531
    :cond_9
    const/16 v9, 0x200

    if-ge v6, v9, :cond_a

    .line 533
    const/4 v7, 0x1

    .line 535
    :cond_a
    invoke-virtual {v4, v12}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->setBlockNumber(I)V

    .line 536
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/commons/net/tftp/TFTPClient;->_sendBuffer:[B

    const/4 v11, 0x4

    invoke-virtual {v4, v9, v11, v6}, Lorg/apache/commons/net/tftp/TFTPDataPacket;->setData([BII)V

    move v9, v12

    move-object v11, v4

    move/from16 v17, v7

    move v7, v6

    move/from16 v6, v17

    .line 537
    goto :goto_5

    :cond_b
    move-object v6, v5

    goto/16 :goto_1

    :cond_c
    move v12, v6

    goto :goto_3

    :cond_d
    move-object v5, v6

    goto/16 :goto_2

    .line 458
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setMaxTimeouts(I)V
    .locals 1

    .prologue
    .line 90
    if-gtz p1, :cond_0

    .line 91
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    .line 94
    :goto_0
    return-void

    .line 93
    :cond_0
    iput p1, p0, Lorg/apache/commons/net/tftp/TFTPClient;->__maxTimeouts:I

    goto :goto_0
.end method
