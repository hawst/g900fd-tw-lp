.class public Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;
.super Lorg/apache/commons/net/telnet/TelnetOptionHandler;
.source "SourceFile"


# static fields
.field protected static final WINDOW_SIZE:I = 0x1f


# instance fields
.field private m_nHeight:I

.field private m_nWidth:I


# direct methods
.method public constructor <init>(II)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 88
    const/16 v1, 0x1f

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lorg/apache/commons/net/telnet/TelnetOptionHandler;-><init>(IZZZZ)V

    .line 32
    const/16 v0, 0x50

    iput v0, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nWidth:I

    .line 37
    const/16 v0, 0x18

    iput v0, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nHeight:I

    .line 96
    iput p1, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nWidth:I

    .line 97
    iput p2, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nHeight:I

    .line 98
    return-void
.end method

.method public constructor <init>(IIZZZZ)V
    .locals 6

    .prologue
    .line 65
    const/16 v1, 0x1f

    move-object v0, p0

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lorg/apache/commons/net/telnet/TelnetOptionHandler;-><init>(IZZZZ)V

    .line 32
    const/16 v0, 0x50

    iput v0, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nWidth:I

    .line 37
    const/16 v0, 0x18

    iput v0, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nHeight:I

    .line 73
    iput p1, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nWidth:I

    .line 74
    iput p2, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nHeight:I

    .line 75
    return-void
.end method


# virtual methods
.method public answerSubnegotiation([II)[I
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return-object v0
.end method

.method public startSubnegotiationLocal()[I
    .locals 8

    .prologue
    const/16 v6, 0xff

    .line 123
    iget v0, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nWidth:I

    const/high16 v1, 0x10000

    mul-int/2addr v0, v1

    iget v1, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nHeight:I

    add-int v4, v0, v1

    .line 124
    const/4 v0, 0x5

    .line 129
    iget v1, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nWidth:I

    rem-int/lit16 v1, v1, 0x100

    if-ne v1, v6, :cond_0

    .line 130
    const/4 v0, 0x6

    .line 133
    :cond_0
    iget v1, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nWidth:I

    div-int/lit16 v1, v1, 0x100

    if-ne v1, v6, :cond_1

    .line 134
    add-int/lit8 v0, v0, 0x1

    .line 137
    :cond_1
    iget v1, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nHeight:I

    rem-int/lit16 v1, v1, 0x100

    if-ne v1, v6, :cond_2

    .line 138
    add-int/lit8 v0, v0, 0x1

    .line 141
    :cond_2
    iget v1, p0, Lorg/apache/commons/net/telnet/WindowSizeOptionHandler;->m_nHeight:I

    div-int/lit16 v1, v1, 0x100

    if-ne v1, v6, :cond_3

    .line 142
    add-int/lit8 v0, v0, 0x1

    .line 148
    :cond_3
    new-array v5, v0, [I

    .line 158
    const/4 v1, 0x0

    const/16 v2, 0x1f

    aput v2, v5, v1

    .line 161
    const/4 v2, 0x1

    const/16 v1, 0x18

    move v7, v1

    move v1, v2

    move v2, v7

    .line 162
    :goto_0
    if-ge v1, v0, :cond_5

    .line 165
    shl-int v3, v6, v2

    .line 167
    and-int/2addr v3, v4

    ushr-int/2addr v3, v2

    aput v3, v5, v1

    .line 169
    aget v3, v5, v1

    if-ne v3, v6, :cond_4

    .line 170
    add-int/lit8 v1, v1, 0x1

    .line 171
    aput v6, v5, v1

    .line 163
    :cond_4
    add-int/lit8 v3, v1, 0x1

    add-int/lit8 v1, v2, -0x8

    move v2, v1

    move v1, v3

    goto :goto_0

    .line 175
    :cond_5
    return-object v5
.end method

.method public startSubnegotiationRemote()[I
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    return-object v0
.end method
