.class public Lcom/android/calendar/secfeature/holidays/JPNCalendarHoliday;
.super Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
.source "JPNCalendarHoliday.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v2, 0xa

    .line 28
    invoke-direct {p0, p1}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;-><init>(Landroid/content/Context;)V

    .line 30
    iput v2, p0, Lcom/android/calendar/secfeature/holidays/JPNCalendarHoliday;->mHolidayCount:I

    .line 31
    new-array v2, v2, [Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    iput-object v2, p0, Lcom/android/calendar/secfeature/holidays/JPNCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    .line 33
    iget-object v2, p0, Lcom/android/calendar/secfeature/holidays/JPNCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 34
    .local v1, "r":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/android/calendar/secfeature/holidays/JPNCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "packageName":Ljava/lang/String;
    return-void
.end method


# virtual methods
.method public getHolidayCalendarName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 39
    const-string v1, ""

    .line 40
    .local v1, "calendarName":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/JPNCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 41
    .local v3, "r":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/JPNCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 43
    .local v2, "packageName":Ljava/lang/String;
    const-string v4, "jp_calendar_name"

    const-string v5, "string"

    invoke-virtual {v3, v4, v5, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 44
    .local v0, "calNameID":I
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/JPNCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 46
    return-object v1
.end method
