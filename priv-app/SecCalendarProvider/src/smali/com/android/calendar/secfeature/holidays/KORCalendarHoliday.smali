.class public Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;
.super Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
.source "KORCalendarHoliday.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 27
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct/range {p0 .. p1}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;-><init>(Landroid/content/Context;)V

    .line 30
    const/16 v20, 0xb

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolidayCount:I

    .line 31
    const/16 v20, 0xb

    move/from16 v0, v20

    new-array v0, v0, [Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    .line 33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 34
    .local v19, "r":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v18

    .line 36
    .local v18, "packageName":Ljava/lang/String;
    const-string v20, "holiday_1_newyear"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v17

    .line 37
    .local v17, "newYearID":I
    const-string v20, "holiday_3_31"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 38
    .local v7, "hol3ID":I
    const-string v20, "holiday_5_children"

    const-string v21, "string"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v19 .. v22}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 39
    .local v8, "hol5ID":I
    const-string v20, "holiday_6_memorial"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 40
    .local v9, "hol6ID":I
    const-string v20, "holiday_8_independence"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 41
    .local v10, "hol8ID":I
    const-string v20, "holiday_10_foundation"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 42
    .local v4, "hol10ID":I
    const-string v20, "holiday_12_xmas"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 43
    .local v6, "hol12ID":I
    const-string v20, "holiday_1_newyear_lunar"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    .line 44
    .local v16, "lunNewYearID":I
    const-string v20, "holiday_4_buddha_lunar"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 45
    .local v12, "lunBuddhaID":I
    const-string v20, "holiday_8_harvest_lunar"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v14

    .line 46
    .local v14, "lunChusokID":I
    const-string v20, "holiday_1_newyear_lunar_string"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v15

    .line 47
    .local v15, "lunNewYearComID":I
    const-string v20, "holiday_4_buddha_lunar_string"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    .line 48
    .local v11, "lunBuddhaComID":I
    const-string v20, "holiday_8_harvest_lunar_string"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    .line 49
    .local v13, "lunChusokComID":I
    const-string v20, "holiday_10_9_hanguel"

    const-string v21, "string"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 51
    .local v5, "hol10_9ID":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    new-instance v22, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    const-string v25, "1902-01-01"

    const/16 v26, 0x0

    invoke-direct/range {v22 .. v26}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v22, v20, v21

    .line 52
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    new-instance v22, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    const-string v25, "1950-03-01"

    const/16 v26, 0x0

    invoke-direct/range {v22 .. v26}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v22, v20, v21

    .line 53
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x2

    new-instance v22, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    const-string v25, "1946-05-05"

    const/16 v26, 0x0

    invoke-direct/range {v22 .. v26}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v22, v20, v21

    .line 54
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x3

    new-instance v22, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    const-string v25, "1971-06-06"

    const/16 v26, 0x0

    invoke-direct/range {v22 .. v26}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v22, v20, v21

    .line 55
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    new-instance v22, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    const-string v25, "1950-08-15"

    const/16 v26, 0x0

    invoke-direct/range {v22 .. v26}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v22, v20, v21

    .line 56
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x5

    new-instance v22, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    const-string v25, "1949-10-03"

    const/16 v26, 0x0

    invoke-direct/range {v22 .. v26}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v22, v20, v21

    .line 57
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x6

    new-instance v22, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    const-string v25, "1902-12-25"

    const/16 v26, 0x0

    invoke-direct/range {v22 .. v26}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v22, v20, v21

    .line 58
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x7

    new-instance v22, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    const-string v25, "1902-02-08"

    const/16 v26, 0x1

    invoke-direct/range {v22 .. v26}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v22, v20, v21

    .line 59
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x8

    new-instance v22, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    const-string v25, "1902-05-15"

    const/16 v26, 0x1

    invoke-direct/range {v22 .. v26}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v22, v20, v21

    .line 60
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x9

    new-instance v22, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    const-string v25, "1902-09-16"

    const/16 v26, 0x1

    invoke-direct/range {v22 .. v26}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v22, v20, v21

    .line 61
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v20, v0

    const/16 v21, 0xa

    new-instance v22, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    const-string v25, "2013-10-09"

    const/16 v26, 0x0

    invoke-direct/range {v22 .. v26}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v22, v20, v21

    .line 62
    return-void
.end method


# virtual methods
.method public getHolidayCalendarAccountColor()I
    .locals 3

    .prologue
    .line 76
    const/16 v0, 0xde

    const/16 v1, 0x54

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method

.method public getHolidayCalendarName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 65
    const-string v1, ""

    .line 66
    .local v1, "calendarName":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 67
    .local v3, "r":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 69
    .local v2, "packageName":Ljava/lang/String;
    const-string v4, "holiday_calendar_label"

    const-string v5, "string"

    invoke-virtual {v3, v4, v5, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 70
    .local v0, "calNameID":I
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 72
    return-object v1
.end method
