.class public Lcom/android/calendar/secfeature/holidays/CHNCalendarHoliday;
.super Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
.source "CHNCalendarHoliday.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 36
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct/range {p0 .. p1}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;-><init>(Landroid/content/Context;)V

    .line 30
    const/16 v29, 0x11

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolidayCount:I

    .line 31
    const/16 v29, 0x11

    move/from16 v0, v29

    new-array v0, v0, [Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    .line 33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 34
    .local v24, "r":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v22

    .line 36
    .local v22, "packageName":Ljava/lang/String;
    const-string v29, "chn_holiday_new_years_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v21

    .line 37
    .local v21, "newYearsID":I
    const-string v29, "chn_holiday_valentines_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v26

    .line 38
    .local v26, "valentinesID":I
    const-string v29, "chn_holiday_womens_day"

    const-string v30, "string"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v27

    .line 39
    .local v27, "womensID":I
    const-string v29, "chn_holiday_labor_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    .line 40
    .local v11, "laborID":I
    const-string v29, "chn_holiday_chinese_youth_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v28

    .line 41
    .local v28, "youthID":I
    const-string v29, "chn_holiday_chilrens_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 42
    .local v5, "chilerensID":I
    const-string v29, "chn_holiday_partys_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v23

    .line 43
    .local v23, "partysID":I
    const-string v29, "chn_holiday_armys_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 44
    .local v4, "armysID":I
    const-string v29, "chn_holiday_tearchers_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v25

    .line 45
    .local v25, "tearchersID":I
    const-string v29, "chn_holiday_national_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v20

    .line 46
    .local v20, "nationalID":I
    const-string v29, "chn_holiday_christmas"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 48
    .local v7, "christmasID":I
    const-string v29, "chn_holiday_chinese_new_years_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 49
    .local v6, "chineseNewYearsID":I
    const-string v29, "chn_holiday_lantern_festival"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 50
    .local v12, "lanternID":I
    const-string v29, "chn_holiday_dragon_boat_festival"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 51
    .local v10, "dragonBoatID":I
    const-string v29, "chn_holiday_double_seventh_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 52
    .local v9, "doubleSeventhID":I
    const-string v29, "chn_holiday_mid_autumn_festival"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v19

    .line 53
    .local v19, "midAutumnID":I
    const-string v29, "chn_holiday_double_ninth_day"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 55
    .local v8, "doubleNinthID":I
    const-string v29, "chn_holiday_lunar_1_1"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v14

    .line 56
    .local v14, "lunar11ID":I
    const-string v29, "chn_holiday_lunar_1_15"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    .line 57
    .local v13, "lunar115ID":I
    const-string v29, "chn_holiday_lunar_5_5"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v15

    .line 58
    .local v15, "lunar55ID":I
    const-string v29, "chn_holiday_lunar_7_7"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    .line 59
    .local v16, "lunar77ID":I
    const-string v29, "chn_holiday_lunar_8_15"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v17

    .line 60
    .local v17, "lunar815ID":I
    const-string v29, "chn_holiday_lunar_9_9"

    const-string v30, "string"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v18

    .line 62
    .local v18, "lunar99ID":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const-string v33, ""

    const-string v34, "1902-01-01"

    const/16 v35, 0x0

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 63
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x1

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const-string v33, ""

    const-string v34, "1902-02-14"

    const/16 v35, 0x0

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 64
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x2

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const-string v33, ""

    const-string v34, "1911-03-08"

    const/16 v35, 0x0

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 65
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x3

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const-string v33, ""

    const-string v34, "1902-05-01"

    const/16 v35, 0x0

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 66
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x4

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const-string v33, ""

    const-string v34, "1902-05-04"

    const/16 v35, 0x0

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 67
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x5

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const-string v33, ""

    const-string v34, "1902-06-01"

    const/16 v35, 0x0

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 68
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x6

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const-string v33, ""

    const-string v34, "1902-07-01"

    const/16 v35, 0x0

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 69
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x7

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const-string v33, ""

    const-string v34, "1902-08-01"

    const/16 v35, 0x0

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x8

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const-string v33, ""

    const-string v34, "1902-09-10"

    const/16 v35, 0x0

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x9

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const-string v33, ""

    const-string v34, "1949-10-01"

    const/16 v35, 0x0

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0xa

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    const-string v33, ""

    const-string v34, "1902-12-25"

    const/16 v35, 0x0

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 74
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0xb

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v33

    const-string v34, "1902-02-08"

    const/16 v35, 0x1

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 75
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0xc

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v33

    const-string v34, "1902-02-22"

    const/16 v35, 0x1

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 76
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0xd

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v33

    const-string v34, "1902-06-10"

    const/16 v35, 0x1

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 77
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0xe

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v24

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v33

    const-string v34, "1902-08-10"

    const/16 v35, 0x1

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 78
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0xf

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v24

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v33

    const-string v34, "1902-09-16"

    const/16 v35, 0x1

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 79
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x10

    new-instance v31, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v24

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v33

    const-string v34, "1902-10-10"

    const/16 v35, 0x1

    invoke-direct/range {v31 .. v35}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v31, v29, v30

    .line 80
    return-void
.end method


# virtual methods
.method public getHolidayCalendarAccountColor()I
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 94
    const/16 v0, 0xff

    invoke-static {v0, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method

.method public getHolidayCalendarName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 83
    const-string v1, ""

    .line 84
    .local v1, "calendarName":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/CHNCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 85
    .local v3, "r":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/CHNCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 87
    .local v2, "packageName":Ljava/lang/String;
    const-string v4, "chn_festival_calendar_label"

    const-string v5, "string"

    invoke-virtual {v3, v4, v5, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 88
    .local v0, "calNameID":I
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/CHNCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 90
    return-object v1
.end method
