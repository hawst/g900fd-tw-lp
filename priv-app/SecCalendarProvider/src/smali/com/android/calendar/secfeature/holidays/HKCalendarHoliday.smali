.class public Lcom/android/calendar/secfeature/holidays/HKCalendarHoliday;
.super Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
.source "HKCalendarHoliday.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 34
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct/range {p0 .. p1}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;-><init>(Landroid/content/Context;)V

    .line 30
    const/16 v27, 0xe

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolidayCount:I

    .line 31
    const/16 v27, 0xe

    move/from16 v0, v27

    new-array v0, v0, [Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    .line 33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    .line 34
    .local v23, "r":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v22

    .line 36
    .local v22, "packageName":Ljava/lang/String;
    const-string v27, "hk_holiday_new_years_day"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v21

    .line 37
    .local v21, "newYearsID":I
    const-string v27, "hk_holiday_valentines_day"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v26

    .line 38
    .local v26, "valentinesID":I
    const-string v27, "hk_holiday_labour_day"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 39
    .local v10, "labourID":I
    const-string v27, "hk_holiday_buddhas_birthday"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 40
    .local v4, "buddhasBirthID":I
    const-string v27, "hk_holiday_establishment_day"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 41
    .local v8, "establishmentID":I
    const-string v27, "hk_holiday_national_day"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v20

    .line 42
    .local v20, "nationalID":I
    const-string v27, "hk_holiday_halloween"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 43
    .local v9, "halloweenID":I
    const-string v27, "hk_holiday_christmas"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 45
    .local v5, "christmasID":I
    const-string v27, "hk_holiday_lunar_new_year"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v18

    .line 46
    .local v18, "lunarNewYearID":I
    const-string v27, "hk_holiday_second_lunar_new_year"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v24

    .line 47
    .local v24, "secondlunarNewYearID":I
    const-string v27, "hk_holiday_third_lunar_new_year"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v25

    .line 48
    .local v25, "thirdlunarNewYearID":I
    const-string v27, "hk_holiday_dragon_boat_festival"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 49
    .local v7, "dragonBoatID":I
    const-string v27, "hk_holiday_mid_autumn_festival"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v19

    .line 50
    .local v19, "midAutumnID":I
    const-string v27, "hk_holiday_double_ninth_festival"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 52
    .local v6, "doubleNinthID":I
    const-string v27, "hk_holiday_lunar_1_1"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    .line 53
    .local v11, "lunar11ID":I
    const-string v27, "hk_holiday_lunar_1_2"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 54
    .local v12, "lunar12ID":I
    const-string v27, "hk_holiday_lunar_1_3"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    .line 55
    .local v13, "lunar13ID":I
    const-string v27, "hk_holiday_lunar_4_8"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v14

    .line 56
    .local v14, "lunar48ID":I
    const-string v27, "hk_holiday_lunar_5_5"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v15

    .line 57
    .local v15, "lunar55ID":I
    const-string v27, "hk_holiday_lunar_8_15"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    .line 58
    .local v16, "lunar815ID":I
    const-string v27, "hk_holiday_lunar_9_9"

    const-string v28, "string"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v17

    .line 60
    .local v17, "lunar99ID":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    const-string v31, ""

    const-string v32, "1902-01-01"

    const/16 v33, 0x0

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 61
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0x1

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    const-string v31, ""

    const-string v32, "1902-02-14"

    const/16 v33, 0x0

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 62
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0x2

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    const-string v31, ""

    const-string v32, "1902-05-01"

    const/16 v33, 0x0

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 63
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0x3

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    const-string v31, ""

    const-string v32, "1902-07-01"

    const/16 v33, 0x0

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 64
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0x4

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    const-string v31, ""

    const-string v32, "1902-10-01"

    const/16 v33, 0x0

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 65
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0x5

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    const-string v31, ""

    const-string v32, "1902-10-31"

    const/16 v33, 0x0

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 66
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0x6

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    const-string v31, ""

    const-string v32, "1902-12-25"

    const/16 v33, 0x0

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 68
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0x7

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    const-string v32, "1902-02-08"

    const/16 v33, 0x1

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 69
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0x8

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    const-string v32, "1902-02-09"

    const/16 v33, 0x1

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0x9

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    const-string v32, "1902-02-10"

    const/16 v33, 0x1

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0xa

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    const-string v32, "1902-05-15"

    const/16 v33, 0x1

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0xb

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    const-string v32, "1902-06-10"

    const/16 v33, 0x1

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 73
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0xc

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v23

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    const-string v32, "1902-09-16"

    const/16 v33, 0x1

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 74
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v27, v0

    const/16 v28, 0xd

    new-instance v29, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v23

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    const-string v32, "1902-10-10"

    const/16 v33, 0x1

    invoke-direct/range {v29 .. v33}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v29, v27, v28

    .line 75
    return-void
.end method


# virtual methods
.method public getHolidayCalendarAccountColor()I
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 89
    const/16 v0, 0xff

    invoke-static {v0, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method

.method public getHolidayCalendarName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 78
    const-string v1, ""

    .line 79
    .local v1, "calendarName":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/HKCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 80
    .local v3, "r":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/HKCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "packageName":Ljava/lang/String;
    const-string v4, "hk_festival_calendar_label"

    const-string v5, "string"

    invoke-virtual {v3, v4, v5, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 83
    .local v0, "calNameID":I
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/HKCalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 85
    return-object v1
.end method
