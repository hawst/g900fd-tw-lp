.class public abstract Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
.super Ljava/lang/Object;
.source "CalendarHoliday.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;
    }
.end annotation


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

.field protected mHolidayCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolidayCount:I

    .line 45
    iput-object p1, p0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mContext:Landroid/content/Context;

    .line 46
    return-void
.end method


# virtual methods
.method public getHolidayCalendarAccountColor()I
    .locals 2

    .prologue
    const/16 v1, 0xff

    .line 58
    const/4 v0, 0x0

    invoke-static {v1, v1, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method

.method public getHolidayCalendarName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, ""

    .line 54
    .local v0, "calendarName":Ljava/lang/String;
    return-object v0
.end method

.method public getHolidayCount()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolidayCount:I

    return v0
.end method

.method public getHolidayEvent(I)Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 62
    const/4 v0, 0x0

    .line 63
    .local v0, "result":Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    aget-object v0, v1, p1

    .line 66
    :cond_0
    return-object v0
.end method
