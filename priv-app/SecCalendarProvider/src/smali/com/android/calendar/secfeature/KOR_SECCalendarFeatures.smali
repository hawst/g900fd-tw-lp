.class public Lcom/android/calendar/secfeature/KOR_SECCalendarFeatures;
.super Lcom/android/calendar/secfeature/SECCalendarFeatures;
.source "KOR_SECCalendarFeatures.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public are24SoloarTermsSupported()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public areNationalHolidaysSupported()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public getCalendarHoliday(Landroid/content/Context;)Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    new-instance v0, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;

    invoke-direct {v0, p1}, Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;-><init>(Landroid/content/Context;)V

    .line 50
    .local v0, "calHol":Lcom/android/calendar/secfeature/holidays/KORCalendarHoliday;
    return-object v0
.end method

.method public getCalendarSubstituteHoliday(Landroid/content/Context;)Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    new-instance v0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;

    invoke-direct {v0, p1}, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;-><init>(Landroid/content/Context;)V

    .line 44
    .local v0, "calAltHol":Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;
    return-object v0
.end method

.method public getSolarLunarConverter()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    new-instance v1, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesKOR;

    invoke-direct {v1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesKOR;-><init>()V

    invoke-direct {v0, v1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;-><init>(Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;)V

    .line 56
    .local v0, "converter":Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    return-object v0
.end method

.method public getSolarLunarTables()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesKOR;

    invoke-direct {v0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesKOR;-><init>()V

    .line 62
    .local v0, "tables":Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;
    return-object v0
.end method

.method public isLunarCalendarSupported()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    return v0
.end method
