.class public Lcom/android/calendar/secfeature/JPN_SECCalendarFeatures;
.super Lcom/android/calendar/secfeature/SECCalendarFeatures;
.source "JPN_SECCalendarFeatures.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;-><init>()V

    .line 31
    return-void
.end method


# virtual methods
.method public are24SoloarTermsSupported()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public areNationalHolidaysSupported()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public getCalendarHoliday(Landroid/content/Context;)Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    new-instance v0, Lcom/android/calendar/secfeature/holidays/JPNCalendarHoliday;

    invoke-direct {v0, p1}, Lcom/android/calendar/secfeature/holidays/JPNCalendarHoliday;-><init>(Landroid/content/Context;)V

    .line 39
    .local v0, "calHol":Lcom/android/calendar/secfeature/holidays/JPNCalendarHoliday;
    return-object v0
.end method

.method public getSolarLunarConverter()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSolarLunarTables()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public isLunarCalendarSupported()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method
