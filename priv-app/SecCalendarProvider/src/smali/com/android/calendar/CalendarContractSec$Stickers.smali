.class public final Lcom/android/calendar/CalendarContractSec$Stickers;
.super Ljava/lang/Object;
.source "CalendarContractSec.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/CalendarContractSec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Stickers"
.end annotation


# static fields
.field public static final STICKER_GROUPS:[Ljava/lang/String;

.field public static final STICKER_GROUPS_UPG:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 110
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Event"

    aput-object v1, v0, v3

    const-string v1, "Highlight"

    aput-object v1, v0, v4

    const-string v1, "Activity"

    aput-object v1, v0, v5

    const-string v1, "Emotion"

    aput-object v1, v0, v6

    const-string v1, "Food"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Leisure"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Nature"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Objects"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "People"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Background"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Anniversary"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Emphasize"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Life"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Person"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/CalendarContractSec$Stickers;->STICKER_GROUPS:[Ljava/lang/String;

    .line 128
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Event"

    aput-object v1, v0, v3

    const-string v1, "Highlight"

    aput-object v1, v0, v4

    const-string v1, "Todo"

    aput-object v1, v0, v5

    const-string v1, "Appointment"

    aput-object v1, v0, v6

    const-string v1, "SportsLeisure"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Common"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Activity"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Emotion"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Food"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Leisure"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Nature"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Objects"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "People"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Background"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/CalendarContractSec$Stickers;->STICKER_GROUPS_UPG:[Ljava/lang/String;

    return-void
.end method
