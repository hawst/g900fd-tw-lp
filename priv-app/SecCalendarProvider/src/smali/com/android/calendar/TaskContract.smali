.class public final Lcom/android/calendar/TaskContract;
.super Ljava/lang/Object;
.source "TaskContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/TaskContract$Tasks;,
        Lcom/android/calendar/TaskContract$TasksReminders;,
        Lcom/android/calendar/TaskContract$TasksAccounts;
    }
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "content://com.android.calendar"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/TaskContract;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method
