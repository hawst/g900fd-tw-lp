.class public Lcom/android/providers/calendar/CalendarDeltaTimeChangedReciever;
.super Landroid/content/BroadcastReceiver;
.source "CalendarDeltaTimeChangedReciever.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 14
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 23
    :cond_0
    :goto_0
    return-void

    .line 17
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v4, "com.sec.android.samsungcloudsync.DELTA_TIME_CHANGED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18
    const-string v1, "TimeDifference"

    const-wide/16 v4, 0x0

    invoke-virtual {p2, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 19
    .local v2, "timeDifference":J
    const-string v1, "CalendarDeltaTimeChangedReciever"

    const-string v4, "Delta Time Has changed. "

    invoke-static {v1, v4}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 20
    invoke-static {}, Lcom/android/providers/calendar/CalendarProvider2;->getInstance()Lcom/android/providers/calendar/CalendarProvider2;

    move-result-object v0

    .line 21
    .local v0, "provider":Lcom/android/providers/calendar/CalendarProvider2;
    invoke-static {v2, v3}, Lcom/android/providers/calendar/CalendarProvider2;->setDeltaTime(J)V

    goto :goto_0
.end method
