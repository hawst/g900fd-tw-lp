.class Lcom/android/providers/calendar/CalendarDatabaseHelper$InsertRunnable;
.super Ljava/lang/Object;
.source "CalendarDatabaseHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/calendar/CalendarDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InsertRunnable"
.end annotation


# instance fields
.field mOps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/providers/calendar/CalendarDatabaseHelper;


# direct methods
.method public constructor <init>(Lcom/android/providers/calendar/CalendarDatabaseHelper;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 6163
    .local p2, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iput-object p1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper$InsertRunnable;->this$0:Lcom/android/providers/calendar/CalendarDatabaseHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6164
    iput-object p2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper$InsertRunnable;->mOps:Ljava/util/ArrayList;

    .line 6165
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 6170
    :try_start_0
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper$InsertRunnable;->this$0:Lcom/android/providers/calendar/CalendarDatabaseHelper;

    # getter for: Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->access$000(Lcom/android/providers/calendar/CalendarDatabaseHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "com.android.calendar"

    iget-object v3, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper$InsertRunnable;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 6176
    :goto_0
    return-void

    .line 6171
    :catch_0
    move-exception v0

    .line 6172
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "CalendarDatabaseHelper"

    const-string v2, "Ignoring unexpected remote exception"

    invoke-static {v1, v2, v0}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 6173
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 6174
    .local v0, "e":Landroid/content/OperationApplicationException;
    const-string v1, "CalendarDatabaseHelper"

    const-string v2, "Ignoring unexpected exception"

    invoke-static {v1, v2, v0}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
