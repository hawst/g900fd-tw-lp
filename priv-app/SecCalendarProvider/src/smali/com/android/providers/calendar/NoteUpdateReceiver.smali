.class public Lcom/android/providers/calendar/NoteUpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NoteUpdateReceiver.java"


# static fields
.field static final NOTES_CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    sget-object v0, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "notes"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/calendar/NoteUpdateReceiver;->NOTES_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 48
    .local v3, "extra":Landroid/os/Bundle;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 50
    .local v2, "cr":Landroid/content/ContentResolver;
    const-string v8, "android.intent.action.SNOTE_UPDATE_LOCK_INFO"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    if-eqz v3, :cond_0

    .line 51
    const-string v8, "SnbFileName"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 52
    .local v5, "notePath":Ljava/lang/String;
    const-string v8, "SnbFileLock"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 55
    .local v4, "isNoteLocked":Ljava/lang/Boolean;
    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    .line 56
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 57
    .local v6, "values":Landroid/content/ContentValues;
    const-string v8, "locked"

    invoke-virtual {v6, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 59
    const-string v7, "filepath =?"

    .line 60
    .local v7, "where":Ljava/lang/String;
    const/4 v8, 0x1

    new-array v1, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v5, v1, v8

    .line 62
    .local v1, "args":[Ljava/lang/String;
    sget-object v8, Lcom/android/providers/calendar/NoteUpdateReceiver;->NOTES_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v8, v6, v7, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 66
    .end local v1    # "args":[Ljava/lang/String;
    .end local v4    # "isNoteLocked":Ljava/lang/Boolean;
    .end local v5    # "notePath":Ljava/lang/String;
    .end local v6    # "values":Landroid/content/ContentValues;
    .end local v7    # "where":Ljava/lang/String;
    :cond_0
    return-void
.end method
