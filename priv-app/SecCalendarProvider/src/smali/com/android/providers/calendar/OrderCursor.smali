.class public Lcom/android/providers/calendar/OrderCursor;
.super Landroid/database/AbstractCursor;
.source "OrderCursor.java"


# instance fields
.field private cols:[Ljava/lang/String;

.field private mCacheValid:Z

.field private mCursor:Landroid/database/Cursor;

.field private mIndent:I

.field private mOrder:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<[J>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 23
    iput v0, p0, Lcom/android/providers/calendar/OrderCursor;->mIndent:I

    .line 26
    iput-boolean v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCacheValid:Z

    .line 28
    if-eqz p1, :cond_0

    .line 29
    iput-object p1, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    .line 30
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/OrderCursor;->newOrderByGroup(Landroid/database/Cursor;)V

    .line 31
    invoke-virtual {p0}, Lcom/android/providers/calendar/OrderCursor;->moveToFirst()Z

    .line 33
    :cond_0
    return-void
.end method

.method private newOrderByGroup(Landroid/database/Cursor;)V
    .locals 24
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 50
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v19

    .line 54
    .local v19, "size":I
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/providers/calendar/OrderCursor;->mCacheValid:Z

    .line 55
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    .line 56
    const/4 v3, 0x1

    move/from16 v0, v19

    if-ge v0, v3, :cond_0

    .line 105
    :goto_0
    return-void

    .line 57
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/util/Vector;->setSize(I)V

    .line 58
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 59
    .local v4, "mId":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Long;>;"
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 60
    .local v5, "mPre":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Long;>;"
    new-instance v6, Ljava/util/Hashtable;

    invoke-direct {v6}, Ljava/util/Hashtable;-><init>()V

    .line 61
    .local v6, "mPar":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;[Ljava/lang/Long;>;"
    const/4 v15, 0x0

    .line 62
    .local v15, "j":I
    const/4 v7, 0x0

    .line 63
    .local v7, "lastPos":I
    const-wide/16 v8, -0x1

    .line 64
    .local v8, "groupid":J
    const/16 v18, 0x0

    .line 65
    .local v18, "rc":I
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 66
    const-string v3, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 67
    .local v12, "idx_id":I
    const-string v3, "groupId"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 68
    .local v11, "idx_group":I
    const-string v3, "previousId"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 69
    .local v14, "idx_previous":I
    const-string v3, "parentId"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 71
    .local v13, "idx_parent":I
    :cond_1
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 72
    .local v10, "id":Ljava/lang/Long;
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 73
    .local v2, "g":Ljava/lang/Long;
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 74
    .local v16, "p":Ljava/lang/Long;
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 75
    .local v17, "pa":Ljava/lang/Long;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    cmp-long v3, v20, v8

    if-eqz v3, :cond_3

    .line 76
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v3

    if-lez v3, :cond_2

    move-object/from16 v3, p0

    .line 77
    invoke-direct/range {v3 .. v9}, Lcom/android/providers/calendar/OrderCursor;->updateOrder(Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Hashtable;IJ)I

    .line 78
    invoke-virtual {v4}, Ljava/util/Vector;->clear()V

    .line 79
    invoke-virtual {v5}, Ljava/util/Vector;->clear()V

    .line 80
    invoke-virtual {v6}, Ljava/util/Hashtable;->clear()V

    .line 82
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 83
    move v7, v15

    .line 85
    :cond_3
    add-int/lit8 v15, v15, 0x1

    .line 86
    invoke-virtual {v4, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 87
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 88
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Long;

    const/16 v20, 0x0

    aput-object v17, v3, v20

    const/16 v20, 0x1

    const-wide/16 v22, 0x0

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    aput-object v21, v3, v20

    invoke-virtual {v6, v10, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 91
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v3

    if-lez v3, :cond_4

    move-object/from16 v3, p0

    .line 92
    invoke-direct/range {v3 .. v9}, Lcom/android/providers/calendar/OrderCursor;->updateOrder(Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Hashtable;IJ)I

    move-result v18

    .line 93
    invoke-virtual {v4}, Ljava/util/Vector;->clear()V

    .line 94
    invoke-virtual {v5}, Ljava/util/Vector;->clear()V

    .line 95
    invoke-virtual {v6}, Ljava/util/Hashtable;->clear()V

    .line 96
    const/4 v3, -0x1

    move/from16 v0, v18

    if-ne v0, v3, :cond_4

    .line 97
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->clear()V

    .line 100
    :cond_4
    const/4 v4, 0x0

    .line 101
    const/4 v5, 0x0

    .line 102
    const/4 v6, 0x0

    .line 105
    goto/16 :goto_0
.end method

.method private updateOrder(Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Hashtable;IJ)I
    .locals 17
    .param p4, "from"    # I
    .param p5, "gid"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "[",
            "Ljava/lang/Long;",
            ">;IJ)I"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "mId":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Long;>;"
    .local p2, "mPre":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Long;>;"
    .local p3, "mPar":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;[Ljava/lang/Long;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    .line 109
    .local v7, "tmp":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Long;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->size()I

    move-result v6

    .line 137
    .local v6, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v6, :cond_1

    .line 138
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 139
    .local v3, "id":Ljava/lang/Long;
    invoke-virtual {v7, v3}, Ljava/util/Vector;->indexOf(Ljava/lang/Object;)I

    move-result v9

    sub-int v5, v9, v2

    .line 140
    .local v5, "off":I
    add-int v4, p4, v2

    .line 141
    .local v4, "idx":I
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/Long;

    .line 142
    .local v8, "val":[Ljava/lang/Long;
    const/4 v9, 0x0

    aget-object v9, v8, v9

    if-eqz v9, :cond_0

    const/4 v9, 0x0

    aget-object v9, v8, v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-eqz v9, :cond_0

    .line 143
    const/4 v10, 0x1

    const/4 v9, 0x0

    aget-object v9, v8, v9

    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Long;

    const/4 v11, 0x1

    aget-object v9, v9, v11

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v10

    .line 145
    :cond_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    const/4 v9, 0x6

    new-array v11, v9, [J

    const/4 v9, 0x0

    add-int v12, v4, v5

    int-to-long v12, v12

    aput-wide v12, v11, v9

    const/4 v9, 0x1

    const/4 v12, 0x1

    aget-object v12, v8, v12

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    aput-wide v12, v11, v9

    const/4 v9, 0x2

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    aput-wide v12, v11, v9

    const/4 v9, 0x3

    const/4 v12, 0x0

    aget-object v12, v8, v12

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    aput-wide v12, v11, v9

    const/4 v9, 0x4

    aput-wide p5, v11, v9

    const/4 v12, 0x5

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    aput-wide v14, v11, v12

    invoke-virtual {v10, v4, v11}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 137
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 148
    .end local v3    # "id":Ljava/lang/Long;
    .end local v4    # "idx":I
    .end local v5    # "off":I
    .end local v8    # "val":[Ljava/lang/Long;
    :cond_1
    invoke-virtual {v7}, Ljava/util/Vector;->clear()V

    .line 149
    const/4 v7, 0x0

    .line 151
    const/4 v9, 0x1

    return v9
.end method


# virtual methods
.method public clone(Z)Ljava/lang/Object;
    .locals 4
    .param p1, "isPart"    # Z

    .prologue
    const/4 v2, 0x0

    .line 352
    if-nez p1, :cond_0

    .line 354
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 368
    :goto_0
    return-object v0

    .line 355
    :catch_0
    move-exception v1

    .line 357
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v1}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    move-object v0, v2

    .line 358
    goto :goto_0

    .line 361
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    :cond_0
    iget-object v3, p0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    if-nez v3, :cond_1

    move-object v0, v2

    goto :goto_0

    .line 362
    :cond_1
    new-instance v0, Lcom/android/providers/calendar/OrderCursor;

    invoke-direct {v0, v2}, Lcom/android/providers/calendar/OrderCursor;-><init>(Landroid/database/Cursor;)V

    .line 363
    .local v0, "cln":Lcom/android/providers/calendar/OrderCursor;
    iget-object v2, p0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    iput-object v2, v0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    .line 365
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/android/providers/calendar/OrderCursor;->mCacheValid:Z

    goto :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 439
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 441
    iput-object v1, p0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->cols:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/android/providers/calendar/OrderCursor;->cols:[Ljava/lang/String;

    .line 444
    :cond_1
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    .line 445
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 446
    iput-object v1, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    .line 448
    :cond_2
    return-void
.end method

.method public copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 1
    .param p1, "columnIndex"    # I
    .param p2, "buffer"    # Landroid/database/CharArrayBuffer;

    .prologue
    .line 385
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    .line 386
    return-void
.end method

.method public deactivate()V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->deactivate()V

    .line 453
    return-void
.end method

.method public getBlob(I)[B
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 389
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 463
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 469
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 474
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 341
    iget-object v2, p0, Lcom/android/providers/calendar/OrderCursor;->cols:[Ljava/lang/String;

    if-nez v2, :cond_1

    .line 342
    iget-object v2, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getColumnCount()I

    move-result v1

    .line 343
    .local v1, "size":I
    add-int/lit8 v2, v1, 0x1

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/android/providers/calendar/OrderCursor;->cols:[Ljava/lang/String;

    .line 344
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 345
    iget-object v2, p0, Lcom/android/providers/calendar/OrderCursor;->cols:[Ljava/lang/String;

    iget-object v3, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v0

    aput-object v3, v2, v0

    .line 344
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 347
    :cond_0
    iget-object v2, p0, Lcom/android/providers/calendar/OrderCursor;->cols:[Ljava/lang/String;

    const-string v3, "indent"

    aput-object v3, v2, v1

    .line 349
    .end local v0    # "i":I
    .end local v1    # "size":I
    :cond_1
    iget-object v2, p0, Lcom/android/providers/calendar/OrderCursor;->cols:[Ljava/lang/String;

    return-object v2
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 393
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getFloat(I)F
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 397
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 401
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLong(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 406
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getOrderSize()I
    .locals 1

    .prologue
    .line 531
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getShort(I)S
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 411
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 416
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 417
    iget v0, p0, Lcom/android/providers/calendar/OrderCursor;->mIndent:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 419
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getWantsAllOnMoveCalls()Z
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getWantsAllOnMoveCalls()Z

    move-result v0

    return v0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 424
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public onMove(II)Z
    .locals 4
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I

    .prologue
    .line 428
    move v0, p2

    .line 429
    .local v0, "p":I
    iget-object v2, p0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge p2, v2, :cond_0

    .line 430
    iget-object v2, p0, Lcom/android/providers/calendar/OrderCursor;->mOrder:Ljava/util/Vector;

    invoke-virtual {v2, p2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [J

    .line 431
    .local v1, "val":[J
    const/4 v2, 0x0

    aget-wide v2, v1, v2

    long-to-int v0, v2

    .line 432
    const/4 v2, 0x1

    aget-wide v2, v1, v2

    long-to-int v2, v2

    iput v2, p0, Lcom/android/providers/calendar/OrderCursor;->mIndent:I

    .line 434
    .end local v1    # "val":[J
    :cond_0
    iget-object v2, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    return v2
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 495
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 496
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 500
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 501
    return-void
.end method

.method public requery()Z
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    move-result v0

    return v0
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 510
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public setCacheInvalid()V
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCacheValid:Z

    .line 374
    invoke-virtual {p0}, Lcom/android/providers/calendar/OrderCursor;->close()V

    .line 375
    return-void
.end method

.method public setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 1
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "notifyUri"    # Landroid/net/Uri;

    .prologue
    .line 515
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 516
    return-void
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 521
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 522
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 526
    iget-object v0, p0, Lcom/android/providers/calendar/OrderCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 527
    return-void
.end method
