.class public Lcom/android/providers/calendar/CalendarInstancesHelper;
.super Ljava/lang/Object;
.source "CalendarInstancesHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;,
        Lcom/android/providers/calendar/CalendarInstancesHelper$EventInstancesMap;
    }
.end annotation


# static fields
.field private static EXPAND_COLUMNS:[Ljava/lang/String;


# instance fields
.field private mCalendarCache:Lcom/android/providers/calendar/CalendarCache;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mDbHelper:Lcom/android/providers/calendar/CalendarDatabaseHelper;

.field private mMetaData:Lcom/android/providers/calendar/MetaData;

.field private mSECCalendarFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 118
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "eventStatus"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "dtstart"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "rdate"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "exrule"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "exdate"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "allDay"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "original_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "originalInstanceTime"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "calendar_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "deleted"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "facebook_schedule_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/calendar/CalendarInstancesHelper;->EXPAND_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/providers/calendar/CalendarDatabaseHelper;Lcom/android/providers/calendar/MetaData;)V
    .locals 4
    .param p1, "calendarDbHelper"    # Lcom/android/providers/calendar/CalendarDatabaseHelper;
    .param p2, "metaData"    # Lcom/android/providers/calendar/MetaData;

    .prologue
    const/4 v3, 0x0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    invoke-static {}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getInstance()Lcom/android/calendar/secfeature/SECCalendarFeatures;

    move-result-object v2

    iput-object v2, p0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mSECCalendarFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    .line 152
    iget-object v2, p0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mSECCalendarFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v2}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    sget-object v2, Lcom/android/providers/calendar/CalendarInstancesHelper;->EXPAND_COLUMNS:[Ljava/lang/String;

    array-length v1, v2

    .line 154
    .local v1, "srcAarrLength":I
    add-int/lit8 v2, v1, 0x1

    new-array v0, v2, [Ljava/lang/String;

    .line 155
    .local v0, "EXPAND_COLUMNS_TEMP":[Ljava/lang/String;
    sget-object v2, Lcom/android/providers/calendar/CalendarInstancesHelper;->EXPAND_COLUMNS:[Ljava/lang/String;

    invoke-static {v2, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 156
    const-string v2, "setLunar"

    aput-object v2, v0, v1

    .line 157
    sput-object v0, Lcom/android/providers/calendar/CalendarInstancesHelper;->EXPAND_COLUMNS:[Ljava/lang/String;

    .line 161
    .end local v0    # "EXPAND_COLUMNS_TEMP":[Ljava/lang/String;
    .end local v1    # "srcAarrLength":I
    :cond_0
    iput-object p1, p0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mDbHelper:Lcom/android/providers/calendar/CalendarDatabaseHelper;

    .line 162
    iget-object v2, p0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mDbHelper:Lcom/android/providers/calendar/CalendarDatabaseHelper;

    invoke-virtual {v2}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iput-object v2, p0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 163
    iput-object p2, p0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mMetaData:Lcom/android/providers/calendar/MetaData;

    .line 164
    new-instance v2, Lcom/android/providers/calendar/CalendarCache;

    iget-object v3, p0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mDbHelper:Lcom/android/providers/calendar/CalendarDatabaseHelper;

    invoke-direct {v2, v3}, Lcom/android/providers/calendar/CalendarCache;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;)V

    iput-object v2, p0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mCalendarCache:Lcom/android/providers/calendar/CalendarCache;

    .line 165
    return-void
.end method

.method static computeTimezoneDependentFields(JJLandroid/text/format/Time;Landroid/content/ContentValues;)V
    .locals 6
    .param p0, "begin"    # J
    .param p2, "end"    # J
    .param p4, "local"    # Landroid/text/format/Time;
    .param p5, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 965
    invoke-virtual {p4, p0, p1}, Landroid/text/format/Time;->set(J)V

    .line 969
    iget-wide v4, p4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {p0, p1, v4, v5}, Lcom/android/providers/calendar/MetaData;->getJulianDay(JJ)I

    move-result v2

    .line 971
    .local v2, "startDay":I
    iget v4, p4, Landroid/text/format/Time;->hour:I

    mul-int/lit8 v4, v4, 0x3c

    iget v5, p4, Landroid/text/format/Time;->minute:I

    add-int v3, v4, v5

    .line 973
    .local v3, "startMinute":I
    invoke-virtual {p4, p2, p3}, Landroid/text/format/Time;->set(J)V

    .line 977
    iget-wide v4, p4, Landroid/text/format/Time;->gmtoff:J

    invoke-static {p2, p3, v4, v5}, Lcom/android/providers/calendar/MetaData;->getJulianDay(JJ)I

    move-result v0

    .line 979
    .local v0, "endDay":I
    iget v4, p4, Landroid/text/format/Time;->hour:I

    mul-int/lit8 v4, v4, 0x3c

    iget v5, p4, Landroid/text/format/Time;->minute:I

    add-int v1, v4, v5

    .line 985
    .local v1, "endMinute":I
    if-nez v1, :cond_0

    if-le v0, v2, :cond_0

    .line 986
    const/16 v1, 0x5a0

    .line 987
    add-int/lit8 v0, v0, -0x1

    .line 990
    :cond_0
    const-string v4, "startDay"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p5, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 991
    const-string v4, "endDay"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p5, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 992
    const-string v4, "startMinute"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p5, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 993
    const-string v4, "endMinute"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p5, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 994
    return-void
.end method

.method private getEntries(JJ)Landroid/database/Cursor;
    .locals 13
    .param p1, "begin"    # J
    .param p3, "end"    # J

    .prologue
    .line 631
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 632
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "view_events"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 633
    sget-object v1, Lcom/android/providers/calendar/CalendarProvider2;->sEventsProjectionMap:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 635
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    .line 636
    .local v8, "beginString":Ljava/lang/String;
    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    .line 654
    .local v11, "endString":Ljava/lang/String;
    const-string v1, "((dtstart <= ? AND (lastDate IS NULL OR lastDate >= ?)) OR (originalInstanceTime IS NOT NULL AND originalInstanceTime <= ? AND originalInstanceTime >= ?)) AND (sync_events != ?) AND (lastSynced = ?)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 655
    const/4 v1, 0x6

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v11, v4, v1

    const/4 v1, 0x1

    aput-object v8, v4, v1

    const/4 v1, 0x2

    aput-object v11, v4, v1

    const/4 v1, 0x3

    const-wide/32 v2, 0x240c8400

    sub-long v2, p1, v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    const/4 v1, 0x4

    const-string v2, "0"

    aput-object v2, v4, v1

    const/4 v1, 0x5

    const-string v2, "0"

    aput-object v2, v4, v1

    .line 663
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 665
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v2, Lcom/android/providers/calendar/CalendarInstancesHelper;->EXPAND_COLUMNS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 667
    const-string v1, "CalInstances"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 668
    const-string v1, "CalInstances"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Instance expansion:  got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " entries"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 673
    :cond_0
    :goto_0
    return-object v9

    .line 670
    :catch_0
    move-exception v10

    .line 671
    .local v10, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method private static getEventValue(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "rowId"    # J
    .param p3, "columnName"    # Ljava/lang/String;

    .prologue
    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SELECT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Events"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "where":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getRelevantRecurrenceEntries(Ljava/lang/String;J)Landroid/database/Cursor;
    .locals 10
    .param p1, "recurrenceSyncId"    # Ljava/lang/String;
    .param p2, "rowId"    # J

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 909
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 911
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "view_events"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 912
    sget-object v1, Lcom/android/providers/calendar/CalendarProvider2;->sEventsProjectionMap:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 914
    if-nez p1, :cond_1

    .line 915
    const-string v8, "_id=?"

    .line 916
    .local v8, "where":Ljava/lang/String;
    invoke-virtual {v0, v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 917
    new-array v4, v5, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    .line 931
    .local v4, "selectionArgs":[Ljava/lang/String;
    :goto_0
    const-string v1, "CalInstances"

    invoke-static {v1, v6}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 932
    const-string v1, "CalInstances"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retrieving events to expand: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    :cond_0
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v2, Lcom/android/providers/calendar/CalendarInstancesHelper;->EXPAND_COLUMNS:[Ljava/lang/String;

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    .line 922
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    .end local v8    # "where":Ljava/lang/String;
    :cond_1
    const-string v8, "(_sync_id=? OR original_sync_id=?) AND lastSynced = ?"

    .line 924
    .restart local v8    # "where":Ljava/lang/String;
    invoke-virtual {v0, v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 925
    const/4 v1, 0x3

    new-array v4, v1, [Ljava/lang/String;

    aput-object p1, v4, v2

    aput-object p1, v4, v5

    const-string v1, "0"

    aput-object v1, v4, v6

    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    goto :goto_0
.end method

.method static getSyncIdKey(Ljava/lang/String;J)Ljava/lang/String;
    .locals 3
    .param p0, "syncId"    # Ljava/lang/String;
    .param p1, "calendarId"    # J

    .prologue
    .line 951
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private updateRecurrenceInstancesLocked(Landroid/content/ContentValues;JLandroid/database/sqlite/SQLiteDatabase;)V
    .locals 18
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "rowId"    # J
    .param p4, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 820
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mMetaData:Lcom/android/providers/calendar/MetaData;

    invoke-virtual {v5}, Lcom/android/providers/calendar/MetaData;->getFieldsLocked()Lcom/android/providers/calendar/MetaData$Fields;

    move-result-object v12

    .line 821
    .local v12, "fields":Lcom/android/providers/calendar/MetaData$Fields;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mCalendarCache:Lcom/android/providers/calendar/CalendarCache;

    invoke-virtual {v5}, Lcom/android/providers/calendar/CalendarCache;->readTimezoneInstances()Ljava/lang/String;

    move-result-object v10

    .line 824
    .local v10, "instancesTimezone":Ljava/lang/String;
    const-string v5, "original_sync_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 825
    .local v14, "originalSyncId":Ljava/lang/String;
    if-nez v14, :cond_0

    .line 826
    const-string v5, "original_sync_id"

    move-object/from16 v0, p4

    move-wide/from16 v1, p2

    invoke-static {v0, v1, v2, v5}, Lcom/android/providers/calendar/CalendarInstancesHelper;->getEventValue(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 830
    :cond_0
    if-eqz v14, :cond_4

    .line 832
    move-object/from16 v16, v14

    .line 846
    .local v16, "recurrenceSyncId":Ljava/lang/String;
    :cond_1
    :goto_0
    if-nez v16, :cond_6

    .line 850
    const-string v5, "original_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 851
    .local v13, "originalId":Ljava/lang/String;
    if-nez v13, :cond_2

    .line 853
    const-string v5, "original_id"

    move-object/from16 v0, p4

    move-wide/from16 v1, p2

    invoke-static {v0, v1, v2, v5}, Lcom/android/providers/calendar/CalendarInstancesHelper;->getEventValue(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 856
    :cond_2
    if-eqz v13, :cond_5

    .line 858
    move-object v15, v13

    .line 866
    .local v15, "recurrenceId":Ljava/lang/String;
    :goto_1
    const-string v17, "_id IN (SELECT Instances._id as _id FROM Instances INNER JOIN Events ON (Events._id=Instances.event_id) WHERE Events._id=? OR Events.original_id=?)"

    .line 867
    .local v17, "where":Ljava/lang/String;
    const-string v5, "Instances"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v15, v6, v7

    const/4 v7, 0x1

    aput-object v15, v6, v7

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v5, v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 885
    .end local v13    # "originalId":Ljava/lang/String;
    .end local v15    # "recurrenceId":Ljava/lang/String;
    .local v4, "delCount":I
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-wide/from16 v2, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/android/providers/calendar/CalendarInstancesHelper;->getRelevantRecurrenceEntries(Ljava/lang/String;J)Landroid/database/Cursor;

    move-result-object v11

    .line 887
    .local v11, "entries":Landroid/database/Cursor;
    :try_start_0
    iget-wide v6, v12, Lcom/android/providers/calendar/MetaData$Fields;->minInstance:J

    iget-wide v8, v12, Lcom/android/providers/calendar/MetaData$Fields;->maxInstance:J

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/android/providers/calendar/CalendarInstancesHelper;->performInstanceExpansion(JJLjava/lang/String;Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 890
    if-eqz v11, :cond_3

    .line 891
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 894
    :cond_3
    return-void

    .line 837
    .end local v4    # "delCount":I
    .end local v11    # "entries":Landroid/database/Cursor;
    .end local v16    # "recurrenceSyncId":Ljava/lang/String;
    .end local v17    # "where":Ljava/lang/String;
    :cond_4
    const-string v5, "_sync_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 838
    .restart local v16    # "recurrenceSyncId":Ljava/lang/String;
    if-nez v16, :cond_1

    .line 840
    const-string v5, "_sync_id"

    move-object/from16 v0, p4

    move-wide/from16 v1, p2

    invoke-static {v0, v1, v2, v5}, Lcom/android/providers/calendar/CalendarInstancesHelper;->getEventValue(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v16

    goto :goto_0

    .line 861
    .restart local v13    # "originalId":Ljava/lang/String;
    :cond_5
    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "recurrenceId":Ljava/lang/String;
    goto :goto_1

    .line 874
    .end local v13    # "originalId":Ljava/lang/String;
    .end local v15    # "recurrenceId":Ljava/lang/String;
    :cond_6
    const-string v17, "_id IN (SELECT Instances._id as _id FROM Instances INNER JOIN Events ON (Events._id=Instances.event_id) WHERE Events._sync_id=? OR Events.original_sync_id=?)"

    .line 875
    .restart local v17    # "where":Ljava/lang/String;
    const-string v5, "Instances"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v16, v6, v7

    const/4 v7, 0x1

    aput-object v16, v6, v7

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v5, v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .restart local v4    # "delCount":I
    goto :goto_2

    .line 890
    .restart local v11    # "entries":Landroid/database/Cursor;
    :catchall_0
    move-exception v5

    if-eqz v11, :cond_7

    .line 891
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v5
.end method


# virtual methods
.method protected expandInstanceRangeLocked(JJLjava/lang/String;)V
    .locals 9
    .param p1, "begin"    # J
    .param p3, "end"    # J
    .param p5, "localTimezone"    # Ljava/lang/String;

    .prologue
    .line 604
    const-string v1, "CalInstances"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 605
    const-string v1, "CalInstances"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expanding events between "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/providers/calendar/CalendarInstancesHelper;->getEntries(JJ)Landroid/database/Cursor;

    move-result-object v7

    .local v7, "entries":Landroid/database/Cursor;
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    .line 610
    :try_start_0
    invoke-virtual/range {v1 .. v7}, Lcom/android/providers/calendar/CalendarInstancesHelper;->performInstanceExpansion(JJLjava/lang/String;Landroid/database/Cursor;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 614
    if-eqz v7, :cond_1

    .line 615
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 621
    :cond_1
    :goto_0
    return-void

    .line 611
    :catch_0
    move-exception v0

    .line 612
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614
    if-eqz v7, :cond_1

    .line 615
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 614
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_2

    .line 615
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1
.end method

.method protected performInstanceExpansion(JJLjava/lang/String;Landroid/database/Cursor;)V
    .locals 83
    .param p1, "begin"    # J
    .param p3, "end"    # J
    .param p5, "localTimezone"    # Ljava/lang/String;
    .param p6, "entries"    # Landroid/database/Cursor;

    .prologue
    .line 193
    new-instance v5, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;

    invoke-direct {v5}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;-><init>()V

    .line 197
    .local v5, "rp":Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;
    const-string v4, "ORIGINAL_EVENT_AND_CALENDAR"

    .line 199
    .local v4, "ORIGINAL_EVENT_AND_CALENDAR":Ljava/lang/String;
    const-string v8, "eventStatus"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v78

    .line 200
    .local v78, "statusColumn":I
    const-string v8, "dtstart"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v36

    .line 201
    .local v36, "dtstartColumn":I
    const-string v8, "dtend"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v35

    .line 202
    .local v35, "dtendColumn":I
    const-string v8, "eventTimezone"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v45

    .line 203
    .local v45, "eventTimezoneColumn":I
    const-string v8, "duration"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v38

    .line 204
    .local v38, "durationColumn":I
    const-string v8, "rrule"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v75

    .line 205
    .local v75, "rruleColumn":I
    const-string v8, "rdate"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v73

    .line 206
    .local v73, "rdateColumn":I
    const-string v8, "exrule"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v48

    .line 207
    .local v48, "exruleColumn":I
    const-string v8, "exdate"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v46

    .line 208
    .local v46, "exdateColumn":I
    const-string v8, "allDay"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 209
    .local v25, "allDayColumn":I
    const-string v8, "_id"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v54

    .line 210
    .local v54, "idColumn":I
    const-string v8, "_sync_id"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v80

    .line 211
    .local v80, "syncIdColumn":I
    const-string v8, "original_sync_id"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v64

    .line 212
    .local v64, "originalEventColumn":I
    const-string v8, "originalInstanceTime"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v66

    .line 213
    .local v66, "originalInstanceTimeColumn":I
    const-string v8, "calendar_id"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 214
    .local v27, "calendarIdColumn":I
    const-string v8, "deleted"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    .line 217
    .local v34, "deletedColumn":I
    const-string v8, "facebook_schedule_id"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v50

    .line 222
    .local v50, "facebookColoumn":I
    const/16 v51, -0x1

    .line 223
    .local v51, "forLunarCalendarColumn":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mSECCalendarFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v8}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 224
    const-string v8, "setLunar"

    move-object/from16 v0, p6

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v51

    .line 226
    :cond_0
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v55

    .line 230
    .local v55, "inLunar":Ljava/lang/Boolean;
    new-instance v57, Lcom/android/providers/calendar/CalendarInstancesHelper$EventInstancesMap;

    invoke-direct/range {v57 .. v57}, Lcom/android/providers/calendar/CalendarInstancesHelper$EventInstancesMap;-><init>()V

    .line 233
    .local v57, "instancesMap":Lcom/android/providers/calendar/CalendarInstancesHelper$EventInstancesMap;
    new-instance v37, Lcom/android/providers/calendar/calendarcommon/Duration;

    invoke-direct/range {v37 .. v37}, Lcom/android/providers/calendar/calendarcommon/Duration;-><init>()V

    .line 234
    .local v37, "duration":Lcom/android/providers/calendar/calendarcommon/Duration;
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 249
    .local v6, "eventTime":Landroid/text/format/Time;
    :cond_1
    :goto_0
    invoke-interface/range {p6 .. p6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_1f

    .line 251
    const/16 v56, 0x0

    .line 253
    .local v56, "initialValues":Landroid/content/ContentValues;
    :try_start_0
    move-object/from16 v0, p6

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_6

    const/16 v24, 0x1

    .line 255
    .local v24, "allDay":Z
    :goto_1
    move-object/from16 v0, p6

    move/from16 v1, v45

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v44

    .line 256
    .local v44, "eventTimezone":Ljava/lang/String;
    if-nez v24, :cond_2

    invoke-static/range {v44 .. v44}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 260
    :cond_2
    const-string v44, "UTC"

    .line 263
    :cond_3
    move-object/from16 v0, p6

    move/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 264
    .local v18, "dtstartMillis":J
    move-object/from16 v0, p6

    move/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v43

    .line 266
    .local v43, "eventId":Ljava/lang/Long;
    move-object/from16 v0, p6

    move/from16 v1, v38

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/util/TimeFormatException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v39

    .line 267
    .local v39, "durationStr":Ljava/lang/String;
    if-eqz v39, :cond_4

    .line 269
    :try_start_1
    move-object/from16 v0, v37

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/calendarcommon/Duration;->parse(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/util/TimeFormatException; {:try_start_1 .. :try_end_1} :catch_3

    .line 286
    :cond_4
    :goto_2
    :try_start_2
    move-object/from16 v0, p6

    move/from16 v1, v80

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v79

    .line 287
    .local v79, "syncId":Ljava/lang/String;
    move-object/from16 v0, p6

    move/from16 v1, v64

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v63

    .line 289
    .local v63, "originalEvent":Ljava/lang/String;
    const-wide/16 v68, -0x1

    .line 290
    .local v68, "originalInstanceTimeMillis":J
    move-object/from16 v0, p6

    move/from16 v1, v66

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_5

    .line 291
    move-object/from16 v0, p6

    move/from16 v1, v66

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v68

    .line 293
    :cond_5
    move-object/from16 v0, p6

    move/from16 v1, v78

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v77

    .line 294
    .local v77, "status":I
    move-object/from16 v0, p6

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_8

    const/16 v33, 0x1

    .line 296
    .local v33, "deleted":Z
    :goto_3
    move-object/from16 v0, p6

    move/from16 v1, v75

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v76

    .line 297
    .local v76, "rruleStr":Ljava/lang/String;
    move-object/from16 v0, p6

    move/from16 v1, v73

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v74

    .line 298
    .local v74, "rdateStr":Ljava/lang/String;
    move-object/from16 v0, p6

    move/from16 v1, v48

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v49

    .line 299
    .local v49, "exruleStr":Ljava/lang/String;
    move-object/from16 v0, p6

    move/from16 v1, v46

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v47

    .line 300
    .local v47, "exdateStr":Ljava/lang/String;
    move-object/from16 v0, p6

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 302
    .local v30, "calendarId":J
    move-object/from16 v0, v79

    move-wide/from16 v1, v30

    invoke-static {v0, v1, v2}, Lcom/android/providers/calendar/CalendarInstancesHelper;->getSyncIdKey(Ljava/lang/String;J)Ljava/lang/String;
    :try_end_2
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/util/TimeFormatException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v81

    .line 304
    .local v81, "syncIdKey":Ljava/lang/String;
    const/4 v7, 0x0

    .line 306
    .local v7, "recur":Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;
    :try_start_3
    new-instance v7, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;

    .end local v7    # "recur":Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;
    move-object/from16 v0, v76

    move-object/from16 v1, v74

    move-object/from16 v2, v49

    move-object/from16 v3, v47

    invoke-direct {v7, v0, v1, v2, v3}, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/android/providers/calendar/calendarcommon/EventRecurrence$InvalidFormatException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/util/TimeFormatException; {:try_start_3 .. :try_end_3} :catch_3

    .line 315
    .restart local v7    # "recur":Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;
    if-eqz v7, :cond_16

    :try_start_4
    invoke-virtual {v7}, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->hasRecurrence()Z

    move-result v8

    if-eqz v8, :cond_16

    .line 318
    const/4 v8, 0x2

    move/from16 v0, v77

    if-ne v0, v8, :cond_9

    .line 320
    const-string v8, "CalendarProvider2"

    const/4 v11, 0x6

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 321
    const-string v8, "CalendarProvider2"

    const-string v11, "Found canceled recurring event in Events table.  Ignoring."

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/util/TimeFormatException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 484
    .end local v7    # "recur":Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;
    .end local v18    # "dtstartMillis":J
    .end local v24    # "allDay":Z
    .end local v30    # "calendarId":J
    .end local v33    # "deleted":Z
    .end local v39    # "durationStr":Ljava/lang/String;
    .end local v43    # "eventId":Ljava/lang/Long;
    .end local v44    # "eventTimezone":Ljava/lang/String;
    .end local v47    # "exdateStr":Ljava/lang/String;
    .end local v49    # "exruleStr":Ljava/lang/String;
    .end local v63    # "originalEvent":Ljava/lang/String;
    .end local v68    # "originalInstanceTimeMillis":J
    .end local v74    # "rdateStr":Ljava/lang/String;
    .end local v76    # "rruleStr":Ljava/lang/String;
    .end local v77    # "status":I
    .end local v79    # "syncId":Ljava/lang/String;
    .end local v81    # "syncIdKey":Ljava/lang/String;
    :catch_0
    move-exception v42

    move-object/from16 v17, v56

    .line 485
    .end local v56    # "initialValues":Landroid/content/ContentValues;
    .local v17, "initialValues":Landroid/content/ContentValues;
    .local v42, "e":Lcom/android/providers/calendar/calendarcommon/DateException;
    :goto_4
    const-string v8, "CalendarProvider2"

    const/4 v11, 0x6

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 486
    const-string v8, "CalendarProvider2"

    const-string v11, "RecurrenceProcessor error "

    move-object/from16 v0, v42

    invoke-static {v8, v11, v0}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 253
    .end local v17    # "initialValues":Landroid/content/ContentValues;
    .end local v42    # "e":Lcom/android/providers/calendar/calendarcommon/DateException;
    .restart local v56    # "initialValues":Landroid/content/ContentValues;
    :cond_6
    const/16 v24, 0x0

    goto/16 :goto_1

    .line 271
    .restart local v18    # "dtstartMillis":J
    .restart local v24    # "allDay":Z
    .restart local v39    # "durationStr":Ljava/lang/String;
    .restart local v43    # "eventId":Ljava/lang/Long;
    .restart local v44    # "eventTimezone":Ljava/lang/String;
    :catch_1
    move-exception v42

    .line 272
    .restart local v42    # "e":Lcom/android/providers/calendar/calendarcommon/DateException;
    :try_start_5
    const-string v8, "CalendarProvider2"

    const/4 v11, 0x6

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 273
    const-string v8, "CalendarProvider2"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "error parsing duration for event "

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v43

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v16, "\'"

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v39

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v16, "\'"

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v42

    invoke-static {v8, v11, v0}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 276
    :cond_7
    const/4 v8, 0x1

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->sign:I

    .line 277
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->weeks:I

    .line 278
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->days:I

    .line 279
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->hours:I

    .line 280
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->minutes:I

    .line 281
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->seconds:I

    .line 282
    const-string v39, "+P0S"

    goto/16 :goto_2

    .line 294
    .end local v42    # "e":Lcom/android/providers/calendar/calendarcommon/DateException;
    .restart local v63    # "originalEvent":Ljava/lang/String;
    .restart local v68    # "originalInstanceTimeMillis":J
    .restart local v77    # "status":I
    .restart local v79    # "syncId":Ljava/lang/String;
    :cond_8
    const/16 v33, 0x0

    goto/16 :goto_3

    .line 307
    .restart local v30    # "calendarId":J
    .restart local v33    # "deleted":Z
    .restart local v47    # "exdateStr":Ljava/lang/String;
    .restart local v49    # "exruleStr":Ljava/lang/String;
    .restart local v74    # "rdateStr":Ljava/lang/String;
    .restart local v76    # "rruleStr":Ljava/lang/String;
    .restart local v81    # "syncIdKey":Ljava/lang/String;
    :catch_2
    move-exception v42

    .line 308
    .local v42, "e":Lcom/android/providers/calendar/calendarcommon/EventRecurrence$InvalidFormatException;
    const-string v8, "CalendarProvider2"

    const/4 v11, 0x6

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 309
    const-string v8, "CalendarProvider2"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Could not parse RRULE recurrence string: "

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v76

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v42

    invoke-static {v8, v11, v0}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/util/TimeFormatException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    .line 488
    .end local v18    # "dtstartMillis":J
    .end local v24    # "allDay":Z
    .end local v30    # "calendarId":J
    .end local v33    # "deleted":Z
    .end local v39    # "durationStr":Ljava/lang/String;
    .end local v42    # "e":Lcom/android/providers/calendar/calendarcommon/EventRecurrence$InvalidFormatException;
    .end local v43    # "eventId":Ljava/lang/Long;
    .end local v44    # "eventTimezone":Ljava/lang/String;
    .end local v47    # "exdateStr":Ljava/lang/String;
    .end local v49    # "exruleStr":Ljava/lang/String;
    .end local v63    # "originalEvent":Ljava/lang/String;
    .end local v68    # "originalInstanceTimeMillis":J
    .end local v74    # "rdateStr":Ljava/lang/String;
    .end local v76    # "rruleStr":Ljava/lang/String;
    .end local v77    # "status":I
    .end local v79    # "syncId":Ljava/lang/String;
    .end local v81    # "syncIdKey":Ljava/lang/String;
    :catch_3
    move-exception v42

    move-object/from16 v17, v56

    .line 489
    .end local v56    # "initialValues":Landroid/content/ContentValues;
    .restart local v17    # "initialValues":Landroid/content/ContentValues;
    .local v42, "e":Landroid/util/TimeFormatException;
    :goto_5
    const-string v8, "CalendarProvider2"

    const/4 v11, 0x6

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 490
    const-string v8, "CalendarProvider2"

    const-string v11, "RecurrenceProcessor error "

    move-object/from16 v0, v42

    invoke-static {v8, v11, v0}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 326
    .end local v17    # "initialValues":Landroid/content/ContentValues;
    .end local v42    # "e":Landroid/util/TimeFormatException;
    .restart local v7    # "recur":Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;
    .restart local v18    # "dtstartMillis":J
    .restart local v24    # "allDay":Z
    .restart local v30    # "calendarId":J
    .restart local v33    # "deleted":Z
    .restart local v39    # "durationStr":Ljava/lang/String;
    .restart local v43    # "eventId":Ljava/lang/Long;
    .restart local v44    # "eventTimezone":Ljava/lang/String;
    .restart local v47    # "exdateStr":Ljava/lang/String;
    .restart local v49    # "exruleStr":Ljava/lang/String;
    .restart local v56    # "initialValues":Landroid/content/ContentValues;
    .restart local v63    # "originalEvent":Ljava/lang/String;
    .restart local v68    # "originalInstanceTimeMillis":J
    .restart local v74    # "rdateStr":Ljava/lang/String;
    .restart local v76    # "rruleStr":Ljava/lang/String;
    .restart local v77    # "status":I
    .restart local v79    # "syncId":Ljava/lang/String;
    .restart local v81    # "syncIdKey":Ljava/lang/String;
    :cond_9
    if-eqz v33, :cond_a

    .line 327
    :try_start_6
    const-string v8, "CalendarProvider2"

    const/4 v11, 0x3

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 328
    const-string v8, "CalendarProvider2"

    const-string v11, "Found deleted recurring event in Events table.  Ignoring."

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 335
    :cond_a
    move-object/from16 v0, v44

    iput-object v0, v6, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 336
    move-wide/from16 v0, v18

    invoke-virtual {v6, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 337
    move/from16 v0, v24

    iput-boolean v0, v6, Landroid/text/format/Time;->allDay:Z

    .line 339
    if-nez v39, :cond_c

    .line 341
    const-string v8, "CalendarProvider2"

    const/4 v11, 0x6

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 342
    const-string v8, "CalendarProvider2"

    const-string v11, "Repeating event has no duration -- should not happen."

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :cond_b
    if-eqz v24, :cond_e

    .line 347
    const/4 v8, 0x1

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->sign:I

    .line 348
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->weeks:I

    .line 349
    const/4 v8, 0x1

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->days:I

    .line 350
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->hours:I

    .line 351
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->minutes:I

    .line 352
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->seconds:I

    .line 353
    const-string v39, "+P1D"

    .line 376
    :cond_c
    :goto_6
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mSECCalendarFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v8}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v8

    if-eqz v8, :cond_11

    if-ltz v51, :cond_11

    .line 377
    move-object/from16 v0, p6

    move/from16 v1, v51

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_10

    const/4 v8, 0x1

    :goto_7
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v55

    .line 382
    :goto_8
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mSECCalendarFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v8}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v8

    if-eqz v8, :cond_13

    .line 383
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v8

    const-string v11, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v8, v11}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v61

    .line 385
    .local v61, "localHolidayDisplay":Ljava/lang/String;
    const-string v8, "HKTW"

    move-object/from16 v0, v61

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_d

    const-string v8, "HONGKONG"

    move-object/from16 v0, v61

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_d

    const-string v8, "TAIWAN"

    move-object/from16 v0, v61

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_12

    :cond_d
    invoke-virtual/range {v55 .. v55}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_12

    move-wide/from16 v8, p1

    move-wide/from16 v10, p3

    .line 388
    invoke-virtual/range {v5 .. v11}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;JJ)[J

    move-result-object v32

    .line 402
    .end local v61    # "localHolidayDisplay":Ljava/lang/String;
    .local v32, "dates":[J
    :goto_9
    if-eqz v24, :cond_14

    .line 403
    const-string v8, "UTC"

    iput-object v8, v6, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 408
    :goto_a
    invoke-virtual/range {v37 .. v37}, Lcom/android/providers/calendar/calendarcommon/Duration;->getMillis()J

    move-result-wide v40

    .line 409
    .local v40, "durationMillis":J
    move-object/from16 v26, v32

    .local v26, "arr$":[J
    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v59, v0

    .local v59, "len$":I
    const/16 v52, 0x0

    .local v52, "i$":I
    :goto_b
    move/from16 v0, v52

    move/from16 v1, v59

    if-ge v0, v1, :cond_15

    aget-wide v12, v26, v52

    .line 410
    .local v12, "date":J
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V
    :try_end_6
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Landroid/util/TimeFormatException; {:try_start_6 .. :try_end_6} :catch_3

    .line 411
    .end local v56    # "initialValues":Landroid/content/ContentValues;
    .restart local v17    # "initialValues":Landroid/content/ContentValues;
    :try_start_7
    const-string v8, "event_id"

    move-object/from16 v0, v17

    move-object/from16 v1, v43

    invoke-virtual {v0, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 413
    const-string v8, "begin"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 414
    add-long v14, v12, v40

    .line 415
    .local v14, "dtendMillis":J
    const-string v8, "end"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v16, v6

    .line 417
    invoke-static/range {v12 .. v17}, Lcom/android/providers/calendar/CalendarInstancesHelper;->computeTimezoneDependentFields(JJLandroid/text/format/Time;Landroid/content/ContentValues;)V

    .line 419
    move-object/from16 v0, v57

    move-object/from16 v1, v81

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/calendar/CalendarInstancesHelper$EventInstancesMap;->add(Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_7
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Landroid/util/TimeFormatException; {:try_start_7 .. :try_end_7} :catch_5

    .line 409
    add-int/lit8 v52, v52, 0x1

    move-object/from16 v56, v17

    .end local v17    # "initialValues":Landroid/content/ContentValues;
    .restart local v56    # "initialValues":Landroid/content/ContentValues;
    goto :goto_b

    .line 357
    .end local v12    # "date":J
    .end local v14    # "dtendMillis":J
    .end local v26    # "arr$":[J
    .end local v32    # "dates":[J
    .end local v40    # "durationMillis":J
    .end local v52    # "i$":I
    .end local v59    # "len$":I
    :cond_e
    const/4 v8, 0x1

    :try_start_8
    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->sign:I

    .line 358
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->weeks:I

    .line 359
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->days:I

    .line 360
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->hours:I

    .line 361
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->minutes:I

    .line 362
    move-object/from16 v0, p6

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_f

    .line 363
    move-object/from16 v0, p6

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 364
    .restart local v14    # "dtendMillis":J
    sub-long v20, v14, v18

    const-wide/16 v22, 0x3e8

    div-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v8, v0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->seconds:I

    .line 365
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "+P"

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v37

    iget v11, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->seconds:I

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, "S"

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    .line 366
    goto/16 :goto_6

    .line 367
    .end local v14    # "dtendMillis":J
    :cond_f
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iput v8, v0, Lcom/android/providers/calendar/calendarcommon/Duration;->seconds:I

    .line 368
    const-string v39, "+P0S"

    goto/16 :goto_6

    .line 377
    :cond_10
    const/4 v8, 0x0

    goto/16 :goto_7

    .line 380
    :cond_11
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v55

    goto/16 :goto_8

    .line 390
    .restart local v61    # "localHolidayDisplay":Ljava/lang/String;
    :cond_12
    new-instance v10, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    invoke-direct {v10, v6}, Lcom/android/providers/calendar/lunarRecurrence/LTime;-><init>(Landroid/text/format/Time;)V

    .line 391
    .local v10, "tempEventTime":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    invoke-virtual/range {v55 .. v55}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iput-boolean v8, v10, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    .line 392
    new-instance v9, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;

    invoke-direct {v9}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;-><init>()V

    .local v9, "rpLunar":Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;
    move-object v11, v7

    move-wide/from16 v12, p1

    move-wide/from16 v14, p3

    .line 393
    invoke-virtual/range {v9 .. v15}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;JJ)[J

    move-result-object v32

    .restart local v32    # "dates":[J
    goto/16 :goto_9

    .end local v9    # "rpLunar":Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;
    .end local v10    # "tempEventTime":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    .end local v32    # "dates":[J
    .end local v61    # "localHolidayDisplay":Ljava/lang/String;
    :cond_13
    move-object v11, v5

    move-object v12, v6

    move-object v13, v7

    move-wide/from16 v14, p1

    move-wide/from16 v16, p3

    .line 396
    invoke-virtual/range {v11 .. v17}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;JJ)[J

    move-result-object v32

    .restart local v32    # "dates":[J
    goto/16 :goto_9

    .line 405
    :cond_14
    move-object/from16 v0, p5

    iput-object v0, v6, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    goto/16 :goto_a

    .restart local v26    # "arr$":[J
    .restart local v40    # "durationMillis":J
    .restart local v52    # "i$":I
    .restart local v59    # "len$":I
    :cond_15
    move-object/from16 v17, v56

    .line 421
    .end local v56    # "initialValues":Landroid/content/ContentValues;
    .restart local v17    # "initialValues":Landroid/content/ContentValues;
    goto/16 :goto_0

    .line 423
    .end local v17    # "initialValues":Landroid/content/ContentValues;
    .end local v26    # "arr$":[J
    .end local v32    # "dates":[J
    .end local v40    # "durationMillis":J
    .end local v52    # "i$":I
    .end local v59    # "len$":I
    .restart local v56    # "initialValues":Landroid/content/ContentValues;
    :cond_16
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V
    :try_end_8
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Landroid/util/TimeFormatException; {:try_start_8 .. :try_end_8} :catch_3

    .line 429
    .end local v56    # "initialValues":Landroid/content/ContentValues;
    .restart local v17    # "initialValues":Landroid/content/ContentValues;
    if-eqz v63, :cond_17

    const-wide/16 v20, -0x1

    cmp-long v8, v68, v20

    if-eqz v8, :cond_17

    .line 433
    :try_start_9
    const-string v8, "ORIGINAL_EVENT_AND_CALENDAR"

    move-object/from16 v0, v63

    move-wide/from16 v1, v30

    invoke-static {v0, v1, v2}, Lcom/android/providers/calendar/CalendarInstancesHelper;->getSyncIdKey(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    const-string v8, "originalInstanceTime"

    invoke-static/range {v68 .. v69}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 437
    const-string v8, "eventStatus"

    invoke-static/range {v77 .. v77}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 440
    :cond_17
    move-wide/from16 v14, v18

    .line 441
    .restart local v14    # "dtendMillis":J
    if-eqz v39, :cond_18

    invoke-static/range {v39 .. v39}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1c

    .line 442
    :cond_18
    move-object/from16 v0, p6

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-nez v8, :cond_19

    .line 443
    move-object/from16 v0, p6

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 454
    :cond_19
    :goto_c
    cmp-long v8, v14, p1

    if-ltz v8, :cond_1a

    cmp-long v8, v18, p3

    if-lez v8, :cond_1b

    .line 455
    :cond_1a
    if-eqz v63, :cond_1d

    const-wide/16 v20, -0x1

    cmp-long v8, v68, v20

    if-eqz v8, :cond_1d

    .line 456
    const-string v8, "eventStatus"

    const/4 v11, 0x2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 466
    :cond_1b
    const-string v8, "event_id"

    move-object/from16 v0, v17

    move-object/from16 v1, v43

    invoke-virtual {v0, v8, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 468
    const-string v8, "begin"

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 469
    const-string v8, "end"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 472
    const-string v8, "deleted"

    invoke-static/range {v33 .. v33}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 474
    if-eqz v24, :cond_1e

    .line 475
    const-string v8, "UTC"

    iput-object v8, v6, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    :goto_d
    move-wide/from16 v20, v14

    move-object/from16 v22, v6

    move-object/from16 v23, v17

    .line 479
    invoke-static/range {v18 .. v23}, Lcom/android/providers/calendar/CalendarInstancesHelper;->computeTimezoneDependentFields(JJLandroid/text/format/Time;Landroid/content/ContentValues;)V

    .line 482
    move-object/from16 v0, v57

    move-object/from16 v1, v81

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/calendar/CalendarInstancesHelper$EventInstancesMap;->add(Ljava/lang/String;Landroid/content/ContentValues;)V

    goto/16 :goto_0

    .line 484
    .end local v14    # "dtendMillis":J
    :catch_4
    move-exception v42

    goto/16 :goto_4

    .line 446
    .restart local v14    # "dtendMillis":J
    :cond_1c
    move-object/from16 v0, v37

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/calendar/calendarcommon/Duration;->addTo(J)J

    move-result-wide v14

    goto :goto_c

    .line 458
    :cond_1d
    const-string v8, "CalendarProvider2"

    const/4 v11, 0x6

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 459
    const-string v8, "CalendarProvider2"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Unexpected event outside window: "

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v79

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 488
    .end local v14    # "dtendMillis":J
    :catch_5
    move-exception v42

    goto/16 :goto_5

    .line 477
    .restart local v14    # "dtendMillis":J
    :cond_1e
    move-object/from16 v0, p5

    iput-object v0, v6, Landroid/text/format/Time;->timezone:Ljava/lang/String;
    :try_end_9
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Landroid/util/TimeFormatException; {:try_start_9 .. :try_end_9} :catch_5

    goto :goto_d

    .line 517
    .end local v7    # "recur":Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;
    .end local v14    # "dtendMillis":J
    .end local v17    # "initialValues":Landroid/content/ContentValues;
    .end local v18    # "dtstartMillis":J
    .end local v24    # "allDay":Z
    .end local v30    # "calendarId":J
    .end local v33    # "deleted":Z
    .end local v39    # "durationStr":Ljava/lang/String;
    .end local v43    # "eventId":Ljava/lang/Long;
    .end local v44    # "eventTimezone":Ljava/lang/String;
    .end local v47    # "exdateStr":Ljava/lang/String;
    .end local v49    # "exruleStr":Ljava/lang/String;
    .end local v63    # "originalEvent":Ljava/lang/String;
    .end local v68    # "originalInstanceTimeMillis":J
    .end local v74    # "rdateStr":Ljava/lang/String;
    .end local v76    # "rruleStr":Ljava/lang/String;
    .end local v77    # "status":I
    .end local v79    # "syncId":Ljava/lang/String;
    .end local v81    # "syncIdKey":Ljava/lang/String;
    :cond_1f
    invoke-virtual/range {v57 .. v57}, Lcom/android/providers/calendar/CalendarInstancesHelper$EventInstancesMap;->keySet()Ljava/util/Set;

    move-result-object v58

    .line 518
    .local v58, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v58 .. v58}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v52

    :cond_20
    invoke-interface/range {v52 .. v52}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_23

    invoke-interface/range {v52 .. v52}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v81

    check-cast v81, Ljava/lang/String;

    .line 519
    .restart local v81    # "syncIdKey":Ljava/lang/String;
    move-object/from16 v0, v57

    move-object/from16 v1, v81

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/CalendarInstancesHelper$EventInstancesMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v60

    check-cast v60, Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;

    .line 520
    .local v60, "list":Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;
    invoke-virtual/range {v60 .. v60}, Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;->iterator()Ljava/util/Iterator;

    move-result-object v53

    .local v53, "i$":Ljava/util/Iterator;
    :cond_21
    invoke-interface/range {v53 .. v53}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_20

    invoke-interface/range {v53 .. v53}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v82

    check-cast v82, Landroid/content/ContentValues;

    .line 524
    .local v82, "values":Landroid/content/ContentValues;
    const-string v8, "ORIGINAL_EVENT_AND_CALENDAR"

    move-object/from16 v0, v82

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_21

    .line 528
    const-string v8, "ORIGINAL_EVENT_AND_CALENDAR"

    move-object/from16 v0, v82

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v65

    .line 529
    .local v65, "originalEventPlusCalendar":Ljava/lang/String;
    const-string v8, "originalInstanceTime"

    move-object/from16 v0, v82

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v70

    .line 530
    .local v70, "originalTime":J
    move-object/from16 v0, v57

    move-object/from16 v1, v65

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/CalendarInstancesHelper$EventInstancesMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v67

    check-cast v67, Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;

    .line 532
    .local v67, "originalList":Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;
    if-eqz v67, :cond_21

    .line 542
    invoke-virtual/range {v67 .. v67}, Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;->size()I

    move-result v8

    add-int/lit8 v62, v8, -0x1

    .local v62, "num":I
    :goto_e
    if-ltz v62, :cond_21

    .line 543
    move-object/from16 v0, v67

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;->get(I)Ljava/lang/Object;

    move-result-object v72

    check-cast v72, Landroid/content/ContentValues;

    .line 544
    .local v72, "originalValues":Landroid/content/ContentValues;
    const-string v8, "begin"

    move-object/from16 v0, v72

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    .line 545
    .local v28, "beginTime":J
    cmp-long v8, v28, v70

    if-nez v8, :cond_22

    .line 547
    move-object/from16 v0, v67

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;->remove(I)Ljava/lang/Object;

    .line 542
    :cond_22
    add-int/lit8 v62, v62, -0x1

    goto :goto_e

    .line 569
    .end local v28    # "beginTime":J
    .end local v53    # "i$":Ljava/util/Iterator;
    .end local v60    # "list":Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;
    .end local v62    # "num":I
    .end local v65    # "originalEventPlusCalendar":Ljava/lang/String;
    .end local v67    # "originalList":Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;
    .end local v70    # "originalTime":J
    .end local v72    # "originalValues":Landroid/content/ContentValues;
    .end local v81    # "syncIdKey":Ljava/lang/String;
    .end local v82    # "values":Landroid/content/ContentValues;
    :cond_23
    invoke-interface/range {v58 .. v58}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v52

    :cond_24
    invoke-interface/range {v52 .. v52}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_28

    invoke-interface/range {v52 .. v52}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v81

    check-cast v81, Ljava/lang/String;

    .line 570
    .restart local v81    # "syncIdKey":Ljava/lang/String;
    move-object/from16 v0, v57

    move-object/from16 v1, v81

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/CalendarInstancesHelper$EventInstancesMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v60

    check-cast v60, Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;

    .line 571
    .restart local v60    # "list":Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;
    invoke-virtual/range {v60 .. v60}, Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;->iterator()Ljava/util/Iterator;

    move-result-object v53

    .restart local v53    # "i$":Ljava/util/Iterator;
    :cond_25
    :goto_f
    invoke-interface/range {v53 .. v53}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_24

    invoke-interface/range {v53 .. v53}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v82

    check-cast v82, Landroid/content/ContentValues;

    .line 575
    .restart local v82    # "values":Landroid/content/ContentValues;
    const-string v8, "eventStatus"

    move-object/from16 v0, v82

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v77

    .line 576
    .local v77, "status":Ljava/lang/Integer;
    const-string v8, "deleted"

    move-object/from16 v0, v82

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_27

    const-string v8, "deleted"

    move-object/from16 v0, v82

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v33

    .line 578
    .restart local v33    # "deleted":Z
    :goto_10
    if-eqz v77, :cond_26

    invoke-virtual/range {v77 .. v77}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const/4 v11, 0x2

    if-eq v8, v11, :cond_25

    :cond_26
    if-nez v33, :cond_25

    .line 583
    const-string v8, "deleted"

    move-object/from16 v0, v82

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 586
    const-string v8, "ORIGINAL_EVENT_AND_CALENDAR"

    move-object/from16 v0, v82

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 587
    const-string v8, "originalInstanceTime"

    move-object/from16 v0, v82

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 588
    const-string v8, "eventStatus"

    move-object/from16 v0, v82

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 590
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mDbHelper:Lcom/android/providers/calendar/CalendarDatabaseHelper;

    move-object/from16 v0, v82

    invoke-virtual {v8, v0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->instancesReplace(Landroid/content/ContentValues;)J

    goto :goto_f

    .line 576
    .end local v33    # "deleted":Z
    :cond_27
    const/16 v33, 0x0

    goto :goto_10

    .line 593
    .end local v53    # "i$":Ljava/util/Iterator;
    .end local v60    # "list":Lcom/android/providers/calendar/CalendarInstancesHelper$InstancesList;
    .end local v77    # "status":Ljava/lang/Integer;
    .end local v81    # "syncIdKey":Ljava/lang/String;
    .end local v82    # "values":Landroid/content/ContentValues;
    :cond_28
    return-void
.end method

.method public updateInstancesLocked(Landroid/content/ContentValues;JZLandroid/database/sqlite/SQLiteDatabase;)V
    .locals 28
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "rowId"    # J
    .param p4, "newEvent"    # Z
    .param p5, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 696
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mMetaData:Lcom/android/providers/calendar/MetaData;

    invoke-virtual {v6}, Lcom/android/providers/calendar/MetaData;->getFieldsLocked()Lcom/android/providers/calendar/MetaData$Fields;

    move-result-object v17

    .line 697
    .local v17, "fields":Lcom/android/providers/calendar/MetaData$Fields;
    move-object/from16 v0, v17

    iget-wide v6, v0, Lcom/android/providers/calendar/MetaData$Fields;->maxInstance:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_1

    .line 790
    :cond_0
    :goto_0
    return-void

    .line 701
    :cond_1
    const-string v6, "dtstart"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v16

    .line 702
    .local v16, "dtstartMillis":Ljava/lang/Long;
    if-nez v16, :cond_3

    .line 703
    if-eqz p4, :cond_2

    .line 705
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "DTSTART missing."

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 707
    :cond_2
    const-string v6, "CalInstances"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 708
    const-string v6, "CalInstances"

    const-string v7, "Missing DTSTART.  No need to update instance."

    invoke-static {v6, v7}, Lcom/android/providers/calendar/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 713
    :cond_3
    if-nez p4, :cond_4

    .line 719
    const-string v6, "Instances"

    const-string v7, "event_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v8, v9

    move-object/from16 v0, p5

    invoke-virtual {v0, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 724
    :cond_4
    const-string v6, "rrule"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 725
    .local v24, "rrule":Ljava/lang/String;
    const-string v6, "rdate"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 726
    .local v23, "rdate":Ljava/lang/String;
    const-string v6, "original_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 727
    .local v20, "originalId":Ljava/lang/String;
    const-string v6, "original_sync_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 728
    .local v22, "originalSyncId":Ljava/lang/String;
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    move-object/from16 v2, v20

    move-object/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Lcom/android/providers/calendar/CalendarProvider2;->isRecurrenceEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 729
    const-string v6, "lastDate"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v19

    .line 730
    .local v19, "lastDateMillis":Ljava/lang/Long;
    const-string v6, "originalInstanceTime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v21

    .line 734
    .local v21, "originalInstanceTime":Ljava/lang/Long;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v17

    iget-wide v8, v0, Lcom/android/providers/calendar/MetaData$Fields;->maxInstance:J

    cmp-long v6, v6, v8

    if-gtz v6, :cond_7

    if-eqz v19, :cond_5

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v17

    iget-wide v8, v0, Lcom/android/providers/calendar/MetaData$Fields;->minInstance:J

    cmp-long v6, v6, v8

    if-ltz v6, :cond_7

    :cond_5
    const/16 v18, 0x1

    .line 739
    .local v18, "insideWindow":Z
    :goto_1
    if-eqz v21, :cond_8

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v17

    iget-wide v8, v0, Lcom/android/providers/calendar/MetaData$Fields;->maxInstance:J

    cmp-long v6, v6, v8

    if-gtz v6, :cond_8

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v17

    iget-wide v8, v0, Lcom/android/providers/calendar/MetaData$Fields;->minInstance:J

    const-wide/32 v26, 0x240c8400

    sub-long v8, v8, v26

    cmp-long v6, v6, v8

    if-ltz v6, :cond_8

    const/4 v12, 0x1

    .line 746
    .local v12, "affectsWindow":Z
    :goto_2
    if-nez v18, :cond_6

    if-eqz v12, :cond_0

    .line 747
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    move-object/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/providers/calendar/CalendarInstancesHelper;->updateRecurrenceInstancesLocked(Landroid/content/ContentValues;JLandroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0

    .line 734
    .end local v12    # "affectsWindow":Z
    .end local v18    # "insideWindow":Z
    :cond_7
    const/16 v18, 0x0

    goto :goto_1

    .line 739
    .restart local v18    # "insideWindow":Z
    :cond_8
    const/4 v12, 0x0

    goto :goto_2

    .line 755
    .end local v18    # "insideWindow":Z
    .end local v19    # "lastDateMillis":Ljava/lang/Long;
    .end local v21    # "originalInstanceTime":Ljava/lang/Long;
    :cond_9
    const-string v6, "dtend"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v15

    .line 756
    .local v15, "dtendMillis":Ljava/lang/Long;
    if-nez v15, :cond_a

    .line 757
    move-object/from16 v15, v16

    .line 765
    :cond_a
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v17

    iget-wide v8, v0, Lcom/android/providers/calendar/MetaData$Fields;->maxInstance:J

    cmp-long v6, v6, v8

    if-gtz v6, :cond_0

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v17

    iget-wide v8, v0, Lcom/android/providers/calendar/MetaData$Fields;->minInstance:J

    cmp-long v6, v6, v8

    if-ltz v6, :cond_0

    .line 766
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 767
    .local v11, "instanceValues":Landroid/content/ContentValues;
    const-string v6, "event_id"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v11, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 768
    const-string v6, "begin"

    move-object/from16 v0, v16

    invoke-virtual {v11, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 769
    const-string v6, "end"

    invoke-virtual {v11, v6, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 771
    const/4 v13, 0x0

    .line 772
    .local v13, "allDay":Z
    const-string v6, "allDay"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    .line 773
    .local v14, "allDayInteger":Ljava/lang/Integer;
    if-eqz v14, :cond_b

    .line 774
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-eqz v6, :cond_c

    const/4 v13, 0x1

    .line 778
    :cond_b
    :goto_3
    new-instance v10, Landroid/text/format/Time;

    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    .line 780
    .local v10, "local":Landroid/text/format/Time;
    if-eqz v13, :cond_d

    .line 781
    const-string v6, "UTC"

    iput-object v6, v10, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 786
    :goto_4
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static/range {v6 .. v11}, Lcom/android/providers/calendar/CalendarInstancesHelper;->computeTimezoneDependentFields(JJLandroid/text/format/Time;Landroid/content/ContentValues;)V

    .line 788
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/providers/calendar/CalendarInstancesHelper;->mDbHelper:Lcom/android/providers/calendar/CalendarDatabaseHelper;

    invoke-virtual {v6, v11}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->instancesInsert(Landroid/content/ContentValues;)J

    goto/16 :goto_0

    .line 774
    .end local v10    # "local":Landroid/text/format/Time;
    :cond_c
    const/4 v13, 0x0

    goto :goto_3

    .line 783
    .restart local v10    # "local":Landroid/text/format/Time;
    :cond_d
    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/android/providers/calendar/MetaData$Fields;->timezone:Ljava/lang/String;

    iput-object v6, v10, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    goto :goto_4
.end method
