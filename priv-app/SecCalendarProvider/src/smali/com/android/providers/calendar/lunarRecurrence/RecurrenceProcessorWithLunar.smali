.class public Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;
.super Ljava/lang/Object;
.source "RecurrenceProcessorWithLunar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;
    }
.end annotation


# static fields
.field private static final DAYS_IN_YEAR_PRECEDING_MONTH:[I

.field private static final DAYS_PER_MONTH:[I

.field private static mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

.field private static mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;


# instance fields
.field private Monthly_Lastday:Z

.field private Monthly_week_day:Z

.field private Monthly_weekend:Z

.field private mDays:Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;

.field private mGenerated:Lcom/android/providers/calendar/lunarRecurrence/LTime;

.field private mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

.field private mStringBuilder:Ljava/lang/StringBuilder;

.field private mUntil:Landroid/text/format/Time;

.field private yearly_anyday:Z

.field private yearly_first_second_third_fourth_day_of_month:Z

.field private yearly_lastday_of_month:Z

.field private yearly_weekday:Z

.field private yearly_weekend:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xc

    .line 1586
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->DAYS_PER_MONTH:[I

    .line 1588
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->DAYS_IN_YEAR_PRECEDING_MONTH:[I

    return-void

    .line 1586
    nop

    :array_0
    .array-data 4
        0x1f
        0x1c
        0x1f
        0x1e
        0x1f
        0x1e
        0x1f
        0x1f
        0x1e
        0x1f
        0x1e
        0x1f
    .end array-data

    .line 1588
    :array_1
    .array-data 4
        0x0
        0x1f
        0x3b
        0x5a
        0x78
        0x97
        0xb4
        0xd4
        0xf3
        0x111
        0x130
        0x14e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .line 34
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mUntil:Landroid/text/format/Time;

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mStringBuilder:Ljava/lang/StringBuilder;

    .line 36
    new-instance v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mGenerated:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .line 37
    new-instance v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;

    invoke-direct {v0, v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;-><init>(Z)V

    iput-object v0, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mDays:Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;

    .line 39
    iput-boolean v2, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_Lastday:Z

    .line 40
    iput-boolean v2, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_weekend:Z

    .line 41
    iput-boolean v2, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_week_day:Z

    .line 42
    iput-boolean v2, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_anyday:Z

    .line 43
    iput-boolean v2, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_weekday:Z

    .line 44
    iput-boolean v2, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_weekend:Z

    .line 45
    iput-boolean v2, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_first_second_third_fourth_day_of_month:Z

    .line 46
    iput-boolean v2, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_lastday_of_month:Z

    .line 52
    invoke-static {}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getInstance()Lcom/android/calendar/secfeature/SECCalendarFeatures;

    move-result-object v0

    sput-object v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    .line 53
    sget-object v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getSolarLunarTables()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    move-result-object v0

    sput-object v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    .line 54
    return-void
.end method

.method static convert2SolarDate(Landroid/text/format/Time;Z)Landroid/text/format/Time;
    .locals 5
    .param p0, "lunar"    # Landroid/text/format/Time;
    .param p1, "leap"    # Z

    .prologue
    .line 1427
    sget-object v2, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v2}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getSolarLunarConverter()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    move-result-object v0

    .line 1428
    .local v0, "slc":Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    iget v2, p0, Landroid/text/format/Time;->year:I

    iget v3, p0, Landroid/text/format/Time;->month:I

    iget v4, p0, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v0, v2, v3, v4, p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->convertLunarToSolar(IIIZ)V

    .line 1430
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 1431
    .local v1, "solar":Landroid/text/format/Time;
    invoke-virtual {v1, p0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1432
    invoke-virtual {v0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getYear()I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->year:I

    .line 1433
    invoke-virtual {v0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getMonth()I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->month:I

    .line 1434
    invoke-virtual {v0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getDay()I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->monthDay:I

    .line 1436
    return-object v1
.end method

.method private static filter(Lcom/android/providers/calendar/calendarcommon/EventRecurrence;Landroid/text/format/Time;)I
    .locals 10
    .param p0, "r"    # Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .param p1, "iterator"    # Landroid/text/format/Time;

    .prologue
    .line 196
    iget v3, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    .line 198
    .local v3, "freq":I
    const/4 v6, 0x6

    if-lt v6, v3, :cond_0

    .line 200
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthCount:I

    if-lez v6, :cond_0

    .line 201
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonth:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthCount:I

    iget v8, p1, Landroid/text/format/Time;->month:I

    add-int/lit8 v8, v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->listContains([III)Z

    move-result v2

    .line 203
    .local v2, "found":Z
    if-nez v2, :cond_0

    .line 204
    const/4 v6, 0x1

    .line 284
    .end local v2    # "found":Z
    :goto_0
    return v6

    .line 208
    :cond_0
    const/4 v6, 0x5

    if-lt v6, v3, :cond_1

    .line 211
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byweeknoCount:I

    if-lez v6, :cond_1

    .line 212
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byweekno:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byweeknoCount:I

    invoke-virtual {p1}, Landroid/text/format/Time;->getWeekNumber()I

    move-result v8

    const/16 v9, 0x9

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->listContains([IIII)Z

    move-result v2

    .line 215
    .restart local v2    # "found":Z
    if-nez v2, :cond_1

    .line 216
    const/4 v6, 0x2

    goto :goto_0

    .line 220
    .end local v2    # "found":Z
    :cond_1
    const/4 v6, 0x4

    if-lt v6, v3, :cond_4

    .line 222
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byyeardayCount:I

    if-lez v6, :cond_2

    .line 223
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byyearday:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byyeardayCount:I

    iget v8, p1, Landroid/text/format/Time;->yearDay:I

    const/16 v9, 0x8

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->listContains([IIII)Z

    move-result v2

    .line 225
    .restart local v2    # "found":Z
    if-nez v2, :cond_2

    .line 226
    const/4 v6, 0x3

    goto :goto_0

    .line 230
    .end local v2    # "found":Z
    :cond_2
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    if-lez v6, :cond_3

    .line 231
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthday:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    iget v8, p1, Landroid/text/format/Time;->monthDay:I

    const/4 v9, 0x4

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->listContains([IIII)Z

    move-result v2

    .line 234
    .restart local v2    # "found":Z
    if-nez v2, :cond_3

    .line 235
    const/4 v6, 0x4

    goto :goto_0

    .line 241
    .end local v2    # "found":Z
    :cond_3
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    if-lez v6, :cond_4

    .line 242
    iget-object v1, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byday:[I

    .line 243
    .local v1, "a":[I
    iget v0, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    .line 244
    .local v0, "N":I
    iget v6, p1, Landroid/text/format/Time;->weekDay:I

    invoke-static {v6}, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->timeDay2Day(I)I

    move-result v5

    .line 245
    .local v5, "v":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v0, :cond_6

    .line 246
    aget v6, v1, v4

    if-ne v6, v5, :cond_5

    .line 253
    .end local v0    # "N":I
    .end local v1    # "a":[I
    .end local v4    # "i":I
    .end local v5    # "v":I
    :cond_4
    const/4 v6, 0x3

    if-lt v6, v3, :cond_7

    .line 255
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byhour:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byhourCount:I

    iget v8, p1, Landroid/text/format/Time;->hour:I

    const/4 v9, 0x3

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->listContains([IIII)Z

    move-result v2

    .line 258
    .restart local v2    # "found":Z
    if-nez v2, :cond_7

    .line 259
    const/4 v6, 0x6

    goto :goto_0

    .line 245
    .end local v2    # "found":Z
    .restart local v0    # "N":I
    .restart local v1    # "a":[I
    .restart local v4    # "i":I
    .restart local v5    # "v":I
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 250
    :cond_6
    const/4 v6, 0x5

    goto :goto_0

    .line 262
    .end local v0    # "N":I
    .end local v1    # "a":[I
    .end local v4    # "i":I
    .end local v5    # "v":I
    :cond_7
    const/4 v6, 0x2

    if-lt v6, v3, :cond_8

    .line 264
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byminute:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byminuteCount:I

    iget v8, p1, Landroid/text/format/Time;->minute:I

    const/4 v9, 0x2

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->listContains([IIII)Z

    move-result v2

    .line 267
    .restart local v2    # "found":Z
    if-nez v2, :cond_8

    .line 268
    const/4 v6, 0x7

    goto/16 :goto_0

    .line 271
    .end local v2    # "found":Z
    :cond_8
    const/4 v6, 0x1

    if-lt v6, v3, :cond_9

    .line 273
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysecond:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysecondCount:I

    iget v8, p1, Landroid/text/format/Time;->second:I

    const/4 v9, 0x1

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->listContains([IIII)Z

    move-result v2

    .line 276
    .restart local v2    # "found":Z
    if-nez v2, :cond_9

    .line 277
    const/16 v6, 0x8

    goto/16 :goto_0

    .line 284
    .end local v2    # "found":Z
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_0
.end method

.method private static isLeapMonthIfWith(IIIZ)Z
    .locals 10
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "value"    # I
    .param p3, "isLeapMonth"    # Z

    .prologue
    const/16 v7, 0xd

    const/16 v6, 0xc

    .line 803
    move v2, p3

    .line 804
    .local v2, "isLeap":Z
    sget-object v8, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit16 v8, p0, -0x759

    sget-object v9, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    mul-int/lit8 v5, v8, 0xe

    .line 805
    .local v5, "startIndexOfYear":I
    sget-object v8, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    sget-object v9, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v9, v5, 0xd

    invoke-virtual {v8, v9}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v0

    .line 806
    .local v0, "indexOfLeapMonthOfYear":I
    if-ge p1, v0, :cond_0

    move v1, p1

    .line 807
    .local v1, "indexOfMonth":I
    :goto_0
    if-le v0, v6, :cond_1

    move v3, v6

    .line 809
    .local v3, "lengthOfYear":I
    :goto_1
    if-lez p2, :cond_3

    .line 810
    sub-int v8, v3, v1

    add-int/lit8 v4, v8, 0x1

    .line 811
    .local v4, "r":I
    if-le p2, v4, :cond_3

    .line 812
    sub-int/2addr p2, v4

    .line 813
    add-int/lit8 p0, p0, 0x1

    .line 815
    sget-object v8, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v5, v5, 0xe

    .line 816
    sget-object v8, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    sget-object v9, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v9, v5, 0xd

    invoke-virtual {v8, v9}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v0

    .line 817
    if-le v0, v6, :cond_2

    move v3, v6

    .line 821
    :goto_2
    goto :goto_1

    .line 806
    .end local v1    # "indexOfMonth":I
    .end local v3    # "lengthOfYear":I
    .end local v4    # "r":I
    :cond_0
    add-int/lit8 v1, p1, 0x1

    goto :goto_0

    .restart local v1    # "indexOfMonth":I
    :cond_1
    move v3, v7

    .line 807
    goto :goto_1

    .restart local v3    # "lengthOfYear":I
    .restart local v4    # "r":I
    :cond_2
    move v3, v7

    .line 817
    goto :goto_2

    .line 823
    .end local v4    # "r":I
    :cond_3
    :goto_3
    if-gez p2, :cond_5

    .line 824
    move v4, v1

    .line 825
    .restart local v4    # "r":I
    neg-int v8, p2

    if-le v8, v4, :cond_5

    .line 826
    add-int/2addr p2, v4

    .line 827
    add-int/lit8 p0, p0, -0x1

    .line 829
    sget-object v8, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v5, v5, -0xe

    .line 830
    sget-object v8, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    sget-object v9, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v9, v5, 0xd

    invoke-virtual {v8, v9}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v0

    .line 831
    if-le v0, v6, :cond_4

    move v1, v6

    .line 835
    :goto_4
    goto :goto_3

    :cond_4
    move v1, v7

    .line 831
    goto :goto_4

    .line 837
    .end local v4    # "r":I
    :cond_5
    add-int v6, v1, p2

    if-ne v0, v6, :cond_6

    const/4 v2, 0x1

    .line 838
    :goto_5
    return v2

    .line 837
    :cond_6
    const/4 v2, 0x0

    goto :goto_5
.end method

.method static isLeapYear(I)Z
    .locals 1
    .param p0, "year"    # I

    .prologue
    .line 1562
    rem-int/lit8 v0, p0, 0x4

    if-nez v0, :cond_1

    rem-int/lit8 v0, p0, 0x64

    if-nez v0, :cond_0

    rem-int/lit16 v0, p0, 0x190

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static listContains([III)Z
    .locals 2
    .param p0, "a"    # [I
    .param p1, "N"    # I
    .param p2, "v"    # I

    .prologue
    .line 151
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 152
    aget v1, p0, v0

    if-ne v1, p2, :cond_0

    .line 153
    const/4 v1, 0x1

    .line 156
    :goto_1
    return v1

    .line 151
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 156
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static listContains([IIII)Z
    .locals 3
    .param p0, "a"    # [I
    .param p1, "N"    # I
    .param p2, "v"    # I
    .param p3, "max"    # I

    .prologue
    const/4 v2, 0x1

    .line 169
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_3

    .line 170
    aget v1, p0, v0

    .line 171
    .local v1, "w":I
    if-lez v1, :cond_1

    .line 172
    if-ne v1, p2, :cond_2

    .line 182
    .end local v1    # "w":I
    :cond_0
    :goto_1
    return v2

    .line 176
    .restart local v1    # "w":I
    :cond_1
    add-int/2addr p3, v1

    .line 177
    if-eq p3, p2, :cond_0

    .line 169
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    .end local v1    # "w":I
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method static lunarMonthLength(IIZ)I
    .locals 5
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "isLeapMonth"    # Z

    .prologue
    .line 1614
    sget-object v3, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit16 v0, p0, -0x759

    .line 1615
    .local v0, "indexOfYear":I
    sget-object v3, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    mul-int/lit8 v2, v0, 0xe

    .line 1616
    .local v2, "startIndexOfYear":I
    sget-object v3, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    sget-object v4, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v4, v2, 0xd

    invoke-virtual {v3, v4}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v1

    .line 1619
    .local v1, "leapMonth":I
    if-nez p2, :cond_0

    if-ge p1, v1, :cond_0

    .line 1620
    sget-object v3, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    add-int v4, v2, p1

    invoke-virtual {v3, v4}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v3

    .line 1622
    :goto_0
    return v3

    :cond_0
    sget-object v3, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    add-int v4, v2, p1

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v3

    goto :goto_0
.end method

.method static lunarYearLength(I)I
    .locals 3
    .param p0, "year"    # I

    .prologue
    .line 1581
    sget-object v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit16 v0, p0, -0x759

    .line 1582
    .local v0, "indexOfYear":I
    sget-object v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getAccumulatedLunarDays(I)I

    move-result v1

    sget-object v2, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v2, v0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getAccumulatedLunarDays(I)I

    move-result v2

    sub-int/2addr v1, v2

    return v1
.end method

.method static monthLength(II)I
    .locals 3
    .param p0, "year"    # I
    .param p1, "month"    # I

    .prologue
    const/16 v1, 0x1c

    .line 1599
    sget-object v2, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->DAYS_PER_MONTH:[I

    aget v0, v2, p1

    .line 1600
    .local v0, "n":I
    if-eq v0, v1, :cond_0

    .line 1603
    .end local v0    # "n":I
    :goto_0
    return v0

    .restart local v0    # "n":I
    :cond_0
    invoke-static {p0}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->isLeapYear(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x1d

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static final normDateTimeComparisonValue(Landroid/text/format/Time;)J
    .locals 4
    .param p0, "normalized"    # Landroid/text/format/Time;

    .prologue
    .line 1674
    iget v0, p0, Landroid/text/format/Time;->year:I

    int-to-long v0, v0

    const/16 v2, 0x1a

    shl-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->month:I

    shl-int/lit8 v2, v2, 0x16

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->monthDay:I

    shl-int/lit8 v2, v2, 0x11

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->hour:I

    shl-int/lit8 v2, v2, 0xc

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->minute:I

    shl-int/lit8 v2, v2, 0x6

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->second:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private static final setTimeFromLongValue(Landroid/text/format/Time;J)V
    .locals 3
    .param p0, "date"    # Landroid/text/format/Time;
    .param p1, "val"    # J

    .prologue
    .line 1680
    const/16 v0, 0x1a

    shr-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Landroid/text/format/Time;->year:I

    .line 1681
    const/16 v0, 0x16

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0xf

    iput v0, p0, Landroid/text/format/Time;->month:I

    .line 1682
    const/16 v0, 0x11

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x1f

    iput v0, p0, Landroid/text/format/Time;->monthDay:I

    .line 1683
    const/16 v0, 0xc

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x1f

    iput v0, p0, Landroid/text/format/Time;->hour:I

    .line 1684
    const/4 v0, 0x6

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x3f

    iput v0, p0, Landroid/text/format/Time;->minute:I

    .line 1685
    const-wide/16 v0, 0x3f

    and-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, Landroid/text/format/Time;->second:I

    .line 1686
    return-void
.end method

.method static unsafeNormalize(Landroid/text/format/Time;)V
    .locals 1
    .param p0, "date"    # Landroid/text/format/Time;

    .prologue
    .line 1548
    new-instance v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    invoke-direct {v0}, Lcom/android/providers/calendar/lunarRecurrence/LTime;-><init>()V

    .line 1549
    .local v0, "t":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    invoke-virtual {v0, p0}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(Landroid/text/format/Time;)V

    .line 1551
    invoke-static {v0}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;)V

    .line 1552
    return-void
.end method

.method static unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;)V
    .locals 1
    .param p0, "date"    # Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .prologue
    .line 1544
    iget-boolean v0, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    invoke-static {p0, v0}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;Z)V

    .line 1545
    return-void
.end method

.method static unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;Z)V
    .locals 16
    .param p0, "date"    # Lcom/android/providers/calendar/lunarRecurrence/LTime;
    .param p1, "leapMonth"    # Z

    .prologue
    .line 1453
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    .line 1454
    .local v6, "lunarDate":Z
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->second:I

    .line 1455
    .local v11, "second":I
    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->minute:I

    .line 1456
    .local v7, "minute":I
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->hour:I

    .line 1457
    .local v5, "hour":I
    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    .line 1458
    .local v9, "monthDay":I
    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->month:I

    .line 1459
    .local v8, "month":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->year:I

    .line 1461
    .local v12, "year":I
    if-gez v11, :cond_0

    add-int/lit8 v15, v11, -0x3b

    :goto_0
    div-int/lit8 v3, v15, 0x3c

    .line 1462
    .local v3, "addMinutes":I
    mul-int/lit8 v15, v3, 0x3c

    sub-int/2addr v11, v15

    .line 1463
    add-int/2addr v7, v3

    .line 1464
    if-gez v7, :cond_1

    add-int/lit8 v15, v7, -0x3b

    :goto_1
    div-int/lit8 v2, v15, 0x3c

    .line 1465
    .local v2, "addHours":I
    mul-int/lit8 v15, v2, 0x3c

    sub-int/2addr v7, v15

    .line 1466
    add-int/2addr v5, v2

    .line 1467
    if-gez v5, :cond_2

    add-int/lit8 v15, v5, -0x17

    :goto_2
    div-int/lit8 v1, v15, 0x18

    .line 1468
    .local v1, "addDays":I
    mul-int/lit8 v15, v1, 0x18

    sub-int/2addr v5, v15

    .line 1469
    add-int/2addr v9, v1

    .line 1474
    :goto_3
    if-gtz v9, :cond_6

    .line 1482
    const/4 v4, 0x0

    .line 1483
    .local v4, "days":I
    const/4 v15, 0x1

    if-ne v6, v15, :cond_4

    .line 1485
    const/4 v15, 0x1

    if-le v8, v15, :cond_3

    invoke-static {v12}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->lunarYearLength(I)I

    move-result v4

    .line 1489
    :goto_4
    add-int/2addr v9, v4

    .line 1490
    add-int/lit8 v12, v12, -0x1

    .line 1491
    goto :goto_3

    .end local v1    # "addDays":I
    .end local v2    # "addHours":I
    .end local v3    # "addMinutes":I
    .end local v4    # "days":I
    :cond_0
    move v15, v11

    .line 1461
    goto :goto_0

    .restart local v3    # "addMinutes":I
    :cond_1
    move v15, v7

    .line 1464
    goto :goto_1

    .restart local v2    # "addHours":I
    :cond_2
    move v15, v5

    .line 1467
    goto :goto_2

    .line 1485
    .restart local v1    # "addDays":I
    .restart local v4    # "days":I
    :cond_3
    add-int/lit8 v15, v12, -0x1

    invoke-static {v15}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->lunarYearLength(I)I

    move-result v4

    goto :goto_4

    .line 1487
    :cond_4
    const/4 v15, 0x1

    if-le v8, v15, :cond_5

    invoke-static {v12}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearLength(I)I

    move-result v4

    :goto_5
    goto :goto_4

    :cond_5
    add-int/lit8 v15, v12, -0x1

    invoke-static {v15}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearLength(I)I

    move-result v4

    goto :goto_5

    .line 1493
    .end local v4    # "days":I
    :cond_6
    if-gez v8, :cond_9

    .line 1494
    add-int/lit8 v15, v8, 0x1

    div-int/lit8 v15, v15, 0xc

    add-int/lit8 v14, v15, -0x1

    .line 1495
    .local v14, "years":I
    add-int/2addr v12, v14

    .line 1496
    mul-int/lit8 v15, v14, 0xc

    sub-int/2addr v8, v15

    .line 1506
    .end local v14    # "years":I
    :cond_7
    :goto_6
    if-nez v8, :cond_8

    .line 1508
    if-eqz v6, :cond_a

    invoke-static {v12}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->lunarYearLength(I)I

    move-result v13

    .line 1509
    .local v13, "yearLength":I
    :goto_7
    if-le v9, v13, :cond_8

    .line 1511
    add-int/lit8 v12, v12, 0x1

    .line 1512
    sub-int/2addr v9, v13

    .line 1516
    .end local v13    # "yearLength":I
    :cond_8
    if-eqz v6, :cond_b

    move/from16 v0, p1

    invoke-static {v12, v8, v0}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->lunarMonthLength(IIZ)I

    move-result v10

    .line 1518
    .local v10, "monthLength":I
    :goto_8
    if-le v9, v10, :cond_c

    .line 1520
    sub-int/2addr v9, v10

    .line 1521
    add-int/lit8 v8, v8, 0x1

    .line 1522
    const/16 v15, 0xc

    if-lt v8, v15, :cond_7

    .line 1524
    add-int/lit8 v8, v8, -0xc

    .line 1525
    add-int/lit8 v12, v12, 0x1

    goto :goto_6

    .line 1497
    .end local v10    # "monthLength":I
    :cond_9
    const/16 v15, 0xc

    if-lt v8, v15, :cond_7

    .line 1498
    div-int/lit8 v14, v8, 0xc

    .line 1499
    .restart local v14    # "years":I
    add-int/2addr v12, v14

    .line 1500
    mul-int/lit8 v15, v14, 0xc

    sub-int/2addr v8, v15

    goto :goto_6

    .line 1508
    .end local v14    # "years":I
    :cond_a
    invoke-static {v12}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearLength(I)I

    move-result v13

    goto :goto_7

    .line 1516
    :cond_b
    invoke-static {v12, v8}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->monthLength(II)I

    move-result v10

    goto :goto_8

    .line 1532
    .restart local v10    # "monthLength":I
    :cond_c
    move-object/from16 v0, p0

    iput v11, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->second:I

    .line 1533
    move-object/from16 v0, p0

    iput v7, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->minute:I

    .line 1534
    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->hour:I

    .line 1535
    move-object/from16 v0, p0

    iput v9, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    .line 1536
    move-object/from16 v0, p0

    iput v8, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->month:I

    .line 1537
    move-object/from16 v0, p0

    iput v12, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->year:I

    .line 1538
    invoke-static {v12, v8, v9}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->weekDay(III)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    .line 1539
    invoke-static {v12, v8, v9}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearDay(III)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->yearDay:I

    .line 1540
    return-void
.end method

.method private static useBYX(III)Z
    .locals 1
    .param p0, "freq"    # I
    .param p1, "freqConstant"    # I
    .param p2, "count"    # I

    .prologue
    .line 309
    if-le p0, p1, :cond_0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static weekDay(III)I
    .locals 2
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "day"    # I

    .prologue
    .line 1636
    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    .line 1637
    add-int/lit8 p1, p1, 0xc

    .line 1638
    add-int/lit8 p0, p0, -0x1

    .line 1640
    :cond_0
    mul-int/lit8 v0, p1, 0xd

    add-int/lit8 v0, v0, -0xe

    div-int/lit8 v0, v0, 0x5

    add-int/2addr v0, p2

    add-int/2addr v0, p0

    div-int/lit8 v1, p0, 0x4

    add-int/2addr v0, v1

    div-int/lit8 v1, p0, 0x64

    sub-int/2addr v0, v1

    div-int/lit16 v1, p0, 0x190

    add-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x7

    return v0
.end method

.method static yearDay(III)I
    .locals 2
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "day"    # I

    .prologue
    .line 1652
    sget-object v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->DAYS_IN_YEAR_PRECEDING_MONTH:[I

    aget v1, v1, p1

    add-int/2addr v1, p2

    add-int/lit8 v0, v1, -0x1

    .line 1653
    .local v0, "yearDay":I
    const/4 v1, 0x2

    if-lt p1, v1, :cond_0

    invoke-static {p0}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->isLeapYear(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1654
    add-int/lit8 v0, v0, 0x1

    .line 1656
    :cond_0
    return v0
.end method

.method static yearLength(I)I
    .locals 1
    .param p0, "year"    # I

    .prologue
    .line 1572
    invoke-static {p0}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->isLeapYear(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x16e

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x16d

    goto :goto_0
.end method


# virtual methods
.method public expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/EventRecurrence;JJZLjava/util/TreeSet;)V
    .locals 65
    .param p1, "dtstart"    # Landroid/text/format/Time;
    .param p2, "r"    # Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .param p3, "rangeStartDateValue"    # J
    .param p5, "rangeEndDateValue"    # J
    .param p7, "add"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/format/Time;",
            "Lcom/android/providers/calendar/calendarcommon/EventRecurrence;",
            "JJZ",
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/calendar/calendarcommon/DateException;
        }
    .end annotation

    .prologue
    .line 870
    .local p8, "out":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Long;>;"
    const/16 v39, 0x0

    .line 871
    .local v39, "leapExpected":Z
    const/16 v31, 0x0

    .line 874
    .local v31, "inLunar":Z
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    move/from16 v60, v0

    if-eqz v60, :cond_2

    move-object/from16 v47, p1

    .line 875
    check-cast v47, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .line 876
    .local v47, "t":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    move/from16 v60, v0

    const/16 v61, 0x6

    move/from16 v0, v60

    move/from16 v1, v61

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    move/from16 v60, v0

    const/16 v61, 0x7

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_4

    .line 878
    :cond_0
    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    move/from16 v31, v0

    .line 880
    if-eqz v31, :cond_1

    .line 882
    sget-object v60, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual/range {v60 .. v60}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getSolarLunarConverter()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    move-result-object v46

    .line 883
    .local v46, "slc":Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    move-object/from16 v0, p1

    iget v0, v0, Landroid/text/format/Time;->year:I

    move/from16 v60, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/text/format/Time;->month:I

    move/from16 v61, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    move/from16 v62, v0

    move-object/from16 v0, v46

    move/from16 v1, v60

    move/from16 v2, v61

    move/from16 v3, v62

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->convertSolarToLunar(III)V

    .line 885
    invoke-virtual/range {v46 .. v46}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getYear()I

    move-result v60

    move/from16 v0, v60

    move-object/from16 v1, v47

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->year:I

    .line 886
    invoke-virtual/range {v46 .. v46}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getMonth()I

    move-result v60

    move/from16 v0, v60

    move-object/from16 v1, v47

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->month:I

    .line 887
    invoke-virtual/range {v46 .. v46}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getDay()I

    move-result v60

    move/from16 v0, v60

    move-object/from16 v1, v47

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    .line 888
    invoke-virtual/range {v46 .. v46}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isLeapMonth()Z

    move-result v60

    move/from16 v0, v60

    move-object/from16 v1, v47

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    .line 890
    .end local v46    # "slc":Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    :cond_1
    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    move/from16 v39, v0

    .line 898
    .end local v47    # "t":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    :cond_2
    :goto_0
    invoke-static/range {p1 .. p1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 900
    if-eqz v31, :cond_5

    .line 902
    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-static {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->convert2SolarDate(Landroid/text/format/Time;Z)Landroid/text/format/Time;

    move-result-object v48

    .line 903
    .local v48, "tempSolarDate":Landroid/text/format/Time;
    invoke-static/range {v48 .. v48}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v20

    .line 908
    .end local v48    # "tempSolarDate":Landroid/text/format/Time;
    .local v20, "dtstartDateValue":J
    :goto_1
    const/16 v16, 0x0

    .line 920
    .local v16, "count":I
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    move/from16 v60, v0

    const/16 v61, 0x6

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_b

    .line 921
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    if-eqz v60, :cond_6

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_6

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_6

    .line 925
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayNum:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v62, v0

    const/16 v63, 0x0

    aget v62, v62, v63

    aput v62, v60, v61

    .line 975
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    move-object/from16 v32, v0

    .line 976
    .local v32, "iterator":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mUntil:Landroid/text/format/Time;

    move-object/from16 v49, v0

    .line 977
    .local v49, "until":Landroid/text/format/Time;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mStringBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v44, v0

    .line 978
    .local v44, "sb":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mGenerated:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .line 979
    .local v4, "generated":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mDays:Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;

    move-object/from16 v18, v0

    .line 983
    .local v18, "days":Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;
    :try_start_0
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->setRecurrence(Lcom/android/providers/calendar/calendarcommon/EventRecurrence;)V

    .line 984
    const-wide v60, 0x7fffffffffffffffL

    cmp-long v60, p5, v60

    if-nez v60, :cond_11

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->until:Ljava/lang/String;

    move-object/from16 v60, v0

    if-nez v60, :cond_11

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->count:I

    move/from16 v60, v0

    if-nez v60, :cond_11

    .line 985
    new-instance v60, Lcom/android/providers/calendar/calendarcommon/DateException;

    const-string v61, "No range end provided for a recurrence that has no UNTIL or COUNT."

    invoke-direct/range {v60 .. v61}, Lcom/android/providers/calendar/calendarcommon/DateException;-><init>(Ljava/lang/String;)V

    throw v60
    :try_end_0
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1408
    :catch_0
    move-exception v22

    .line 1409
    .local v22, "e":Lcom/android/providers/calendar/calendarcommon/DateException;
    const-string v60, "RecurrenceProcessorWithLunar"

    new-instance v61, Ljava/lang/StringBuilder;

    invoke-direct/range {v61 .. v61}, Ljava/lang/StringBuilder;-><init>()V

    const-string v62, "DateException with r="

    invoke-virtual/range {v61 .. v62}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v61

    move-object/from16 v0, v61

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v61

    const-string v62, " rangeStart="

    invoke-virtual/range {v61 .. v62}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v61

    move-object/from16 v0, v61

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v61

    const-string v62, " rangeEnd="

    invoke-virtual/range {v61 .. v62}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v61

    move-object/from16 v0, v61

    move-wide/from16 v1, p5

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v61

    invoke-virtual/range {v61 .. v61}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v61

    invoke-static/range {v60 .. v61}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1411
    throw v22

    .line 893
    .end local v4    # "generated":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    .end local v16    # "count":I
    .end local v18    # "days":Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;
    .end local v20    # "dtstartDateValue":J
    .end local v22    # "e":Lcom/android/providers/calendar/calendarcommon/DateException;
    .end local v32    # "iterator":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    .end local v44    # "sb":Ljava/lang/StringBuilder;
    .end local v49    # "until":Landroid/text/format/Time;
    .restart local v47    # "t":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    :cond_4
    const/16 v60, 0x0

    move/from16 v0, v60

    move-object/from16 v1, v47

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    goto/16 :goto_0

    .line 905
    .end local v47    # "t":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    :cond_5
    invoke-static/range {p1 .. p1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v20

    .restart local v20    # "dtstartDateValue":J
    goto/16 :goto_1

    .line 926
    .restart local v16    # "count":I
    :cond_6
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    if-eqz v60, :cond_9

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_9

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v60, v0

    const/16 v61, 0x7

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_9

    .line 928
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    aget v60, v60, v61

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-eq v0, v1, :cond_7

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    aget v60, v60, v61

    const/16 v61, 0x2

    move/from16 v0, v60

    move/from16 v1, v61

    if-eq v0, v1, :cond_7

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    aget v60, v60, v61

    const/16 v61, 0x3

    move/from16 v0, v60

    move/from16 v1, v61

    if-eq v0, v1, :cond_7

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    aget v60, v60, v61

    const/16 v61, 0x4

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_8

    .line 931
    :cond_7
    const/16 v60, 0x1

    move/from16 v0, v60

    new-array v14, v0, [I

    .line 932
    .local v14, "bymonthday":[I
    const/16 v60, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v61, v0

    const/16 v62, 0x0

    aget v61, v61, v62

    aput v61, v14, v60

    .line 933
    move-object/from16 v0, p2

    iput-object v14, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthday:[I

    .line 934
    const/16 v60, 0x1

    move/from16 v0, v60

    move-object/from16 v1, p2

    iput v0, v1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    goto/16 :goto_2

    .line 935
    .end local v14    # "bymonthday":[I
    :cond_8
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    aget v60, v60, v61

    const/16 v61, -0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_3

    .line 937
    const/16 v60, 0x1

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_Lastday:Z

    goto/16 :goto_2

    .line 939
    :cond_9
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    if-eqz v60, :cond_a

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_a

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v60, v0

    const/16 v61, 0x2

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_a

    .line 942
    const/16 v60, 0x1

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_weekend:Z

    goto/16 :goto_2

    .line 944
    :cond_a
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    if-eqz v60, :cond_3

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v60, v0

    const/16 v61, 0x5

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_3

    .line 946
    const/16 v60, 0x1

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_week_day:Z

    goto/16 :goto_2

    .line 948
    :cond_b
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    move/from16 v60, v0

    const/16 v61, 0x7

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_3

    .line 949
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    if-eqz v60, :cond_c

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_c

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_c

    .line 951
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayNum:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v62, v0

    const/16 v63, 0x0

    aget v62, v62, v63

    aput v62, v60, v61

    .line 952
    const/16 v60, 0x1

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_anyday:Z

    goto/16 :goto_2

    .line 953
    :cond_c
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    if-eqz v60, :cond_d

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_d

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v60, v0

    const/16 v61, 0x5

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_d

    .line 955
    const/16 v60, 0x1

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_weekday:Z

    goto/16 :goto_2

    .line 956
    :cond_d
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    if-eqz v60, :cond_e

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_e

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v60, v0

    const/16 v61, 0x2

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_e

    .line 958
    const/16 v60, 0x1

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_weekend:Z

    goto/16 :goto_2

    .line 959
    :cond_e
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    if-eqz v60, :cond_3

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v60, v0

    const/16 v61, 0x7

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_3

    .line 960
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    aget v60, v60, v61

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-eq v0, v1, :cond_f

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    aget v60, v60, v61

    const/16 v61, 0x2

    move/from16 v0, v60

    move/from16 v1, v61

    if-eq v0, v1, :cond_f

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    aget v60, v60, v61

    const/16 v61, 0x3

    move/from16 v0, v60

    move/from16 v1, v61

    if-eq v0, v1, :cond_f

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    aget v60, v60, v61

    const/16 v61, 0x4

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_10

    .line 963
    :cond_f
    const/16 v60, 0x1

    move/from16 v0, v60

    new-array v14, v0, [I

    .line 964
    .restart local v14    # "bymonthday":[I
    const/16 v60, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v61, v0

    const/16 v62, 0x0

    aget v61, v61, v62

    aput v61, v14, v60

    .line 965
    move-object/from16 v0, p2

    iput-object v14, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthday:[I

    .line 966
    const/16 v60, 0x1

    move/from16 v0, v60

    move-object/from16 v1, p2

    iput v0, v1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    .line 967
    const/16 v60, 0x1

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_first_second_third_fourth_day_of_month:Z

    goto/16 :goto_2

    .line 968
    .end local v14    # "bymonthday":[I
    :cond_10
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v60, v0

    const/16 v61, 0x0

    aget v60, v60, v61

    const/16 v61, -0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_3

    .line 970
    const/16 v60, 0x1

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_lastday_of_month:Z

    goto/16 :goto_2

    .line 991
    .restart local v4    # "generated":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    .restart local v18    # "days":Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;
    .restart local v32    # "iterator":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    .restart local v44    # "sb":Ljava/lang/StringBuilder;
    .restart local v49    # "until":Landroid/text/format/Time;
    :cond_11
    :try_start_1
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->interval:I

    move/from16 v25, v0

    .line 992
    .local v25, "freqAmount":I
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    move/from16 v24, v0

    .line 993
    .local v24, "freq":I
    packed-switch v24, :pswitch_data_0

    .line 1021
    new-instance v60, Lcom/android/providers/calendar/calendarcommon/DateException;

    new-instance v61, Ljava/lang/StringBuilder;

    invoke-direct/range {v61 .. v61}, Ljava/lang/StringBuilder;-><init>()V

    const-string v62, "bad freq="

    invoke-virtual/range {v61 .. v62}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v61

    move-object/from16 v0, v61

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v61

    invoke-virtual/range {v61 .. v61}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v61

    invoke-direct/range {v60 .. v61}, Lcom/android/providers/calendar/calendarcommon/DateException;-><init>(Ljava/lang/String;)V

    throw v60
    :try_end_1
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1413
    .end local v24    # "freq":I
    .end local v25    # "freqAmount":I
    :catch_1
    move-exception v47

    .line 1414
    .local v47, "t":Ljava/lang/RuntimeException;
    const-string v60, "RecurrenceProcessorWithLunar"

    new-instance v61, Ljava/lang/StringBuilder;

    invoke-direct/range {v61 .. v61}, Ljava/lang/StringBuilder;-><init>()V

    const-string v62, "RuntimeException with r="

    invoke-virtual/range {v61 .. v62}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v61

    move-object/from16 v0, v61

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v61

    const-string v62, " rangeStart="

    invoke-virtual/range {v61 .. v62}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v61

    move-object/from16 v0, v61

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v61

    const-string v62, " rangeEnd="

    invoke-virtual/range {v61 .. v62}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v61

    move-object/from16 v0, v61

    move-wide/from16 v1, p5

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v61

    invoke-virtual/range {v61 .. v61}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v61

    invoke-static/range {v60 .. v61}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1416
    throw v47

    .line 996
    .end local v47    # "t":Ljava/lang/RuntimeException;
    .restart local v24    # "freq":I
    .restart local v25    # "freqAmount":I
    :pswitch_0
    const/16 v26, 0x1

    .line 1023
    .local v26, "freqField":I
    :cond_12
    :goto_3
    if-gtz v25, :cond_13

    .line 1024
    const/16 v25, 0x1

    .line 1027
    :cond_13
    :try_start_2
    move-object/from16 v0, p2

    iget v13, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthCount:I

    .line 1028
    .local v13, "bymonthCount":I
    const/16 v60, 0x6

    move/from16 v0, v24

    move/from16 v1, v60

    invoke-static {v0, v1, v13}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->useBYX(III)Z

    move-result v57

    .line 1029
    .local v57, "usebymonth":Z
    const/16 v60, 0x5

    move/from16 v0, v24

    move/from16 v1, v60

    if-lt v0, v1, :cond_1a

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v60, v0

    if-gtz v60, :cond_14

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    move/from16 v60, v0

    if-lez v60, :cond_1a

    :cond_14
    const/16 v54, 0x1

    .line 1031
    .local v54, "useDays":Z
    :goto_4
    move-object/from16 v0, p2

    iget v11, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byhourCount:I

    .line 1032
    .local v11, "byhourCount":I
    const/16 v60, 0x3

    move/from16 v0, v24

    move/from16 v1, v60

    invoke-static {v0, v1, v11}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->useBYX(III)Z

    move-result v55

    .line 1033
    .local v55, "usebyhour":Z
    move-object/from16 v0, p2

    iget v12, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byminuteCount:I

    .line 1034
    .local v12, "byminuteCount":I
    const/16 v60, 0x2

    move/from16 v0, v24

    move/from16 v1, v60

    invoke-static {v0, v1, v12}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->useBYX(III)Z

    move-result v56

    .line 1035
    .local v56, "usebyminute":Z
    move-object/from16 v0, p2

    iget v15, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysecondCount:I

    .line 1036
    .local v15, "bysecondCount":I
    const/16 v60, 0x1

    move/from16 v0, v24

    move/from16 v1, v60

    invoke-static {v0, v1, v15}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->useBYX(III)Z

    move-result v58

    .line 1039
    .local v58, "usebysecond":Z
    move-object/from16 v0, v32

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(Landroid/text/format/Time;)V

    .line 1040
    const/16 v60, 0x5

    move/from16 v0, v26

    move/from16 v1, v60

    if-ne v0, v1, :cond_15

    .line 1041
    if-eqz v54, :cond_15

    .line 1047
    const/16 v60, 0x1

    move/from16 v0, v60

    move-object/from16 v1, v32

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    .line 1052
    :cond_15
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->until:Ljava/lang/String;

    move-object/from16 v60, v0

    if-eqz v60, :cond_1b

    .line 1054
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->until:Ljava/lang/String;

    move-object/from16 v52, v0

    .line 1058
    .local v52, "untilStr":Ljava/lang/String;
    invoke-virtual/range {v52 .. v52}, Ljava/lang/String;->length()I

    move-result v60

    const/16 v61, 0xf

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_16

    .line 1059
    new-instance v60, Ljava/lang/StringBuilder;

    invoke-direct/range {v60 .. v60}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v60

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v60

    const/16 v61, 0x5a

    invoke-virtual/range {v60 .. v61}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v60

    invoke-virtual/range {v60 .. v60}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v52

    .line 1062
    :cond_16
    move-object/from16 v0, v49

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 1064
    move-object/from16 v0, v49

    iget v0, v0, Landroid/text/format/Time;->year:I

    move/from16 v60, v0

    const/16 v61, 0x7f4

    move/from16 v0, v60

    move/from16 v1, v61

    if-le v0, v1, :cond_17

    .line 1065
    const/16 v60, 0x7f4

    move/from16 v0, v60

    move-object/from16 v1, v49

    iput v0, v1, Landroid/text/format/Time;->year:I

    .line 1066
    const/16 v60, 0xb

    move/from16 v0, v60

    move-object/from16 v1, v49

    iput v0, v1, Landroid/text/format/Time;->month:I

    .line 1067
    const/16 v60, 0x1f

    move/from16 v0, v60

    move-object/from16 v1, v49

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 1073
    :cond_17
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    move-object/from16 v60, v0

    move-object/from16 v0, v49

    move-object/from16 v1, v60

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 1074
    invoke-static/range {v49 .. v49}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v50

    .line 1086
    .end local v52    # "untilStr":Ljava/lang/String;
    .local v50, "untilDateValue":J
    :goto_5
    const/16 v60, 0xf

    move-object/from16 v0, v44

    move/from16 v1, v60

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->ensureCapacity(I)V

    .line 1087
    const/16 v60, 0xf

    move-object/from16 v0, v44

    move/from16 v1, v60

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1098
    :cond_18
    const/16 v41, 0x0

    .line 1100
    .local v41, "monthIndex":I
    move-object/from16 v0, v32

    move/from16 v1, v39

    invoke-static {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;Z)V

    .line 1102
    move-object/from16 v0, v32

    iget v10, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->year:I

    .line 1103
    .local v10, "iteratorYear":I
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->month:I

    move/from16 v60, v0

    add-int/lit8 v36, v60, 0x1

    .line 1104
    .local v36, "iteratorMonth":I
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    move/from16 v33, v0

    .line 1105
    .local v33, "iteratorDay":I
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->hour:I

    move/from16 v34, v0

    .line 1106
    .local v34, "iteratorHour":I
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->minute:I

    move/from16 v35, v0

    .line 1107
    .local v35, "iteratorMinute":I
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->second:I

    move/from16 v37, v0

    .line 1110
    .local v37, "iteratorSecond":I
    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(Landroid/text/format/Time;)V

    .line 1113
    iget v0, v4, Lcom/android/providers/calendar/lunarRecurrence/LTime;->year:I

    move/from16 v60, v0

    const/16 v61, 0x7f4

    move/from16 v0, v60

    move/from16 v1, v61

    if-le v0, v1, :cond_1c

    .line 1399
    :cond_19
    const/16 v60, 0x0

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_Lastday:Z

    .line 1400
    const/16 v60, 0x0

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_weekend:Z

    .line 1401
    const/16 v60, 0x0

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_week_day:Z

    .line 1402
    const/16 v60, 0x0

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_anyday:Z

    .line 1403
    const/16 v60, 0x0

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_weekday:Z

    .line 1404
    const/16 v60, 0x0

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_weekend:Z

    .line 1405
    const/16 v60, 0x0

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_lastday_of_month:Z

    .line 1406
    const/16 v60, 0x0

    move/from16 v0, v60

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_first_second_third_fourth_day_of_month:Z

    .line 1418
    return-void

    .line 999
    .end local v10    # "iteratorYear":I
    .end local v11    # "byhourCount":I
    .end local v12    # "byminuteCount":I
    .end local v13    # "bymonthCount":I
    .end local v15    # "bysecondCount":I
    .end local v26    # "freqField":I
    .end local v33    # "iteratorDay":I
    .end local v34    # "iteratorHour":I
    .end local v35    # "iteratorMinute":I
    .end local v36    # "iteratorMonth":I
    .end local v37    # "iteratorSecond":I
    .end local v41    # "monthIndex":I
    .end local v50    # "untilDateValue":J
    .end local v54    # "useDays":Z
    .end local v55    # "usebyhour":Z
    .end local v56    # "usebyminute":Z
    .end local v57    # "usebymonth":Z
    .end local v58    # "usebysecond":Z
    :pswitch_1
    const/16 v26, 0x2

    .line 1000
    .restart local v26    # "freqField":I
    goto/16 :goto_3

    .line 1002
    .end local v26    # "freqField":I
    :pswitch_2
    const/16 v26, 0x3

    .line 1003
    .restart local v26    # "freqField":I
    goto/16 :goto_3

    .line 1005
    .end local v26    # "freqField":I
    :pswitch_3
    const/16 v26, 0x4

    .line 1006
    .restart local v26    # "freqField":I
    goto/16 :goto_3

    .line 1008
    .end local v26    # "freqField":I
    :pswitch_4
    const/16 v26, 0x4

    .line 1009
    .restart local v26    # "freqField":I
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->interval:I

    move/from16 v60, v0

    mul-int/lit8 v25, v60, 0x7

    .line 1010
    if-gtz v25, :cond_12

    .line 1011
    const/16 v25, 0x7

    goto/16 :goto_3

    .line 1015
    .end local v26    # "freqField":I
    :pswitch_5
    const/16 v26, 0x5

    .line 1016
    .restart local v26    # "freqField":I
    goto/16 :goto_3

    .line 1018
    .end local v26    # "freqField":I
    :pswitch_6
    const/16 v26, 0x6

    .line 1019
    .restart local v26    # "freqField":I
    goto/16 :goto_3

    .line 1029
    .restart local v13    # "bymonthCount":I
    .restart local v57    # "usebymonth":Z
    :cond_1a
    const/16 v54, 0x0

    goto/16 :goto_4

    .line 1076
    .restart local v11    # "byhourCount":I
    .restart local v12    # "byminuteCount":I
    .restart local v15    # "bysecondCount":I
    .restart local v54    # "useDays":Z
    .restart local v55    # "usebyhour":Z
    .restart local v56    # "usebyminute":Z
    .restart local v58    # "usebysecond":Z
    :cond_1b
    new-instance v53, Landroid/text/format/Time;

    move-object/from16 v0, v53

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1078
    .local v53, "untilTime":Landroid/text/format/Time;
    const/16 v60, 0x7f4

    move/from16 v0, v60

    move-object/from16 v1, v53

    iput v0, v1, Landroid/text/format/Time;->year:I

    .line 1079
    const/16 v60, 0xb

    move/from16 v0, v60

    move-object/from16 v1, v53

    iput v0, v1, Landroid/text/format/Time;->month:I

    .line 1080
    const/16 v60, 0x1f

    move/from16 v0, v60

    move-object/from16 v1, v53

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 1081
    const/16 v60, 0x1

    move-object/from16 v0, v53

    move/from16 v1, v60

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 1083
    invoke-static/range {v53 .. v53}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v50

    .restart local v50    # "untilDateValue":J
    goto/16 :goto_5

    .line 1118
    .end local v53    # "untilTime":Landroid/text/format/Time;
    .restart local v10    # "iteratorYear":I
    .restart local v33    # "iteratorDay":I
    .restart local v34    # "iteratorHour":I
    .restart local v35    # "iteratorMinute":I
    .restart local v36    # "iteratorMonth":I
    .restart local v37    # "iteratorSecond":I
    .restart local v41    # "monthIndex":I
    :cond_1c
    if-eqz v57, :cond_2a

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonth:[I

    move-object/from16 v60, v0

    aget v9, v60, v41

    .line 1121
    .local v9, "month":I
    :goto_6
    add-int/lit8 v9, v9, -0x1

    .line 1124
    const/16 v17, 0x1

    .line 1125
    .local v17, "dayIndex":I
    const/16 v38, 0x0

    .line 1130
    .local v38, "lastDayToExamine":I
    if-eqz v54, :cond_1d

    .line 1134
    const/16 v60, 0x5

    move/from16 v0, v24

    move/from16 v1, v60

    if-ne v0, v1, :cond_2b

    .line 1135
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    move/from16 v19, v0

    .line 1136
    .local v19, "dow":I
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    move/from16 v60, v0

    sub-int v17, v60, v19

    .line 1137
    add-int/lit8 v38, v17, 0x6

    .line 1149
    .end local v19    # "dow":I
    :cond_1d
    :goto_7
    if-eqz v54, :cond_36

    .line 1150
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_Lastday:Z

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_2c

    .line 1152
    move-object/from16 v0, v18

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getlastday(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I

    move-result v8

    .line 1153
    .local v8, "day":I
    move/from16 v17, v8

    .line 1201
    :goto_8
    const/16 v30, 0x0

    .line 1203
    .local v30, "hourIndex":I
    :cond_1e
    if-eqz v55, :cond_37

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byhour:[I

    move-object/from16 v60, v0

    aget v7, v60, v30

    .line 1209
    .local v7, "hour":I
    :goto_9
    const/16 v40, 0x0

    .line 1211
    .local v40, "minuteIndex":I
    :cond_1f
    if-eqz v56, :cond_38

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byminute:[I

    move-object/from16 v60, v0

    aget v6, v60, v40

    .line 1217
    .local v6, "minute":I
    :goto_a
    const/16 v45, 0x0

    .line 1219
    .local v45, "secondIndex":I
    :cond_20
    if-eqz v58, :cond_39

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysecond:[I

    move-object/from16 v60, v0

    aget v5, v60, v45

    .line 1227
    .local v5, "second":I
    :goto_b
    invoke-virtual/range {v4 .. v10}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(IIIIII)V

    .line 1228
    const/16 v60, 0x0

    move/from16 v0, v60

    invoke-static {v4, v0}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;Z)V

    .line 1231
    const-wide/16 v28, 0x0

    .line 1232
    .local v28, "genDateValue":J
    if-eqz v31, :cond_3a

    .line 1234
    move/from16 v0, v39

    invoke-static {v4, v0}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->convert2SolarDate(Landroid/text/format/Time;Z)Landroid/text/format/Time;

    move-result-object v27

    .line 1235
    .local v27, "generatedSolar":Landroid/text/format/Time;
    invoke-static/range {v27 .. v27}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v28

    .line 1244
    .end local v27    # "generatedSolar":Landroid/text/format/Time;
    :goto_c
    cmp-long v60, v28, v20

    if-ltz v60, :cond_23

    .line 1246
    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->filter(Lcom/android/providers/calendar/calendarcommon/EventRecurrence;Landroid/text/format/Time;)I

    move-result v23

    .line 1247
    .local v23, "filtered":I
    if-nez v23, :cond_23

    .line 1266
    if-eqz p7, :cond_3b

    cmp-long v60, v20, p3

    if-ltz v60, :cond_3b

    cmp-long v60, v20, p5

    if-gez v60, :cond_3b

    .line 1270
    add-int/lit8 v16, v16, 0x1

    .line 1282
    :cond_21
    :goto_d
    cmp-long v60, v28, v50

    if-gtz v60, :cond_19

    .line 1292
    cmp-long v60, v28, p5

    if-gez v60, :cond_19

    .line 1301
    cmp-long v60, v28, p3

    if-ltz v60, :cond_22

    .line 1305
    if-eqz p7, :cond_3c

    .line 1306
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v60

    move-object/from16 v0, p8

    move-object/from16 v1, v60

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 1312
    :cond_22
    :goto_e
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->count:I

    move/from16 v60, v0

    if-lez v60, :cond_23

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->count:I

    move/from16 v60, v0

    move/from16 v0, v60

    move/from16 v1, v16

    if-eq v0, v1, :cond_19

    .line 1318
    .end local v23    # "filtered":I
    :cond_23
    add-int/lit8 v45, v45, 0x1

    .line 1319
    if-eqz v58, :cond_24

    move/from16 v0, v45

    if-lt v0, v15, :cond_20

    .line 1320
    :cond_24
    add-int/lit8 v40, v40, 0x1

    .line 1321
    if-eqz v56, :cond_25

    move/from16 v0, v40

    if-lt v0, v12, :cond_1f

    .line 1322
    :cond_25
    add-int/lit8 v30, v30, 0x1

    .line 1323
    if-eqz v55, :cond_26

    move/from16 v0, v30

    if-lt v0, v11, :cond_1e

    .line 1324
    :cond_26
    add-int/lit8 v17, v17, 0x1

    .line 1325
    .end local v5    # "second":I
    .end local v6    # "minute":I
    .end local v7    # "hour":I
    .end local v8    # "day":I
    .end local v28    # "genDateValue":J
    .end local v30    # "hourIndex":I
    .end local v40    # "minuteIndex":I
    .end local v45    # "secondIndex":I
    :goto_f
    if-eqz v54, :cond_27

    move/from16 v0, v17

    move/from16 v1, v38

    if-le v0, v1, :cond_1d

    .line 1326
    :cond_27
    add-int/lit8 v41, v41, 0x1

    .line 1327
    if-eqz v57, :cond_28

    move/from16 v0, v41

    if-lt v0, v13, :cond_1c

    .line 1333
    :cond_28
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    move/from16 v43, v0

    .line 1334
    .local v43, "oldDay":I
    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(Landroid/text/format/Time;)V

    .line 1335
    const/16 v42, 0x1

    .line 1337
    .local v42, "n":I
    :goto_10
    mul-int v59, v25, v42

    .line 1340
    .local v59, "value":I
    if-eqz v31, :cond_29

    const/16 v60, 0x5

    move/from16 v0, v26

    move/from16 v1, v60

    if-ne v0, v1, :cond_29

    .line 1341
    if-nez v39, :cond_3d

    .line 1342
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->year:I

    move/from16 v60, v0

    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->month:I

    move/from16 v61, v0

    const/16 v62, 0x0

    move/from16 v0, v60

    move/from16 v1, v61

    move/from16 v2, v59

    move/from16 v3, v62

    invoke-static {v0, v1, v2, v3}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->isLeapMonthIfWith(IIIZ)Z

    move-result v39

    .line 1347
    if-eqz v39, :cond_29

    .line 1348
    const/16 v60, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v60

    invoke-static {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;Z)V

    .line 1349
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    move/from16 v60, v0

    move/from16 v0, v60

    move/from16 v1, v43

    if-eq v0, v1, :cond_18

    .line 1358
    :cond_29
    :goto_11
    packed-switch v26, :pswitch_data_1

    .line 1384
    new-instance v60, Ljava/lang/RuntimeException;

    new-instance v61, Ljava/lang/StringBuilder;

    invoke-direct/range {v61 .. v61}, Ljava/lang/StringBuilder;-><init>()V

    const-string v62, "bad field="

    invoke-virtual/range {v61 .. v62}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v61

    move-object/from16 v0, v61

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v61

    invoke-virtual/range {v61 .. v61}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v61

    invoke-direct/range {v60 .. v61}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v60

    .end local v9    # "month":I
    .end local v17    # "dayIndex":I
    .end local v38    # "lastDayToExamine":I
    .end local v42    # "n":I
    .end local v43    # "oldDay":I
    .end local v59    # "value":I
    :cond_2a
    move/from16 v9, v36

    .line 1118
    goto/16 :goto_6

    .line 1139
    .restart local v9    # "month":I
    .restart local v17    # "dayIndex":I
    .restart local v38    # "lastDayToExamine":I
    :cond_2b
    const/16 v60, 0x4

    move/from16 v0, v60

    invoke-virtual {v4, v0}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->getActualMaximum(I)I

    move-result v38

    goto/16 :goto_7

    .line 1154
    :cond_2c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_weekend:Z

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_2d

    .line 1156
    move-object/from16 v0, v18

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getweekend_day(Landroid/text/format/Time;)I

    move-result v8

    .line 1157
    .restart local v8    # "day":I
    move-object/from16 v0, v18

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getlastday(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I

    move-result v17

    goto/16 :goto_8

    .line 1158
    .end local v8    # "day":I
    :cond_2d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->Monthly_week_day:Z

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_2e

    .line 1160
    move-object/from16 v0, v18

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getweek_day(Landroid/text/format/Time;)I

    move-result v8

    .line 1161
    .restart local v8    # "day":I
    move-object/from16 v0, v18

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getlastday(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I

    move-result v17

    goto/16 :goto_8

    .line 1164
    .end local v8    # "day":I
    :cond_2e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_anyday:Z

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-eq v0, v1, :cond_2f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_weekday:Z

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-eq v0, v1, :cond_2f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_weekend:Z

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-eq v0, v1, :cond_2f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_first_second_third_fourth_day_of_month:Z

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-eq v0, v1, :cond_2f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_lastday_of_month:Z

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_30

    .line 1168
    :cond_2f
    move-object/from16 v0, v32

    iput v9, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->month:I

    .line 1170
    :cond_30
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_weekday:Z

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_31

    .line 1171
    move-object/from16 v0, v18

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getweek_day(Landroid/text/format/Time;)I

    move-result v8

    .line 1172
    .restart local v8    # "day":I
    const/16 v17, 0x1f

    goto/16 :goto_8

    .line 1173
    .end local v8    # "day":I
    :cond_31
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_weekend:Z

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_32

    .line 1174
    move-object/from16 v0, v18

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getweekend_day(Landroid/text/format/Time;)I

    move-result v8

    .line 1175
    .restart local v8    # "day":I
    const/16 v17, 0x1f

    goto/16 :goto_8

    .line 1176
    .end local v8    # "day":I
    :cond_32
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->yearly_lastday_of_month:Z

    move/from16 v60, v0

    const/16 v61, 0x1

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_33

    .line 1177
    move-object/from16 v0, v18

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getlastday(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I

    move-result v8

    .line 1178
    .restart local v8    # "day":I
    const/16 v17, 0x1f

    goto/16 :goto_8

    .line 1181
    .end local v8    # "day":I
    :cond_33
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    move/from16 v60, v0

    const/16 v61, 0x7

    move/from16 v0, v60

    move/from16 v1, v61

    if-ne v0, v1, :cond_34

    .line 1182
    const/16 v60, 0x4

    move-object/from16 v0, v32

    move/from16 v1, v60

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->getActualMaximum(I)I

    move-result v38

    .line 1184
    :cond_34
    move-object/from16 v0, v18

    move-object/from16 v1, v32

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->get(Lcom/android/providers/calendar/lunarRecurrence/LTime;I)Z

    move-result v60

    if-nez v60, :cond_35

    .line 1185
    add-int/lit8 v17, v17, 0x1

    .line 1186
    goto/16 :goto_f

    .line 1188
    :cond_35
    move/from16 v8, v17

    .restart local v8    # "day":I
    goto/16 :goto_8

    .line 1194
    .end local v8    # "day":I
    :cond_36
    move/from16 v8, v33

    .restart local v8    # "day":I
    goto/16 :goto_8

    .restart local v30    # "hourIndex":I
    :cond_37
    move/from16 v7, v34

    .line 1203
    goto/16 :goto_9

    .restart local v7    # "hour":I
    .restart local v40    # "minuteIndex":I
    :cond_38
    move/from16 v6, v35

    .line 1211
    goto/16 :goto_a

    .restart local v6    # "minute":I
    .restart local v45    # "secondIndex":I
    :cond_39
    move/from16 v5, v37

    .line 1219
    goto/16 :goto_b

    .line 1237
    .restart local v5    # "second":I
    .restart local v28    # "genDateValue":J
    :cond_3a
    invoke-static {v4}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v28

    goto/16 :goto_c

    .line 1277
    .restart local v23    # "filtered":I
    :cond_3b
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->count:I

    move/from16 v60, v0

    if-lez v60, :cond_21

    .line 1278
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_d

    .line 1308
    :cond_3c
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v60

    move-object/from16 v0, p8

    move-object/from16 v1, v60

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_e

    .line 1354
    .end local v5    # "second":I
    .end local v6    # "minute":I
    .end local v7    # "hour":I
    .end local v8    # "day":I
    .end local v23    # "filtered":I
    .end local v28    # "genDateValue":J
    .end local v30    # "hourIndex":I
    .end local v40    # "minuteIndex":I
    .end local v45    # "secondIndex":I
    .restart local v42    # "n":I
    .restart local v43    # "oldDay":I
    .restart local v59    # "value":I
    :cond_3d
    const/16 v39, 0x0

    goto/16 :goto_11

    .line 1360
    :pswitch_7
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->second:I

    move/from16 v60, v0

    add-int v60, v60, v59

    move/from16 v0, v60

    move-object/from16 v1, v32

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->second:I

    .line 1387
    :goto_12
    const/16 v60, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v60

    invoke-static {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;Z)V

    .line 1388
    const/16 v60, 0x6

    move/from16 v0, v26

    move/from16 v1, v60

    if-eq v0, v1, :cond_3e

    const/16 v60, 0x5

    move/from16 v0, v26

    move/from16 v1, v60

    if-ne v0, v1, :cond_18

    .line 1391
    :cond_3e
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    move/from16 v60, v0

    move/from16 v0, v60

    move/from16 v1, v43

    if-eq v0, v1, :cond_18

    .line 1394
    add-int/lit8 v42, v42, 0x1

    .line 1395
    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(Landroid/text/format/Time;)V

    goto/16 :goto_10

    .line 1363
    :pswitch_8
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->minute:I

    move/from16 v60, v0

    add-int v60, v60, v59

    move/from16 v0, v60

    move-object/from16 v1, v32

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->minute:I

    goto :goto_12

    .line 1366
    :pswitch_9
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->hour:I

    move/from16 v60, v0

    add-int v60, v60, v59

    move/from16 v0, v60

    move-object/from16 v1, v32

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->hour:I

    goto :goto_12

    .line 1369
    :pswitch_a
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    move/from16 v60, v0

    add-int v60, v60, v59

    move/from16 v0, v60

    move-object/from16 v1, v32

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    goto :goto_12

    .line 1372
    :pswitch_b
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->month:I

    move/from16 v60, v0

    add-int v60, v60, v59

    move/from16 v0, v60

    move-object/from16 v1, v32

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->month:I

    goto :goto_12

    .line 1375
    :pswitch_c
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->year:I

    move/from16 v60, v0

    add-int v60, v60, v59

    move/from16 v0, v60

    move-object/from16 v1, v32

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->year:I

    goto :goto_12

    .line 1378
    :pswitch_d
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    move/from16 v60, v0

    add-int v60, v60, v59

    move/from16 v0, v60

    move-object/from16 v1, v32

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    goto/16 :goto_12

    .line 1381
    :pswitch_e
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    move/from16 v60, v0

    add-int v60, v60, v59

    move/from16 v0, v60

    move-object/from16 v1, v32

    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I
    :try_end_2
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_12

    .line 993
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 1358
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;JJ)[J
    .locals 37
    .param p1, "dtstart"    # Landroid/text/format/Time;
    .param p2, "recur"    # Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;
    .param p3, "rangeStartMillis"    # J
    .param p5, "rangeEndMillis"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/calendar/calendarcommon/DateException;
        }
    .end annotation

    .prologue
    .line 719
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    move-object/from16 v34, v0

    .line 720
    .local v34, "timezone":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    move-object/from16 v0, v34

    invoke-virtual {v5, v0}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->clear(Ljava/lang/String;)V

    .line 721
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mGenerated:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    move-object/from16 v0, v34

    invoke-virtual {v5, v0}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->clear(Ljava/lang/String;)V

    .line 727
    const-wide v18, -0x1f3be2e8340L

    cmp-long v5, p3, v18

    if-gez v5, :cond_0

    .line 731
    const-wide p3, -0x1f3be2e8340L

    .line 734
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(J)V

    .line 735
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    invoke-static {v5}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v8

    .line 738
    .local v8, "rangeStartDateValue":J
    const-wide/16 v18, -0x1

    cmp-long v5, p5, v18

    if-eqz v5, :cond_1

    .line 739
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    move-wide/from16 v0, p5

    invoke-virtual {v5, v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(J)V

    .line 740
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    invoke-static {v5}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v10

    .line 745
    .local v10, "rangeEndDateValue":J
    :goto_0
    new-instance v13, Ljava/util/TreeSet;

    invoke-direct {v13}, Ljava/util/TreeSet;-><init>()V

    .line 747
    .local v13, "dtSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Long;>;"
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    if-eqz v5, :cond_2

    .line 748
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .local v4, "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    array-length v0, v4

    move/from16 v32, v0

    .local v32, "len$":I
    const/16 v30, 0x0

    .local v30, "i$":I
    :goto_1
    move/from16 v0, v30

    move/from16 v1, v32

    if-ge v0, v1, :cond_2

    aget-object v7, v4, v30

    .line 749
    .local v7, "rrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    const/4 v12, 0x1

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-virtual/range {v5 .. v13}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/EventRecurrence;JJZLjava/util/TreeSet;)V

    .line 748
    add-int/lit8 v30, v30, 0x1

    goto :goto_1

    .line 742
    .end local v4    # "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .end local v7    # "rrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .end local v10    # "rangeEndDateValue":J
    .end local v13    # "dtSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Long;>;"
    .end local v30    # "i$":I
    .end local v32    # "len$":I
    :cond_1
    const-wide v10, 0x7fffffffffffffffL

    .restart local v10    # "rangeEndDateValue":J
    goto :goto_0

    .line 753
    .restart local v13    # "dtSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Long;>;"
    :cond_2
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    if-eqz v5, :cond_3

    .line 754
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    .local v4, "arr$":[J
    array-length v0, v4

    move/from16 v32, v0

    .restart local v32    # "len$":I
    const/16 v30, 0x0

    .restart local v30    # "i$":I
    :goto_2
    move/from16 v0, v30

    move/from16 v1, v32

    if-ge v0, v1, :cond_3

    aget-wide v24, v4, v30

    .line 757
    .local v24, "dt":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    move-wide/from16 v0, v24

    invoke-virtual {v5, v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(J)V

    .line 758
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    invoke-static {v5}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v26

    .line 759
    .local v26, "dtvalue":J
    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v13, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 754
    add-int/lit8 v30, v30, 0x1

    goto :goto_2

    .line 762
    .end local v4    # "arr$":[J
    .end local v24    # "dt":J
    .end local v26    # "dtvalue":J
    .end local v30    # "i$":I
    .end local v32    # "len$":I
    :cond_3
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    if-eqz v5, :cond_4

    .line 763
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .local v4, "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    array-length v0, v4

    move/from16 v32, v0

    .restart local v32    # "len$":I
    const/16 v30, 0x0

    .restart local v30    # "i$":I
    :goto_3
    move/from16 v0, v30

    move/from16 v1, v32

    if-ge v0, v1, :cond_4

    aget-object v17, v4, v30

    .line 764
    .local v17, "exrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    const/16 v22, 0x0

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    move-wide/from16 v18, v8

    move-wide/from16 v20, v10

    move-object/from16 v23, v13

    invoke-virtual/range {v15 .. v23}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/EventRecurrence;JJZLjava/util/TreeSet;)V

    .line 763
    add-int/lit8 v30, v30, 0x1

    goto :goto_3

    .line 768
    .end local v4    # "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .end local v17    # "exrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .end local v30    # "i$":I
    .end local v32    # "len$":I
    :cond_4
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exdates:[J

    if-eqz v5, :cond_5

    .line 769
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exdates:[J

    .local v4, "arr$":[J
    array-length v0, v4

    move/from16 v32, v0

    .restart local v32    # "len$":I
    const/16 v30, 0x0

    .restart local v30    # "i$":I
    :goto_4
    move/from16 v0, v30

    move/from16 v1, v32

    if-ge v0, v1, :cond_5

    aget-wide v24, v4, v30

    .line 772
    .restart local v24    # "dt":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    move-wide/from16 v0, v24

    invoke-virtual {v5, v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(J)V

    .line 773
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    invoke-static {v5}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v26

    .line 774
    .restart local v26    # "dtvalue":J
    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v13, v5}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 769
    add-int/lit8 v30, v30, 0x1

    goto :goto_4

    .line 777
    .end local v4    # "arr$":[J
    .end local v24    # "dt":J
    .end local v26    # "dtvalue":J
    .end local v30    # "i$":I
    .end local v32    # "len$":I
    :cond_5
    invoke-virtual {v13}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 780
    const/4 v5, 0x0

    new-array v14, v5, [J

    .line 797
    :cond_6
    return-object v14

    .line 787
    :cond_7
    invoke-virtual {v13}, Ljava/util/TreeSet;->size()I

    move-result v31

    .line 788
    .local v31, "len":I
    move/from16 v0, v31

    new-array v14, v0, [J

    .line 789
    .local v14, "dates":[J
    const/16 v28, 0x0

    .line 791
    .local v28, "i":I
    new-instance v33, Landroid/text/format/Time;

    invoke-direct/range {v33 .. v33}, Landroid/text/format/Time;-><init>()V

    .line 792
    .local v33, "time":Landroid/text/format/Time;
    invoke-virtual/range {v33 .. v34}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 793
    invoke-virtual {v13}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v30

    .local v30, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/Long;

    .line 794
    .local v35, "val":Ljava/lang/Long;
    invoke-virtual/range {v35 .. v35}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, v33

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->setTimeFromLongValue(Landroid/text/format/Time;J)V

    .line 795
    add-int/lit8 v29, v28, 0x1

    .end local v28    # "i":I
    .local v29, "i":I
    const/4 v5, 0x1

    move-object/from16 v0, v33

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v18

    aput-wide v18, v14, v28

    move/from16 v28, v29

    .line 796
    .end local v29    # "i":I
    .restart local v28    # "i":I
    goto :goto_5
.end method

.method public getLastOccurence(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;)J
    .locals 22
    .param p1, "dtstart"    # Landroid/text/format/Time;
    .param p2, "recur"    # Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/calendar/calendarcommon/DateException;
        }
    .end annotation

    .prologue
    .line 79
    const-wide/16 v16, -0x1

    .line 80
    .local v16, "lastTime":J
    const/4 v11, 0x0

    .line 84
    .local v11, "hasCount":Z
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    if-eqz v3, :cond_6

    .line 85
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .local v2, "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    array-length v15, v2

    .local v15, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_0
    if-ge v14, v15, :cond_3

    aget-object v18, v2, v14

    .line 86
    .local v18, "rrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    move-object/from16 v0, v18

    iget v3, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->count:I

    if-eqz v3, :cond_1

    .line 87
    const/4 v11, 0x1

    .line 85
    :cond_0
    :goto_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 88
    :cond_1
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->until:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 90
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->until:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->parse(Ljava/lang/String;)Z

    .line 91
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->mIterator:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->toMillis(Z)J

    move-result-wide v20

    .line 92
    .local v20, "untilTime":J
    cmp-long v3, v20, v16

    if-lez v3, :cond_0

    .line 93
    move-wide/from16 v16, v20

    goto :goto_1

    .line 98
    .end local v20    # "untilTime":J
    :cond_2
    const-wide/16 v4, -0x1

    .line 141
    .end local v2    # "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    .end local v18    # "rrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    :goto_2
    return-wide v4

    .line 101
    .restart local v2    # "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .restart local v14    # "i$":I
    .restart local v15    # "len$":I
    :cond_3
    const-wide/16 v4, -0x1

    cmp-long v3, v16, v4

    if-eqz v3, :cond_5

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    if-eqz v3, :cond_5

    .line 102
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    .local v2, "arr$":[J
    array-length v15, v2

    const/4 v14, 0x0

    :goto_3
    if-ge v14, v15, :cond_5

    aget-wide v12, v2, v14

    .line 103
    .local v12, "dt":J
    cmp-long v3, v12, v16

    if-lez v3, :cond_4

    .line 104
    move-wide/from16 v16, v12

    .line 102
    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 111
    .end local v2    # "arr$":[J
    .end local v12    # "dt":J
    :cond_5
    const-wide/16 v4, -0x1

    cmp-long v3, v16, v4

    if-eqz v3, :cond_9

    if-nez v11, :cond_9

    move-wide/from16 v4, v16

    .line 112
    goto :goto_2

    .line 114
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    :cond_6
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    if-eqz v3, :cond_9

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    if-nez v3, :cond_9

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exdates:[J

    if-nez v3, :cond_9

    .line 117
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    .restart local v2    # "arr$":[J
    array-length v15, v2

    .restart local v15    # "len$":I
    const/4 v14, 0x0

    .restart local v14    # "i$":I
    :goto_4
    if-ge v14, v15, :cond_8

    aget-wide v12, v2, v14

    .line 118
    .restart local v12    # "dt":J
    cmp-long v3, v12, v16

    if-lez v3, :cond_7

    .line 119
    move-wide/from16 v16, v12

    .line 117
    :cond_7
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    .end local v12    # "dt":J
    :cond_8
    move-wide/from16 v4, v16

    .line 122
    goto :goto_2

    .line 127
    .end local v2    # "arr$":[J
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    :cond_9
    if-nez v11, :cond_a

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    if-eqz v3, :cond_c

    .line 130
    :cond_a
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    const-wide/16 v8, -0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;JJ)[J

    move-result-object v10

    .line 136
    .local v10, "dates":[J
    array-length v3, v10

    if-nez v3, :cond_b

    .line 137
    const-wide/16 v4, 0x0

    goto :goto_2

    .line 139
    :cond_b
    array-length v3, v10

    add-int/lit8 v3, v3, -0x1

    aget-wide v4, v10, v3

    goto :goto_2

    .line 141
    .end local v10    # "dates":[J
    :cond_c
    const-wide/16 v4, -0x1

    goto :goto_2
.end method
