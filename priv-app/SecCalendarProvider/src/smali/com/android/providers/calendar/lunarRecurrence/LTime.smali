.class public final Lcom/android/providers/calendar/lunarRecurrence/LTime;
.super Landroid/text/format/Time;
.source "LTime.java"


# instance fields
.field public inLunar:Z

.field public isLeapMonth:Z

.field private mCSCFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Landroid/text/format/Time;-><init>()V

    .line 24
    iput-boolean v0, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    .line 25
    iput-boolean v0, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    .line 30
    invoke-static {}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getInstance()Lcom/android/calendar/secfeature/SECCalendarFeatures;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->mCSCFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/text/format/Time;)V
    .locals 1
    .param p1, "other"    # Landroid/text/format/Time;

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 24
    iput-boolean v0, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    .line 25
    iput-boolean v0, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    .line 40
    invoke-static {}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getInstance()Lcom/android/calendar/secfeature/SECCalendarFeatures;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->mCSCFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "timezone"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 24
    iput-boolean v0, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    .line 25
    iput-boolean v0, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    .line 35
    invoke-static {}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getInstance()Lcom/android/calendar/secfeature/SECCalendarFeatures;

    move-result-object v0

    iput-object v0, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->mCSCFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    .line 36
    return-void
.end method


# virtual methods
.method public getActualMaximum(I)I
    .locals 5
    .param p1, "field"    # I

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    .local v0, "result":I
    iget-boolean v1, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    if-eq p1, v1, :cond_1

    .line 47
    :cond_0
    invoke-super {p0, p1}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v0

    .line 52
    :goto_0
    return v0

    .line 49
    :cond_1
    iget-object v1, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->mCSCFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getSolarLunarConverter()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    move-result-object v1

    iget v2, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->year:I

    iget v3, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->month:I

    iget-boolean v4, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getDayLengthOf(IIZ)I

    move-result v0

    goto :goto_0
.end method

.method public set(Landroid/text/format/Time;)V
    .locals 2
    .param p1, "that"    # Landroid/text/format/Time;

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 59
    instance-of v1, p1, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 60
    check-cast v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .line 61
    .local v0, "lthat":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    iget-boolean v1, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    iput-boolean v1, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    .line 62
    iget-boolean v1, v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    iput-boolean v1, p0, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    .line 64
    .end local v0    # "lthat":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    :cond_0
    return-void
.end method
