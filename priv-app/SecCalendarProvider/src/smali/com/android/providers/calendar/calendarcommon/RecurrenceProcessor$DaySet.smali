.class public Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;
.super Ljava/lang/Object;
.source "RecurrenceProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DaySet"
.end annotation


# instance fields
.field private mDays:I

.field private mMonth:I

.field private mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

.field private mTime:Landroid/text/format/Time;

.field private mYear:I


# direct methods
.method public constructor <init>(Z)V
    .locals 2
    .param p1, "zulu"    # Z

    .prologue
    .line 439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mTime:Landroid/text/format/Time;

    .line 441
    return-void
.end method

.method private static generateDaysList(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/EventRecurrence;)I
    .locals 13
    .param p0, "generated"    # Landroid/text/format/Time;
    .param p1, "r"    # Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .prologue
    const/4 v12, 0x1

    .line 697
    const/4 v4, 0x0

    .line 705
    .local v4, "days":I
    const/4 v10, 0x4

    invoke-virtual {p0, v10}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v8

    .line 708
    .local v8, "lastDayThisMonth":I
    iget v3, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    .line 709
    .local v3, "count":I
    if-lez v3, :cond_7

    .line 711
    iget v7, p0, Landroid/text/format/Time;->monthDay:I

    .line 712
    .local v7, "j":I
    :goto_0
    const/16 v10, 0x8

    if-lt v7, v10, :cond_0

    .line 713
    add-int/lit8 v7, v7, -0x7

    goto :goto_0

    .line 715
    :cond_0
    iget v5, p0, Landroid/text/format/Time;->weekDay:I

    .line 716
    .local v5, "first":I
    if-lt v5, v7, :cond_2

    .line 717
    sub-int v10, v5, v7

    add-int/lit8 v5, v10, 0x1

    .line 726
    :goto_1
    iget-object v0, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byday:[I

    .line 727
    .local v0, "byday":[I
    iget-object v1, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayNum:[I

    .line 728
    .local v1, "bydayNum":[I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    if-ge v6, v3, :cond_7

    .line 729
    aget v9, v1, v6

    .line 730
    .local v9, "v":I
    aget v10, v0, v6

    invoke-static {v10}, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->day2TimeDay(I)I

    move-result v10

    sub-int/2addr v10, v5

    add-int/lit8 v7, v10, 0x1

    .line 731
    if-gtz v7, :cond_1

    .line 732
    add-int/lit8 v7, v7, 0x7

    .line 734
    :cond_1
    if-nez v9, :cond_3

    .line 736
    :goto_3
    if-gt v7, v8, :cond_4

    .line 739
    shl-int v10, v12, v7

    or-int/2addr v4, v10

    .line 736
    add-int/lit8 v7, v7, 0x7

    goto :goto_3

    .line 719
    .end local v0    # "byday":[I
    .end local v1    # "bydayNum":[I
    .end local v6    # "i":I
    .end local v9    # "v":I
    :cond_2
    sub-int v10, v5, v7

    add-int/lit8 v5, v10, 0x8

    goto :goto_1

    .line 742
    .restart local v0    # "byday":[I
    .restart local v1    # "bydayNum":[I
    .restart local v6    # "i":I
    .restart local v9    # "v":I
    :cond_3
    if-lez v9, :cond_5

    .line 745
    add-int/lit8 v10, v9, -0x1

    mul-int/lit8 v10, v10, 0x7

    add-int/2addr v7, v10

    .line 746
    if-gt v7, v8, :cond_4

    .line 750
    shl-int v10, v12, v7

    or-int/2addr v4, v10

    .line 728
    :cond_4
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 756
    :cond_5
    :goto_5
    if-gt v7, v8, :cond_6

    add-int/lit8 v7, v7, 0x7

    goto :goto_5

    .line 762
    :cond_6
    mul-int/lit8 v10, v9, 0x7

    add-int/2addr v7, v10

    .line 763
    if-lt v7, v12, :cond_4

    .line 766
    shl-int v10, v12, v7

    or-int/2addr v4, v10

    goto :goto_4

    .line 776
    .end local v0    # "byday":[I
    .end local v1    # "bydayNum":[I
    .end local v5    # "first":I
    .end local v6    # "i":I
    .end local v7    # "j":I
    .end local v9    # "v":I
    :cond_7
    iget v10, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    const/4 v11, 0x5

    if-le v10, v11, :cond_e

    .line 777
    iget v3, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    .line 778
    if-eqz v3, :cond_e

    .line 779
    iget-object v2, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthday:[I

    .line 780
    .local v2, "bymonthday":[I
    iget v10, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    if-nez v10, :cond_a

    .line 781
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_6
    if-ge v6, v3, :cond_e

    .line 782
    aget v9, v2, v6

    .line 783
    .restart local v9    # "v":I
    if-ltz v9, :cond_9

    .line 784
    shl-int v10, v12, v9

    or-int/2addr v4, v10

    .line 781
    :cond_8
    :goto_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 786
    :cond_9
    add-int v10, v8, v9

    add-int/lit8 v7, v10, 0x1

    .line 787
    .restart local v7    # "j":I
    if-lt v7, v12, :cond_8

    if-gt v7, v8, :cond_8

    .line 788
    shl-int v10, v12, v7

    or-int/2addr v4, v10

    goto :goto_7

    .line 795
    .end local v6    # "i":I
    .end local v7    # "j":I
    .end local v9    # "v":I
    :cond_a
    const/4 v7, 0x1

    .restart local v7    # "j":I
    :goto_8
    if-gt v7, v8, :cond_e

    .line 797
    shl-int v10, v12, v7

    and-int/2addr v10, v4

    if-eqz v10, :cond_b

    .line 798
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_9
    if-ge v6, v3, :cond_d

    .line 799
    aget v10, v2, v6

    if-ne v10, v7, :cond_c

    .line 795
    .end local v6    # "i":I
    :cond_b
    :goto_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 798
    .restart local v6    # "i":I
    :cond_c
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    .line 803
    :cond_d
    shl-int v10, v12, v7

    xor-int/lit8 v10, v10, -0x1

    and-int/2addr v4, v10

    goto :goto_a

    .line 810
    .end local v2    # "bymonthday":[I
    .end local v6    # "i":I
    .end local v7    # "j":I
    :cond_e
    return v4
.end method


# virtual methods
.method get(Landroid/text/format/Time;I)Z
    .locals 6
    .param p1, "iterator"    # Landroid/text/format/Time;
    .param p2, "day"    # I

    .prologue
    const/4 v3, 0x1

    .line 633
    iget v1, p1, Landroid/text/format/Time;->year:I

    .line 634
    .local v1, "realYear":I
    iget v0, p1, Landroid/text/format/Time;->month:I

    .line 636
    .local v0, "realMonth":I
    const/4 v2, 0x0

    .line 644
    .local v2, "t":Landroid/text/format/Time;
    if-lt p2, v3, :cond_0

    const/16 v4, 0x1c

    if-le p2, v4, :cond_1

    .line 646
    :cond_0
    iget-object v2, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mTime:Landroid/text/format/Time;

    .line 647
    invoke-virtual {v2, p2, v0, v1}, Landroid/text/format/Time;->set(III)V

    .line 648
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 649
    iget v1, v2, Landroid/text/format/Time;->year:I

    .line 650
    iget v0, v2, Landroid/text/format/Time;->month:I

    .line 651
    iget p2, v2, Landroid/text/format/Time;->monthDay:I

    .line 664
    :cond_1
    iget v4, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mYear:I

    if-ne v1, v4, :cond_2

    iget v4, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mMonth:I

    if-eq v0, v4, :cond_4

    .line 665
    :cond_2
    if-nez v2, :cond_3

    .line 666
    iget-object v2, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mTime:Landroid/text/format/Time;

    .line 667
    invoke-virtual {v2, p2, v0, v1}, Landroid/text/format/Time;->set(III)V

    .line 668
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 676
    :cond_3
    iput v1, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mYear:I

    .line 677
    iput v0, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mMonth:I

    .line 678
    iget-object v4, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    invoke-static {v2, v4}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->generateDaysList(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/EventRecurrence;)I

    move-result v4

    iput v4, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mDays:I

    .line 683
    :cond_4
    iget v4, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mDays:I

    shl-int v5, v3, p2

    and-int/2addr v4, v5

    if-eqz v4, :cond_5

    :goto_0
    return v3

    :cond_5
    const/4 v3, 0x0

    goto :goto_0
.end method

.method getfirstweekend(Landroid/text/format/Time;)I
    .locals 3
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    .line 532
    const/4 v0, 0x1

    .line 533
    .local v0, "day":I
    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-eqz v1, :cond_0

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    .line 534
    :cond_0
    const/4 v0, 0x1

    .line 538
    :goto_0
    return v0

    .line 536
    :cond_1
    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    rsub-int/lit8 v0, v1, 0x7

    goto :goto_0
.end method

.method getfourthweekend(Landroid/text/format/Time;)I
    .locals 4
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    .line 570
    const/4 v0, 0x1

    .line 571
    .local v0, "day":I
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getthirdweekend(Landroid/text/format/Time;)I

    move-result v0

    .line 572
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 573
    .local v1, "time1":Landroid/text/format/Time;
    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 574
    invoke-static {v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 575
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 576
    add-int/lit8 v0, v0, 0x1

    .line 580
    :cond_0
    :goto_0
    return v0

    .line 577
    :cond_1
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-nez v2, :cond_0

    .line 578
    add-int/lit8 v0, v0, 0x6

    goto :goto_0
.end method

.method getlastday(Landroid/text/format/Time;)I
    .locals 2
    .param p1, "iterator"    # Landroid/text/format/Time;

    .prologue
    .line 627
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v0

    .line 628
    .local v0, "day":I
    return v0
.end method

.method getsecondweekend(Landroid/text/format/Time;)I
    .locals 4
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    .line 542
    const/4 v0, 0x1

    .line 543
    .local v0, "day":I
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getfirstweekend(Landroid/text/format/Time;)I

    move-result v0

    .line 544
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 545
    .local v1, "time1":Landroid/text/format/Time;
    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 546
    invoke-static {v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 547
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 548
    add-int/lit8 v0, v0, 0x1

    .line 552
    :cond_0
    :goto_0
    return v0

    .line 549
    :cond_1
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-nez v2, :cond_0

    .line 550
    add-int/lit8 v0, v0, 0x6

    goto :goto_0
.end method

.method getthirdweekend(Landroid/text/format/Time;)I
    .locals 4
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    .line 556
    const/4 v0, 0x1

    .line 557
    .local v0, "day":I
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getsecondweekend(Landroid/text/format/Time;)I

    move-result v0

    .line 558
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 559
    .local v1, "time1":Landroid/text/format/Time;
    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 560
    invoke-static {v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 561
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 562
    add-int/lit8 v0, v0, 0x1

    .line 566
    :cond_0
    :goto_0
    return v0

    .line 563
    :cond_1
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-nez v2, :cond_0

    .line 564
    add-int/lit8 v0, v0, 0x6

    goto :goto_0
.end method

.method getweek_day(Landroid/text/format/Time;)I
    .locals 10
    .param p1, "iterator"    # Landroid/text/format/Time;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x6

    const/4 v5, 0x1

    .line 451
    const/4 v0, 0x1

    .line 452
    .local v0, "day":I
    iget-object v3, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v7

    if-ne v3, v5, :cond_3

    .line 453
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 454
    .local v2, "time":Landroid/text/format/Time;
    iput v5, v2, Landroid/text/format/Time;->monthDay:I

    .line 455
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 456
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eqz v3, :cond_1

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v6, :cond_1

    .line 457
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    .line 528
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_0
    :goto_0
    return v0

    .line 459
    .restart local v2    # "time":Landroid/text/format/Time;
    :cond_1
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-nez v3, :cond_2

    .line 460
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x1

    goto :goto_0

    .line 461
    :cond_2
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v6, :cond_0

    .line 462
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x2

    goto :goto_0

    .line 464
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_3
    iget-object v3, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v7

    if-ne v3, v9, :cond_8

    .line 465
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 466
    .restart local v2    # "time":Landroid/text/format/Time;
    iput v5, v2, Landroid/text/format/Time;->monthDay:I

    .line 467
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 468
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v5, :cond_4

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v9, :cond_4

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v8, :cond_5

    .line 470
    :cond_4
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x1

    goto :goto_0

    .line 472
    :cond_5
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_6

    .line 473
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x3

    goto :goto_0

    .line 474
    :cond_6
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v6, :cond_7

    .line 475
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x3

    goto :goto_0

    .line 476
    :cond_7
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-nez v3, :cond_0

    .line 477
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x2

    goto :goto_0

    .line 479
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_8
    iget-object v3, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v7

    const/4 v4, 0x3

    if-ne v3, v4, :cond_e

    .line 480
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 481
    .restart local v2    # "time":Landroid/text/format/Time;
    iput v5, v2, Landroid/text/format/Time;->monthDay:I

    .line 482
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 483
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v5, :cond_9

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v9, :cond_9

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_a

    .line 484
    :cond_9
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x2

    goto :goto_0

    .line 486
    :cond_a
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v8, :cond_b

    .line 487
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x4

    goto/16 :goto_0

    .line 488
    :cond_b
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_c

    .line 489
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x4

    goto/16 :goto_0

    .line 490
    :cond_c
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v6, :cond_d

    .line 491
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x4

    goto/16 :goto_0

    .line 492
    :cond_d
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-nez v3, :cond_0

    .line 493
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x3

    goto/16 :goto_0

    .line 495
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_e
    iget-object v3, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v7

    if-ne v3, v8, :cond_15

    .line 496
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 497
    .restart local v2    # "time":Landroid/text/format/Time;
    iput v5, v2, Landroid/text/format/Time;->monthDay:I

    .line 498
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 499
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v5, :cond_f

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v9, :cond_10

    .line 500
    :cond_f
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x3

    goto/16 :goto_0

    .line 502
    :cond_10
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_11

    .line 503
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x5

    goto/16 :goto_0

    .line 504
    :cond_11
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v8, :cond_12

    .line 505
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x5

    goto/16 :goto_0

    .line 506
    :cond_12
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_13

    .line 507
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x5

    goto/16 :goto_0

    .line 508
    :cond_13
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v6, :cond_14

    .line 509
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x5

    goto/16 :goto_0

    .line 510
    :cond_14
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-nez v3, :cond_0

    .line 511
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x4

    goto/16 :goto_0

    .line 513
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_15
    iget-object v3, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v7

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 514
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 516
    .restart local v2    # "time":Landroid/text/format/Time;
    invoke-virtual {p1, v8}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v1

    .line 517
    .local v1, "lastday":I
    iput v1, v2, Landroid/text/format/Time;->monthDay:I

    .line 518
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 519
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eqz v3, :cond_16

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v6, :cond_16

    .line 520
    move v0, v1

    goto/16 :goto_0

    .line 522
    :cond_16
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-nez v3, :cond_17

    .line 523
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, -0x2

    goto/16 :goto_0

    .line 524
    :cond_17
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v6, :cond_0

    .line 525
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, -0x1

    goto/16 :goto_0
.end method

.method getweekend_day(Landroid/text/format/Time;)I
    .locals 8
    .param p1, "iterator"    # Landroid/text/format/Time;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 584
    const/4 v0, 0x1

    .line 585
    .local v0, "day":I
    iget-object v3, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v5

    if-ne v3, v6, :cond_1

    .line 586
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 587
    .local v2, "time":Landroid/text/format/Time;
    iput v6, v2, Landroid/text/format/Time;->monthDay:I

    .line 588
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 589
    invoke-virtual {p0, v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getfirstweekend(Landroid/text/format/Time;)I

    move-result v0

    .line 623
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_0
    :goto_0
    return v0

    .line 591
    :cond_1
    iget-object v3, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v5

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 592
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 593
    .restart local v2    # "time":Landroid/text/format/Time;
    iput v6, v2, Landroid/text/format/Time;->monthDay:I

    .line 594
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 595
    invoke-virtual {p0, v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getsecondweekend(Landroid/text/format/Time;)I

    move-result v0

    .line 597
    goto :goto_0

    .end local v2    # "time":Landroid/text/format/Time;
    :cond_2
    iget-object v3, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v5

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    .line 598
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 599
    .restart local v2    # "time":Landroid/text/format/Time;
    iput v6, v2, Landroid/text/format/Time;->monthDay:I

    .line 600
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 601
    invoke-virtual {p0, v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getthirdweekend(Landroid/text/format/Time;)I

    move-result v0

    .line 603
    goto :goto_0

    .end local v2    # "time":Landroid/text/format/Time;
    :cond_3
    iget-object v3, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v5

    if-ne v3, v7, :cond_4

    .line 604
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 605
    .restart local v2    # "time":Landroid/text/format/Time;
    iput v6, v2, Landroid/text/format/Time;->monthDay:I

    .line 606
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 607
    invoke-virtual {p0, v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getfourthweekend(Landroid/text/format/Time;)I

    move-result v0

    .line 609
    goto :goto_0

    .end local v2    # "time":Landroid/text/format/Time;
    :cond_4
    iget-object v3, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v5

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 610
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 612
    .restart local v2    # "time":Landroid/text/format/Time;
    invoke-virtual {p1, v7}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v1

    .line 613
    .local v1, "lastday":I
    iput v1, v2, Landroid/text/format/Time;->monthDay:I

    .line 614
    invoke-static {v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 615
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eqz v3, :cond_5

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_6

    .line 616
    :cond_5
    move v0, v1

    goto :goto_0

    .line 618
    :cond_6
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    sub-int v0, v1, v3

    goto :goto_0
.end method

.method setRecurrence(Lcom/android/providers/calendar/calendarcommon/EventRecurrence;)V
    .locals 1
    .param p1, "r"    # Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .prologue
    .line 445
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mYear:I

    .line 446
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mMonth:I

    .line 447
    iput-object p1, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .line 448
    return-void
.end method
