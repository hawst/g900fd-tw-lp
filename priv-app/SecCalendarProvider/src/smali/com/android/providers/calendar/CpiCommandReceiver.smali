.class public Lcom/android/providers/calendar/CpiCommandReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CpiCommandReceiver.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getCalendarDBSize()Ljava/lang/String;
    .locals 8

    .prologue
    .line 56
    const-wide/16 v4, 0x0

    .line 58
    .local v4, "size":J
    const-string v0, "/data/data/com.android.providers.calendar/databases/calendar.db"

    .line 59
    .local v0, "dbpath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 61
    const-wide/16 v6, 0x400

    div-long v2, v4, v6

    .line 62
    .local v2, "kilobytes":J
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private sendResponse()V
    .locals 3

    .prologue
    .line 66
    const-string v1, "CpiCommandReceiver"

    const-string v2, "sendResponse()"

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BCS_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 68
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "response"

    invoke-direct {p0}, Lcom/android/providers/calendar/CpiCommandReceiver;->getCalendarDBSize()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    iget-object v1, p0, Lcom/android/providers/calendar/CpiCommandReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 70
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 36
    const-string v2, "CpiCommandReceiver"

    const-string v3, "Received android.intent.action.BCS_REQUEST"

    invoke-static {v2, v3}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    iput-object p1, p0, Lcom/android/providers/calendar/CpiCommandReceiver;->mContext:Landroid/content/Context;

    .line 38
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 40
    .local v1, "data":Landroid/os/Bundle;
    if-nez v1, :cond_0

    .line 41
    const-string v2, "CpiCommandReceiver"

    const-string v3, "There is no extras"

    invoke-static {v2, v3}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :goto_0
    return-void

    .line 45
    :cond_0
    const-string v2, "command"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "command":Ljava/lang/String;
    const-string v2, "CpiCommandReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "android.intent.action.BCS_REQUEST : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const-string v2, "AT+CCALD=SZ"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 49
    invoke-direct {p0}, Lcom/android/providers/calendar/CpiCommandReceiver;->sendResponse()V

    goto :goto_0

    .line 51
    :cond_1
    const-string v2, "CpiCommandReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invaild Command : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
