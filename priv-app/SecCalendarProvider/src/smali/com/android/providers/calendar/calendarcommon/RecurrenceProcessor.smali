.class public Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;
.super Ljava/lang/Object;
.source "RecurrenceProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;
    }
.end annotation


# static fields
.field private static final DAYS_IN_YEAR_PRECEDING_MONTH:[I

.field private static final DAYS_PER_MONTH:[I


# instance fields
.field private Monthly_Lastday:Z

.field private Monthly_week_day:Z

.field private Monthly_weekend:Z

.field private mDays:Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;

.field private mGenerated:Landroid/text/format/Time;

.field private mIterator:Landroid/text/format/Time;

.field private mStringBuilder:Ljava/lang/StringBuilder;

.field private mUntil:Landroid/text/format/Time;

.field private yearly_anyday:Z

.field private yearly_first_second_third_fourth_day_of_month:Z

.field private yearly_lastday_of_month:Z

.field private yearly_weekday:Z

.field private yearly_weekend:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xc

    .line 1601
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->DAYS_PER_MONTH:[I

    .line 1603
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->DAYS_IN_YEAR_PRECEDING_MONTH:[I

    return-void

    .line 1601
    nop

    :array_0
    .array-data 4
        0x1f
        0x1c
        0x1f
        0x1e
        0x1f
        0x1e
        0x1f
        0x1f
        0x1e
        0x1f
        0x1e
        0x1f
    .end array-data

    .line 1603
    :array_1
    .array-data 4
        0x0
        0x1f
        0x3b
        0x5a
        0x78
        0x97
        0xb4
        0xd4
        0xf3
        0x111
        0x130
        0x14e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    .line 29
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mUntil:Landroid/text/format/Time;

    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mStringBuilder:Ljava/lang/StringBuilder;

    .line 31
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mGenerated:Landroid/text/format/Time;

    .line 32
    new-instance v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;

    invoke-direct {v0, v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;-><init>(Z)V

    iput-object v0, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mDays:Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;

    .line 34
    iput-boolean v2, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_Lastday:Z

    .line 35
    iput-boolean v2, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_weekend:Z

    .line 36
    iput-boolean v2, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_week_day:Z

    .line 37
    iput-boolean v2, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_anyday:Z

    .line 38
    iput-boolean v2, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_weekday:Z

    .line 39
    iput-boolean v2, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_weekend:Z

    .line 40
    iput-boolean v2, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_first_second_third_fourth_day_of_month:Z

    .line 41
    iput-boolean v2, p0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_lastday_of_month:Z

    .line 52
    return-void
.end method

.method private static filter(Lcom/android/providers/calendar/calendarcommon/EventRecurrence;Landroid/text/format/Time;)I
    .locals 10
    .param p0, "r"    # Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .param p1, "iterator"    # Landroid/text/format/Time;

    .prologue
    .line 218
    iget v3, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    .line 220
    .local v3, "freq":I
    const/4 v6, 0x6

    if-lt v6, v3, :cond_0

    .line 222
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthCount:I

    if-lez v6, :cond_0

    .line 223
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonth:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthCount:I

    iget v8, p1, Landroid/text/format/Time;->month:I

    add-int/lit8 v8, v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->listContains([III)Z

    move-result v2

    .line 225
    .local v2, "found":Z
    if-nez v2, :cond_0

    .line 226
    const/4 v6, 0x1

    .line 330
    .end local v2    # "found":Z
    :goto_0
    return v6

    .line 230
    :cond_0
    const/4 v6, 0x5

    if-lt v6, v3, :cond_1

    .line 233
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byweeknoCount:I

    if-lez v6, :cond_1

    .line 234
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byweekno:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byweeknoCount:I

    invoke-virtual {p1}, Landroid/text/format/Time;->getWeekNumber()I

    move-result v8

    const/16 v9, 0x9

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->listContains([IIII)Z

    move-result v2

    .line 237
    .restart local v2    # "found":Z
    if-nez v2, :cond_1

    .line 238
    const/4 v6, 0x2

    goto :goto_0

    .line 242
    .end local v2    # "found":Z
    :cond_1
    const/4 v6, 0x4

    if-lt v6, v3, :cond_4

    .line 244
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byyeardayCount:I

    if-lez v6, :cond_2

    .line 245
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byyearday:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byyeardayCount:I

    iget v8, p1, Landroid/text/format/Time;->yearDay:I

    const/16 v9, 0x8

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->listContains([IIII)Z

    move-result v2

    .line 247
    .restart local v2    # "found":Z
    if-nez v2, :cond_2

    .line 248
    const/4 v6, 0x3

    goto :goto_0

    .line 252
    .end local v2    # "found":Z
    :cond_2
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    if-lez v6, :cond_3

    .line 253
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthday:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    iget v8, p1, Landroid/text/format/Time;->monthDay:I

    const/4 v9, 0x4

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->listContains([IIII)Z

    move-result v2

    .line 256
    .restart local v2    # "found":Z
    if-nez v2, :cond_3

    .line 257
    const/4 v6, 0x4

    goto :goto_0

    .line 263
    .end local v2    # "found":Z
    :cond_3
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    if-lez v6, :cond_4

    .line 264
    iget-object v1, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byday:[I

    .line 265
    .local v1, "a":[I
    iget v0, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    .line 266
    .local v0, "N":I
    iget v6, p1, Landroid/text/format/Time;->weekDay:I

    invoke-static {v6}, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->timeDay2Day(I)I

    move-result v5

    .line 267
    .local v5, "v":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v0, :cond_6

    .line 268
    aget v6, v1, v4

    if-ne v6, v5, :cond_5

    .line 275
    .end local v0    # "N":I
    .end local v1    # "a":[I
    .end local v4    # "i":I
    .end local v5    # "v":I
    :cond_4
    const/4 v6, 0x3

    if-lt v6, v3, :cond_7

    .line 277
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byhour:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byhourCount:I

    iget v8, p1, Landroid/text/format/Time;->hour:I

    const/4 v9, 0x3

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->listContains([IIII)Z

    move-result v2

    .line 280
    .restart local v2    # "found":Z
    if-nez v2, :cond_7

    .line 281
    const/4 v6, 0x6

    goto :goto_0

    .line 267
    .end local v2    # "found":Z
    .restart local v0    # "N":I
    .restart local v1    # "a":[I
    .restart local v4    # "i":I
    .restart local v5    # "v":I
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 272
    :cond_6
    const/4 v6, 0x5

    goto :goto_0

    .line 284
    .end local v0    # "N":I
    .end local v1    # "a":[I
    .end local v4    # "i":I
    .end local v5    # "v":I
    :cond_7
    const/4 v6, 0x2

    if-lt v6, v3, :cond_8

    .line 286
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byminute:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byminuteCount:I

    iget v8, p1, Landroid/text/format/Time;->minute:I

    const/4 v9, 0x2

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->listContains([IIII)Z

    move-result v2

    .line 289
    .restart local v2    # "found":Z
    if-nez v2, :cond_8

    .line 290
    const/4 v6, 0x7

    goto/16 :goto_0

    .line 293
    .end local v2    # "found":Z
    :cond_8
    const/4 v6, 0x1

    if-lt v6, v3, :cond_9

    .line 295
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysecond:[I

    iget v7, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysecondCount:I

    iget v8, p1, Landroid/text/format/Time;->second:I

    const/4 v9, 0x1

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->listContains([IIII)Z

    move-result v2

    .line 298
    .restart local v2    # "found":Z
    if-nez v2, :cond_9

    .line 299
    const/16 v6, 0x8

    goto/16 :goto_0

    .line 303
    .end local v2    # "found":Z
    :cond_9
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    if-lez v6, :cond_a

    .line 306
    const/4 v6, 0x6

    if-ne v3, v6, :cond_d

    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    if-lez v6, :cond_d

    .line 308
    iget v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    add-int/lit8 v4, v6, -0x1

    .restart local v4    # "i":I
    :goto_2
    if-ltz v4, :cond_c

    .line 309
    iget-object v6, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayNum:[I

    aget v6, v6, v4

    if-eqz v6, :cond_b

    .line 310
    const-string v6, "RecurrenceProcessor"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 311
    const-string v6, "RecurrenceProcessor"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BYSETPOS not supported with these rules: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/providers/calendar/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    .end local v4    # "i":I
    :cond_a
    :goto_3
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 308
    .restart local v4    # "i":I
    :cond_b
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 316
    :cond_c
    invoke-static {p0, p1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->filterMonthlySetPos(Lcom/android/providers/calendar/calendarcommon/EventRecurrence;Landroid/text/format/Time;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 318
    const/16 v6, 0x9

    goto/16 :goto_0

    .line 321
    .end local v4    # "i":I
    :cond_d
    const-string v6, "RecurrenceProcessor"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 322
    const-string v6, "RecurrenceProcessor"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BYSETPOS not supported with these rules: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/providers/calendar/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private static filterMonthlySetPos(Lcom/android/providers/calendar/calendarcommon/EventRecurrence;Landroid/text/format/Time;)Z
    .locals 14
    .param p0, "r"    # Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .param p1, "instance"    # Landroid/text/format/Time;

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 349
    iget v12, p1, Landroid/text/format/Time;->weekDay:I

    iget v13, p1, Landroid/text/format/Time;->monthDay:I

    sub-int/2addr v12, v13

    add-int/lit8 v12, v12, 0x24

    rem-int/lit8 v5, v12, 0x7

    .line 355
    .local v5, "dotw":I
    const/4 v0, 0x0

    .line 356
    .local v0, "bydayMask":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget v12, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    if-ge v6, v12, :cond_0

    .line 357
    iget-object v12, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byday:[I

    aget v12, v12, v6

    or-int/2addr v0, v12

    .line 356
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 364
    :cond_0
    const/4 v12, 0x4

    invoke-virtual {p1, v12}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v8

    .line 365
    .local v8, "maxDay":I
    new-array v2, v8, [I

    .line 366
    .local v2, "daySet":[I
    const/4 v3, 0x0

    .line 368
    .local v3, "daySetLength":I
    const/4 v9, 0x1

    .local v9, "md":I
    move v4, v3

    .end local v3    # "daySetLength":I
    .local v4, "daySetLength":I
    :goto_1
    if-gt v9, v8, :cond_2

    .line 371
    const/high16 v12, 0x10000

    shl-int v1, v12, v5

    .line 372
    .local v1, "dayBit":I
    and-int v12, v0, v1

    if-eqz v12, :cond_9

    .line 373
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "daySetLength":I
    .restart local v3    # "daySetLength":I
    aput v9, v2, v4

    .line 376
    :goto_2
    add-int/lit8 v5, v5, 0x1

    .line 377
    const/4 v12, 0x7

    if-ne v5, v12, :cond_1

    .line 378
    const/4 v5, 0x0

    .line 368
    :cond_1
    add-int/lit8 v9, v9, 0x1

    move v4, v3

    .end local v3    # "daySetLength":I
    .restart local v4    # "daySetLength":I
    goto :goto_1

    .line 385
    .end local v1    # "dayBit":I
    :cond_2
    iget v12, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    add-int/lit8 v6, v12, -0x1

    :goto_3
    if-ltz v6, :cond_8

    .line 386
    iget-object v12, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    if-eqz v12, :cond_4

    iget-object v12, p0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v7, v12, v6

    .line 387
    .local v7, "index":I
    :goto_4
    if-lez v7, :cond_6

    .line 388
    if-le v7, v4, :cond_5

    .line 385
    :cond_3
    add-int/lit8 v6, v6, -0x1

    goto :goto_3

    .end local v7    # "index":I
    :cond_4
    move v7, v11

    .line 386
    goto :goto_4

    .line 391
    .restart local v7    # "index":I
    :cond_5
    add-int/lit8 v12, v7, -0x1

    aget v12, v2, v12

    iget v13, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v12, v13, :cond_3

    .line 407
    .end local v7    # "index":I
    :goto_5
    return v10

    .line 394
    .restart local v7    # "index":I
    :cond_6
    if-gez v7, :cond_7

    .line 395
    add-int v12, v4, v7

    if-ltz v12, :cond_3

    .line 398
    add-int v12, v4, v7

    aget v12, v2, v12

    iget v13, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v12, v13, :cond_3

    goto :goto_5

    .line 403
    :cond_7
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "invalid bysetpos value"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    .end local v7    # "index":I
    :cond_8
    move v10, v11

    .line 407
    goto :goto_5

    .restart local v1    # "dayBit":I
    :cond_9
    move v3, v4

    .end local v4    # "daySetLength":I
    .restart local v3    # "daySetLength":I
    goto :goto_2
.end method

.method static isLeapYear(I)Z
    .locals 1
    .param p0, "year"    # I

    .prologue
    .line 1588
    rem-int/lit8 v0, p0, 0x4

    if-nez v0, :cond_1

    rem-int/lit8 v0, p0, 0x64

    if-nez v0, :cond_0

    rem-int/lit16 v0, p0, 0x190

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static listContains([III)Z
    .locals 2
    .param p0, "a"    # [I
    .param p1, "N"    # I
    .param p2, "v"    # I

    .prologue
    .line 173
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 174
    aget v1, p0, v0

    if-ne v1, p2, :cond_0

    .line 175
    const/4 v1, 0x1

    .line 178
    :goto_1
    return v1

    .line 173
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 178
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static listContains([IIII)Z
    .locals 3
    .param p0, "a"    # [I
    .param p1, "N"    # I
    .param p2, "v"    # I
    .param p3, "max"    # I

    .prologue
    const/4 v2, 0x1

    .line 191
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_3

    .line 192
    aget v1, p0, v0

    .line 193
    .local v1, "w":I
    if-lez v1, :cond_1

    .line 194
    if-ne v1, p2, :cond_2

    .line 204
    .end local v1    # "w":I
    :cond_0
    :goto_1
    return v2

    .line 198
    .restart local v1    # "w":I
    :cond_1
    add-int/2addr p3, v1

    .line 199
    if-eq p3, p2, :cond_0

    .line 191
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 204
    .end local v1    # "w":I
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method static monthLength(II)I
    .locals 3
    .param p0, "year"    # I
    .param p1, "month"    # I

    .prologue
    const/16 v1, 0x1c

    .line 1614
    sget-object v2, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->DAYS_PER_MONTH:[I

    aget v0, v2, p1

    .line 1615
    .local v0, "n":I
    if-eq v0, v1, :cond_0

    .line 1618
    .end local v0    # "n":I
    :goto_0
    return v0

    .restart local v0    # "n":I
    :cond_0
    invoke-static {p0}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->isLeapYear(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x1d

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static final normDateTimeComparisonValue(Landroid/text/format/Time;)J
    .locals 4
    .param p0, "normalized"    # Landroid/text/format/Time;

    .prologue
    .line 1669
    iget v0, p0, Landroid/text/format/Time;->year:I

    int-to-long v0, v0

    const/16 v2, 0x1a

    shl-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->month:I

    shl-int/lit8 v2, v2, 0x16

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->monthDay:I

    shl-int/lit8 v2, v2, 0x11

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->hour:I

    shl-int/lit8 v2, v2, 0xc

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->minute:I

    shl-int/lit8 v2, v2, 0x6

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->second:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private static final setTimeFromLongValue(Landroid/text/format/Time;J)V
    .locals 3
    .param p0, "date"    # Landroid/text/format/Time;
    .param p1, "val"    # J

    .prologue
    .line 1675
    const/16 v0, 0x1a

    shr-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Landroid/text/format/Time;->year:I

    .line 1676
    const/16 v0, 0x16

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0xf

    iput v0, p0, Landroid/text/format/Time;->month:I

    .line 1677
    const/16 v0, 0x11

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x1f

    iput v0, p0, Landroid/text/format/Time;->monthDay:I

    .line 1678
    const/16 v0, 0xc

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x1f

    iput v0, p0, Landroid/text/format/Time;->hour:I

    .line 1679
    const/4 v0, 0x6

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x3f

    iput v0, p0, Landroid/text/format/Time;->minute:I

    .line 1680
    const-wide/16 v0, 0x3f

    and-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, Landroid/text/format/Time;->second:I

    .line 1681
    return-void
.end method

.method static unsafeNormalize(Landroid/text/format/Time;)V
    .locals 15
    .param p0, "date"    # Landroid/text/format/Time;

    .prologue
    const/16 v14, 0xc

    .line 1505
    iget v9, p0, Landroid/text/format/Time;->second:I

    .line 1506
    .local v9, "second":I
    iget v5, p0, Landroid/text/format/Time;->minute:I

    .line 1507
    .local v5, "minute":I
    iget v4, p0, Landroid/text/format/Time;->hour:I

    .line 1508
    .local v4, "hour":I
    iget v7, p0, Landroid/text/format/Time;->monthDay:I

    .line 1509
    .local v7, "monthDay":I
    iget v6, p0, Landroid/text/format/Time;->month:I

    .line 1510
    .local v6, "month":I
    iget v10, p0, Landroid/text/format/Time;->year:I

    .line 1512
    .local v10, "year":I
    if-gez v9, :cond_0

    add-int/lit8 v13, v9, -0x3b

    :goto_0
    div-int/lit8 v2, v13, 0x3c

    .line 1513
    .local v2, "addMinutes":I
    mul-int/lit8 v13, v2, 0x3c

    sub-int/2addr v9, v13

    .line 1514
    add-int/2addr v5, v2

    .line 1515
    if-gez v5, :cond_1

    add-int/lit8 v13, v5, -0x3b

    :goto_1
    div-int/lit8 v1, v13, 0x3c

    .line 1516
    .local v1, "addHours":I
    mul-int/lit8 v13, v1, 0x3c

    sub-int/2addr v5, v13

    .line 1517
    add-int/2addr v4, v1

    .line 1518
    if-gez v4, :cond_2

    add-int/lit8 v13, v4, -0x17

    :goto_2
    div-int/lit8 v0, v13, 0x18

    .line 1519
    .local v0, "addDays":I
    mul-int/lit8 v13, v0, 0x18

    sub-int/2addr v4, v13

    .line 1520
    add-int/2addr v7, v0

    .line 1525
    :goto_3
    if-gtz v7, :cond_4

    .line 1533
    const/4 v13, 0x1

    if-le v6, v13, :cond_3

    invoke-static {v10}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearLength(I)I

    move-result v3

    .line 1534
    .local v3, "days":I
    :goto_4
    add-int/2addr v7, v3

    .line 1535
    add-int/lit8 v10, v10, -0x1

    .line 1536
    goto :goto_3

    .end local v0    # "addDays":I
    .end local v1    # "addHours":I
    .end local v2    # "addMinutes":I
    .end local v3    # "days":I
    :cond_0
    move v13, v9

    .line 1512
    goto :goto_0

    .restart local v2    # "addMinutes":I
    :cond_1
    move v13, v5

    .line 1515
    goto :goto_1

    .restart local v1    # "addHours":I
    :cond_2
    move v13, v4

    .line 1518
    goto :goto_2

    .line 1533
    .restart local v0    # "addDays":I
    :cond_3
    add-int/lit8 v13, v10, -0x1

    invoke-static {v13}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearLength(I)I

    move-result v3

    goto :goto_4

    .line 1538
    :cond_4
    if-gez v6, :cond_7

    .line 1539
    add-int/lit8 v13, v6, 0x1

    div-int/lit8 v13, v13, 0xc

    add-int/lit8 v12, v13, -0x1

    .line 1540
    .local v12, "years":I
    add-int/2addr v10, v12

    .line 1541
    mul-int/lit8 v13, v12, 0xc

    sub-int/2addr v6, v13

    .line 1551
    .end local v12    # "years":I
    :cond_5
    :goto_5
    if-nez v6, :cond_6

    .line 1552
    invoke-static {v10}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearLength(I)I

    move-result v11

    .line 1553
    .local v11, "yearLength":I
    if-le v7, v11, :cond_6

    .line 1554
    add-int/lit8 v10, v10, 0x1

    .line 1555
    sub-int/2addr v7, v11

    .line 1558
    .end local v11    # "yearLength":I
    :cond_6
    invoke-static {v10, v6}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->monthLength(II)I

    move-result v8

    .line 1559
    .local v8, "monthLength":I
    if-le v7, v8, :cond_8

    .line 1560
    sub-int/2addr v7, v8

    .line 1561
    add-int/lit8 v6, v6, 0x1

    .line 1562
    if-lt v6, v14, :cond_5

    .line 1563
    add-int/lit8 v6, v6, -0xc

    .line 1564
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 1542
    .end local v8    # "monthLength":I
    :cond_7
    if-lt v6, v14, :cond_5

    .line 1543
    div-int/lit8 v12, v6, 0xc

    .line 1544
    .restart local v12    # "years":I
    add-int/2addr v10, v12

    .line 1545
    mul-int/lit8 v13, v12, 0xc

    sub-int/2addr v6, v13

    goto :goto_5

    .line 1571
    .end local v12    # "years":I
    .restart local v8    # "monthLength":I
    :cond_8
    iput v9, p0, Landroid/text/format/Time;->second:I

    .line 1572
    iput v5, p0, Landroid/text/format/Time;->minute:I

    .line 1573
    iput v4, p0, Landroid/text/format/Time;->hour:I

    .line 1574
    iput v7, p0, Landroid/text/format/Time;->monthDay:I

    .line 1575
    iput v6, p0, Landroid/text/format/Time;->month:I

    .line 1576
    iput v10, p0, Landroid/text/format/Time;->year:I

    .line 1577
    invoke-static {v10, v6, v7}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->weekDay(III)I

    move-result v13

    iput v13, p0, Landroid/text/format/Time;->weekDay:I

    .line 1578
    invoke-static {v10, v6, v7}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearDay(III)I

    move-result v13

    iput v13, p0, Landroid/text/format/Time;->yearDay:I

    .line 1579
    return-void
.end method

.method private static useBYX(III)Z
    .locals 1
    .param p0, "freq"    # I
    .param p1, "freqConstant"    # I
    .param p2, "count"    # I

    .prologue
    .line 433
    if-le p0, p1, :cond_0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static weekDay(III)I
    .locals 2
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "day"    # I

    .prologue
    .line 1631
    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    .line 1632
    add-int/lit8 p1, p1, 0xc

    .line 1633
    add-int/lit8 p0, p0, -0x1

    .line 1635
    :cond_0
    mul-int/lit8 v0, p1, 0xd

    add-int/lit8 v0, v0, -0xe

    div-int/lit8 v0, v0, 0x5

    add-int/2addr v0, p2

    add-int/2addr v0, p0

    div-int/lit8 v1, p0, 0x4

    add-int/2addr v0, v1

    div-int/lit8 v1, p0, 0x64

    sub-int/2addr v0, v1

    div-int/lit16 v1, p0, 0x190

    add-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x7

    return v0
.end method

.method static yearDay(III)I
    .locals 2
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "day"    # I

    .prologue
    .line 1647
    sget-object v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->DAYS_IN_YEAR_PRECEDING_MONTH:[I

    aget v1, v1, p1

    add-int/2addr v1, p2

    add-int/lit8 v0, v1, -0x1

    .line 1648
    .local v0, "yearDay":I
    const/4 v1, 0x2

    if-lt p1, v1, :cond_0

    invoke-static {p0}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->isLeapYear(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1649
    add-int/lit8 v0, v0, 0x1

    .line 1651
    :cond_0
    return v0
.end method

.method static yearLength(I)I
    .locals 1
    .param p0, "year"    # I

    .prologue
    .line 1598
    invoke-static {p0}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->isLeapYear(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x16e

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x16d

    goto :goto_0
.end method


# virtual methods
.method public expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/EventRecurrence;JJZLjava/util/TreeSet;)V
    .locals 61
    .param p1, "dtstart"    # Landroid/text/format/Time;
    .param p2, "r"    # Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .param p3, "rangeStartDateValue"    # J
    .param p5, "rangeEndDateValue"    # J
    .param p7, "add"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/format/Time;",
            "Lcom/android/providers/calendar/calendarcommon/EventRecurrence;",
            "JJZ",
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/calendar/calendarcommon/DateException;
        }
    .end annotation

    .prologue
    .line 945
    .local p8, "out":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Long;>;"
    invoke-static/range {p1 .. p1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 946
    invoke-static/range {p1 .. p1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v20

    .line 947
    .local v20, "dtstartDateValue":J
    const/16 v16, 0x0

    .line 972
    .local v16, "count":I
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    move/from16 v57, v0

    const/16 v58, 0x6

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_6

    .line 973
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    if-eqz v57, :cond_1

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_1

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_1

    .line 977
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayNum:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v59, v0

    const/16 v60, 0x0

    aget v59, v59, v60

    aput v59, v57, v58

    .line 1028
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    move-object/from16 v31, v0

    .line 1029
    .local v31, "iterator":Landroid/text/format/Time;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mUntil:Landroid/text/format/Time;

    move-object/from16 v45, v0

    .line 1030
    .local v45, "until":Landroid/text/format/Time;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mStringBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v42, v0

    .line 1031
    .local v42, "sb":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mGenerated:Landroid/text/format/Time;

    .line 1032
    .local v4, "generated":Landroid/text/format/Time;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mDays:Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;

    move-object/from16 v18, v0

    .line 1036
    .local v18, "days":Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;
    :try_start_0
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->setRecurrence(Lcom/android/providers/calendar/calendarcommon/EventRecurrence;)V

    .line 1037
    const-wide v58, 0x7fffffffffffffffL

    cmp-long v57, p5, v58

    if-nez v57, :cond_c

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->until:Ljava/lang/String;

    move-object/from16 v57, v0

    if-nez v57, :cond_c

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->count:I

    move/from16 v57, v0

    if-nez v57, :cond_c

    .line 1038
    new-instance v57, Lcom/android/providers/calendar/calendarcommon/DateException;

    const-string v58, "No range end provided for a recurrence that has no UNTIL or COUNT."

    invoke-direct/range {v57 .. v58}, Lcom/android/providers/calendar/calendarcommon/DateException;-><init>(Ljava/lang/String;)V

    throw v57
    :try_end_0
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1481
    :catch_0
    move-exception v19

    .line 1482
    .local v19, "e":Lcom/android/providers/calendar/calendarcommon/DateException;
    const-string v57, "RecurrenceProcessor"

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "DateException with r="

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v58

    const-string v59, " rangeStart="

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v58

    const-string v59, " rangeEnd="

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-wide/from16 v1, p5

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1484
    throw v19

    .line 978
    .end local v4    # "generated":Landroid/text/format/Time;
    .end local v18    # "days":Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;
    .end local v19    # "e":Lcom/android/providers/calendar/calendarcommon/DateException;
    .end local v31    # "iterator":Landroid/text/format/Time;
    .end local v42    # "sb":Ljava/lang/StringBuilder;
    .end local v45    # "until":Landroid/text/format/Time;
    :cond_1
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    if-eqz v57, :cond_4

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v57, v0

    const/16 v58, 0x7

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_4

    .line 980
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    aget v57, v57, v58

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    aget v57, v57, v58

    const/16 v58, 0x2

    move/from16 v0, v57

    move/from16 v1, v58

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    aget v57, v57, v58

    const/16 v58, 0x3

    move/from16 v0, v57

    move/from16 v1, v58

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    aget v57, v57, v58

    const/16 v58, 0x4

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_3

    .line 983
    :cond_2
    const/16 v57, 0x1

    move/from16 v0, v57

    new-array v14, v0, [I

    .line 984
    .local v14, "bymonthday":[I
    const/16 v57, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aget v58, v58, v59

    aput v58, v14, v57

    .line 985
    move-object/from16 v0, p2

    iput-object v14, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthday:[I

    .line 986
    const/16 v57, 0x1

    move/from16 v0, v57

    move-object/from16 v1, p2

    iput v0, v1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    goto/16 :goto_0

    .line 987
    .end local v14    # "bymonthday":[I
    :cond_3
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    aget v57, v57, v58

    const/16 v58, -0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_0

    .line 989
    const/16 v57, 0x1

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_Lastday:Z

    goto/16 :goto_0

    .line 991
    :cond_4
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    if-eqz v57, :cond_5

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v57, v0

    const/16 v58, 0x2

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_5

    .line 994
    const/16 v57, 0x1

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_weekend:Z

    goto/16 :goto_0

    .line 996
    :cond_5
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    if-eqz v57, :cond_0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v57, v0

    const/16 v58, 0x5

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_0

    .line 998
    const/16 v57, 0x1

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_week_day:Z

    goto/16 :goto_0

    .line 1000
    :cond_6
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    move/from16 v57, v0

    const/16 v58, 0x7

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_0

    .line 1001
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    if-eqz v57, :cond_7

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_7

    .line 1003
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayNum:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v59, v0

    const/16 v60, 0x0

    aget v59, v59, v60

    aput v59, v57, v58

    .line 1004
    const/16 v57, 0x1

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_anyday:Z

    goto/16 :goto_0

    .line 1005
    :cond_7
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    if-eqz v57, :cond_8

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_8

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v57, v0

    const/16 v58, 0x5

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_8

    .line 1007
    const/16 v57, 0x1

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_weekday:Z

    goto/16 :goto_0

    .line 1008
    :cond_8
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    if-eqz v57, :cond_9

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_9

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v57, v0

    const/16 v58, 0x2

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_9

    .line 1010
    const/16 v57, 0x1

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_weekend:Z

    goto/16 :goto_0

    .line 1011
    :cond_9
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    if-eqz v57, :cond_0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetposCount:I

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v57, v0

    const/16 v58, 0x7

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_0

    .line 1012
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    aget v57, v57, v58

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-eq v0, v1, :cond_a

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    aget v57, v57, v58

    const/16 v58, 0x2

    move/from16 v0, v57

    move/from16 v1, v58

    if-eq v0, v1, :cond_a

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    aget v57, v57, v58

    const/16 v58, 0x3

    move/from16 v0, v57

    move/from16 v1, v58

    if-eq v0, v1, :cond_a

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    aget v57, v57, v58

    const/16 v58, 0x4

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_b

    .line 1015
    :cond_a
    const/16 v57, 0x1

    move/from16 v0, v57

    new-array v14, v0, [I

    .line 1016
    .restart local v14    # "bymonthday":[I
    const/16 v57, 0x0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aget v58, v58, v59

    aput v58, v14, v57

    .line 1017
    move-object/from16 v0, p2

    iput-object v14, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthday:[I

    .line 1018
    const/16 v57, 0x1

    move/from16 v0, v57

    move-object/from16 v1, p2

    iput v0, v1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    .line 1019
    const/16 v57, 0x1

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_first_second_third_fourth_day_of_month:Z

    goto/16 :goto_0

    .line 1020
    .end local v14    # "bymonthday":[I
    :cond_b
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    move-object/from16 v57, v0

    const/16 v58, 0x0

    aget v57, v57, v58

    const/16 v58, -0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_0

    .line 1022
    const/16 v57, 0x1

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_lastday_of_month:Z

    goto/16 :goto_0

    .line 1044
    .restart local v4    # "generated":Landroid/text/format/Time;
    .restart local v18    # "days":Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;
    .restart local v31    # "iterator":Landroid/text/format/Time;
    .restart local v42    # "sb":Ljava/lang/StringBuilder;
    .restart local v45    # "until":Landroid/text/format/Time;
    :cond_c
    :try_start_1
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->interval:I

    move/from16 v26, v0

    .line 1045
    .local v26, "freqAmount":I
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    move/from16 v25, v0

    .line 1046
    .local v25, "freq":I
    packed-switch v25, :pswitch_data_0

    .line 1074
    new-instance v57, Lcom/android/providers/calendar/calendarcommon/DateException;

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "bad freq="

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-direct/range {v57 .. v58}, Lcom/android/providers/calendar/calendarcommon/DateException;-><init>(Ljava/lang/String;)V

    throw v57
    :try_end_1
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1486
    .end local v25    # "freq":I
    .end local v26    # "freqAmount":I
    :catch_1
    move-exception v44

    .line 1487
    .local v44, "t":Ljava/lang/RuntimeException;
    const-string v57, "RecurrenceProcessor"

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "RuntimeException with r="

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v58

    const-string v59, " rangeStart="

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v58

    const-string v59, " rangeEnd="

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move-wide/from16 v1, p5

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-static/range {v57 .. v58}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1489
    throw v44

    .line 1049
    .end local v44    # "t":Ljava/lang/RuntimeException;
    .restart local v25    # "freq":I
    .restart local v26    # "freqAmount":I
    :pswitch_0
    const/16 v27, 0x1

    .line 1076
    .local v27, "freqField":I
    :cond_d
    :goto_1
    if-gtz v26, :cond_e

    .line 1077
    const/16 v26, 0x1

    .line 1080
    :cond_e
    :try_start_2
    move-object/from16 v0, p2

    iget v13, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthCount:I

    .line 1081
    .local v13, "bymonthCount":I
    const/16 v57, 0x6

    move/from16 v0, v25

    move/from16 v1, v57

    invoke-static {v0, v1, v13}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->useBYX(III)Z

    move-result v53

    .line 1082
    .local v53, "usebymonth":Z
    const/16 v57, 0x5

    move/from16 v0, v25

    move/from16 v1, v57

    if-lt v0, v1, :cond_15

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    move/from16 v57, v0

    if-gtz v57, :cond_f

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    move/from16 v57, v0

    if-lez v57, :cond_15

    :cond_f
    const/16 v50, 0x1

    .line 1084
    .local v50, "useDays":Z
    :goto_2
    move-object/from16 v0, p2

    iget v11, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byhourCount:I

    .line 1085
    .local v11, "byhourCount":I
    const/16 v57, 0x3

    move/from16 v0, v25

    move/from16 v1, v57

    invoke-static {v0, v1, v11}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->useBYX(III)Z

    move-result v51

    .line 1086
    .local v51, "usebyhour":Z
    move-object/from16 v0, p2

    iget v12, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byminuteCount:I

    .line 1087
    .local v12, "byminuteCount":I
    const/16 v57, 0x2

    move/from16 v0, v25

    move/from16 v1, v57

    invoke-static {v0, v1, v12}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->useBYX(III)Z

    move-result v52

    .line 1088
    .local v52, "usebyminute":Z
    move-object/from16 v0, p2

    iget v15, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysecondCount:I

    .line 1089
    .local v15, "bysecondCount":I
    const/16 v57, 0x1

    move/from16 v0, v25

    move/from16 v1, v57

    invoke-static {v0, v1, v15}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->useBYX(III)Z

    move-result v54

    .line 1092
    .local v54, "usebysecond":Z
    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1093
    const/16 v57, 0x5

    move/from16 v0, v27

    move/from16 v1, v57

    if-ne v0, v1, :cond_10

    .line 1094
    if-eqz v50, :cond_10

    .line 1100
    const/16 v57, 0x1

    move/from16 v0, v57

    move-object/from16 v1, v31

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 1105
    :cond_10
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->until:Ljava/lang/String;

    move-object/from16 v57, v0

    if-eqz v57, :cond_16

    .line 1107
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->until:Ljava/lang/String;

    move-object/from16 v48, v0

    .line 1111
    .local v48, "untilStr":Ljava/lang/String;
    invoke-virtual/range {v48 .. v48}, Ljava/lang/String;->length()I

    move-result v57

    const/16 v58, 0xf

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_11

    .line 1112
    new-instance v57, Ljava/lang/StringBuilder;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v57

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    const/16 v58, 0x5a

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v48

    .line 1115
    :cond_11
    move-object/from16 v0, v45

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 1117
    move-object/from16 v0, v45

    iget v0, v0, Landroid/text/format/Time;->year:I

    move/from16 v57, v0

    const/16 v58, 0x7f4

    move/from16 v0, v57

    move/from16 v1, v58

    if-le v0, v1, :cond_12

    .line 1118
    const/16 v57, 0x7f4

    move/from16 v0, v57

    move-object/from16 v1, v45

    iput v0, v1, Landroid/text/format/Time;->year:I

    .line 1119
    const/16 v57, 0xb

    move/from16 v0, v57

    move-object/from16 v1, v45

    iput v0, v1, Landroid/text/format/Time;->month:I

    .line 1120
    const/16 v57, 0x1f

    move/from16 v0, v57

    move-object/from16 v1, v45

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 1127
    :cond_12
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    move-object/from16 v57, v0

    move-object/from16 v0, v45

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 1128
    invoke-static/range {v45 .. v45}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v46

    .line 1142
    .end local v48    # "untilStr":Ljava/lang/String;
    .local v46, "untilDateValue":J
    :goto_3
    const/16 v57, 0xf

    move-object/from16 v0, v42

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->ensureCapacity(I)V

    .line 1143
    const/16 v57, 0xf

    move-object/from16 v0, v42

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1152
    const/16 v22, 0x0

    .line 1153
    .local v22, "eventEnded":Z
    const/16 v23, 0x0

    .line 1156
    .local v23, "failsafe":I
    :cond_13
    const/16 v39, 0x0

    .line 1163
    .local v39, "monthIndex":I
    invoke-static/range {v31 .. v31}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 1165
    move-object/from16 v0, v31

    iget v10, v0, Landroid/text/format/Time;->year:I

    .line 1166
    .local v10, "iteratorYear":I
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->month:I

    move/from16 v57, v0

    add-int/lit8 v35, v57, 0x1

    .line 1167
    .local v35, "iteratorMonth":I
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    move/from16 v32, v0

    .line 1168
    .local v32, "iteratorDay":I
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->hour:I

    move/from16 v33, v0

    .line 1169
    .local v33, "iteratorHour":I
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->minute:I

    move/from16 v34, v0

    .line 1170
    .local v34, "iteratorMinute":I
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->second:I

    move/from16 v36, v0

    .line 1177
    .local v36, "iteratorSecond":I
    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1181
    iget v0, v4, Landroid/text/format/Time;->year:I

    move/from16 v57, v0

    const/16 v58, 0x7f4

    move/from16 v0, v57

    move/from16 v1, v58

    if-le v0, v1, :cond_17

    .line 1471
    :cond_14
    const/16 v57, 0x0

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_Lastday:Z

    .line 1472
    const/16 v57, 0x0

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_weekend:Z

    .line 1473
    const/16 v57, 0x0

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_week_day:Z

    .line 1474
    const/16 v57, 0x0

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_anyday:Z

    .line 1475
    const/16 v57, 0x0

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_weekday:Z

    .line 1476
    const/16 v57, 0x0

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_weekend:Z

    .line 1477
    const/16 v57, 0x0

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_lastday_of_month:Z

    .line 1478
    const/16 v57, 0x0

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_first_second_third_fourth_day_of_month:Z

    .line 1491
    return-void

    .line 1052
    .end local v10    # "iteratorYear":I
    .end local v11    # "byhourCount":I
    .end local v12    # "byminuteCount":I
    .end local v13    # "bymonthCount":I
    .end local v15    # "bysecondCount":I
    .end local v22    # "eventEnded":Z
    .end local v23    # "failsafe":I
    .end local v27    # "freqField":I
    .end local v32    # "iteratorDay":I
    .end local v33    # "iteratorHour":I
    .end local v34    # "iteratorMinute":I
    .end local v35    # "iteratorMonth":I
    .end local v36    # "iteratorSecond":I
    .end local v39    # "monthIndex":I
    .end local v46    # "untilDateValue":J
    .end local v50    # "useDays":Z
    .end local v51    # "usebyhour":Z
    .end local v52    # "usebyminute":Z
    .end local v53    # "usebymonth":Z
    .end local v54    # "usebysecond":Z
    :pswitch_1
    const/16 v27, 0x2

    .line 1053
    .restart local v27    # "freqField":I
    goto/16 :goto_1

    .line 1055
    .end local v27    # "freqField":I
    :pswitch_2
    const/16 v27, 0x3

    .line 1056
    .restart local v27    # "freqField":I
    goto/16 :goto_1

    .line 1058
    .end local v27    # "freqField":I
    :pswitch_3
    const/16 v27, 0x4

    .line 1059
    .restart local v27    # "freqField":I
    goto/16 :goto_1

    .line 1061
    .end local v27    # "freqField":I
    :pswitch_4
    const/16 v27, 0x4

    .line 1062
    .restart local v27    # "freqField":I
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->interval:I

    move/from16 v57, v0

    mul-int/lit8 v26, v57, 0x7

    .line 1063
    if-gtz v26, :cond_d

    .line 1064
    const/16 v26, 0x7

    goto/16 :goto_1

    .line 1068
    .end local v27    # "freqField":I
    :pswitch_5
    const/16 v27, 0x5

    .line 1069
    .restart local v27    # "freqField":I
    goto/16 :goto_1

    .line 1071
    .end local v27    # "freqField":I
    :pswitch_6
    const/16 v27, 0x6

    .line 1072
    .restart local v27    # "freqField":I
    goto/16 :goto_1

    .line 1082
    .restart local v13    # "bymonthCount":I
    .restart local v53    # "usebymonth":Z
    :cond_15
    const/16 v50, 0x0

    goto/16 :goto_2

    .line 1132
    .restart local v11    # "byhourCount":I
    .restart local v12    # "byminuteCount":I
    .restart local v15    # "bysecondCount":I
    .restart local v50    # "useDays":Z
    .restart local v51    # "usebyhour":Z
    .restart local v52    # "usebyminute":Z
    .restart local v54    # "usebysecond":Z
    :cond_16
    new-instance v49, Landroid/text/format/Time;

    move-object/from16 v0, v49

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1133
    .local v49, "untilTime":Landroid/text/format/Time;
    const/16 v57, 0x7f4

    move/from16 v0, v57

    move-object/from16 v1, v49

    iput v0, v1, Landroid/text/format/Time;->year:I

    .line 1134
    const/16 v57, 0xb

    move/from16 v0, v57

    move-object/from16 v1, v49

    iput v0, v1, Landroid/text/format/Time;->month:I

    .line 1135
    const/16 v57, 0x1f

    move/from16 v0, v57

    move-object/from16 v1, v49

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 1136
    const/16 v57, 0x1

    move-object/from16 v0, v49

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 1138
    invoke-static/range {v49 .. v49}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v46

    .restart local v46    # "untilDateValue":J
    goto/16 :goto_3

    .line 1186
    .end local v49    # "untilTime":Landroid/text/format/Time;
    .restart local v10    # "iteratorYear":I
    .restart local v22    # "eventEnded":Z
    .restart local v23    # "failsafe":I
    .restart local v32    # "iteratorDay":I
    .restart local v33    # "iteratorHour":I
    .restart local v34    # "iteratorMinute":I
    .restart local v35    # "iteratorMonth":I
    .restart local v36    # "iteratorSecond":I
    .restart local v39    # "monthIndex":I
    :cond_17
    if-eqz v53, :cond_24

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonth:[I

    move-object/from16 v57, v0

    aget v9, v57, v39

    .line 1189
    .local v9, "month":I
    :goto_4
    add-int/lit8 v9, v9, -0x1

    .line 1192
    const/16 v17, 0x1

    .line 1193
    .local v17, "dayIndex":I
    const/16 v37, 0x0

    .line 1198
    .local v37, "lastDayToExamine":I
    if-eqz v50, :cond_18

    .line 1202
    const/16 v57, 0x5

    move/from16 v0, v25

    move/from16 v1, v57

    if-ne v0, v1, :cond_25

    .line 1220
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->weekDay:I

    move/from16 v57, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->wkst:I

    move/from16 v58, v0

    invoke-static/range {v58 .. v58}, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->day2TimeDay(I)I

    move-result v58

    sub-int v57, v57, v58

    add-int/lit8 v57, v57, 0x7

    rem-int/lit8 v56, v57, 0x7

    .line 1222
    .local v56, "weekStartAdj":I
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    move/from16 v57, v0

    sub-int v17, v57, v56

    .line 1223
    add-int/lit8 v37, v17, 0x6

    .line 1247
    .end local v56    # "weekStartAdj":I
    :cond_18
    :goto_5
    if-eqz v50, :cond_30

    .line 1249
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_Lastday:Z

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_26

    .line 1251
    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getlastday(Landroid/text/format/Time;)I

    move-result v8

    .line 1252
    .local v8, "day":I
    move/from16 v17, v8

    .line 1299
    :goto_6
    const/16 v30, 0x0

    .line 1301
    .local v30, "hourIndex":I
    :cond_19
    if-eqz v51, :cond_31

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byhour:[I

    move-object/from16 v57, v0

    aget v7, v57, v30

    .line 1307
    .local v7, "hour":I
    :goto_7
    const/16 v38, 0x0

    .line 1309
    .local v38, "minuteIndex":I
    :cond_1a
    if-eqz v52, :cond_32

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byminute:[I

    move-object/from16 v57, v0

    aget v6, v57, v38

    .line 1315
    .local v6, "minute":I
    :goto_8
    const/16 v43, 0x0

    .line 1317
    .local v43, "secondIndex":I
    :cond_1b
    if-eqz v54, :cond_33

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysecond:[I

    move-object/from16 v57, v0

    aget v5, v57, v43

    .line 1325
    .local v5, "second":I
    :goto_9
    invoke-virtual/range {v4 .. v10}, Landroid/text/format/Time;->set(IIIIII)V

    .line 1326
    invoke-static {v4}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 1328
    invoke-static {v4}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v28

    .line 1332
    .local v28, "genDateValue":J
    cmp-long v57, v28, v20

    if-ltz v57, :cond_1e

    .line 1337
    move-object/from16 v0, p2

    invoke-static {v0, v4}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->filter(Lcom/android/providers/calendar/calendarcommon/EventRecurrence;Landroid/text/format/Time;)I

    move-result v24

    .line 1338
    .local v24, "filtered":I
    if-nez v24, :cond_1e

    .line 1362
    if-eqz p7, :cond_34

    cmp-long v57, v20, p3

    if-ltz v57, :cond_34

    cmp-long v57, v20, p5

    if-gez v57, :cond_34

    .line 1365
    add-int/lit8 v16, v16, 0x1

    .line 1373
    :cond_1c
    :goto_a
    cmp-long v57, v28, v46

    if-gtz v57, :cond_14

    .line 1383
    cmp-long v57, v28, p5

    if-gez v57, :cond_14

    .line 1392
    cmp-long v57, v28, p3

    if-ltz v57, :cond_1d

    .line 1396
    if-eqz p7, :cond_35

    .line 1397
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v57

    move-object/from16 v0, p8

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 1403
    :cond_1d
    :goto_b
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->count:I

    move/from16 v57, v0

    if-lez v57, :cond_1e

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->count:I

    move/from16 v57, v0

    move/from16 v0, v57

    move/from16 v1, v16

    if-eq v0, v1, :cond_14

    .line 1409
    .end local v24    # "filtered":I
    :cond_1e
    add-int/lit8 v43, v43, 0x1

    .line 1410
    if-eqz v54, :cond_1f

    move/from16 v0, v43

    if-lt v0, v15, :cond_1b

    .line 1411
    :cond_1f
    add-int/lit8 v38, v38, 0x1

    .line 1412
    if-eqz v52, :cond_20

    move/from16 v0, v38

    if-lt v0, v12, :cond_1a

    .line 1413
    :cond_20
    add-int/lit8 v30, v30, 0x1

    .line 1414
    if-eqz v51, :cond_21

    move/from16 v0, v30

    if-lt v0, v11, :cond_19

    .line 1415
    :cond_21
    add-int/lit8 v17, v17, 0x1

    .line 1416
    .end local v5    # "second":I
    .end local v6    # "minute":I
    .end local v7    # "hour":I
    .end local v8    # "day":I
    .end local v28    # "genDateValue":J
    .end local v30    # "hourIndex":I
    .end local v38    # "minuteIndex":I
    .end local v43    # "secondIndex":I
    :goto_c
    if-eqz v50, :cond_22

    move/from16 v0, v17

    move/from16 v1, v37

    if-le v0, v1, :cond_18

    .line 1417
    :cond_22
    add-int/lit8 v39, v39, 0x1

    .line 1418
    if-eqz v53, :cond_23

    move/from16 v0, v39

    if-lt v0, v13, :cond_17

    .line 1424
    :cond_23
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    move/from16 v41, v0

    .line 1425
    .local v41, "oldDay":I
    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1426
    const/16 v40, 0x1

    .line 1428
    .local v40, "n":I
    :goto_d
    mul-int v55, v26, v40

    .line 1429
    .local v55, "value":I
    packed-switch v27, :pswitch_data_1

    .line 1455
    new-instance v57, Ljava/lang/RuntimeException;

    new-instance v58, Ljava/lang/StringBuilder;

    invoke-direct/range {v58 .. v58}, Ljava/lang/StringBuilder;-><init>()V

    const-string v59, "bad field="

    invoke-virtual/range {v58 .. v59}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    move-object/from16 v0, v58

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v58

    invoke-virtual/range {v58 .. v58}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v58

    invoke-direct/range {v57 .. v58}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v57

    .end local v9    # "month":I
    .end local v17    # "dayIndex":I
    .end local v37    # "lastDayToExamine":I
    .end local v40    # "n":I
    .end local v41    # "oldDay":I
    .end local v55    # "value":I
    :cond_24
    move/from16 v9, v35

    .line 1186
    goto/16 :goto_4

    .line 1225
    .restart local v9    # "month":I
    .restart local v17    # "dayIndex":I
    .restart local v37    # "lastDayToExamine":I
    :cond_25
    const/16 v57, 0x4

    move/from16 v0, v57

    invoke-virtual {v4, v0}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v37

    goto/16 :goto_5

    .line 1253
    :cond_26
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_weekend:Z

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_27

    .line 1255
    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getweekend_day(Landroid/text/format/Time;)I

    move-result v8

    .line 1256
    .restart local v8    # "day":I
    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getlastday(Landroid/text/format/Time;)I

    move-result v17

    goto/16 :goto_6

    .line 1257
    .end local v8    # "day":I
    :cond_27
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->Monthly_week_day:Z

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_28

    .line 1259
    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getweek_day(Landroid/text/format/Time;)I

    move-result v8

    .line 1260
    .restart local v8    # "day":I
    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getlastday(Landroid/text/format/Time;)I

    move-result v17

    goto/16 :goto_6

    .line 1262
    .end local v8    # "day":I
    :cond_28
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_anyday:Z

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-eq v0, v1, :cond_29

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_weekday:Z

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-eq v0, v1, :cond_29

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_weekend:Z

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-eq v0, v1, :cond_29

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_first_second_third_fourth_day_of_month:Z

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-eq v0, v1, :cond_29

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_lastday_of_month:Z

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_2a

    .line 1266
    :cond_29
    move-object/from16 v0, v31

    iput v9, v0, Landroid/text/format/Time;->month:I

    .line 1268
    :cond_2a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_weekday:Z

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_2b

    .line 1269
    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getweek_day(Landroid/text/format/Time;)I

    move-result v8

    .line 1270
    .restart local v8    # "day":I
    const/16 v17, 0x1f

    goto/16 :goto_6

    .line 1271
    .end local v8    # "day":I
    :cond_2b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_weekend:Z

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_2c

    .line 1272
    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getweekend_day(Landroid/text/format/Time;)I

    move-result v8

    .line 1273
    .restart local v8    # "day":I
    const/16 v17, 0x1f

    goto/16 :goto_6

    .line 1274
    .end local v8    # "day":I
    :cond_2c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->yearly_lastday_of_month:Z

    move/from16 v57, v0

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_2d

    .line 1275
    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->getlastday(Landroid/text/format/Time;)I

    move-result v8

    .line 1276
    .restart local v8    # "day":I
    const/16 v17, 0x1f

    goto/16 :goto_6

    .line 1279
    .end local v8    # "day":I
    :cond_2d
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    move/from16 v57, v0

    const/16 v58, 0x7

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_2e

    .line 1280
    const/16 v57, 0x4

    move-object/from16 v0, v31

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v37

    .line 1283
    :cond_2e
    move-object/from16 v0, v18

    move-object/from16 v1, v31

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor$DaySet;->get(Landroid/text/format/Time;I)Z

    move-result v57

    if-nez v57, :cond_2f

    .line 1284
    add-int/lit8 v17, v17, 0x1

    .line 1285
    goto/16 :goto_c

    .line 1287
    :cond_2f
    move/from16 v8, v17

    .restart local v8    # "day":I
    goto/16 :goto_6

    .line 1294
    .end local v8    # "day":I
    :cond_30
    move/from16 v8, v32

    .restart local v8    # "day":I
    goto/16 :goto_6

    .restart local v30    # "hourIndex":I
    :cond_31
    move/from16 v7, v33

    .line 1301
    goto/16 :goto_7

    .restart local v7    # "hour":I
    .restart local v38    # "minuteIndex":I
    :cond_32
    move/from16 v6, v34

    .line 1309
    goto/16 :goto_8

    .restart local v6    # "minute":I
    .restart local v43    # "secondIndex":I
    :cond_33
    move/from16 v5, v36

    .line 1317
    goto/16 :goto_9

    .line 1367
    .restart local v5    # "second":I
    .restart local v24    # "filtered":I
    .restart local v28    # "genDateValue":J
    :cond_34
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->count:I

    move/from16 v57, v0

    if-lez v57, :cond_1c

    .line 1368
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_a

    .line 1399
    :cond_35
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v57

    move-object/from16 v0, p8

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_b

    .line 1431
    .end local v5    # "second":I
    .end local v6    # "minute":I
    .end local v7    # "hour":I
    .end local v8    # "day":I
    .end local v24    # "filtered":I
    .end local v28    # "genDateValue":J
    .end local v30    # "hourIndex":I
    .end local v38    # "minuteIndex":I
    .end local v43    # "secondIndex":I
    .restart local v40    # "n":I
    .restart local v41    # "oldDay":I
    .restart local v55    # "value":I
    :pswitch_7
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->second:I

    move/from16 v57, v0

    add-int v57, v57, v55

    move/from16 v0, v57

    move-object/from16 v1, v31

    iput v0, v1, Landroid/text/format/Time;->second:I

    .line 1458
    :goto_e
    invoke-static/range {v31 .. v31}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 1459
    const/16 v57, 0x6

    move/from16 v0, v27

    move/from16 v1, v57

    if-eq v0, v1, :cond_36

    const/16 v57, 0x5

    move/from16 v0, v27

    move/from16 v1, v57

    if-ne v0, v1, :cond_13

    .line 1462
    :cond_36
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    move/from16 v57, v0

    move/from16 v0, v57

    move/from16 v1, v41

    if-eq v0, v1, :cond_13

    .line 1465
    add-int/lit8 v40, v40, 0x1

    .line 1466
    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    goto/16 :goto_d

    .line 1434
    :pswitch_8
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->minute:I

    move/from16 v57, v0

    add-int v57, v57, v55

    move/from16 v0, v57

    move-object/from16 v1, v31

    iput v0, v1, Landroid/text/format/Time;->minute:I

    goto :goto_e

    .line 1437
    :pswitch_9
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->hour:I

    move/from16 v57, v0

    add-int v57, v57, v55

    move/from16 v0, v57

    move-object/from16 v1, v31

    iput v0, v1, Landroid/text/format/Time;->hour:I

    goto :goto_e

    .line 1440
    :pswitch_a
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    move/from16 v57, v0

    add-int v57, v57, v55

    move/from16 v0, v57

    move-object/from16 v1, v31

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    goto :goto_e

    .line 1443
    :pswitch_b
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->month:I

    move/from16 v57, v0

    add-int v57, v57, v55

    move/from16 v0, v57

    move-object/from16 v1, v31

    iput v0, v1, Landroid/text/format/Time;->month:I

    goto :goto_e

    .line 1446
    :pswitch_c
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->year:I

    move/from16 v57, v0

    add-int v57, v57, v55

    move/from16 v0, v57

    move-object/from16 v1, v31

    iput v0, v1, Landroid/text/format/Time;->year:I

    goto :goto_e

    .line 1449
    :pswitch_d
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    move/from16 v57, v0

    add-int v57, v57, v55

    move/from16 v0, v57

    move-object/from16 v1, v31

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    goto/16 :goto_e

    .line 1452
    :pswitch_e
    move-object/from16 v0, v31

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    move/from16 v57, v0

    add-int v57, v57, v55

    move/from16 v0, v57

    move-object/from16 v1, v31

    iput v0, v1, Landroid/text/format/Time;->monthDay:I
    :try_end_2
    .catch Lcom/android/providers/calendar/calendarcommon/DateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_e

    .line 1046
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 1429
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;JJ)[J
    .locals 33
    .param p1, "dtstart"    # Landroid/text/format/Time;
    .param p2, "recur"    # Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;
    .param p3, "rangeStartMillis"    # J
    .param p5, "rangeEndMillis"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/calendar/calendarcommon/DateException;
        }
    .end annotation

    .prologue
    .line 841
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    move-object/from16 v31, v0

    .line 842
    .local v31, "timezone":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    move-object/from16 v0, v31

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 843
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mGenerated:Landroid/text/format/Time;

    move-object/from16 v0, v31

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 850
    const-wide v16, -0x1f3be2e8340L

    cmp-long v3, p3, v16

    if-gez v3, :cond_0

    .line 851
    const-wide p3, -0x1f3be2e8340L

    .line 854
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 855
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    invoke-static {v3}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v6

    .line 858
    .local v6, "rangeStartDateValue":J
    const-wide/16 v16, -0x1

    cmp-long v3, p5, v16

    if-eqz v3, :cond_1

    .line 859
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 860
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    invoke-static {v3}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v8

    .line 865
    .local v8, "rangeEndDateValue":J
    :goto_0
    new-instance v11, Ljava/util/TreeSet;

    invoke-direct {v11}, Ljava/util/TreeSet;-><init>()V

    .line 867
    .local v11, "dtSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Long;>;"
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    if-eqz v3, :cond_2

    .line 868
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .local v2, "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    array-length v0, v2

    move/from16 v30, v0

    .local v30, "len$":I
    const/16 v28, 0x0

    .local v28, "i$":I
    :goto_1
    move/from16 v0, v28

    move/from16 v1, v30

    if-ge v0, v1, :cond_2

    aget-object v5, v2, v28

    .line 869
    .local v5, "rrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    const/4 v10, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v11}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/EventRecurrence;JJZLjava/util/TreeSet;)V

    .line 868
    add-int/lit8 v28, v28, 0x1

    goto :goto_1

    .line 862
    .end local v2    # "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .end local v5    # "rrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .end local v8    # "rangeEndDateValue":J
    .end local v11    # "dtSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Long;>;"
    .end local v28    # "i$":I
    .end local v30    # "len$":I
    :cond_1
    const-wide v8, 0x7fffffffffffffffL

    .restart local v8    # "rangeEndDateValue":J
    goto :goto_0

    .line 873
    .restart local v11    # "dtSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Long;>;"
    :cond_2
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    if-eqz v3, :cond_3

    .line 874
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    .local v2, "arr$":[J
    array-length v0, v2

    move/from16 v30, v0

    .restart local v30    # "len$":I
    const/16 v28, 0x0

    .restart local v28    # "i$":I
    :goto_2
    move/from16 v0, v28

    move/from16 v1, v30

    if-ge v0, v1, :cond_3

    aget-wide v22, v2, v28

    .line 877
    .local v22, "dt":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    move-wide/from16 v0, v22

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 878
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    invoke-static {v3}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v24

    .line 879
    .local v24, "dtvalue":J
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 874
    add-int/lit8 v28, v28, 0x1

    goto :goto_2

    .line 882
    .end local v2    # "arr$":[J
    .end local v22    # "dt":J
    .end local v24    # "dtvalue":J
    .end local v28    # "i$":I
    .end local v30    # "len$":I
    :cond_3
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    if-eqz v3, :cond_4

    .line 883
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .local v2, "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    array-length v0, v2

    move/from16 v30, v0

    .restart local v30    # "len$":I
    const/16 v28, 0x0

    .restart local v28    # "i$":I
    :goto_3
    move/from16 v0, v28

    move/from16 v1, v30

    if-ge v0, v1, :cond_4

    aget-object v15, v2, v28

    .line 884
    .local v15, "exrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    const/16 v20, 0x0

    move-object/from16 v13, p0

    move-object/from16 v14, p1

    move-wide/from16 v16, v6

    move-wide/from16 v18, v8

    move-object/from16 v21, v11

    invoke-virtual/range {v13 .. v21}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/EventRecurrence;JJZLjava/util/TreeSet;)V

    .line 883
    add-int/lit8 v28, v28, 0x1

    goto :goto_3

    .line 888
    .end local v2    # "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .end local v15    # "exrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .end local v28    # "i$":I
    .end local v30    # "len$":I
    :cond_4
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exdates:[J

    if-eqz v3, :cond_5

    .line 889
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exdates:[J

    .local v2, "arr$":[J
    array-length v0, v2

    move/from16 v30, v0

    .restart local v30    # "len$":I
    const/16 v28, 0x0

    .restart local v28    # "i$":I
    :goto_4
    move/from16 v0, v28

    move/from16 v1, v30

    if-ge v0, v1, :cond_5

    aget-wide v22, v2, v28

    .line 892
    .restart local v22    # "dt":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    move-wide/from16 v0, v22

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 893
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    invoke-static {v3}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->normDateTimeComparisonValue(Landroid/text/format/Time;)J

    move-result-wide v24

    .line 894
    .restart local v24    # "dtvalue":J
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 889
    add-int/lit8 v28, v28, 0x1

    goto :goto_4

    .line 897
    .end local v2    # "arr$":[J
    .end local v22    # "dt":J
    .end local v24    # "dtvalue":J
    .end local v28    # "i$":I
    .end local v30    # "len$":I
    :cond_5
    invoke-virtual {v11}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 900
    const/4 v3, 0x0

    new-array v12, v3, [J

    .line 914
    :cond_6
    return-object v12

    .line 907
    :cond_7
    invoke-virtual {v11}, Ljava/util/TreeSet;->size()I

    move-result v29

    .line 908
    .local v29, "len":I
    move/from16 v0, v29

    new-array v12, v0, [J

    .line 909
    .local v12, "dates":[J
    const/16 v26, 0x0

    .line 910
    .local v26, "i":I
    invoke-virtual {v11}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .local v28, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/Long;

    .line 911
    .local v32, "val":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-static {v3, v0, v1}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->setTimeFromLongValue(Landroid/text/format/Time;J)V

    .line 912
    add-int/lit8 v27, v26, 0x1

    .end local v26    # "i":I
    .local v27, "i":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v16

    aput-wide v16, v12, v26

    move/from16 v26, v27

    .line 913
    .end local v27    # "i":I
    .restart local v26    # "i":I
    goto :goto_5
.end method

.method public getLastOccurence(Landroid/text/format/Time;Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;)J
    .locals 22
    .param p1, "dtstart"    # Landroid/text/format/Time;
    .param p2, "maxtime"    # Landroid/text/format/Time;
    .param p3, "recur"    # Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/calendar/calendarcommon/DateException;
        }
    .end annotation

    .prologue
    .line 98
    const-wide/16 v16, -0x1

    .line 99
    .local v16, "lastTime":J
    const/4 v11, 0x0

    .line 103
    .local v11, "hasCount":Z
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    if-eqz v3, :cond_6

    .line 104
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .local v2, "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    array-length v15, v2

    .local v15, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_0
    if-ge v14, v15, :cond_3

    aget-object v18, v2, v14

    .line 105
    .local v18, "rrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    move-object/from16 v0, v18

    iget v3, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->count:I

    if-eqz v3, :cond_1

    .line 106
    const/4 v11, 0x1

    .line 104
    :cond_0
    :goto_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 107
    :cond_1
    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->until:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 109
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->until:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 110
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v20

    .line 111
    .local v20, "untilTime":J
    cmp-long v3, v20, v16

    if-lez v3, :cond_0

    .line 112
    move-wide/from16 v16, v20

    goto :goto_1

    .line 118
    .end local v20    # "untilTime":J
    :cond_2
    const-wide/16 v4, -0x1

    .line 163
    .end local v2    # "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    .end local v18    # "rrule":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    :goto_2
    return-wide v4

    .line 122
    .restart local v2    # "arr$":[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .restart local v14    # "i$":I
    .restart local v15    # "len$":I
    :cond_3
    const-wide/16 v4, -0x1

    cmp-long v3, v16, v4

    if-eqz v3, :cond_5

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    if-eqz v3, :cond_5

    .line 123
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    .local v2, "arr$":[J
    array-length v15, v2

    const/4 v14, 0x0

    :goto_3
    if-ge v14, v15, :cond_5

    aget-wide v12, v2, v14

    .line 124
    .local v12, "dt":J
    cmp-long v3, v12, v16

    if-lez v3, :cond_4

    .line 125
    move-wide/from16 v16, v12

    .line 123
    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 132
    .end local v2    # "arr$":[J
    .end local v12    # "dt":J
    :cond_5
    const-wide/16 v4, -0x1

    cmp-long v3, v16, v4

    if-eqz v3, :cond_9

    if-nez v11, :cond_9

    move-wide/from16 v4, v16

    .line 133
    goto :goto_2

    .line 135
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    :cond_6
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    if-eqz v3, :cond_9

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exrules:[Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    if-nez v3, :cond_9

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->exdates:[J

    if-nez v3, :cond_9

    .line 138
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    .restart local v2    # "arr$":[J
    array-length v15, v2

    .restart local v15    # "len$":I
    const/4 v14, 0x0

    .restart local v14    # "i$":I
    :goto_4
    if-ge v14, v15, :cond_8

    aget-wide v12, v2, v14

    .line 139
    .restart local v12    # "dt":J
    cmp-long v3, v12, v16

    if-lez v3, :cond_7

    .line 140
    move-wide/from16 v16, v12

    .line 138
    :cond_7
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    .end local v12    # "dt":J
    :cond_8
    move-wide/from16 v4, v16

    .line 143
    goto :goto_2

    .line 148
    .end local v2    # "arr$":[J
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    :cond_9
    if-nez v11, :cond_a

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;->rdates:[J

    if-nez v3, :cond_a

    if-eqz p2, :cond_d

    .line 151
    :cond_a
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    if-eqz p2, :cond_b

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    :goto_5
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    invoke-virtual/range {v3 .. v9}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->expand(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;JJ)[J

    move-result-object v10

    .line 158
    .local v10, "dates":[J
    array-length v3, v10

    if-nez v3, :cond_c

    .line 159
    const-wide/16 v4, 0x0

    goto :goto_2

    .line 151
    .end local v10    # "dates":[J
    :cond_b
    const-wide/16 v8, -0x1

    goto :goto_5

    .line 161
    .restart local v10    # "dates":[J
    :cond_c
    array-length v3, v10

    add-int/lit8 v3, v3, -0x1

    aget-wide v4, v10, v3

    goto/16 :goto_2

    .line 163
    .end local v10    # "dates":[J
    :cond_d
    const-wide/16 v4, -0x1

    goto/16 :goto_2
.end method

.method public getLastOccurence(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;)J
    .locals 2
    .param p1, "dtstart"    # Landroid/text/format/Time;
    .param p2, "recur"    # Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/providers/calendar/calendarcommon/DateException;
        }
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/providers/calendar/calendarcommon/RecurrenceProcessor;->getLastOccurence(Landroid/text/format/Time;Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/RecurrenceSet;)J

    move-result-wide v0

    return-wide v0
.end method
