.class Lcom/android/providers/calendar/CalendarDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "CalendarDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/calendar/CalendarDatabaseHelper$InsertRunnable;
    }
.end annotation


# static fields
.field public static ACCOUNT_DISABLED_FEATURE_FOR_LOGGING:Ljava/lang/String;

.field public static ACCOUNT_ENABLED_FEATURE_FOR_LOGGING:Ljava/lang/String;

.field private static APP_ID_FOR_LOGGING:Ljava/lang/String;

.field static mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

.field private static sSingleton:Lcom/android/providers/calendar/CalendarDatabaseHelper;

.field private static final sStickerUpgradeNameMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sStickerUpgradeNameMapM0:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private STICKERPATH:Ljava/lang/String;

.field private final SUBST_HOLS_CALENDAR_NAME:Ljava/lang/String;

.field private final SUBST_HOLS_SETTING:Ljava/lang/String;

.field private createNoteField:Z

.field private isM0stickers:Z

.field private mAttendeesInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mCalendarAlertsInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mCalendarsInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mColorsInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mContext:Landroid/content/Context;

.field private mEventsInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mEventsRawTimesInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mExtendedPropertiesInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mFacebooksInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mImagesInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field public mInTestMode:Z

.field private mInstancesInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mMapInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mMemosInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mNotesInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mQSMemosInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mRemindersInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mStickersInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private final mSyncState:Lcom/android/common/content/SyncStateContentProviderHelper;

.field private mTaskAccountId:J

.field private mTaskGroupInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mTasksAccountsInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mTasksInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mTasksReminderInserter:Landroid/database/DatabaseUtils$InsertHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 123
    const-string v0, "com.android.calendar"

    sput-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->APP_ID_FOR_LOGGING:Ljava/lang/String;

    .line 124
    const-string v0, "ACCE"

    sput-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->ACCOUNT_ENABLED_FEATURE_FOR_LOGGING:Ljava/lang/String;

    .line 125
    const-string v0, "ACCD"

    sput-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->ACCOUNT_DISABLED_FEATURE_FOR_LOGGING:Ljava/lang/String;

    .line 312
    const/4 v0, 0x0

    sput-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sSingleton:Lcom/android/providers/calendar/CalendarDatabaseHelper;

    .line 337
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    .line 339
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Appointment_01.png"

    const-string v2, "food_coffee.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Appointment_02.png"

    const-string v2, "food_restaurant.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Appointment_03.png"

    const-string v2, "food_ice-cream.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Appointment_04.png"

    const-string v2, "food_beer.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Appointment_05.png"

    const-string v2, "food_cocktail.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Appointment_06.png"

    const-string v2, "leisure_film.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Appointment_07.png"

    const-string v2, "objects_gift.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Appointment_08.png"

    const-string v2, "leisure_theater.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Appointment_09.png"

    const-string v2, "leisure_amusement-park.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Appointment_10.png"

    const-string v2, "leisure_picnic.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_01.png"

    const-string v2, "nature_star.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_02.png"

    const-string v2, "nature_sun.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_03.png"

    const-string v2, "nature_cloud.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_04.png"

    const-string v2, "nature_snow.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_05.png"

    const-string v2, "nature_rain.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_06.png"

    const-string v2, "emotion_smiling.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_07.png"

    const-string v2, "emotion_happy.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_08.png"

    const-string v2, "emotion_devil.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_09.png"

    const-string v2, "emotion_satisfied.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_10.png"

    const-string v2, "emotion_sad.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_11.png"

    const-string v2, "emotion_sleepy.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_12.png"

    const-string v2, "emotion_angry.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_13.png"

    const-string v2, "emotion_thumbs-up.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_14.png"

    const-string v2, "emotion_sick.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_15.png"

    const-string v2, "emotion_poker-face.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_16.png"

    const-string v2, "emotion_cute.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_17.png"

    const-string v2, "emotion_pleasure.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_18.png"

    const-string v2, "emotion_gloom.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_19.png"

    const-string v2, "emotion_surprised.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Common_20.png"

    const-string v2, "emotion_embarrassed.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Event_01.png"

    const-string v2, "event_birthday.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Event_02.png"

    const-string v2, "event_wedding.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Event_03.png"

    const-string v2, "event_party.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Event_04.png"

    const-string v2, "event_heart.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Event_05.png"

    const-string v2, "event_deadline.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Highlight_01.png"

    const-string v2, "highlight_tick.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Highlight_02.png"

    const-string v2, "highlight_circle.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Highlight_03.png"

    const-string v2, "highlight_asterisk.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Highlight_04.png"

    const-string v2, "highlight_twinkle.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Highlight_05.png"

    const-string v2, "background_yellow-background.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Highlight_06.png"

    const-string v2, "background_green-background.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Highlight_07.png"

    const-string v2, "background_red-background.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Highlight_08.png"

    const-string v2, "background_pink-background.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_01.png"

    const-string v2, "leisure_baseball.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_02.png"

    const-string v2, "leisure_basketball.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_03.png"

    const-string v2, "leisure_football.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_04.png"

    const-string v2, "leisure_tennis.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_05.png"

    const-string v2, "leisure_swimming.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_06.png"

    const-string v2, "leisure_exercise.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_07.png"

    const-string v2, "leisure_camping.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_08.png"

    const-string v2, "leisure_golf.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_09.png"

    const-string v2, "objects_aeroplane.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_10.png"

    const-string v2, "objects_car.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_11.png"

    const-string v2, "objects_train.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_12.png"

    const-string v2, "leisure_snowboarding.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_13.png"

    const-string v2, "leisure_climbing.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_14.png"

    const-string v2, "leisure_cycling.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "SportsLeisure_15.png"

    const-string v2, "leisure_sailing.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Todo_01.png"

    const-string v2, "activity_bank.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Todo_02.png"

    const-string v2, "activity_hospital.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Todo_03.png"

    const-string v2, "activity_conference.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Todo_04.png"

    const-string v2, "activity_business.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Todo_05.png"

    const-string v2, "activity_cleaning.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Todo_06.png"

    const-string v2, "leisure_reading.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Todo_07.png"

    const-string v2, "activity_shopping.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Todo_08.png"

    const-string v2, "activity_shopping-bag.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Todo_09.png"

    const-string v2, "activity_diet.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Todo_10.png"

    const-string v2, "activity_beauty.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_01.png"

    const-string v2, "leisure_baseball.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_02.png"

    const-string v2, "leisure_basketball.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_03.png"

    const-string v2, "leisure_football.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_04.png"

    const-string v2, "leisure_tennis.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_05.png"

    const-string v2, "leisure_swimming.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_06.png"

    const-string v2, "leisure_exercise.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_07.png"

    const-string v2, "leisure_camping.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_08.png"

    const-string v2, "leisure_golf.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_09.png"

    const-string v2, "objects_aeroplane.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_10.png"

    const-string v2, "objects_car.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_11.png"

    const-string v2, "objects_train.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_12.png"

    const-string v2, "leisure_snowboarding.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_13.png"

    const-string v2, "leisure_climbing.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_14.png"

    const-string v2, "leisure_cycling.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "Sports-&-Leisure_15.png"

    const-string v2, "leisure_sailing.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "to-do_01.png"

    const-string v2, "activity_bank.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "to-do_02.png"

    const-string v2, "activity_hospital.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "to-do_03.png"

    const-string v2, "activity_conference.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "to-do_04.png"

    const-string v2, "activity_business.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "to-do_05.png"

    const-string v2, "activity_cleaning.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "to-do_06.png"

    const-string v2, "leisure_reading.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "to-do_07.png"

    const-string v2, "activity_shopping.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "to-do_08.png"

    const-string v2, "activity_shopping-bag.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "to-do_09.png"

    const-string v2, "activity_diet.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    const-string v1, "to-do_10.png"

    const-string v2, "activity_beauty.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    .line 440
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_person_baby_boy_s.png"

    const-string v2, "samsung_preload_people_baby-boy.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_person_baby_girl_s.png"

    const-string v2, "samsung_preload_people_baby-girl.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_person_grandma_s.png"

    const-string v2, "samsung_preload_people_grandmother.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_person_grandpa_s.png"

    const-string v2, "samsung_preload_people_grandfather.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_person_little_boy_s.png"

    const-string v2, "samsung_preload_people_boy.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_person_little_girl_s.png"

    const-string v2, "samsung_preload_people_girl.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_person_middle_aged_men_s.png"

    const-string v2, "samsung_preload_people_dad.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_person_middle_aged_woman_s.png"

    const-string v2, "samsung_preload_people_mom.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_person_young_man_s.png"

    const-string v2, "samsung_preload_people_young-man.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_person_young_woman_s.png"

    const-string v2, "samsung_preload_people_young-woman.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_wood.png"

    const-string v2, "samsung_preload_nature_tree.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_winter.png"

    const-string v2, "samsung_preload_nature_winter.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_test.png"

    const-string v2, "samsung_preload_activity_exam.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_hospital.png"

    const-string v2, "samsung_preload_activity_hospital.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_Emphasize_check.png"

    const-string v2, "samsung_preload_highlight_tick.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_Emphasize_circle.png"

    const-string v2, "samsung_preload_highlight_circle.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 456
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emphasize_pin_s.png"

    const-string v2, "samsung_preload_highlight_drawing-pin.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_Emphasize_clip.png"

    const-string v2, "samsung_preload_highlight_paper-clip.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_Emphasize_star.png"

    const-string v2, "samsung_preload_highlight_asterisk.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_Emphasize_star3.png"

    const-string v2, "samsung_preload_highlight_three-asterisks.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_Emphasize_x.png"

    const-string v2, "samsung_preload_highlight_cross.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_Emphasize_shiny.png"

    const-string v2, "samsung_preload_highlight_twinkle.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_angry_s.png"

    const-string v2, "samsung_preload_emotion_angry.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_surprised_s.png"

    const-string v2, "samsung_preload_emotion_surprised.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_tired_s.png"

    const-string v2, "samsung_preload_emotion_tired.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_wink_s.png"

    const-string v2, "samsung_preload_emotion_wink.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 466
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_birthday.png"

    const-string v2, "samsung_preload_event_birthday.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_christmas.png"

    const-string v2, "samsung_preload_event_christmas.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_d_day.png"

    const-string v2, "samsung_preload_event_deadline.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_dining_together.png"

    const-string v2, "samsung_preload_event_get-together.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_ealloween.png"

    const-string v2, "samsung_preload_event_halloween.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_Emphasize_heart.png"

    const-string v2, "samsung_preload_event_heart.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_worldcup_s.png"

    const-string v2, "samsung_preload_event_olympics.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_somnolence_s.png"

    const-string v2, "samsung_preload_emotion_sleepy.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_devil_s.png"

    const-string v2, "samsung_preload_emotion_devil.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_pain_s.png"

    const-string v2, "samsung_preload_emotion_sick.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_delight_s.png"

    const-string v2, "samsung_preload_emotion_pleasure.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_expressionless_s.png"

    const-string v2, "samsung_preload_emotion_puzzled.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_cute_2.png"

    const-string v2, "samsung_preload_emotion_cute.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_affection_s.png"

    const-string v2, "samsung_preload_emotion_happy.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_shame.png"

    const-string v2, "samsung_preload_emotion_ashamed.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_pride_s.png"

    const-string v2, "samsung_preload_emotion_arrogant.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 482
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_sorrow_s.png"

    const-string v2, "samsung_preload_emotion_sad.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_weep_s.png"

    const-string v2, "samsung_preload_emotion_crying.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_bewilderment_s.png"

    const-string v2, "samsung_preload_emotion_embarrassed.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_melancholy_s.png"

    const-string v2, "samsung_preload_emotion_gloom.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_contentment_s.png"

    const-string v2, "samsung_preload_emotion_smiling.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_maximum_s.png"

    const-string v2, "samsung_preload_emotion_satisfied.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 488
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_emotion_absurd_s.png"

    const-string v2, "samsung_preload_emotion_poker-face.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_hamburger.png"

    const-string v2, "samsung_preload_food_burger.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_curry_rice.png"

    const-string v2, "samsung_preload_food_curry-and-rice.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_coffee_s.png"

    const-string v2, "samsung_preload_food_coffee.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_pizza.png"

    const-string v2, "samsung_preload_food_pizza.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_ramyon.png"

    const-string v2, "samsung_preload_food_noodles.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_ice_cream.png"

    const-string v2, "samsung_preload_food_ice-cream.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_basketball_s.png"

    const-string v2, "samsung_preload_leisure_basketball.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_camping_s.png"

    const-string v2, "samsung_preload_leisure_camping.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_amusement_Park_s.png"

    const-string v2, "samsung_preload_leisure_amusement-park.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_football_s.png"

    const-string v2, "samsung_preload_leisure_football.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_bicycle_s.png"

    const-string v2, "samsung_preload_leisure_cycling.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_tea.png"

    const-string v2, "samsung_preload_food_tea.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_vegetables.png"

    const-string v2, "samsung_preload_food_vegetables.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_sushi.png"

    const-string v2, "samsung_preload_food_sushi.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 503
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_movie_s.png"

    const-string v2, "samsung_preload_leisure_film.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_musical_instruments_s.png"

    const-string v2, "samsung_preload_leisure_piano.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 505
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_riding_s.png"

    const-string v2, "samsung_preload_leisure_horse-riding.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_Post.png"

    const-string v2, "samsung_preload_activity_post-office.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 507
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_presentation.png"

    const-string v2, "samsung_preload_activity_presentation.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_report.png"

    const-string v2, "samsung_preload_activity_report.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 509
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_mart.png"

    const-string v2, "samsung_preload_activity_shopping.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_cleaning.png"

    const-string v2, "samsung_preload_activity_cleaning.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_bank.png"

    const-string v2, "samsung_preload_activity_bank.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_delivery.png"

    const-string v2, "samsung_preload_activity_delivery.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_diet.png"

    const-string v2, "samsung_preload_activity_diet.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_beauty_shop.png"

    const-string v2, "samsung_preload_activity_beauty.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_conference.png"

    const-string v2, "samsung_preload_activity_conference.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_flower.png"

    const-string v2, "samsung_preload_nature_flower.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_grass_s.png"

    const-string v2, "samsung_preload_nature_grass.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 518
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_moon.png"

    const-string v2, "samsung_preload_nature_moon.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_snow.png"

    const-string v2, "samsung_preload_nature_snow.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_star.png"

    const-string v2, "samsung_preload_nature_star.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_storm.png"

    const-string v2, "samsung_preload_nature_storm.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_spring.png"

    const-string v2, "samsung_preload_nature_spring.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_sun.png"

    const-string v2, "samsung_preload_nature_sun.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 524
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_fall.png"

    const-string v2, "samsung_preload_nature_autumn.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 525
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_cat.png"

    const-string v2, "samsung_preload_nature_cat.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_puppy.png"

    const-string v2, "samsung_preload_nature_dog.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_birds.png"

    const-string v2, "samsung_preload_nature_bird.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_butterfly_s.png"

    const-string v2, "samsung_preload_nature_butterfly.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 529
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_ski_s.png"

    const-string v2, "samsung_preload_leisure_skiing.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_museum_of_art_s.png"

    const-string v2, "samsung_preload_leisure_museum.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_board_s.png"

    const-string v2, "samsung_preload_leisure_snowboarding.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_ping_pong_s.png"

    const-string v2, "samsung_preload_leisure_table-tennis.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_baseball_s.png"

    const-string v2, "samsung_preload_leisure_baseball.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_mountaineering_s.png"

    const-string v2, "samsung_preload_leisure_climbing.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_listening_to_music_s.png"

    const-string v2, "samsung_preload_leisure_music.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_picnic_s.png"

    const-string v2, "samsung_preload_leisure_picnic.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_musical_s.png"

    const-string v2, "samsung_preload_leisure_musical.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_reading_s.png"

    const-string v2, "samsung_preload_leisure_reading.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_inlineskating_s.png"

    const-string v2, "samsung_preload_leisure_inline-skating.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_camera_s.png"

    const-string v2, "samsung_preload_objects_camera.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_car_s.png"

    const-string v2, "samsung_preload_objects_car.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_alarm.png"

    const-string v2, "samsung_preload_objects_clock.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_golf_s.png"

    const-string v2, "samsung_preload_leisure_golf.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_rain.png"

    const-string v2, "samsung_preload_nature_rain.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_rainbow.png"

    const-string v2, "samsung_preload_nature_rainbow.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_fork_spoon.png"

    const-string v2, "samsung_preload_food_restaurant.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_rice.png"

    const-string v2, "samsung_preload_food_rice.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_snack.png"

    const-string v2, "samsung_preload_food_snack.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_iced_tea.png"

    const-string v2, "samsung_preload_food_iced-tea.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_cola_s.png"

    const-string v2, "samsung_preload_food_cola.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_cocktail.png"

    const-string v2, "samsung_preload_food_cocktail.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_chocolate.png"

    const-string v2, "samsung_preload_food_chocolate.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_bread.png"

    const-string v2, "samsung_preload_food_bread.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_cookies.png"

    const-string v2, "samsung_preload_food_biscuit.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_beer.png"

    const-string v2, "samsung_preload_food_beer.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_marriage.png"

    const-string v2, "samsung_preload_event_wedding.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 557
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_thanksgivingday.png"

    const-string v2, "samsung_preload_event_thanksgiving.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 558
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_airplane_s.png"

    const-string v2, "samsung_preload_objects_aeroplane.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_bus_s.png"

    const-string v2, "samsung_preload_objects_bus.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_gift.png"

    const-string v2, "samsung_preload_objects_gift.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_letter.png"

    const-string v2, "samsung_preload_objects_letter.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_summer.png"

    const-string v2, "samsung_preload_nature_summer.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_tv_s.png"

    const-string v2, "samsung_preload_leisure_tv.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_train_s.png"

    const-string v2, "samsung_preload_objects_train.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_love.png"

    const-string v2, "samsung_preload_event_love.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_appointment.png"

    const-string v2, "samsung_preload_emotion_promise.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_wine.png"

    const-string v2, "samsung_preload_food_wine.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_tennis_s.png"

    const-string v2, "samsung_preload_leisure_tennis.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_money.png"

    const-string v2, "samsung_preload_objects_money.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 570
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_shopping.png"

    const-string v2, "samsung_preload_activity_shopping-bag.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_tie_s.png"

    const-string v2, "samsung_preload_objects_tie.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_game_s.png"

    const-string v2, "samsung_preload_leisure_game.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_parting.png"

    const-string v2, "samsung_preload_event_broken-heart.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 574
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_suntan.png"

    const-string v2, "samsung_preload_leisure_sun-bathing.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 575
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_election.png"

    const-string v2, "samsung_preload_event_vote.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_gas-station.png"

    const-string v2, "samsung_preload_activity_petrol-station.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 577
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_school.png"

    const-string v2, "samsung_preload_activity_school.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 578
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_valentinesday.png"

    const-string v2, "samsung_preload_event_valentines-day.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_easter.png"

    const-string v2, "samsung_preload_event_easter.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 580
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_study.png"

    const-string v2, "samsung_preload_activity_study.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_swimming_s.png"

    const-string v2, "samsung_preload_leisure_swimming.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 582
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_household.png"

    const-string v2, "samsung_preload_activity_calculation.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_company.png"

    const-string v2, "samsung_preload_activity_company.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 584
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_Emphasize_ribbon.png"

    const-string v2, "samsung_preload_objects_ribbon.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_muffin.png"

    const-string v2, "samsung_preload_food_muffin.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 586
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_porridge_s.png"

    const-string v2, "samsung_preload_food_porridge.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 587
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_skydive_s.png"

    const-string v2, "samsung_preload_leisure_skydiving.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 588
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_island.png"

    const-string v2, "samsung_preload_objects_island.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 589
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_briefcase.png"

    const-string v2, "samsung_preload_activity_business.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 590
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_eestival.png"

    const-string v2, "samsung_preload_event_party.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 591
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_fish_s.png"

    const-string v2, "samsung_preload_nature_fish.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 592
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_fruit.png"

    const-string v2, "samsung_preload_food_fruits.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 593
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_liquer.png"

    const-string v2, "samsung_preload_food_liquor.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 594
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_overtime.png"

    const-string v2, "samsung_preload_activity_overtime.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 595
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_conversation.png"

    const-string v2, "samsung_preload_objects_talking.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 596
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_earth_s.png"

    const-string v2, "samsung_preload_nature_earth.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 597
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_cloud_2.png"

    const-string v2, "samsung_preload_nature_cloud.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 598
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_exercise.png"

    const-string v2, "samsung_preload_leisure_exercise.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 599
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_wind_2.png"

    const-string v2, "samsung_preload_nature_wind.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 600
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_walk_s.png"

    const-string v2, "samsung_preload_leisure_stroll.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 601
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_museum.png"

    const-string v2, "samsung_preload_leisure_art-gallery.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 602
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_spa_s.png"

    const-string v2, "samsung_preload_activity_bath.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 603
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_take_out_coffee.png"

    const-string v2, "samsung_preload_food_taskout-coffee.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 604
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_suzhou.png"

    const-string v2, "samsung_preload_food_suzhou.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 605
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_baby_s.png"

    const-string v2, "samsung_preload_people_baby.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 606
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_sports.png"

    const-string v2, "samsung_preload_activity_exercise.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_photo.png"

    const-string v2, "samsung_preload_objects_photo.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 608
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_funeral_service.png"

    const-string v2, "samsung_preload_event_funeral-service.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 609
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_newyear.png"

    const-string v2, "samsung_preload_event_newyear.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 610
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_war_commemoration_s.png"

    const-string v2, "samsung_preload_event_war-commemoration.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_whiteday.png"

    const-string v2, "samsung_preload_event_whiteday.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 612
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_anniversary_yearend.png"

    const-string v2, "samsung_preload_event_yearend.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 613
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_candy.png"

    const-string v2, "samsung_preload_food_candy.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 614
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_sandwich.png"

    const-string v2, "samsung_preload_food_sandwich.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 615
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_food_steak.png"

    const-string v2, "samsung_preload_food_steak.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 616
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_christianity_s.png"

    const-string v2, "samsung_preload_activity_christianity.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 617
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_cooking_s.png"

    const-string v2, "samsung_preload_activity_cooking.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 618
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_leisure_shoot_s.png"

    const-string v2, "samsung_preload_activity_shoot.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 619
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_Buddhist_temple_s.png"

    const-string v2, "samsung_preload_activity_buddhist-temple.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_cathedral_s.png"

    const-string v2, "samsung_preload_activity_cathedral.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 621
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_islam_s.png"

    const-string v2, "samsung_preload_activity_islam.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 622
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_manicure.png"

    const-string v2, "samsung_preload_objects_manicure.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 623
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_mountain.png"

    const-string v2, "samsung_preload_objects_mountain.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    const-string v1, "cal_sticker_life_sea.png"

    const-string v2, "samsung_preload_objects_sea.png"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 625
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 720
    const-string v0, "calendar.db"

    const/16 v1, 0x258

    invoke-direct {p0, p1, v0, v3, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 93
    iput-boolean v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mInTestMode:Z

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createNoteField:Z

    .line 117
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTaskAccountId:J

    .line 120
    iput-object v3, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    .line 121
    iput-boolean v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->isM0stickers:Z

    .line 5434
    const-string v0, "substHolsCalendarVersion"

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->SUBST_HOLS_SETTING:Ljava/lang/String;

    .line 5435
    const-string v0, "legalSubstHoliday"

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->SUBST_HOLS_CALENDAR_NAME:Ljava/lang/String;

    .line 723
    new-instance v0, Lcom/android/common/content/SyncStateContentProviderHelper;

    invoke-direct {v0}, Lcom/android/common/content/SyncStateContentProviderHelper;-><init>()V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSyncState:Lcom/android/common/content/SyncStateContentProviderHelper;

    .line 725
    iput-object p1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    .line 726
    invoke-static {}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getInstance()Lcom/android/calendar/secfeature/SECCalendarFeatures;

    move-result-object v0

    sput-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    .line 728
    return-void
.end method

.method private CopyAssets(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 26
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4974
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    .line 4975
    .local v4, "assetManager":Landroid/content/res/AssetManager;
    const/4 v10, 0x0

    .line 4977
    .local v10, "files":[Ljava/lang/String;
    const-string v22, "CalendarDatabaseHelper"

    const-string v23, "CopyAssets"

    invoke-static/range {v22 .. v23}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4980
    :try_start_0
    const-string v22, "stickers"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 4985
    :goto_0
    const-string v22, "CalendarDatabaseHelper"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "len:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    array-length v0, v10

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4986
    array-length v0, v10

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_a

    .line 4987
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    array-length v0, v10

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v11, v0, :cond_a

    .line 4988
    const/4 v12, 0x0

    .line 4989
    .local v12, "in":Ljava/io/InputStream;
    const/4 v15, 0x0

    .line 4991
    .local v15, "out":Ljava/io/OutputStream;
    const/4 v6, 0x0

    .line 4992
    .local v6, "cursor":Landroid/database/Cursor;
    const/16 v21, 0x0

    .line 4995
    .local v21, "wrongGroup_cursor":Landroid/database/Cursor;
    :try_start_1
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "stickers/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v10, v11

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v12

    .line 4998
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v19

    .line 5000
    .local v19, "str":Ljava/lang/String;
    const-string v22, "mounted"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 5004
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 5005
    .local v3, "appPath":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/sticker/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    .line 5006
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5007
    .local v13, "mpath":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->isDirectory()Z

    move-result v22

    if-nez v22, :cond_0

    .line 5008
    invoke-virtual {v13}, Ljava/io/File;->mkdirs()Z

    .line 5009
    const/16 v22, 0x1

    const/16 v23, 0x0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Ljava/io/File;->setReadable(ZZ)Z

    .line 5010
    const/16 v22, 0x1

    const/16 v23, 0x0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 5014
    :cond_0
    new-instance v16, Ljava/io/FileOutputStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v10, v11

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5016
    .end local v15    # "out":Ljava/io/OutputStream;
    .local v16, "out":Ljava/io/OutputStream;
    :try_start_2
    aget-object v22, v10, v11

    const-string v23, "\'"

    const-string v24, "\'\'"

    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    const-string v23, "%"

    const-string v24, "`%"

    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v10, v11

    .line 5017
    aget-object v22, v10, v11

    const-string v23, "_"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 5019
    .local v20, "t":[Ljava/lang/String;
    const-string v22, "SELECT * FROM Stickers WHERE sticker_name=? AND sticker_group=?"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x3

    aget-object v25, v20, v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x2

    aget-object v25, v20, v25

    invoke-static/range {v25 .. v25}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 5024
    const-string v22, "SELECT * FROM Stickers WHERE sticker_name=? AND sticker_group!=?"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x3

    aget-object v25, v20, v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x2

    aget-object v25, v20, v25

    invoke-static/range {v25 .. v25}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 5028
    if-eqz v6, :cond_7

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v22

    if-nez v22, :cond_7

    if-eqz v21, :cond_7

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v22

    if-nez v22, :cond_7

    .line 5029
    const/16 v22, 0x3

    aget-object v22, v20, v22

    const-string v23, ".png"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_1

    .line 5030
    const/16 v22, 0x3

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v24, 0x3

    aget-object v24, v20, v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ".png"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v20, v22

    .line 5032
    :cond_1
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "INSERT INTO Stickers(sticker_name,filepath,sticker_group ) VALUES (\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x3

    aget-object v23, v20, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\' , \'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v10, v11

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\',\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x2

    aget-object v23, v20, v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\')"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5043
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v12, v1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 5045
    new-instance v17, Ljava/io/File;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v10, v11

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5046
    .local v17, "readR":Ljava/io/File;
    const/16 v22, 0x1

    const/16 v23, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setReadable(ZZ)Z

    .line 5047
    const/16 v22, 0x1

    const/16 v23, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 5049
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V

    .line 5050
    const/4 v12, 0x0

    .line 5051
    invoke-virtual/range {v16 .. v16}, Ljava/io/OutputStream;->flush()V

    .line 5052
    invoke-virtual/range {v16 .. v16}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 5053
    const/4 v15, 0x0

    .line 5057
    .end local v16    # "out":Ljava/io/OutputStream;
    .restart local v15    # "out":Ljava/io/OutputStream;
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 5058
    :cond_3
    if-eqz v21, :cond_4

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 4987
    .end local v3    # "appPath":Ljava/lang/String;
    .end local v13    # "mpath":Ljava/io/File;
    .end local v17    # "readR":Ljava/io/File;
    .end local v19    # "str":Ljava/lang/String;
    .end local v20    # "t":[Ljava/lang/String;
    :cond_4
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 4981
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v11    # "i":I
    .end local v12    # "in":Ljava/io/InputStream;
    .end local v15    # "out":Ljava/io/OutputStream;
    .end local v21    # "wrongGroup_cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 4982
    .local v7, "e":Ljava/io/IOException;
    const-string v22, "CalendarDatabaseHelper"

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 5002
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "i":I
    .restart local v12    # "in":Ljava/io/InputStream;
    .restart local v15    # "out":Ljava/io/OutputStream;
    .restart local v19    # "str":Ljava/lang/String;
    .restart local v21    # "wrongGroup_cursor":Landroid/database/Cursor;
    :cond_5
    :try_start_3
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 5054
    .end local v19    # "str":Ljava/lang/String;
    :catch_1
    move-exception v7

    .line 5055
    .local v7, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_4
    const-string v22, "CalendarDatabaseHelper"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Cause:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v7}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " Msg:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 5057
    if-eqz v6, :cond_6

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 5058
    :cond_6
    if-eqz v21, :cond_4

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 5035
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v15    # "out":Ljava/io/OutputStream;
    .restart local v3    # "appPath":Ljava/lang/String;
    .restart local v13    # "mpath":Ljava/io/File;
    .restart local v16    # "out":Ljava/io/OutputStream;
    .restart local v19    # "str":Ljava/lang/String;
    .restart local v20    # "t":[Ljava/lang/String;
    :cond_7
    if-eqz v6, :cond_2

    :try_start_5
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v22

    if-nez v22, :cond_2

    if-eqz v21, :cond_2

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v22

    if-lez v22, :cond_2

    .line 5037
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "UPDATE Stickers SET sticker_group=\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x2

    aget-object v23, v20, v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\' WHERE "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "sticker_name"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "=\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x3

    aget-object v23, v20, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\' AND "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "sticker_group"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "!=\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x2

    aget-object v23, v20, v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto/16 :goto_3

    .line 5054
    .end local v20    # "t":[Ljava/lang/String;
    :catch_2
    move-exception v7

    move-object/from16 v15, v16

    .end local v16    # "out":Ljava/io/OutputStream;
    .restart local v15    # "out":Ljava/io/OutputStream;
    goto/16 :goto_5

    .line 5057
    .end local v3    # "appPath":Ljava/lang/String;
    .end local v13    # "mpath":Ljava/io/File;
    .end local v19    # "str":Ljava/lang/String;
    :catchall_0
    move-exception v22

    :goto_6
    if-eqz v6, :cond_8

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 5058
    :cond_8
    if-eqz v21, :cond_9

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v22

    .line 5064
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v11    # "i":I
    .end local v12    # "in":Ljava/io/InputStream;
    .end local v15    # "out":Ljava/io/OutputStream;
    .end local v21    # "wrongGroup_cursor":Landroid/database/Cursor;
    :cond_a
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->isM0sticker(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 5065
    const-string v22, "SELECT filepath FROM Stickers"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 5067
    .local v5, "c":Landroid/database/Cursor;
    if-eqz v5, :cond_c

    :try_start_6
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v22

    if-lez v22, :cond_c

    .line 5068
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    .line 5070
    :cond_b
    const-string v22, "filepath"

    move-object/from16 v0, v22

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 5071
    .local v9, "filepath":Ljava/lang/String;
    const-string v22, "/"

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 5072
    .local v8, "filename":[Ljava/lang/String;
    sget-object v22, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    array-length v0, v8

    move/from16 v23, v0

    add-int/lit8 v23, v23, -0x1

    aget-object v23, v8, v23

    invoke-virtual/range {v22 .. v23}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 5073
    .local v14, "newName":Ljava/lang/String;
    const-string v22, "_"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 5074
    .local v18, "selection":[Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "UPDATE Stickers SET sticker_group=\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x2

    aget-object v23, v18, v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " WHERE "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "filepath"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "=\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5078
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v22

    if-nez v22, :cond_b

    .line 5081
    .end local v8    # "filename":[Ljava/lang/String;
    .end local v9    # "filepath":Ljava/lang/String;
    .end local v14    # "newName":Ljava/lang/String;
    .end local v18    # "selection":[Ljava/lang/String;
    :cond_c
    if-eqz v5, :cond_d

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 5084
    .end local v5    # "c":Landroid/database/Cursor;
    :cond_d
    return-void

    .line 5081
    .restart local v5    # "c":Landroid/database/Cursor;
    :catchall_1
    move-exception v22

    if-eqz v5, :cond_e

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v22

    .line 5057
    .end local v5    # "c":Landroid/database/Cursor;
    .restart local v3    # "appPath":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "i":I
    .restart local v12    # "in":Ljava/io/InputStream;
    .restart local v13    # "mpath":Ljava/io/File;
    .restart local v16    # "out":Ljava/io/OutputStream;
    .restart local v19    # "str":Ljava/lang/String;
    .restart local v21    # "wrongGroup_cursor":Landroid/database/Cursor;
    :catchall_2
    move-exception v22

    move-object/from16 v15, v16

    .end local v16    # "out":Ljava/io/OutputStream;
    .restart local v15    # "out":Ljava/io/OutputStream;
    goto/16 :goto_6
.end method

.method private Upgrade403To50xStickers(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 21
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 5322
    const-string v1, "CalendarDatabaseHelper"

    const-string v2, "Upgrade403To50xStickers"

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5324
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/stickers/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 5326
    .local v19, "stickerPath":Ljava/lang/String;
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    new-array v14, v1, [Ljava/lang/String;

    .line 5327
    .local v14, "keyArray":[Ljava/lang/String;
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v14}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 5329
    const/4 v13, 0x0

    .local v13, "index":I
    :goto_0
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-ge v13, v1, :cond_0

    .line 5330
    aget-object v18, v14, v13

    .line 5331
    .local v18, "oldSticker":Ljava/lang/String;
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMap:Ljava/util/HashMap;

    aget-object v2, v14, v13

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 5333
    .local v16, "newSticker":Ljava/lang/String;
    const-string v1, "_"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 5334
    .local v17, "oldOne":[Ljava/lang/String;
    const-string v1, "_"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 5335
    .local v15, "newOne":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 5338
    .local v20, "updateFilePath":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UPDATE Stickers SET sticker_name=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v2, v15, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\',"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sticker_group"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, v15, v2

    invoke-static {v2}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\',"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "filepath"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' WHERE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "filepath"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5329
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_0

    .line 5343
    .end local v15    # "newOne":[Ljava/lang/String;
    .end local v16    # "newSticker":Ljava/lang/String;
    .end local v17    # "oldOne":[Ljava/lang/String;
    .end local v18    # "oldSticker":Ljava/lang/String;
    .end local v20    # "updateFilePath":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v9

    .line 5344
    .local v9, "assetManager":Landroid/content/res/AssetManager;
    const/4 v12, 0x0

    .line 5347
    .local v12, "files":[Ljava/lang/String;
    :try_start_0
    const-string v1, "stickers"

    invoke-virtual {v9, v1}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 5354
    :goto_1
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "sticker_name"

    aput-object v2, v3, v1

    .line 5357
    .local v3, "STICKERS_PROJECTION":[Ljava/lang/String;
    const-string v2, "Stickers"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 5360
    .local v10, "cursorStickerEntryCheck":Landroid/database/Cursor;
    if-eqz v10, :cond_1

    if-eqz v12, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    array-length v2, v12

    if-ge v1, v2, :cond_1

    .line 5361
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->CopyAssets(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 5363
    :cond_1
    if-eqz v10, :cond_2

    .line 5364
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 5366
    :cond_2
    const/4 v10, 0x0

    .line 5368
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->updateFilepath(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 5369
    return-void

    .line 5348
    .end local v3    # "STICKERS_PROJECTION":[Ljava/lang/String;
    .end local v10    # "cursorStickerEntryCheck":Landroid/database/Cursor;
    :catch_0
    move-exception v11

    .line 5349
    .local v11, "e":Ljava/io/IOException;
    const-string v1, "CalendarDatabaseHelper"

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/android/providers/calendar/CalendarDatabaseHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/providers/calendar/CalendarDatabaseHelper;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private bootstrapDB(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    .line 808
    const-string v1, "CalendarDatabaseHelper"

    const-string v2, "Bootstrapping database"

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSyncState:Lcom/android/common/content/SyncStateContentProviderHelper;

    invoke-virtual {v1, p1}, Lcom/android/common/content/SyncStateContentProviderHelper;->createDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 812
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createColorsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 814
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 816
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createEventsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 818
    const-string v1, "CREATE TABLE EventsRawTimes (_id INTEGER PRIMARY KEY,event_id INTEGER NOT NULL,dtstart2445 TEXT,dtend2445 TEXT,originalInstanceTime2445 TEXT,lastDate2445 TEXT,UNIQUE (event_id));"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 828
    const-string v1, "CREATE TABLE Instances (_id INTEGER PRIMARY KEY,event_id INTEGER,begin INTEGER,end INTEGER,startDay INTEGER,endDay INTEGER,startMinute INTEGER,endMinute INTEGER,UNIQUE (event_id, begin, end));"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 843
    const-string v1, "CREATE INDEX instancesStartDayIndex ON Instances (startDay);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 847
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarMetaDataTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 849
    invoke-direct {p0, p1, v3}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarCacheTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 851
    const-string v1, "CREATE TABLE Attendees (_id INTEGER PRIMARY KEY,event_id INTEGER,attendeeName TEXT,attendeeEmail TEXT,attendeeStatus INTEGER,attendeeRelationship INTEGER,attendeeType INTEGER,attendeeIdentity TEXT,attendeeIdNamespace TEXT,attendee_contact_id INTEGER);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 866
    const-string v1, "CREATE INDEX attendeesEventIdIndex ON Attendees (event_id);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 870
    const-string v1, "CREATE TABLE Reminders (_id INTEGER PRIMARY KEY,event_id INTEGER,minutes INTEGER,method INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 878
    const-string v1, "CREATE INDEX remindersEventIdIndex ON Reminders (event_id);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 883
    const-string v1, "CREATE TABLE CalendarAlerts (_id INTEGER PRIMARY KEY,event_id INTEGER,begin INTEGER NOT NULL,end INTEGER NOT NULL,alarmTime INTEGER NOT NULL,creationTime INTEGER NOT NULL DEFAULT 0,receivedTime INTEGER NOT NULL DEFAULT 0,notifyTime INTEGER NOT NULL DEFAULT 0,state INTEGER NOT NULL,minutes INTEGER,UNIQUE (alarmTime, begin, event_id));"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 903
    const-string v1, "CREATE INDEX calendarAlertsEventIdIndex ON CalendarAlerts (event_id);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 907
    const-string v1, "CREATE TABLE ExtendedProperties (_id INTEGER PRIMARY KEY,event_id INTEGER,name TEXT,value TEXT);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 914
    const-string v1, "CREATE INDEX extendedPropertiesEventIdIndex ON ExtendedProperties (event_id);"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 923
    const-string v1, "CREATE TRIGGER events_cleanup_delete DELETE ON Events BEGIN DELETE FROM Instances WHERE event_id=old._id;DELETE FROM EventsRawTimes WHERE event_id=old._id;DELETE FROM Attendees WHERE event_id=old._id;DELETE FROM Reminders WHERE event_id=old._id;DELETE FROM CalendarAlerts WHERE event_id=old._id;DELETE FROM ExtendedProperties WHERE event_id=old._id;DELETE FROM Memos WHERE event_id = old._id AND event_type = 1;DELETE FROM Images WHERE event_id = old._id AND event_type = 1;DELETE FROM Maps WHERE event_id = old._id;DELETE FROM Facebooks WHERE event_id = old._id;DELETE FROM Notes WHERE event_id = old._id;END"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 930
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createColorsTriggers(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 934
    const-string v1, "CREATE TRIGGER original_sync_update UPDATE OF _sync_id ON Events BEGIN UPDATE Events SET original_sync_id=new._sync_id WHERE original_id=old._id; END"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 936
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createQSMemosTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 937
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createMemosTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 938
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createImagesTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 939
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createMapsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 940
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createFacebooksTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 942
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createNotesTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 943
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createMyCalendar(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 946
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->areNationalHolidaysSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 947
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "JAPAN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 948
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 949
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "true"

    const-string v2, "isKnoxMode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 950
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createJapanHolidayTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 960
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createTasksTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 961
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createTasksRemindersTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 962
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createTasksAccountsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 963
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createGroupTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 964
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createTasksView(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 965
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createMyTask(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 967
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createStickerTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 968
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->CopyAssets(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 971
    invoke-static {p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createEventsView(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 973
    const/4 v1, 0x0

    invoke-virtual {p0, v3, v1, v3}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->scheduleSync(Landroid/accounts/Account;ZLjava/lang/String;)V

    .line 974
    return-void

    .line 954
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createLegalHolidayTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 955
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createSubstituteHolidayTable(Landroid/database/sqlite/SQLiteDatabase;)I

    goto :goto_0
.end method

.method public static calendarEmailAddressFromFeedUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "feed"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x5

    .line 4767
    const-string v3, "/"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 4768
    .local v1, "pathComponents":[Ljava/lang/String;
    array-length v3, v1

    if-le v3, v4, :cond_0

    const-string v3, "feeds"

    const/4 v4, 0x4

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4770
    const/4 v3, 0x5

    :try_start_0
    aget-object v3, v1, v3

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 4778
    :goto_0
    return-object v2

    .line 4771
    :catch_0
    move-exception v0

    .line 4772
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v3, "CalendarDatabaseHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unable to url decode the email address in calendar "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4777
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_0
    const-string v3, "CalendarDatabaseHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unable to find the email address in calendar "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private cleanupInstancesTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 5453
    :try_start_0
    const-string v1, "Instances"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5455
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 5456
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "minInstance"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5457
    const-string v1, "maxInstance"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5458
    const-string v1, "CalendarMetaData"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5461
    return-void

    .line 5459
    .end local v0    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v1

    throw v1
.end method

.method private cleanupSubstHolidays(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 5465
    :try_start_0
    const-string v0, "Calendars"

    const-string v1, "name=\'legalSubstHoliday\'"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5468
    return-void

    .line 5466
    :catchall_0
    move-exception v0

    throw v0
.end method

.method static copyEventRelatedTables(Landroid/database/sqlite/SQLiteDatabase;JJ)V
    .locals 7
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "newId"    # J
    .param p3, "id"    # J

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4919
    const-string v0, "INSERT INTO Reminders ( event_id, minutes,method) SELECT ?,minutes,method FROM Reminders WHERE event_id = ?"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4926
    const-string v0, "INSERT INTO Attendees (event_id,attendeeName,attendeeEmail,attendeeStatus,attendeeRelationship,attendeeType,attendeeIdentity,attendeeIdNamespace) SELECT ?,attendeeName,attendeeEmail,attendeeStatus,attendeeRelationship,attendeeType,attendeeIdentity,attendeeIdNamespace FROM Attendees WHERE event_id = ?"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4933
    const-string v0, "INSERT INTO ExtendedProperties (event_id,name,value) SELECT ?, name,value FROM ExtendedProperties WHERE event_id = ?"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4940
    return-void
.end method

.method private copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 5
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "out"    # Ljava/io/OutputStream;

    .prologue
    .line 5404
    const/16 v3, 0x400

    new-array v0, v3, [B

    .line 5407
    .local v0, "buffer":[B
    :goto_0
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .local v2, "read":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 5408
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 5410
    .end local v2    # "read":I
    :catch_0
    move-exception v1

    .line 5412
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 5413
    const-string v3, "CalendarDatabaseHelper"

    const-string v4, "copy file Fail!!"

    invoke-static {v3, v4}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5415
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    return-void
.end method

.method private createCalendarCacheTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldTimezoneDbVersion"    # Ljava/lang/String;

    .prologue
    .line 1559
    const-string v0, "DROP TABLE IF EXISTS CalendarCache;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1562
    const-string v0, "CREATE TABLE IF NOT EXISTS CalendarCache (_id INTEGER PRIMARY KEY,key TEXT NOT NULL,value TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1568
    invoke-direct {p0, p1, p2}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->initCalendarCacheTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 1569
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->updateCalendarCacheTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1570
    return-void
.end method

.method private createCalendarMetaDataTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1539
    const-string v0, "CREATE TABLE CalendarMetaData (_id INTEGER PRIMARY KEY,localTimezone TEXT,minInstance INTEGER,maxInstance INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1545
    return-void
.end method

.method private createCalendarMetaDataTable59(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1548
    const-string v0, "CREATE TABLE CalendarMetaData (_id INTEGER PRIMARY KEY,localTimezone TEXT,minInstance INTEGER,maxInstance INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1554
    return-void
.end method

.method private createCalendarsCleanup200(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1532
    const-string v0, "CREATE TRIGGER calendar_cleanup DELETE ON Calendars BEGIN DELETE FROM Events WHERE calendar_id=old._id;END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1536
    return-void
.end method

.method private createCalendarsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1310
    const-string v0, "CREATE TABLE Calendars (_id INTEGER PRIMARY KEY,account_name TEXT,account_type TEXT,_sync_id TEXT,dirty INTEGER,mutators TEXT,name TEXT,calendar_displayName TEXT,calendar_color INTEGER,calendar_color_index TEXT,calendar_access_level INTEGER,visible INTEGER NOT NULL DEFAULT 1,sync_events INTEGER NOT NULL DEFAULT 0,calendar_location TEXT,calendar_timezone TEXT,ownerAccount TEXT, isPrimary INTEGER, canOrganizerRespond INTEGER NOT NULL DEFAULT 1,canModifyTimeZone INTEGER DEFAULT 1,canPartiallyUpdate INTEGER DEFAULT 0,maxReminders INTEGER DEFAULT 5,allowedReminders TEXT DEFAULT \'0,1\',allowedAvailability TEXT DEFAULT \'0,1\',allowedAttendeeTypes TEXT DEFAULT \'0,1,2\',deleted INTEGER NOT NULL DEFAULT 0,cal_sync1 TEXT,cal_sync2 TEXT,cal_sync3 TEXT,cal_sync4 TEXT,cal_sync5 TEXT,cal_sync6 TEXT,cal_sync7 TEXT,cal_sync8 TEXT,cal_sync9 TEXT,cal_sync10 TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1349
    const-string v0, "CREATE TRIGGER calendar_cleanup DELETE ON Calendars BEGIN DELETE FROM Events WHERE calendar_id=old._id;END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1353
    return-void
.end method

.method private createCalendarsTable200(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1500
    const-string v0, "CREATE TABLE Calendars (_id INTEGER PRIMARY KEY,_sync_account TEXT,_sync_account_type TEXT,_sync_id TEXT,_sync_version TEXT,_sync_time TEXT,_sync_local_id INTEGER,_sync_dirty INTEGER,_sync_mark INTEGER,name TEXT,displayName TEXT,hidden INTEGER NOT NULL DEFAULT 0,color INTEGER,access_level INTEGER,selected INTEGER NOT NULL DEFAULT 1,sync_events INTEGER NOT NULL DEFAULT 0,location TEXT,timezone TEXT,ownerAccount TEXT, organizerCanRespond INTEGER NOT NULL DEFAULT 1,deleted INTEGER NOT NULL DEFAULT 0,sync1 TEXT,sync2 TEXT,sync3 TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1527
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarsCleanup200(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1528
    return-void
.end method

.method private createCalendarsTable202(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1468
    const-string v0, "CREATE TABLE Calendars (_id INTEGER PRIMARY KEY,_sync_account TEXT,_sync_account_type TEXT,_sync_id TEXT,_sync_version TEXT,_sync_time TEXT,_sync_local_id INTEGER,_sync_dirty INTEGER,_sync_mark INTEGER,name TEXT,displayName TEXT,color INTEGER,access_level INTEGER,selected INTEGER NOT NULL DEFAULT 1,sync_events INTEGER NOT NULL DEFAULT 0,location TEXT,timezone TEXT,ownerAccount TEXT, organizerCanRespond INTEGER NOT NULL DEFAULT 1,deleted INTEGER NOT NULL DEFAULT 0,sync1 TEXT,sync2 TEXT,sync3 TEXT,sync4 TEXT,sync5 TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1496
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarsCleanup200(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1497
    return-void
.end method

.method private createCalendarsTable205(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1435
    const-string v0, "CREATE TABLE Calendars (_id INTEGER PRIMARY KEY,_sync_account TEXT,_sync_account_type TEXT,_sync_id TEXT,_sync_version TEXT,_sync_time TEXT,_sync_dirty INTEGER,name TEXT,displayName TEXT,color INTEGER,access_level INTEGER,visible INTEGER NOT NULL DEFAULT 1,sync_events INTEGER NOT NULL DEFAULT 0,location TEXT,timezone TEXT,ownerAccount TEXT, canOrganizerRespond INTEGER NOT NULL DEFAULT 1,canModifyTimeZone INTEGER DEFAULT 1, maxReminders INTEGER DEFAULT 5,deleted INTEGER NOT NULL DEFAULT 0,sync1 TEXT,sync2 TEXT,sync3 TEXT,sync4 TEXT,sync5 TEXT,sync6 TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1464
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarsCleanup200(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1465
    return-void
.end method

.method private createCalendarsTable300(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1397
    const-string v0, "CREATE TABLE Calendars (_id INTEGER PRIMARY KEY,account_name TEXT,account_type TEXT,_sync_id TEXT,_sync_version TEXT,_sync_time TEXT,dirty INTEGER,name TEXT,displayName TEXT,calendar_color INTEGER,access_level INTEGER,visible INTEGER NOT NULL DEFAULT 1,sync_events INTEGER NOT NULL DEFAULT 0,calendar_location TEXT,calendar_timezone TEXT,ownerAccount TEXT, canOrganizerRespond INTEGER NOT NULL DEFAULT 1,canModifyTimeZone INTEGER DEFAULT 1,maxReminders INTEGER DEFAULT 5,allowedReminders TEXT DEFAULT \'0,1,2\',deleted INTEGER NOT NULL DEFAULT 0,sync1 TEXT,sync2 TEXT,sync3 TEXT,sync4 TEXT,sync5 TEXT,sync6 TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1428
    const-string v0, "CREATE TRIGGER calendar_cleanup DELETE ON Calendars BEGIN DELETE FROM Events WHERE calendar_id=old._id;END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1432
    return-void
.end method

.method private createCalendarsTable303(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1254
    const-string v0, "CREATE TABLE Calendars (_id INTEGER PRIMARY KEY,account_name TEXT,account_type TEXT,_sync_id TEXT,_sync_version TEXT,_sync_time TEXT,dirty INTEGER,name TEXT,displayName TEXT,calendar_color INTEGER,access_level INTEGER,visible INTEGER NOT NULL DEFAULT 1,sync_events INTEGER NOT NULL DEFAULT 0,calendar_location TEXT,calendar_timezone TEXT,ownerAccount TEXT, canOrganizerRespond INTEGER NOT NULL DEFAULT 1,canModifyTimeZone INTEGER DEFAULT 1,maxReminders INTEGER DEFAULT 5,allowedReminders TEXT DEFAULT \'0,1\',deleted INTEGER NOT NULL DEFAULT 0,cal_sync1 TEXT,cal_sync2 TEXT,cal_sync3 TEXT,cal_sync4 TEXT,cal_sync5 TEXT,cal_sync6 TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1285
    const-string v0, "CREATE TRIGGER calendar_cleanup DELETE ON Calendars BEGIN DELETE FROM Events WHERE calendar_id=old._id;END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1289
    return-void
.end method

.method private createCalendarsTable305(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1356
    const-string v0, "CREATE TABLE Calendars (_id INTEGER PRIMARY KEY,account_name TEXT,account_type TEXT,_sync_id TEXT,dirty INTEGER,name TEXT,calendar_displayName TEXT,calendar_color INTEGER,calendar_access_level INTEGER,visible INTEGER NOT NULL DEFAULT 1,sync_events INTEGER NOT NULL DEFAULT 0,calendar_location TEXT,calendar_timezone TEXT,ownerAccount TEXT, canOrganizerRespond INTEGER NOT NULL DEFAULT 1,canModifyTimeZone INTEGER DEFAULT 1,canPartiallyUpdate INTEGER DEFAULT 0,maxReminders INTEGER DEFAULT 5,allowedReminders TEXT DEFAULT \'0,1\',deleted INTEGER NOT NULL DEFAULT 0,cal_sync1 TEXT,cal_sync2 TEXT,cal_sync3 TEXT,cal_sync4 TEXT,cal_sync5 TEXT,cal_sync6 TEXT,cal_sync7 TEXT,cal_sync8 TEXT,cal_sync9 TEXT,cal_sync10 TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1390
    const-string v0, "CREATE TRIGGER calendar_cleanup DELETE ON Calendars BEGIN DELETE FROM Events WHERE calendar_id=old._id;END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1394
    return-void
.end method

.method private createColorsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1293
    const-string v0, "CREATE TABLE Colors (_id INTEGER PRIMARY KEY,account_name TEXT NOT NULL,account_type TEXT NOT NULL,data TEXT,color_type INTEGER NOT NULL,color_index TEXT NOT NULL,color INTEGER NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1302
    return-void
.end method

.method private createEventsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 982
    const-string v0, "CREATE TABLE Events (_id INTEGER PRIMARY KEY AUTOINCREMENT,_sync_id TEXT,dirty INTEGER,mutators TEXT,lastSynced INTEGER DEFAULT 0,calendar_id INTEGER NOT NULL,title TEXT,eventLocation TEXT,description TEXT,eventColor INTEGER,eventColor_index TEXT,eventStatus INTEGER,selfAttendeeStatus INTEGER NOT NULL DEFAULT 0,dtstart INTEGER,dtend INTEGER,eventTimezone TEXT,duration TEXT,allDay INTEGER NOT NULL DEFAULT 0,accessLevel INTEGER NOT NULL DEFAULT 0,availability INTEGER NOT NULL DEFAULT 0,hasAlarm INTEGER NOT NULL DEFAULT 0,hasExtendedProperties INTEGER NOT NULL DEFAULT 0,rrule TEXT,rdate TEXT,exrule TEXT,exdate TEXT,original_id INTEGER,original_sync_id TEXT,originalInstanceTime INTEGER,originalAllDay INTEGER,lastDate INTEGER,hasAttendeeData INTEGER NOT NULL DEFAULT 0,guestsCanModify INTEGER NOT NULL DEFAULT 0,guestsCanInviteOthers INTEGER NOT NULL DEFAULT 1,guestsCanSeeGuests INTEGER NOT NULL DEFAULT 1,organizer STRING,isOrganizer INTEGER,deleted INTEGER NOT NULL DEFAULT 0,eventEndTimezone TEXT,customAppPackage TEXT,customAppUri TEXT,uid2445 TEXT,sync_data1 TEXT,sync_data2 TEXT,sync_data3 TEXT,sync_data4 TEXT,sync_data5 TEXT,sync_data6 TEXT,sync_data7 TEXT,sync_data8 TEXT,sync_data9 TEXT,sync_data10 TEXT,latitude INTEGER,longitude INTEGER,contact_data_id INTEGER,contact_id INTEGER,contactEventType INTEGER NOT NULL DEFAULT 0,contact_account_type TEXT,availabilityStatus INTEGER NOT NULL DEFAULT 0,facebook_schedule_id VARCHAR(20),facebook_service_provider INTEGER,facebook_event_type INTEGER,facebook_owner VARCHAR(20),facebook_hostname TEXT,facebook_post_time TIMESTAMP,facebook_photo_url VARCHAR(512),facebook_mem_count INTEGER,phone_number TEXT,sticker_type INTEGER,sticker_ename TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1071
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1072
    const-string v0, "ALTER TABLE Events ADD setLunar INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1078
    :cond_0
    const-string v0, "CREATE INDEX eventsCalendarIdIndex ON Events (calendar_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1080
    return-void
.end method

.method private createEventsTable300(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1172
    const-string v0, "CREATE TABLE Events (_id INTEGER PRIMARY KEY,_sync_id TEXT,_sync_version TEXT,_sync_time TEXT,_sync_local_id INTEGER,dirty INTEGER,_sync_mark INTEGER,calendar_id INTEGER NOT NULL,htmlUri TEXT,title TEXT,eventLocation TEXT,description TEXT,eventStatus INTEGER,selfAttendeeStatus INTEGER NOT NULL DEFAULT 0,commentsUri TEXT,dtstart INTEGER,dtend INTEGER,eventTimezone TEXT,duration TEXT,allDay INTEGER NOT NULL DEFAULT 0,accessLevel INTEGER NOT NULL DEFAULT 0,availability INTEGER NOT NULL DEFAULT 0,hasAlarm INTEGER NOT NULL DEFAULT 0,hasExtendedProperties INTEGER NOT NULL DEFAULT 0,rrule TEXT,rdate TEXT,exrule TEXT,exdate TEXT,original_sync_id TEXT,originalInstanceTime INTEGER,originalAllDay INTEGER,lastDate INTEGER,hasAttendeeData INTEGER NOT NULL DEFAULT 0,guestsCanModify INTEGER NOT NULL DEFAULT 0,guestsCanInviteOthers INTEGER NOT NULL DEFAULT 1,guestsCanSeeGuests INTEGER NOT NULL DEFAULT 1,organizer STRING,deleted INTEGER NOT NULL DEFAULT 0,eventEndTimezone TEXT,sync_data1 TEXT,latitude INTEGER,longitude INTEGER,contact_data_id INTEGER,contact_id INTEGER,contactEventType INTEGER NOT NULL DEFAULT 0,contact_account_type TEXT,availabilityStatus INTEGER NOT NULL DEFAULT 0,facebook_schedule_id VARCHAR(20),facebook_service_provider INTEGER,facebook_event_type INTEGER,facebook_owner VARCHAR(20),facebook_hostname TEXT,facebook_post_time TIMESTAMP,facebook_photo_url VARCHAR(512),facebook_mem_count INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1245
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1246
    const-string v0, "ALTER TABLE Events ADD setLunar INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1250
    :cond_0
    const-string v0, "CREATE INDEX eventsCalendarIdIndex ON Events (calendar_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1251
    return-void
.end method

.method private createEventsTable307(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1083
    const-string v0, "CREATE TABLE Events (_id INTEGER PRIMARY KEY AUTOINCREMENT,_sync_id TEXT,dirty INTEGER,lastSynced INTEGER DEFAULT 0,calendar_id INTEGER NOT NULL,title TEXT,eventLocation TEXT,description TEXT,eventColor INTEGER,eventStatus INTEGER,selfAttendeeStatus INTEGER NOT NULL DEFAULT 0,dtstart INTEGER,dtend INTEGER,eventTimezone TEXT,duration TEXT,allDay INTEGER NOT NULL DEFAULT 0,accessLevel INTEGER NOT NULL DEFAULT 0,availability INTEGER NOT NULL DEFAULT 0,hasAlarm INTEGER NOT NULL DEFAULT 0,hasExtendedProperties INTEGER NOT NULL DEFAULT 0,rrule TEXT,rdate TEXT,exrule TEXT,exdate TEXT,original_id INTEGER,original_sync_id TEXT,originalInstanceTime INTEGER,originalAllDay INTEGER,lastDate INTEGER,hasAttendeeData INTEGER NOT NULL DEFAULT 0,guestsCanModify INTEGER NOT NULL DEFAULT 0,guestsCanInviteOthers INTEGER NOT NULL DEFAULT 1,guestsCanSeeGuests INTEGER NOT NULL DEFAULT 1,organizer STRING,deleted INTEGER NOT NULL DEFAULT 0,eventEndTimezone TEXT,sync_data1 TEXT,sync_data2 TEXT,sync_data3 TEXT,sync_data4 TEXT,sync_data5 TEXT,sync_data6 TEXT,sync_data7 TEXT,sync_data8 TEXT,sync_data9 TEXT,sync_data10 TEXT,latitude INTEGER,longitude INTEGER,contact_data_id INTEGER,contact_id INTEGER,contactEventType INTEGER NOT NULL DEFAULT 0,contact_account_type TEXT,availabilityStatus INTEGER NOT NULL DEFAULT 0,facebook_schedule_id VARCHAR(20),facebook_service_provider INTEGER,facebook_event_type INTEGER,facebook_owner VARCHAR(20),facebook_hostname TEXT,facebook_post_time TIMESTAMP,facebook_photo_url VARCHAR(512),facebook_mem_count INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1162
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1163
    const-string v0, "ALTER TABLE Events ADD setLunar INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1167
    :cond_0
    const-string v0, "CREATE INDEX eventsCalendarIdIndex ON Events (calendar_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1168
    return-void
.end method

.method private static createEventsView(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4231
    const-string v1, "DROP VIEW IF EXISTS view_events;"

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4232
    const-string v0, "SELECT Events._id AS _id,title,description,eventLocation,eventColor,eventColor_index,eventStatus,selfAttendeeStatus,dtstart,dtend,duration,eventTimezone,eventEndTimezone,allDay,accessLevel,availability,hasAlarm,hasExtendedProperties,rrule,rdate,exrule,exdate,original_sync_id,original_id,originalInstanceTime,originalAllDay,lastDate,hasAttendeeData,calendar_id,guestsCanInviteOthers,guestsCanModify,guestsCanSeeGuests,organizer,COALESCE(isOrganizer, organizer = ownerAccount) AS isOrganizer,customAppPackage,customAppUri,uid2445,sync_data1,sync_data2,sync_data3,sync_data4,sync_data5,sync_data6,sync_data7,sync_data8,sync_data9,sync_data10,Events.deleted AS deleted,Events._sync_id AS _sync_id,Events.dirty AS dirty,Events.mutators AS mutators,lastSynced,Calendars.account_name AS account_name,Calendars.account_type AS account_type,latitude,longitude,contact_data_id,contact_id,contactEventType,contact_account_type,availabilityStatus,facebook_schedule_id,facebook_service_provider,facebook_event_type,facebook_owner,facebook_hostname,facebook_post_time,facebook_photo_url,facebook_mem_count,phone_number,sticker_type,sticker_ename,sticker_group,filepath,packageId,downloaded,calendar_timezone,calendar_displayName,calendar_location,visible,calendar_color,calendar_color_index,calendar_access_level,maxReminders,allowedReminders,allowedAttendeeTypes,allowedAvailability,canOrganizerRespond,canModifyTimeZone,canPartiallyUpdate,cal_sync1,cal_sync2,cal_sync3,cal_sync4,cal_sync5,cal_sync6,cal_sync7,cal_sync8,cal_sync9,cal_sync10,ownerAccount,sync_events,ifnull(eventColor,calendar_color) AS displayColor"

    .line 4349
    .local v0, "eventsSelect":Ljava/lang/String;
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4350
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", setLunar"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4353
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Events"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " JOIN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Calendars"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ON ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Events"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "calendar_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Calendars"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "))"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " LEFT OUTER JOIN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Stickers"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " On ( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Events"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sticker_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Stickers"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4360
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE VIEW view_events AS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4361
    return-void
.end method

.method private createFacebooksTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4698
    const-string v0, "CREATE TABLE Facebooks (_id INTEGER PRIMARY KEY,event_id INTEGER,photo BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4703
    return-void
.end method

.method private createGroupTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4598
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "CREATE TABLE "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, "TaskGroup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " INTEGER PRIMARY KEY,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "_accountId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " INTEGER,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "groupName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " TEXT,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "selected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " INTEGER NOT NULL DEFAULT 1,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "group_order"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " INTEGER DEFAULT 0);"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4607
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "INSERT INTO "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, "TaskGroup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "groupName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "_accountId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "selected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "group_order"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ") VALUES (\'Default\',"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTaskAccountId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ",1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ",0)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4615
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "CREATE TRIGGER deleteTaskGroup DELETE ON "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, "TaskGroup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " BEGIN DELETE FROM Tasks"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "groupId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "=old._id; END "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4621
    return-void
.end method

.method private createImagesTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4681
    const-string v0, "CREATE TABLE Images (_id INTEGER PRIMARY KEY,event_id INTEGER,filepath TEXT,event_type INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4687
    return-void
.end method

.method private createJapanHolidayTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v6, 0x1

    .line 5607
    iget-object v4, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 5608
    .local v2, "r":Landroid/content/res/Resources;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 5609
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "account_name"

    const-string v5, "local"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5610
    const-string v4, "account_type"

    const-string v5, "LOCAL"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5611
    const-string v4, "_sync_id"

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5612
    const-string v4, "name"

    const-string v5, "legalHoliday"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5613
    const-string v4, "calendar_displayName"

    const v5, 0x7f060081

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5614
    const-string v4, "calendar_access_level"

    const/16 v5, 0xc8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5615
    const-string v4, "visible"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5616
    const-string v4, "sync_events"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5617
    const-string v4, "calendar_color"

    const/16 v5, 0xd4

    const/16 v6, 0x3f

    const/16 v7, 0x16

    invoke-static {v5, v6, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5618
    const-string v4, "ownerAccount"

    const-string v5, "local"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5619
    new-instance v1, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v4, "Calendars"

    invoke-direct {v1, p1, v4}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 5620
    .local v1, "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    invoke-virtual {v1, v3}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v4

    long-to-int v0, v4

    .line 5622
    .local v0, "calendar_id":I
    invoke-virtual {p0, p1, v0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->insertJANEvent(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 5624
    invoke-virtual {v1}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 5625
    return-void
.end method

.method private createLegalHolidayTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 17
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 5527
    sget-object v2, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v2}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->areNationalHolidaysSupported()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 5529
    sget-object v2, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getCalendarHoliday(Landroid/content/Context;)Lcom/android/calendar/secfeature/holidays/CalendarHoliday;

    move-result-object v8

    .line 5531
    .local v8, "calHol":Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    if-nez v8, :cond_1

    .line 5604
    .end local v8    # "calHol":Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    :cond_0
    :goto_0
    return-void

    .line 5535
    .restart local v8    # "calHol":Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    :cond_1
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 5536
    .local v15, "values":Landroid/content/ContentValues;
    const-string v2, "account_name"

    const-string v3, "local"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5537
    const-string v2, "account_type"

    const-string v3, "LOCAL"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5538
    const-string v2, "_sync_id"

    const-string v3, "1"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5539
    const-string v2, "name"

    const-string v3, "legalHoliday"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5540
    const-string v2, "calendar_displayName"

    invoke-virtual {v8}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayCalendarName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5541
    const-string v2, "calendar_access_level"

    const/16 v3, 0xc8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5542
    const-string v2, "visible"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5543
    const-string v2, "sync_events"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5544
    const-string v2, "calendar_color"

    invoke-virtual {v8}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayCalendarAccountColor()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5545
    const-string v2, "ownerAccount"

    const-string v3, "local"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5547
    const/4 v9, 0x0

    .line 5548
    .local v9, "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    const/4 v11, 0x0

    .line 5551
    .local v11, "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    :try_start_0
    new-instance v10, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v2, "Calendars"

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v2}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5552
    .end local v9    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .local v10, "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    :try_start_1
    invoke-virtual {v10, v15}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v6

    .line 5555
    .local v6, "calId":J
    invoke-virtual {v8}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayCount()I

    move-result v14

    .line 5556
    .local v14, "size":I
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 5557
    .local v4, "value":Landroid/content/ContentValues;
    new-instance v12, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v2, "Events"

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v2}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 5558
    .end local v11    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .local v12, "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    if-ge v13, v14, :cond_3

    .line 5559
    :try_start_2
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 5561
    invoke-virtual {v8, v13}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayEvent(I)Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 5562
    invoke-virtual {v8, v13}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayEvent(I)Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-result-object v3

    const/4 v5, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->pushEvent(Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;Landroid/content/ContentValues;ZJ)V

    .line 5563
    invoke-virtual {v12, v4}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    .line 5558
    :cond_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 5568
    :cond_3
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "CHINA"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 5569
    const v2, 0x7f06002f

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v4, v2}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->pushTombSweepingEvent(Landroid/database/DatabaseUtils$InsertHelper;Landroid/content/ContentValues;I)V

    .line 5571
    sget-object v2, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v2}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->are24SoloarTermsSupported()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5573
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 5574
    const-string v2, "account_name"

    const-string v3, "local"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5575
    const-string v2, "account_type"

    const-string v3, "LOCAL"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5576
    const-string v2, "_sync_id"

    const-string v3, "1"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5577
    const-string v2, "name"

    const-string v3, "24SolarTerms"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5578
    const-string v2, "calendar_displayName"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f06004e

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5579
    const-string v2, "calendar_access_level"

    const/16 v3, 0xc8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5580
    const-string v2, "visible"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5581
    const-string v2, "sync_events"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5582
    const-string v2, "calendar_color"

    const/16 v3, 0xca

    const/16 v5, 0x8e

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v3, v5, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5583
    const-string v2, "ownerAccount"

    const-string v3, "local"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5584
    invoke-virtual {v10, v15}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    .line 5586
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v4}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->insertCHNSolarTerm24(Landroid/database/DatabaseUtils$InsertHelper;Landroid/content/ContentValues;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5595
    :cond_4
    :goto_2
    if-eqz v12, :cond_5

    .line 5596
    invoke-virtual {v12}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 5599
    :cond_5
    if-eqz v10, :cond_0

    .line 5600
    invoke-virtual {v10}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    goto/16 :goto_0

    .line 5588
    :cond_6
    :try_start_3
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "HONGKONG"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 5589
    const v2, 0x7f06005e

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v4, v2}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->pushTombSweepingEvent(Landroid/database/DatabaseUtils$InsertHelper;Landroid/content/ContentValues;I)V

    .line 5590
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v4}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->pushEasterDaysEvent(Landroid/database/DatabaseUtils$InsertHelper;Landroid/content/ContentValues;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 5595
    :catchall_0
    move-exception v2

    move-object v11, v12

    .end local v12    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v11    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    move-object v9, v10

    .end local v4    # "value":Landroid/content/ContentValues;
    .end local v6    # "calId":J
    .end local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .end local v13    # "i":I
    .end local v14    # "size":I
    .restart local v9    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    :goto_3
    if-eqz v11, :cond_7

    .line 5596
    invoke-virtual {v11}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 5599
    :cond_7
    if-eqz v9, :cond_8

    .line 5600
    invoke-virtual {v9}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    :cond_8
    throw v2

    .line 5591
    .end local v9    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .end local v11    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v4    # "value":Landroid/content/ContentValues;
    .restart local v6    # "calId":J
    .restart local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v12    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v13    # "i":I
    .restart local v14    # "size":I
    :cond_9
    :try_start_4
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "TAIWAN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5592
    const v2, 0x7f06007a

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v4, v2}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->pushTombSweepingEvent(Landroid/database/DatabaseUtils$InsertHelper;Landroid/content/ContentValues;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 5595
    .end local v4    # "value":Landroid/content/ContentValues;
    .end local v6    # "calId":J
    .end local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .end local v12    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .end local v13    # "i":I
    .end local v14    # "size":I
    .restart local v9    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v11    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    :catchall_1
    move-exception v2

    goto :goto_3

    .end local v9    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    :catchall_2
    move-exception v2

    move-object v9, v10

    .end local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v9    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    goto :goto_3
.end method

.method private createMapsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4690
    const-string v0, "CREATE TABLE Maps (_id INTEGER PRIMARY KEY,event_id INTEGER,map BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4695
    return-void
.end method

.method private createMemosTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4672
    const-string v0, "CREATE TABLE Memos (_id INTEGER PRIMARY KEY,event_id INTEGER,memo_id INTEGER,event_type INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4678
    return-void
.end method

.method private createNotesTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4706
    const-string v0, "CREATE TABLE IF NOT EXISTS Notes (_id INTEGER PRIMARY KEY,event_id INTEGER,filepath TEXT,imagepath TEXT,page_no INTEGER,event_type INTEGER ,date INTEGER NULL DEFAULT 0,memo_id INTEGER NULL DEFAULT 0,locked INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4718
    return-void
.end method

.method private createOriginalGroupTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4637
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "CREATE TABLE "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, "TaskGroup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " INTEGER PRIMARY KEY,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "_accountId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " INTEGER,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "groupName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " TEXT,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "selected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " INTEGER NOT NULL DEFAULT 1);"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4645
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "INSERT INTO "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, "TaskGroup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "groupName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "_accountId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "selected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ") VALUES (\'Default\',"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTaskAccountId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ",1)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4653
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "CREATE TRIGGER deleteTaskGroup DELETE ON "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, "TaskGroup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " BEGIN DELETE FROM Tasks"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "groupId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "=old._id; END "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4659
    return-void
.end method

.method private createOriginalTasksAccountsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4634
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createTasksAccountsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4635
    return-void
.end method

.method private createOriginalTasksRemindersTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4631
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createTasksRemindersTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4632
    return-void
.end method

.method private createOriginalTasksTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4628
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createTasksTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4629
    return-void
.end method

.method private createQSMemosTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4663
    const-string v0, "CREATE TABLE QSMemos (_id INTEGER PRIMARY KEY,date INTEGER,memo_id INTEGER,memo_text TEXT );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4669
    return-void
.end method

.method private createStickerTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4721
    const-string v0, "CREATE TABLE Stickers (_id INTEGER PRIMARY KEY AUTOINCREMENT,sticker_name TEXT,filepath TEXT,sticker_group TEXT,recently INTEGER DEFAULT 0, packageId INTEGER DEFAULT 0, downloaded INTEGER DEFAULT 0 );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4732
    const-string v0, "CREATE TRIGGER sticker_cleanup_delete DELETE ON Stickers BEGIN UPDATE Events SET sticker_type=0,sticker_ename=NULL WHERE sticker_type=old._id;END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4734
    return-void
.end method

.method private createSubstituteHolidayTable(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 18
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 5471
    const/4 v8, 0x1

    .line 5472
    .local v8, "INITIAL_VERSION":I
    sget-object v2, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v2}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->areNationalHolidaysSupported()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 5473
    sget-object v2, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getCalendarSubstituteHoliday(Landroid/content/Context;)Lcom/android/calendar/secfeature/holidays/CalendarHoliday;

    move-result-object v9

    .line 5475
    .local v9, "calHol":Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    if-nez v9, :cond_0

    .line 5476
    const/4 v2, 0x0

    .line 5523
    .end local v9    # "calHol":Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    :goto_0
    return v2

    .line 5479
    .restart local v9    # "calHol":Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    :cond_0
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 5480
    .local v17, "values":Landroid/content/ContentValues;
    const-string v2, "account_name"

    const-string v3, "local"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5481
    const-string v2, "account_type"

    const-string v3, "LOCAL"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5482
    const-string v2, "name"

    const-string v3, "legalSubstHoliday"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5483
    const-string v2, "calendar_displayName"

    invoke-virtual {v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayCalendarName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5484
    const-string v2, "calendar_access_level"

    const/16 v3, 0xc8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5485
    const-string v2, "visible"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5486
    const-string v2, "sync_events"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5487
    const-string v2, "calendar_color"

    invoke-virtual {v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayCalendarAccountColor()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5488
    const-string v2, "ownerAccount"

    const-string v3, "local"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5490
    const/4 v10, 0x0

    .line 5491
    .local v10, "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    const/4 v12, 0x0

    .line 5494
    .local v12, "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    :try_start_0
    new-instance v11, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v2, "Calendars"

    move-object/from16 v0, p1

    invoke-direct {v11, v0, v2}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5495
    .end local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .local v11, "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    :try_start_1
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v6

    .line 5498
    .local v6, "calId":J
    invoke-virtual {v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayCount()I

    move-result v16

    .line 5499
    .local v16, "size":I
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 5500
    .local v4, "value":Landroid/content/ContentValues;
    new-instance v13, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v2, "Events"

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v2}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 5501
    .end local v12    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .local v13, "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    move/from16 v0, v16

    if-ge v14, v0, :cond_2

    .line 5502
    :try_start_2
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 5504
    invoke-virtual {v9, v14}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayEvent(I)Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 5505
    invoke-virtual {v9, v14}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayEvent(I)Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-result-object v3

    const/4 v5, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->pushEvent(Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;Landroid/content/ContentValues;ZJ)V

    .line 5506
    invoke-virtual {v13, v4}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 5501
    :cond_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 5510
    :cond_2
    if-eqz v13, :cond_3

    .line 5511
    invoke-virtual {v13}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 5513
    :cond_3
    if-eqz v11, :cond_4

    .line 5514
    invoke-virtual {v11}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 5516
    :cond_4
    const/4 v12, 0x0

    .line 5517
    .end local v13    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v12    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    const/4 v10, 0x0

    .line 5520
    .end local v11    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    const-string v3, "CalendarDatabaseHelper"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v15

    .line 5521
    .local v15, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v15}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "substHolsCalendarVersion"

    const/4 v5, 0x1

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 5523
    .end local v4    # "value":Landroid/content/ContentValues;
    .end local v6    # "calId":J
    .end local v9    # "calHol":Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    .end local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .end local v12    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .end local v14    # "i":I
    .end local v15    # "prefs":Landroid/content/SharedPreferences;
    .end local v16    # "size":I
    .end local v17    # "values":Landroid/content/ContentValues;
    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 5510
    .restart local v9    # "calHol":Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    .restart local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v12    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v17    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v2

    :goto_2
    if-eqz v12, :cond_6

    .line 5511
    invoke-virtual {v12}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 5513
    :cond_6
    if-eqz v10, :cond_7

    .line 5514
    invoke-virtual {v10}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 5516
    :cond_7
    const/4 v12, 0x0

    .line 5517
    const/4 v10, 0x0

    throw v2

    .line 5510
    .end local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v11    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    :catchall_1
    move-exception v2

    move-object v10, v11

    .end local v11    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    goto :goto_2

    .end local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .end local v12    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v4    # "value":Landroid/content/ContentValues;
    .restart local v6    # "calId":J
    .restart local v11    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v13    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v14    # "i":I
    .restart local v16    # "size":I
    :catchall_2
    move-exception v2

    move-object v12, v13

    .end local v13    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v12    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    move-object v10, v11

    .end local v11    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v10    # "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    goto :goto_2
.end method

.method private createTasksAccountsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4586
    const-string v0, "CREATE TABLE TasksAccounts (_id INTEGER PRIMARY KEY,_sync_account TEXT,_sync_account_key TEXT,_sync_account_type TEXT,url TEXT,selected INTEGER NOT NULL DEFAULT 1,displayName TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4595
    return-void
.end method

.method private createTasksRemindersTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4578
    const-string v0, "_id INTEGER PRIMARY KEY,task_id INTEGER ,reminder_time LONG , state INTEGER , subject TEXT, start_date INTEGER, due_date INTEGER, accountkey INTEGER, reminder_type INTEGER"

    .line 4581
    .local v0, "_reminder_Columns":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE TABLE TasksReminders ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4582
    return-void
.end method

.method private createTasksTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4418
    const-string v0, "_id INTEGER PRIMARY KEY,syncServerId TEXT,displayName TEXT,_sync_dirty INTEGER,clientId TEXT,sourceid INTEGER,bodyType INTEGER,body_size INTEGER,body_truncated INTEGER,due_date INTEGER,utc_due_date INTEGER,importance INTEGER,date_completed INTEGER,complete INTEGER NOT NULL DEFAULT 0,recurrence_type INTEGER,recurrence_start INTEGER,recurrence_until INTEGER,recurrence_occurrences INTEGER,recurrence_interval INTEGER,recurrence_day_of_month INTEGER,recurrence_day_of_week INTEGER,recurrence_week_of_month INTEGER,recurrence_month_of_year INTEGER,recurrence_regenerate INTEGER,recurrence_dead_occur INTEGER,reminder_set INTEGER,reminder_time INTEGER,sensitivity INTEGER,start_date INTEGER,utc_start_date INTEGER,parentId INTEGER NOT NULL DEFAULT 0,subject TEXT,body TEXT,category1 TEXT,category2 TEXT,category3 TEXT,mailboxKey integer,accountKey integer,accountName TEXT,reminder_type INTEGER,groupId INTEGER,previousId INTEGER,syncTime TEXT,deleted INTEGER NOT NULL DEFAULT 0,glance_deleted INTEGER DEFAULT 0, task_order INTEGER "

    .line 4470
    .local v0, "taskColumns":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE TABLE Tasks ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4472
    const-string v1, "CREATE INDEX TasksSyncAccountAndIdIndex ON Tasks (sourceid)"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4475
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE TABLE DeletedTasks ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4477
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE TABLE UpdatedTasks ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4479
    const-string v1, "CREATE TRIGGER deleteTask_trigger DELETE ON Tasks BEGIN UPDATE Tasks SET parentId=old.parentId WHERE parentId=old._id; UPDATE Tasks SET previousId=old.previousId WHERE previousId=old._id; DELETE FROM Memos WHERE event_id = old._id AND event_type = 2; DELETE FROM Notes WHERE event_id = old._id AND event_type = 2; DELETE FROM Images WHERE event_id = old._id AND event_type = 2; END"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4495
    const-string v1, "CREATE TRIGGER updateTask_trigger2 UPDATE OF groupId ON Tasks BEGIN UPDATE Tasks SET parentId=old.parentId WHERE parentId=old._id; UPDATE Tasks SET previousId=old.previousId WHERE previousId=old._id; UPDATE Tasks SET parentId=0,previousId=(SELECT t1._id FROM Tasks t1 WHERE t1.groupId=new.groupId AND t1._id NOT IN (SELECT t2.previousId FROM Tasks t2 WHERE t2.groupId=new.groupId) ORDER BY t1._id LIMIT 0,1) WHERE _id=old._id;  END"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4502
    const-string v1, "CREATE TRIGGER updateTask_trigger3 UPDATE OF parentId ON Tasks BEGIN UPDATE Tasks SET complete=0 WHERE _id==new.parentId AND complete=1 AND new.complete=0; END"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4506
    return-void
.end method

.method private dropTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 4172
    const-string v0, "CalendarDatabaseHelper"

    const-string v1, "Clearing database"

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4174
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "type"

    aput-object v0, v2, v4

    const-string v0, "name"

    aput-object v0, v2, v5

    .line 4177
    .local v2, "columns":[Ljava/lang/String;
    const-string v1, "sqlite_master"

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 4178
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_0

    .line 4197
    :goto_0
    return-void

    .line 4182
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4183
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 4184
    .local v10, "name":Ljava/lang/String;
    const-string v0, "sqlite_"

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DROP "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IF EXISTS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 4188
    .local v11, "sql":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p1, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 4189
    :catch_0
    move-exception v9

    .line 4190
    .local v9, "e":Landroid/database/SQLException;
    :try_start_2
    const-string v0, "CalendarDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error executing "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 4195
    .end local v9    # "e":Landroid/database/SQLException;
    .end local v10    # "name":Ljava/lang/String;
    .end local v11    # "sql":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private static fixAllDayTime(Landroid/text/format/Time;Ljava/lang/String;Ljava/lang/Long;)Z
    .locals 4
    .param p0, "time"    # Landroid/text/format/Time;
    .param p1, "timezone"    # Ljava/lang/String;
    .param p2, "timeInMillis"    # Ljava/lang/Long;

    .prologue
    const/4 v0, 0x0

    .line 2163
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 2164
    iget v1, p0, Landroid/text/format/Time;->hour:I

    if-nez v1, :cond_0

    iget v1, p0, Landroid/text/format/Time;->minute:I

    if-nez v1, :cond_0

    iget v1, p0, Landroid/text/format/Time;->second:I

    if-eqz v1, :cond_1

    .line 2165
    :cond_0
    iput v0, p0, Landroid/text/format/Time;->hour:I

    .line 2166
    iput v0, p0, Landroid/text/format/Time;->minute:I

    .line 2167
    iput v0, p0, Landroid/text/format/Time;->second:I

    .line 2168
    const/4 v0, 0x1

    .line 2170
    :cond_1
    return v0
.end method

.method private static getAllCalendarsUrlFromEventsUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x3

    .line 4797
    if-nez p0, :cond_1

    .line 4798
    const-string v1, "CalendarDatabaseHelper"

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4799
    const-string v1, "CalendarDatabaseHelper"

    const-string v2, "Cannot get AllCalendars url from a NULL url"

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4815
    :cond_0
    :goto_0
    return-object v0

    .line 4803
    :cond_1
    const-string v1, "/private/full"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4804
    const-string v0, "/private/full"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/calendar/feeds"

    const-string v2, "/calendar/feeds/default/allcalendars/full"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 4807
    :cond_2
    const-string v1, "/private/free-busy"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4808
    const-string v0, "/private/free-busy"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/calendar/feeds"

    const-string v2, "/calendar/feeds/default/allcalendars/full"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 4812
    :cond_3
    const-string v1, "CalendarDatabaseHelper"

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4813
    const-string v1, "CalendarDatabaseHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot get AllCalendars url from the following url: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static getEditUrlFromEventsUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 4833
    invoke-static {p0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getAllCalendarsUrlFromEventsUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->rewriteUrlFromHttpToHttps(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/providers/calendar/CalendarDatabaseHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 673
    const-class v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sSingleton:Lcom/android/providers/calendar/CalendarDatabaseHelper;

    if-nez v0, :cond_0

    .line 674
    new-instance v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;

    invoke-direct {v0, p0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sSingleton:Lcom/android/providers/calendar/CalendarDatabaseHelper;

    .line 676
    :cond_0
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sSingleton:Lcom/android/providers/calendar/CalendarDatabaseHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 673
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getSelfUrlFromEventsUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 4824
    invoke-static {p0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getAllCalendarsUrlFromEventsUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->rewriteUrlFromHttpToHttps(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getStickerGroup(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 5372
    const/4 v0, 0x0

    .line 5373
    .local v0, "bFound":Z
    const/4 v3, 0x0

    .line 5374
    .local v3, "val":Ljava/lang/String;
    sget-object v4, Lcom/android/calendar/CalendarContractSec$Stickers;->STICKER_GROUPS:[Ljava/lang/String;

    array-length v2, v4

    .line 5376
    .local v2, "length":I
    if-lez v2, :cond_0

    .line 5377
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 5378
    sget-object v4, Lcom/android/calendar/CalendarContractSec$Stickers;->STICKER_GROUPS:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 5379
    add-int/lit8 v4, v1, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 5380
    const/4 v0, 0x1

    .line 5386
    .end local v1    # "i":I
    :cond_0
    if-nez v0, :cond_1

    const-string v4, "user"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 5387
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 5388
    const/4 v0, 0x1

    .line 5391
    :cond_1
    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Calendar_LocalStickerConfig"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "CHINA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 5394
    const-string v4, "Festival"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 5395
    add-int/lit8 v4, v2, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 5399
    :cond_2
    return-object v3

    .line 5377
    .restart local v1    # "i":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private initCalendarCacheTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldTimezoneDbVersion"    # Ljava/lang/String;

    .prologue
    .line 1573
    if-eqz p2, :cond_0

    move-object v0, p2

    .line 1577
    .local v0, "timezoneDbVersion":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT OR REPLACE INTO CalendarCache (_id, key, value) VALUES ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneDatabaseVersion"

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneDatabaseVersion"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\',"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1585
    return-void

    .line 1573
    .end local v0    # "timezoneDbVersion":Ljava/lang/String;
    :cond_0
    const-string v0, "2009s"

    goto :goto_0
.end method

.method private initCalendarCacheTable203(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldTimezoneDbVersion"    # Ljava/lang/String;

    .prologue
    .line 1622
    if-eqz p2, :cond_0

    move-object v0, p2

    .line 1626
    .local v0, "timezoneDbVersion":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT OR REPLACE INTO CalendarCache (_id, key, value) VALUES ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneDatabaseVersion"

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'timezoneDatabaseVersion\',"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1634
    return-void

    .line 1622
    .end local v0    # "timezoneDbVersion":Ljava/lang/String;
    :cond_0
    const-string v0, "2009s"

    goto :goto_0
.end method

.method private isM0sticker(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2241
    const/4 v2, 0x0

    .line 2242
    .local v2, "filepath":Ljava/lang/String;
    const-string v3, "SELECT filepath FROM Stickers"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2244
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_1

    .line 2245
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2247
    :cond_0
    const-string v3, "filepath"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2248
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2249
    .local v1, "filename":[Ljava/lang/String;
    sget-object v3, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    array-length v4, v1

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2250
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->isM0stickers:Z

    .line 2254
    :goto_0
    const-string v3, "CalendarDatabaseHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isM0stickers:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->isM0stickers:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2257
    .end local v1    # "filename":[Ljava/lang/String;
    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2260
    :cond_2
    iget-boolean v3, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->isM0stickers:Z

    return v3

    .line 2253
    .restart local v1    # "filename":[Ljava/lang/String;
    :cond_3
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 2257
    .end local v1    # "filename":[Ljava/lang/String;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v3
.end method

.method private pushEasterDaysEvent(Landroid/database/DatabaseUtils$InsertHelper;Landroid/content/ContentValues;)V
    .locals 29
    .param p1, "eventsInserter"    # Landroid/database/DatabaseUtils$InsertHelper;
    .param p2, "value"    # Landroid/content/ContentValues;

    .prologue
    .line 5746
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 5747
    .local v6, "stGoodFriday":Landroid/text/format/Time;
    new-instance v12, Landroid/text/format/Time;

    invoke-direct {v12}, Landroid/text/format/Time;-><init>()V

    .line 5748
    .local v12, "stEasterDay":Landroid/text/format/Time;
    new-instance v18, Landroid/text/format/Time;

    invoke-direct/range {v18 .. v18}, Landroid/text/format/Time;-><init>()V

    .line 5750
    .local v18, "stEasterMonday":Landroid/text/format/Time;
    const/16 v1, 0x76e

    iput v1, v6, Landroid/text/format/Time;->year:I

    .line 5751
    const/4 v1, 0x2

    iput v1, v6, Landroid/text/format/Time;->month:I

    .line 5752
    const/16 v1, 0x1c

    iput v1, v6, Landroid/text/format/Time;->monthDay:I

    .line 5754
    const/4 v1, 0x1

    iput-boolean v1, v6, Landroid/text/format/Time;->allDay:Z

    .line 5755
    const/4 v1, 0x0

    iput v1, v6, Landroid/text/format/Time;->hour:I

    .line 5756
    const/4 v1, 0x0

    iput v1, v6, Landroid/text/format/Time;->minute:I

    .line 5757
    const/4 v1, 0x0

    iput v1, v6, Landroid/text/format/Time;->second:I

    .line 5758
    const-string v1, "UTC"

    iput-object v1, v6, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 5760
    invoke-virtual {v12, v6}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 5761
    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 5763
    const/16 v1, 0x1e

    iput v1, v12, Landroid/text/format/Time;->monthDay:I

    .line 5764
    const/16 v1, 0x1f

    move-object/from16 v0, v18

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 5768
    iget v0, v6, Landroid/text/format/Time;->year:I

    move/from16 v19, v0

    .local v19, "i":I
    :goto_0
    const/16 v1, 0x7f5

    move/from16 v0, v19

    if-ge v0, v1, :cond_1

    .line 5769
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06005f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v4, p2

    invoke-virtual/range {v1 .. v6}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->insertCHNEventValues(Landroid/database/DatabaseUtils$InsertHelper;ILandroid/content/ContentValues;Ljava/lang/String;Landroid/text/format/Time;)V

    .line 5771
    const/4 v9, 0x2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v10, p2

    invoke-virtual/range {v7 .. v12}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->insertCHNEventValues(Landroid/database/DatabaseUtils$InsertHelper;ILandroid/content/ContentValues;Ljava/lang/String;Landroid/text/format/Time;)V

    .line 5773
    const/4 v15, 0x2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060061

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v13, p0

    move-object/from16 v14, p1

    move-object/from16 v16, p2

    invoke-virtual/range {v13 .. v18}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->insertCHNEventValues(Landroid/database/DatabaseUtils$InsertHelper;ILandroid/content/ContentValues;Ljava/lang/String;Landroid/text/format/Time;)V

    .line 5776
    iget v1, v6, Landroid/text/format/Time;->year:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v6, Landroid/text/format/Time;->year:I

    .line 5778
    iget v1, v6, Landroid/text/format/Time;->year:I

    div-int/lit8 v20, v1, 0x64

    .line 5779
    .local v20, "temp1":I
    iget v1, v6, Landroid/text/format/Time;->year:I

    iget v2, v6, Landroid/text/format/Time;->year:I

    div-int/lit8 v2, v2, 0x13

    mul-int/lit8 v2, v2, 0x13

    sub-int v21, v1, v2

    .line 5780
    .local v21, "temp2":I
    add-int/lit8 v1, v20, -0x11

    div-int/lit8 v22, v1, 0x19

    .line 5781
    .local v22, "temp3":I
    div-int/lit8 v1, v20, 0x4

    sub-int v2, v20, v22

    div-int/lit8 v2, v2, 0x3

    add-int/2addr v1, v2

    sub-int v1, v20, v1

    mul-int/lit8 v2, v21, 0x13

    add-int/2addr v1, v2

    add-int/lit8 v23, v1, 0xf

    .line 5782
    .local v23, "temp4":I
    div-int/lit8 v1, v23, 0x1e

    mul-int/lit8 v1, v1, 0x1e

    sub-int v24, v23, v1

    .line 5783
    .local v24, "temp5":I
    div-int/lit8 v1, v24, 0x1c

    div-int/lit8 v2, v24, 0x1c

    rsub-int/lit8 v2, v2, 0x1

    mul-int/2addr v1, v2

    const/16 v2, 0x1d

    add-int/lit8 v3, v24, 0x1

    div-int/2addr v2, v3

    mul-int/2addr v1, v2

    rsub-int/lit8 v2, v21, 0x15

    div-int/lit8 v2, v2, 0xb

    mul-int/2addr v1, v2

    sub-int v25, v24, v1

    .line 5785
    .local v25, "temp6":I
    iget v1, v6, Landroid/text/format/Time;->year:I

    iget v2, v6, Landroid/text/format/Time;->year:I

    div-int/lit8 v2, v2, 0x4

    add-int/2addr v1, v2

    add-int v1, v1, v24

    add-int/lit8 v1, v1, 0x2

    sub-int v1, v1, v20

    div-int/lit8 v2, v20, 0x4

    add-int v26, v1, v2

    .line 5786
    .local v26, "temp7":I
    div-int/lit8 v1, v26, 0x7

    mul-int/lit8 v1, v1, 0x7

    sub-int v27, v26, v1

    .line 5787
    .local v27, "temp8":I
    sub-int v28, v25, v27

    .line 5789
    .local v28, "temp9":I
    add-int/lit8 v1, v28, 0x28

    div-int/lit8 v1, v1, 0x2c

    add-int/lit8 v1, v1, 0x3

    iput v1, v6, Landroid/text/format/Time;->month:I

    .line 5790
    add-int/lit8 v1, v28, 0x1a

    iget v2, v6, Landroid/text/format/Time;->month:I

    div-int/lit8 v2, v2, 0x4

    mul-int/lit8 v2, v2, 0x1f

    sub-int/2addr v1, v2

    iput v1, v6, Landroid/text/format/Time;->monthDay:I

    .line 5791
    iget v1, v6, Landroid/text/format/Time;->month:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v6, Landroid/text/format/Time;->month:I

    .line 5793
    invoke-virtual {v12, v6}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 5794
    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 5796
    iget v1, v6, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x2

    iput v1, v12, Landroid/text/format/Time;->monthDay:I

    .line 5797
    iget v1, v6, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x3

    move-object/from16 v0, v18

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 5799
    iget v1, v6, Landroid/text/format/Time;->monthDay:I

    if-gtz v1, :cond_0

    .line 5800
    iget v1, v6, Landroid/text/format/Time;->month:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v6, Landroid/text/format/Time;->month:I

    .line 5801
    iget v1, v6, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x1f

    iput v1, v6, Landroid/text/format/Time;->monthDay:I

    .line 5768
    :cond_0
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_0

    .line 5804
    .end local v20    # "temp1":I
    .end local v21    # "temp2":I
    .end local v22    # "temp3":I
    .end local v23    # "temp4":I
    .end local v24    # "temp5":I
    .end local v25    # "temp6":I
    .end local v26    # "temp7":I
    .end local v27    # "temp8":I
    .end local v28    # "temp9":I
    :cond_1
    return-void
.end method

.method private pushEvent(Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;Landroid/content/ContentValues;ZJ)V
    .locals 18
    .param p1, "event"    # Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;
    .param p2, "value"    # Landroid/content/ContentValues;
    .param p3, "reapeatingEvent"    # Z
    .param p4, "calId"    # J

    .prologue
    .line 5628
    const-wide/16 v12, 0x0

    .local v12, "startMillis":J
    const-wide/16 v6, 0x0

    .line 5629
    .local v6, "endMillis":J
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->startTime:Landroid/text/format/Time;

    .line 5631
    .local v11, "mStartTime":Landroid/text/format/Time;
    const/4 v14, 0x1

    iput-boolean v14, v11, Landroid/text/format/Time;->allDay:Z

    .line 5632
    const/4 v14, 0x0

    iput v14, v11, Landroid/text/format/Time;->hour:I

    .line 5633
    const/4 v14, 0x0

    iput v14, v11, Landroid/text/format/Time;->minute:I

    .line 5634
    const/4 v14, 0x0

    iput v14, v11, Landroid/text/format/Time;->second:I

    .line 5635
    const-string v14, "UTC"

    iput-object v14, v11, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 5636
    const/4 v14, 0x1

    invoke-virtual {v11, v14}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v12

    .line 5637
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->startTime:Landroid/text/format/Time;

    .line 5638
    .local v5, "mEndTime":Landroid/text/format/Time;
    iget v14, v5, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v14, v14, 0x1

    iput v14, v5, Landroid/text/format/Time;->monthDay:I

    .line 5639
    const/4 v14, 0x1

    invoke-virtual {v5, v14}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 5641
    const-string v14, "calendar_id"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5642
    const-string v14, "eventTimezone"

    const-string v15, "UTC"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5643
    const-string v14, "title"

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->title:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5644
    const-string v14, "allDay"

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5645
    const-string v14, "dtstart"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5646
    const-string v14, "dtend"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5647
    const-string v14, "hasAttendeeData"

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5649
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->description:Ljava/lang/String;

    if-nez v14, :cond_0

    .line 5650
    const-string v14, ""

    move-object/from16 v0, p1

    iput-object v14, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->description:Ljava/lang/String;

    .line 5653
    :cond_0
    const-string v14, "description"

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->description:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5654
    const-string v14, "contactEventType"

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5655
    const-string v14, "accessLevel"

    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5656
    const-string v14, "hasAlarm"

    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5657
    const-string v15, "setLunar"

    move-object/from16 v0, p1

    iget-boolean v14, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->lunar:Z

    if-eqz v14, :cond_3

    const/4 v14, 0x1

    :goto_0
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v15, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5659
    const/4 v14, 0x1

    move/from16 v0, p3

    if-ne v0, v14, :cond_2

    .line 5660
    new-instance v8, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    invoke-direct {v8}, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;-><init>()V

    .line 5661
    .local v8, "mEventRecurrence":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    const-string v10, ""

    .line 5662
    .local v10, "mRrule":Ljava/lang/String;
    new-instance v9, Landroid/text/format/Time;

    invoke-direct {v9}, Landroid/text/format/Time;-><init>()V

    .line 5663
    .local v9, "mRepeatUntilDate":Landroid/text/format/Time;
    const/4 v14, 0x7

    iput v14, v8, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    .line 5665
    invoke-virtual {v9, v12, v13}, Landroid/text/format/Time;->set(J)V

    .line 5666
    const/16 v14, 0x7f4

    iput v14, v9, Landroid/text/format/Time;->year:I

    .line 5667
    const/4 v14, 0x0

    iput v14, v9, Landroid/text/format/Time;->hour:I

    .line 5668
    const/4 v14, 0x0

    iput v14, v9, Landroid/text/format/Time;->minute:I

    .line 5669
    const/4 v14, 0x0

    iput v14, v9, Landroid/text/format/Time;->second:I

    .line 5670
    const/4 v14, 0x1

    iput-boolean v14, v9, Landroid/text/format/Time;->allDay:Z

    .line 5671
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v14

    const-string v15, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v14, v15}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "CHINA"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 5674
    move-object/from16 v0, p1

    iget-boolean v14, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->lunar:Z

    if-eqz v14, :cond_1

    .line 5675
    const/16 v14, 0xb

    iput v14, v9, Landroid/text/format/Time;->month:I

    .line 5676
    const/16 v14, 0x1f

    iput v14, v9, Landroid/text/format/Time;->monthDay:I

    .line 5679
    :cond_1
    const/4 v14, 0x1

    invoke-virtual {v9, v14}, Landroid/text/format/Time;->normalize(Z)J

    .line 5680
    invoke-virtual {v9}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v8, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->until:Ljava/lang/String;

    .line 5681
    invoke-virtual {v8}, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 5682
    const-string v14, "rrule"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5684
    sub-long v14, v6, v12

    const-wide/32 v16, 0x5265c00

    add-long v14, v14, v16

    const-wide/16 v16, 0x1

    sub-long v14, v14, v16

    const-wide/32 v16, 0x5265c00

    div-long v2, v14, v16

    .line 5685
    .local v2, "days":J
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "P"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "D"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 5686
    .local v4, "duration":Ljava/lang/String;
    const-string v14, "duration"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5689
    .end local v2    # "days":J
    .end local v4    # "duration":Ljava/lang/String;
    .end local v8    # "mEventRecurrence":Lcom/android/providers/calendar/calendarcommon/EventRecurrence;
    .end local v9    # "mRepeatUntilDate":Landroid/text/format/Time;
    .end local v10    # "mRrule":Ljava/lang/String;
    :cond_2
    const-string v14, "organizer"

    const-string v15, "local"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5690
    return-void

    .line 5657
    :cond_3
    const/4 v14, 0x0

    goto/16 :goto_0
.end method

.method private pushTombSweepingEvent(Landroid/database/DatabaseUtils$InsertHelper;Landroid/content/ContentValues;I)V
    .locals 11
    .param p1, "eventsInserter"    # Landroid/database/DatabaseUtils$InsertHelper;
    .param p2, "value"    # Landroid/content/ContentValues;
    .param p3, "stringID"    # I

    .prologue
    const/4 v10, 0x3

    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v9, 0xa

    .line 5696
    const/16 v0, 0xf

    new-array v6, v0, [[I

    new-array v0, v9, [I

    fill-array-data v0, :array_0

    aput-object v0, v6, v3

    new-array v0, v9, [I

    fill-array-data v0, :array_1

    aput-object v0, v6, v4

    new-array v0, v9, [I

    fill-array-data v0, :array_2

    aput-object v0, v6, v2

    new-array v0, v9, [I

    fill-array-data v0, :array_3

    aput-object v0, v6, v10

    const/4 v0, 0x4

    new-array v1, v9, [I

    fill-array-data v1, :array_4

    aput-object v1, v6, v0

    const/4 v0, 0x5

    new-array v1, v9, [I

    fill-array-data v1, :array_5

    aput-object v1, v6, v0

    const/4 v0, 0x6

    new-array v1, v9, [I

    fill-array-data v1, :array_6

    aput-object v1, v6, v0

    const/4 v0, 0x7

    new-array v1, v9, [I

    fill-array-data v1, :array_7

    aput-object v1, v6, v0

    const/16 v0, 0x8

    new-array v1, v9, [I

    fill-array-data v1, :array_8

    aput-object v1, v6, v0

    const/16 v0, 0x9

    new-array v1, v9, [I

    fill-array-data v1, :array_9

    aput-object v1, v6, v0

    new-array v0, v9, [I

    fill-array-data v0, :array_a

    aput-object v0, v6, v9

    const/16 v0, 0xb

    new-array v1, v9, [I

    fill-array-data v1, :array_b

    aput-object v1, v6, v0

    const/16 v0, 0xc

    new-array v1, v9, [I

    fill-array-data v1, :array_c

    aput-object v1, v6, v0

    const/16 v0, 0xd

    new-array v1, v9, [I

    fill-array-data v1, :array_d

    aput-object v1, v6, v0

    const/16 v0, 0xe

    new-array v1, v9, [I

    fill-array-data v1, :array_e

    aput-object v1, v6, v0

    .line 5714
    .local v6, "arrayTombSweeping":[[I
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 5716
    .local v5, "startTime":Landroid/text/format/Time;
    const/16 v0, 0x76e

    iput v0, v5, Landroid/text/format/Time;->year:I

    .line 5717
    iput v10, v5, Landroid/text/format/Time;->month:I

    .line 5718
    const/4 v0, 0x5

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    .line 5720
    iput-boolean v4, v5, Landroid/text/format/Time;->allDay:Z

    .line 5721
    iput v3, v5, Landroid/text/format/Time;->hour:I

    .line 5722
    iput v3, v5, Landroid/text/format/Time;->minute:I

    .line 5723
    iput v3, v5, Landroid/text/format/Time;->second:I

    .line 5724
    const-string v0, "UTC"

    iput-object v0, v5, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 5726
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    const/16 v0, 0xe

    if-ge v7, v0, :cond_3

    .line 5727
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    if-ge v8, v9, :cond_1

    .line 5728
    if-nez v7, :cond_0

    if-nez v8, :cond_0

    .line 5727
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 5731
    :cond_0
    const/16 v0, 0xd

    if-ne v7, v0, :cond_2

    const/4 v0, 0x6

    if-ne v8, v0, :cond_2

    .line 5726
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 5734
    :cond_2
    aget-object v0, v6, v7

    aget v0, v0, v8

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    .line 5736
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->insertCHNEventValues(Landroid/database/DatabaseUtils$InsertHelper;ILandroid/content/ContentValues;Ljava/lang/String;Landroid/text/format/Time;)V

    .line 5739
    iget v0, v5, Landroid/text/format/Time;->year:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v5, Landroid/text/format/Time;->year:I

    goto :goto_2

    .line 5743
    .end local v8    # "j":I
    :cond_3
    return-void

    .line 5696
    :array_0
    .array-data 4
        0x5
        0x6
        0x6
        0x5
        0x5
        0x6
        0x6
        0x5
        0x5
        0x6
    .end array-data

    :array_1
    .array-data 4
        0x6
        0x5
        0x5
        0x5
        0x6
        0x5
        0x5
        0x5
        0x6
        0x5
    .end array-data

    :array_2
    .array-data 4
        0x5
        0x5
        0x6
        0x5
        0x5
        0x5
        0x6
        0x5
        0x5
        0x5
    .end array-data

    :array_3
    .array-data 4
        0x6
        0x5
        0x5
        0x5
        0x6
        0x5
        0x5
        0x5
        0x6
        0x5
    .end array-data

    :array_4
    .array-data 4
        0x5
        0x5
        0x6
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
    .end array-data

    :array_5
    .array-data 4
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
    .end array-data

    :array_6
    .array-data 4
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
        0x5
    .end array-data

    :array_7
    .array-data 4
        0x5
        0x5
        0x5
        0x5
        0x5
        0x4
        0x5
        0x5
        0x5
        0x4
    .end array-data

    :array_8
    .array-data 4
        0x5
        0x5
        0x5
        0x4
        0x5
        0x5
        0x5
        0x4
        0x5
        0x5
    .end array-data

    :array_9
    .array-data 4
        0x5
        0x4
        0x5
        0x5
        0x5
        0x4
        0x5
        0x5
        0x5
        0x4
    .end array-data

    :array_a
    .array-data 4
        0x5
        0x5
        0x5
        0x4
        0x5
        0x5
        0x5
        0x4
        0x4
        0x5
    .end array-data

    :array_b
    .array-data 4
        0x5
        0x4
        0x4
        0x5
        0x5
        0x4
        0x4
        0x5
        0x5
        0x4
    .end array-data

    :array_c
    .array-data 4
        0x4
        0x5
        0x5
        0x4
        0x4
        0x5
        0x5
        0x4
        0x4
        0x5
    .end array-data

    :array_d
    .array-data 4
        0x5
        0x4
        0x4
        0x5
        0x5
        0x4
        0x4
        0x5
        0x5
        0x4
    .end array-data

    :array_e
    .array-data 4
        0x4
        0x4
        0x5
        0x4
        0x4
        0x4
        0x5
        0x4
        0x4
        0x4
    .end array-data
.end method

.method private recreateMetaDataAndInstances67(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2155
    const-string v0, "DROP TABLE CalendarMetaData;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2156
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarMetaDataTable59(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2159
    const-string v0, "DELETE FROM Instances;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2160
    return-void
.end method

.method static removeOrphans(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v4, 0x0

    .line 1705
    const-string v1, "CalendarDatabaseHelper"

    const-string v2, "Checking for orphaned entries"

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1708
    const-string v1, "Attendees"

    const-string v2, "event_id IN (SELECT event_id FROM Attendees LEFT OUTER JOIN Events ON event_id=Events._id WHERE Events._id IS NULL)"

    invoke-virtual {p0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1709
    .local v0, "count":I
    if-eqz v0, :cond_0

    .line 1710
    const-string v1, "CalendarDatabaseHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Deleted "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " orphaned Attendees"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1713
    :cond_0
    const-string v1, "Reminders"

    const-string v2, "event_id IN (SELECT event_id FROM Reminders LEFT OUTER JOIN Events ON event_id=Events._id WHERE Events._id IS NULL)"

    invoke-virtual {p0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1714
    if-eqz v0, :cond_1

    .line 1715
    const-string v1, "CalendarDatabaseHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Deleted "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " orphaned Reminders"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1717
    :cond_1
    return-void
.end method

.method private static rewriteUrlFromHttpToHttps(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 4842
    if-nez p0, :cond_2

    .line 4843
    const-string v0, "CalendarDatabaseHelper"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4844
    const-string v0, "CalendarDatabaseHelper"

    const-string v1, "Cannot rewrite a NULL url"

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4846
    :cond_0
    const/4 p0, 0x0

    .line 4854
    .end local p0    # "url":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p0

    .line 4848
    .restart local p0    # "url":Ljava/lang/String;
    :cond_2
    const-string v0, "https://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4851
    const-string v0, "http://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 4852
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid url parameter, unknown scheme: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4854
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "http://"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private updateCalendarCacheTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1589
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO CalendarCache (_id, key, value) VALUES ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneType"

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\',"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "auto"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1598
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    .line 1601
    .local v0, "defaultTimezone":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO CalendarCache (_id, key, value) VALUES ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneInstances"

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneInstances"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\',"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1611
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO CalendarCache (_id, key, value) VALUES ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneInstancesPrevious"

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneInstancesPrevious"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\',"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1619
    return-void
.end method

.method private updateCalendarCacheTableTo203(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1638
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO CalendarCache (_id, key, value) VALUES ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneType"

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'timezoneType\',"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'auto\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1645
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    .line 1648
    .local v0, "defaultTimezone":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO CalendarCache (_id, key, value) VALUES ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneInstances"

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'timezoneInstances\',"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1656
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO CalendarCache (_id, key, value) VALUES ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "timezoneInstancesPrevious"

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'timezoneInstancesPrevious\',"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1662
    return-void
.end method

.method private updateFilepath(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 5214
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    .line 5215
    .local v2, "assetManager":Landroid/content/res/AssetManager;
    const/4 v5, 0x0

    .line 5217
    .local v5, "files":[Ljava/lang/String;
    const-string v15, "CalendarDatabaseHelper"

    const-string v16, "updateFilepath"

    invoke-static/range {v15 .. v16}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5220
    :try_start_0
    const-string v15, "stickers"

    invoke-virtual {v2, v15}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 5225
    :goto_0
    const-string v15, "CalendarDatabaseHelper"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "len:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    array-length v0, v5

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5226
    array-length v15, v5

    const/16 v16, 0x1

    move/from16 v0, v16

    if-le v15, v0, :cond_3

    .line 5227
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    array-length v15, v5

    if-ge v6, v15, :cond_3

    .line 5228
    const/4 v7, 0x0

    .line 5229
    .local v7, "in":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 5232
    .local v9, "out":Ljava/io/OutputStream;
    :try_start_1
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "stickers/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    aget-object v16, v5, v6

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v15}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v7

    .line 5235
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v13

    .line 5237
    .local v13, "str":Ljava/lang/String;
    const-string v15, "mounted"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 5242
    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v15

    iget-object v1, v15, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 5243
    .local v1, "appPath":Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/sticker/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    .line 5246
    aget-object v15, v5, v6

    const-string v16, "\'"

    const-string v17, "\'\'"

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "%"

    const-string v17, "`%"

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v6

    .line 5247
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    invoke-direct {v8, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5248
    .local v8, "mpath":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->isDirectory()Z

    move-result v15

    if-nez v15, :cond_0

    .line 5249
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    .line 5250
    const/4 v15, 0x1

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Ljava/io/File;->setReadable(ZZ)Z

    .line 5251
    const/4 v15, 0x1

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 5253
    :cond_0
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    aget-object v16, v5, v6

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v10, v15}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 5255
    .end local v9    # "out":Ljava/io/OutputStream;
    .local v10, "out":Ljava/io/OutputStream;
    :try_start_2
    aget-object v15, v5, v6

    const-string v16, "\'"

    const-string v17, "\'\'"

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "%"

    const-string v17, "`%"

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v6

    .line 5256
    aget-object v15, v5, v6

    const-string v16, "_"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 5259
    .local v14, "t":[Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->isM0sticker(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 5260
    sget-object v15, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    aget-object v16, v5, v6

    invoke-virtual/range {v15 .. v16}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 5261
    .local v4, "filename":Ljava/lang/String;
    const-string v15, "_"

    invoke-virtual {v4, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 5262
    .local v12, "selection":[Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "UPDATE Stickers SET filepath=\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    aget-object v16, v5, v6

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\' WHERE "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "sticker_name"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "=\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x3

    aget-object v16, v12, v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\' AND "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "sticker_group"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "=\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x2

    aget-object v16, v12, v16

    invoke-static/range {v16 .. v16}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5277
    .end local v4    # "filename":Ljava/lang/String;
    .end local v12    # "selection":[Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v10}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 5279
    new-instance v11, Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    aget-object v16, v5, v6

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v11, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5280
    .local v11, "readR":Ljava/io/File;
    const/4 v15, 0x1

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Ljava/io/File;->setReadable(ZZ)Z

    .line 5281
    const/4 v15, 0x1

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 5283
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 5284
    const/4 v7, 0x0

    .line 5285
    invoke-virtual {v10}, Ljava/io/OutputStream;->flush()V

    .line 5286
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 5287
    const/4 v9, 0x0

    .line 5227
    .end local v1    # "appPath":Ljava/lang/String;
    .end local v8    # "mpath":Ljava/io/File;
    .end local v10    # "out":Ljava/io/OutputStream;
    .end local v11    # "readR":Ljava/io/File;
    .end local v13    # "str":Ljava/lang/String;
    .end local v14    # "t":[Ljava/lang/String;
    .restart local v9    # "out":Ljava/io/OutputStream;
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 5221
    .end local v6    # "i":I
    .end local v7    # "in":Ljava/io/InputStream;
    .end local v9    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v3

    .line 5222
    .local v3, "e":Ljava/io/IOException;
    const-string v15, "CalendarDatabaseHelper"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 5239
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v6    # "i":I
    .restart local v7    # "in":Ljava/io/InputStream;
    .restart local v9    # "out":Ljava/io/OutputStream;
    .restart local v13    # "str":Ljava/lang/String;
    :cond_1
    :try_start_3
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_2

    .line 5289
    .end local v13    # "str":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 5290
    .local v3, "e":Ljava/lang/Exception;
    :goto_5
    const-string v15, "CalendarDatabaseHelper"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Cause:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " Msg:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 5267
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v9    # "out":Ljava/io/OutputStream;
    .restart local v1    # "appPath":Ljava/lang/String;
    .restart local v8    # "mpath":Ljava/io/File;
    .restart local v10    # "out":Ljava/io/OutputStream;
    .restart local v13    # "str":Ljava/lang/String;
    .restart local v14    # "t":[Ljava/lang/String;
    :cond_2
    :try_start_4
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "UPDATE Stickers SET sticker_group=\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x2

    aget-object v16, v14, v16

    invoke-static/range {v16 .. v16}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\' WHERE "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "sticker_name"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "=\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x3

    aget-object v16, v14, v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\' AND "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "sticker_group"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "!=\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x2

    aget-object v16, v14, v16

    invoke-static/range {v16 .. v16}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5271
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "UPDATE Stickers SET filepath=\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    aget-object v16, v5, v6

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\' WHERE "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "sticker_name"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "=\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x3

    aget-object v16, v14, v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\' AND "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "sticker_group"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "=\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x2

    aget-object v16, v14, v16

    invoke-static/range {v16 .. v16}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\'"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_3

    .line 5289
    .end local v14    # "t":[Ljava/lang/String;
    :catch_2
    move-exception v3

    move-object v9, v10

    .end local v10    # "out":Ljava/io/OutputStream;
    .restart local v9    # "out":Ljava/io/OutputStream;
    goto/16 :goto_5

    .line 5294
    .end local v1    # "appPath":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v7    # "in":Ljava/io/InputStream;
    .end local v8    # "mpath":Ljava/io/File;
    .end local v9    # "out":Ljava/io/OutputStream;
    .end local v13    # "str":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method private upgradeResync(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4023
    const-string v5, "DELETE FROM _sync_state;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4026
    const-string v5, "SELECT _sync_account,_sync_account_type,url FROM Calendars"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 4029
    .local v4, "cursor":Landroid/database/Cursor;
    if-eqz v4, :cond_1

    .line 4031
    :goto_0
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 4032
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 4033
    .local v1, "accountName":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 4034
    .local v2, "accountType":Ljava/lang/String;
    new-instance v0, Landroid/accounts/Account;

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4035
    .local v0, "account":Landroid/accounts/Account;
    const/4 v5, 0x2

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4036
    .local v3, "calendarUrl":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {p0, v0, v5, v3}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->scheduleSync(Landroid/accounts/Account;ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4039
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    .end local v2    # "accountType":Ljava/lang/String;
    .end local v3    # "calendarUrl":Ljava/lang/String;
    :catchall_0
    move-exception v5

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v5

    :cond_0
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 4042
    :cond_1
    return-void
.end method

.method private upgradeSyncState(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 774
    const-string v2, "SELECT version FROM _sync_state_metadata"

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    .line 778
    .local v0, "version":J
    const-wide/16 v2, 0x3

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 779
    const-string v2, "CalendarDatabaseHelper"

    const-string v3, "Upgrading calendar sync state table"

    invoke-static {v2, v3}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    const-string v2, "CREATE TEMPORARY TABLE state_backup(_sync_account TEXT, _sync_account_type TEXT, data TEXT);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 782
    const-string v2, "INSERT INTO state_backup SELECT _sync_account, _sync_account_type, data FROM _sync_state WHERE _sync_account is not NULL and _sync_account_type is not NULL;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 786
    const-string v2, "DROP TABLE _sync_state;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 787
    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSyncState:Lcom/android/common/content/SyncStateContentProviderHelper;

    invoke-virtual {v2, p1}, Lcom/android/common/content/SyncStateContentProviderHelper;->onDatabaseOpened(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 788
    const-string v2, "INSERT INTO _sync_state(account_name,account_type,data) SELECT _sync_account, _sync_account_type, data from state_backup;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 793
    const-string v2, "DROP TABLE state_backup;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 800
    :goto_0
    return-void

    .line 798
    :cond_0
    const-string v2, "CalendarDatabaseHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "upgradeSyncState: current version is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", skipping upgrade."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private upgradeToVersion200(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3462
    const-string v8, "ALTER TABLE Calendars RENAME TO Calendars_Backup;"

    invoke-virtual {p1, v8}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3464
    const-string v8, "DROP TRIGGER IF EXISTS calendar_cleanup"

    invoke-virtual {p1, v8}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3465
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarsTable200(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 3468
    const-string v8, "INSERT INTO Calendars (_id, _sync_account, _sync_account_type, _sync_id, _sync_version, _sync_time, _sync_local_id, _sync_dirty, _sync_mark, name, displayName, color, access_level, selected, sync_events, location, timezone, ownerAccount, organizerCanRespond, deleted, sync1) SELECT _id, _sync_account, _sync_account_type, _sync_id, _sync_version, _sync_time, _sync_local_id, _sync_dirty, _sync_mark, name, displayName, color, access_level, selected, sync_events, location, timezone, ownerAccount, organizerCanRespond, 0, url FROM Calendars_Backup;"

    invoke-virtual {p1, v8}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3517
    const-string v4, "SELECT _id, url FROM Calendars_Backup WHERE _sync_account_type=\'com.google\' AND url IS NOT NULL;"

    .line 3522
    .local v4, "selectSql":Ljava/lang/String;
    const-string v6, "UPDATE Calendars SET sync2=?, sync3=? WHERE _id=?;"

    .line 3527
    .local v6, "updateSql":Ljava/lang/String;
    const/4 v8, 0x0

    invoke-virtual {p1, v4, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3528
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_1

    .line 3530
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v8

    if-lez v8, :cond_0

    .line 3531
    const/4 v8, 0x3

    new-array v0, v8, [Ljava/lang/Object;

    .line 3532
    .local v0, "bindArgs":[Ljava/lang/Object;
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 3533
    const/4 v8, 0x0

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 3534
    .local v3, "id":Ljava/lang/Long;
    const/4 v8, 0x1

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 3535
    .local v7, "url":Ljava/lang/String;
    invoke-static {v7}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getSelfUrlFromEventsUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3536
    .local v5, "selfUrl":Ljava/lang/String;
    invoke-static {v7}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getEditUrlFromEventsUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3538
    .local v2, "editUrl":Ljava/lang/String;
    const/4 v8, 0x0

    aput-object v2, v0, v8

    .line 3539
    const/4 v8, 0x1

    aput-object v5, v0, v8

    .line 3540
    const/4 v8, 0x2

    aput-object v3, v0, v8

    .line 3542
    invoke-virtual {p1, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3546
    .end local v0    # "bindArgs":[Ljava/lang/Object;
    .end local v2    # "editUrl":Ljava/lang/String;
    .end local v3    # "id":Ljava/lang/Long;
    .end local v5    # "selfUrl":Ljava/lang/String;
    .end local v7    # "url":Ljava/lang/String;
    :catchall_0
    move-exception v8

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v8

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3551
    :cond_1
    const-string v8, "DROP TABLE Calendars_Backup;"

    invoke-virtual {p1, v8}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3552
    return-void
.end method

.method private upgradeToVersion201(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3456
    const-string v0, "ALTER TABLE Calendars ADD COLUMN sync4 TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3457
    return-void
.end method

.method private upgradeToVersion202(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3389
    const-string v0, "ALTER TABLE Calendars RENAME TO Calendars_Backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3391
    const-string v0, "DROP TRIGGER IF EXISTS calendar_cleanup"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3392
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarsTable202(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 3396
    const-string v0, "INSERT INTO Calendars (_id, _sync_account, _sync_account_type, _sync_id, _sync_version, _sync_time, _sync_local_id, _sync_dirty, _sync_mark, name, displayName, color, access_level, selected, sync_events, location, timezone, ownerAccount, organizerCanRespond, deleted, sync1, sync2, sync3, sync4,sync5) SELECT _id, _sync_account, _sync_account_type, _sync_id, _sync_version, _sync_time, _sync_local_id, _sync_dirty, _sync_mark, name, displayName, color, access_level, selected, sync_events, location, timezone, ownerAccount, organizerCanRespond, deleted, sync1, sync2, sync3, sync4, hidden FROM Calendars_Backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3452
    const-string v0, "DROP TABLE Calendars_Backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3453
    return-void
.end method

.method private upgradeToVersion203(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v5, 0x0

    .line 3362
    const-string v2, "SELECT value FROM CalendarCache WHERE key=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "timezoneDatabaseVersion"

    aput-object v4, v3, v5

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 3365
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 3366
    .local v1, "oldTimezoneDbVersion":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 3368
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3369
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 3370
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 3371
    const/4 v0, 0x0

    .line 3373
    const-string v2, "DELETE FROM CalendarCache;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3376
    :cond_0
    if-eqz v0, :cond_1

    .line 3377
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 3381
    :cond_1
    invoke-direct {p0, p1, v1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->initCalendarCacheTable203(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 3384
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->updateCalendarCacheTableTo203(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 3385
    return-void

    .line 3376
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_2

    .line 3377
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v2
.end method

.method private upgradeToVersion205(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3290
    const-string v0, "ALTER TABLE Calendars RENAME TO Calendars_Backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3291
    const-string v0, "DROP TRIGGER IF EXISTS calendar_cleanup"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3292
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarsTable205(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 3295
    const-string v0, "INSERT INTO Calendars (_id, _sync_account, _sync_account_type, _sync_id, _sync_version, _sync_time, _sync_dirty, name, displayName, color, access_level, visible, sync_events, location, timezone, ownerAccount, canOrganizerRespond, canModifyTimeZone, maxReminders, deleted, sync1, sync2, sync3, sync4,sync5,sync6) SELECT _id, _sync_account, _sync_account_type, _sync_id, _sync_version, _sync_time, _sync_dirty, name, displayName, color, access_level, selected, sync_events, location, timezone, ownerAccount, organizerCanRespond, 1, 5, deleted, sync1, sync2, sync3, sync4, sync5, _sync_mark FROM Calendars_Backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3353
    const-string v0, "UPDATE Calendars SET canModifyTimeZone=0, maxReminders=1 WHERE _sync_account_type=\'com.android.exchange\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3357
    const-string v0, "DROP TABLE Calendars_Backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3358
    return-void
.end method

.method private upgradeToVersion300(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x1

    .line 3027
    const-string v1, "ALTER TABLE Calendars RENAME TO Calendars_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3028
    const-string v1, "DROP TRIGGER IF EXISTS calendar_cleanup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3029
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarsTable300(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 3032
    const-string v1, "INSERT INTO Calendars (_id, account_name, account_type, _sync_id, _sync_version, _sync_time, dirty, name, displayName, calendar_color, access_level, visible, sync_events, calendar_location, calendar_timezone, ownerAccount, canOrganizerRespond, canModifyTimeZone, maxReminders, allowedReminders,deleted, sync1, sync2, sync3, sync4,sync5,sync6) SELECT _id, _sync_account, _sync_account_type, _sync_id, _sync_version, _sync_time, _sync_dirty, name, displayName, color, access_level, visible, sync_events, location, timezone, ownerAccount, canOrganizerRespond, canModifyTimeZone, maxReminders, \'0,1,2,3\',deleted, sync1, sync2, sync3, sync4, sync5, sync6 FROM Calendars_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3093
    const-string v1, "UPDATE Calendars SET allowedReminders = \'0,1,2\' WHERE account_type = \'com.google\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3097
    const-string v1, "UPDATE Calendars SET account_type = \'LOCAL\',  ownerAccount = \'My calendar\' WHERE _id = \'1\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3103
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->areNationalHolidaysSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3104
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "JAPAN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3105
    const-string v1, "UPDATE Calendars SET account_type = \'LOCAL\', ownerAccount = \'local\' WHERE _id != \'1\' AND account_type = \'local\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3106
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->checkJapanCalendar(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 3107
    const-string v1, "UPDATE Calendars SET visible = \'0\', sync_events = \'0\' WHERE account_type = \'com.android.nttdocomo\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3116
    :cond_0
    :goto_0
    const-string v1, "DROP TABLE Calendars_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3118
    const-string v1, "ALTER TABLE Events RENAME TO Events_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3119
    const-string v1, "DROP TRIGGER IF EXISTS events_insert"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3120
    const-string v1, "DROP TRIGGER IF EXISTS events_cleanup_delete"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3121
    const-string v1, "DROP INDEX IF EXISTS eventSyncAccountAndIdIndex"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3122
    const-string v1, "DROP INDEX IF EXISTS eventsCalendarIdIndex"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3123
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createEventsTable300(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 3125
    const-string v0, "INSERT INTO Events (_id, _sync_id, _sync_version, _sync_time, _sync_local_id, dirty, _sync_mark, calendar_id, htmlUri, title, eventLocation, description, eventStatus, selfAttendeeStatus, commentsUri, dtstart, dtend, eventTimezone, eventEndTimezone, duration, allDay, accessLevel, availability, hasAlarm, hasExtendedProperties, rrule, rdate, exrule, exdate, original_sync_id, originalInstanceTime, originalAllDay, lastDate, hasAttendeeData, guestsCanModify, guestsCanInviteOthers, guestsCanSeeGuests, organizer, deleted, sync_data1,latitude,longitude,contact_data_id,contact_id,contactEventType,contact_account_type,availabilityStatus,facebook_schedule_id,facebook_service_provider,facebook_event_type,facebook_owner,facebook_hostname,facebook_post_time,facebook_photo_url,facebook_mem_count"

    .line 3186
    .local v0, "COPY_QUERY":Ljava/lang/String;
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 3187
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "CHINA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "HKTW"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3189
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",setLunar"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3194
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SELECT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_id, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sync_id, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sync_version, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sync_time, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sync_local_id, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sync_dirty, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sync_mark, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "calendar_id, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "htmlUri, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "title, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "eventLocation, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "description, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "eventStatus, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "selfAttendeeStatus, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "commentsUri, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dtstart, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dtend, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "eventTimezone, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "eventTimezone2, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "duration, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "allDay, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "visibility, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "transparency, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "hasAlarm, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "hasExtendedProperties, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "rrule, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "rdate, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "exrule, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "exdate, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "originalEvent, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "originalInstanceTime, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "originalAllDay, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "lastDate, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "hasAttendeeData, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "guestsCanModify, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "guestsCanInviteOthers, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "guestsCanSeeGuests, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "organizer, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "deleted, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "syncAdapterData "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "latitude,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "longitude,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "contact_data_id,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "contact_id,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "contactEventType,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "contact_account_type,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "availabilityStatus,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_schedule_id,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_service_provider,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_event_type,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_owner,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_hostname,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_post_time,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_photo_url,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_mem_count"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3255
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 3256
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "CHINA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "HKTW"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3258
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",setLunar"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3263
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM Events_Backup;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3266
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3268
    const-string v1, "DROP TABLE Events_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3271
    const-string v1, "CREATE TRIGGER events_cleanup_delete DELETE ON Events BEGIN DELETE FROM Instances WHERE event_id=old._id;DELETE FROM EventsRawTimes WHERE event_id=old._id;DELETE FROM Attendees WHERE event_id=old._id;DELETE FROM Reminders WHERE event_id=old._id;DELETE FROM CalendarAlerts WHERE event_id=old._id;DELETE FROM ExtendedProperties WHERE event_id=old._id;DELETE FROM Memos WHERE event_id = old._id AND event_type = 1;DELETE FROM Images WHERE event_id = old._id AND event_type = 1;DELETE FROM Maps WHERE event_id = old._id;DELETE FROM Facebooks WHERE event_id = old._id;DELETE FROM Notes WHERE event_id = old._id;END"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3276
    return-void

    .line 3109
    .end local v0    # "COPY_QUERY":Ljava/lang/String;
    :cond_3
    const-string v1, "UPDATE Calendars SET account_type = \'LOCAL\', ownerAccount = \'local\' WHERE _id = \'2\'"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private upgradeToVersion301(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2992
    const-string v0, "DROP TRIGGER IF EXISTS original_sync_update;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2994
    const-string v0, "ALTER TABLE Events ADD COLUMN original_id INTEGER;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2997
    const-string v0, "UPDATE Events set original_id=(SELECT Events2._id FROM Events AS Events2 WHERE Events2._sync_id=Events.original_sync_id) WHERE Events.original_sync_id NOT NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3003
    const-string v0, "CREATE TRIGGER original_sync_update UPDATE OF _sync_id ON Events BEGIN UPDATE Events SET original_sync_id=new._sync_id WHERE original_id=old._id; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3004
    return-void
.end method

.method private upgradeToVersion302(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2978
    const-string v0, "UPDATE Events SET sync_data1=eventEndTimezone WHERE calendar_id IN (SELECT _id FROM Calendars WHERE account_type=\'com.android.exchange\');"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2981
    const-string v0, "UPDATE Events SET eventEndTimezone=NULL WHERE calendar_id IN (SELECT _id FROM Calendars WHERE account_type=\'com.android.exchange\');"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2983
    return-void
.end method

.method private upgradeToVersion303(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2905
    const-string v0, "ALTER TABLE Calendars RENAME TO Calendars_Backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2906
    const-string v0, "DROP TRIGGER IF EXISTS calendar_cleanup"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2907
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarsTable303(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2910
    const-string v0, "INSERT INTO Calendars (_id, account_name, account_type, _sync_id, _sync_version, _sync_time, dirty, name, displayName, calendar_color, access_level, visible, sync_events, calendar_location, calendar_timezone, ownerAccount, canOrganizerRespond, canModifyTimeZone, maxReminders, allowedReminders, deleted, cal_sync1, cal_sync2, cal_sync3, cal_sync4, cal_sync5, cal_sync6) SELECT _id, account_name, account_type, _sync_id, _sync_version, _sync_time, dirty, name, displayName, calendar_color, access_level, visible, sync_events, calendar_location, calendar_timezone, ownerAccount, canOrganizerRespond, canModifyTimeZone, maxReminders, allowedReminders,deleted, sync1, sync2, sync3, sync4,sync5,sync6 FROM Calendars_Backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2970
    const-string v0, "DROP TABLE Calendars_Backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2971
    return-void
.end method

.method private upgradeToVersion304(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2893
    const-string v0, "ALTER TABLE Calendars ADD COLUMN canPartiallyUpdate INTEGER DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2894
    const-string v0, "ALTER TABLE Events ADD COLUMN sync_data7 TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2895
    const-string v0, "ALTER TABLE Events ADD COLUMN lastSynced INTEGER DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2896
    return-void
.end method

.method private upgradeToVersion305(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x1

    .line 2646
    const-string v1, "ALTER TABLE Calendars RENAME TO Calendars_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2647
    const-string v1, "DROP TRIGGER IF EXISTS calendar_cleanup"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2648
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarsTable305(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2651
    const-string v1, "INSERT INTO Calendars (_id, account_name, account_type, _sync_id, cal_sync7, cal_sync8, dirty, name, calendar_displayName, calendar_color, calendar_access_level, visible, sync_events, calendar_location, calendar_timezone, ownerAccount, canOrganizerRespond, canModifyTimeZone, maxReminders, allowedReminders, deleted, canPartiallyUpdate,cal_sync1, cal_sync2, cal_sync3, cal_sync4, cal_sync5, cal_sync6) SELECT _id, account_name, account_type, _sync_id, _sync_version, _sync_time, dirty, name, displayName, calendar_color, access_level, visible, sync_events, calendar_location, calendar_timezone, ownerAccount, canOrganizerRespond, canModifyTimeZone, maxReminders, allowedReminders, deleted, canPartiallyUpdate,cal_sync1, cal_sync2, cal_sync3, cal_sync4, cal_sync5, cal_sync6 FROM Calendars_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2712
    const-string v1, "DROP TABLE Calendars_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2714
    const-string v1, "ALTER TABLE Events RENAME TO Events_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2715
    const-string v1, "DROP TRIGGER IF EXISTS events_cleanup_delete"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2716
    const-string v1, "DROP INDEX IF EXISTS eventsCalendarIdIndex"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2721
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createEventsTable307(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2723
    const-string v0, "INSERT INTO Events (_id, _sync_id, sync_data4, sync_data5, sync_data2, dirty, sync_data8, calendar_id, sync_data3, title, eventLocation, description, eventStatus, selfAttendeeStatus, sync_data6, dtstart, dtend, eventTimezone, eventEndTimezone, duration, allDay, accessLevel, availability, hasAlarm, hasExtendedProperties, rrule, rdate, exrule, exdate, original_id,original_sync_id, originalInstanceTime, originalAllDay, lastDate, hasAttendeeData, guestsCanModify, guestsCanInviteOthers, guestsCanSeeGuests, organizer, deleted, sync_data7,lastSynced,sync_data1,latitude,longitude,contact_data_id,contact_id,contactEventType,contact_account_type,availabilityStatus,facebook_schedule_id,facebook_service_provider,facebook_event_type,facebook_owner ,facebook_hostname,facebook_post_time,facebook_photo_url,facebook_mem_count"

    .line 2787
    .local v0, "COPY_QUERY":Ljava/lang/String;
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 2788
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "CHINA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "HKTW"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2790
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",setLunar"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2795
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SELECT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_id, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sync_id, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sync_version, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sync_time, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sync_local_id, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dirty, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_sync_mark, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "calendar_id, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "htmlUri, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "title, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "eventLocation, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "description, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "eventStatus, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "selfAttendeeStatus, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "commentsUri, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dtstart, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dtend, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "eventTimezone, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "eventEndTimezone, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "duration, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "allDay, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "accessLevel, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "availability, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "hasAlarm, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "hasExtendedProperties, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "rrule, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "rdate, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "exrule, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "exdate, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "original_id,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "original_sync_id, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "originalInstanceTime, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "originalAllDay, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "lastDate, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "hasAttendeeData, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "guestsCanModify, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "guestsCanInviteOthers, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "guestsCanSeeGuests, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "organizer, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "deleted, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sync_data7,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "lastSynced,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sync_data1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "latitude,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "longitude,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "contact_data_id,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "contact_id,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "contactEventType,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "contact_account_type,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "availabilityStatus,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_schedule_id,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_service_provider,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_event_type,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_owner ,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_hostname,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_post_time,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_photo_url,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "facebook_mem_count"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2860
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 2861
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "CHINA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "HKTW"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2863
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",setLunar"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2868
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM Events_Backup;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2871
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2873
    const-string v1, "DROP TABLE Events_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2876
    const-string v1, "CREATE TRIGGER events_cleanup_delete DELETE ON Events BEGIN DELETE FROM Instances WHERE event_id=old._id;DELETE FROM EventsRawTimes WHERE event_id=old._id;DELETE FROM Attendees WHERE event_id=old._id;DELETE FROM Reminders WHERE event_id=old._id;DELETE FROM CalendarAlerts WHERE event_id=old._id;DELETE FROM ExtendedProperties WHERE event_id=old._id;DELETE FROM Memos WHERE event_id = old._id AND event_type = 1;DELETE FROM Images WHERE event_id = old._id AND event_type = 1;DELETE FROM Maps WHERE event_id = old._id;DELETE FROM Facebooks WHERE event_id = old._id;DELETE FROM Notes WHERE event_id = old._id;END"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2883
    const-string v1, "CREATE TRIGGER original_sync_update UPDATE OF _sync_id ON Events BEGIN UPDATE Events SET original_sync_id=new._sync_id WHERE original_id=old._id; END"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2884
    return-void
.end method

.method private upgradeToVersion306(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2610
    const-string v0, "DROP TRIGGER IF EXISTS original_sync_update"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2611
    const-string v0, "UPDATE Events SET _sync_id = REPLACE(_sync_id, \'/private/full/\', \'/events/\'), original_sync_id = REPLACE(original_sync_id, \'/private/full/\', \'/events/\') WHERE _id IN (SELECT Events._id FROM Events JOIN Calendars ON Events.calendar_id = Calendars._id WHERE account_type = \'com.google\')"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2618
    const-string v0, "CREATE TRIGGER original_sync_update UPDATE OF _sync_id ON Events BEGIN UPDATE Events SET original_sync_id=new._sync_id WHERE original_id=old._id; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2620
    const-string v0, "UPDATE Calendars SET canPartiallyUpdate = 1 WHERE account_type = \'com.google\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2622
    const-string v0, "DELETE FROM _sync_state WHERE account_type = \'com.google\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2623
    return-void
.end method

.method private upgradeToVersion307(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2507
    const-string v1, "ALTER TABLE Events RENAME TO Events_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2508
    const-string v1, "DROP TRIGGER IF EXISTS events_cleanup_delete"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2509
    const-string v1, "DROP TRIGGER IF EXISTS original_sync_update"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2510
    const-string v1, "DROP INDEX IF EXISTS eventsCalendarIdIndex"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2511
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createEventsTable307(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2513
    const-string v0, "_id, _sync_id, dirty, lastSynced,calendar_id, title, eventLocation, description, eventColor, eventStatus, selfAttendeeStatus, dtstart, dtend, eventTimezone, duration, allDay, accessLevel, availability, hasAlarm, hasExtendedProperties, rrule, rdate, exrule, exdate, original_id,original_sync_id, originalInstanceTime, originalAllDay, lastDate, hasAttendeeData, guestsCanModify, guestsCanInviteOthers, guestsCanSeeGuests, organizer, deleted, eventEndTimezone, sync_data1,sync_data2,sync_data3,sync_data4,sync_data5,sync_data6,sync_data7,sync_data8,sync_data9,sync_data10,latitude,longitude,contact_data_id,contact_id,contactEventType,contact_account_type,availabilityStatus,facebook_schedule_id,facebook_service_provider,facebook_event_type,facebook_owner ,facebook_hostname,facebook_post_time,facebook_photo_url,facebook_mem_count"

    .line 2579
    .local v0, "FIELD_LIST":Ljava/lang/String;
    sget-object v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2580
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",setLunar"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2585
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSERT INTO Events ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") SELECT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM Events_Backup;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2588
    const-string v1, "DROP TABLE Events_Backup;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2591
    const-string v1, "CREATE TRIGGER events_cleanup_delete DELETE ON Events BEGIN DELETE FROM Instances WHERE event_id=old._id;DELETE FROM EventsRawTimes WHERE event_id=old._id;DELETE FROM Attendees WHERE event_id=old._id;DELETE FROM Reminders WHERE event_id=old._id;DELETE FROM CalendarAlerts WHERE event_id=old._id;DELETE FROM ExtendedProperties WHERE event_id=old._id;DELETE FROM Memos WHERE event_id = old._id AND event_type = 1;DELETE FROM Images WHERE event_id = old._id AND event_type = 1;DELETE FROM Maps WHERE event_id = old._id;DELETE FROM Facebooks WHERE event_id = old._id;DELETE FROM Notes WHERE event_id = old._id;END"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2596
    const-string v1, "CREATE TRIGGER original_sync_update UPDATE OF _sync_id ON Events BEGIN UPDATE Events SET original_sync_id=new._sync_id WHERE original_id=old._id; END"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2597
    return-void
.end method

.method private upgradeToVersion308(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2481
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createColorsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2483
    const-string v0, "ALTER TABLE Calendars ADD COLUMN allowedAvailability TEXT DEFAULT \'0,1\';"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2484
    const-string v0, "ALTER TABLE Calendars ADD COLUMN allowedAttendeeTypes TEXT DEFAULT \'0,1,2\';"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2485
    const-string v0, "ALTER TABLE Calendars ADD COLUMN calendar_color_index TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2486
    const-string v0, "ALTER TABLE Events ADD COLUMN eventColor_index TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2490
    const-string v0, "UPDATE Calendars SET allowedAvailability=\'0,1,2\' WHERE _id IN (SELECT _id FROM Calendars WHERE account_type=\'com.android.exchange\');"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2495
    const-string v0, "UPDATE Events SET organizer=\'My calendar\' WHERE calendar_id=1;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2499
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createColorsTriggers(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2500
    return-void
.end method

.method private upgradeToVersion400(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2442
    const-string v0, "DROP TRIGGER IF EXISTS calendar_color_update"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2444
    const-string v0, "CREATE TRIGGER calendar_color_update UPDATE OF calendar_color_index ON Calendars WHEN new.calendar_color_index NOT NULL BEGIN UPDATE Calendars SET calendar_color=(SELECT color FROM Colors WHERE account_name=new.account_name AND account_type=new.account_type AND color_index=new.calendar_color_index AND color_type=0)  WHERE _id=old._id; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2455
    const-string v0, "DROP TRIGGER IF EXISTS event_color_update"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2457
    const-string v0, "CREATE TRIGGER event_color_update UPDATE OF eventColor_index ON Events WHEN new.eventColor_index NOT NULL BEGIN UPDATE Events SET eventColor=(SELECT color FROM Colors WHERE account_name=(SELECT account_name FROM Calendars WHERE _id=new.calendar_id) AND account_type=(SELECT account_type FROM Calendars WHERE _id=new.calendar_id) AND color_index=new.eventColor_index AND color_type=1)  WHERE _id=old._id; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2470
    return-void
.end method

.method private upgradeToVersion401(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2434
    const-string v0, "UPDATE events SET original_id=(SELECT _id FROM events inner_events WHERE inner_events._sync_id=events.original_sync_id AND inner_events.calendar_id=events.calendar_id) WHERE NOT original_id IS NULL AND (SELECT calendar_id FROM events ex_events WHERE ex_events._id=events.original_id) <> calendar_id "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2439
    return-void
.end method

.method private upgradeToVersion402(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2425
    const-string v0, "ALTER TABLE Attendees ADD COLUMN attendeeIdentity TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2426
    const-string v0, "ALTER TABLE Attendees ADD COLUMN attendeeIdNamespace TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2427
    return-void
.end method

.method private upgradeToVersion403(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2386
    const-string v0, "CalendarDatabaseHelper"

    const-string v1, "upgradeToVersion403"

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2387
    const-string v0, "ALTER TABLE Events ADD COLUMN customAppPackage TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2388
    const-string v0, "ALTER TABLE Events ADD COLUMN customAppUri TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2392
    const-string v0, "ALTER TABLE Tasks ADD COLUMN syncTime TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2393
    const-string v0, "ALTER TABLE Tasks ADD COLUMN deleted INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2395
    const-string v0, "ALTER TABLE Tasks ADD COLUMN task_order INTEGER;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2397
    iget-boolean v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createNoteField:Z

    if-eqz v0, :cond_0

    .line 2398
    const-string v0, "CalendarDatabaseHelper"

    const-string v1, "add fields to Notes"

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2400
    const-string v0, "ALTER TABLE Notes ADD COLUMN locked INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2403
    const-string v0, "ALTER TABLE Notes ADD COLUMN date INTEGER DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2404
    const-string v0, "ALTER TABLE Notes ADD COLUMN memo_id INTEGER DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2408
    :cond_0
    const-string v0, "ALTER TABLE Events ADD COLUMN sticker_type INTEGER DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2410
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createStickerTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2412
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->CopyAssets(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2414
    invoke-virtual {p0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->databaseExist()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2415
    const-string v0, "Tasks"

    invoke-virtual {p0, p1, v0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->isTableExists(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2416
    invoke-virtual {p0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->markNeedToInsertTask()V

    .line 2418
    :cond_1
    return-void
.end method

.method private upgradeToVersion404(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2348
    const/16 v6, 0xa

    .line 2349
    .local v6, "HANGUEL_HOL_ID":I
    const/4 v8, 0x0

    .line 2351
    .local v8, "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->areNationalHolidaysSupported()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    instance-of v0, v0, Lcom/android/calendar/secfeature/KOR_SECCalendarFeatures;

    if-eqz v0, :cond_0

    .line 2353
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getCalendarHoliday(Landroid/content/Context;)Lcom/android/calendar/secfeature/holidays/CalendarHoliday;

    move-result-object v7

    .line 2354
    .local v7, "calHol":Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    if-nez v7, :cond_1

    .line 2379
    .end local v7    # "calHol":Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    :cond_0
    :goto_0
    return-void

    .line 2360
    .restart local v7    # "calHol":Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    :cond_1
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2361
    .local v2, "value":Landroid/content/ContentValues;
    new-instance v9, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v0, "Events"

    invoke-direct {v9, p1, v0}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2362
    .end local v8    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .local v9, "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    :try_start_1
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 2364
    const/16 v0, 0xa

    invoke-virtual {v7, v0}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayEvent(I)Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2365
    const/16 v0, 0xa

    invoke-virtual {v7, v0}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->getHolidayEvent(I)Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-result-object v1

    const/4 v3, 0x0

    const-wide/16 v4, 0x2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->pushEvent(Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;Landroid/content/ContentValues;ZJ)V

    .line 2366
    invoke-virtual {v9, v2}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    .line 2371
    :cond_2
    const-string v0, "DELETE FROM Instances;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2372
    const-string v0, "UPDATE CalendarMetaData SET minInstance=0, maxInstance=0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2374
    if-eqz v9, :cond_4

    .line 2375
    invoke-virtual {v9}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    move-object v8, v9

    .end local v9    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v8    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    goto :goto_0

    .line 2374
    .end local v2    # "value":Landroid/content/ContentValues;
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v8, :cond_3

    .line 2375
    invoke-virtual {v8}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    :cond_3
    throw v0

    .line 2374
    .end local v8    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v2    # "value":Landroid/content/ContentValues;
    .restart local v9    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    :catchall_1
    move-exception v0

    move-object v8, v9

    .end local v9    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v8    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    goto :goto_1

    .end local v8    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v9    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    :cond_4
    move-object v8, v9

    .end local v9    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    .restart local v8    # "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    goto :goto_0
.end method

.method private upgradeToVersion501(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2230
    const-string v0, "ALTER TABLE Events ADD COLUMN isOrganizer INTEGER;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2231
    const-string v0, "ALTER TABLE Calendars ADD COLUMN isPrimary INTEGER;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2233
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->isM0sticker(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2234
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->Upgrade403To50xStickers(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2236
    :cond_0
    return-void
.end method

.method private upgradeToVersion502(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2268
    const-string v0, "ALTER TABLE Events ADD COLUMN uid2445 TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2272
    const-string v0, "QSMemos"

    const-string v1, "memo_text"

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->checkTableField(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2273
    const-string v0, "ALTER TABLE QSMemos ADD COLUMN memo_text TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2276
    :cond_0
    const-string v0, "Stickers"

    const-string v1, "packageId"

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->checkTableField(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2277
    const-string v0, "ALTER TABLE Stickers ADD COLUMN packageId INTEGER DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2280
    :cond_1
    return-void
.end method

.method private upgradeToVersion503(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2288
    const-string v5, "Stickers"

    const-string v6, "downloaded"

    invoke-virtual {p0, p1, v5, v6}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->checkTableField(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2289
    const-string v5, "ALTER TABLE Stickers ADD COLUMN downloaded INTEGER DEFAULT 0;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2293
    :cond_0
    const-string v5, "Events"

    const-string v6, "sticker_ename"

    invoke-virtual {p0, p1, v5, v6}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->checkTableField(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2294
    const-string v5, "ALTER TABLE Events ADD COLUMN sticker_ename TEXT;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2295
    const-string v5, "UPDATE Events SET sticker_ename = (SELECT sticker_name FROM Stickers WHERE sticker_type = _id)"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2299
    :cond_1
    invoke-virtual {p0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->removeStickers()V

    .line 2303
    const-string v5, "SELECT filepath FROM Stickers"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2304
    .local v0, "c":Landroid/database/Cursor;
    const-string v5, "CalendarDatabaseHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "M0 stickers count:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2305
    const/4 v1, 0x0

    .line 2307
    .local v1, "filepath":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->isM0sticker(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_3

    .line 2308
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2310
    :cond_2
    const-string v5, "filepath"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2311
    const-string v5, "CalendarDatabaseHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "filepath:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2312
    const-string v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2313
    .local v2, "getFileName":[Ljava/lang/String;
    const-string v5, "CalendarDatabaseHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getFileName:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v2

    add-int/lit8 v7, v7, -0x1

    aget-object v7, v2, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2316
    sget-object v5, Lcom/android/providers/calendar/CalendarDatabaseHelper;->sStickerUpgradeNameMapM0:Ljava/util/HashMap;

    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    aget-object v6, v2, v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2317
    .local v3, "mappingName":Ljava/lang/String;
    const-string v5, "_"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 2319
    .local v4, "newStickerName":[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UPDATE Stickers SET sticker_name=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x3

    aget-object v6, v4, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\',"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "sticker_group"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x2

    aget-object v6, v4, v6

    invoke-static {v6}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " WHERE "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "filepath"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2322
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_2

    .line 2325
    .end local v2    # "getFileName":[Ljava/lang/String;
    .end local v3    # "mappingName":Ljava/lang/String;
    .end local v4    # "newStickerName":[Ljava/lang/String;
    :cond_3
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2326
    :cond_4
    const/4 v0, 0x0

    .line 2329
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->updateFilepath(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2333
    const-string v5, "Tasks"

    const-string v6, "task_order"

    invoke-virtual {p0, p1, v5, v6}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->checkTableField(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2334
    const-string v5, "ALTER TABLE Tasks ADD COLUMN task_order INTEGER DEFAULT 0;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2336
    :cond_5
    return-void

    .line 2325
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_6

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2326
    :cond_6
    const/4 v0, 0x0

    throw v5
.end method

.method private upgradeToVersion51(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4150
    const-string v0, "CalendarDatabaseHelper"

    const-string v1, "Upgrading DeletedEvents table"

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 4158
    const-string v0, "ALTER TABLE DeletedEvents ADD COLUMN calendar_id INTEGER;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4161
    const-string v0, "DROP TRIGGER IF EXISTS calendar_cleanup"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4162
    const-string v0, "CREATE TRIGGER calendar_cleanup DELETE ON Calendars BEGIN DELETE FROM Events WHERE calendar_id=old._id;DELETE FROM DeletedEvents WHERE calendar_id = old._id;END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4168
    const-string v0, "DROP TRIGGER IF EXISTS event_to_deleted"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4169
    return-void
.end method

.method private upgradeToVersion52(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4090
    const-string v6, "ALTER TABLE Events ADD COLUMN originalAllDay INTEGER;"

    invoke-virtual {p1, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4109
    const-string v6, "SELECT _id,originalEvent FROM Events WHERE originalEvent IS NOT NULL"

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 4114
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_3

    .line 4116
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 4117
    const/4 v6, 0x0

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 4118
    .local v2, "id":J
    const/4 v6, 0x1

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 4121
    .local v4, "originalEvent":Ljava/lang/String;
    const-string v6, "SELECT allDay FROM Events WHERE _sync_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-virtual {p1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 4125
    .local v5, "recur":Landroid/database/Cursor;
    if-eqz v5, :cond_0

    .line 4133
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 4134
    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 4135
    .local v0, "allDay":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "UPDATE Events SET originalAllDay="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " WHERE _id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4140
    .end local v0    # "allDay":I
    :cond_1
    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4144
    .end local v2    # "id":J
    .end local v4    # "originalEvent":Ljava/lang/String;
    .end local v5    # "recur":Landroid/database/Cursor;
    :catchall_0
    move-exception v6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v6

    .line 4140
    .restart local v2    # "id":J
    .restart local v4    # "originalEvent":Ljava/lang/String;
    .restart local v5    # "recur":Landroid/database/Cursor;
    :catchall_1
    move-exception v6

    :try_start_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4144
    .end local v2    # "id":J
    .end local v4    # "originalEvent":Ljava/lang/String;
    .end local v5    # "recur":Landroid/database/Cursor;
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4147
    :cond_3
    return-void
.end method

.method private upgradeToVersion53(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4074
    const-string v0, "CalendarDatabaseHelper"

    const-string v1, "Upgrading CalendarAlerts table"

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 4075
    const-string v0, "ALTER TABLE CalendarAlerts ADD COLUMN creationTime INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4077
    const-string v0, "ALTER TABLE CalendarAlerts ADD COLUMN receivedTime INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4079
    const-string v0, "ALTER TABLE CalendarAlerts ADD COLUMN notifyTime INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4081
    return-void
.end method

.method private upgradeToVersion54(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4068
    const-string v0, "CalendarDatabaseHelper"

    const-string v1, "adding eventSyncAccountAndIdIndex"

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 4069
    const-string v0, "CREATE INDEX eventSyncAccountAndIdIndex ON Events (_sync_account, _sync_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4071
    return-void
.end method

.method private upgradeToVersion55(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4045
    const-string v0, "ALTER TABLE Calendars ADD COLUMN _sync_account_type TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4047
    const-string v0, "ALTER TABLE Events ADD COLUMN _sync_account_type TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4049
    const-string v0, "ALTER TABLE DeletedEvents ADD COLUMN _sync_account_type TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4050
    const-string v0, "UPDATE Calendars SET _sync_account_type=\'com.google\' WHERE _sync_account IS NOT NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4053
    const-string v0, "UPDATE Events SET _sync_account_type=\'com.google\' WHERE _sync_account IS NOT NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4056
    const-string v0, "UPDATE DeletedEvents SET _sync_account_type=\'com.google\' WHERE _sync_account IS NOT NULL"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4059
    const-string v0, "CalendarDatabaseHelper"

    const-string v1, "re-creating eventSyncAccountAndIdIndex"

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 4060
    const-string v0, "DROP INDEX eventSyncAccountAndIdIndex"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4061
    const-string v0, "CREATE INDEX eventSyncAccountAndIdIndex ON Events (_sync_account_type, _sync_account, _sync_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4065
    return-void
.end method

.method private upgradeToVersion56(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3976
    const-string v5, "ALTER TABLE Calendars ADD COLUMN ownerAccount TEXT;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3978
    const-string v5, "ALTER TABLE Events ADD COLUMN hasAttendeeData INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3985
    const-string v5, "UPDATE Events SET _sync_dirty=0, _sync_version=NULL, _sync_id=REPLACE(_sync_id, \'/private/full-selfattendance\', \'/private/full\'),commentsUri=REPLACE(commentsUri, \'/private/full-selfattendance\', \'/private/full\');"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3995
    const-string v5, "UPDATE Calendars SET url=REPLACE(url, \'/private/full-selfattendance\', \'/private/full\');"

    invoke-virtual {p1, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4000
    const-string v5, "SELECT _id, url FROM Calendars"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 4004
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 4006
    :try_start_0
    const-string v3, "UPDATE Calendars SET ownerAccount=? WHERE _id=?"

    .line 4009
    .local v3, "updateSql":Ljava/lang/String;
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 4010
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 4011
    .local v1, "id":Ljava/lang/Long;
    const/4 v5, 0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 4012
    .local v4, "url":Ljava/lang/String;
    invoke-static {v4}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->calendarEmailAddressFromFeedUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4013
    .local v2, "owner":Ljava/lang/String;
    const-string v5, "UPDATE Calendars SET ownerAccount=? WHERE _id=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    invoke-virtual {p1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4016
    .end local v1    # "id":Ljava/lang/Long;
    .end local v2    # "owner":Ljava/lang/String;
    .end local v3    # "updateSql":Ljava/lang/String;
    .end local v4    # "url":Ljava/lang/String;
    :catchall_0
    move-exception v5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v5

    .restart local v3    # "updateSql":Ljava/lang/String;
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 4019
    .end local v3    # "updateSql":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private upgradeToVersion57(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3953
    const-string v0, "ALTER TABLE Events ADD COLUMN guestsCanModify INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3956
    const-string v0, "ALTER TABLE Events ADD COLUMN guestsCanInviteOthers INTEGER NOT NULL DEFAULT 1;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3959
    const-string v0, "ALTER TABLE Events ADD COLUMN guestsCanSeeGuests INTEGER NOT NULL DEFAULT 1;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3962
    const-string v0, "ALTER TABLE Events ADD COLUMN organizer STRING;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3965
    const-string v0, "UPDATE Events SET organizer=(SELECT attendeeEmail FROM Attendees WHERE Attendees.event_id=Events._id AND Attendees.attendeeRelationship=2);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3973
    return-void
.end method

.method private upgradeToVersion59(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3926
    const-string v0, "DROP TABLE IF EXISTS BusyBits;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3927
    const-string v0, "CREATE TEMPORARY TABLE CalendarMetaData_Backup(_id,localTimezone,minInstance,maxInstance);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3933
    const-string v0, "INSERT INTO CalendarMetaData_Backup SELECT _id,localTimezone,minInstance,maxInstance FROM CalendarMetaData;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3940
    const-string v0, "DROP TABLE CalendarMetaData;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3941
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createCalendarMetaDataTable59(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 3942
    const-string v0, "INSERT INTO CalendarMetaData SELECT _id,localTimezone,minInstance,maxInstance FROM CalendarMetaData_Backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3949
    const-string v0, "DROP TABLE CalendarMetaData_Backup;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3950
    return-void
.end method

.method private upgradeToVersion60(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3856
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeSyncState(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 3857
    const-string v0, "DROP TRIGGER IF EXISTS calendar_cleanup"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3858
    const-string v0, "CREATE TRIGGER calendar_cleanup DELETE ON Calendars BEGIN DELETE FROM Events WHERE calendar_id=old._id;END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3863
    const-string v0, "ALTER TABLE Events ADD COLUMN deleted INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3865
    const-string v0, "DROP TRIGGER IF EXISTS events_insert"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3867
    const-string v0, "CREATE TRIGGER events_insert AFTER INSERT ON Events BEGIN UPDATE Events SET _sync_account= (SELECT _sync_account FROM Calendars WHERE Calendars._id=new.calendar_id),_sync_account_type= (SELECT _sync_account_type FROM Calendars WHERE Calendars._id=new.calendar_id) WHERE Events._id=new._id;END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3878
    const-string v0, "DROP TABLE IF EXISTS DeletedEvents;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3879
    const-string v0, "DROP TRIGGER IF EXISTS events_cleanup_delete"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3881
    const-string v0, "CREATE TRIGGER events_cleanup_delete DELETE ON Events BEGIN DELETE FROM Instances WHERE event_id=old._id;DELETE FROM EventsRawTimes WHERE event_id=old._id;DELETE FROM Attendees WHERE event_id=old._id;DELETE FROM Reminders WHERE event_id=old._id;DELETE FROM CalendarAlerts WHERE event_id=old._id;DELETE FROM ExtendedProperties WHERE event_id=old._id;DELETE FROM Memos WHERE event_id = old._id;DELETE FROM Images WHERE event_id = old._id;DELETE FROM Maps WHERE event_id = old._id;DELETE FROM Facebooks WHERE event_id = old._id;DELETE FROM Notes WHERE event_id = old._id;END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3903
    const-string v0, "DROP TRIGGER IF EXISTS attendees_update"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3904
    const-string v0, "DROP TRIGGER IF EXISTS attendees_insert"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3905
    const-string v0, "DROP TRIGGER IF EXISTS attendees_delete"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3906
    const-string v0, "DROP TRIGGER IF EXISTS reminders_update"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3907
    const-string v0, "DROP TRIGGER IF EXISTS reminders_insert"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3908
    const-string v0, "DROP TRIGGER IF EXISTS reminders_delete"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3910
    const-string v0, "DROP TRIGGER IF EXISTS qsmemos_update"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3911
    const-string v0, "DROP TRIGGER IF EXISTS qsmemos_insert"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3912
    const-string v0, "DROP TRIGGER IF EXISTS qsmemos_delete"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3913
    const-string v0, "DROP TRIGGER IF EXISTS memos_update"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3914
    const-string v0, "DROP TRIGGER IF EXISTS memos_insert"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3915
    const-string v0, "DROP TRIGGER IF EXISTS memos_delete"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3916
    const-string v0, "DROP TRIGGER IF EXISTS images_update"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3917
    const-string v0, "DROP TRIGGER IF EXISTS images_insert"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3918
    const-string v0, "DROP TRIGGER IF EXISTS images_delete"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3920
    const-string v0, "DROP TRIGGER IF EXISTS extended_properties_update"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3921
    const-string v0, "DROP TRIGGER IF EXISTS extended_properties_insert"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3922
    const-string v0, "DROP TRIGGER IF EXISTS extended_properties_delete"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3923
    return-void
.end method

.method private upgradeToVersion600(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 2187
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2188
    const-string v0, "Events"

    const-string v1, "setLunar"

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->checkTableField(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2189
    const-string v0, "ALTER TABLE Events ADD setLunar INTEGER NOT NULL DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2193
    :cond_0
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->areNationalHolidaysSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    instance-of v0, v0, Lcom/android/calendar/secfeature/VI_SECCalendarFeatures;

    if-eqz v0, :cond_1

    .line 2194
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createLegalHolidayTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2200
    :cond_1
    const-string v0, "ALTER TABLE Events ADD COLUMN mutators TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2201
    const-string v0, "ALTER TABLE Calendars ADD COLUMN mutators TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2205
    const-string v0, "Events"

    const-string v1, "sticker_ename"

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->checkTableField(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2206
    const-string v0, "ALTER TABLE Events ADD COLUMN sticker_ename TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2207
    const-string v0, "UPDATE Events SET sticker_ename = (SELECT sticker_name FROM Stickers WHERE sticker_type = _id)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2213
    :cond_2
    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->areNationalHolidaysSupported()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    sget-object v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getCalendarSubstituteHoliday(Landroid/content/Context;)Lcom/android/calendar/secfeature/holidays/CalendarHoliday;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2215
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createSubstituteHolidayTable(Landroid/database/sqlite/SQLiteDatabase;)I

    .line 2217
    :cond_3
    return-void
.end method

.method private upgradeToVersion61(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3837
    const-string v0, "DROP TABLE IF EXISTS CalendarCache;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3840
    const-string v0, "CREATE TABLE IF NOT EXISTS CalendarCache (_id INTEGER PRIMARY KEY,key TEXT NOT NULL,value TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3846
    const-string v0, "INSERT INTO CalendarCache (key, value) VALUES (\'timezoneDatabaseVersion\',\'2009s\');"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3852
    return-void
.end method

.method private upgradeToVersion62(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 18
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3757
    const-string v15, "ALTER TABLE Events ADD COLUMN dtstart2 INTEGER;"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3759
    const-string v15, "ALTER TABLE Events ADD COLUMN dtend2 INTEGER;"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3761
    const-string v15, "ALTER TABLE Events ADD COLUMN eventTimezone2 TEXT;"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3764
    const/4 v15, 0x1

    new-array v2, v15, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "0"

    aput-object v16, v2, v15

    .line 3766
    .local v2, "allDayBit":[Ljava/lang/String;
    const-string v15, "UPDATE Events SET dtstart2=dtstart,dtend2=dtend,eventTimezone2=eventTimezone WHERE allDay=?;"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3774
    const/4 v15, 0x0

    const-string v16, "1"

    aput-object v16, v2, v15

    .line 3775
    const-string v15, "SELECT Events._id,dtstart,dtend,eventTimezone,timezone FROM Events INNER JOIN Calendars WHERE Events.calendar_id=Calendars._id AND allDay=?"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 3785
    .local v3, "cursor":Landroid/database/Cursor;
    new-instance v13, Landroid/text/format/Time;

    invoke-direct {v13}, Landroid/text/format/Time;-><init>()V

    .line 3786
    .local v13, "oldTime":Landroid/text/format/Time;
    new-instance v12, Landroid/text/format/Time;

    invoke-direct {v12}, Landroid/text/format/Time;-><init>()V

    .line 3788
    .local v12, "newTime":Landroid/text/format/Time;
    if-eqz v3, :cond_2

    .line 3790
    const/4 v15, 0x4

    :try_start_0
    new-array v9, v15, [Ljava/lang/String;

    .line 3791
    .local v9, "newData":[Ljava/lang/String;
    const/4 v15, -0x1

    invoke-interface {v3, v15}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3792
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v15

    if-eqz v15, :cond_1

    .line 3793
    const/4 v15, 0x0

    invoke-interface {v3, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 3794
    .local v10, "id":J
    const/4 v15, 0x1

    invoke-interface {v3, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 3795
    .local v6, "dtstart":J
    const/4 v15, 0x2

    invoke-interface {v3, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 3796
    .local v4, "dtend":J
    const/4 v15, 0x3

    invoke-interface {v3, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 3797
    .local v8, "eTz":Ljava/lang/String;
    const/4 v15, 0x4

    invoke-interface {v3, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 3799
    .local v14, "tz":Ljava/lang/String;
    if-nez v8, :cond_0

    .line 3800
    const-string v8, "UTC"

    .line 3804
    :cond_0
    invoke-virtual {v13, v8}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 3805
    invoke-virtual {v13, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 3806
    invoke-virtual {v12, v14}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 3807
    iget v15, v13, Landroid/text/format/Time;->monthDay:I

    iget v0, v13, Landroid/text/format/Time;->month:I

    move/from16 v16, v0

    iget v0, v13, Landroid/text/format/Time;->year:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v12, v15, v0, v1}, Landroid/text/format/Time;->set(III)V

    .line 3808
    const/4 v15, 0x0

    invoke-virtual {v12, v15}, Landroid/text/format/Time;->normalize(Z)J

    .line 3809
    const/4 v15, 0x0

    invoke-virtual {v12, v15}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    .line 3812
    invoke-virtual {v13, v8}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 3813
    invoke-virtual {v13, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 3814
    invoke-virtual {v12, v14}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 3815
    iget v15, v13, Landroid/text/format/Time;->monthDay:I

    iget v0, v13, Landroid/text/format/Time;->month:I

    move/from16 v16, v0

    iget v0, v13, Landroid/text/format/Time;->year:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v12, v15, v0, v1}, Landroid/text/format/Time;->set(III)V

    .line 3816
    const/4 v15, 0x0

    invoke-virtual {v12, v15}, Landroid/text/format/Time;->normalize(Z)J

    .line 3817
    const/4 v15, 0x0

    invoke-virtual {v12, v15}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 3819
    const/4 v15, 0x0

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v9, v15

    .line 3820
    const/4 v15, 0x1

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v9, v15

    .line 3821
    const/4 v15, 0x2

    aput-object v14, v9, v15

    .line 3822
    const/4 v15, 0x3

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v9, v15

    .line 3823
    const-string v15, "UPDATE Events SET dtstart2=?, dtend2=?, eventTimezone2=? WHERE _id=?"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v9}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 3831
    .end local v4    # "dtend":J
    .end local v6    # "dtstart":J
    .end local v8    # "eTz":Ljava/lang/String;
    .end local v9    # "newData":[Ljava/lang/String;
    .end local v10    # "id":J
    .end local v14    # "tz":Ljava/lang/String;
    :catchall_0
    move-exception v15

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v15

    .restart local v9    # "newData":[Ljava/lang/String;
    :cond_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 3834
    .end local v9    # "newData":[Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private upgradeToVersion64(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3751
    const-string v0, "ALTER TABLE Events ADD COLUMN syncAdapterData TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3753
    return-void
.end method

.method private upgradeToVersion66(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3745
    const-string v0, "ALTER TABLE Calendars ADD COLUMN organizerCanRespond INTEGER NOT NULL DEFAULT 1;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3747
    return-void
.end method

.method public static upgradeToVersion69(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 25
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3573
    const-string v15, "SELECT _id, dtstart, dtend, duration, dtstart2, dtend2, eventTimezone, eventTimezone2, rrule FROM Events WHERE allDay=?"

    .line 3584
    .local v15, "sql":Ljava/lang/String;
    const-string v21, "SELECT _id, dtstart, dtend, duration, dtstart2, dtend2, eventTimezone, eventTimezone2, rrule FROM Events WHERE allDay=?"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const-string v24, "1"

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 3585
    .local v4, "cursor":Landroid/database/Cursor;
    if-eqz v4, :cond_d

    .line 3594
    :try_start_0
    new-instance v16, Landroid/text/format/Time;

    invoke-direct/range {v16 .. v16}, Landroid/text/format/Time;-><init>()V

    .line 3597
    .local v16, "time":Landroid/text/format/Time;
    const-string v20, "UTC"

    .line 3598
    .local v20, "utc":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v21

    if-eqz v21, :cond_c

    .line 3599
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 3600
    .local v13, "rrule":Ljava/lang/String;
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    .line 3601
    .local v11, "id":Ljava/lang/Long;
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 3602
    .local v8, "dtstart":Ljava/lang/Long;
    const/4 v9, 0x0

    .line 3603
    .local v9, "dtstart2":Ljava/lang/Long;
    const/16 v21, 0x6

    move/from16 v0, v21

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 3604
    .local v17, "timezone":Ljava/lang/String;
    const/16 v21, 0x7

    move/from16 v0, v21

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 3605
    .local v18, "timezone2":Ljava/lang/String;
    const/16 v21, 0x3

    move/from16 v0, v21

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 3607
    .local v10, "duration":Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 3610
    const/16 v21, 0x2

    move/from16 v0, v21

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 3611
    .local v6, "dtend":Ljava/lang/Long;
    const/4 v7, 0x0

    .line 3614
    .local v7, "dtend2":Ljava/lang/Long;
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_1

    .line 3615
    const/16 v21, 0x4

    move/from16 v0, v21

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 3616
    const/16 v21, 0x5

    move/from16 v0, v21

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 3619
    :cond_1
    const/16 v19, 0x0

    .line 3620
    .local v19, "update":Z
    const-string v21, "UTC"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_2

    .line 3621
    const/16 v19, 0x1

    .line 3622
    const-string v17, "UTC"

    .line 3625
    :cond_2
    invoke-virtual/range {v16 .. v17}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 3626
    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v8}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->fixAllDayTime(Landroid/text/format/Time;Ljava/lang/String;Ljava/lang/Long;)Z

    move-result v21

    or-int v19, v19, v21

    .line 3627
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 3629
    invoke-virtual/range {v16 .. v17}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 3630
    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v6}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->fixAllDayTime(Landroid/text/format/Time;Ljava/lang/String;Ljava/lang/Long;)Z

    move-result v21

    or-int v19, v19, v21

    .line 3631
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 3633
    if-eqz v9, :cond_3

    .line 3634
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 3635
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1, v9}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->fixAllDayTime(Landroid/text/format/Time;Ljava/lang/String;Ljava/lang/Long;)Z

    move-result v21

    or-int v19, v19, v21

    .line 3636
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 3639
    :cond_3
    if-eqz v7, :cond_4

    .line 3640
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 3641
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1, v7}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->fixAllDayTime(Landroid/text/format/Time;Ljava/lang/String;Ljava/lang/Long;)Z

    move-result v21

    or-int v19, v19, v21

    .line 3642
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 3645
    :cond_4
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_5

    .line 3646
    const/16 v19, 0x1

    .line 3649
    :cond_5
    if-eqz v19, :cond_0

    .line 3651
    const-string v21, "UPDATE Events SET dtstart=?, dtend=?, dtstart2=?, dtend2=?, duration=?, eventTimezone=?, eventTimezone2=? WHERE _id=?"

    const/16 v22, 0x8

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v8, v22, v23

    const/16 v23, 0x1

    aput-object v6, v22, v23

    const/16 v23, 0x2

    aput-object v9, v22, v23

    const/16 v23, 0x3

    aput-object v7, v22, v23

    const/16 v23, 0x4

    const/16 v24, 0x0

    aput-object v24, v22, v23

    const/16 v23, 0x5

    aput-object v17, v22, v23

    const/16 v23, 0x6

    aput-object v18, v22, v23

    const/16 v23, 0x7

    aput-object v11, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 3737
    .end local v6    # "dtend":Ljava/lang/Long;
    .end local v7    # "dtend2":Ljava/lang/Long;
    .end local v8    # "dtstart":Ljava/lang/Long;
    .end local v9    # "dtstart2":Ljava/lang/Long;
    .end local v10    # "duration":Ljava/lang/String;
    .end local v11    # "id":Ljava/lang/Long;
    .end local v13    # "rrule":Ljava/lang/String;
    .end local v16    # "time":Landroid/text/format/Time;
    .end local v17    # "timezone":Ljava/lang/String;
    .end local v18    # "timezone2":Ljava/lang/String;
    .end local v19    # "update":Z
    .end local v20    # "utc":Ljava/lang/String;
    :catchall_0
    move-exception v21

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v21

    .line 3676
    .restart local v8    # "dtstart":Ljava/lang/Long;
    .restart local v9    # "dtstart2":Ljava/lang/Long;
    .restart local v10    # "duration":Ljava/lang/String;
    .restart local v11    # "id":Ljava/lang/Long;
    .restart local v13    # "rrule":Ljava/lang/String;
    .restart local v16    # "time":Landroid/text/format/Time;
    .restart local v17    # "timezone":Ljava/lang/String;
    .restart local v18    # "timezone2":Ljava/lang/String;
    .restart local v20    # "utc":Ljava/lang/String;
    :cond_6
    :try_start_1
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_7

    .line 3677
    const/16 v21, 0x4

    move/from16 v0, v21

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 3680
    :cond_7
    const/16 v19, 0x0

    .line 3681
    .restart local v19    # "update":Z
    const-string v21, "UTC"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_8

    .line 3682
    const/16 v19, 0x1

    .line 3683
    const-string v17, "UTC"

    .line 3686
    :cond_8
    invoke-virtual/range {v16 .. v17}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 3687
    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v8}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->fixAllDayTime(Landroid/text/format/Time;Ljava/lang/String;Ljava/lang/Long;)Z

    move-result v21

    or-int v19, v19, v21

    .line 3688
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 3690
    if-eqz v9, :cond_9

    .line 3691
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 3692
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1, v9}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->fixAllDayTime(Landroid/text/format/Time;Ljava/lang/String;Ljava/lang/Long;)Z

    move-result v21

    or-int v19, v19, v21

    .line 3693
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 3696
    :cond_9
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 3698
    const-string v10, "P1D"

    .line 3699
    const/16 v19, 0x1

    .line 3712
    :cond_a
    :goto_1
    if-eqz v19, :cond_0

    .line 3714
    const-string v21, "UPDATE Events SET dtstart=?, dtend=?, dtstart2=?, dtend2=?, duration=?,eventTimezone=?, eventTimezone2=? WHERE _id=?"

    const/16 v22, 0x8

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v8, v22, v23

    const/16 v23, 0x1

    const/16 v24, 0x0

    aput-object v24, v22, v23

    const/16 v23, 0x2

    aput-object v9, v22, v23

    const/16 v23, 0x3

    const/16 v24, 0x0

    aput-object v24, v22, v23

    const/16 v23, 0x4

    aput-object v10, v22, v23

    const/16 v23, 0x5

    aput-object v17, v22, v23

    const/16 v23, 0x6

    aput-object v18, v22, v23

    const/16 v23, 0x7

    aput-object v11, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 3701
    :cond_b
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v12

    .line 3703
    .local v12, "len":I
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->charAt(I)C

    move-result v21

    const/16 v22, 0x50

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    add-int/lit8 v21, v12, -0x1

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->charAt(I)C

    move-result v21

    const/16 v22, 0x53

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    .line 3705
    const/16 v21, 0x1

    add-int/lit8 v22, v12, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 3706
    .local v14, "seconds":I
    const v21, 0x15180

    add-int v21, v21, v14

    add-int/lit8 v21, v21, -0x1

    const v22, 0x15180

    div-int v5, v21, v22

    .line 3707
    .local v5, "days":I
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "P"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "D"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v10

    .line 3708
    const/16 v19, 0x1

    goto/16 :goto_1

    .line 3737
    .end local v5    # "days":I
    .end local v8    # "dtstart":Ljava/lang/Long;
    .end local v9    # "dtstart2":Ljava/lang/Long;
    .end local v10    # "duration":Ljava/lang/String;
    .end local v11    # "id":Ljava/lang/Long;
    .end local v12    # "len":I
    .end local v13    # "rrule":Ljava/lang/String;
    .end local v14    # "seconds":I
    .end local v17    # "timezone":Ljava/lang/String;
    .end local v18    # "timezone2":Ljava/lang/String;
    .end local v19    # "update":Z
    :cond_c
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 3740
    .end local v16    # "time":Landroid/text/format/Time;
    .end local v20    # "utc":Ljava/lang/String;
    :cond_d
    return-void
.end method


# virtual methods
.method public TaskGroupInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 701
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTaskGroupInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public TasksAccountsInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 704
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTasksAccountsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public TasksInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 698
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTasksInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public TasksRemindersInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 707
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTasksReminderInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public attendeesInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 657
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mAttendeesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public calendarAlertsInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 665
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mCalendarAlertsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public calendarsInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 629
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mCalendarsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method checkJapanCalendar(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 5980
    const-string v2, "SELECT _id FROM Calendars WHERE account_type=\'LOCAL\' AND _id != \'1\'"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 5986
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 5988
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createJapanHolidayTable(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6002
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 6004
    return-void

    .line 5990
    :cond_0
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5991
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 5992
    .local v1, "id":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DELETE FROM Events WHERE calendar_id = \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\';"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5995
    invoke-virtual {p0, p1, v1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->insertJANEvent(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 6002
    .end local v1    # "id":I
    :catchall_0
    move-exception v2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v2

    .line 5998
    :cond_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createJapanHolidayTable(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public checkTableField(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "table"    # Ljava/lang/String;
    .param p3, "field"    # Ljava/lang/String;

    .prologue
    .line 5419
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT sql FROM sqlite_master WHERE name=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' AND sql LIKE \'%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 5420
    .local v0, "c":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 5423
    .local v1, "check":Z
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-lez v2, :cond_0

    .line 5424
    const/4 v1, 0x1

    .line 5427
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 5429
    return v1

    .line 5427
    :catchall_0
    move-exception v2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v2
.end method

.method public colorsInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 633
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mColorsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public createColorsTriggers(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1305
    const-string v0, "CREATE TRIGGER event_color_update UPDATE OF eventColor_index ON Events WHEN new.eventColor_index NOT NULL BEGIN UPDATE Events SET eventColor=(SELECT color FROM Colors WHERE account_name=(SELECT account_name FROM Calendars WHERE _id=new.calendar_id) AND account_type=(SELECT account_type FROM Calendars WHERE _id=new.calendar_id) AND color_index=new.eventColor_index AND color_type=1)  WHERE _id=old._id; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1306
    const-string v0, "CREATE TRIGGER calendar_color_update UPDATE OF calendar_color_index ON Calendars WHEN new.calendar_color_index NOT NULL BEGIN UPDATE Calendars SET calendar_color=(SELECT color FROM Colors WHERE account_name=new.account_name AND account_type=new.account_type AND color_index=new.calendar_color_index AND color_type=0)  WHERE _id=old._id; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1307
    return-void
.end method

.method createLocalCalendar(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)J
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "calendar_name"    # Ljava/lang/String;
    .param p3, "color"    # I

    .prologue
    const/4 v6, 0x1

    .line 4383
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 4384
    .local v1, "values":Landroid/content/ContentValues;
    const-string v4, "account_name"

    const-string v5, "My calendar"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4385
    const-string v4, "account_type"

    const-string v5, "LOCAL"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4386
    const-string v4, "name"

    invoke-virtual {v1, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4387
    const-string v4, "calendar_displayName"

    invoke-virtual {v1, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4388
    const-string v4, "calendar_access_level"

    const/16 v5, 0x2bc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4389
    const-string v4, "visible"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4390
    const-string v4, "sync_events"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4391
    const-string v4, "calendar_color"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4392
    const-string v4, "ownerAccount"

    const-string v5, "My calendar"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4394
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v4, "Calendars"

    invoke-direct {v0, p1, v4}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 4396
    .local v0, "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    invoke-virtual {v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 4397
    .local v2, "id":J
    invoke-virtual {v0}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 4398
    return-wide v2
.end method

.method createMyCalendar(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v4, 0x1

    .line 4365
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 4366
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "account_name"

    const-string v3, "My calendar"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4367
    const-string v2, "account_type"

    const-string v3, "LOCAL"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4368
    const-string v2, "name"

    const-string v3, "My calendar"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4369
    const-string v2, "calendar_displayName"

    const-string v3, "My calendar"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4370
    const-string v2, "calendar_access_level"

    const/16 v3, 0x2bc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4371
    const-string v2, "visible"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4372
    const-string v2, "sync_events"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4373
    const-string v2, "calendar_color"

    sget v3, Lcom/android/calendar/CalendarContractSec$CalendarsSec;->MY_CALENDAR_COLOR:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4374
    const-string v2, "ownerAccount"

    const-string v3, "My calendar"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4376
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v2, "Calendars"

    invoke-direct {v0, p1, v2}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 4377
    .local v0, "calendarsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    invoke-virtual {v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    .line 4379
    invoke-virtual {v0}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 4380
    return-void
.end method

.method createMyTask(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4404
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 4405
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "_sync_account"

    const-string v3, "My Task"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4406
    const-string v2, "_sync_account_key"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4407
    const-string v2, "_sync_account_type"

    const-string v3, "local"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4408
    const-string v2, "url"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4409
    const-string v2, "selected"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4410
    const-string v2, "displayName"

    const-string v3, "My Task"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4411
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v2, "TasksAccounts"

    invoke-direct {v0, p1, v2}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 4412
    .local v0, "TasksAccountsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    invoke-virtual {v0, v1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTaskAccountId:J

    .line 4414
    invoke-virtual {v0}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 4415
    return-void
.end method

.method public createTasksView(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4509
    const-string v1, "DROP VIEW IF EXISTS view_tasks;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4510
    const-string v0, "SELECT Tasks._id AS _id,syncServerId,Tasks.displayName AS displayName,_sync_dirty,clientId,sourceid,bodyType,body_size,body_truncated,due_date,utc_due_date,importance,date_completed,complete,recurrence_type,recurrence_start,recurrence_until,recurrence_occurrences,recurrence_interval,recurrence_day_of_month,recurrence_day_of_week,recurrence_week_of_month,recurrence_month_of_year,recurrence_regenerate,recurrence_dead_occur,reminder_set,reminder_time,sensitivity,start_date,utc_start_date,parentId,subject,body,category1,category2,category3,mailboxKey,accountKey,accountName,reminder_type,syncTime,deleted,glance_deleted,task_order,_sync_account,tg.group_order,ta.selected AS selected ,groupId,previousId,tg.selected AS groupSelected,groupName FROM (Tasks INNER JOIN TaskGroup tg ON tg._id=Tasks.groupId) JOIN TasksAccounts ta ON (Tasks.accountKey=ta._sync_account_key)"

    .line 4573
    .local v0, "tasksSelect":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE VIEW view_tasks AS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4574
    return-void
.end method

.method public databaseExist()Z
    .locals 3

    .prologue
    .line 6135
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v2, "/data/data/com.android.providers.tasks/databases/tasks.db"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6136
    .local v0, "dbFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 6138
    .end local v0    # "dbFile":Ljava/io/File;
    :goto_0
    return v2

    .line 6137
    :catch_0
    move-exception v1

    .line 6138
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected duplicateEvent(J)V
    .locals 11
    .param p1, "id"    # J

    .prologue
    .line 4866
    invoke-virtual {p0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 4873
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    const-string v6, "SELECT canPartiallyUpdate FROM view_events WHERE _id = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v2, v6, v7}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 4878
    .local v0, "canPartiallyUpdate":J
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-nez v6, :cond_1

    .line 4908
    .end local v0    # "canPartiallyUpdate":J
    :cond_0
    :goto_0
    return-void

    .line 4882
    :catch_0
    move-exception v3

    .line 4883
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    goto :goto_0

    .line 4887
    .end local v3    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v0    # "canPartiallyUpdate":J
    :cond_1
    const-string v6, "INSERT INTO Events  (_sync_id,calendar_id,title,eventLocation,description,eventColor,eventColor_index,eventStatus,selfAttendeeStatus,dtstart,dtend,eventTimezone,eventEndTimezone,duration,allDay,accessLevel,availability,hasAlarm,hasExtendedProperties,rrule,rdate,exrule,exdate,original_sync_id,original_id,originalInstanceTime,originalAllDay,lastDate,hasAttendeeData,guestsCanModify,guestsCanInviteOthers,guestsCanSeeGuests,organizer,isOrganizer,customAppPackage,customAppUri,uid2445,dirty,lastSynced) SELECT _sync_id,calendar_id,title,eventLocation,description,eventColor,eventColor_index,eventStatus,selfAttendeeStatus,dtstart,dtend,eventTimezone,eventEndTimezone,duration,allDay,accessLevel,availability,hasAlarm,hasExtendedProperties,rrule,rdate,exrule,exdate,original_sync_id,original_id,originalInstanceTime,originalAllDay,lastDate,hasAttendeeData,guestsCanModify,guestsCanInviteOthers,guestsCanSeeGuests,organizer,isOrganizer,customAppPackage,customAppUri,uid2445, 0, 1 FROM Events WHERE _id = ? AND dirty = ?"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 4897
    const-string v6, "SELECT CASE changes() WHEN 0 THEN -1 ELSE last_insert_rowid() END"

    const/4 v7, 0x0

    invoke-static {v2, v6, v7}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    .line 4899
    .local v4, "newId":J
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_0

    .line 4903
    const-string v6, "CalendarDatabaseHelper"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 4904
    const-string v6, "CalendarDatabaseHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Duplicating event "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " into new event "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/providers/calendar/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4907
    :cond_2
    invoke-static {v2, v4, v5, p1, p2}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->copyEventRelatedTables(Landroid/database/sqlite/SQLiteDatabase;JJ)V

    goto :goto_0
.end method

.method public eventsInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 637
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mEventsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public eventsRawTimesReplace(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 645
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mEventsRawTimesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->replace(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public extendedPropertiesInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 669
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mExtendedPropertiesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public facebookInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 692
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mFacebooksInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public forceRestoreStickerAsset()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    .line 6216
    iget-object v11, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 6217
    .local v1, "assetManager":Landroid/content/res/AssetManager;
    const/4 v3, 0x0

    .line 6219
    .local v3, "files":[Ljava/lang/String;
    const-string v11, "CalendarDatabaseHelper"

    const-string v12, "forceRestoreStickerAsset"

    invoke-static {v11, v12}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6221
    :try_start_0
    const-string v11, "stickers"

    invoke-virtual {v1, v11}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 6227
    :goto_0
    if-eqz v3, :cond_8

    array-length v11, v3

    if-le v11, v13, :cond_8

    .line 6229
    const-string v11, "CalendarDatabaseHelper"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "len:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    array-length v13, v3

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6230
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v11, v3

    if-ge v4, v11, :cond_8

    .line 6231
    const/4 v5, 0x0

    .line 6232
    .local v5, "in":Ljava/io/InputStream;
    const/4 v7, 0x0

    .line 6235
    .local v7, "out":Ljava/io/OutputStream;
    :try_start_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "stickers/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v3, v4

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 6238
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v10

    .line 6240
    .local v10, "str":Ljava/lang/String;
    const-string v11, "mounted"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 6245
    :goto_2
    iget-object v11, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v11

    iget-object v0, v11, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 6246
    .local v0, "appPath":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/sticker/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    .line 6247
    new-instance v6, Ljava/io/File;

    iget-object v11, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    invoke-direct {v6, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6249
    .local v6, "mpath":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-nez v11, :cond_0

    .line 6250
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    .line 6251
    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v12}, Ljava/io/File;->setReadable(ZZ)Z

    .line 6252
    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v12}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 6256
    :cond_0
    new-instance v8, Ljava/io/FileOutputStream;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v3, v4

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 6259
    .end local v7    # "out":Ljava/io/OutputStream;
    .local v8, "out":Ljava/io/OutputStream;
    :try_start_2
    invoke-direct {p0, v5, v8}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 6261
    new-instance v9, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v3, v4

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6262
    .local v9, "readR":Ljava/io/File;
    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-virtual {v9, v11, v12}, Ljava/io/File;->setReadable(ZZ)Z

    .line 6263
    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-virtual {v9, v11, v12}, Ljava/io/File;->setExecutable(ZZ)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_7

    .line 6268
    if-eqz v5, :cond_1

    .line 6269
    :try_start_3
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 6274
    :cond_1
    const/4 v5, 0x0

    .line 6278
    :goto_3
    if-eqz v8, :cond_2

    .line 6279
    :try_start_4
    invoke-virtual {v8}, Ljava/io/OutputStream;->flush()V

    .line 6280
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 6285
    :cond_2
    const/4 v7, 0x0

    .line 6230
    .end local v0    # "appPath":Ljava/lang/String;
    .end local v6    # "mpath":Ljava/io/File;
    .end local v8    # "out":Ljava/io/OutputStream;
    .end local v9    # "readR":Ljava/io/File;
    .end local v10    # "str":Ljava/lang/String;
    .restart local v7    # "out":Ljava/io/OutputStream;
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 6222
    .end local v4    # "i":I
    .end local v5    # "in":Ljava/io/InputStream;
    .end local v7    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v2

    .line 6223
    .local v2, "e":Ljava/io/IOException;
    const-string v11, "CalendarDatabaseHelper"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 6242
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v4    # "i":I
    .restart local v5    # "in":Ljava/io/InputStream;
    .restart local v7    # "out":Ljava/io/OutputStream;
    .restart local v10    # "str":Ljava/lang/String;
    :cond_3
    :try_start_5
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    goto/16 :goto_2

    .line 6264
    .end local v10    # "str":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 6265
    .local v2, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_6
    const-string v11, "CalendarDatabaseHelper"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Cause:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " Msg:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 6268
    if-eqz v5, :cond_4

    .line 6269
    :try_start_7
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 6274
    :cond_4
    const/4 v5, 0x0

    .line 6278
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_6
    if-eqz v7, :cond_5

    .line 6279
    :try_start_8
    invoke-virtual {v7}, Ljava/io/OutputStream;->flush()V

    .line 6280
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 6285
    :cond_5
    const/4 v7, 0x0

    .line 6286
    goto :goto_4

    .line 6271
    .end local v7    # "out":Ljava/io/OutputStream;
    .restart local v0    # "appPath":Ljava/lang/String;
    .restart local v6    # "mpath":Ljava/io/File;
    .restart local v8    # "out":Ljava/io/OutputStream;
    .restart local v9    # "readR":Ljava/io/File;
    .restart local v10    # "str":Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 6272
    .local v2, "e":Ljava/io/IOException;
    :try_start_9
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 6274
    const/4 v5, 0x0

    .line 6275
    goto :goto_3

    .line 6274
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    const/4 v5, 0x0

    throw v11

    .line 6282
    :catch_3
    move-exception v2

    .line 6283
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_a
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 6285
    const/4 v7, 0x0

    .line 6286
    .end local v8    # "out":Ljava/io/OutputStream;
    .restart local v7    # "out":Ljava/io/OutputStream;
    goto :goto_4

    .line 6285
    .end local v2    # "e":Ljava/io/IOException;
    .end local v7    # "out":Ljava/io/OutputStream;
    .restart local v8    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v11

    const/4 v7, 0x0

    .end local v8    # "out":Ljava/io/OutputStream;
    .restart local v7    # "out":Ljava/io/OutputStream;
    throw v11

    .line 6271
    .end local v0    # "appPath":Ljava/lang/String;
    .end local v6    # "mpath":Ljava/io/File;
    .end local v9    # "readR":Ljava/io/File;
    .end local v10    # "str":Ljava/lang/String;
    .local v2, "e":Ljava/lang/Exception;
    :catch_4
    move-exception v2

    .line 6272
    .local v2, "e":Ljava/io/IOException;
    :try_start_b
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 6274
    const/4 v5, 0x0

    .line 6275
    goto :goto_6

    .line 6274
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_2
    move-exception v11

    const/4 v5, 0x0

    throw v11

    .line 6282
    :catch_5
    move-exception v2

    .line 6283
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_c
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 6285
    const/4 v7, 0x0

    .line 6286
    goto :goto_4

    .line 6285
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_3
    move-exception v11

    const/4 v7, 0x0

    throw v11

    .line 6267
    :catchall_4
    move-exception v11

    .line 6268
    :goto_7
    if-eqz v5, :cond_6

    .line 6269
    :try_start_d
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    .line 6274
    :cond_6
    const/4 v5, 0x0

    .line 6278
    :goto_8
    if-eqz v7, :cond_7

    .line 6279
    :try_start_e
    invoke-virtual {v7}, Ljava/io/OutputStream;->flush()V

    .line 6280
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    .line 6285
    :cond_7
    const/4 v7, 0x0

    :goto_9
    throw v11

    .line 6271
    :catch_6
    move-exception v2

    .line 6272
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_f
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    .line 6274
    const/4 v5, 0x0

    .line 6275
    goto :goto_8

    .line 6274
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_5
    move-exception v11

    const/4 v5, 0x0

    throw v11

    .line 6282
    :catch_7
    move-exception v2

    .line 6283
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_10
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    .line 6285
    const/4 v7, 0x0

    .line 6286
    goto :goto_9

    .line 6285
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_6
    move-exception v11

    const/4 v7, 0x0

    throw v11

    .line 6290
    .end local v4    # "i":I
    .end local v5    # "in":Ljava/io/InputStream;
    .end local v7    # "out":Ljava/io/OutputStream;
    :cond_8
    return-void

    .line 6267
    .restart local v0    # "appPath":Ljava/lang/String;
    .restart local v4    # "i":I
    .restart local v5    # "in":Ljava/io/InputStream;
    .restart local v6    # "mpath":Ljava/io/File;
    .restart local v8    # "out":Ljava/io/OutputStream;
    .restart local v10    # "str":Ljava/lang/String;
    :catchall_7
    move-exception v11

    move-object v7, v8

    .end local v8    # "out":Ljava/io/OutputStream;
    .restart local v7    # "out":Ljava/io/OutputStream;
    goto :goto_7

    .line 6264
    .end local v7    # "out":Ljava/io/OutputStream;
    .restart local v8    # "out":Ljava/io/OutputStream;
    :catch_8
    move-exception v2

    move-object v7, v8

    .end local v8    # "out":Ljava/io/OutputStream;
    .restart local v7    # "out":Ljava/io/OutputStream;
    goto/16 :goto_5
.end method

.method getNeedToInsertTask()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6156
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    const-string v2, "CalendarDatabaseHelper"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 6157
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "needInsertTask"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public getNeedToRestoreSticker()Z
    .locals 12

    .prologue
    .line 6181
    iget-object v7, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 6182
    .local v1, "assetManager":Landroid/content/res/AssetManager;
    const/4 v5, 0x0

    .line 6183
    .local v5, "files":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 6184
    .local v4, "file":Ljava/io/File;
    const/4 v2, 0x0

    .line 6189
    .local v2, "bRestore":Z
    :try_start_0
    const-string v7, "stickers"

    invoke-virtual {v1, v7}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 6194
    :goto_0
    iget-object v7, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iget-object v0, v7, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 6195
    .local v0, "appPath":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/sticker/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    .line 6197
    if-eqz v5, :cond_1

    .line 6198
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    array-length v7, v5

    if-ge v6, v7, :cond_1

    .line 6200
    new-instance v4, Ljava/io/File;

    .end local v4    # "file":Ljava/io/File;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v5, v6

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6202
    .restart local v4    # "file":Ljava/io/File;
    if-eqz v4, :cond_2

    .line 6203
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-nez v7, :cond_2

    .line 6204
    :cond_0
    const/4 v2, 0x1

    .line 6212
    .end local v6    # "i":I
    :cond_1
    return v2

    .line 6190
    .end local v0    # "appPath":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 6191
    .local v3, "e":Ljava/io/IOException;
    const-string v7, "CalendarDatabaseHelper"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6198
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v0    # "appPath":Ljava/lang/String;
    .restart local v6    # "i":I
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method public getSubstHolsCalendarVersion(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    .line 5438
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    const-string v2, "CalendarDatabaseHelper"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 5439
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "substHolsCalendarVersion"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public getSyncState()Lcom/android/common/content/SyncStateContentProviderHelper;
    .locals 1

    .prologue
    .line 4206
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSyncState:Lcom/android/common/content/SyncStateContentProviderHelper;

    return-object v0
.end method

.method public getVersionOfContextProviders()I
    .locals 6

    .prologue
    .line 6321
    const/4 v2, -0x1

    .line 6323
    .local v2, "version":I
    :try_start_0
    iget-object v3, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 6325
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6329
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 6326
    :catch_0
    move-exception v0

    .line 6327
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "Calendar"

    const-string v4, "[SW] Could not find ContextProvider"

    invoke-static {v3, v4}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 4201
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 4202
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    monitor-exit p0

    return-object v0

    .line 4201
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public imagesInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 686
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mImagesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public insertAccountLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "accountType"    # Ljava/lang/String;

    .prologue
    .line 6296
    invoke-virtual {p0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getVersionOfContextProviders()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    .line 6314
    :goto_0
    return-void

    .line 6300
    :cond_0
    const-string v4, "content://com.samsung.android.providers.context.log.use_app_feature_survey"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 6303
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v4, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6304
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 6305
    .local v2, "row":Landroid/content/ContentValues;
    const-string v4, "app_id"

    sget-object v5, Lcom/android/providers/calendar/CalendarDatabaseHelper;->APP_ID_FOR_LOGGING:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6306
    const-string v4, "feature"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6307
    const-string v4, "extra"

    invoke-virtual {v2, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6308
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 6309
    const-string v4, "Calendar"

    const-string v5, "ContextProvider insertion operation is performed."

    invoke-static {v4, v5}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 6310
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 6311
    .local v1, "ex":Ljava/lang/Exception;
    const-string v4, "Calendar"

    const-string v5, "Error while using the ContextProvider"

    invoke-static {v4, v5}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 6312
    const-string v4, "Calendar"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public insertCHNEventValues(Landroid/database/DatabaseUtils$InsertHelper;ILandroid/content/ContentValues;Ljava/lang/String;Landroid/text/format/Time;)V
    .locals 14
    .param p1, "eventsInserter"    # Landroid/database/DatabaseUtils$InsertHelper;
    .param p2, "calendarID"    # I
    .param p3, "value"    # Landroid/content/ContentValues;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "startTime"    # Landroid/text/format/Time;

    .prologue
    .line 5807
    const-wide/16 v8, 0x0

    .line 5808
    .local v8, "startMillis":J
    const-wide/16 v6, 0x0

    .line 5810
    .local v6, "endMillis":J
    const/4 v10, 0x1

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v8

    .line 5812
    move-object/from16 v5, p5

    .line 5813
    .local v5, "endTime":Landroid/text/format/Time;
    iget v10, v5, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v10, v10, 0x1

    iput v10, v5, Landroid/text/format/Time;->monthDay:I

    .line 5814
    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 5816
    invoke-virtual/range {p3 .. p3}, Landroid/content/ContentValues;->clear()V

    .line 5818
    const-string v10, "calendar_id"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5819
    const-string v10, "eventTimezone"

    const-string v11, "UTC"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5820
    const-string v10, "title"

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5821
    const-string v10, "allDay"

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5822
    const-string v10, "dtstart"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5823
    const-string v10, "dtend"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5824
    const-string v10, "hasAttendeeData"

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5825
    const-string v10, "description"

    const-string v11, ""

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5826
    const-string v10, "contactEventType"

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5827
    const-string v10, "accessLevel"

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5828
    const-string v10, "hasAlarm"

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5829
    const-string v10, "setLunar"

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5831
    sub-long v10, v6, v8

    const-wide/32 v12, 0x5265c00

    add-long/2addr v10, v12

    const-wide/16 v12, 0x1

    sub-long/2addr v10, v12

    const-wide/32 v12, 0x5265c00

    div-long v2, v10, v12

    .line 5832
    .local v2, "days":J
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "P"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "D"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 5833
    .local v4, "duration":Ljava/lang/String;
    const-string v10, "duration"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5834
    const-string v10, "organizer"

    const-string v11, "local"

    move-object/from16 v0, p3

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5836
    move-object/from16 v0, p3

    invoke-virtual {p1, v0}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    .line 5837
    return-void
.end method

.method insertCHNSolarTerm24(Landroid/database/DatabaseUtils$InsertHelper;Landroid/content/ContentValues;)V
    .locals 13
    .param p1, "eventsInserter"    # Landroid/database/DatabaseUtils$InsertHelper;
    .param p2, "value"    # Landroid/content/ContentValues;

    .prologue
    .line 5851
    new-instance v11, Lcom/android/providers/calendar/lunarRecurrence/SolarLunarTermTablesCHN;

    invoke-direct {v11}, Lcom/android/providers/calendar/lunarRecurrence/SolarLunarTermTablesCHN;-><init>()V

    .line 5853
    .local v11, "solarlunartermtablesCHN":Lcom/android/providers/calendar/lunarRecurrence/SolarLunarTermTablesCHN;
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 5855
    .local v5, "mSolarTermTime":Landroid/text/format/Time;
    const/4 v0, 0x1

    iput-boolean v0, v5, Landroid/text/format/Time;->allDay:Z

    .line 5856
    const/4 v0, 0x0

    iput v0, v5, Landroid/text/format/Time;->hour:I

    .line 5857
    const/4 v0, 0x0

    iput v0, v5, Landroid/text/format/Time;->minute:I

    .line 5858
    const/4 v0, 0x0

    iput v0, v5, Landroid/text/format/Time;->second:I

    .line 5859
    const-string v0, "UTC"

    iput-object v0, v5, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 5861
    const/16 v8, 0x7f4

    .line 5862
    .local v8, "mMaxYear":I
    const/16 v9, 0x7bd

    .line 5863
    .local v9, "mMinYear":I
    const/4 v12, 0x0

    .line 5865
    .local v12, "termlength":I
    const/16 v0, 0x18

    new-array v10, v0, [Ljava/lang/String;

    .line 5867
    .local v10, "solarTermString":[Ljava/lang/String;
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060036

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5868
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5869
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5870
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5871
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5872
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06003b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5873
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06003c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5874
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5875
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06003e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5876
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06003f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5877
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5878
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5879
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060042

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5880
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060043

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5881
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060044

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5882
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060045

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5883
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5884
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060047

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5885
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5886
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060049

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5887
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5888
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5889
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5890
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06004d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 5892
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    const/16 v0, 0x38

    if-ge v6, v0, :cond_1

    .line 5893
    sget-object v0, Lcom/android/providers/calendar/lunarRecurrence/SolarLunarTermTablesCHN;->SolarTerm24:[[[I

    aget-object v0, v0, v6

    array-length v12, v0

    .line 5894
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    if-ge v7, v12, :cond_0

    .line 5895
    sget-object v0, Lcom/android/providers/calendar/lunarRecurrence/SolarLunarTermTablesCHN;->SolarTerm24:[[[I

    aget-object v0, v0, v6

    aget-object v0, v0, v7

    const/4 v1, 0x0

    aget v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    iput v0, v5, Landroid/text/format/Time;->month:I

    .line 5896
    sget-object v0, Lcom/android/providers/calendar/lunarRecurrence/SolarLunarTermTablesCHN;->SolarTerm24:[[[I

    aget-object v0, v0, v6

    aget-object v0, v0, v7

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    .line 5897
    sget-object v0, Lcom/android/providers/calendar/lunarRecurrence/SolarLunarTermTablesCHN;->SolarTerm24:[[[I

    aget-object v0, v0, v6

    aget-object v0, v0, v7

    const/4 v1, 0x2

    aget v0, v0, v1

    iput v0, v5, Landroid/text/format/Time;->year:I

    .line 5899
    const/4 v2, 0x3

    aget-object v4, v10, v7

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->insertCHNEventValues(Landroid/database/DatabaseUtils$InsertHelper;ILandroid/content/ContentValues;Ljava/lang/String;Landroid/text/format/Time;)V

    .line 5894
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 5892
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 5902
    .end local v7    # "j":I
    :cond_1
    return-void
.end method

.method public insertJANEvent(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 13
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "calendar_id"    # I

    .prologue
    .line 5908
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 5910
    .local v5, "japanHolidayTime":Landroid/text/format/Time;
    const/4 v0, 0x1

    iput-boolean v0, v5, Landroid/text/format/Time;->allDay:Z

    .line 5911
    const/4 v0, 0x0

    iput v0, v5, Landroid/text/format/Time;->hour:I

    .line 5912
    const/4 v0, 0x0

    iput v0, v5, Landroid/text/format/Time;->minute:I

    .line 5913
    const/4 v0, 0x0

    iput v0, v5, Landroid/text/format/Time;->second:I

    .line 5914
    const-string v0, "UTC"

    iput-object v0, v5, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 5916
    const/16 v10, 0x7f4

    .line 5917
    .local v10, "mMaxYear":I
    const/16 v11, 0x76e

    .line 5919
    .local v11, "mMinYear":I
    const/16 v0, 0x11

    new-array v9, v0, [Ljava/lang/String;

    .line 5921
    .local v9, "japanHolidayString":[Ljava/lang/String;
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f060082

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5922
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f060083

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5923
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f060084

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5924
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f060085

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5925
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f060086

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5926
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f060087

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5927
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f060088

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5928
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f060089

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5929
    const/16 v0, 0x8

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f06008a

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5930
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f06008b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5931
    const/16 v0, 0xa

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f06008c

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5932
    const/16 v0, 0xb

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f06008d

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5933
    const/16 v0, 0xc

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f06008e

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5934
    const/16 v0, 0xd

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f06008f

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5935
    const/16 v0, 0xe

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f060090

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5936
    const/16 v0, 0xf

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f060091

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5937
    const/16 v0, 0x10

    iget-object v2, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f060092

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    .line 5939
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 5940
    .local v3, "value":Landroid/content/ContentValues;
    new-instance v1, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v0, "Events"

    invoke-direct {v1, p1, v0}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 5942
    .local v1, "eventsInserter":Landroid/database/DatabaseUtils$InsertHelper;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    const/16 v0, 0x86

    if-gt v7, v0, :cond_1

    .line 5943
    sget-object v0, Lcom/android/providers/calendar/JapaneseHolidays/JapanHolidayTables;->HolidayTables:[[[I

    aget-object v0, v0, v7

    array-length v12, v0

    .line 5944
    .local v12, "tablelength":I
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    if-ge v8, v12, :cond_0

    .line 5945
    sget-object v0, Lcom/android/providers/calendar/JapaneseHolidays/JapanHolidayTables;->HolidayTables:[[[I

    aget-object v0, v0, v7

    aget-object v0, v0, v8

    const/4 v2, 0x0

    aget v0, v0, v2

    iput v0, v5, Landroid/text/format/Time;->monthDay:I

    .line 5946
    sget-object v0, Lcom/android/providers/calendar/JapaneseHolidays/JapanHolidayTables;->HolidayTables:[[[I

    aget-object v0, v0, v7

    aget-object v0, v0, v8

    const/4 v2, 0x1

    aget v0, v0, v2

    add-int/lit8 v0, v0, -0x1

    iput v0, v5, Landroid/text/format/Time;->month:I

    .line 5947
    sget-object v0, Lcom/android/providers/calendar/JapaneseHolidays/JapanHolidayTables;->HolidayTables:[[[I

    aget-object v0, v0, v7

    aget-object v0, v0, v8

    const/4 v2, 0x2

    aget v0, v0, v2

    iput v0, v5, Landroid/text/format/Time;->year:I

    .line 5948
    sget-object v0, Lcom/android/providers/calendar/JapaneseHolidays/JapanHolidayTables;->HolidayTables:[[[I

    aget-object v0, v0, v7

    aget-object v0, v0, v8

    const/4 v2, 0x3

    aget v6, v0, v2

    .line 5949
    .local v6, "eventString":I
    aget-object v4, v9, v6

    move-object v0, p0

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->insertJANEventValues(Landroid/database/DatabaseUtils$InsertHelper;ILandroid/content/ContentValues;Ljava/lang/String;Landroid/text/format/Time;)V

    .line 5944
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 5942
    .end local v6    # "eventString":I
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 5953
    .end local v8    # "j":I
    .end local v12    # "tablelength":I
    :cond_1
    invoke-virtual {v1}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 5954
    return-void
.end method

.method public insertJANEventValues(Landroid/database/DatabaseUtils$InsertHelper;ILandroid/content/ContentValues;Ljava/lang/String;Landroid/text/format/Time;)V
    .locals 9
    .param p1, "eventsInserter"    # Landroid/database/DatabaseUtils$InsertHelper;
    .param p2, "calendar_id"    # I
    .param p3, "value"    # Landroid/content/ContentValues;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "startTime"    # Landroid/text/format/Time;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 5956
    const-wide/16 v4, 0x0

    .line 5957
    .local v4, "startMillis":J
    const-wide/16 v0, 0x0

    .line 5958
    .local v0, "endMillis":J
    invoke-virtual {p5, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 5959
    move-object v2, p5

    .line 5960
    .local v2, "endTime":Landroid/text/format/Time;
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Landroid/text/format/Time;->monthDay:I

    .line 5961
    invoke-virtual {v2, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 5962
    invoke-virtual {p3}, Landroid/content/ContentValues;->clear()V

    .line 5964
    const-string v3, "calendar_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p3, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5965
    const-string v3, "title"

    invoke-virtual {p3, v3, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5966
    const-string v3, "selfAttendeeStatus"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p3, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5967
    const-string v3, "dtstart"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p3, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5968
    const-string v3, "dtend"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p3, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5969
    const-string v3, "eventTimezone"

    const-string v6, "UTC"

    invoke-virtual {p3, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5970
    const-string v3, "allDay"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p3, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5971
    const-string v3, "accessLevel"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p3, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5972
    const-string v3, "lastDate"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p3, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5973
    const-string v3, "hasAttendeeData"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p3, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5974
    const-string v3, "organizer"

    const-string v6, ""

    invoke-virtual {p3, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5976
    invoke-virtual {p1, p3}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    .line 5977
    return-void
.end method

.method public insertStickers(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Z
    .locals 25
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "packageId"    # I

    .prologue
    .line 5094
    const/4 v4, 0x0

    .line 5095
    .local v4, "assetManager":Landroid/content/res/AssetManager;
    const/4 v8, 0x0

    .line 5097
    .local v8, "files":[Ljava/lang/String;
    const-string v22, "CalendarDatabaseHelper"

    const-string v23, "CopyAssets"

    invoke-static/range {v22 .. v23}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5099
    if-eqz p2, :cond_4

    .line 5100
    const-string v22, "/"

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 5101
    .local v9, "getFileName":[Ljava/lang/String;
    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v8, v0, [Ljava/lang/String;

    .end local v8    # "files":[Ljava/lang/String;
    const/16 v22, 0x0

    array-length v0, v9

    move/from16 v23, v0

    add-int/lit8 v23, v23, -0x1

    aget-object v23, v9, v23

    aput-object v23, v8, v22

    .line 5112
    .end local v9    # "getFileName":[Ljava/lang/String;
    .restart local v8    # "files":[Ljava/lang/String;
    :goto_0
    const-string v22, "CalendarDatabaseHelper"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "len:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    array-length v0, v8

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5113
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    array-length v0, v8

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v10, v0, :cond_10

    .line 5114
    const/4 v11, 0x0

    .line 5115
    .local v11, "in":Ljava/io/InputStream;
    const/4 v15, 0x0

    .line 5117
    .local v15, "out":Ljava/io/OutputStream;
    const/4 v5, 0x0

    .line 5120
    .local v5, "cursor":Landroid/database/Cursor;
    if-eqz p2, :cond_5

    .line 5121
    :try_start_0
    new-instance v12, Ljava/io/FileInputStream;

    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .end local v11    # "in":Ljava/io/InputStream;
    .local v12, "in":Ljava/io/InputStream;
    move-object v11, v12

    .line 5127
    .end local v12    # "in":Ljava/io/InputStream;
    .restart local v11    # "in":Ljava/io/InputStream;
    :goto_2
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v18

    .line 5129
    .local v18, "str":Ljava/lang/String;
    const-string v22, "mounted"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 5133
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 5134
    .local v3, "appPath":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/sticker/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    .line 5135
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5136
    .local v14, "mpath":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->isDirectory()Z

    move-result v22

    if-nez v22, :cond_0

    .line 5137
    invoke-virtual {v14}, Ljava/io/File;->mkdirs()Z

    .line 5138
    const/16 v22, 0x1

    const/16 v23, 0x0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Ljava/io/File;->setReadable(ZZ)Z

    .line 5139
    const/16 v22, 0x1

    const/16 v23, 0x0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 5142
    :cond_0
    new-instance v16, Ljava/io/FileOutputStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v8, v10

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 5144
    .end local v15    # "out":Ljava/io/OutputStream;
    .local v16, "out":Ljava/io/OutputStream;
    :try_start_1
    aget-object v22, v8, v10

    const-string v23, "\'"

    const-string v24, "\'\'"

    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    const-string v23, "%"

    const-string v24, "`%"

    invoke-virtual/range {v22 .. v24}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v8, v10

    .line 5145
    aget-object v22, v8, v10

    const-string v23, "_"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 5147
    .local v19, "t":[Ljava/lang/String;
    const-string v21, "samsung_preload_activity_bank.png"

    .line 5148
    .local v21, "template":Ljava/lang/String;
    const-string v22, "_"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 5149
    .local v20, "temp":[Ljava/lang/String;
    aget-object v7, v8, v10

    .line 5151
    .local v7, "fileName":Ljava/lang/String;
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v22, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_7

    .line 5152
    const-string v22, "CalendarDatabaseHelper"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "fileName:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", Wrong template format:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5153
    new-instance v22, Ljava/lang/Exception;

    invoke-direct/range {v22 .. v22}, Ljava/lang/Exception;-><init>()V

    throw v22
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_7

    .line 5180
    .end local v7    # "fileName":Ljava/lang/String;
    .end local v19    # "t":[Ljava/lang/String;
    .end local v20    # "temp":[Ljava/lang/String;
    .end local v21    # "template":Ljava/lang/String;
    :catch_0
    move-exception v6

    move-object/from16 v15, v16

    .line 5181
    .end local v3    # "appPath":Ljava/lang/String;
    .end local v14    # "mpath":Ljava/io/File;
    .end local v16    # "out":Ljava/io/OutputStream;
    .end local v18    # "str":Ljava/lang/String;
    .local v6, "e":Ljava/lang/Exception;
    .restart local v15    # "out":Ljava/io/OutputStream;
    :goto_4
    :try_start_2
    const-string v22, "CalendarDatabaseHelper"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Cause:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v6}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " Msg:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 5182
    const/16 v22, 0x0

    .line 5184
    if-eqz v5, :cond_1

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 5186
    :cond_1
    if-eqz v11, :cond_2

    .line 5187
    :try_start_3
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 5192
    :cond_2
    const/4 v11, 0x0

    .line 5196
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_5
    if-eqz v15, :cond_3

    .line 5197
    :try_start_4
    invoke-virtual {v15}, Ljava/io/OutputStream;->flush()V

    .line 5198
    invoke-virtual {v15}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 5203
    :cond_3
    const/4 v15, 0x0

    .line 5208
    .end local v5    # "cursor":Landroid/database/Cursor;
    .end local v10    # "i":I
    .end local v11    # "in":Ljava/io/InputStream;
    .end local v15    # "out":Ljava/io/OutputStream;
    :goto_6
    return v22

    .line 5104
    :cond_4
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    .line 5105
    const-string v22, "stickers"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    move-result-object v8

    goto/16 :goto_0

    .line 5106
    :catch_1
    move-exception v6

    .line 5107
    .local v6, "e":Ljava/io/IOException;
    const-string v22, "CalendarDatabaseHelper"

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5108
    const/16 v22, 0x0

    goto :goto_6

    .line 5123
    .end local v6    # "e":Ljava/io/IOException;
    .restart local v5    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "i":I
    .restart local v11    # "in":Ljava/io/InputStream;
    .restart local v15    # "out":Ljava/io/OutputStream;
    :cond_5
    :try_start_6
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "stickers/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v8, v10

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v11

    goto/16 :goto_2

    .line 5131
    .restart local v18    # "str":Ljava/lang/String;
    :cond_6
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    goto/16 :goto_3

    .line 5180
    .end local v18    # "str":Ljava/lang/String;
    :catch_2
    move-exception v6

    goto/16 :goto_4

    .line 5156
    .end local v15    # "out":Ljava/io/OutputStream;
    .restart local v3    # "appPath":Ljava/lang/String;
    .restart local v7    # "fileName":Ljava/lang/String;
    .restart local v14    # "mpath":Ljava/io/File;
    .restart local v16    # "out":Ljava/io/OutputStream;
    .restart local v18    # "str":Ljava/lang/String;
    .restart local v19    # "t":[Ljava/lang/String;
    .restart local v20    # "temp":[Ljava/lang/String;
    .restart local v21    # "template":Ljava/lang/String;
    :cond_7
    :try_start_7
    const-string v22, "preload"

    const/16 v23, 0x1

    aget-object v23, v19, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_c

    const/4 v13, 0x0

    .line 5163
    .local v13, "isDownload":I
    :goto_7
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SELECT * FROM Stickers WHERE filepath LIKE \'%"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "%\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 5167
    if-eqz v5, :cond_8

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v22

    if-nez v22, :cond_8

    .line 5168
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "INSERT INTO Stickers(sticker_name,filepath,sticker_group,downloaded,packageId ) VALUES (\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x3

    aget-object v23, v19, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\' , \'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\',\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x2

    aget-object v23, v19, v23

    invoke-static/range {v23 .. v23}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getStickerGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\',\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\',\'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\')"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 5174
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v11, v1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 5176
    new-instance v17, Ljava/io/File;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v8, v10

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5177
    .local v17, "readR":Ljava/io/File;
    const/16 v22, 0x1

    const/16 v23, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setReadable(ZZ)Z

    .line 5178
    const/16 v22, 0x1

    const/16 v23, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setExecutable(ZZ)Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    .line 5184
    if-eqz v5, :cond_9

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 5186
    :cond_9
    if-eqz v11, :cond_a

    .line 5187
    :try_start_8
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 5192
    :cond_a
    const/4 v11, 0x0

    .line 5196
    :goto_8
    if-eqz v16, :cond_b

    .line 5197
    :try_start_9
    invoke-virtual/range {v16 .. v16}, Ljava/io/OutputStream;->flush()V

    .line 5198
    invoke-virtual/range {v16 .. v16}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 5203
    :cond_b
    const/4 v15, 0x0

    .line 5113
    .end local v16    # "out":Ljava/io/OutputStream;
    .restart local v15    # "out":Ljava/io/OutputStream;
    :goto_9
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 5156
    .end local v13    # "isDownload":I
    .end local v15    # "out":Ljava/io/OutputStream;
    .end local v17    # "readR":Ljava/io/File;
    .restart local v16    # "out":Ljava/io/OutputStream;
    :cond_c
    const/4 v13, 0x1

    goto/16 :goto_7

    .line 5189
    .restart local v13    # "isDownload":I
    .restart local v17    # "readR":Ljava/io/File;
    :catch_3
    move-exception v6

    .line 5190
    .restart local v6    # "e":Ljava/io/IOException;
    :try_start_a
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 5192
    const/4 v11, 0x0

    .line 5193
    goto :goto_8

    .line 5192
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v22

    const/4 v11, 0x0

    throw v22

    .line 5200
    :catch_4
    move-exception v6

    .line 5201
    .restart local v6    # "e":Ljava/io/IOException;
    :try_start_b
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 5203
    const/4 v15, 0x0

    .line 5204
    .end local v16    # "out":Ljava/io/OutputStream;
    .restart local v15    # "out":Ljava/io/OutputStream;
    goto :goto_9

    .line 5203
    .end local v6    # "e":Ljava/io/IOException;
    .end local v15    # "out":Ljava/io/OutputStream;
    .restart local v16    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v22

    const/4 v15, 0x0

    .end local v16    # "out":Ljava/io/OutputStream;
    .restart local v15    # "out":Ljava/io/OutputStream;
    throw v22

    .line 5189
    .end local v3    # "appPath":Ljava/lang/String;
    .end local v7    # "fileName":Ljava/lang/String;
    .end local v13    # "isDownload":I
    .end local v14    # "mpath":Ljava/io/File;
    .end local v17    # "readR":Ljava/io/File;
    .end local v18    # "str":Ljava/lang/String;
    .end local v19    # "t":[Ljava/lang/String;
    .end local v20    # "temp":[Ljava/lang/String;
    .end local v21    # "template":Ljava/lang/String;
    .local v6, "e":Ljava/lang/Exception;
    :catch_5
    move-exception v6

    .line 5190
    .local v6, "e":Ljava/io/IOException;
    :try_start_c
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 5192
    const/4 v11, 0x0

    .line 5193
    goto/16 :goto_5

    .line 5192
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_2
    move-exception v22

    const/4 v11, 0x0

    throw v22

    .line 5200
    :catch_6
    move-exception v6

    .line 5201
    .restart local v6    # "e":Ljava/io/IOException;
    :try_start_d
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 5203
    const/4 v15, 0x0

    .line 5204
    goto/16 :goto_6

    .line 5203
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_3
    move-exception v22

    const/4 v15, 0x0

    throw v22

    .line 5184
    :catchall_4
    move-exception v22

    :goto_a
    if-eqz v5, :cond_d

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 5186
    :cond_d
    if-eqz v11, :cond_e

    .line 5187
    :try_start_e
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    .line 5192
    :cond_e
    const/4 v11, 0x0

    .line 5196
    :goto_b
    if-eqz v15, :cond_f

    .line 5197
    :try_start_f
    invoke-virtual {v15}, Ljava/io/OutputStream;->flush()V

    .line 5198
    invoke-virtual {v15}, Ljava/io/OutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    .line 5203
    :cond_f
    const/4 v15, 0x0

    :goto_c
    throw v22

    .line 5189
    :catch_7
    move-exception v6

    .line 5190
    .restart local v6    # "e":Ljava/io/IOException;
    :try_start_10
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    .line 5192
    const/4 v11, 0x0

    .line 5193
    goto :goto_b

    .line 5192
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_5
    move-exception v22

    const/4 v11, 0x0

    throw v22

    .line 5200
    :catch_8
    move-exception v6

    .line 5201
    .restart local v6    # "e":Ljava/io/IOException;
    :try_start_11
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    .line 5203
    const/4 v15, 0x0

    .line 5204
    goto :goto_c

    .line 5203
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_6
    move-exception v22

    const/4 v15, 0x0

    throw v22

    .line 5208
    .end local v5    # "cursor":Landroid/database/Cursor;
    .end local v11    # "in":Ljava/io/InputStream;
    .end local v15    # "out":Ljava/io/OutputStream;
    :cond_10
    const/16 v22, 0x1

    goto/16 :goto_6

    .line 5184
    .restart local v3    # "appPath":Ljava/lang/String;
    .restart local v5    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "in":Ljava/io/InputStream;
    .restart local v14    # "mpath":Ljava/io/File;
    .restart local v16    # "out":Ljava/io/OutputStream;
    .restart local v18    # "str":Ljava/lang/String;
    :catchall_7
    move-exception v22

    move-object/from16 v15, v16

    .end local v16    # "out":Ljava/io/OutputStream;
    .restart local v15    # "out":Ljava/io/OutputStream;
    goto :goto_a
.end method

.method public instancesInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 649
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mInstancesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public instancesReplace(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 653
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mInstancesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->replace(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method isTableExists(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 6115
    const-string v4, "CalendarDatabaseHelper"

    const-string v5, "isTableExists"

    invoke-static {v4, v5}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6116
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v4

    if-nez v4, :cond_1

    .line 6129
    :cond_0
    :goto_0
    return v3

    .line 6120
    :cond_1
    const-string v4, "SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "table"

    aput-object v6, v5, v3

    aput-object p2, v5, v2

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 6121
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_2

    .line 6123
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 6126
    :cond_2
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 6127
    .local v0, "count":I
    const-string v4, "CalendarDatabaseHelper"

    const-string v5, "isTableExists count"

    invoke-static {v4, v5}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6128
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 6129
    if-lez v0, :cond_3

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method public mapInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 695
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mMapInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method markDontNeedToInsertTask()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6151
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    const-string v2, "CalendarDatabaseHelper"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 6152
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "needInsertTask"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 6153
    return-void
.end method

.method markNeedToInsertTask()V
    .locals 4

    .prologue
    .line 6146
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    const-string v2, "CalendarDatabaseHelper"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 6147
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "needInsertTask"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 6148
    return-void
.end method

.method public memosInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 683
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mMemosInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public notesInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 689
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mNotesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 804
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->bootstrapDB(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 805
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 2143
    const-string v0, "CalendarDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t downgrade DB from version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2144
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2145
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->bootstrapDB(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2146
    return-void
.end method

.method public onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 732
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mSyncState:Lcom/android/common/content/SyncStateContentProviderHelper;

    invoke-virtual {v0, p1}, Lcom/android/common/content/SyncStateContentProviderHelper;->onDatabaseOpened(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 734
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Calendars"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mCalendarsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 735
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Colors"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mColorsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 736
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Events"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mEventsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 737
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "EventsRawTimes"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mEventsRawTimesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 738
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Instances"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mInstancesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 739
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Attendees"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mAttendeesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 740
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Reminders"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mRemindersInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 741
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "CalendarAlerts"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mCalendarAlertsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 742
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "ExtendedProperties"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mExtendedPropertiesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 745
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "QSMemos"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mQSMemosInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 746
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Memos"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mMemosInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 747
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Images"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mImagesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 748
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Notes"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mNotesInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 749
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Facebooks"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mFacebooksInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 750
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Maps"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mMapInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 751
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Tasks"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTasksInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 752
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "TasksReminders"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTasksReminderInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 753
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "TasksAccounts"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTasksAccountsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 754
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "TaskGroup"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mTaskGroupInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 756
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Stickers"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mStickersInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 758
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 26
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 1722
    const-string v20, "CalendarDatabaseHelper"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Upgrading DB from version "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " to "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1723
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v18

    .line 1725
    .local v18, "startWhen":J
    const/16 v20, 0x31

    move/from16 v0, p2

    move/from16 v1, v20

    if-ge v0, v1, :cond_0

    .line 1726
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1727
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->bootstrapDB(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2139
    :goto_0
    return-void

    .line 1737
    :cond_0
    const/16 v20, 0x3b

    move/from16 v0, p2

    move/from16 v1, v20

    if-lt v0, v1, :cond_48

    const/16 v20, 0x42

    move/from16 v0, p2

    move/from16 v1, v20

    if-gt v0, v1, :cond_48

    const/16 v17, 0x1

    .line 1738
    .local v17, "recreateMetaDataAndInstances":Z
    :goto_1
    const/4 v5, 0x0

    .line 1740
    .local v5, "createEventsView":Z
    const/4 v6, 0x0

    .line 1744
    .local v6, "createTasksView":Z
    const/16 v20, 0x33

    move/from16 v0, p2

    move/from16 v1, v20

    if-ge v0, v1, :cond_1

    .line 1745
    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion51(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1746
    const/16 p2, 0x33

    .line 1748
    :cond_1
    const/16 v20, 0x33

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 1749
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion52(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1750
    add-int/lit8 p2, p2, 0x1

    .line 1752
    :cond_2
    const/16 v20, 0x34

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 1753
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion53(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1754
    add-int/lit8 p2, p2, 0x1

    .line 1756
    :cond_3
    const/16 v20, 0x35

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 1757
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion54(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1758
    add-int/lit8 p2, p2, 0x1

    .line 1760
    :cond_4
    const/16 v20, 0x36

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    .line 1761
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion55(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1762
    add-int/lit8 p2, p2, 0x1

    .line 1764
    :cond_5
    const/16 v20, 0x37

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_6

    const/16 v20, 0x38

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 1766
    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeResync(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1768
    :cond_7
    const/16 v20, 0x37

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_8

    .line 1769
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion56(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1770
    add-int/lit8 p2, p2, 0x1

    .line 1772
    :cond_8
    const/16 v20, 0x38

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_9

    .line 1773
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion57(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1774
    add-int/lit8 p2, p2, 0x1

    .line 1776
    :cond_9
    const/16 v20, 0x39

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_a

    .line 1778
    add-int/lit8 p2, p2, 0x1

    .line 1780
    :cond_a
    const/16 v20, 0x3a

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    .line 1781
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion59(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1782
    add-int/lit8 p2, p2, 0x1

    .line 1784
    :cond_b
    const/16 v20, 0x3b

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_c

    .line 1785
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion60(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1786
    const/4 v5, 0x1

    .line 1787
    add-int/lit8 p2, p2, 0x1

    .line 1789
    :cond_c
    const/16 v20, 0x3c

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_d

    .line 1790
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion61(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1791
    add-int/lit8 p2, p2, 0x1

    .line 1793
    :cond_d
    const/16 v20, 0x3d

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_e

    .line 1794
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion62(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1795
    add-int/lit8 p2, p2, 0x1

    .line 1797
    :cond_e
    const/16 v20, 0x3e

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_f

    .line 1798
    const/4 v5, 0x1

    .line 1799
    add-int/lit8 p2, p2, 0x1

    .line 1801
    :cond_f
    const/16 v20, 0x3f

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_10

    .line 1802
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion64(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1803
    add-int/lit8 p2, p2, 0x1

    .line 1805
    :cond_10
    const/16 v20, 0x40

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_11

    .line 1806
    const/4 v5, 0x1

    .line 1807
    add-int/lit8 p2, p2, 0x1

    .line 1809
    :cond_11
    const/16 v20, 0x41

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_12

    .line 1810
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion66(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1811
    add-int/lit8 p2, p2, 0x1

    .line 1813
    :cond_12
    const/16 v20, 0x42

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_13

    .line 1815
    add-int/lit8 p2, p2, 0x1

    .line 1817
    :cond_13
    if-eqz v17, :cond_14

    .line 1818
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->recreateMetaDataAndInstances67(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1820
    :cond_14
    const/16 v20, 0x43

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_15

    const/16 v20, 0x44

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_16

    .line 1821
    :cond_15
    invoke-static/range {p1 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion69(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1822
    const/16 p2, 0x45

    .line 1828
    :cond_16
    const/16 v20, 0x45

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_17

    .line 1829
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion200(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1830
    const/4 v5, 0x1

    .line 1831
    const/16 p2, 0xc8

    .line 1833
    :cond_17
    const/16 v20, 0x46

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_18

    .line 1834
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion200(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1835
    const/16 p2, 0xc8

    .line 1837
    :cond_18
    const/16 v20, 0x64

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_19

    .line 1839
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion200(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1840
    const/16 p2, 0xc8

    .line 1842
    :cond_19
    const/4 v10, 0x1

    .line 1844
    .local v10, "need203Update":Z
    const/16 v16, 0x1

    .line 1845
    .local v16, "needCreatingTasks":Z
    const/4 v15, 0x1

    .line 1846
    .local v15, "needCreatingQSMemos":Z
    const/4 v14, 0x1

    .line 1847
    .local v14, "needCreatingMemos":Z
    const/4 v12, 0x1

    .line 1848
    .local v12, "needCreatingImages":Z
    const/4 v13, 0x1

    .line 1849
    .local v13, "needCreatingMaps":Z
    const/4 v11, 0x1

    .line 1851
    .local v11, "needCreatingFacebooks":Z
    const/16 v20, 0x65

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_1a

    const/16 v20, 0x66

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_1a

    const/16 v20, 0x67

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_1a

    const/16 v20, 0x68

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_1a

    const/16 v20, 0x69

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_1a

    const/16 v20, 0x6a

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_21

    .line 1861
    :cond_1a
    const/16 v20, 0x65

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_1b

    const/16 v20, 0x66

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_1b

    const/16 v20, 0x67

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_1c

    .line 1862
    :cond_1b
    invoke-virtual/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion104(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1863
    const/16 p2, 0x68

    .line 1865
    :cond_1c
    const/16 v20, 0x68

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_1d

    .line 1866
    invoke-virtual/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion105(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1867
    const/16 p2, 0x69

    .line 1869
    :cond_1d
    const/16 v20, 0x69

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_1f

    .line 1874
    const/4 v13, 0x0

    .line 1875
    const/4 v11, 0x0

    .line 1879
    const/4 v4, 0x0

    .line 1881
    .local v4, "c":Landroid/database/Cursor;
    :try_start_1
    const-string v20, "SELECT name FROM SQLITE_MASTER WHERE type=\'table\' and name=\'Tasks\'"

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1882
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v20

    if-lez v20, :cond_1e

    .line 1883
    const/16 p2, 0x6a

    .line 1886
    :cond_1e
    if-eqz v4, :cond_1f

    .line 1887
    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1892
    .end local v4    # "c":Landroid/database/Cursor;
    :cond_1f
    const/16 v20, 0x6a

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_20

    .line 1894
    const/16 v16, 0x0

    .line 1895
    const/4 v15, 0x0

    .line 1896
    const/4 v14, 0x0

    .line 1897
    const/4 v12, 0x0

    .line 1898
    const/4 v13, 0x0

    .line 1899
    const/4 v11, 0x0

    .line 1901
    const-string v20, "ALTER TABLE Images RENAME TO Images_Backup;"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1902
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createImagesTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1903
    const-string v20, "INSERT INTO Images (_id, event_id, filepath, event_type) SELECT _id, event_id, image_path, event_type FROM Images_Backup;"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1914
    const-string v20, "DROP TABLE Images_Backup;"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1918
    :cond_20
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion200(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1919
    const/16 p2, 0xc8

    .line 1920
    const/4 v10, 0x0

    .line 1922
    :cond_21
    const/16 v20, 0xc8

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_22

    .line 1923
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion201(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1924
    add-int/lit8 p2, p2, 0x1

    .line 1926
    :cond_22
    const/16 v20, 0xc9

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_23

    .line 1927
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion202(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1928
    const/4 v5, 0x1

    .line 1929
    add-int/lit8 p2, p2, 0x1

    .line 1931
    :cond_23
    const/16 v20, 0xca

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_25

    .line 1932
    if-eqz v10, :cond_24

    .line 1933
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion203(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1935
    :cond_24
    add-int/lit8 p2, p2, 0x1

    .line 1937
    :cond_25
    const/16 v20, 0xcb

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_26

    .line 1938
    const/4 v5, 0x1

    .line 1939
    add-int/lit8 p2, p2, 0x1

    .line 1941
    :cond_26
    const/16 v20, 0xce

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_27

    .line 1944
    add-int/lit8 p2, p2, -0x2

    .line 1946
    :cond_27
    const/16 v20, 0xcc

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_28

    .line 1948
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion205(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1949
    const/4 v5, 0x1

    .line 1950
    add-int/lit8 p2, p2, 0x1

    .line 1952
    :cond_28
    const/16 v20, 0xcd

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_29

    const/16 v20, 0xd1

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_2b

    .line 1958
    :cond_29
    const/16 v20, 0xd1

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_2a

    .line 1959
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion205(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1960
    const/4 v13, 0x0

    .line 1961
    const/4 v11, 0x0

    .line 1964
    :cond_2a
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion300(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1965
    const/4 v5, 0x1

    .line 1966
    const/16 p2, 0x12c

    .line 1968
    :cond_2b
    const/16 v20, 0x12c

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_2c

    .line 1969
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion301(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1970
    const/4 v5, 0x1

    .line 1971
    add-int/lit8 p2, p2, 0x1

    .line 1973
    :cond_2c
    const/16 v20, 0x12d

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_2d

    .line 1974
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion302(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1975
    add-int/lit8 p2, p2, 0x1

    .line 1977
    :cond_2d
    const/16 v20, 0x12e

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_2e

    .line 1978
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion303(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1979
    add-int/lit8 p2, p2, 0x1

    .line 1980
    const/4 v5, 0x1

    .line 1982
    :cond_2e
    const/16 v20, 0x12f

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_2f

    .line 1983
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion304(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1984
    add-int/lit8 p2, p2, 0x1

    .line 1985
    const/4 v5, 0x1

    .line 1987
    :cond_2f
    const/16 v20, 0x130

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_30

    .line 1988
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion305(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1989
    add-int/lit8 p2, p2, 0x1

    .line 1990
    const/4 v5, 0x1

    .line 1992
    :cond_30
    const/16 v20, 0x131

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_31

    .line 1993
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion306(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1995
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->scheduleSync(Landroid/accounts/Account;ZLjava/lang/String;)V

    .line 1996
    add-int/lit8 p2, p2, 0x1

    .line 1998
    :cond_31
    const/16 v20, 0x132

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_32

    .line 1999
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion307(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2000
    add-int/lit8 p2, p2, 0x1

    .line 2002
    :cond_32
    const/16 v20, 0x133

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_39

    .line 2005
    if-eqz v16, :cond_33

    .line 2008
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createOriginalTasksTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2009
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createOriginalTasksRemindersTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2010
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createOriginalTasksAccountsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2011
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createOriginalGroupTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2012
    invoke-virtual/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createMyTask(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2014
    :cond_33
    if-eqz v15, :cond_34

    .line 2015
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createQSMemosTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2017
    :cond_34
    if-eqz v14, :cond_35

    .line 2018
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createMemosTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2020
    :cond_35
    if-eqz v12, :cond_36

    .line 2021
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createImagesTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2023
    :cond_36
    if-eqz v13, :cond_37

    .line 2024
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createMapsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2026
    :cond_37
    if-eqz v11, :cond_38

    .line 2027
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createFacebooksTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2031
    :cond_38
    invoke-virtual/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion308_SEC(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2032
    const/4 v6, 0x1

    .line 2034
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion308(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2035
    add-int/lit8 p2, p2, 0x1

    .line 2036
    const/4 v5, 0x1

    .line 2038
    :cond_39
    const/16 v20, 0x134

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_3b

    .line 2039
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion400(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2042
    const-string v20, "Notes"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->isTableExists(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_3a

    .line 2043
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createNotesTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2044
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createNoteField:Z

    .line 2047
    :cond_3a
    const/4 v5, 0x1

    .line 2048
    const/16 p2, 0x190

    .line 2051
    :cond_3b
    const/16 v20, 0x135

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_3c

    const/16 v20, 0x190

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_3d

    .line 2052
    :cond_3c
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion401(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2053
    const/4 v5, 0x1

    .line 2054
    const/16 p2, 0x191

    .line 2056
    :cond_3d
    const/16 v20, 0x191

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_3e

    .line 2057
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion402(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2058
    const/4 v5, 0x1

    .line 2059
    const/16 p2, 0x192

    .line 2061
    :cond_3e
    const/16 v20, 0x192

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_3f

    .line 2062
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion403(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2063
    const/4 v6, 0x1

    .line 2064
    const/4 v5, 0x1

    .line 2065
    const/16 p2, 0x193

    .line 2067
    :cond_3f
    const/16 v20, 0x193

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_40

    .line 2068
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion404(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2069
    const/16 p2, 0x194

    .line 2071
    :cond_40
    const/16 v20, 0x194

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_41

    .line 2072
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion501(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2073
    const/4 v5, 0x1

    .line 2074
    const/16 p2, 0x1f5

    .line 2076
    :cond_41
    const/16 v20, 0x1f5

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_42

    .line 2077
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion502(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2078
    const/4 v5, 0x1

    .line 2079
    const/16 p2, 0x1f6

    .line 2082
    :cond_42
    const/16 v20, 0x1f6

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_43

    .line 2083
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion503(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2084
    const/4 v5, 0x1

    .line 2085
    const/4 v6, 0x1

    .line 2086
    const/16 p2, 0x1f7

    .line 2089
    :cond_43
    const/16 v20, 0x258

    move/from16 v0, p2

    move/from16 v1, v20

    if-ge v0, v1, :cond_44

    .line 2090
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->upgradeToVersion600(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2091
    const/4 v5, 0x1

    .line 2092
    const/16 p2, 0x258

    .line 2095
    :cond_44
    const-string v20, "Tasks"

    const-string v21, "glance_deleted"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->checkTableField(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_45

    .line 2096
    const-string v20, "ALTER TABLE Tasks ADD COLUMN glance_deleted INTEGER DEFAULT 0;"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2099
    :cond_45
    if-eqz v6, :cond_46

    .line 2100
    invoke-virtual/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createTasksView(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2104
    :cond_46
    if-eqz v5, :cond_47

    .line 2105
    invoke-static/range {p1 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createEventsView(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2108
    :cond_47
    const/16 v20, 0x258

    move/from16 v0, p2

    move/from16 v1, v20

    if-eq v0, v1, :cond_4a

    .line 2109
    const-string v20, "CalendarDatabaseHelper"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Need to recreate Calendar schema because of unknown Calendar database version: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2111
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2112
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->bootstrapDB(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2113
    const/16 p2, 0x258

    .line 2114
    invoke-virtual/range {p0 .. p0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->markDontNeedToInsertTask()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2130
    :goto_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .line 2131
    .local v8, "endWhen":J
    const-string v20, "CalendarDatabaseHelper"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Calendar upgrade took "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sub-long v22, v8, v18

    const-wide/32 v24, 0xf4240

    div-long v22, v22, v24

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "ms"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1737
    .end local v5    # "createEventsView":Z
    .end local v6    # "createTasksView":Z
    .end local v8    # "endWhen":J
    .end local v10    # "need203Update":Z
    .end local v11    # "needCreatingFacebooks":Z
    .end local v12    # "needCreatingImages":Z
    .end local v13    # "needCreatingMaps":Z
    .end local v14    # "needCreatingMemos":Z
    .end local v15    # "needCreatingQSMemos":Z
    .end local v16    # "needCreatingTasks":Z
    .end local v17    # "recreateMetaDataAndInstances":Z
    :cond_48
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 1886
    .restart local v4    # "c":Landroid/database/Cursor;
    .restart local v5    # "createEventsView":Z
    .restart local v6    # "createTasksView":Z
    .restart local v10    # "need203Update":Z
    .restart local v11    # "needCreatingFacebooks":Z
    .restart local v12    # "needCreatingImages":Z
    .restart local v13    # "needCreatingMaps":Z
    .restart local v14    # "needCreatingMemos":Z
    .restart local v15    # "needCreatingQSMemos":Z
    .restart local v16    # "needCreatingTasks":Z
    .restart local v17    # "recreateMetaDataAndInstances":Z
    :catchall_0
    move-exception v20

    if-eqz v4, :cond_49

    .line 1887
    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_49
    throw v20
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 2118
    .end local v4    # "c":Landroid/database/Cursor;
    .end local v10    # "need203Update":Z
    .end local v11    # "needCreatingFacebooks":Z
    .end local v12    # "needCreatingImages":Z
    .end local v13    # "needCreatingMaps":Z
    .end local v14    # "needCreatingMemos":Z
    .end local v15    # "needCreatingQSMemos":Z
    .end local v16    # "needCreatingTasks":Z
    :catch_0
    move-exception v7

    .line 2119
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mInTestMode:Z

    move/from16 v20, v0

    if-eqz v20, :cond_4b

    .line 2121
    throw v7

    .line 2116
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v10    # "need203Update":Z
    .restart local v11    # "needCreatingFacebooks":Z
    .restart local v12    # "needCreatingImages":Z
    .restart local v13    # "needCreatingMaps":Z
    .restart local v14    # "needCreatingMemos":Z
    .restart local v15    # "needCreatingQSMemos":Z
    .restart local v16    # "needCreatingTasks":Z
    :cond_4a
    :try_start_4
    invoke-static/range {p1 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->removeOrphans(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 2123
    .end local v10    # "need203Update":Z
    .end local v11    # "needCreatingFacebooks":Z
    .end local v12    # "needCreatingImages":Z
    .end local v13    # "needCreatingMaps":Z
    .end local v14    # "needCreatingMemos":Z
    .end local v15    # "needCreatingQSMemos":Z
    .end local v16    # "needCreatingTasks":Z
    .restart local v7    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_4b
    const-string v20, "CalendarDatabaseHelper"

    const-string v21, "onUpgrade: SQLiteException, recreating db. "

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v7}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2124
    const-string v20, "CalendarDatabaseHelper"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "(oldVersion was "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2125
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2126
    invoke-direct/range {p0 .. p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->bootstrapDB(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0
.end method

.method public qsmemosInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 680
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mQSMemosInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public remindersInsert(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 661
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mRemindersInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v0, p1}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method protected removeDuplicateEvent(J)V
    .locals 9
    .param p1, "id"    # J

    .prologue
    const/4 v5, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 4943
    invoke-virtual {p0}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 4944
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v4, "SELECT _id FROM Events WHERE _sync_id = (SELECT _sync_id FROM Events WHERE _id = ?) AND lastSynced = ?"

    new-array v5, v5, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    const-string v6, "1"

    aput-object v6, v5, v8

    invoke-virtual {v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 4956
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 4957
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 4959
    .local v2, "dupId":J
    const-string v4, "CalendarDatabaseHelper"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4960
    const-string v4, "CalendarDatabaseHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Removing duplicate event "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " of original event "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/providers/calendar/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4963
    :cond_0
    const-string v4, "DELETE FROM Events WHERE _id = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4966
    .end local v2    # "dupId":J
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 4968
    return-void

    .line 4966
    :catchall_0
    move-exception v4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v4
.end method

.method public removeStickers()V
    .locals 10

    .prologue
    .line 5298
    iget-object v7, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iget-object v0, v7, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 5299
    .local v0, "appPath":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/sticker/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    .line 5301
    new-instance v1, Ljava/io/File;

    iget-object v7, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5303
    .local v1, "dir":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v6

    .line 5304
    .local v6, "old_stickers":[Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 5305
    const-string v7, "CalendarDatabaseHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "old_stickers:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, v6

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5306
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v7, v6

    if-ge v5, v7, :cond_1

    .line 5307
    aget-object v4, v6, v5

    .line 5308
    .local v4, "filename":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->STICKERPATH:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5310
    .local v3, "f":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 5311
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5306
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 5315
    .end local v3    # "f":Ljava/io/File;
    .end local v4    # "filename":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v6    # "old_stickers":[Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 5316
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "CalendarDatabaseHelper"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cause:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Msg:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5318
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    return-void
.end method

.method restoreMyTasks()V
    .locals 18

    .prologue
    .line 6009
    const-string v5, "CalendarDatabaseHelper"

    const-string v6, "restoreMyTasks"

    invoke-static {v5, v6}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6011
    const/16 v5, 0x13

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "_sync_dirty"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "clientId"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "sourceid"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "bodyType"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "body_size"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "body_truncated"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "due_date"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "utc_due_date"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string v6, "importance"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "date_completed"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    const-string v6, "complete"

    aput-object v6, v4, v5

    const/16 v5, 0xc

    const-string v6, "reminder_set"

    aput-object v6, v4, v5

    const/16 v5, 0xd

    const-string v6, "reminder_time"

    aput-object v6, v4, v5

    const/16 v5, 0xe

    const-string v6, "subject"

    aput-object v6, v4, v5

    const/16 v5, 0xf

    const-string v6, "body"

    aput-object v6, v4, v5

    const/16 v5, 0x10

    const-string v6, "accountKey"

    aput-object v6, v4, v5

    const/16 v5, 0x11

    const-string v6, "accountName"

    aput-object v6, v4, v5

    const/16 v5, 0x12

    const-string v6, "reminder_type"

    aput-object v6, v4, v5

    .line 6053
    .local v4, "TASKS_PROJECTION":[Ljava/lang/String;
    const-string v8, "content://tasks/tasks"

    .line 6054
    .local v8, "TASKS_CONTENT_URI":Ljava/lang/String;
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 6055
    .local v3, "mUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 6056
    .local v2, "cr":Landroid/content/ContentResolver;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 6057
    .local v13, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v15, 0x0

    .line 6059
    .local v15, "tasksCursor":Landroid/database/Cursor;
    :try_start_0
    const-string v5, "accountName=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v17, "My task"

    aput-object v17, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 6060
    const-string v5, "CalendarDatabaseHelper"

    const-string v6, "restoreMyTasks tasksCursor done"

    invoke-static {v5, v6}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6066
    :goto_0
    if-nez v15, :cond_0

    .line 6067
    const-string v5, "CalendarDatabaseHelper"

    const-string v6, "tasksCursor is null"

    invoke-static {v5, v6}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6108
    :goto_1
    new-instance v10, Landroid/os/Handler;

    invoke-direct {v10}, Landroid/os/Handler;-><init>()V

    .line 6109
    .local v10, "handler":Landroid/os/Handler;
    new-instance v14, Lcom/android/providers/calendar/CalendarDatabaseHelper$InsertRunnable;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v13}, Lcom/android/providers/calendar/CalendarDatabaseHelper$InsertRunnable;-><init>(Lcom/android/providers/calendar/CalendarDatabaseHelper;Ljava/util/ArrayList;)V

    .line 6110
    .local v14, "runnable":Lcom/android/providers/calendar/CalendarDatabaseHelper$InsertRunnable;
    const-wide/16 v6, 0xc8

    invoke-virtual {v10, v14, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 6111
    return-void

    .line 6061
    .end local v10    # "handler":Landroid/os/Handler;
    .end local v14    # "runnable":Lcom/android/providers/calendar/CalendarDatabaseHelper$InsertRunnable;
    :catch_0
    move-exception v12

    .line 6062
    .local v12, "ie":Ljava/lang/IllegalArgumentException;
    const-string v5, "CalendarDatabaseHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown URL Content "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6069
    .end local v12    # "ie":Ljava/lang/IllegalArgumentException;
    :cond_0
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-nez v5, :cond_1

    .line 6070
    const-string v5, "CalendarDatabaseHelper"

    const-string v6, "tasksCursor.getCount() is zero"

    invoke-static {v5, v6}, Lcom/android/providers/calendar/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 6071
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 6074
    :cond_1
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 6075
    .local v16, "values":Landroid/content/ContentValues;
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    .line 6076
    const/4 v11, 0x0

    .line 6078
    .local v11, "id":I
    :cond_2
    invoke-virtual/range {v16 .. v16}, Landroid/content/ContentValues;->clear()V

    .line 6079
    const-string v5, "_id"

    add-int/lit8 v11, v11, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6080
    const-string v5, "_sync_dirty"

    const-string v6, "_sync_dirty"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6081
    const-string v5, "clientId"

    const-string v6, "clientId"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6082
    const-string v5, "sourceid"

    const-string v6, "sourceid"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6083
    const-string v5, "bodyType"

    const-string v6, "bodyType"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6084
    const-string v5, "body_size"

    const-string v6, "body_size"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6085
    const-string v5, "body_truncated"

    const-string v6, "body_truncated"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6086
    const-string v5, "due_date"

    const-string v6, "due_date"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 6087
    const-string v5, "utc_due_date"

    const-string v6, "utc_due_date"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 6088
    const-string v5, "importance"

    const-string v6, "importance"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6089
    const-string v5, "date_completed"

    const-string v6, "date_completed"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6090
    const-string v5, "complete"

    const-string v6, "complete"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6091
    const-string v5, "reminder_set"

    const-string v6, "reminder_set"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6092
    const-string v5, "reminder_time"

    const-string v6, "reminder_time"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6093
    const-string v5, "subject"

    const-string v6, "subject"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6094
    const-string v6, "body"

    const-string v5, "body"

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    const-string v5, ""

    :goto_2
    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6095
    const-string v5, "accountKey"

    const-string v6, "accountKey"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6096
    const-string v5, "accountName"

    const-string v6, "accountName"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6097
    const-string v5, "reminder_type"

    const-string v6, "reminder_type"

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6098
    const-string v5, "groupId"

    const-string v6, "1"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6099
    const-string v5, "previousId"

    add-int/lit8 v6, v11, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6101
    const-string v5, "content://com.android.calendar/syncTasks"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    .line 6102
    .local v9, "b":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6103
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 6104
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 6094
    .end local v9    # "b":Landroid/content/ContentProviderOperation$Builder;
    :cond_3
    const-string v5, "body"

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2
.end method

.method scheduleSync(Landroid/accounts/Account;ZLjava/lang/String;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "uploadChangesOnly"    # Z
    .param p3, "url"    # Ljava/lang/String;

    .prologue
    .line 4219
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4220
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz p2, :cond_0

    .line 4221
    const-string v1, "upload"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 4223
    :cond_0
    if-eqz p3, :cond_1

    .line 4224
    const-string v1, "feed"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4226
    :cond_1
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 4228
    return-void
.end method

.method public updateSubstituteHolidays(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 5443
    move v0, p2

    .line 5444
    .local v0, "version":I
    if-nez v0, :cond_0

    .line 5445
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->cleanupSubstHolidays(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 5446
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createSubstituteHolidayTable(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    .line 5448
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->cleanupInstancesTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 5449
    return-void
.end method

.method upgradeToVersion104(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3562
    const-string v0, "ALTER TABLE Events ADD COLUMN availabilityStatus INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3563
    return-void
.end method

.method upgradeToVersion105(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3557
    const-string v0, "CREATE INDEX contactIdIndex ON Events (contact_data_id, contact_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3558
    return-void
.end method

.method upgradeToVersion308_SEC(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 4739
    const-string v0, "ALTER TABLE TaskGroup ADD COLUMN group_order INTEGER DEFAULT 0;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4742
    const-string v0, "ALTER TABLE Events ADD COLUMN phone_number TEXT;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4745
    invoke-direct {p0, p1}, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createNotesTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4746
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/providers/calendar/CalendarDatabaseHelper;->createNoteField:Z

    .line 4747
    return-void
.end method
