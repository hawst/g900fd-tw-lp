.class public Lcom/android/providers/calendar/DemoUpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DemoUpdateReceiver.java"


# static fields
.field private static final DEMO_EVENTS_DATA:[Ljava/lang/String;

.field private static final DEMO_TASKS_DATA:[Ljava/lang/String;

.field private static final SYNC_TASKS_CONTENT_URI:Landroid/net/Uri;


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 20
    const-string v0, "content://com.android.calendar/syncTasks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/calendar/DemoUpdateReceiver;->SYNC_TASKS_CONTENT_URI:Landroid/net/Uri;

    .line 24
    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Repair shop - Note PC,20130501T130000,20130501T140000,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=1WE,0"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "Task report,20130502T100000,20130502T110000,Asia/Seoul,WEEKLY,INTERVAL=1;BYDAY=TH,0"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Happy hour at SOHO club,20130503T160000,20130503T170000,Asia/Seoul,WEEKLY,INTERVAL=2;BYDAY=FR,0"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Serrah\'s birthday party,20130503T200000,20130503T210000,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=1FR,50"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Brunch at Grand Hotel,20130504T100000,20130504T110000,Asia/Seoul,WEEKLY,INTERVAL=2;BYDAY=SA,73"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Shopping - Depart,20130505T140000,20130505T150000,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=1SU,21"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Dinner with family,20130505T180000,20130505T190000,Asia/Seoul,WEEKLY,INTERVAL=1;BYDAY=1SU,0"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Weekly meeting,20130429T090000,20130429T100000,Asia/Seoul,WEEKLY,INTERVAL=1;BYDAY=MO,117"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Membership training,20130506,20130509,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=1MO,9"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Cooking class,20130508T190000,20130508T200000,Asia/Seoul,WEEKLY,INTERVAL=2;BYDAY=WE,66"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Party with roommate,20130510T220000,20130510T230000,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=2FR,113"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Golf with friends,20130512,20130513,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=2SU,109"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Dinner with family,20130512T190000,20130512T200000,Asia/Seoul,WEEKLY,INTERVAL=2;BYDAY=SU,0"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Mike\'s birthday,20130514,20130515,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=2TU,50"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Dental appointment,20130515T150000,20130515T160000,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=3WE,14"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "Movie with Aoki,20130516T150000,20130516T160000,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=3TH,0"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Movie with Alex,20130518T173000,20130518T183000,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=3SA,0"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Pay day,20130521,20130522,Asia/Seoul,MONTHLY,INTERVAL=1;BYMONTHDAY=21,29"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Dinner with family,20130523T180000,20130523T190000,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=4TH,0"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "Phoenix Park,20130426,20130428,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=4FR,102"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "Wedding - Ferrero,20130428T120000,20130428T130000,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=4SU,64"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "Monthly report,20130424T180000,20130424T190000,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=-1WE,31"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "Lim\'s birthday party,20130531T210000,20130531T220000,Asia/Seoul,MONTHLY,INTERVAL=1;BYDAY=-1FR,50"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/providers/calendar/DemoUpdateReceiver;->DEMO_EVENTS_DATA:[Ljava/lang/String;

    .line 51
    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/android/providers/calendar/DemoUpdateReceiver;->DEMO_TASKS_DATA:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private pushEvent([Ljava/lang/String;)V
    .locals 24
    .param p1, "data"    # [Ljava/lang/String;

    .prologue
    .line 96
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 97
    .local v18, "values":Landroid/content/ContentValues;
    invoke-virtual/range {v18 .. v18}, Landroid/content/ContentValues;->clear()V

    .line 100
    const/16 v19, 0x0

    aget-object v17, p1, v19

    .line 101
    .local v17, "title":Ljava/lang/String;
    const/16 v19, 0x3

    aget-object v16, p1, v19

    .line 103
    .local v16, "timezone":Ljava/lang/String;
    if-eqz v16, :cond_0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 104
    :cond_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v16

    .line 107
    :cond_1
    new-instance v14, Landroid/text/format/Time;

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 108
    .local v14, "startTime":Landroid/text/format/Time;
    new-instance v5, Landroid/text/format/Time;

    move-object/from16 v0, v16

    invoke-direct {v5, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 110
    .local v5, "endTime":Landroid/text/format/Time;
    const/16 v19, 0x1

    aget-object v19, p1, v19

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 111
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v12

    .line 112
    .local v12, "startMillis":J
    const/16 v19, 0x2

    aget-object v19, p1, v19

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 113
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 115
    .local v6, "endMillis":J
    iget-boolean v8, v14, Landroid/text/format/Time;->allDay:Z

    .line 116
    .local v8, "isAllDay":Z
    const/16 v19, 0x6

    aget-object v19, p1, v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 118
    .local v15, "stickerId":I
    if-eqz v8, :cond_2

    .line 119
    const-string v16, "UTC"

    .line 121
    move-object/from16 v0, v16

    iput-object v0, v14, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 122
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v12

    .line 124
    move-object/from16 v0, v16

    iput-object v0, v5, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 125
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 128
    :cond_2
    const-string v19, "FREQ=%s;UNTIL=20361231T000000Z;WKST=MO;%s"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x4

    aget-object v22, p1, v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const/16 v22, 0x5

    aget-object v22, p1, v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 130
    .local v9, "rrule":Ljava/lang/String;
    const-string v19, "calendar_id"

    const/16 v20, 0x1

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 131
    const-string v19, "eventTimezone"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v19, "title"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v20, "allDay"

    if-eqz v8, :cond_3

    const/16 v19, 0x1

    :goto_0
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 134
    const-string v19, "dtstart"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 136
    const-string v19, "rrule"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    sub-long v20, v6, v12

    const-wide/16 v22, 0x3e8

    div-long v10, v20, v22

    .line 139
    .local v10, "seconds":J
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "P"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "S"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 141
    .local v4, "duration":Ljava/lang/String;
    const-string v19, "duration"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v20, "dtend"

    const/16 v19, 0x0

    check-cast v19, Ljava/lang/Long;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 144
    const-string v20, "description"

    const/16 v19, 0x0

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v20, "eventLocation"

    const/16 v19, 0x0

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v19, "availability"

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 147
    const-string v19, "accessLevel"

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 149
    const-string v19, "sticker_type"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 150
    const-string v19, "availabilityStatus"

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 151
    const-string v19, "latitude"

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152
    const-string v19, "longitude"

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/providers/calendar/DemoUpdateReceiver;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v19, v0

    sget-object v20, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 155
    return-void

    .line 133
    .end local v4    # "duration":Ljava/lang/String;
    .end local v10    # "seconds":J
    :cond_3
    const/16 v19, 0x0

    goto/16 :goto_0
.end method

.method private pushTask(Ljava/lang/String;)V
    .locals 6
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 159
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 160
    .local v0, "values":Landroid/content/ContentValues;
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 163
    const-string v1, "subject"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v3, "body"

    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v1, "body_size"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 170
    const-string v1, "accountKey"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 171
    const-string v1, "accountName"

    const-string v3, "My task"

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string v1, "importance"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 177
    const-string v1, "groupId"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 180
    const-string v1, "reminder_type"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 181
    const-string v1, "reminder_set"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 182
    const-string v1, "reminder_time"

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v1, "_sync_dirty"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 185
    iget-object v1, p0, Lcom/android/providers/calendar/DemoUpdateReceiver;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/android/providers/calendar/DemoUpdateReceiver;->SYNC_TASKS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 186
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 78
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iput-object v4, p0, Lcom/android/providers/calendar/DemoUpdateReceiver;->mContentResolver:Landroid/content/ContentResolver;

    .line 81
    const-string v4, "com.sec.android.intent.action.CREATE_DEMO_EVENTS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 82
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_0
    sget-object v4, Lcom/android/providers/calendar/DemoUpdateReceiver;->DEMO_EVENTS_DATA:[Ljava/lang/String;

    array-length v4, v4

    if-ge v3, v4, :cond_0

    .line 83
    const-string v2, "[,]"

    .line 84
    .local v2, "delims":Ljava/lang/String;
    sget-object v4, Lcom/android/providers/calendar/DemoUpdateReceiver;->DEMO_EVENTS_DATA:[Ljava/lang/String;

    aget-object v4, v4, v3

    invoke-virtual {v4, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "data":[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/providers/calendar/DemoUpdateReceiver;->pushEvent([Ljava/lang/String;)V

    .line 82
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 89
    .end local v1    # "data":[Ljava/lang/String;
    .end local v2    # "delims":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    :goto_1
    sget-object v4, Lcom/android/providers/calendar/DemoUpdateReceiver;->DEMO_TASKS_DATA:[Ljava/lang/String;

    array-length v4, v4

    if-ge v3, v4, :cond_1

    .line 90
    sget-object v4, Lcom/android/providers/calendar/DemoUpdateReceiver;->DEMO_TASKS_DATA:[Ljava/lang/String;

    aget-object v4, v4, v3

    invoke-direct {p0, v4}, Lcom/android/providers/calendar/DemoUpdateReceiver;->pushTask(Ljava/lang/String;)V

    .line 89
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 93
    .end local v3    # "index":I
    :cond_1
    return-void
.end method
