.class public Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;
.super Ljava/lang/Object;
.source "RecurrenceProcessorWithLunar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DaySet"
.end annotation


# instance fields
.field private mDays:I

.field private mMonth:I

.field private mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

.field private mTime:Lcom/android/providers/calendar/lunarRecurrence/LTime;

.field private mYear:I


# direct methods
.method public constructor <init>(Z)V
    .locals 2
    .param p1, "zulu"    # Z

    .prologue
    .line 315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316
    new-instance v0, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mTime:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .line 317
    return-void
.end method

.method private static generateDaysList(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/EventRecurrence;)I
    .locals 13
    .param p0, "generated"    # Landroid/text/format/Time;
    .param p1, "r"    # Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .prologue
    const/4 v12, 0x1

    .line 576
    const/4 v4, 0x0

    .line 583
    .local v4, "days":I
    const/4 v10, 0x4

    invoke-virtual {p0, v10}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v8

    .line 586
    .local v8, "lastDayThisMonth":I
    iget v3, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    .line 587
    .local v3, "count":I
    if-lez v3, :cond_7

    .line 589
    iget v7, p0, Landroid/text/format/Time;->monthDay:I

    .line 590
    .local v7, "j":I
    :goto_0
    const/16 v10, 0x8

    if-lt v7, v10, :cond_0

    .line 591
    add-int/lit8 v7, v7, -0x7

    goto :goto_0

    .line 593
    :cond_0
    iget v5, p0, Landroid/text/format/Time;->weekDay:I

    .line 594
    .local v5, "first":I
    if-lt v5, v7, :cond_2

    .line 595
    sub-int v10, v5, v7

    add-int/lit8 v5, v10, 0x1

    .line 604
    :goto_1
    iget-object v0, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->byday:[I

    .line 605
    .local v0, "byday":[I
    iget-object v1, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayNum:[I

    .line 606
    .local v1, "bydayNum":[I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    if-ge v6, v3, :cond_7

    .line 607
    aget v9, v1, v6

    .line 608
    .local v9, "v":I
    aget v10, v0, v6

    invoke-static {v10}, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->day2TimeDay(I)I

    move-result v10

    sub-int/2addr v10, v5

    add-int/lit8 v7, v10, 0x1

    .line 609
    if-gtz v7, :cond_1

    .line 610
    add-int/lit8 v7, v7, 0x7

    .line 612
    :cond_1
    if-nez v9, :cond_3

    .line 614
    :goto_3
    if-gt v7, v8, :cond_4

    .line 617
    shl-int v10, v12, v7

    or-int/2addr v4, v10

    .line 614
    add-int/lit8 v7, v7, 0x7

    goto :goto_3

    .line 597
    .end local v0    # "byday":[I
    .end local v1    # "bydayNum":[I
    .end local v6    # "i":I
    .end local v9    # "v":I
    :cond_2
    sub-int v10, v5, v7

    add-int/lit8 v5, v10, 0x8

    goto :goto_1

    .line 620
    .restart local v0    # "byday":[I
    .restart local v1    # "bydayNum":[I
    .restart local v6    # "i":I
    .restart local v9    # "v":I
    :cond_3
    if-lez v9, :cond_5

    .line 623
    add-int/lit8 v10, v9, -0x1

    mul-int/lit8 v10, v10, 0x7

    add-int/2addr v7, v10

    .line 624
    if-gt v7, v8, :cond_4

    .line 628
    shl-int v10, v12, v7

    or-int/2addr v4, v10

    .line 606
    :cond_4
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 634
    :cond_5
    :goto_5
    if-gt v7, v8, :cond_6

    add-int/lit8 v7, v7, 0x7

    goto :goto_5

    .line 640
    :cond_6
    mul-int/lit8 v10, v9, 0x7

    add-int/2addr v7, v10

    .line 641
    if-lt v7, v12, :cond_4

    .line 644
    shl-int v10, v12, v7

    or-int/2addr v4, v10

    goto :goto_4

    .line 654
    .end local v0    # "byday":[I
    .end local v1    # "bydayNum":[I
    .end local v5    # "first":I
    .end local v6    # "i":I
    .end local v7    # "j":I
    .end local v9    # "v":I
    :cond_7
    iget v10, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->freq:I

    const/4 v11, 0x5

    if-le v10, v11, :cond_e

    .line 655
    iget v3, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthdayCount:I

    .line 656
    if-eqz v3, :cond_e

    .line 657
    iget-object v2, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bymonthday:[I

    .line 658
    .local v2, "bymonthday":[I
    iget v10, p1, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bydayCount:I

    if-nez v10, :cond_a

    .line 659
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_6
    if-ge v6, v3, :cond_e

    .line 660
    aget v9, v2, v6

    .line 661
    .restart local v9    # "v":I
    if-ltz v9, :cond_9

    .line 662
    shl-int v10, v12, v9

    or-int/2addr v4, v10

    .line 659
    :cond_8
    :goto_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 664
    :cond_9
    add-int v10, v8, v9

    add-int/lit8 v7, v10, 0x1

    .line 665
    .restart local v7    # "j":I
    if-lt v7, v12, :cond_8

    if-gt v7, v8, :cond_8

    .line 666
    shl-int v10, v12, v7

    or-int/2addr v4, v10

    goto :goto_7

    .line 673
    .end local v6    # "i":I
    .end local v7    # "j":I
    .end local v9    # "v":I
    :cond_a
    const/4 v7, 0x1

    .restart local v7    # "j":I
    :goto_8
    if-gt v7, v8, :cond_e

    .line 675
    shl-int v10, v12, v7

    and-int/2addr v10, v4

    if-eqz v10, :cond_b

    .line 676
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_9
    if-ge v6, v3, :cond_d

    .line 677
    aget v10, v2, v6

    if-ne v10, v7, :cond_c

    .line 673
    .end local v6    # "i":I
    :cond_b
    :goto_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 676
    .restart local v6    # "i":I
    :cond_c
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    .line 681
    :cond_d
    shl-int v10, v12, v7

    xor-int/lit8 v10, v10, -0x1

    and-int/2addr v4, v10

    goto :goto_a

    .line 688
    .end local v2    # "bymonthday":[I
    .end local v6    # "i":I
    .end local v7    # "j":I
    :cond_e
    return v4
.end method


# virtual methods
.method get(Lcom/android/providers/calendar/lunarRecurrence/LTime;I)Z
    .locals 6
    .param p1, "iterator"    # Lcom/android/providers/calendar/lunarRecurrence/LTime;
    .param p2, "day"    # I

    .prologue
    const/4 v3, 0x1

    .line 506
    iget v1, p1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->year:I

    .line 507
    .local v1, "realYear":I
    iget v0, p1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->month:I

    .line 509
    .local v0, "realMonth":I
    const/4 v2, 0x0

    .line 517
    .local v2, "t":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    if-lt p2, v3, :cond_0

    const/16 v4, 0x1c

    if-le p2, v4, :cond_1

    .line 519
    :cond_0
    iget-object v2, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mTime:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .line 520
    invoke-virtual {v2, p2, v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(III)V

    .line 522
    iget-boolean v4, p1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    iput-boolean v4, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    .line 523
    iget-boolean v4, p1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    iput-boolean v4, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    .line 524
    invoke-static {v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;)V

    .line 525
    iget v1, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->year:I

    .line 526
    iget v0, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->month:I

    .line 527
    iget p2, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    .line 540
    :cond_1
    iget v4, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mYear:I

    if-ne v1, v4, :cond_2

    iget v4, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mMonth:I

    if-eq v0, v4, :cond_4

    .line 541
    :cond_2
    if-nez v2, :cond_3

    .line 542
    iget-object v2, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mTime:Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .line 543
    invoke-virtual {v2, p2, v0, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->set(III)V

    .line 545
    iget-boolean v4, p1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    iput-boolean v4, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->inLunar:Z

    .line 546
    iget-boolean v4, p1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    iput-boolean v4, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->isLeapMonth:Z

    .line 547
    invoke-static {v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;)V

    .line 555
    :cond_3
    iput v1, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mYear:I

    .line 556
    iput v0, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mMonth:I

    .line 557
    iget-object v4, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    invoke-static {v2, v4}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->generateDaysList(Landroid/text/format/Time;Lcom/android/providers/calendar/calendarcommon/EventRecurrence;)I

    move-result v4

    iput v4, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mDays:I

    .line 562
    :cond_4
    iget v4, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mDays:I

    shl-int v5, v3, p2

    and-int/2addr v4, v5

    if-eqz v4, :cond_5

    :goto_0
    return v3

    :cond_5
    const/4 v3, 0x0

    goto :goto_0
.end method

.method getfirstweekend(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I
    .locals 3
    .param p1, "time"    # Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .prologue
    .line 414
    const/4 v0, 0x1

    .line 415
    .local v0, "day":I
    iget v1, p1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    if-eqz v1, :cond_0

    iget v1, p1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    .line 416
    :cond_0
    const/4 v0, 0x1

    .line 420
    :goto_0
    return v0

    .line 418
    :cond_1
    iget v1, p1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    rsub-int/lit8 v0, v1, 0x7

    goto :goto_0
.end method

.method getfourthweekend(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I
    .locals 4
    .param p1, "time"    # Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .prologue
    .line 452
    const/4 v0, 0x1

    .line 453
    .local v0, "day":I
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getthirdweekend(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I

    move-result v0

    .line 454
    new-instance v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    invoke-direct {v1, p1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;-><init>(Landroid/text/format/Time;)V

    .line 455
    .local v1, "time1":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    .line 456
    invoke-static {v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;)V

    .line 457
    iget v2, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 458
    add-int/lit8 v0, v0, 0x1

    .line 462
    :cond_0
    :goto_0
    return v0

    .line 459
    :cond_1
    iget v2, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    if-nez v2, :cond_0

    .line 460
    add-int/lit8 v0, v0, 0x6

    goto :goto_0
.end method

.method getlastday(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I
    .locals 2
    .param p1, "iterator"    # Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .prologue
    .line 500
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;->getActualMaximum(I)I

    move-result v0

    .line 501
    .local v0, "day":I
    return v0
.end method

.method getsecondweekend(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I
    .locals 4
    .param p1, "time"    # Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .prologue
    .line 424
    const/4 v0, 0x1

    .line 425
    .local v0, "day":I
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getfirstweekend(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I

    move-result v0

    .line 426
    new-instance v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    invoke-direct {v1, p1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;-><init>(Landroid/text/format/Time;)V

    .line 427
    .local v1, "time1":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    .line 428
    invoke-static {v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;)V

    .line 429
    iget v2, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 430
    add-int/lit8 v0, v0, 0x1

    .line 434
    :cond_0
    :goto_0
    return v0

    .line 431
    :cond_1
    iget v2, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    if-nez v2, :cond_0

    .line 432
    add-int/lit8 v0, v0, 0x6

    goto :goto_0
.end method

.method getthirdweekend(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I
    .locals 4
    .param p1, "time"    # Lcom/android/providers/calendar/lunarRecurrence/LTime;

    .prologue
    .line 438
    const/4 v0, 0x1

    .line 439
    .local v0, "day":I
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getsecondweekend(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I

    move-result v0

    .line 440
    new-instance v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    invoke-direct {v1, p1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;-><init>(Landroid/text/format/Time;)V

    .line 441
    .local v1, "time1":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    iput v0, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    .line 442
    invoke-static {v1}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;)V

    .line 443
    iget v2, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 444
    add-int/lit8 v0, v0, 0x1

    .line 448
    :cond_0
    :goto_0
    return v0

    .line 445
    :cond_1
    iget v2, v1, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    if-nez v2, :cond_0

    .line 446
    add-int/lit8 v0, v0, 0x6

    goto :goto_0
.end method

.method getweek_day(Landroid/text/format/Time;)I
    .locals 10
    .param p1, "iterator"    # Landroid/text/format/Time;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x6

    const/4 v5, 0x1

    .line 328
    const/4 v0, 0x1

    .line 329
    .local v0, "day":I
    iget-object v3, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v7

    if-ne v3, v5, :cond_3

    .line 330
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 331
    .local v2, "time":Landroid/text/format/Time;
    iput v5, v2, Landroid/text/format/Time;->monthDay:I

    .line 332
    invoke-static {v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 333
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eqz v3, :cond_1

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v6, :cond_1

    .line 334
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    .line 410
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_0
    :goto_0
    return v0

    .line 336
    .restart local v2    # "time":Landroid/text/format/Time;
    :cond_1
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-nez v3, :cond_2

    .line 337
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x1

    goto :goto_0

    .line 338
    :cond_2
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v6, :cond_0

    .line 339
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x2

    goto :goto_0

    .line 342
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_3
    iget-object v3, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v7

    if-ne v3, v9, :cond_8

    .line 343
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 344
    .restart local v2    # "time":Landroid/text/format/Time;
    iput v5, v2, Landroid/text/format/Time;->monthDay:I

    .line 345
    invoke-static {v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 346
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v5, :cond_4

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v9, :cond_4

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v8, :cond_5

    .line 348
    :cond_4
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x1

    goto :goto_0

    .line 350
    :cond_5
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_6

    .line 351
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x3

    goto :goto_0

    .line 352
    :cond_6
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v6, :cond_7

    .line 353
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x3

    goto :goto_0

    .line 354
    :cond_7
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-nez v3, :cond_0

    .line 355
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x2

    goto :goto_0

    .line 358
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_8
    iget-object v3, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v7

    const/4 v4, 0x3

    if-ne v3, v4, :cond_e

    .line 359
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 360
    .restart local v2    # "time":Landroid/text/format/Time;
    iput v5, v2, Landroid/text/format/Time;->monthDay:I

    .line 361
    invoke-static {v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 362
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v5, :cond_9

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v9, :cond_9

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_a

    .line 363
    :cond_9
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x2

    goto :goto_0

    .line 365
    :cond_a
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v8, :cond_b

    .line 366
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x4

    goto/16 :goto_0

    .line 367
    :cond_b
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_c

    .line 368
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x4

    goto/16 :goto_0

    .line 369
    :cond_c
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v6, :cond_d

    .line 370
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x4

    goto/16 :goto_0

    .line 371
    :cond_d
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-nez v3, :cond_0

    .line 372
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x3

    goto/16 :goto_0

    .line 375
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_e
    iget-object v3, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v7

    if-ne v3, v8, :cond_15

    .line 376
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 377
    .restart local v2    # "time":Landroid/text/format/Time;
    iput v5, v2, Landroid/text/format/Time;->monthDay:I

    .line 378
    invoke-static {v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 379
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v5, :cond_f

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v9, :cond_10

    .line 380
    :cond_f
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x3

    goto/16 :goto_0

    .line 382
    :cond_10
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_11

    .line 383
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x5

    goto/16 :goto_0

    .line 384
    :cond_11
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v8, :cond_12

    .line 385
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x5

    goto/16 :goto_0

    .line 386
    :cond_12
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_13

    .line 387
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x5

    goto/16 :goto_0

    .line 388
    :cond_13
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v6, :cond_14

    .line 389
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x5

    goto/16 :goto_0

    .line 390
    :cond_14
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-nez v3, :cond_0

    .line 391
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, 0x4

    goto/16 :goto_0

    .line 394
    .end local v2    # "time":Landroid/text/format/Time;
    :cond_15
    iget-object v3, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v7

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 395
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 397
    .restart local v2    # "time":Landroid/text/format/Time;
    invoke-virtual {p1, v8}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v1

    .line 398
    .local v1, "lastday":I
    iput v1, v2, Landroid/text/format/Time;->monthDay:I

    .line 399
    invoke-static {v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Landroid/text/format/Time;)V

    .line 400
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eqz v3, :cond_16

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v6, :cond_16

    .line 401
    move v0, v1

    goto/16 :goto_0

    .line 403
    :cond_16
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-nez v3, :cond_17

    .line 404
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, -0x2

    goto/16 :goto_0

    .line 405
    :cond_17
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v6, :cond_0

    .line 406
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v3, -0x1

    goto/16 :goto_0
.end method

.method getweekend_day(Landroid/text/format/Time;)I
    .locals 7
    .param p1, "iterator"    # Landroid/text/format/Time;

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 466
    const/4 v0, 0x1

    .line 468
    .local v0, "day":I
    new-instance v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;

    invoke-direct {v2, p1}, Lcom/android/providers/calendar/lunarRecurrence/LTime;-><init>(Landroid/text/format/Time;)V

    .line 469
    .local v2, "time":Lcom/android/providers/calendar/lunarRecurrence/LTime;
    iput v4, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    .line 470
    invoke-static {v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;)V

    .line 472
    iget-object v3, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v5

    if-ne v3, v4, :cond_1

    .line 473
    invoke-virtual {p0, v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getfirstweekend(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I

    move-result v0

    .line 495
    :cond_0
    :goto_0
    return v0

    .line 475
    :cond_1
    iget-object v3, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v5

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 476
    invoke-virtual {p0, v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getsecondweekend(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I

    move-result v0

    goto :goto_0

    .line 478
    :cond_2
    iget-object v3, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v5

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    .line 479
    invoke-virtual {p0, v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getthirdweekend(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I

    move-result v0

    goto :goto_0

    .line 481
    :cond_3
    iget-object v3, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v5

    if-ne v3, v6, :cond_4

    .line 482
    invoke-virtual {p0, v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->getfourthweekend(Lcom/android/providers/calendar/lunarRecurrence/LTime;)I

    move-result v0

    goto :goto_0

    .line 484
    :cond_4
    iget-object v3, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    iget-object v3, v3, Lcom/android/providers/calendar/calendarcommon/EventRecurrence;->bysetpos:[I

    aget v3, v3, v5

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 485
    invoke-virtual {p1, v6}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v1

    .line 486
    .local v1, "lastday":I
    iput v1, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->monthDay:I

    .line 487
    invoke-static {v2}, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar;->unsafeNormalize(Lcom/android/providers/calendar/lunarRecurrence/LTime;)V

    .line 488
    iget v3, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    if-eqz v3, :cond_5

    iget v3, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_6

    .line 489
    :cond_5
    move v0, v1

    goto :goto_0

    .line 491
    :cond_6
    iget v3, v2, Lcom/android/providers/calendar/lunarRecurrence/LTime;->weekDay:I

    sub-int v0, v1, v3

    goto :goto_0
.end method

.method setRecurrence(Lcom/android/providers/calendar/calendarcommon/EventRecurrence;)V
    .locals 1
    .param p1, "r"    # Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .prologue
    .line 321
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mYear:I

    .line 322
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mMonth:I

    .line 323
    iput-object p1, p0, Lcom/android/providers/calendar/lunarRecurrence/RecurrenceProcessorWithLunar$DaySet;->mR:Lcom/android/providers/calendar/calendarcommon/EventRecurrence;

    .line 324
    return-void
.end method
