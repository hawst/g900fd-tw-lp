.class Lcom/android/providers/calendar/CalendarReceiver$1;
.super Ljava/lang/Object;
.source "CalendarReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/providers/calendar/CalendarReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/providers/calendar/CalendarReceiver;

.field final synthetic val$action:Ljava/lang/String;

.field final synthetic val$cr:Landroid/content/ContentResolver;

.field final synthetic val$reason:I

.field final synthetic val$result:Landroid/content/BroadcastReceiver$PendingResult;


# direct methods
.method constructor <init>(Lcom/android/providers/calendar/CalendarReceiver;Ljava/lang/String;Landroid/content/ContentResolver;ILandroid/content/BroadcastReceiver$PendingResult;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->this$0:Lcom/android/providers/calendar/CalendarReceiver;

    iput-object p2, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->val$action:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->val$cr:Landroid/content/ContentResolver;

    iput p4, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->val$reason:I

    iput-object p5, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->val$result:Landroid/content/BroadcastReceiver$PendingResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 60
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->val$action:Ljava/lang/String;

    const-string v1, "com.android.providers.calendar.SCHEDULE_ALARM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->val$cr:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/providers/calendar/CalendarAlarmManager;->SCHEDULE_ALARM_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 67
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->val$result:Landroid/content/BroadcastReceiver$PendingResult;

    invoke-virtual {v0}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    .line 68
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->this$0:Lcom/android/providers/calendar/CalendarReceiver;

    # getter for: Lcom/android/providers/calendar/CalendarReceiver;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/android/providers/calendar/CalendarReceiver;->access$100(Lcom/android/providers/calendar/CalendarReceiver;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 69
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->val$action:Ljava/lang/String;

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->val$action:Ljava/lang/String;

    const-string v1, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->val$reason:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 65
    :cond_2
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->this$0:Lcom/android/providers/calendar/CalendarReceiver;

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarReceiver$1;->val$cr:Landroid/content/ContentResolver;

    # invokes: Lcom/android/providers/calendar/CalendarReceiver;->removeScheduledAlarms(Landroid/content/ContentResolver;)V
    invoke-static {v0, v1}, Lcom/android/providers/calendar/CalendarReceiver;->access$000(Lcom/android/providers/calendar/CalendarReceiver;Landroid/content/ContentResolver;)V

    goto :goto_0
.end method
