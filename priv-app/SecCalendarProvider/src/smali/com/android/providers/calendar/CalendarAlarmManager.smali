.class public Lcom/android/providers/calendar/CalendarAlarmManager;
.super Ljava/lang/Object;
.source "CalendarAlarmManager.java"


# static fields
.field static final SCHEDULE_ALARM_REMOVE_URI:Landroid/net/Uri;

.field static final SCHEDULE_ALARM_URI:Landroid/net/Uri;


# instance fields
.field protected mAlarmLock:Ljava/lang/Object;

.field private mAlarmManager:Landroid/app/AlarmManager;

.field protected mContext:Landroid/content/Context;

.field protected mNextAlarmCheckScheduled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mScheduleNextAlarmWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    sget-object v0, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "schedule_alarms_remove"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/calendar/CalendarAlarmManager;->SCHEDULE_ALARM_REMOVE_URI:Landroid/net/Uri;

    .line 65
    sget-object v0, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "schedule_alarms"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/calendar/CalendarAlarmManager;->SCHEDULE_ALARM_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    invoke-virtual {p0, p1}, Lcom/android/providers/calendar/CalendarAlarmManager;->initializeWithContext(Landroid/content/Context;)V

    .line 132
    return-void
.end method

.method private static removeScheduledAlarmsLocked(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 570
    const-string v0, "CalendarProvider2"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    const-string v0, "CalendarProvider2"

    const-string v1, "removing scheduled alarms"

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    :cond_0
    const-string v0, "CalendarAlerts"

    const-string v1, "state=0"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 575
    return-void
.end method

.method private static removeTaskScheduledAlarms(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 580
    const-string v0, "CalendarProvider2"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    const-string v0, "CalendarProvider2"

    const-string v1, "removing Task scheduled alarms"

    invoke-static {v0, v1}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    :cond_0
    const-string v0, "TasksReminders"

    const-string v1, "state= \'0\'"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 584
    return-void
.end method

.method private scheduleNextAlarmLocked(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/calendar/CalendarProvider2;)V
    .locals 64
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "cp2"    # Lcom/android/providers/calendar/CalendarProvider2;

    .prologue
    .line 273
    new-instance v55, Landroid/text/format/Time;

    invoke-direct/range {v55 .. v55}, Landroid/text/format/Time;-><init>()V

    .line 275
    .local v55, "time":Landroid/text/format/Time;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v30

    .line 276
    .local v30, "currentMillis":J
    const-wide/32 v22, 0x6ddd00

    sub-long v48, v30, v22

    .line 277
    .local v48, "start":J
    const-wide/32 v22, 0x5265c00

    add-long v34, v48, v22

    .line 278
    .local v34, "end":J
    const-string v5, "CalendarProvider2"

    const/4 v14, 0x3

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 279
    move-object/from16 v0, v55

    move-wide/from16 v1, v48

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    .line 280
    const-string v5, " %a, %b %d, %Y %I:%M%P"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    .line 281
    .local v50, "startTimeStr":Ljava/lang/String;
    const-string v5, "CalendarProvider2"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "runScheduleNextAlarm() start search: "

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v50

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    .end local v50    # "startTimeStr":Ljava/lang/String;
    :cond_0
    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v47, v0

    const/4 v5, 0x0

    const-wide/32 v22, 0x247a6100

    sub-long v22, v30, v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v47, v5

    .line 293
    .local v47, "selectArg":[Ljava/lang/String;
    const-string v5, "CalendarAlerts"

    const-string v14, "_id IN (SELECT ca._id FROM CalendarAlerts AS ca LEFT OUTER JOIN Instances USING (event_id,begin,end) LEFT OUTER JOIN Reminders AS r ON (ca.event_id=r.event_id AND ca.minutes=r.minutes) LEFT OUTER JOIN view_events AS e ON (ca.event_id=e._id) WHERE Instances.begin ISNULL   OR ca.alarmTime<?   OR (r.minutes ISNULL       AND ca.minutes<>0)   OR e.visible=0)"

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-virtual {v0, v5, v14, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v45

    .line 296
    .local v45, "rowsDeleted":I
    move-wide/from16 v40, v34

    .line 297
    .local v40, "nextAlarmTime":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/providers/calendar/CalendarAlarmManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    .line 298
    .local v15, "resolver":Landroid/content/ContentResolver;
    move-wide/from16 v0, v30

    invoke-static {v15, v0, v1}, Landroid/provider/CalendarContract$CalendarAlerts;->findNextAlarmTime(Landroid/content/ContentResolver;J)J

    move-result-wide v58

    .line 299
    .local v58, "tmpAlarmTime":J
    const-wide/16 v22, -0x1

    cmp-long v5, v58, v22

    if-eqz v5, :cond_1

    cmp-long v5, v58, v40

    if-gez v5, :cond_1

    .line 300
    move-wide/from16 v40, v58

    .line 325
    :cond_1
    invoke-virtual/range {v55 .. v55}, Landroid/text/format/Time;->setToNow()V

    .line 326
    const/4 v5, 0x0

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    .line 327
    move-object/from16 v0, v55

    iget-wide v0, v0, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v22, v0

    const-wide/16 v62, 0x3e8

    mul-long v38, v22, v62

    .line 329
    .local v38, "localOffset":J
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " -("

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v38

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, ") "

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 330
    .local v26, "allDayOffset":Ljava/lang/String;
    const-string v51, "SELECT begin"

    .line 331
    .local v51, "subQueryPrefix":Ljava/lang/String;
    const-string v52, " -(minutes*60000) AS myAlarmTime,Instances.event_id AS eventId,begin,end,title,allDay,method,minutes FROM Instances INNER JOIN view_events ON (view_events._id=Instances.event_id) INNER JOIN Reminders ON (Instances.event_id=Reminders.event_id) WHERE visible=1 AND myAlarmTime>=CAST(? AS INT) AND myAlarmTime<=CAST(? AS INT) AND end>=? AND method=1"

    .line 347
    .local v52, "subQuerySuffix":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v51

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v52

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, " AND "

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "allDay"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "=1"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 349
    .local v27, "allDayQuery":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v51

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v52

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, " AND "

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "allDay"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "=0"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    .line 354
    .local v42, "nonAllDayQuery":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "SELECT * FROM ("

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, " UNION ALL "

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v42

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, ")"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, " WHERE 0=(SELECT count(*) FROM "

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "CalendarAlerts"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, " CA"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, " WHERE CA."

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "event_id"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "=eventId"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, " AND CA."

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "begin"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "="

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "begin"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, " AND CA."

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "alarmTime"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "=myAlarmTime)"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, " ORDER BY myAlarmTime,"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "begin"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, ","

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v14, "title"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    .line 361
    .local v43, "query":Ljava/lang/String;
    const/4 v5, 0x6

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v44, v0

    const/4 v5, 0x0

    invoke-static/range {v48 .. v49}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v44, v5

    const/4 v5, 0x1

    invoke-static/range {v40 .. v41}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v44, v5

    const/4 v5, 0x2

    invoke-static/range {v30 .. v31}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v44, v5

    const/4 v5, 0x3

    invoke-static/range {v48 .. v49}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v44, v5

    const/4 v5, 0x4

    invoke-static/range {v40 .. v41}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v44, v5

    const/4 v5, 0x5

    invoke-static/range {v30 .. v31}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v44, v5

    .line 365
    .local v44, "queryParams":[Ljava/lang/String;
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/providers/calendar/CalendarProvider2;->mCalendarCache:Lcom/android/providers/calendar/CalendarCache;

    invoke-virtual {v5}, Lcom/android/providers/calendar/CalendarCache;->readTimezoneInstances()Ljava/lang/String;

    move-result-object v12

    .line 366
    .local v12, "instancesTimezone":Ljava/lang/String;
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/providers/calendar/CalendarProvider2;->mCalendarCache:Lcom/android/providers/calendar/CalendarCache;

    invoke-virtual {v5}, Lcom/android/providers/calendar/CalendarCache;->readTimezoneType()Ljava/lang/String;

    move-result-object v5

    const-string v14, "home"

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    .line 370
    .local v13, "isHomeTimezone":Z
    const-wide/32 v22, 0x5265c00

    sub-long v6, v48, v22

    const-wide/32 v22, 0x5265c00

    add-long v8, v34, v22

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v5, p2

    invoke-virtual/range {v5 .. v13}, Lcom/android/providers/calendar/CalendarProvider2;->acquireInstanceRangeLocked(JJZZLjava/lang/String;Z)V

    .line 373
    const/16 v32, 0x0

    .line 375
    .local v32, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p1

    move-object/from16 v1, v43

    move-object/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v32

    .line 377
    const-string v5, "begin"

    move-object/from16 v0, v32

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 378
    .local v28, "beginIndex":I
    const-string v5, "end"

    move-object/from16 v0, v32

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    .line 379
    .local v33, "endIndex":I
    const-string v5, "eventId"

    move-object/from16 v0, v32

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v36

    .line 380
    .local v36, "eventIdIndex":I
    const-string v5, "myAlarmTime"

    move-object/from16 v0, v32

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 381
    .local v4, "alarmTimeIndex":I
    const-string v5, "minutes"

    move-object/from16 v0, v32

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v37

    .line 383
    .local v37, "minutesIndex":I
    const-string v5, "CalendarProvider2"

    const/4 v14, 0x3

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 384
    move-object/from16 v0, v55

    move-wide/from16 v1, v40

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    .line 385
    const-string v5, " %a, %b %d, %Y %I:%M%P"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 386
    .local v25, "alarmTimeStr":Ljava/lang/String;
    const-string v5, "CalendarProvider2"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "cursor results: "

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->getCount()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v22, " nextAlarmTime: "

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    .end local v25    # "alarmTimeStr":Ljava/lang/String;
    :cond_2
    :goto_0
    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 398
    move-object/from16 v0, v32

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 399
    .local v10, "alarmTime":J
    move-object/from16 v0, v32

    move/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 400
    .local v6, "eventId":J
    move-object/from16 v0, v32

    move/from16 v1, v37

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    .line 401
    .local v24, "minutes":I
    move-object/from16 v0, v32

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 402
    .local v8, "startTime":J
    invoke-interface/range {v32 .. v33}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 404
    .local v20, "endTime":J
    const-string v5, "CalendarProvider2"

    const/4 v14, 0x3

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 405
    move-object/from16 v0, v55

    invoke-virtual {v0, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 406
    const-string v5, " %a, %b %d, %Y %I:%M%P"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v46

    .line 407
    .local v46, "schedTime":Ljava/lang/String;
    move-object/from16 v0, v55

    invoke-virtual {v0, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 408
    const-string v5, " %a, %b %d, %Y %I:%M%P"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    .line 410
    .restart local v50    # "startTimeStr":Ljava/lang/String;
    const-string v5, "CalendarProvider2"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "  looking at id: "

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v22, " "

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v50

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v22, " alarm: "

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v46

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    .end local v46    # "schedTime":Ljava/lang/String;
    .end local v50    # "startTimeStr":Ljava/lang/String;
    :cond_3
    cmp-long v5, v10, v40

    if-gez v5, :cond_6

    .line 416
    move-wide/from16 v40, v10

    :cond_4
    move-object v5, v15

    .line 429
    invoke-static/range {v5 .. v11}, Landroid/provider/CalendarContract$CalendarAlerts;->alarmExists(Landroid/content/ContentResolver;JJJ)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 430
    const-string v5, "CalendarProvider2"

    const/4 v14, 0x3

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 431
    const-string v5, "title"

    move-object/from16 v0, v32

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v57

    .line 432
    .local v57, "titleIndex":I
    move-object/from16 v0, v32

    move/from16 v1, v57

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v56

    .line 433
    .local v56, "title":Ljava/lang/String;
    const-string v5, "CalendarProvider2"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "  alarm exists for id: "

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v22, " "

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v56

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 453
    .end local v4    # "alarmTimeIndex":I
    .end local v6    # "eventId":J
    .end local v8    # "startTime":J
    .end local v10    # "alarmTime":J
    .end local v20    # "endTime":J
    .end local v24    # "minutes":I
    .end local v28    # "beginIndex":I
    .end local v33    # "endIndex":I
    .end local v36    # "eventIdIndex":I
    .end local v37    # "minutesIndex":I
    .end local v56    # "title":Ljava/lang/String;
    .end local v57    # "titleIndex":I
    :catchall_0
    move-exception v5

    if-eqz v32, :cond_5

    .line 454
    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v5

    .line 417
    .restart local v4    # "alarmTimeIndex":I
    .restart local v6    # "eventId":J
    .restart local v8    # "startTime":J
    .restart local v10    # "alarmTime":J
    .restart local v20    # "endTime":J
    .restart local v24    # "minutes":I
    .restart local v28    # "beginIndex":I
    .restart local v33    # "endIndex":I
    .restart local v36    # "eventIdIndex":I
    .restart local v37    # "minutesIndex":I
    :cond_6
    const-wide/32 v22, 0xea60

    add-long v22, v22, v40

    cmp-long v5, v10, v22

    if-lez v5, :cond_4

    .line 420
    :try_start_1
    const-string v5, "CalendarProvider2"

    const/4 v14, 0x3

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 421
    const-string v5, "CalendarProvider2"

    const-string v14, "This event alarm (and all later ones) will be scheduled later"

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453
    .end local v6    # "eventId":J
    .end local v8    # "startTime":J
    .end local v10    # "alarmTime":J
    .end local v20    # "endTime":J
    .end local v24    # "minutes":I
    :cond_7
    if-eqz v32, :cond_8

    .line 454
    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->close()V

    .line 459
    :cond_8
    if-lez v45, :cond_9

    .line 460
    move-object/from16 v0, p0

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/calendar/CalendarAlarmManager;->scheduleAlarm(J)V

    .line 468
    :cond_9
    const-wide v22, 0x7fffffffffffffffL

    cmp-long v5, v40, v22

    if-eqz v5, :cond_f

    .line 469
    const-wide/32 v22, 0xea60

    add-long v22, v22, v40

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/calendar/CalendarAlarmManager;->scheduleNextAlarmCheck(J)V

    .line 479
    :goto_1
    const/16 v29, 0x0

    .line 480
    .local v29, "c":Landroid/database/Cursor;
    const-string v53, "SELECT * FROM Tasks WHERE reminder_time > ?  AND due_date > ?  AND reminder_type != \'0\'  AND deleted = \'0\'  ORDER BY reminder_time ASC"

    .line 487
    .local v53, "taskReminderQuery":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v54, v0

    const/4 v5, 0x0

    invoke-static/range {v30 .. v31}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v54, v5

    const/4 v5, 0x1

    const-wide/32 v22, 0x5265c00

    sub-long v22, v30, v22

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v54, v5

    .line 489
    .local v54, "taskReminderQueryParams":[Ljava/lang/String;
    const-string v5, "CalendarAlarmManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "sys current time:"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/16 v22, 0x0

    aget-object v22, v54, v22

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    :try_start_2
    new-instance v61, Landroid/content/ContentValues;

    invoke-direct/range {v61 .. v61}, Landroid/content/ContentValues;-><init>()V

    .line 492
    .local v61, "values":Landroid/content/ContentValues;
    move-object/from16 v0, p1

    move-object/from16 v1, v53

    move-object/from16 v2, v54

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v29

    .line 493
    const-wide/16 v18, 0x0

    .line 494
    .local v18, "reminder_time":J
    const-wide/16 v16, 0x0

    .line 496
    .local v16, "task_id":J
    const-string v5, "CalendarAlarmManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "reminder amount:"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->getCount()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    if-eqz v29, :cond_b

    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_b

    .line 499
    const-string v5, "CalendarAlarmManager"

    const-string v14, "Task alert register"

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToFirst()Z

    .line 502
    :cond_a
    const-string v5, "CalendarAlarmManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Task id:"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v22, "_id"

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, v29

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    const-string v5, "CalendarAlarmManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Task remindertime:"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v22, "reminder_time"

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, v29

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    const-string v5, "_id"

    move-object/from16 v0, v29

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v29

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 506
    const-string v5, "reminder_time"

    move-object/from16 v0, v29

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v29

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    move-object/from16 v14, p0

    .line 508
    invoke-direct/range {v14 .. v19}, Lcom/android/providers/calendar/CalendarAlarmManager;->taskAlarmExists(Landroid/content/ContentResolver;JJ)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 509
    const-string v5, "CalendarAlarmManager"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "already exist id:"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    :goto_2
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v5

    if-nez v5, :cond_a

    .line 526
    :cond_b
    if-eqz v29, :cond_c

    .line 527
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->close()V

    .line 528
    const/16 v29, 0x0

    .line 532
    :cond_c
    return-void

    .end local v16    # "task_id":J
    .end local v18    # "reminder_time":J
    .end local v29    # "c":Landroid/database/Cursor;
    .end local v53    # "taskReminderQuery":Ljava/lang/String;
    .end local v54    # "taskReminderQueryParams":[Ljava/lang/String;
    .end local v61    # "values":Landroid/content/ContentValues;
    .restart local v6    # "eventId":J
    .restart local v8    # "startTime":J
    .restart local v10    # "alarmTime":J
    .restart local v20    # "endTime":J
    .restart local v24    # "minutes":I
    :cond_d
    move-wide/from16 v16, v6

    move-wide/from16 v18, v8

    move-wide/from16 v22, v10

    .line 440
    :try_start_3
    invoke-static/range {v15 .. v24}, Landroid/provider/CalendarContract$CalendarAlerts;->insert(Landroid/content/ContentResolver;JJJJI)Landroid/net/Uri;

    move-result-object v60

    .line 442
    .local v60, "uri":Landroid/net/Uri;
    if-nez v60, :cond_e

    .line 443
    const-string v5, "CalendarProvider2"

    const/4 v14, 0x6

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 444
    const-string v5, "CalendarProvider2"

    const-string v14, "runScheduleNextAlarm() insert into CalendarAlerts table failed"

    invoke-static {v5, v14}, Lcom/android/providers/calendar/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 450
    :cond_e
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/android/providers/calendar/CalendarAlarmManager;->scheduleAlarm(J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 471
    .end local v6    # "eventId":J
    .end local v8    # "startTime":J
    .end local v10    # "alarmTime":J
    .end local v20    # "endTime":J
    .end local v24    # "minutes":I
    .end local v60    # "uri":Landroid/net/Uri;
    :cond_f
    const-wide/32 v22, 0x5265c00

    add-long v22, v22, v30

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/android/providers/calendar/CalendarAlarmManager;->scheduleNextAlarmCheck(J)V

    goto/16 :goto_1

    .line 513
    .restart local v16    # "task_id":J
    .restart local v18    # "reminder_time":J
    .restart local v29    # "c":Landroid/database/Cursor;
    .restart local v53    # "taskReminderQuery":Ljava/lang/String;
    .restart local v54    # "taskReminderQueryParams":[Ljava/lang/String;
    .restart local v61    # "values":Landroid/content/ContentValues;
    :cond_10
    :try_start_4
    const-string v5, "task_id"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    move-object/from16 v0, v61

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 514
    const-string v5, "reminder_time"

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    move-object/from16 v0, v61

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 515
    const-string v5, "state"

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 v0, v61

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 516
    const-string v5, "subject"

    const-string v14, "subject"

    move-object/from16 v0, v29

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, v29

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, v61

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    const-string v5, "due_date"

    const-string v14, "due_date"

    move-object/from16 v0, v29

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, v29

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    move-object/from16 v0, v61

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 518
    const-string v5, "accountkey"

    const-string v14, "accountKey"

    move-object/from16 v0, v29

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, v29

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 v0, v61

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 519
    const-string v5, "reminder_type"

    const-string v14, "reminder_type"

    move-object/from16 v0, v29

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, v29

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 v0, v61

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 520
    sget-object v5, Lcom/android/calendar/TaskContract$TasksReminders;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v61

    invoke-virtual {v15, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 521
    invoke-virtual/range {v61 .. v61}, Landroid/content/ContentValues;->clear()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_2

    .line 526
    .end local v16    # "task_id":J
    .end local v18    # "reminder_time":J
    .end local v61    # "values":Landroid/content/ContentValues;
    :catchall_1
    move-exception v5

    if-eqz v29, :cond_11

    .line 527
    invoke-interface/range {v29 .. v29}, Landroid/database/Cursor;->close()V

    .line 528
    const/16 v29, 0x0

    :cond_11
    throw v5
.end method

.method private taskAlarmExists(Landroid/content/ContentResolver;JJ)Z
    .locals 10
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "taskId"    # J
    .param p4, "reminder_time"    # J

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 538
    const-string v3, "task_id = ?  AND reminder_time = ? "

    .line 540
    .local v3, "WHERE_ALARM_EXISTS":Ljava/lang/String;
    new-array v2, v8, [Ljava/lang/String;

    const-string v0, "reminder_time"

    aput-object v0, v2, v5

    .line 541
    .local v2, "projection":[Ljava/lang/String;
    sget-object v1, Lcom/android/calendar/TaskContract$TasksReminders;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 543
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 545
    .local v7, "found":Z
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    .line 546
    const/4 v7, 0x1

    .line 549
    :cond_0
    if-eqz v6, :cond_1

    .line 550
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 553
    :cond_1
    return v7

    .line 549
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 550
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method


# virtual methods
.method acquireScheduleNextAlarmWakeLock()V
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/android/providers/calendar/CalendarAlarmManager;->getScheduleNextAlarmWakeLock()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 181
    return-void
.end method

.method public cancel(Landroid/app/PendingIntent;)V
    .locals 1
    .param p1, "operation"    # Landroid/app/PendingIntent;

    .prologue
    .line 592
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 593
    return-void
.end method

.method getScheduleNextAlarmWakeLock()Landroid/os/PowerManager$WakeLock;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 164
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mScheduleNextAlarmWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 165
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 169
    .local v0, "powerManager":Landroid/os/PowerManager;
    const-string v1, "ScheduleNextAlarmWakeLock"

    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mScheduleNextAlarmWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 174
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mScheduleNextAlarmWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 176
    .end local v0    # "powerManager":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mScheduleNextAlarmWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v1
.end method

.method protected initializeWithContext(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mContext:Landroid/content/Context;

    .line 136
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mAlarmManager:Landroid/app/AlarmManager;

    .line 137
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mNextAlarmCheckScheduled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 138
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mAlarmLock:Ljava/lang/Object;

    .line 139
    return-void
.end method

.method releaseScheduleNextAlarmWakeLock()V
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/android/providers/calendar/CalendarAlarmManager;->getScheduleNextAlarmWakeLock()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/android/providers/calendar/CalendarAlarmManager;->getScheduleNextAlarmWakeLock()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 191
    :cond_0
    return-void
.end method

.method rescheduleMissedAlarms()V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/providers/calendar/CalendarAlarmManager;->rescheduleMissedAlarms(Landroid/content/ContentResolver;)V

    .line 195
    return-void
.end method

.method public rescheduleMissedAlarms(Landroid/content/ContentResolver;)V
    .locals 2
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 600
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-static {p1, v0, v1}, Landroid/provider/CalendarContract$CalendarAlerts;->rescheduleMissedAlarms(Landroid/content/ContentResolver;Landroid/content/Context;Landroid/app/AlarmManager;)V

    .line 601
    return-void
.end method

.method runScheduleNextAlarm(ZLcom/android/providers/calendar/CalendarProvider2;)V
    .locals 3
    .param p1, "removeAlarms"    # Z
    .param p2, "cp2"    # Lcom/android/providers/calendar/CalendarProvider2;

    .prologue
    .line 204
    iget-object v0, p2, Lcom/android/providers/calendar/CalendarProvider2;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 205
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mNextAlarmCheckScheduled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 210
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 212
    if-eqz p1, :cond_1

    .line 213
    :try_start_0
    invoke-static {v0}, Lcom/android/providers/calendar/CalendarAlarmManager;->removeScheduledAlarmsLocked(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 216
    invoke-static {v0}, Lcom/android/providers/calendar/CalendarAlarmManager;->removeTaskScheduledAlarms(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 219
    :cond_1
    invoke-direct {p0, v0, p2}, Lcom/android/providers/calendar/CalendarAlarmManager;->scheduleNextAlarmLocked(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/providers/calendar/CalendarProvider2;)V

    .line 220
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public scheduleAlarm(J)V
    .locals 3
    .param p1, "alarmTime"    # J

    .prologue
    .line 596
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-static {v0, v1, p1, p2}, Landroid/provider/CalendarContract$CalendarAlerts;->scheduleAlarm(Landroid/content/Context;Landroid/app/AlarmManager;J)V

    .line 597
    return-void
.end method

.method scheduleNextAlarm(Z)V
    .locals 8
    .param p1, "removeAlarms"    # Z

    .prologue
    const/4 v6, 0x0

    .line 142
    iget-object v4, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mNextAlarmCheckScheduled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p1, :cond_3

    .line 143
    :cond_0
    const-string v4, "CalendarProvider2"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 144
    const-string v4, "CalendarProvider2"

    const-string v5, "Scheduling check of next Alarm"

    invoke-static {v4, v5}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.android.providers.calendar.intent.CalendarProvider2"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 147
    .local v0, "intent":Landroid/content/Intent;
    const-string v4, "removeAlarms"

    invoke-virtual {v0, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 148
    iget-object v4, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mContext:Landroid/content/Context;

    const/high16 v5, 0x20000000

    invoke-static {v4, v6, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 150
    .local v1, "pending":Landroid/app/PendingIntent;
    if-eqz v1, :cond_2

    .line 152
    invoke-virtual {p0, v1}, Lcom/android/providers/calendar/CalendarAlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 154
    :cond_2
    iget-object v4, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mContext:Landroid/content/Context;

    const/high16 v5, 0x10000000

    invoke-static {v4, v6, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 158
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v6, 0x1388

    add-long v2, v4, v6

    .line 159
    .local v2, "triggerAtTime":J
    const/4 v4, 0x2

    invoke-virtual {p0, v4, v2, v3, v1}, Lcom/android/providers/calendar/CalendarAlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 161
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "pending":Landroid/app/PendingIntent;
    .end local v2    # "triggerAtTime":J
    :cond_3
    return-void
.end method

.method scheduleNextAlarmCheck(J)V
    .locals 9
    .param p1, "triggerTime"    # J

    .prologue
    const/4 v7, 0x0

    .line 227
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.android.providers.calendar.SCHEDULE_ALARM"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 228
    .local v0, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mContext:Landroid/content/Context;

    const-class v5, Lcom/android/providers/calendar/CalendarReceiver;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 229
    iget-object v4, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mContext:Landroid/content/Context;

    const/high16 v5, 0x20000000

    invoke-static {v4, v7, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 231
    .local v1, "pending":Landroid/app/PendingIntent;
    if-eqz v1, :cond_0

    .line 233
    invoke-virtual {p0, v1}, Lcom/android/providers/calendar/CalendarAlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 235
    :cond_0
    iget-object v4, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mContext:Landroid/content/Context;

    const/high16 v5, 0x10000000

    invoke-static {v4, v7, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 238
    const-string v4, "CalendarProvider2"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/android/providers/calendar/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 239
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 240
    .local v2, "time":Landroid/text/format/Time;
    invoke-virtual {v2, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 241
    const-string v4, " %a, %b %d, %Y %I:%M%P"

    invoke-virtual {v2, v4}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 242
    .local v3, "timeStr":Ljava/lang/String;
    const-string v4, "CalendarProvider2"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "scheduleNextAlarmCheck at: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/providers/calendar/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    .end local v2    # "time":Landroid/text/format/Time;
    .end local v3    # "timeStr":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, v7, p1, p2, v1}, Lcom/android/providers/calendar/CalendarAlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 246
    return-void
.end method

.method public set(IJLandroid/app/PendingIntent;)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "triggerAtTime"    # J
    .param p4, "operation"    # Landroid/app/PendingIntent;

    .prologue
    .line 588
    iget-object v0, p0, Lcom/android/providers/calendar/CalendarAlarmManager;->mAlarmManager:Landroid/app/AlarmManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 589
    return-void
.end method
