.class public Lcom/android/i18n/addressinput/AddressWidget;
.super Ljava/lang/Object;
.source "AddressWidget.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/i18n/addressinput/AddressWidget$3;,
        Lcom/android/i18n/addressinput/AddressWidget$Listener;,
        Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;,
        Lcom/android/i18n/addressinput/AddressWidget$UpdateRunnable;,
        Lcom/android/i18n/addressinput/AddressWidget$ZipLabel;
    }
.end annotation


# static fields
.field private static final ADMIN_ERROR_MESSAGES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final ADMIN_LABELS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final SHOW_ALL_FIELDS:Lcom/android/i18n/addressinput/FormOptions;


# instance fields
.field private mAdminLabel:Ljava/lang/String;

.field private mCacheData:Lcom/android/i18n/addressinput/CacheData;

.field private mContext:Landroid/content/Context;

.field private mCurrentRegion:Ljava/lang/String;

.field private mFormController:Lcom/android/i18n/addressinput/FormController;

.field private mFormOptions:Lcom/android/i18n/addressinput/FormOptions;

.field private mFormatInterpreter:Lcom/android/i18n/addressinput/FormatInterpreter;

.field final mHandler:Landroid/os/Handler;

.field private mInflater:Landroid/view/LayoutInflater;

.field private final mInputWidgets:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/android/i18n/addressinput/AddressField;",
            "Lcom/android/i18n/addressinput/AddressUIComponent;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/android/i18n/addressinput/AddressWidget$Listener;

.field private mRootView:Lcom/google/android/finsky/layout/AddressFieldsLayout;

.field private mSavedAddress:Lcom/android/i18n/addressinput/AddressData;

.field private mSavedErrors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/android/i18n/addressinput/AddressField;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mScript:Lcom/android/i18n/addressinput/LookupKey$ScriptType;

.field private mSpinners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSuggestionProvider:Lcom/google/android/finsky/layout/AddressSuggestionProvider;

.field private mVerifier:Lcom/android/i18n/addressinput/StandardAddressVerifier;

.field private mWidgetLocale:Ljava/lang/String;

.field private mZipLabel:Lcom/android/i18n/addressinput/AddressWidget$ZipLabel;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xf

    .line 107
    new-instance v2, Lcom/android/i18n/addressinput/FormOptions$Builder;

    invoke-direct {v2}, Lcom/android/i18n/addressinput/FormOptions$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/android/i18n/addressinput/FormOptions$Builder;->build()Lcom/android/i18n/addressinput/FormOptions;

    move-result-object v2

    sput-object v2, Lcom/android/i18n/addressinput/AddressWidget;->SHOW_ALL_FIELDS:Lcom/android/i18n/addressinput/FormOptions;

    .line 120
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 121
    .local v1, "adminLabelMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v2, "area"

    const v3, 0x7f0c0084

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    const-string v2, "county"

    const v3, 0x7f0c0085

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    const-string v2, "department"

    const v3, 0x7f0c0086

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    const-string v2, "district"

    const v3, 0x7f0c007e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    const-string v2, "do_si"

    const v3, 0x7f0c0087

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    const-string v2, "emirate"

    const v3, 0x7f0c0088

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    const-string v2, "island"

    const v3, 0x7f0c0089

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    const-string v2, "parish"

    const v3, 0x7f0c008a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    const-string v2, "prefecture"

    const v3, 0x7f0c008b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    const-string v2, "province"

    const v3, 0x7f0c008c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    const-string v2, "state"

    const v3, 0x7f0c008d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    sput-object v2, Lcom/android/i18n/addressinput/AddressWidget;->ADMIN_LABELS:Ljava/util/Map;

    .line 134
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 135
    .local v0, "adminErrorMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v2, "area"

    const v3, 0x7f0c0094

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    const-string v2, "county"

    const v3, 0x7f0c0095

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    const-string v2, "department"

    const v3, 0x7f0c0096

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    const-string v2, "district"

    const v3, 0x7f0c0091

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    const-string v2, "do_si"

    const v3, 0x7f0c0097

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    const-string v2, "emirate"

    const v3, 0x7f0c0098

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    const-string v2, "island"

    const v3, 0x7f0c0099

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    const-string v2, "parish"

    const v3, 0x7f0c009a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    const-string v2, "prefecture"

    const v3, 0x7f0c009b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    const-string v2, "province"

    const v3, 0x7f0c009c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    const-string v2, "state"

    const v3, 0x7f0c009d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    sput-object v2, Lcom/android/i18n/addressinput/AddressWidget;->ADMIN_ERROR_MESSAGES:Ljava/util/Map;

    .line 147
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/layout/AddressFieldsLayout;Lcom/android/i18n/addressinput/FormOptions;Lcom/android/i18n/addressinput/ClientCacheManager;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rootView"    # Lcom/google/android/finsky/layout/AddressFieldsLayout;
    .param p3, "formOptions"    # Lcom/android/i18n/addressinput/FormOptions;
    .param p4, "cacheManager"    # Lcom/android/i18n/addressinput/ClientCacheManager;
    .param p5, "region"    # Ljava/lang/String;

    .prologue
    .line 602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    .line 150
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mHandler:Landroid/os/Handler;

    .line 283
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSpinners:Ljava/util/ArrayList;

    .line 604
    if-eqz p5, :cond_0

    invoke-static {p5}, Lcom/android/i18n/addressinput/AddressWidget;->isValidRegionCode(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605
    iput-object p5, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    .line 609
    :goto_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/i18n/addressinput/AddressWidget;->init(Landroid/content/Context;Lcom/google/android/finsky/layout/AddressFieldsLayout;Lcom/android/i18n/addressinput/FormOptions;Lcom/android/i18n/addressinput/ClientCacheManager;)V

    .line 610
    return-void

    .line 607
    :cond_0
    invoke-static {p1}, Lcom/android/i18n/addressinput/AddressWidget;->getDefaultRegionCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/i18n/addressinput/AddressWidget;Lcom/android/i18n/addressinput/AddressField;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/i18n/addressinput/AddressWidget;
    .param p1, "x1"    # Lcom/android/i18n/addressinput/AddressField;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/i18n/addressinput/AddressWidget;->updateInputWidget(Lcom/android/i18n/addressinput/AddressField;)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/i18n/addressinput/AddressWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/i18n/addressinput/AddressWidget;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/i18n/addressinput/AddressWidget;->updateFields()V

    return-void
.end method

.method static synthetic access$600(Lcom/android/i18n/addressinput/AddressWidget;)Lcom/android/i18n/addressinput/AddressWidget$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/android/i18n/addressinput/AddressWidget;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mListener:Lcom/android/i18n/addressinput/AddressWidget$Listener;

    return-object v0
.end method

.method private buildFieldWidgets()V
    .locals 14

    .prologue
    .line 353
    new-instance v12, Lcom/android/i18n/addressinput/AddressData$Builder;

    invoke-direct {v12}, Lcom/android/i18n/addressinput/AddressData$Builder;-><init>()V

    iget-object v13, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v12, v13}, Lcom/android/i18n/addressinput/AddressData$Builder;->setCountry(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/i18n/addressinput/AddressData$Builder;->build()Lcom/android/i18n/addressinput/AddressData;

    move-result-object v4

    .line 354
    .local v4, "data":Lcom/android/i18n/addressinput/AddressData;
    new-instance v12, Lcom/android/i18n/addressinput/LookupKey$Builder;

    sget-object v13, Lcom/android/i18n/addressinput/LookupKey$KeyType;->DATA:Lcom/android/i18n/addressinput/LookupKey$KeyType;

    invoke-direct {v12, v13}, Lcom/android/i18n/addressinput/LookupKey$Builder;-><init>(Lcom/android/i18n/addressinput/LookupKey$KeyType;)V

    invoke-virtual {v12, v4}, Lcom/android/i18n/addressinput/LookupKey$Builder;->setAddressData(Lcom/android/i18n/addressinput/AddressData;)Lcom/android/i18n/addressinput/LookupKey$Builder;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/i18n/addressinput/LookupKey$Builder;->build()Lcom/android/i18n/addressinput/LookupKey;

    move-result-object v5

    .line 355
    .local v5, "key":Lcom/android/i18n/addressinput/LookupKey;
    new-instance v12, Lcom/android/i18n/addressinput/ClientData;

    iget-object v13, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCacheData:Lcom/android/i18n/addressinput/CacheData;

    invoke-direct {v12, v13}, Lcom/android/i18n/addressinput/ClientData;-><init>(Lcom/android/i18n/addressinput/CacheData;)V

    invoke-virtual {v5}, Lcom/android/i18n/addressinput/LookupKey;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/android/i18n/addressinput/ClientData;->getDefaultData(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressVerificationNodeData;

    move-result-object v3

    .line 359
    .local v3, "countryNode":Lcom/android/i18n/addressinput/AddressVerificationNodeData;
    new-instance v2, Lcom/android/i18n/addressinput/AddressUIComponent;

    sget-object v12, Lcom/android/i18n/addressinput/AddressField;->ADMIN_AREA:Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {v2, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;-><init>(Lcom/android/i18n/addressinput/AddressField;)V

    .line 360
    .local v2, "adminAreaUI":Lcom/android/i18n/addressinput/AddressUIComponent;
    invoke-direct {p0, v3}, Lcom/android/i18n/addressinput/AddressWidget;->getAdminAreaFieldName(Lcom/android/i18n/addressinput/AddressVerificationNodeData;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;->setFieldName(Ljava/lang/String;)V

    .line 361
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v13, Lcom/android/i18n/addressinput/AddressField;->ADMIN_AREA:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v12, v13, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    new-instance v6, Lcom/android/i18n/addressinput/AddressUIComponent;

    sget-object v12, Lcom/android/i18n/addressinput/AddressField;->LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {v6, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;-><init>(Lcom/android/i18n/addressinput/AddressField;)V

    .line 365
    .local v6, "localityUI":Lcom/android/i18n/addressinput/AddressUIComponent;
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mContext:Landroid/content/Context;

    const v13, 0x7f0c007d

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;->setFieldName(Ljava/lang/String;)V

    .line 366
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v13, Lcom/android/i18n/addressinput/AddressField;->LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v12, v13, v6}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    new-instance v11, Lcom/android/i18n/addressinput/AddressUIComponent;

    sget-object v12, Lcom/android/i18n/addressinput/AddressField;->DEPENDENT_LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {v11, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;-><init>(Lcom/android/i18n/addressinput/AddressField;)V

    .line 370
    .local v11, "subLocalityUI":Lcom/android/i18n/addressinput/AddressUIComponent;
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mContext:Landroid/content/Context;

    const v13, 0x7f0c007e

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;->setFieldName(Ljava/lang/String;)V

    .line 371
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v13, Lcom/android/i18n/addressinput/AddressField;->DEPENDENT_LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v12, v13, v11}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    new-instance v0, Lcom/android/i18n/addressinput/AddressUIComponent;

    sget-object v12, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_1:Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {v0, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;-><init>(Lcom/android/i18n/addressinput/AddressField;)V

    .line 375
    .local v0, "addressLine1UI":Lcom/android/i18n/addressinput/AddressUIComponent;
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mContext:Landroid/content/Context;

    const v13, 0x7f0c0081

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;->setFieldName(Ljava/lang/String;)V

    .line 376
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v13, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_1:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v12, v13, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v13, Lcom/android/i18n/addressinput/AddressField;->STREET_ADDRESS:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v12, v13, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    new-instance v1, Lcom/android/i18n/addressinput/AddressUIComponent;

    sget-object v12, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_2:Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {v1, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;-><init>(Lcom/android/i18n/addressinput/AddressField;)V

    .line 381
    .local v1, "addressLine2UI":Lcom/android/i18n/addressinput/AddressUIComponent;
    const-string v12, ""

    invoke-virtual {v1, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;->setFieldName(Ljava/lang/String;)V

    .line 382
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v13, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_2:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v12, v13, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    new-instance v7, Lcom/android/i18n/addressinput/AddressUIComponent;

    sget-object v12, Lcom/android/i18n/addressinput/AddressField;->ORGANIZATION:Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {v7, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;-><init>(Lcom/android/i18n/addressinput/AddressField;)V

    .line 386
    .local v7, "organizationUI":Lcom/android/i18n/addressinput/AddressUIComponent;
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mContext:Landroid/content/Context;

    const v13, 0x7f0c007f

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;->setFieldName(Ljava/lang/String;)V

    .line 387
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v13, Lcom/android/i18n/addressinput/AddressField;->ORGANIZATION:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v12, v13, v7}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    new-instance v9, Lcom/android/i18n/addressinput/AddressUIComponent;

    sget-object v12, Lcom/android/i18n/addressinput/AddressField;->RECIPIENT:Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {v9, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;-><init>(Lcom/android/i18n/addressinput/AddressField;)V

    .line 391
    .local v9, "recipientUI":Lcom/android/i18n/addressinput/AddressUIComponent;
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mContext:Landroid/content/Context;

    const v13, 0x7f0c0080

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;->setFieldName(Ljava/lang/String;)V

    .line 392
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v13, Lcom/android/i18n/addressinput/AddressField;->RECIPIENT:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v12, v13, v9}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    new-instance v8, Lcom/android/i18n/addressinput/AddressUIComponent;

    sget-object v12, Lcom/android/i18n/addressinput/AddressField;->POSTAL_CODE:Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {v8, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;-><init>(Lcom/android/i18n/addressinput/AddressField;)V

    .line 396
    .local v8, "postalCodeUI":Lcom/android/i18n/addressinput/AddressUIComponent;
    invoke-direct {p0, v3}, Lcom/android/i18n/addressinput/AddressWidget;->getZipFieldName(Lcom/android/i18n/addressinput/AddressVerificationNodeData;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;->setFieldName(Ljava/lang/String;)V

    .line 397
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v13, Lcom/android/i18n/addressinput/AddressField;->POSTAL_CODE:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v12, v13, v8}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    new-instance v10, Lcom/android/i18n/addressinput/AddressUIComponent;

    sget-object v12, Lcom/android/i18n/addressinput/AddressField;->SORTING_CODE:Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {v10, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;-><init>(Lcom/android/i18n/addressinput/AddressField;)V

    .line 401
    .local v10, "sortingCodeUI":Lcom/android/i18n/addressinput/AddressUIComponent;
    const-string v12, "CEDEX"

    invoke-virtual {v10, v12}, Lcom/android/i18n/addressinput/AddressUIComponent;->setFieldName(Ljava/lang/String;)V

    .line 402
    iget-object v12, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v13, Lcom/android/i18n/addressinput/AddressField;->SORTING_CODE:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v12, v13, v10}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    return-void
.end method

.method private createView(Lcom/google/android/finsky/layout/AddressFieldsLayout;Lcom/android/i18n/addressinput/AddressUIComponent;Ljava/lang/String;ZZ)Landroid/view/View;
    .locals 8
    .param p1, "rootView"    # Lcom/google/android/finsky/layout/AddressFieldsLayout;
    .param p2, "field"    # Lcom/android/i18n/addressinput/AddressUIComponent;
    .param p3, "defaultKey"    # Ljava/lang/String;
    .param p4, "readOnly"    # Z
    .param p5, "isFirstField"    # Z

    .prologue
    const/4 v5, 0x0

    .line 287
    invoke-virtual {p2}, Lcom/android/i18n/addressinput/AddressUIComponent;->getFieldName()Ljava/lang/String;

    move-result-object v1

    .line 289
    .local v1, "fieldText":Ljava/lang/String;
    if-eqz p5, :cond_0

    if-nez p4, :cond_0

    invoke-virtual {p2}, Lcom/android/i18n/addressinput/AddressUIComponent;->getUIType()Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    move-result-object v6

    sget-object v7, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->EDIT:Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    invoke-virtual {v6, v7}, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p2}, Lcom/android/i18n/addressinput/AddressUIComponent;->getId()Lcom/android/i18n/addressinput/AddressField;

    move-result-object v6

    sget-object v7, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_1:Lcom/android/i18n/addressinput/AddressField;

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSuggestionProvider:Lcom/google/android/finsky/layout/AddressSuggestionProvider;

    if-eqz v6, :cond_0

    .line 293
    iget-object v6, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f040017

    invoke-virtual {v6, v7, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/AddressAutoComplete;

    .line 295
    .local v4, "view":Lcom/google/android/finsky/layout/AddressAutoComplete;
    invoke-virtual {p2, v4}, Lcom/android/i18n/addressinput/AddressUIComponent;->setView(Landroid/view/View;)V

    .line 296
    invoke-virtual {v4, v1}, Lcom/google/android/finsky/layout/AddressAutoComplete;->setHint(Ljava/lang/CharSequence;)V

    .line 297
    iget-object v5, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSuggestionProvider:Lcom/google/android/finsky/layout/AddressSuggestionProvider;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/AddressAutoComplete;->setSuggestionProvider(Lcom/google/android/finsky/layout/AddressSuggestionProvider;)V

    .line 330
    .end local v4    # "view":Lcom/google/android/finsky/layout/AddressAutoComplete;
    :goto_0
    return-object v4

    .line 299
    :cond_0
    invoke-virtual {p2}, Lcom/android/i18n/addressinput/AddressUIComponent;->getUIType()Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    move-result-object v6

    sget-object v7, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->EDIT:Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    invoke-virtual {v6, v7}, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 300
    iget-object v6, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f040018

    invoke-virtual {v6, v7, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 301
    .local v4, "view":Landroid/view/View;
    invoke-virtual {p2, v4}, Lcom/android/i18n/addressinput/AddressUIComponent;->setView(Landroid/view/View;)V

    move-object v0, v4

    .line 302
    check-cast v0, Landroid/widget/EditText;

    .line 303
    .local v0, "editText":Landroid/widget/EditText;
    if-nez p4, :cond_1

    const/4 v5, 0x1

    :cond_1
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 304
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 305
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 307
    :cond_2
    invoke-virtual {p2}, Lcom/android/i18n/addressinput/AddressUIComponent;->getId()Lcom/android/i18n/addressinput/AddressField;

    move-result-object v5

    sget-object v6, Lcom/android/i18n/addressinput/AddressField;->POSTAL_CODE:Lcom/android/i18n/addressinput/AddressField;

    if-ne v5, v6, :cond_3

    .line 308
    invoke-virtual {v0}, Landroid/widget/EditText;->getInputType()I

    move-result v5

    or-int/lit16 v5, v5, 0x1000

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setInputType(I)V

    :cond_3
    move-object v4, v0

    .line 311
    goto :goto_0

    .line 312
    .end local v0    # "editText":Landroid/widget/EditText;
    .end local v4    # "view":Landroid/view/View;
    :cond_4
    invoke-virtual {p2}, Lcom/android/i18n/addressinput/AddressUIComponent;->getUIType()Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    move-result-object v6

    sget-object v7, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->SPINNER:Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    invoke-virtual {v6, v7}, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 313
    iget-object v6, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f04001a

    invoke-virtual {v6, v7, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 314
    .restart local v4    # "view":Landroid/view/View;
    invoke-virtual {p2, v4}, Lcom/android/i18n/addressinput/AddressUIComponent;->setView(Landroid/view/View;)V

    move-object v2, v4

    .line 315
    check-cast v2, Landroid/widget/Spinner;

    .line 316
    .local v2, "spinner":Landroid/widget/Spinner;
    new-instance v3, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;

    invoke-virtual {p2}, Lcom/android/i18n/addressinput/AddressUIComponent;->getId()Lcom/android/i18n/addressinput/AddressField;

    move-result-object v5

    invoke-virtual {p2}, Lcom/android/i18n/addressinput/AddressUIComponent;->getParentId()Lcom/android/i18n/addressinput/AddressField;

    move-result-object v6

    invoke-direct {v3, v2, v5, v6}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;-><init>(Landroid/widget/Spinner;Lcom/android/i18n/addressinput/AddressField;Lcom/android/i18n/addressinput/AddressField;)V

    .line 318
    .local v3, "spinnerInfo":Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    iget-object v5, p0, Lcom/android/i18n/addressinput/AddressWidget;->mContext:Landroid/content/Context;

    const v6, 0x1090008

    const v7, 0x1090009

    invoke-virtual {v3, v5, v6, v7}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->initAdapter(Landroid/content/Context;II)V

    .line 320
    # getter for: Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->mAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v3}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->access$100(Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;)Landroid/widget/ArrayAdapter;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 321
    invoke-virtual {p2}, Lcom/android/i18n/addressinput/AddressUIComponent;->getCandidatesList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3, v5, p3}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->setSpinnerList(Ljava/util/List;Ljava/lang/String;)V

    .line 323
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_5

    .line 324
    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 326
    :cond_5
    invoke-virtual {v2, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 327
    iget-object v5, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSpinners:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v4, v2

    .line 328
    goto/16 :goto_0

    .line 330
    .end local v2    # "spinner":Landroid/widget/Spinner;
    .end local v3    # "spinnerInfo":Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    .end local v4    # "view":Landroid/view/View;
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method private findSpinnerByView(Landroid/view/View;)Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 442
    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSpinners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;

    .line 443
    .local v1, "spinnerInfo":Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    # getter for: Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->mView:Landroid/widget/Spinner;
    invoke-static {v1}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->access$200(Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;)Landroid/widget/Spinner;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 447
    .end local v1    # "spinnerInfo":Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getAdminAreaFieldName(Lcom/android/i18n/addressinput/AddressVerificationNodeData;)Ljava/lang/String;
    .locals 4
    .param p1, "countryNode"    # Lcom/android/i18n/addressinput/AddressVerificationNodeData;

    .prologue
    .line 431
    sget-object v2, Lcom/android/i18n/addressinput/AddressDataKey;->STATE_NAME_TYPE:Lcom/android/i18n/addressinput/AddressDataKey;

    invoke-virtual {p1, v2}, Lcom/android/i18n/addressinput/AddressVerificationNodeData;->get(Lcom/android/i18n/addressinput/AddressDataKey;)Ljava/lang/String;

    move-result-object v0

    .line 432
    .local v0, "adminLabelType":Ljava/lang/String;
    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mAdminLabel:Ljava/lang/String;

    .line 433
    sget-object v2, Lcom/android/i18n/addressinput/AddressWidget;->ADMIN_LABELS:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 434
    .local v1, "result":Ljava/lang/Integer;
    if-nez v1, :cond_0

    .line 436
    const v2, 0x7f0c008c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 438
    :cond_0
    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static getDefaultRegionCode(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 979
    const-string v0, "US"

    .line 982
    .local v0, "defaultRegionCode":Ljava/lang/String;
    if-nez p0, :cond_1

    .line 994
    .end local v0    # "defaultRegionCode":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 987
    .restart local v0    # "defaultRegionCode":Ljava/lang/String;
    :cond_1
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 991
    .local v1, "region":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move-object v0, v1

    .line 992
    goto :goto_0
.end method

.method private getErrorMessageIdForInvalidEntryIn(Lcom/android/i18n/addressinput/AddressField;)I
    .locals 2
    .param p1, "field"    # Lcom/android/i18n/addressinput/AddressField;

    .prologue
    .line 856
    sget-object v0, Lcom/android/i18n/addressinput/AddressWidget$3;->$SwitchMap$com$android$i18n$addressinput$AddressField:[I

    invoke-virtual {p1}, Lcom/android/i18n/addressinput/AddressField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 868
    const v0, 0x7f0c008f

    :goto_0
    return v0

    .line 858
    :pswitch_0
    sget-object v0, Lcom/android/i18n/addressinput/AddressWidget;->ADMIN_ERROR_MESSAGES:Ljava/util/Map;

    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mAdminLabel:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 860
    :pswitch_1
    const v0, 0x7f0c0090

    goto :goto_0

    .line 862
    :pswitch_2
    const v0, 0x7f0c0091

    goto :goto_0

    .line 864
    :pswitch_3
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mZipLabel:Lcom/android/i18n/addressinput/AddressWidget$ZipLabel;

    sget-object v1, Lcom/android/i18n/addressinput/AddressWidget$ZipLabel;->POSTAL:Lcom/android/i18n/addressinput/AddressWidget$ZipLabel;

    if-ne v0, v1, :cond_0

    const v0, 0x7f0c0092

    goto :goto_0

    :cond_0
    const v0, 0x7f0c0093

    goto :goto_0

    .line 856
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getFullEnvelopeAddress(Lcom/android/i18n/addressinput/AddressData;Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .param p0, "address"    # Lcom/android/i18n/addressinput/AddressData;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/i18n/addressinput/AddressData;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 798
    new-instance v0, Lcom/android/i18n/addressinput/FormatInterpreter;

    sget-object v1, Lcom/android/i18n/addressinput/AddressWidget;->SHOW_ALL_FIELDS:Lcom/android/i18n/addressinput/FormOptions;

    invoke-direct {v0, v1}, Lcom/android/i18n/addressinput/FormatInterpreter;-><init>(Lcom/android/i18n/addressinput/FormOptions;)V

    invoke-static {p1}, Lcom/android/i18n/addressinput/AddressWidget;->getDefaultRegionCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/android/i18n/addressinput/FormatInterpreter;->getEnvelopeAddress(Lcom/android/i18n/addressinput/AddressData;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private getRegionData(Lcom/android/i18n/addressinput/AddressField;)Ljava/util/List;
    .locals 6
    .param p1, "parentField"    # Lcom/android/i18n/addressinput/AddressField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/i18n/addressinput/AddressField;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/android/i18n/addressinput/RegionData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 569
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressWidget;->getAddressData()Lcom/android/i18n/addressinput/AddressData;

    move-result-object v0

    .line 574
    .local v0, "address":Lcom/android/i18n/addressinput/AddressData;
    iget-object v3, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormController:Lcom/android/i18n/addressinput/FormController;

    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressData;->getLanguageCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/i18n/addressinput/FormController;->isDefaultLanguage(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 575
    new-instance v3, Lcom/android/i18n/addressinput/AddressData$Builder;

    invoke-direct {v3, v0}, Lcom/android/i18n/addressinput/AddressData$Builder;-><init>(Lcom/android/i18n/addressinput/AddressData;)V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/i18n/addressinput/AddressData$Builder;->setLanguageCode(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/i18n/addressinput/AddressData$Builder;->build()Lcom/android/i18n/addressinput/AddressData;

    move-result-object v0

    .line 578
    :cond_0
    iget-object v3, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormController:Lcom/android/i18n/addressinput/FormController;

    invoke-virtual {v3, v0}, Lcom/android/i18n/addressinput/FormController;->getDataKeyFor(Lcom/android/i18n/addressinput/AddressData;)Lcom/android/i18n/addressinput/LookupKey;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/android/i18n/addressinput/LookupKey;->getKeyForUpperLevelField(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/LookupKey;

    move-result-object v2

    .line 582
    .local v2, "parentKey":Lcom/android/i18n/addressinput/LookupKey;
    if-nez v2, :cond_1

    .line 583
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t build key with parent field "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". One of"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " the ancestor fields might be empty"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    new-instance v1, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 593
    .local v1, "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/android/i18n/addressinput/RegionData;>;"
    :goto_0
    return-object v1

    .line 591
    .end local v1    # "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/android/i18n/addressinput/RegionData;>;"
    :cond_1
    iget-object v3, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormController:Lcom/android/i18n/addressinput/FormController;

    invoke-virtual {v3, v2}, Lcom/android/i18n/addressinput/FormController;->getRegionData(Lcom/android/i18n/addressinput/LookupKey;)Ljava/util/List;

    move-result-object v1

    .restart local v1    # "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/android/i18n/addressinput/RegionData;>;"
    goto :goto_0
.end method

.method private getViewErrors()Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/android/i18n/addressinput/AddressField;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 671
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 672
    .local v0, "errors":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/android/i18n/addressinput/AddressField;Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormatInterpreter:Lcom/android/i18n/addressinput/FormatInterpreter;

    iget-object v7, p0, Lcom/android/i18n/addressinput/AddressWidget;->mScript:Lcom/android/i18n/addressinput/LookupKey$ScriptType;

    iget-object v8, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/android/i18n/addressinput/FormatInterpreter;->getAddressFieldOrder(Lcom/android/i18n/addressinput/LookupKey$ScriptType;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/i18n/addressinput/AddressField;

    .line 674
    .local v1, "field":Lcom/android/i18n/addressinput/AddressField;
    iget-object v6, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    invoke-virtual {v6, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/i18n/addressinput/AddressUIComponent;

    .line 675
    .local v4, "uiComponent":Lcom/android/i18n/addressinput/AddressUIComponent;
    if-eqz v4, :cond_0

    .line 678
    invoke-virtual {v4}, Lcom/android/i18n/addressinput/AddressUIComponent;->getView()Landroid/view/View;

    move-result-object v5

    .line 679
    .local v5, "view":Landroid/view/View;
    if-eqz v5, :cond_0

    .line 682
    invoke-virtual {v4}, Lcom/android/i18n/addressinput/AddressUIComponent;->getUIType()Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    move-result-object v6

    sget-object v7, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->EDIT:Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    if-ne v6, v7, :cond_0

    .line 683
    check-cast v5, Landroid/widget/EditText;

    .end local v5    # "view":Landroid/view/View;
    invoke-virtual {v5}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v3

    .line 684
    .local v3, "text":Ljava/lang/CharSequence;
    if-eqz v3, :cond_0

    .line 685
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 689
    .end local v1    # "field":Lcom/android/i18n/addressinput/AddressField;
    .end local v3    # "text":Ljava/lang/CharSequence;
    .end local v4    # "uiComponent":Lcom/android/i18n/addressinput/AddressUIComponent;
    :cond_1
    return-object v0
.end method

.method private getZipFieldName(Lcom/android/i18n/addressinput/AddressVerificationNodeData;)Ljava/lang/String;
    .locals 4
    .param p1, "countryNode"    # Lcom/android/i18n/addressinput/AddressVerificationNodeData;

    .prologue
    .line 419
    sget-object v2, Lcom/android/i18n/addressinput/AddressDataKey;->ZIP_NAME_TYPE:Lcom/android/i18n/addressinput/AddressDataKey;

    invoke-virtual {p1, v2}, Lcom/android/i18n/addressinput/AddressVerificationNodeData;->get(Lcom/android/i18n/addressinput/AddressDataKey;)Ljava/lang/String;

    move-result-object v1

    .line 420
    .local v1, "zipType":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 421
    sget-object v2, Lcom/android/i18n/addressinput/AddressWidget$ZipLabel;->POSTAL:Lcom/android/i18n/addressinput/AddressWidget$ZipLabel;

    iput-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mZipLabel:Lcom/android/i18n/addressinput/AddressWidget$ZipLabel;

    .line 422
    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0082

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 427
    .local v0, "zipName":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 424
    .end local v0    # "zipName":Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/android/i18n/addressinput/AddressWidget$ZipLabel;->ZIP:Lcom/android/i18n/addressinput/AddressWidget$ZipLabel;

    iput-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mZipLabel:Lcom/android/i18n/addressinput/AddressWidget$ZipLabel;

    .line 425
    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0083

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "zipName":Ljava/lang/String;
    goto :goto_0
.end method

.method private init(Landroid/content/Context;Lcom/google/android/finsky/layout/AddressFieldsLayout;Lcom/android/i18n/addressinput/FormOptions;Lcom/android/i18n/addressinput/ClientCacheManager;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rootView"    # Lcom/google/android/finsky/layout/AddressFieldsLayout;
    .param p3, "formOptions"    # Lcom/android/i18n/addressinput/FormOptions;
    .param p4, "cacheManager"    # Lcom/android/i18n/addressinput/ClientCacheManager;

    .prologue
    .line 714
    iput-object p1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mContext:Landroid/content/Context;

    .line 715
    iput-object p2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mRootView:Lcom/google/android/finsky/layout/AddressFieldsLayout;

    .line 716
    iput-object p3, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormOptions:Lcom/android/i18n/addressinput/FormOptions;

    .line 717
    new-instance v0, Lcom/android/i18n/addressinput/CacheData;

    invoke-direct {v0, p4}, Lcom/android/i18n/addressinput/CacheData;-><init>(Lcom/android/i18n/addressinput/ClientCacheManager;)V

    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCacheData:Lcom/android/i18n/addressinput/CacheData;

    .line 718
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInflater:Landroid/view/LayoutInflater;

    .line 719
    new-instance v0, Lcom/android/i18n/addressinput/FormController;

    new-instance v1, Lcom/android/i18n/addressinput/ClientData;

    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCacheData:Lcom/android/i18n/addressinput/CacheData;

    invoke-direct {v1, v2}, Lcom/android/i18n/addressinput/ClientData;-><init>(Lcom/android/i18n/addressinput/CacheData;)V

    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mWidgetLocale:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/i18n/addressinput/FormController;-><init>(Lcom/android/i18n/addressinput/ClientData;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormController:Lcom/android/i18n/addressinput/FormController;

    .line 722
    new-instance v0, Lcom/android/i18n/addressinput/FormatInterpreter;

    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormOptions:Lcom/android/i18n/addressinput/FormOptions;

    invoke-direct {v0, v1}, Lcom/android/i18n/addressinput/FormatInterpreter;-><init>(Lcom/android/i18n/addressinput/FormOptions;)V

    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormatInterpreter:Lcom/android/i18n/addressinput/FormatInterpreter;

    .line 723
    new-instance v0, Lcom/android/i18n/addressinput/StandardAddressVerifier;

    new-instance v1, Lcom/android/i18n/addressinput/FieldVerifier;

    new-instance v2, Lcom/android/i18n/addressinput/ClientData;

    iget-object v3, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCacheData:Lcom/android/i18n/addressinput/CacheData;

    invoke-direct {v2, v3}, Lcom/android/i18n/addressinput/ClientData;-><init>(Lcom/android/i18n/addressinput/CacheData;)V

    invoke-direct {v1, v2}, Lcom/android/i18n/addressinput/FieldVerifier;-><init>(Lcom/android/i18n/addressinput/DataSource;)V

    invoke-direct {v0, v1}, Lcom/android/i18n/addressinput/StandardAddressVerifier;-><init>(Lcom/android/i18n/addressinput/FieldVerifier;)V

    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mVerifier:Lcom/android/i18n/addressinput/StandardAddressVerifier;

    .line 725
    return-void
.end method

.method private initializeDropDowns()V
    .locals 6

    .prologue
    .line 406
    iget-object v4, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v5, Lcom/android/i18n/addressinput/AddressField;->ADMIN_AREA:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v4, v5}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/i18n/addressinput/AddressUIComponent;

    .line 407
    .local v1, "adminAreaUI":Lcom/android/i18n/addressinput/AddressUIComponent;
    sget-object v4, Lcom/android/i18n/addressinput/AddressField;->COUNTRY:Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {p0, v4}, Lcom/android/i18n/addressinput/AddressWidget;->getRegionData(Lcom/android/i18n/addressinput/AddressField;)Ljava/util/List;

    move-result-object v0

    .line 408
    .local v0, "adminAreaList":Ljava/util/List;, "Ljava/util/List<Lcom/android/i18n/addressinput/RegionData;>;"
    invoke-virtual {v1, v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->initializeCandidatesList(Ljava/util/List;)V

    .line 410
    iget-object v4, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    sget-object v5, Lcom/android/i18n/addressinput/AddressField;->LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v4, v5}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/i18n/addressinput/AddressUIComponent;

    .line 411
    .local v3, "localityUI":Lcom/android/i18n/addressinput/AddressUIComponent;
    sget-object v4, Lcom/android/i18n/addressinput/AddressField;->ADMIN_AREA:Lcom/android/i18n/addressinput/AddressField;

    invoke-direct {p0, v4}, Lcom/android/i18n/addressinput/AddressWidget;->getRegionData(Lcom/android/i18n/addressinput/AddressField;)Ljava/util/List;

    move-result-object v2

    .line 412
    .local v2, "localityList":Ljava/util/List;, "Ljava/util/List<Lcom/android/i18n/addressinput/RegionData;>;"
    invoke-virtual {v3, v2}, Lcom/android/i18n/addressinput/AddressUIComponent;->initializeCandidatesList(Ljava/util/List;)V

    .line 413
    return-void
.end method

.method private initializeFieldsWithAddress(Lcom/android/i18n/addressinput/AddressData;Z)V
    .locals 9
    .param p1, "savedAddress"    # Lcom/android/i18n/addressinput/AddressData;
    .param p2, "exactAddress"    # Z

    .prologue
    .line 633
    iget-object v6, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormatInterpreter:Lcom/android/i18n/addressinput/FormatInterpreter;

    iget-object v7, p0, Lcom/android/i18n/addressinput/AddressWidget;->mScript:Lcom/android/i18n/addressinput/LookupKey$ScriptType;

    iget-object v8, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/android/i18n/addressinput/FormatInterpreter;->getAddressFieldOrder(Lcom/android/i18n/addressinput/LookupKey$ScriptType;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/i18n/addressinput/AddressField;

    .line 635
    .local v0, "field":Lcom/android/i18n/addressinput/AddressField;
    iget-object v6, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    invoke-virtual {v6, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/i18n/addressinput/AddressUIComponent;

    .line 636
    .local v3, "uiComponent":Lcom/android/i18n/addressinput/AddressUIComponent;
    if-eqz v3, :cond_0

    .line 639
    invoke-virtual {p1, v0}, Lcom/android/i18n/addressinput/AddressData;->getFieldValue(Lcom/android/i18n/addressinput/AddressField;)Ljava/lang/String;

    move-result-object v4

    .line 640
    .local v4, "value":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 641
    const-string v4, ""

    .line 643
    :cond_1
    invoke-virtual {v3}, Lcom/android/i18n/addressinput/AddressUIComponent;->getView()Landroid/view/View;

    move-result-object v5

    .line 644
    .local v5, "view":Landroid/view/View;
    if-eqz v5, :cond_0

    .line 647
    invoke-virtual {v3}, Lcom/android/i18n/addressinput/AddressUIComponent;->getUIType()Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    move-result-object v6

    sget-object v7, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->SPINNER:Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    if-ne v6, v7, :cond_3

    .line 648
    invoke-direct {p0, v5}, Lcom/android/i18n/addressinput/AddressWidget;->findSpinnerByView(Landroid/view/View;)Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;

    move-result-object v2

    .line 649
    .local v2, "spinnerInfo":Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    if-eqz v2, :cond_0

    .line 652
    if-eqz p2, :cond_2

    .line 653
    invoke-virtual {v2, v4}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->setSelectionByKey(Ljava/lang/String;)V

    goto :goto_0

    .line 655
    :cond_2
    invoke-virtual {v2, v4}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->setSelectionByKeyOrDisplayName(Ljava/lang/String;)V

    goto :goto_0

    .line 658
    .end local v2    # "spinnerInfo":Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    :cond_3
    check-cast v5, Landroid/widget/EditText;

    .end local v5    # "view":Landroid/view/View;
    invoke-virtual {v5, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 661
    .end local v0    # "field":Lcom/android/i18n/addressinput/AddressField;
    .end local v3    # "uiComponent":Lcom/android/i18n/addressinput/AddressUIComponent;
    .end local v4    # "value":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method public static isValidRegionCode(Ljava/lang/String;)Z
    .locals 1
    .param p0, "regionCode"    # Ljava/lang/String;

    .prologue
    .line 969
    invoke-static {}, Lcom/android/i18n/addressinput/RegionDataConstants;->getCountryFormatMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private layoutAddressFields()V
    .locals 10

    .prologue
    .line 464
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 465
    .local v7, "fields":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v5, 0x1

    .line 466
    .local v5, "first":Z
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormatInterpreter:Lcom/android/i18n/addressinput/FormatInterpreter;

    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mScript:Lcom/android/i18n/addressinput/LookupKey$ScriptType;

    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/i18n/addressinput/FormatInterpreter;->getAddressFieldOrder(Lcom/android/i18n/addressinput/LookupKey$ScriptType;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/i18n/addressinput/AddressField;

    .line 468
    .local v6, "field":Lcom/android/i18n/addressinput/AddressField;
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormOptions:Lcom/android/i18n/addressinput/FormOptions;

    invoke-virtual {v0, v6}, Lcom/android/i18n/addressinput/FormOptions;->isHidden(Lcom/android/i18n/addressinput/AddressField;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mRootView:Lcom/google/android/finsky/layout/AddressFieldsLayout;

    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    invoke-virtual {v0, v6}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/i18n/addressinput/AddressUIComponent;

    const-string v3, ""

    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormOptions:Lcom/android/i18n/addressinput/FormOptions;

    invoke-virtual {v0, v6}, Lcom/android/i18n/addressinput/FormOptions;->isReadonly(Lcom/android/i18n/addressinput/AddressField;)Z

    move-result v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/i18n/addressinput/AddressWidget;->createView(Lcom/google/android/finsky/layout/AddressFieldsLayout;Lcom/android/i18n/addressinput/AddressUIComponent;Ljava/lang/String;ZZ)Landroid/view/View;

    move-result-object v9

    .line 471
    .local v9, "v":Landroid/view/View;
    if-eqz v9, :cond_0

    .line 472
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    const/4 v5, 0x0

    goto :goto_0

    .line 477
    .end local v6    # "field":Lcom/android/i18n/addressinput/AddressField;
    .end local v9    # "v":Landroid/view/View;
    :cond_1
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mRootView:Lcom/google/android/finsky/layout/AddressFieldsLayout;

    invoke-virtual {v0, v7}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->setFields(Ljava/util/List;)V

    .line 478
    return-void
.end method

.method private setViewErrors(Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/android/i18n/addressinput/AddressField;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 693
    .local p1, "errors":Ljava/util/Map;, "Ljava/util/Map<Lcom/android/i18n/addressinput/AddressField;Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/i18n/addressinput/AddressField;

    .line 694
    .local v2, "field":Lcom/android/i18n/addressinput/AddressField;
    iget-object v6, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    invoke-virtual {v6, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/i18n/addressinput/AddressUIComponent;

    .line 695
    .local v4, "uiComponent":Lcom/android/i18n/addressinput/AddressUIComponent;
    if-eqz v4, :cond_0

    .line 698
    invoke-virtual {v4}, Lcom/android/i18n/addressinput/AddressUIComponent;->getView()Landroid/view/View;

    move-result-object v5

    .line 699
    .local v5, "view":Landroid/view/View;
    if-eqz v5, :cond_0

    .line 702
    invoke-virtual {v4}, Lcom/android/i18n/addressinput/AddressUIComponent;->getUIType()Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    move-result-object v6

    sget-object v7, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->EDIT:Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    if-ne v6, v7, :cond_0

    .line 703
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 704
    .local v1, "errorText":Ljava/lang/String;
    if-eqz v1, :cond_0

    move-object v0, v5

    .line 705
    check-cast v0, Landroid/widget/EditText;

    .line 706
    .local v0, "editView":Landroid/widget/EditText;
    invoke-virtual {v4}, Lcom/android/i18n/addressinput/AddressUIComponent;->getFieldName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6, v1}, Lcom/google/android/finsky/utils/UiUtils;->setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 710
    .end local v0    # "editView":Landroid/widget/EditText;
    .end local v1    # "errorText":Ljava/lang/String;
    .end local v2    # "field":Lcom/android/i18n/addressinput/AddressField;
    .end local v4    # "uiComponent":Lcom/android/i18n/addressinput/AddressUIComponent;
    .end local v5    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private setWidgetLocaleAndScript()V
    .locals 2

    .prologue
    .line 561
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/i18n/addressinput/Util;->getWidgetCompatibleLanguageCode(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mWidgetLocale:Ljava/lang/String;

    .line 562
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormController:Lcom/android/i18n/addressinput/FormController;

    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mWidgetLocale:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/FormController;->setLanguageCode(Ljava/lang/String;)V

    .line 563
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mWidgetLocale:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/i18n/addressinput/Util;->isExplicitLatinScript(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/i18n/addressinput/LookupKey$ScriptType;->LATIN:Lcom/android/i18n/addressinput/LookupKey$ScriptType;

    :goto_0
    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mScript:Lcom/android/i18n/addressinput/LookupKey$ScriptType;

    .line 566
    return-void

    .line 563
    :cond_0
    sget-object v0, Lcom/android/i18n/addressinput/LookupKey$ScriptType;->LOCAL:Lcom/android/i18n/addressinput/LookupKey$ScriptType;

    goto :goto_0
.end method

.method private updateChildNodes(Landroid/widget/AdapterView;I)V
    .locals 5
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .line 481
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-direct {p0, p1}, Lcom/android/i18n/addressinput/AddressWidget;->findSpinnerByView(Landroid/view/View;)Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;

    move-result-object v1

    .line 482
    .local v1, "spinnerInfo":Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    if-nez v1, :cond_1

    .line 506
    :cond_0
    :goto_0
    return-void

    .line 487
    :cond_1
    # getter for: Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->mId:Lcom/android/i18n/addressinput/AddressField;
    invoke-static {v1}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->access$300(Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;)Lcom/android/i18n/addressinput/AddressField;

    move-result-object v0

    .line 488
    .local v0, "myId":Lcom/android/i18n/addressinput/AddressField;
    sget-object v2, Lcom/android/i18n/addressinput/AddressField;->ADMIN_AREA:Lcom/android/i18n/addressinput/AddressField;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/android/i18n/addressinput/AddressField;->LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    if-ne v0, v2, :cond_0

    .line 495
    :cond_2
    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormController:Lcom/android/i18n/addressinput/FormController;

    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressWidget;->getAddressData()Lcom/android/i18n/addressinput/AddressData;

    move-result-object v3

    new-instance v4, Lcom/android/i18n/addressinput/AddressWidget$1;

    invoke-direct {v4, p0, v0}, Lcom/android/i18n/addressinput/AddressWidget$1;-><init>(Lcom/android/i18n/addressinput/AddressWidget;Lcom/android/i18n/addressinput/AddressField;)V

    invoke-virtual {v2, v3, v4}, Lcom/android/i18n/addressinput/FormController;->requestDataForAddress(Lcom/android/i18n/addressinput/AddressData;Lcom/android/i18n/addressinput/DataLoadListener;)V

    goto :goto_0
.end method

.method private updateFields()V
    .locals 2

    .prologue
    .line 451
    invoke-direct {p0}, Lcom/android/i18n/addressinput/AddressWidget;->buildFieldWidgets()V

    .line 452
    invoke-direct {p0}, Lcom/android/i18n/addressinput/AddressWidget;->initializeDropDowns()V

    .line 453
    invoke-direct {p0}, Lcom/android/i18n/addressinput/AddressWidget;->layoutAddressFields()V

    .line 454
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSavedAddress:Lcom/android/i18n/addressinput/AddressData;

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSavedAddress:Lcom/android/i18n/addressinput/AddressData;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/i18n/addressinput/AddressWidget;->initializeFieldsWithAddress(Lcom/android/i18n/addressinput/AddressData;Z)V

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSavedErrors:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 458
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSavedErrors:Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/android/i18n/addressinput/AddressWidget;->setViewErrors(Ljava/util/Map;)V

    .line 460
    :cond_1
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mRootView:Lcom/google/android/finsky/layout/AddressFieldsLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->showFields()V

    .line 461
    return-void
.end method

.method private updateInputWidget(Lcom/android/i18n/addressinput/AddressField;)V
    .locals 4
    .param p1, "myId"    # Lcom/android/i18n/addressinput/AddressField;

    .prologue
    .line 521
    iget-object v3, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSpinners:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;

    .line 522
    .local v1, "child":Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    # getter for: Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->mParentId:Lcom/android/i18n/addressinput/AddressField;
    invoke-static {v1}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->access$400(Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;)Lcom/android/i18n/addressinput/AddressField;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 523
    # getter for: Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->mParentId:Lcom/android/i18n/addressinput/AddressField;
    invoke-static {v1}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->access$400(Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;)Lcom/android/i18n/addressinput/AddressField;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/i18n/addressinput/AddressWidget;->getRegionData(Lcom/android/i18n/addressinput/AddressField;)Ljava/util/List;

    move-result-object v0

    .line 524
    .local v0, "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/android/i18n/addressinput/RegionData;>;"
    const-string v3, ""

    invoke-virtual {v1, v0, v3}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->setSpinnerList(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    .line 527
    .end local v0    # "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/android/i18n/addressinput/RegionData;>;"
    .end local v1    # "child":Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    :cond_1
    return-void
.end method


# virtual methods
.method public clearErrorMessage()V
    .locals 7

    .prologue
    .line 876
    iget-object v4, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormatInterpreter:Lcom/android/i18n/addressinput/FormatInterpreter;

    iget-object v5, p0, Lcom/android/i18n/addressinput/AddressWidget;->mScript:Lcom/android/i18n/addressinput/LookupKey$ScriptType;

    iget-object v6, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/android/i18n/addressinput/FormatInterpreter;->getAddressFieldOrder(Lcom/android/i18n/addressinput/LookupKey$ScriptType;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/i18n/addressinput/AddressField;

    .line 878
    .local v1, "field":Lcom/android/i18n/addressinput/AddressField;
    iget-object v4, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    invoke-virtual {v4, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/i18n/addressinput/AddressUIComponent;

    .line 880
    .local v0, "addressUIComponent":Lcom/android/i18n/addressinput/AddressUIComponent;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->getUIType()Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    move-result-object v4

    sget-object v5, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->EDIT:Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    if-ne v4, v5, :cond_0

    .line 881
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->getView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 882
    .local v3, "view":Landroid/widget/EditText;
    if-eqz v3, :cond_0

    .line 883
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 887
    .end local v0    # "addressUIComponent":Lcom/android/i18n/addressinput/AddressUIComponent;
    .end local v1    # "field":Lcom/android/i18n/addressinput/AddressField;
    .end local v3    # "view":Landroid/widget/EditText;
    :cond_1
    return-void
.end method

.method public displayErrorMessageForInvalidEntryIn(Lcom/android/i18n/addressinput/AddressField;)Landroid/widget/TextView;
    .locals 5
    .param p1, "field"    # Lcom/android/i18n/addressinput/AddressField;

    .prologue
    .line 844
    iget-object v3, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    invoke-virtual {v3, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/i18n/addressinput/AddressUIComponent;

    .line 845
    .local v0, "addressUIComponent":Lcom/android/i18n/addressinput/AddressUIComponent;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->getUIType()Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    move-result-object v3

    sget-object v4, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->EDIT:Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    if-ne v3, v4, :cond_0

    .line 846
    invoke-direct {p0, p1}, Lcom/android/i18n/addressinput/AddressWidget;->getErrorMessageIdForInvalidEntryIn(Lcom/android/i18n/addressinput/AddressField;)I

    move-result v1

    .line 847
    .local v1, "errorMessageId":I
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 848
    .local v2, "view":Landroid/widget/EditText;
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->getFieldName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/i18n/addressinput/AddressWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/utils/UiUtils;->setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    .end local v1    # "errorMessageId":I
    .end local v2    # "view":Landroid/widget/EditText;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getAddressData()Lcom/android/i18n/addressinput/AddressData;
    .locals 10

    .prologue
    .line 747
    new-instance v1, Lcom/android/i18n/addressinput/AddressData$Builder;

    invoke-direct {v1}, Lcom/android/i18n/addressinput/AddressData$Builder;-><init>()V

    .line 748
    .local v1, "builder":Lcom/android/i18n/addressinput/AddressData$Builder;
    iget-object v7, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v1, v7}, Lcom/android/i18n/addressinput/AddressData$Builder;->setCountry(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 749
    iget-object v7, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormatInterpreter:Lcom/android/i18n/addressinput/FormatInterpreter;

    iget-object v8, p0, Lcom/android/i18n/addressinput/AddressWidget;->mScript:Lcom/android/i18n/addressinput/LookupKey$ScriptType;

    iget-object v9, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Lcom/android/i18n/addressinput/FormatInterpreter;->getAddressFieldOrder(Lcom/android/i18n/addressinput/LookupKey$ScriptType;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/i18n/addressinput/AddressField;

    .line 751
    .local v2, "field":Lcom/android/i18n/addressinput/AddressField;
    iget-object v7, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    invoke-virtual {v7, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/i18n/addressinput/AddressUIComponent;

    .line 752
    .local v0, "addressUIComponent":Lcom/android/i18n/addressinput/AddressUIComponent;
    if-eqz v0, :cond_0

    .line 755
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 756
    .local v5, "value":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->getUIType()Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    move-result-object v7

    sget-object v8, Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;->SPINNER:Lcom/android/i18n/addressinput/AddressUIComponent$UIComponent;

    if-ne v7, v8, :cond_1

    .line 758
    invoke-virtual {p0, v2}, Lcom/android/i18n/addressinput/AddressWidget;->getViewForField(Lcom/android/i18n/addressinput/AddressField;)Landroid/view/View;

    move-result-object v6

    .line 759
    .local v6, "view":Landroid/view/View;
    invoke-direct {p0, v6}, Lcom/android/i18n/addressinput/AddressWidget;->findSpinnerByView(Landroid/view/View;)Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;

    move-result-object v4

    .line 760
    .local v4, "spinnerInfo":Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    if-eqz v4, :cond_1

    .line 761
    invoke-virtual {v4, v5}, Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;->getRegionDataKeyForValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 764
    .end local v4    # "spinnerInfo":Lcom/android/i18n/addressinput/AddressWidget$AddressSpinnerInfo;
    .end local v6    # "view":Landroid/view/View;
    :cond_1
    invoke-virtual {v1, v2, v5}, Lcom/android/i18n/addressinput/AddressData$Builder;->set(Lcom/android/i18n/addressinput/AddressField;Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    goto :goto_0

    .line 766
    .end local v0    # "addressUIComponent":Lcom/android/i18n/addressinput/AddressUIComponent;
    .end local v2    # "field":Lcom/android/i18n/addressinput/AddressField;
    .end local v5    # "value":Ljava/lang/String;
    :cond_2
    iget-object v7, p0, Lcom/android/i18n/addressinput/AddressWidget;->mWidgetLocale:Ljava/lang/String;

    invoke-virtual {v1, v7}, Lcom/android/i18n/addressinput/AddressData$Builder;->setLanguageCode(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 767
    invoke-virtual {v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->build()Lcom/android/i18n/addressinput/AddressData;

    move-result-object v7

    return-object v7
.end method

.method public getAddressProblems()Lcom/android/i18n/addressinput/AddressProblems;
    .locals 4

    .prologue
    .line 806
    new-instance v1, Lcom/android/i18n/addressinput/AddressProblems;

    invoke-direct {v1}, Lcom/android/i18n/addressinput/AddressProblems;-><init>()V

    .line 807
    .local v1, "problems":Lcom/android/i18n/addressinput/AddressProblems;
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressWidget;->getAddressData()Lcom/android/i18n/addressinput/AddressData;

    move-result-object v0

    .line 808
    .local v0, "addressData":Lcom/android/i18n/addressinput/AddressData;
    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mVerifier:Lcom/android/i18n/addressinput/StandardAddressVerifier;

    invoke-virtual {v2, v0, v1}, Lcom/android/i18n/addressinput/StandardAddressVerifier;->verify(Lcom/android/i18n/addressinput/AddressData;Lcom/android/i18n/addressinput/AddressProblems;)V

    .line 812
    invoke-virtual {v1}, Lcom/android/i18n/addressinput/AddressProblems;->getProblems()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormOptions:Lcom/android/i18n/addressinput/FormOptions;

    invoke-virtual {v3}, Lcom/android/i18n/addressinput/FormOptions;->getHiddenFields()Ljava/util/EnumSet;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 817
    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormOptions:Lcom/android/i18n/addressinput/FormOptions;

    sget-object v3, Lcom/android/i18n/addressinput/AddressField;->ADMIN_AREA:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v2, v3}, Lcom/android/i18n/addressinput/FormOptions;->isHidden(Lcom/android/i18n/addressinput/AddressField;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/i18n/addressinput/AddressField;->POSTAL_CODE:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/AddressProblems;->getProblem(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/AddressProblemType;

    move-result-object v2

    sget-object v3, Lcom/android/i18n/addressinput/AddressProblemType;->MISSING_REQUIRED_FIELD:Lcom/android/i18n/addressinput/AddressProblemType;

    if-eq v2, v3, :cond_0

    .line 820
    invoke-virtual {v1}, Lcom/android/i18n/addressinput/AddressProblems;->getProblems()Ljava/util/Map;

    move-result-object v2

    sget-object v3, Lcom/android/i18n/addressinput/AddressField;->POSTAL_CODE:Lcom/android/i18n/addressinput/AddressField;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 822
    :cond_0
    return-object v1
.end method

.method public getNameForField(Lcom/android/i18n/addressinput/AddressField;)Ljava/lang/String;
    .locals 2
    .param p1, "field"    # Lcom/android/i18n/addressinput/AddressField;

    .prologue
    .line 898
    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    invoke-virtual {v1, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/i18n/addressinput/AddressUIComponent;

    .line 899
    .local v0, "component":Lcom/android/i18n/addressinput/AddressUIComponent;
    if-nez v0, :cond_0

    .line 900
    const/4 v1, 0x0

    .line 902
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->getFieldName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getViewForField(Lcom/android/i18n/addressinput/AddressField;)Landroid/view/View;
    .locals 2
    .param p1, "field"    # Lcom/android/i18n/addressinput/AddressField;

    .prologue
    .line 890
    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    invoke-virtual {v1, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/i18n/addressinput/AddressUIComponent;

    .line 891
    .local v0, "component":Lcom/android/i18n/addressinput/AddressUIComponent;
    if-nez v0, :cond_0

    .line 892
    const/4 v1, 0x0

    .line 894
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->getView()Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public hideUpperRightProgressBar()V
    .locals 1

    .prologue
    .line 961
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mRootView:Lcom/google/android/finsky/layout/AddressFieldsLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->hideUpperRightProgressBar()V

    .line 962
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 911
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-direct {p0, p1, p3}, Lcom/android/i18n/addressinput/AddressWidget;->updateChildNodes(Landroid/widget/AdapterView;I)V

    .line 912
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 907
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public renderForm()V
    .locals 3

    .prologue
    .line 536
    invoke-direct {p0}, Lcom/android/i18n/addressinput/AddressWidget;->setWidgetLocaleAndScript()V

    .line 537
    new-instance v1, Lcom/android/i18n/addressinput/AddressData$Builder;

    invoke-direct {v1}, Lcom/android/i18n/addressinput/AddressData$Builder;-><init>()V

    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/AddressData$Builder;->setCountry(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mWidgetLocale:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/AddressData$Builder;->setLanguageCode(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->build()Lcom/android/i18n/addressinput/AddressData;

    move-result-object v0

    .line 539
    .local v0, "data":Lcom/android/i18n/addressinput/AddressData;
    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormController:Lcom/android/i18n/addressinput/FormController;

    new-instance v2, Lcom/android/i18n/addressinput/AddressWidget$2;

    invoke-direct {v2, p0}, Lcom/android/i18n/addressinput/AddressWidget$2;-><init>(Lcom/android/i18n/addressinput/AddressWidget;)V

    invoke-virtual {v1, v0, v2}, Lcom/android/i18n/addressinput/FormController;->requestDataForAddress(Lcom/android/i18n/addressinput/AddressData;Lcom/android/i18n/addressinput/DataLoadListener;)V

    .line 557
    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mRootView:Lcom/google/android/finsky/layout/AddressFieldsLayout;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->showProgressBar()V

    .line 558
    return-void
.end method

.method public renderFormWithSavedAddress(Lcom/android/i18n/addressinput/AddressData;)V
    .locals 1
    .param p1, "savedAddress"    # Lcom/android/i18n/addressinput/AddressData;

    .prologue
    .line 616
    if-eqz p1, :cond_0

    .line 617
    iput-object p1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSavedAddress:Lcom/android/i18n/addressinput/AddressData;

    .line 618
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mRootView:Lcom/google/android/finsky/layout/AddressFieldsLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->disableOneFieldMode()V

    .line 620
    :cond_0
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressWidget;->renderForm()V

    .line 621
    return-void
.end method

.method public restoreInstanceState(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "inState"    # Landroid/os/Bundle;

    .prologue
    .line 1014
    const-string v7, "address_data"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Lcom/android/i18n/addressinput/AddressData;

    iput-object v7, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSavedAddress:Lcom/android/i18n/addressinput/AddressData;

    .line 1015
    iget-object v7, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSavedAddress:Lcom/android/i18n/addressinput/AddressData;

    const/4 v8, 0x1

    invoke-direct {p0, v7, v8}, Lcom/android/i18n/addressinput/AddressWidget;->initializeFieldsWithAddress(Lcom/android/i18n/addressinput/AddressData;Z)V

    .line 1018
    const-string v7, "address_error_fields"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 1019
    .local v4, "keys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v7, "address_error_values"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 1020
    .local v6, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v4, :cond_1

    if-eqz v6, :cond_1

    .line 1021
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1022
    .local v0, "errors":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/android/i18n/addressinput/AddressField;Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 1023
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1024
    .local v3, "key":I
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1025
    .local v5, "value":Ljava/lang/String;
    invoke-static {}, Lcom/android/i18n/addressinput/AddressField;->values()[Lcom/android/i18n/addressinput/AddressField;

    move-result-object v7

    aget-object v1, v7, v3

    .line 1026
    .local v1, "field":Lcom/android/i18n/addressinput/AddressField;
    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1022
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1028
    .end local v1    # "field":Lcom/android/i18n/addressinput/AddressField;
    .end local v3    # "key":I
    .end local v5    # "value":Ljava/lang/String;
    :cond_0
    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSavedErrors:Ljava/util/Map;

    .line 1029
    invoke-direct {p0, v0}, Lcom/android/i18n/addressinput/AddressWidget;->setViewErrors(Ljava/util/Map;)V

    .line 1031
    .end local v0    # "errors":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/android/i18n/addressinput/AddressField;Ljava/lang/String;>;"
    .end local v2    # "i":I
    :cond_1
    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 999
    const-string v5, "address_data"

    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressWidget;->getAddressData()Lcom/android/i18n/addressinput/AddressData;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1002
    invoke-direct {p0}, Lcom/android/i18n/addressinput/AddressWidget;->getViewErrors()Ljava/util/Map;

    move-result-object v0

    .line 1003
    .local v0, "errors":Ljava/util/Map;, "Ljava/util/Map<Lcom/android/i18n/addressinput/AddressField;Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1004
    .local v3, "keys":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1005
    .local v4, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/i18n/addressinput/AddressField;

    .line 1006
    .local v1, "field":Lcom/android/i18n/addressinput/AddressField;
    invoke-virtual {v1}, Lcom/android/i18n/addressinput/AddressField;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1007
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1009
    .end local v1    # "field":Lcom/android/i18n/addressinput/AddressField;
    :cond_0
    const-string v5, "address_error_fields"

    invoke-virtual {p1, v5, v3}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1010
    const-string v5, "address_error_values"

    invoke-virtual {p1, v5, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1011
    return-void
.end method

.method public setAddressFromSuggestion(Lcom/android/i18n/addressinput/AddressData;)V
    .locals 3
    .param p1, "address"    # Lcom/android/i18n/addressinput/AddressData;

    .prologue
    .line 934
    sget-object v2, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_1:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {p0, v2}, Lcom/android/i18n/addressinput/AddressWidget;->getViewForField(Lcom/android/i18n/addressinput/AddressField;)Landroid/view/View;

    move-result-object v1

    .line 935
    .local v1, "view":Landroid/view/View;
    instance-of v2, v1, Lcom/google/android/finsky/layout/AddressAutoComplete;

    if-eqz v2, :cond_2

    check-cast v1, Lcom/google/android/finsky/layout/AddressAutoComplete;

    .end local v1    # "view":Landroid/view/View;
    move-object v0, v1

    .line 941
    .local v0, "autoCompleteView":Lcom/google/android/finsky/layout/AddressAutoComplete;
    :goto_0
    if-eqz v0, :cond_0

    .line 942
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddressAutoComplete;->blockNextSuggestion()V

    .line 944
    :cond_0
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/android/i18n/addressinput/AddressWidget;->initializeFieldsWithAddress(Lcom/android/i18n/addressinput/AddressData;Z)V

    .line 945
    if-eqz v0, :cond_1

    .line 946
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddressAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/AddressAutoComplete;->setSelection(I)V

    .line 948
    :cond_1
    return-void

    .line 935
    .end local v0    # "autoCompleteView":Lcom/google/android/finsky/layout/AddressAutoComplete;
    .restart local v1    # "view":Landroid/view/View;
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 342
    iget-object v2, p0, Lcom/android/i18n/addressinput/AddressWidget;->mInputWidgets:Ljava/util/EnumMap;

    invoke-virtual {v2}, Ljava/util/EnumMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/i18n/addressinput/AddressUIComponent;

    .line 343
    .local v0, "addressField":Lcom/android/i18n/addressinput/AddressUIComponent;
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->getView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 344
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressUIComponent;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 347
    .end local v0    # "addressField":Lcom/android/i18n/addressinput/AddressUIComponent;
    :cond_1
    return-void
.end method

.method public setFormOptions(Lcom/android/i18n/addressinput/FormOptions;)V
    .locals 0
    .param p1, "formOptions"    # Lcom/android/i18n/addressinput/FormOptions;

    .prologue
    .line 731
    iput-object p1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormOptions:Lcom/android/i18n/addressinput/FormOptions;

    .line 732
    return-void
.end method

.method public setListener(Lcom/android/i18n/addressinput/AddressWidget$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/i18n/addressinput/AddressWidget$Listener;

    .prologue
    .line 335
    iput-object p1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mListener:Lcom/android/i18n/addressinput/AddressWidget$Listener;

    .line 336
    return-void
.end method

.method public setSuggestionProvider(Lcom/google/android/finsky/layout/AddressSuggestionProvider;)V
    .locals 2
    .param p1, "suggestionProvider"    # Lcom/google/android/finsky/layout/AddressSuggestionProvider;

    .prologue
    .line 921
    iput-object p1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSuggestionProvider:Lcom/google/android/finsky/layout/AddressSuggestionProvider;

    .line 922
    sget-object v1, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_1:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {p0, v1}, Lcom/android/i18n/addressinput/AddressWidget;->getViewForField(Lcom/android/i18n/addressinput/AddressField;)Landroid/view/View;

    move-result-object v0

    .line 923
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/finsky/layout/AddressAutoComplete;

    if-eqz v1, :cond_0

    .line 924
    check-cast v0, Lcom/google/android/finsky/layout/AddressAutoComplete;

    .end local v0    # "view":Landroid/view/View;
    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSuggestionProvider:Lcom/google/android/finsky/layout/AddressSuggestionProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/AddressAutoComplete;->setSuggestionProvider(Lcom/google/android/finsky/layout/AddressSuggestionProvider;)V

    .line 926
    :cond_0
    return-void
.end method

.method public showUpperRightProgressBar()V
    .locals 1

    .prologue
    .line 954
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mRootView:Lcom/google/android/finsky/layout/AddressFieldsLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->showUpperRightProgressBar()V

    .line 955
    return-void
.end method

.method public updateWidgetOnCountryChange(Ljava/lang/String;)V
    .locals 2
    .param p1, "regionCode"    # Ljava/lang/String;

    .prologue
    .line 509
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    :goto_0
    return-void

    .line 514
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mSavedAddress:Lcom/android/i18n/addressinput/AddressData;

    .line 515
    iput-object p1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    .line 516
    iget-object v0, p0, Lcom/android/i18n/addressinput/AddressWidget;->mFormController:Lcom/android/i18n/addressinput/FormController;

    iget-object v1, p0, Lcom/android/i18n/addressinput/AddressWidget;->mCurrentRegion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/FormController;->setCurrentCountry(Ljava/lang/String;)V

    .line 517
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressWidget;->renderForm()V

    goto :goto_0
.end method
