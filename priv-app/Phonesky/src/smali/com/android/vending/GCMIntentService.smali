.class public Lcom/android/vending/GCMIntentService;
.super Lcom/google/android/gcm/GCMBaseIntentService;
.source "GCMIntentService.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "932144863878"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gcm/GCMBaseIntentService;-><init>([Ljava/lang/String;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected onError(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "errId"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-static {p1, p2}, Lcom/google/android/finsky/utils/GcmRegistrationIdHelper;->onGcmError(Landroid/content/Context;Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method protected onMessage(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 33
    invoke-static {p1, p2}, Lcom/google/android/finsky/utils/GcmRegistrationIdHelper;->onGcmMessage(Landroid/content/Context;Landroid/content/Intent;)V

    .line 34
    return-void
.end method

.method protected onRegistered(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "regId"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-static {p1, p2}, Lcom/google/android/finsky/utils/GcmRegistrationIdHelper;->onGcmRegistered(Landroid/content/Context;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method protected onUnregistered(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "regId"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-static {p1, p2}, Lcom/google/android/finsky/utils/GcmRegistrationIdHelper;->onGcmUnregistered(Landroid/content/Context;Ljava/lang/String;)V

    .line 44
    return-void
.end method
