.class public Lcom/android/vending/VendingBackupAgent;
.super Landroid/app/backup/BackupAgentHelper;
.source "VendingBackupAgent.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/backup/BackupAgentHelper;-><init>()V

    return-void
.end method

.method private flushBufferData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V
    .locals 2
    .param p1, "data"    # Landroid/app/backup/BackupDataOutput;
    .param p2, "bufStream"    # Ljava/io/ByteArrayOutputStream;
    .param p3, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-virtual {p2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 102
    .local v0, "backupData":[B
    array-length v1, v0

    invoke-virtual {p1, p3, v1}, Landroid/app/backup/BackupDataOutput;->writeEntityHeader(Ljava/lang/String;I)I

    .line 103
    array-length v1, v0

    invoke-virtual {p1, v0, v1}, Landroid/app/backup/BackupDataOutput;->writeEntityData([BI)I

    .line 104
    invoke-virtual {p2}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 105
    return-void
.end method

.method public static registerWithBackup(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/finsky/utils/VendingPreferences;->BACKED_UP:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Landroid/app/backup/BackupManager;

    invoke-direct {v0, p0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/backup/BackupManager;->dataChanged()V

    .line 53
    :cond_0
    return-void
.end method

.method private writeData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/io/DataOutputStream;Ljava/lang/String;I)V
    .locals 0
    .param p1, "data"    # Landroid/app/backup/BackupDataOutput;
    .param p2, "bufStream"    # Ljava/io/ByteArrayOutputStream;
    .param p3, "outWriter"    # Ljava/io/DataOutputStream;
    .param p4, "key"    # Ljava/lang/String;
    .param p5, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-virtual {p3, p5}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 90
    invoke-direct {p0, p1, p2, p4}, Lcom/android/vending/VendingBackupAgent;->flushBufferData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method private writeData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/io/DataOutputStream;Ljava/lang/String;J)V
    .locals 1
    .param p1, "data"    # Landroid/app/backup/BackupDataOutput;
    .param p2, "bufStream"    # Ljava/io/ByteArrayOutputStream;
    .param p3, "outWriter"    # Ljava/io/DataOutputStream;
    .param p4, "key"    # Ljava/lang/String;
    .param p5, "value"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p3, p5, p6}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 84
    invoke-direct {p0, p1, p2, p4}, Lcom/android/vending/VendingBackupAgent;->flushBufferData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method private writeData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/io/DataOutputStream;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "data"    # Landroid/app/backup/BackupDataOutput;
    .param p2, "bufStream"    # Ljava/io/ByteArrayOutputStream;
    .param p3, "outWriter"    # Ljava/io/DataOutputStream;
    .param p4, "key"    # Ljava/lang/String;
    .param p5, "value"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p3, p5}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 96
    invoke-direct {p0, p1, p2, p4}, Lcom/android/vending/VendingBackupAgent;->flushBufferData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V

    .line 97
    return-void
.end method


# virtual methods
.method public onBackup(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V
    .locals 14
    .param p1, "oldState"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "data"    # Landroid/app/backup/BackupDataOutput;
    .param p3, "newState"    # Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 59
    .local v3, "bufStream":Ljava/io/ByteArrayOutputStream;
    new-instance v4, Ljava/io/DataOutputStream;

    invoke-direct {v4, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 62
    .local v4, "outWriter":Ljava/io/DataOutputStream;
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 63
    .local v6, "aid":J
    const-string v0, "Backing up aid: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    const-string v5, "vending"

    move-object v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/android/vending/VendingBackupAgent;->writeData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/io/DataOutputStream;Ljava/lang/String;J)V

    .line 67
    const-string v12, "auto_update_enabled"

    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    move-object v8, p0

    move-object/from16 v9, p2

    move-object v10, v3

    move-object v11, v4

    invoke-direct/range {v8 .. v13}, Lcom/android/vending/VendingBackupAgent;->writeData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/io/DataOutputStream;Ljava/lang/String;Z)V

    .line 69
    const-string v12, "update_over_wifi_only"

    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    move-object v8, p0

    move-object/from16 v9, p2

    move-object v10, v3

    move-object v11, v4

    invoke-direct/range {v8 .. v13}, Lcom/android/vending/VendingBackupAgent;->writeData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/io/DataOutputStream;Ljava/lang/String;Z)V

    .line 71
    const-string v12, "auto_add_shortcuts"

    sget-object v0, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_ADD_SHORTCUTS:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    move-object v8, p0

    move-object/from16 v9, p2

    move-object v10, v3

    move-object v11, v4

    invoke-direct/range {v8 .. v13}, Lcom/android/vending/VendingBackupAgent;->writeData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/io/DataOutputStream;Ljava/lang/String;Z)V

    .line 73
    const-string v12, "notify_updates"

    sget-object v0, Lcom/google/android/finsky/utils/VendingPreferences;->NOTIFY_UPDATES:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    move-object v8, p0

    move-object/from16 v9, p2

    move-object v10, v3

    move-object v11, v4

    invoke-direct/range {v8 .. v13}, Lcom/android/vending/VendingBackupAgent;->writeData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/io/DataOutputStream;Ljava/lang/String;Z)V

    .line 75
    const-string v12, "content-filter-level"

    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->contentFilterLevel:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object v8, p0

    move-object/from16 v9, p2

    move-object v10, v3

    move-object v11, v4

    invoke-direct/range {v8 .. v13}, Lcom/android/vending/VendingBackupAgent;->writeData(Landroid/app/backup/BackupDataOutput;Ljava/io/ByteArrayOutputStream;Ljava/io/DataOutputStream;Ljava/lang/String;I)V

    .line 78
    sget-object v0, Lcom/google/android/finsky/utils/VendingPreferences;->BACKED_UP:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method public onRestore(Landroid/app/backup/BackupDataInput;ILandroid/os/ParcelFileDescriptor;)V
    .locals 19
    .param p1, "data"    # Landroid/app/backup/BackupDataInput;
    .param p2, "appVersionCode"    # I
    .param p3, "newState"    # Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    const-string v15, "Entered onRestore"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    const/4 v4, 0x0

    .line 118
    .local v4, "aidRestored":Z
    const/4 v7, 0x0

    .line 119
    .local v7, "autoUpdateByDefault":Ljava/lang/Boolean;
    const/4 v8, 0x0

    .line 120
    .local v8, "autoUpdateEnabled":Ljava/lang/Boolean;
    const/4 v9, 0x0

    .line 121
    .local v9, "autoUpdateWifiOnly":Ljava/lang/Boolean;
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/app/backup/BackupDataInput;->readNextHeader()Z

    move-result v15

    if-eqz v15, :cond_7

    .line 122
    invoke-virtual/range {p1 .. p1}, Landroid/app/backup/BackupDataInput;->getDataSize()I

    move-result v12

    .line 123
    .local v12, "dataSize":I
    new-array v11, v12, [B

    .line 124
    .local v11, "dataBuf":[B
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v15, v12}, Landroid/app/backup/BackupDataInput;->readEntityData([BII)I

    .line 125
    new-instance v10, Ljava/io/ByteArrayInputStream;

    invoke-direct {v10, v11}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 126
    .local v10, "baStream":Ljava/io/ByteArrayInputStream;
    new-instance v13, Ljava/io/DataInputStream;

    invoke-direct {v13, v10}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 128
    .local v13, "in":Ljava/io/DataInputStream;
    invoke-virtual/range {p1 .. p1}, Landroid/app/backup/BackupDataInput;->getKey()Ljava/lang/String;

    move-result-object v14

    .line 129
    .local v14, "key":Ljava/lang/String;
    const-string v15, "Restoring key %s"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v14, v16, v17

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 130
    const-string v15, "vending"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 131
    invoke-virtual {v13}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    .line 132
    .local v2, "aid":J
    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v5

    .line 133
    .local v5, "aidString":Ljava/lang/String;
    const-string v15, "Restored aid: %s"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v5}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 136
    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5, v15}, Lcom/google/android/finsky/services/RestoreService;->restoreAccounts(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    sget-object v15, Lcom/google/android/finsky/utils/VendingPreferences;->RESTORED_ANDROID_ID:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v15, v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 144
    const/4 v4, 0x1

    .line 145
    goto :goto_0

    .end local v2    # "aid":J
    .end local v5    # "aidString":Ljava/lang/String;
    :cond_1
    const-string v15, "auto_update_enabled"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 146
    invoke-virtual {v13}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v15

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    goto :goto_0

    .line 147
    :cond_2
    const-string v15, "auto_update_default"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 148
    invoke-virtual {v13}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v15

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    goto/16 :goto_0

    .line 149
    :cond_3
    const-string v15, "update_over_wifi_only"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 150
    invoke-virtual {v13}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v15

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    goto/16 :goto_0

    .line 151
    :cond_4
    const-string v15, "auto_add_shortcuts"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 152
    sget-object v15, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_ADD_SHORTCUTS:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v13}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 153
    :cond_5
    const-string v15, "notify_updates"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 154
    sget-object v15, Lcom/google/android/finsky/utils/VendingPreferences;->NOTIFY_UPDATES:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v13}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 155
    :cond_6
    const-string v15, "content-filter-level"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 156
    sget-object v15, Lcom/google/android/finsky/utils/FinskyPreferences;->contentFilterLevel:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v13}, Ljava/io/DataInputStream;->readInt()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 161
    .end local v10    # "baStream":Ljava/io/ByteArrayInputStream;
    .end local v11    # "dataBuf":[B
    .end local v12    # "dataSize":I
    .end local v13    # "in":Ljava/io/DataInputStream;
    .end local v14    # "key":Ljava/lang/String;
    :cond_7
    sget-object v15, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v15}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v15

    if-eqz v15, :cond_b

    .line 162
    const-string v15, "Skip restore auto-update - already set locally."

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    :cond_8
    :goto_1
    if-eqz v9, :cond_9

    sget-object v15, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v15}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v15

    if-eqz v15, :cond_9

    .line 178
    sget-object v15, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v15, v9}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 181
    :cond_9
    if-nez v4, :cond_a

    .line 182
    const-string v15, "Restore completed, no Market aid was found"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 185
    :cond_a
    const-string v15, "Finished onRestore"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 186
    return-void

    .line 163
    :cond_b
    if-eqz v8, :cond_c

    .line 164
    sget-object v15, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v15, v8}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_1

    .line 165
    :cond_c
    if-eqz v7, :cond_8

    .line 167
    sget-object v15, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_BY_DEFAULT:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v15, v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 168
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v6

    .line 169
    .local v6, "appStates":Lcom/google/android/finsky/appstate/AppStates;
    new-instance v15, Lcom/android/vending/VendingBackupAgent$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v6}, Lcom/android/vending/VendingBackupAgent$1;-><init>(Lcom/android/vending/VendingBackupAgent;Lcom/google/android/finsky/appstate/AppStates;)V

    invoke-virtual {v6, v15}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method
