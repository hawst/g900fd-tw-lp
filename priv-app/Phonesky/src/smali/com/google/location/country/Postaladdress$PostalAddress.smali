.class public final Lcom/google/location/country/Postaladdress$PostalAddress;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Postaladdress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/location/country/Postaladdress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PostalAddress"
.end annotation


# instance fields
.field public addressLine:[Ljava/lang/String;

.field public administrativeAreaName:Ljava/lang/String;

.field public countryName:Ljava/lang/String;

.field public countryNameCode:Ljava/lang/String;

.field public dependentLocalityName:Ljava/lang/String;

.field public dependentThoroughfareName:Ljava/lang/String;

.field public firmName:Ljava/lang/String;

.field public languageCode:Ljava/lang/String;

.field public localityName:Ljava/lang/String;

.field public postBoxNumber:Ljava/lang/String;

.field public postalCodeNumber:Ljava/lang/String;

.field public postalCodeNumberExtension:Ljava/lang/String;

.field public premiseName:Ljava/lang/String;

.field public recipientName:Ljava/lang/String;

.field public sortingCode:Ljava/lang/String;

.field public subAdministrativeAreaName:Ljava/lang/String;

.field public subPremiseName:Ljava/lang/String;

.field public thoroughfareName:Ljava/lang/String;

.field public thoroughfareNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 83
    invoke-virtual {p0}, Lcom/google/location/country/Postaladdress$PostalAddress;->clear()Lcom/google/location/country/Postaladdress$PostalAddress;

    .line 84
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/location/country/Postaladdress$PostalAddress;
    .locals 1

    .prologue
    .line 87
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryName:Ljava/lang/String;

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    .line 91
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subAdministrativeAreaName:Ljava/lang/String;

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->localityName:Ljava/lang/String;

    .line 93
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentLocalityName:Ljava/lang/String;

    .line 94
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareName:Ljava/lang/String;

    .line 95
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareNumber:Ljava/lang/String;

    .line 96
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentThoroughfareName:Ljava/lang/String;

    .line 97
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    .line 98
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumberExtension:Ljava/lang/String;

    .line 99
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->sortingCode:Ljava/lang/String;

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postBoxNumber:Ljava/lang/String;

    .line 101
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->premiseName:Ljava/lang/String;

    .line 102
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subPremiseName:Ljava/lang/String;

    .line 103
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->firmName:Ljava/lang/String;

    .line 105
    const-string v0, ""

    iput-object v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->recipientName:Ljava/lang/String;

    .line 106
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->cachedSize:I

    .line 107
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 180
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 181
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 182
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 185
    :cond_0
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 186
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 189
    :cond_1
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 190
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 193
    :cond_2
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subAdministrativeAreaName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 194
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subAdministrativeAreaName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 197
    :cond_3
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->localityName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 198
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->localityName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 201
    :cond_4
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 202
    const/4 v5, 0x6

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 205
    :cond_5
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareNumber:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 206
    const/16 v5, 0xb

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareNumber:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 209
    :cond_6
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 210
    const/16 v5, 0xc

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 213
    :cond_7
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumberExtension:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 214
    const/16 v5, 0xd

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumberExtension:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 217
    :cond_8
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_b

    .line 218
    const/4 v0, 0x0

    .line 219
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 220
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_a

    .line 221
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 222
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_9

    .line 223
    add-int/lit8 v0, v0, 0x1

    .line 224
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 220
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 228
    .end local v2    # "element":Ljava/lang/String;
    :cond_a
    add-int/2addr v4, v1

    .line 229
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 231
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_b
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->premiseName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 232
    const/16 v5, 0xf

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->premiseName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 235
    :cond_c
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subPremiseName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 236
    const/16 v5, 0x10

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subPremiseName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 239
    :cond_d
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentLocalityName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 240
    const/16 v5, 0x11

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentLocalityName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 243
    :cond_e
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentThoroughfareName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 244
    const/16 v5, 0x15

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentThoroughfareName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 247
    :cond_f
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 248
    const/16 v5, 0x1a

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 251
    :cond_10
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->firmName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_11

    .line 252
    const/16 v5, 0x1b

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->firmName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 255
    :cond_11
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->recipientName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 256
    const/16 v5, 0x1c

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->recipientName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 259
    :cond_12
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->sortingCode:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_13

    .line 260
    const/16 v5, 0x1d

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->sortingCode:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 263
    :cond_13
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postBoxNumber:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_14

    .line 264
    const/16 v5, 0x1e

    iget-object v6, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postBoxNumber:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 267
    :cond_14
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/location/country/Postaladdress$PostalAddress;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 275
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 276
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 280
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 281
    :sswitch_0
    return-object p0

    .line 286
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    goto :goto_0

    .line 290
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryName:Ljava/lang/String;

    goto :goto_0

    .line 294
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    goto :goto_0

    .line 298
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subAdministrativeAreaName:Ljava/lang/String;

    goto :goto_0

    .line 302
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->localityName:Ljava/lang/String;

    goto :goto_0

    .line 306
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareName:Ljava/lang/String;

    goto :goto_0

    .line 310
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareNumber:Ljava/lang/String;

    goto :goto_0

    .line 314
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    goto :goto_0

    .line 318
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumberExtension:Ljava/lang/String;

    goto :goto_0

    .line 322
    :sswitch_a
    const/16 v5, 0x72

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 324
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 325
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 326
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 327
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 329
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 330
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 331
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 329
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 324
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 334
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 335
    iput-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    goto :goto_0

    .line 339
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->premiseName:Ljava/lang/String;

    goto/16 :goto_0

    .line 343
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subPremiseName:Ljava/lang/String;

    goto/16 :goto_0

    .line 347
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentLocalityName:Ljava/lang/String;

    goto/16 :goto_0

    .line 351
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentThoroughfareName:Ljava/lang/String;

    goto/16 :goto_0

    .line 355
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 359
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->firmName:Ljava/lang/String;

    goto/16 :goto_0

    .line 363
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->recipientName:Ljava/lang/String;

    goto/16 :goto_0

    .line 367
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->sortingCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 371
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postBoxNumber:Ljava/lang/String;

    goto/16 :goto_0

    .line 276
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x5a -> :sswitch_7
        0x62 -> :sswitch_8
        0x6a -> :sswitch_9
        0x72 -> :sswitch_a
        0x7a -> :sswitch_b
        0x82 -> :sswitch_c
        0x8a -> :sswitch_d
        0xaa -> :sswitch_e
        0xd2 -> :sswitch_f
        0xda -> :sswitch_10
        0xe2 -> :sswitch_11
        0xea -> :sswitch_12
        0xf2 -> :sswitch_13
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/location/country/Postaladdress$PostalAddress;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/location/country/Postaladdress$PostalAddress;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 114
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryNameCode:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 116
    :cond_0
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 117
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->countryName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 119
    :cond_1
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 120
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->administrativeAreaName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 122
    :cond_2
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subAdministrativeAreaName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 123
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subAdministrativeAreaName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 125
    :cond_3
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->localityName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 126
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->localityName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 128
    :cond_4
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 129
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 131
    :cond_5
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareNumber:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 132
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->thoroughfareNumber:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 134
    :cond_6
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 135
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumber:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 137
    :cond_7
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumberExtension:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 138
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postalCodeNumberExtension:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 140
    :cond_8
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 141
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 142
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->addressLine:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 143
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 144
    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 141
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 148
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_a
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->premiseName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 149
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->premiseName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 151
    :cond_b
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subPremiseName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 152
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->subPremiseName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 154
    :cond_c
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentLocalityName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 155
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentLocalityName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 157
    :cond_d
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentThoroughfareName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 158
    const/16 v2, 0x15

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->dependentThoroughfareName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 160
    :cond_e
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 161
    const/16 v2, 0x1a

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->languageCode:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 163
    :cond_f
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->firmName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 164
    const/16 v2, 0x1b

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->firmName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 166
    :cond_10
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->recipientName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 167
    const/16 v2, 0x1c

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->recipientName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 169
    :cond_11
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->sortingCode:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 170
    const/16 v2, 0x1d

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->sortingCode:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 172
    :cond_12
    iget-object v2, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postBoxNumber:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 173
    const/16 v2, 0x1e

    iget-object v3, p0, Lcom/google/location/country/Postaladdress$PostalAddress;->postBoxNumber:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 175
    :cond_13
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 176
    return-void
.end method
