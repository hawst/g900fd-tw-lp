.class public interface abstract Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;
.super Ljava/lang/Object;
.source "PseudonymousCookieSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;
    }
.end annotation


# virtual methods
.method public abstract getCookieName()Ljava/lang/String;
.end method

.method public abstract getCookieValue()Ljava/lang/String;
.end method

.method public abstract setCookieValue(Ljava/lang/String;)V
.end method
