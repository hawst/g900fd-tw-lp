.class Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
.super Ljava/lang/Object;
.source "RestoreService.java"

# interfaces
.implements Lcom/google/android/finsky/installer/InstallerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/services/RestoreService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RestoreTracker"
.end annotation


# instance fields
.field private final ACCOUNT_MAP_KEY_AID:Ljava/lang/String;

.field private final ACCOUNT_MAP_KEY_ATTEMPTS:Ljava/lang/String;

.field private final KEY_VALUE_DIRECTORY:Ljava/lang/String;

.field private final KEY_VALUE_FILE_ACCOUNTS:Ljava/lang/String;

.field private final KEY_VALUE_FILE_PACKAGES:Ljava/lang/String;

.field private final PACKAGE_MAP_ACCOUNT_NAME:Ljava/lang/String;

.field private final PACKAGE_MAP_APP_ICON_URL:Ljava/lang/String;

.field private final PACKAGE_MAP_DELIVERY_TOKEN:Ljava/lang/String;

.field private final PACKAGE_MAP_KEY_ATTEMPTS:Ljava/lang/String;

.field private final PACKAGE_MAP_PRIORITY:Ljava/lang/String;

.field private final PACKAGE_MAP_RETRY_TIME:Ljava/lang/String;

.field private final PACKAGE_MAP_TITLE:Ljava/lang/String;

.field private final PACKAGE_MAP_VERSION_CODE:Ljava/lang/String;

.field private final PACKAGE_MAP_VISIBLE:Ljava/lang/String;

.field private final mAccountStatusMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mAccountStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

.field private final mBitmapStatusMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mFailed:I

.field private mInstallerRunningPackage:Ljava/lang/String;

.field private final mPackageStatusMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mPackagesStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

.field private mStartupRefCount:I

.field private mSucceeded:I

.field final synthetic this$0:Lcom/google/android/finsky/services/RestoreService;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/services/RestoreService;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 440
    iput-object p1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 443
    const-string v0, "RestoreTracker"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->KEY_VALUE_DIRECTORY:Ljava/lang/String;

    .line 445
    const-string v0, "account-"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->KEY_VALUE_FILE_ACCOUNTS:Ljava/lang/String;

    .line 447
    const-string v0, "package-"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->KEY_VALUE_FILE_PACKAGES:Ljava/lang/String;

    .line 450
    const-string v0, "attempts"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->ACCOUNT_MAP_KEY_ATTEMPTS:Ljava/lang/String;

    .line 451
    const-string v0, "aid"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->ACCOUNT_MAP_KEY_AID:Ljava/lang/String;

    .line 454
    const-string v0, "attempts"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->PACKAGE_MAP_KEY_ATTEMPTS:Ljava/lang/String;

    .line 455
    const-string v0, "versionCode"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->PACKAGE_MAP_VERSION_CODE:Ljava/lang/String;

    .line 456
    const-string v0, "accountName"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->PACKAGE_MAP_ACCOUNT_NAME:Ljava/lang/String;

    .line 457
    const-string v0, "title"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->PACKAGE_MAP_TITLE:Ljava/lang/String;

    .line 458
    const-string v0, "priority"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->PACKAGE_MAP_PRIORITY:Ljava/lang/String;

    .line 459
    const-string v0, "deliveryToken"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->PACKAGE_MAP_DELIVERY_TOKEN:Ljava/lang/String;

    .line 460
    const-string v0, "visible"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->PACKAGE_MAP_VISIBLE:Ljava/lang/String;

    .line 461
    const-string v0, "appIconUrl"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->PACKAGE_MAP_APP_ICON_URL:Ljava/lang/String;

    .line 462
    const-string v0, "retryTime"

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->PACKAGE_MAP_RETRY_TIME:Ljava/lang/String;

    .line 475
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    .line 484
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    .line 494
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mBitmapStatusMap:Ljava/util/Map;

    .line 497
    iput v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mStartupRefCount:I

    .line 500
    iput v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mSucceeded:I

    .line 503
    iput v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mFailed:I

    .line 506
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mInstallerRunningPackage:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/services/RestoreService;Lcom/google/android/finsky/services/RestoreService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p2, "x1"    # Lcom/google/android/finsky/services/RestoreService$1;

    .prologue
    .line 440
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;-><init>(Lcom/google/android/finsky/services/RestoreService;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    return-object v0
.end method

.method static synthetic access$1908(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    .prologue
    .line 440
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mStartupRefCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mStartupRefCount:I

    return v0
.end method

.method static synthetic access$1910(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    .prologue
    .line 440
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mStartupRefCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mStartupRefCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/util/Map;

    .prologue
    .line 440
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->convertEntryToStatus(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/Runnable;

    .prologue
    .line 440
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->initPackagesStore(Landroid/content/Context;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 440
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->writePackageStatus(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mInstallerRunningPackage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackagesStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/util/Map;

    .prologue
    .line 440
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->convertEntryToPackageStatus(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    return-object v0
.end method

.method private convertEntryToPackageStatus(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;"
        }
    .end annotation

    .prologue
    .line 716
    .local p3, "valueMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 718
    .local v8, "pm":Landroid/content/pm/PackageManager;
    const/16 v20, 0x0

    :try_start_0
    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v8, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 721
    const/4 v11, 0x0

    .line 779
    :goto_0
    return-object v11

    .line 722
    :catch_0
    move-exception v20

    .line 726
    const-string v20, "attempts"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 727
    .local v5, "countString":Ljava/lang/String;
    const-string v20, "versionCode"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 728
    .local v17, "versionCodeString":Ljava/lang/String;
    const-string v20, "accountName"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 729
    .local v2, "accountName":Ljava/lang/String;
    const-string v20, "title"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 730
    .local v15, "title":Ljava/lang/String;
    const-string v20, "priority"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 731
    .local v10, "priorityString":Ljava/lang/String;
    const-string v20, "deliveryToken"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 732
    .local v6, "deliveryToken":Ljava/lang/String;
    const-string v20, "visible"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 733
    .local v19, "visibleString":Ljava/lang/String;
    const-string v20, "appIconUrl"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 734
    .local v3, "appIconUrl":Ljava/lang/String;
    const-string v20, "retryTime"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 737
    .local v14, "retryTimeString":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_0

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_0

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_0

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_0

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 741
    :cond_0
    const-string v20, "Missing data for package %s"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object p2, v21, v22

    invoke-static/range {v20 .. v21}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 742
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 750
    :cond_1
    :try_start_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 751
    .local v4, "count":I
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 752
    .local v16, "versionCode":I
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 753
    .local v9, "priority":I
    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v18

    .line 754
    .local v18, "visible":Z
    invoke-static {v14}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v12

    .line 761
    .local v12, "retryTime":J
    if-ltz v4, :cond_2

    sget-object v20, Lcom/google/android/finsky/config/G;->appRestoreDownloadMaxAttempts:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v20

    move/from16 v0, v20

    if-lt v4, v0, :cond_3

    .line 762
    :cond_2
    const-string v20, "Reached limit %d for %s"

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    aput-object p2, v21, v22

    invoke-static/range {v20 .. v21}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 763
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 755
    .end local v4    # "count":I
    .end local v9    # "priority":I
    .end local v12    # "retryTime":J
    .end local v16    # "versionCode":I
    .end local v18    # "visible":Z
    :catch_1
    move-exception v7

    .line 756
    .local v7, "nfe":Ljava/lang/NumberFormatException;
    const-string v20, "Bad data for package %s (%s, %s, %s, %s, %s, %s)"

    const/16 v21, 0x8

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object p2, v21, v22

    const/16 v22, 0x1

    aput-object v5, v21, v22

    const/16 v22, 0x2

    aput-object v17, v21, v22

    const/16 v22, 0x3

    invoke-static {v2}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x4

    aput-object v15, v21, v22

    const/16 v22, 0x5

    aput-object v10, v21, v22

    const/16 v22, 0x6

    aput-object v19, v21, v22

    const/16 v22, 0x7

    aput-object v14, v21, v22

    invoke-static/range {v20 .. v21}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 759
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 765
    .end local v7    # "nfe":Ljava/lang/NumberFormatException;
    .restart local v4    # "count":I
    .restart local v9    # "priority":I
    .restart local v12    # "retryTime":J
    .restart local v16    # "versionCode":I
    .restart local v18    # "visible":Z
    :cond_3
    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v20

    if-nez v20, :cond_4

    .line 766
    const-string v20, "Unknown account %s"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v2}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 767
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 769
    :cond_4
    new-instance v11, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-direct {v11, v0}, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;-><init>(Lcom/google/android/finsky/services/RestoreService$1;)V

    .line 770
    .local v11, "result":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    iput v4, v11, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->attempts:I

    .line 771
    move/from16 v0, v16

    iput v0, v11, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->versionCode:I

    .line 772
    iput-object v2, v11, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->accountName:Ljava/lang/String;

    .line 773
    iput-object v15, v11, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->title:Ljava/lang/String;

    .line 774
    iput v9, v11, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->priority:I

    .line 775
    iput-object v6, v11, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->deliveryToken:Ljava/lang/String;

    .line 776
    move/from16 v0, v18

    iput-boolean v0, v11, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->visible:Z

    .line 777
    iput-object v3, v11, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->appIconUrl:Ljava/lang/String;

    .line 778
    iput-wide v12, v11, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->retryTime:J

    goto/16 :goto_0
.end method

.method private convertEntryToStatus(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "accountName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;"
        }
    .end annotation

    .prologue
    .local p3, "valueMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 554
    invoke-static {p2, p1}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v5

    if-nez v5, :cond_0

    .line 555
    const-string v5, "Unknown account %s"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v7}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v4, v6

    .line 579
    :goto_0
    return-object v4

    .line 558
    :cond_0
    const-string v5, "attempts"

    invoke-interface {p3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 559
    .local v2, "countString":Ljava/lang/String;
    const-string v5, "aid"

    invoke-interface {p3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 560
    .local v0, "aid":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 561
    :cond_1
    const-string v5, "Missing data for account %s"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v7}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v4, v6

    .line 562
    goto :goto_0

    .line 566
    :cond_2
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 572
    .local v1, "count":I
    if-ltz v1, :cond_3

    sget-object v5, Lcom/google/android/finsky/config/G;->appRestoreFetchListMaxAttempts:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-lt v1, v5, :cond_4

    .line 573
    :cond_3
    const-string v5, "Reached limit %d for %s"

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {p2}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v7}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v4, v6

    .line 574
    goto :goto_0

    .line 567
    .end local v1    # "count":I
    :catch_0
    move-exception v3

    .line 568
    .local v3, "nfe":Ljava/lang/NumberFormatException;
    const-string v5, "Bad data for account %s (%s, %s)"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    aput-object v2, v7, v10

    aput-object v0, v7, v11

    invoke-static {v5, v7}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v4, v6

    .line 570
    goto :goto_0

    .line 576
    .end local v3    # "nfe":Ljava/lang/NumberFormatException;
    .restart local v1    # "count":I
    :cond_4
    new-instance v4, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;

    invoke-direct {v4, v6}, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;-><init>(Lcom/google/android/finsky/services/RestoreService$1;)V

    .line 577
    .local v4, "result":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    iput v1, v4, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->attempts:I

    .line 578
    iput-object v0, v4, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->androidId:Ljava/lang/String;

    goto :goto_0
.end method

.method private initPackagesStore(Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onComplete"    # Ljava/lang/Runnable;

    .prologue
    .line 671
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackagesStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    if-nez v0, :cond_0

    .line 672
    new-instance v0, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    new-instance v1, Lcom/google/android/finsky/utils/persistence/FileBasedKeyValueStore;

    const-string v2, "RestoreTracker"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    const-string v3, "package-"

    invoke-direct {v1, v2, v3}, Lcom/google/android/finsky/utils/persistence/FileBasedKeyValueStore;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;-><init>(Lcom/google/android/finsky/utils/persistence/KeyValueStore;)V

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackagesStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    .line 675
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackagesStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    new-instance v1, Lcom/google/android/finsky/services/RestoreService$RestoreTracker$2;

    invoke-direct {v1, p0, p2}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker$2;-><init>(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;->load(Ljava/lang/Runnable;)V

    .line 703
    :goto_0
    return-void

    .line 701
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackagesStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    invoke-virtual {v0, p2}, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;->load(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private trackPackageForListener(Ljava/lang/String;ZZ)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "running"    # Z
    .param p3, "willRetry"    # Z

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1071
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .line 1072
    .local v0, "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->priority:I

    if-eq v1, v4, :cond_1

    .line 1107
    :cond_0
    :goto_0
    return-void

    .line 1076
    :cond_1
    if-eqz p2, :cond_3

    .line 1078
    iput-object p1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mInstallerRunningPackage:Ljava/lang/String;

    .line 1080
    iget-boolean v1, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->visible:Z

    if-eqz v1, :cond_2

    .line 1081
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    const/4 v2, 0x3

    iget-object v3, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->title:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->notifyListener(ILjava/lang/String;Ljava/lang/String;Z)V
    invoke-static {v1, v2, p1, v3, v4}, Lcom/google/android/finsky/services/RestoreService;->access$1000(Lcom/google/android/finsky/services/RestoreService;ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1084
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->notifyListener(ILjava/lang/String;Ljava/lang/String;Z)V
    invoke-static {v1, v5, p1, v2, v3}, Lcom/google/android/finsky/services/RestoreService;->access$1000(Lcom/google/android/finsky/services/RestoreService;ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1088
    :cond_3
    iput-object v2, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mInstallerRunningPackage:Ljava/lang/String;

    .line 1095
    if-eqz p3, :cond_4

    .line 1099
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->notifyListener(ILjava/lang/String;Ljava/lang/String;Z)V
    invoke-static {v1, v5, p1, v2, v3}, Lcom/google/android/finsky/services/RestoreService;->access$1000(Lcom/google/android/finsky/services/RestoreService;ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1102
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->shouldHold(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1104
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->notifyListener(ILjava/lang/String;Ljava/lang/String;Z)V
    invoke-static {v1, v4, p1, v2, v3}, Lcom/google/android/finsky/services/RestoreService;->access$1000(Lcom/google/android/finsky/services/RestoreService;ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private writeAccountStatus(Ljava/lang/String;)V
    .locals 5
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 654
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 655
    .local v0, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;

    .line 656
    .local v1, "status":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    if-nez v1, :cond_0

    .line 657
    iget-object v3, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;->delete(Ljava/lang/String;)V

    .line 664
    :goto_0
    return-void

    .line 659
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    .line 660
    .local v2, "writeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "attempts"

    iget v4, v1, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->attempts:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 661
    const-string v3, "aid"

    iget-object v4, v1, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->androidId:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 662
    iget-object v3, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;->put(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method

.method private writePackageStatus(Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 860
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 861
    .local v0, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .line 862
    .local v1, "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    if-nez v1, :cond_0

    .line 863
    iget-object v3, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackagesStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;->delete(Ljava/lang/String;)V

    .line 879
    :goto_0
    return-void

    .line 865
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    .line 866
    .local v2, "writeMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "attempts"

    iget v4, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->attempts:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 867
    const-string v3, "versionCode"

    iget v4, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->versionCode:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    const-string v3, "accountName"

    iget-object v4, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->accountName:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 869
    const-string v3, "title"

    iget-object v4, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->title:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 870
    const-string v3, "priority"

    iget v4, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->priority:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 871
    iget-object v3, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->deliveryToken:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 872
    const-string v3, "deliveryToken"

    iget-object v4, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->deliveryToken:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 874
    :cond_1
    const-string v3, "visible"

    iget-boolean v4, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->visible:Z

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 875
    const-string v3, "appIconUrl"

    iget-object v4, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->appIconUrl:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 876
    const-string v3, "retryTime"

    iget-wide v4, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->retryTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 877
    iget-object v3, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackagesStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;->put(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method


# virtual methods
.method public finishAccount(Ljava/lang/String;ZI)V
    .locals 5
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "success"    # Z
    .param p3, "errorCode"    # I

    .prologue
    const/4 v3, 0x0

    .line 605
    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;

    .line 608
    .local v1, "status":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    new-instance v2, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    const/16 v4, 0x76

    invoke-direct {v2, v4}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v2, p3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setErrorCode(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v4

    if-eqz v1, :cond_1

    iget v2, v1, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->attempts:I

    :goto_0
    invoke-virtual {v4, v2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setAttempts(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 612
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 615
    if-eqz p2, :cond_2

    .line 616
    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 617
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->writeAccountStatus(Ljava/lang/String;)V

    .line 623
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->stopServiceIfDone()V

    .line 624
    return-void

    .end local v0    # "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    :cond_1
    move v2, v3

    .line 608
    goto :goto_0

    .line 619
    .restart local v0    # "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    :cond_2
    if-eqz v1, :cond_0

    .line 620
    iput-boolean v3, v1, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->inFlight:Z

    goto :goto_1
.end method

.method public finishBitmap(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 902
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mBitmapStatusMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 903
    invoke-virtual {p0}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->stopServiceIfDone()V

    .line 904
    return-void
.end method

.method public finishPackage(Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "success"    # Z
    .param p3, "willRetry"    # Z

    .prologue
    .line 813
    if-eqz p2, :cond_3

    .line 814
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mSucceeded:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mSucceeded:I

    .line 819
    :cond_0
    :goto_0
    if-nez p2, :cond_1

    if-nez p3, :cond_2

    .line 820
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->writePackageStatus(Ljava/lang/String;)V

    .line 823
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->stopServiceIfDone()V

    .line 824
    return-void

    .line 815
    :cond_3
    if-nez p3, :cond_0

    .line 816
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mFailed:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mFailed:I

    goto :goto_0
.end method

.method public initAccountStore(Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onComplete"    # Ljava/lang/Runnable;

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    if-nez v0, :cond_0

    .line 514
    new-instance v0, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    new-instance v1, Lcom/google/android/finsky/utils/persistence/FileBasedKeyValueStore;

    const-string v2, "RestoreTracker"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    const-string v3, "account-"

    invoke-direct {v1, v2, v3}, Lcom/google/android/finsky/utils/persistence/FileBasedKeyValueStore;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;-><init>(Lcom/google/android/finsky/utils/persistence/KeyValueStore;)V

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    .line 517
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    new-instance v1, Lcom/google/android/finsky/services/RestoreService$RestoreTracker$1;

    invoke-direct {v1, p0, p2}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker$1;-><init>(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;->load(Ljava/lang/Runnable;)V

    .line 545
    :goto_0
    return-void

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStore:Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;

    invoke-virtual {v0, p2}, Lcom/google/android/finsky/utils/persistence/WriteThroughKeyValueStore;->load(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public isAccountInFlight(Ljava/lang/String;)Z
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 645
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;

    .line 646
    .local v0, "status":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->inFlight:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onInstallPackageEvent(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 16
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p3, "statusCode"    # I

    .prologue
    .line 984
    const/4 v8, 0x0

    .line 985
    .local v8, "cancelBitmap":Z
    sget-object v2, Lcom/google/android/finsky/services/RestoreService$5;->$SwitchMap$com$google$android$finsky$installer$InstallerListener$InstallerPackageEvent:[I

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1047
    const-string v2, "enum %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1052
    :goto_0
    :pswitch_0
    if-eqz v8, :cond_0

    .line 1053
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/finsky/services/RestoreService;->cancelBitmapDownload(Ljava/lang/String;)V
    invoke-static {v2, v0}, Lcom/google/android/finsky/services/RestoreService;->access$1800(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;)V

    .line 1055
    :cond_0
    return-void

    .line 994
    :pswitch_1
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->trackPackageForListener(Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 998
    :pswitch_2
    const-string v2, "Restore package %s download cancelled"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 999
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->trackPackageForListener(Ljava/lang/String;ZZ)V

    .line 1000
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->finishPackage(Ljava/lang/String;ZZ)V

    .line 1001
    const/4 v8, 0x1

    .line 1002
    goto :goto_0

    .line 1004
    :pswitch_3
    const-string v2, "Restore package %s download error %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1005
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    move/from16 v0, p3

    # invokes: Lcom/google/android/finsky/services/RestoreService;->inErrorRetryBlacklist(I)Z
    invoke-static {v2, v0}, Lcom/google/android/finsky/services/RestoreService;->access$1200(Lcom/google/android/finsky/services/RestoreService;I)Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->tryAgainPackage(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v15, 0x1

    .line 1007
    .local v15, "willRetry":Z
    :goto_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v15}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->trackPackageForListener(Ljava/lang/String;ZZ)V

    .line 1008
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v15}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->finishPackage(Ljava/lang/String;ZZ)V

    .line 1009
    if-eqz v15, :cond_3

    .line 1010
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/finsky/services/RestoreService;->getPackageRestoreIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    invoke-static {v2, v0}, Lcom/google/android/finsky/services/RestoreService;->access$1400(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    .line 1012
    .local v9, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .line 1013
    .local v14, "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    if-eqz v14, :cond_2

    iget v2, v14, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->priority:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    sget-object v2, Lcom/google/android/finsky/config/G;->appRestoreRetryDownloadHoldoffHighPriorityMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    :goto_2
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 1016
    .local v10, "delay":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->jitterDelay(J)J
    invoke-static {v3, v10, v11}, Lcom/google/android/finsky/services/RestoreService;->access$1500(Lcom/google/android/finsky/services/RestoreService;J)J

    move-result-wide v4

    # invokes: Lcom/google/android/finsky/services/RestoreService;->setAlarm(Landroid/content/Intent;J)J
    invoke-static {v2, v9, v4, v5}, Lcom/google/android/finsky/services/RestoreService;->access$1600(Lcom/google/android/finsky/services/RestoreService;Landroid/content/Intent;J)J

    move-result-wide v12

    .line 1017
    .local v12, "retryTime":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v12, v13}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->writePackageRetryTime(Ljava/lang/String;J)V

    .line 1019
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v2

    const-wide/16 v4, 0x0

    iget-object v6, v14, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->title:Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-interface/range {v2 .. v7}, Lcom/google/android/finsky/receivers/Installer;->promiseInstall(Ljava/lang/String;JLjava/lang/String;Landroid/graphics/Bitmap;)V

    .line 1022
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    iget-object v3, v14, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->appIconUrl:Ljava/lang/String;

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/finsky/services/RestoreService;->startBitmapDownload(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v0, v3}, Lcom/google/android/finsky/services/RestoreService;->access$1700(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1005
    .end local v9    # "intent":Landroid/content/Intent;
    .end local v10    # "delay":J
    .end local v12    # "retryTime":J
    .end local v14    # "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    .end local v15    # "willRetry":Z
    :cond_1
    const/4 v15, 0x0

    goto :goto_1

    .line 1013
    .restart local v9    # "intent":Landroid/content/Intent;
    .restart local v14    # "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    .restart local v15    # "willRetry":Z
    :cond_2
    sget-object v2, Lcom/google/android/finsky/config/G;->appRestoreRetryDownloadHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    goto :goto_2

    .line 1027
    .end local v9    # "intent":Landroid/content/Intent;
    .end local v14    # "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    :cond_3
    const/4 v8, 0x1

    .line 1029
    goto/16 :goto_0

    .line 1031
    .end local v15    # "willRetry":Z
    :pswitch_4
    const-string v2, "Restore package %s install error %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1033
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->trackPackageForListener(Ljava/lang/String;ZZ)V

    .line 1034
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->finishPackage(Ljava/lang/String;ZZ)V

    .line 1035
    const/4 v8, 0x1

    .line 1036
    goto/16 :goto_0

    .line 1038
    :pswitch_5
    const-string v2, "Restore package %s install complete"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1039
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->trackPackageForListener(Ljava/lang/String;ZZ)V

    .line 1040
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->finishPackage(Ljava/lang/String;ZZ)V

    .line 1041
    const/4 v8, 0x1

    .line 1042
    goto/16 :goto_0

    .line 985
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method

.method public shouldHold(Ljava/lang/String;)Z
    .locals 6
    .param p1, "ignorePackage"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 957
    iget-object v5, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 958
    iget-object v5, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;

    .line 959
    .local v0, "accountFetchStatus":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    iget-boolean v5, v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->inFlight:Z

    if-eqz v5, :cond_0

    .line 977
    .end local v0    # "accountFetchStatus":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    .end local v2    # "i$":Ljava/util/Iterator;
    :goto_0
    return v4

    .line 965
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 966
    iget-object v5, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 967
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;>;"
    if-eqz p1, :cond_3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 970
    :cond_3
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .line 971
    .local v3, "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    iget v5, v3, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->priority:I

    if-ne v5, v4, :cond_2

    goto :goto_0

    .line 977
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    :cond_4
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public startAccount(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "aid"    # Ljava/lang/String;

    .prologue
    .line 587
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;

    .line 588
    .local v0, "status":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    if-nez v0, :cond_0

    .line 589
    new-instance v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;

    .end local v0    # "status":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;-><init>(Lcom/google/android/finsky/services/RestoreService$1;)V

    .line 590
    .restart local v0    # "status":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->attempts:I

    .line 591
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 593
    :cond_0
    iget v1, v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->attempts:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->attempts:I

    .line 594
    iput-object p2, v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->androidId:Ljava/lang/String;

    .line 595
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->inFlight:Z

    .line 596
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->writeAccountStatus(Ljava/lang/String;)V

    .line 597
    return-void
.end method

.method public startBitmap(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "bitmapUrl"    # Ljava/lang/String;

    .prologue
    .line 886
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mBitmapStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;

    .line 887
    .local v0, "status":Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;
    if-nez v0, :cond_0

    .line 888
    new-instance v0, Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;

    .end local v0    # "status":Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;-><init>(Lcom/google/android/finsky/services/RestoreService$1;)V

    .line 889
    .restart local v0    # "status":Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;
    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;->attempts:I

    .line 890
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mBitmapStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 892
    :cond_0
    iget v1, v0, Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;->attempts:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;->attempts:I

    .line 893
    iput-object p2, v0, Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;->bitmapUrl:Ljava/lang/String;

    .line 894
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;->retryTime:J

    .line 895
    return-void
.end method

.method public startPackage(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "versionCode"    # I
    .param p3, "accountName"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "priority"    # I
    .param p6, "deliveryToken"    # Ljava/lang/String;
    .param p7, "visible"    # Z
    .param p8, "appIconUrl"    # Ljava/lang/String;

    .prologue
    .line 789
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .line 790
    .local v0, "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    if-nez v0, :cond_0

    .line 791
    new-instance v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .end local v0    # "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;-><init>(Lcom/google/android/finsky/services/RestoreService$1;)V

    .line 792
    .restart local v0    # "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->attempts:I

    .line 793
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 795
    :cond_0
    iget v1, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->attempts:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->attempts:I

    .line 796
    iput p2, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->versionCode:I

    .line 797
    iput-object p3, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->accountName:Ljava/lang/String;

    .line 798
    iput-object p4, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->title:Ljava/lang/String;

    .line 799
    iput p5, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->priority:I

    .line 800
    iput-object p6, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->deliveryToken:Ljava/lang/String;

    .line 801
    iput-boolean p7, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->visible:Z

    .line 802
    iput-object p8, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->appIconUrl:Ljava/lang/String;

    .line 803
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->retryTime:J

    .line 804
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->writePackageStatus(Ljava/lang/String;)V

    .line 805
    return-void
.end method

.method public stopServiceIfDone()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 932
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mBitmapStatusMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mStartupRefCount:I

    if-gtz v0, :cond_0

    .line 934
    const-string v0, "Restore complete with %d success and %d failed."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mSucceeded:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mFailed:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 935
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->notifyListener(ILjava/lang/String;Ljava/lang/String;Z)V
    invoke-static {v0, v4, v5, v5, v3}, Lcom/google/android/finsky/services/RestoreService;->access$1000(Lcom/google/android/finsky/services/RestoreService;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 937
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mServiceStartId:I
    invoke-static {v1}, Lcom/google/android/finsky/services/RestoreService;->access$1100(Lcom/google/android/finsky/services/RestoreService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/services/RestoreService;->stopSelf(I)V

    .line 939
    :cond_0
    return-void
.end method

.method public tryAgainBitmap(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 914
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mBitmapStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;

    .line 915
    .local v0, "status":Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;
    if-nez v0, :cond_0

    move v1, v2

    .line 928
    :goto_0
    return v1

    .line 918
    :cond_0
    iget v4, v0, Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;->attempts:I

    sget-object v1, Lcom/google/android/finsky/config/G;->appRestoreAppIconMaxAttempts:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt v4, v1, :cond_1

    .line 919
    const-string v1, "Reached limit %d for %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, v0, Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;->attempts:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p1, v4, v3

    invoke-static {v1, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v2

    .line 920
    goto :goto_0

    .line 922
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    .line 923
    goto :goto_0

    .line 925
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v1

    if-eqz v1, :cond_3

    move v1, v2

    .line 926
    goto :goto_0

    :cond_3
    move v1, v3

    .line 928
    goto :goto_0
.end method

.method public tryAgainOrDeleteAccount(Ljava/lang/String;)Z
    .locals 6
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 630
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;

    .line 631
    .local v0, "status":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    if-eqz v0, :cond_0

    iget v4, v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->attempts:I

    sget-object v1, Lcom/google/android/finsky/config/G;->appRestoreFetchListMaxAttempts:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt v4, v1, :cond_0

    .line 632
    const-string v1, "Reached limit %d for %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, v0, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->attempts:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {p1}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v1, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 634
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 635
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->writeAccountStatus(Ljava/lang/String;)V

    move v1, v2

    .line 638
    :goto_0
    return v1

    :cond_0
    move v1, v3

    goto :goto_0
.end method

.method public tryAgainPackage(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 845
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .line 846
    .local v0, "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    if-nez v0, :cond_0

    move v1, v2

    .line 853
    :goto_0
    return v1

    .line 849
    :cond_0
    iget v4, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->attempts:I

    sget-object v1, Lcom/google/android/finsky/config/G;->appRestoreDownloadMaxAttempts:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt v4, v1, :cond_1

    .line 850
    const-string v1, "Reached limit %d for %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->attempts:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p1, v4, v3

    invoke-static {v1, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v2

    .line 851
    goto :goto_0

    :cond_1
    move v1, v3

    .line 853
    goto :goto_0
.end method

.method public writePackageRetryTime(Ljava/lang/String;J)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "retryTime"    # J

    .prologue
    .line 830
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .line 831
    .local v0, "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    if-nez v0, :cond_0

    .line 832
    const-string v1, "Unexpected missing package %s, can\'t write retry time"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 837
    :goto_0
    return-void

    .line 835
    :cond_0
    iput-wide p2, v0, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->retryTime:J

    .line 836
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->writePackageStatus(Ljava/lang/String;)V

    goto :goto_0
.end method
