.class public interface abstract Lcom/google/android/finsky/activities/ViewPagerTab;
.super Ljava/lang/Object;
.source "ViewPagerTab.java"


# virtual methods
.method public abstract getView(I)Landroid/view/View;
.end method

.method public abstract onDestroy()V
.end method

.method public abstract onRestoreInstanceState(Lcom/google/android/finsky/utils/ObjectMap;)V
.end method

.method public abstract onSaveInstanceState()Lcom/google/android/finsky/utils/ObjectMap;
.end method

.method public abstract setTabSelected(Z)V
.end method
