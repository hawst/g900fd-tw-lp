.class public Lcom/google/android/finsky/setup/SetupWizardNavBar;
.super Landroid/app/Fragment;
.source "SetupWizardNavBar.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/setup/SetupWizardNavBar$NavButton;
    }
.end annotation


# instance fields
.field private mBackButton:Landroid/widget/Button;

.field private mNavigationBarView:Landroid/view/ViewGroup;

.field private mNextButton:Landroid/widget/Button;

.field private mSystemUiFlags:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 50
    const/16 v0, 0x1202

    iput v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mSystemUiFlags:I

    .line 58
    return-void
.end method

.method private getNavbarTheme()I
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v5, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getActivity()Landroid/app/Activity;

    move-result-object v6

    new-array v7, v9, [I

    fill-array-data v7, :array_0

    invoke-virtual {v6, v7}, Landroid/app/Activity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 147
    .local v0, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 148
    .local v4, "theme":I
    if-nez v4, :cond_0

    .line 151
    new-array v2, v9, [F

    .line 152
    .local v2, "foregroundHsv":[F
    new-array v1, v9, [F

    .line 153
    .local v1, "backgroundHsv":[F
    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v6

    invoke-static {v6, v2}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 154
    invoke-virtual {v0, v8, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v6

    invoke-static {v6, v1}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 155
    aget v6, v2, v8

    aget v7, v1, v8

    cmpl-float v6, v6, v7

    if-lez v6, :cond_1

    .line 156
    .local v3, "isDarkBg":Z
    :goto_0
    if-eqz v3, :cond_2

    const v4, 0x7f0d019e

    .line 159
    .end local v1    # "backgroundHsv":[F
    .end local v2    # "foregroundHsv":[F
    .end local v3    # "isDarkBg":Z
    :cond_0
    :goto_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 160
    return v4

    .restart local v1    # "backgroundHsv":[F
    .restart local v2    # "foregroundHsv":[F
    :cond_1
    move v3, v5

    .line 155
    goto :goto_0

    .line 156
    .restart local v3    # "isDarkBg":Z
    :cond_2
    const v4, 0x7f0d019f

    goto :goto_1

    .line 142
    :array_0
    .array-data 4
        0x7f010199
        0x1010030
        0x1010031
    .end array-data
.end method


# virtual methods
.method public getBackButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mBackButton:Landroid/widget/Button;

    return-object v0
.end method

.method public getNextButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNextButton:Landroid/widget/Button;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getNavbarTheme()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 64
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 65
    const v1, 0x7f0401a4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNavigationBarView:Landroid/view/ViewGroup;

    .line 67
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNavigationBarView:Landroid/view/ViewGroup;

    const v2, 0x7f0a00f8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNextButton:Landroid/widget/Button;

    .line 68
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNavigationBarView:Landroid/view/ViewGroup;

    const v2, 0x7f0a0115

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mBackButton:Landroid/widget/Button;

    .line 69
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->resetButtonsState()V

    .line 70
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNavigationBarView:Landroid/view/ViewGroup;

    return-object v1
.end method

.method public onPreDraw()Z
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNavigationBarView:Landroid/view/ViewGroup;

    iget v1, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mSystemUiFlags:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 93
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNavigationBarView:Landroid/view/ViewGroup;

    iget v2, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mSystemUiFlags:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    .line 97
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNavigationBarView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 98
    .local v0, "viewTreeObserver":Landroid/view/ViewTreeObserver;
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 99
    return-void
.end method

.method public resetButtonsState()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNavigationBarView:Landroid/view/ViewGroup;

    const v1, 0x7f0a00f8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNextButton:Landroid/widget/Button;

    .line 78
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNextButton:Landroid/widget/Button;

    const v1, 0x7f0c03fd

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNextButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 81
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mBackButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/finsky/setup/SetupWizardNavBar$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/setup/SetupWizardNavBar$1;-><init>(Lcom/google/android/finsky/setup/SetupWizardNavBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mBackButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 88
    return-void
.end method

.method public setUseImmersiveMode(Z)V
    .locals 0
    .param p1, "useImmersiveMode"    # Z

    .prologue
    .line 115
    invoke-virtual {p0, p1, p1}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->setUseImmersiveMode(ZZ)V

    .line 116
    return-void
.end method

.method public setUseImmersiveMode(ZZ)V
    .locals 2
    .param p1, "useImmersiveMode"    # Z
    .param p2, "layoutHideNavigation"    # Z

    .prologue
    const/high16 v1, -0x80000000

    .line 119
    if-eqz p1, :cond_1

    .line 120
    iget v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mSystemUiFlags:I

    or-int/lit16 v0, v0, 0x1002

    iput v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mSystemUiFlags:I

    .line 121
    if-eqz p2, :cond_0

    .line 122
    iget v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mSystemUiFlags:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mSystemUiFlags:I

    .line 126
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 135
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mNavigationBarView:Landroid/view/ViewGroup;

    iget v1, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mSystemUiFlags:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    .line 136
    return-void

    .line 129
    :cond_1
    iget v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mSystemUiFlags:I

    and-int/lit16 v0, v0, -0x1203

    iput v0, p0, Lcom/google/android/finsky/setup/SetupWizardNavBar;->mSystemUiFlags:I

    .line 132
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method
