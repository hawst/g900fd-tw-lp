.class public final Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV1.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OBSOLETE_FinskyDoc"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;


# instance fields
.field public aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

.field public availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

.field public child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

.field public docid:Lcom/google/android/finsky/protos/Common$Docid;

.field public fetchDocid:Lcom/google/android/finsky/protos/Common$Docid;

.field public hasTitle:Z

.field public hasUrl:Z

.field public image:[Lcom/google/android/finsky/protos/Common$Image;

.field public offer:[Lcom/google/android/finsky/protos/Common$Offer;

.field public priceDeprecated:Lcom/google/android/finsky/protos/Common$Offer;

.field public sampleDocid:Lcom/google/android/finsky/protos/Common$Docid;

.field public title:Ljava/lang/String;

.field public translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 561
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 562
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->clear()Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    .line 563
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
    .locals 2

    .prologue
    .line 512
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    if-nez v0, :cond_1

    .line 513
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 515
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    if-nez v0, :cond_0

    .line 516
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    sput-object v0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    .line 518
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 520
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    return-object v0

    .line 518
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 566
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 567
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->fetchDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 568
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->sampleDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 569
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->title:Ljava/lang/String;

    .line 570
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->hasTitle:Z

    .line 571
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->url:Ljava/lang/String;

    .line 572
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->hasUrl:Z

    .line 573
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    .line 574
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->priceDeprecated:Lcom/google/android/finsky/protos/Common$Offer;

    .line 575
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Offer;->emptyArray()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    .line 576
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    .line 577
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Image;->emptyArray()[Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    .line 578
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    .line 579
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    .line 580
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->cachedSize:I

    .line 581
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 648
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 649
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v3, :cond_0

    .line 650
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 653
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->fetchDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v3, :cond_1

    .line 654
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->fetchDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 657
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->sampleDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v3, :cond_2

    .line 658
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->sampleDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 661
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->hasTitle:Z

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 662
    :cond_3
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 665
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->hasUrl:Z

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->url:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 666
    :cond_5
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->url:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 669
    :cond_6
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->priceDeprecated:Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v3, :cond_7

    .line 670
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->priceDeprecated:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 673
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    if-eqz v3, :cond_8

    .line 674
    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 677
    :cond_8
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-lez v3, :cond_a

    .line 678
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-ge v1, v3, :cond_a

    .line 679
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v3, v1

    .line 680
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_9

    .line 681
    const/16 v3, 0xa

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 678
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 686
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_a
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    array-length v3, v3

    if-lez v3, :cond_c

    .line 687
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    array-length v3, v3

    if-ge v1, v3, :cond_c

    .line 688
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    aget-object v0, v3, v1

    .line 689
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
    if-eqz v0, :cond_b

    .line 690
    const/16 v3, 0xb

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 687
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 695
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
    .end local v1    # "i":I
    :cond_c
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    if-eqz v3, :cond_d

    .line 696
    const/16 v3, 0xd

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 699
    :cond_d
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v3, v3

    if-lez v3, :cond_f

    .line 700
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v3, v3

    if-ge v1, v3, :cond_f

    .line 701
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    aget-object v0, v3, v1

    .line 702
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v0, :cond_e

    .line 703
    const/16 v3, 0xe

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 700
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 708
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v1    # "i":I
    :cond_f
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    array-length v3, v3

    if-lez v3, :cond_11

    .line 709
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    array-length v3, v3

    if-ge v1, v3, :cond_11

    .line 710
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    aget-object v0, v3, v1

    .line 711
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;
    if-eqz v0, :cond_10

    .line 712
    const/16 v3, 0xf

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 709
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 717
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;
    .end local v1    # "i":I
    :cond_11
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 725
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 726
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 730
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 731
    :sswitch_0
    return-object p0

    .line 736
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v5, :cond_1

    .line 737
    new-instance v5, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 739
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 743
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->fetchDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v5, :cond_2

    .line 744
    new-instance v5, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->fetchDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 746
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->fetchDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 750
    :sswitch_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->sampleDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v5, :cond_3

    .line 751
    new-instance v5, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->sampleDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 753
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->sampleDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 757
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->title:Ljava/lang/String;

    .line 758
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->hasTitle:Z

    goto :goto_0

    .line 762
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->url:Ljava/lang/String;

    .line 763
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->hasUrl:Z

    goto :goto_0

    .line 767
    :sswitch_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->priceDeprecated:Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v5, :cond_4

    .line 768
    new-instance v5, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->priceDeprecated:Lcom/google/android/finsky/protos/Common$Offer;

    .line 770
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->priceDeprecated:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 774
    :sswitch_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    if-nez v5, :cond_5

    .line 775
    new-instance v5, Lcom/google/android/finsky/protos/FilterRules$Availability;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/FilterRules$Availability;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    .line 777
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 781
    :sswitch_8
    const/16 v5, 0x52

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 783
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-nez v5, :cond_7

    move v1, v4

    .line 784
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Common$Image;

    .line 786
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v1, :cond_6

    .line 787
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 789
    :cond_6
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_8

    .line 790
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v5, v2, v1

    .line 791
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 792
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 789
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 783
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v1, v5

    goto :goto_1

    .line 795
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_8
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v5, v2, v1

    .line 796
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 797
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    goto/16 :goto_0

    .line 801
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :sswitch_9
    const/16 v5, 0x5a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 803
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    if-nez v5, :cond_a

    move v1, v4

    .line 804
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    .line 806
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
    if-eqz v1, :cond_9

    .line 807
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 809
    :cond_9
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_b

    .line 810
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;-><init>()V

    aput-object v5, v2, v1

    .line 811
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 812
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 809
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 803
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
    :cond_a
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    array-length v1, v5

    goto :goto_3

    .line 815
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
    :cond_b
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;-><init>()V

    aput-object v5, v2, v1

    .line 816
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 817
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    goto/16 :goto_0

    .line 821
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
    :sswitch_a
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    if-nez v5, :cond_c

    .line 822
    new-instance v5, Lcom/google/android/finsky/protos/Rating$AggregateRating;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Rating$AggregateRating;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    .line 824
    :cond_c
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 828
    :sswitch_b
    const/16 v5, 0x72

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 830
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v5, :cond_e

    move v1, v4

    .line 831
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Common$Offer;

    .line 833
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v1, :cond_d

    .line 834
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 836
    :cond_d
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_f

    .line 837
    new-instance v5, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    aput-object v5, v2, v1

    .line 838
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 839
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 836
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 830
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_e
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v1, v5

    goto :goto_5

    .line 842
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_f
    new-instance v5, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    aput-object v5, v2, v1

    .line 843
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 844
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    goto/16 :goto_0

    .line 848
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :sswitch_c
    const/16 v5, 0x7a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 850
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    if-nez v5, :cond_11

    move v1, v4

    .line 851
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    .line 853
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;
    if-eqz v1, :cond_10

    .line 854
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 856
    :cond_10
    :goto_8
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_12

    .line 857
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;-><init>()V

    aput-object v5, v2, v1

    .line 858
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 859
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 856
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 850
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;
    :cond_11
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    array-length v1, v5

    goto :goto_7

    .line 862
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;
    :cond_12
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;-><init>()V

    aput-object v5, v2, v1

    .line 863
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 864
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    goto/16 :goto_0

    .line 726
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x6a -> :sswitch_a
        0x72 -> :sswitch_b
        0x7a -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 506
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 587
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v2, :cond_0

    .line 588
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 590
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->fetchDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v2, :cond_1

    .line 591
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->fetchDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 593
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->sampleDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v2, :cond_2

    .line 594
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->sampleDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 596
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->hasTitle:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 597
    :cond_3
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 599
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->hasUrl:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->url:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 600
    :cond_5
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->url:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 602
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->priceDeprecated:Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v2, :cond_7

    .line 603
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->priceDeprecated:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 605
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    if-eqz v2, :cond_8

    .line 606
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 608
    :cond_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 609
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 610
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v2, v1

    .line 611
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_9

    .line 612
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 609
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 616
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_a
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 617
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    .line 618
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->child:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    aget-object v0, v2, v1

    .line 619
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
    if-eqz v0, :cond_b

    .line 620
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 617
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 624
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;
    .end local v1    # "i":I
    :cond_c
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    if-eqz v2, :cond_d

    .line 625
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 627
    :cond_d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v2, v2

    if-lez v2, :cond_f

    .line 628
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v2, v2

    if-ge v1, v2, :cond_f

    .line 629
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    aget-object v0, v2, v1

    .line 630
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v0, :cond_e

    .line 631
    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 628
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 635
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v1    # "i":I
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    array-length v2, v2

    if-lez v2, :cond_11

    .line 636
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 637
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;->translatedSnippet:[Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;

    aget-object v0, v2, v1

    .line 638
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;
    if-eqz v0, :cond_10

    .line 639
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 636
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 643
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyTranslatedText;
    .end local v1    # "i":I
    :cond_11
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 644
    return-void
.end method
