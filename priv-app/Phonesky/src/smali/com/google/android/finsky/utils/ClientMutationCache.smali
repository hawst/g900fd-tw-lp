.class public Lcom/google/android/finsky/utils/ClientMutationCache;
.super Ljava/lang/Object;
.source "ClientMutationCache.java"


# static fields
.field private static final MAX_NUM_CACHED_CIRCLES_MODEL:I


# instance fields
.field private mAgeVerificationRequired:Ljava/lang/Boolean;

.field private mCachedCirclesModels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/model/CirclesModel;",
            ">;"
        }
    .end annotation
.end field

.field private mDismissedRecommendationDocIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mReviewedDocIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/protos/DocumentV2$Review;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/finsky/config/G;->clientMutationCacheCirclesModelCacheSize:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/utils/ClientMutationCache;->MAX_NUM_CACHED_CIRCLES_MODEL:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mReviewedDocIds:Ljava/util/Map;

    .line 25
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mDismissedRecommendationDocIds:Ljava/util/Set;

    .line 30
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mCachedCirclesModels:Ljava/util/List;

    return-void
.end method

.method private putCirclesModel(Lcom/google/android/finsky/model/CirclesModel;)V
    .locals 2
    .param p1, "circlesModel"    # Lcom/google/android/finsky/model/CirclesModel;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mCachedCirclesModels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sget v1, Lcom/google/android/finsky/utils/ClientMutationCache;->MAX_NUM_CACHED_CIRCLES_MODEL:I

    if-lt v0, v1, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mCachedCirclesModels:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mCachedCirclesModels:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    return-void
.end method


# virtual methods
.method public dismissRecommendation(Ljava/lang/String;)V
    .locals 1
    .param p1, "docId"    # Ljava/lang/String;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mDismissedRecommendationDocIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 109
    return-void
.end method

.method public getCachedCirclesModel(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Lcom/google/android/finsky/model/CirclesModel;
    .locals 5
    .param p1, "targetPerson"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "ownerAccountName"    # Ljava/lang/String;

    .prologue
    .line 132
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mCachedCirclesModels:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 133
    iget-object v3, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mCachedCirclesModels:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/model/CirclesModel;

    .line 135
    .local v0, "cachedModel":Lcom/google/android/finsky/model/CirclesModel;
    invoke-virtual {v0}, Lcom/google/android/finsky/model/CirclesModel;->getTargetPersonDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/model/CirclesModel;->getOwnerAccountName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 138
    iget-object v3, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mCachedCirclesModels:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 139
    iget-object v3, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mCachedCirclesModels:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    .end local v0    # "cachedModel":Lcom/google/android/finsky/model/CirclesModel;
    :goto_1
    return-object v0

    .line 132
    .restart local v0    # "cachedModel":Lcom/google/android/finsky/model/CirclesModel;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 144
    .end local v0    # "cachedModel":Lcom/google/android/finsky/model/CirclesModel;
    :cond_1
    new-instance v1, Lcom/google/android/finsky/model/CirclesModel;

    invoke-direct {v1, p1, p2}, Lcom/google/android/finsky/model/CirclesModel;-><init>(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)V

    .line 145
    .local v1, "circlesModel":Lcom/google/android/finsky/model/CirclesModel;
    invoke-direct {p0, v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->putCirclesModel(Lcom/google/android/finsky/model/CirclesModel;)V

    move-object v0, v1

    .line 147
    goto :goto_1
.end method

.method public getCachedReview(Ljava/lang/String;Lcom/google/android/finsky/protos/DocumentV2$Review;)Lcom/google/android/finsky/protos/DocumentV2$Review;
    .locals 1
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "defaultReview"    # Lcom/google/android/finsky/protos/DocumentV2$Review;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mReviewedDocIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mReviewedDocIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 43
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public isAgeVerificationRequired()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mAgeVerificationRequired:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 92
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeToc;->isAgeVerificationRequired()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mAgeVerificationRequired:Ljava/lang/Boolean;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mAgeVerificationRequired:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isDismissedRecommendation(Ljava/lang/String;)Z
    .locals 1
    .param p1, "docId"    # Ljava/lang/String;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mDismissedRecommendationDocIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeCachedReview(Ljava/lang/String;)V
    .locals 1
    .param p1, "docId"    # Ljava/lang/String;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mReviewedDocIds:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    return-void
.end method

.method public setAgeVerificationRequired(Z)V
    .locals 1
    .param p1, "ageVerificationRequired"    # Z

    .prologue
    .line 84
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mAgeVerificationRequired:Ljava/lang/Boolean;

    .line 85
    return-void
.end method

.method public updateCachedReview(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;)V
    .locals 5
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "rating"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "comment"    # Ljava/lang/String;
    .param p5, "author"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v4, 0x1

    .line 60
    new-instance v0, Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/DocumentV2$Review;-><init>()V

    .line 61
    .local v0, "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    iput p2, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    .line 62
    iput-boolean v4, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasStarRating:Z

    .line 63
    iput-object p3, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    .line 64
    iput-boolean v4, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTitle:Z

    .line 65
    iput-object p4, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    .line 66
    iput-boolean v4, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasComment:Z

    .line 67
    invoke-virtual {p5}, Lcom/google/android/finsky/api/model/Document;->getBackingDocV2()Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->timestampMsec:J

    .line 69
    iput-boolean v4, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTimestampMsec:Z

    .line 70
    iget-object v1, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mReviewedDocIds:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    return-void
.end method

.method public updateCachedReviewDeleted(Ljava/lang/String;)V
    .locals 2
    .param p1, "docId"    # Ljava/lang/String;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/utils/ClientMutationCache;->mReviewedDocIds:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    return-void
.end method
