.class public Lcom/google/android/finsky/api/DfeClearCacheRequest;
.super Lcom/android/volley/Request;
.source "DfeClearCacheRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/volley/Request",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCache:Lcom/android/volley/Cache;

.field private final mCacheKey:Ljava/lang/String;

.field private final mCallback:Ljava/lang/Runnable;

.field private final mFullExpire:Z


# direct methods
.method public constructor <init>(Lcom/android/volley/Cache;Ljava/lang/String;ZLjava/lang/Runnable;)V
    .locals 2
    .param p1, "cache"    # Lcom/android/volley/Cache;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "fullExpire"    # Z
    .param p4, "callback"    # Ljava/lang/Runnable;

    .prologue
    const/4 v1, 0x0

    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/android/volley/Request;-><init>(ILjava/lang/String;Lcom/android/volley/Response$ErrorListener;)V

    .line 29
    iput-object p1, p0, Lcom/google/android/finsky/api/DfeClearCacheRequest;->mCache:Lcom/android/volley/Cache;

    .line 30
    iput-object p2, p0, Lcom/google/android/finsky/api/DfeClearCacheRequest;->mCacheKey:Ljava/lang/String;

    .line 31
    iput-boolean p3, p0, Lcom/google/android/finsky/api/DfeClearCacheRequest;->mFullExpire:Z

    .line 32
    iput-object p4, p0, Lcom/google/android/finsky/api/DfeClearCacheRequest;->mCallback:Ljava/lang/Runnable;

    .line 33
    return-void
.end method


# virtual methods
.method protected deliverResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "response"    # Ljava/lang/Object;

    .prologue
    .line 58
    return-void
.end method

.method public getPriority()Lcom/android/volley/Request$Priority;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/android/volley/Request$Priority;->IMMEDIATE:Lcom/android/volley/Request$Priority;

    return-object v0
.end method

.method public isCanceled()Z
    .locals 4

    .prologue
    .line 38
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeClearCacheRequest;->mCache:Lcom/android/volley/Cache;

    iget-object v2, p0, Lcom/google/android/finsky/api/DfeClearCacheRequest;->mCacheKey:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/finsky/api/DfeClearCacheRequest;->mFullExpire:Z

    invoke-interface {v1, v2, v3}, Lcom/android/volley/Cache;->invalidate(Ljava/lang/String;Z)V

    .line 39
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeClearCacheRequest;->mCallback:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 40
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 41
    .local v0, "handler":Landroid/os/Handler;
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeClearCacheRequest;->mCallback:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 43
    .end local v0    # "handler":Landroid/os/Handler;
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 1
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method
