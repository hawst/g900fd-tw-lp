.class Lcom/google/android/finsky/installer/PackageInstallerImpl$2;
.super Landroid/content/BroadcastReceiver;
.source "PackageInstallerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/installer/PackageInstallerImpl;->getCommitCallback(Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)Landroid/content/IntentSender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/installer/PackageInstallerImpl;

.field final synthetic val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

.field final synthetic val$packageName:Ljava/lang/String;

.field final synthetic val$sessionId:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/installer/PackageInstallerImpl;Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V
    .locals 0

    .prologue
    .line 408
    iput-object p1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$2;->this$0:Lcom/google/android/finsky/installer/PackageInstallerImpl;

    iput-object p2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$2;->val$packageName:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$2;->val$sessionId:I

    iput-object p4, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$2;->val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$2;->this$0:Lcom/google/android/finsky/installer/PackageInstallerImpl;

    # getter for: Lcom/google/android/finsky/installer/PackageInstallerImpl;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->access$200(Lcom/google/android/finsky/installer/PackageInstallerImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 412
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$2;->this$0:Lcom/google/android/finsky/installer/PackageInstallerImpl;

    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$2;->val$packageName:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$2;->val$sessionId:I

    iget-object v3, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$2;->val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    # invokes: Lcom/google/android/finsky/installer/PackageInstallerImpl;->handleCommitCallback(Landroid/content/Intent;Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V
    invoke-static {v0, p2, v1, v2, v3}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->access$300(Lcom/google/android/finsky/installer/PackageInstallerImpl;Landroid/content/Intent;Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V

    .line 413
    return-void
.end method
