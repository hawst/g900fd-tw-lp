.class final Lcom/google/android/finsky/utils/PurchaseInitiator$1;
.super Ljava/lang/Object;
.source "PurchaseInitiator.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/PurchaseInitiator;->createFreePurchaseListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;ZZ)Lcom/android/volley/Response$Listener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Buy$BuyResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$continueUrl:Ljava/lang/String;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$initiateAppDownload:Z

.field final synthetic val$offerType:I

.field final synthetic val$showErrors:Z

.field final synthetic val$successListener:Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;


# direct methods
.method constructor <init>(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;ZLcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;Z)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$account:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput p3, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$offerType:I

    iput-object p4, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$continueUrl:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$initiateAppDownload:Z

    iput-object p6, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$successListener:Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;

    iput-boolean p7, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$showErrors:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/Buy$BuyResponse;)V
    .locals 12
    .param p1, "buyResponse"    # Lcom/google/android/finsky/protos/Buy$BuyResponse;

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v8, -0x1

    .line 95
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    iget-object v10, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$account:Landroid/accounts/Account;

    invoke-virtual {v7, v10}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    .line 96
    .local v1, "eventLog":Lcom/google/android/finsky/analytics/FinskyEventLog;
    iget-object v7, p1, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseResponse:Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    if-eqz v7, :cond_3

    .line 97
    iget-object v0, p1, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseResponse:Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    .line 99
    .local v0, "purchaseResponse":Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;
    iget v7, v0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->status:I

    if-nez v7, :cond_2

    .line 100
    iget-object v7, p1, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    if-eqz v7, :cond_1

    .line 102
    iget-object v7, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$offerType:I

    iget-object v7, p1, Lcom/google/android/finsky/protos/Buy$BuyResponse;->serverLogsCookie:[B

    move-wide v10, v8

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPurchaseBackgroundEvent(ILjava/lang/String;ILjava/lang/String;I[BJJ)V

    .line 106
    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$account:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iget-object v4, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$continueUrl:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    iget-boolean v6, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$initiateAppDownload:Z

    const-string v7, "free_purchase"

    iget-object v8, p1, Lcom/google/android/finsky/protos/Buy$BuyResponse;->encodedDeliveryToken:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/utils/PurchaseInitiator;->processPurchaseStatusResponse(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;ZLjava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v2 .. v8}, Lcom/google/android/finsky/utils/PurchaseInitiator;->access$000(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;ZLjava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$successListener:Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;

    if-eqz v2, :cond_0

    .line 110
    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$successListener:Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;

    iget-object v5, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$account:Landroid/accounts/Account;

    iget-object v6, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-interface {v2, v5, v6}, Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;->onFreePurchaseSuccess(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)V

    .line 134
    .end local v0    # "purchaseResponse":Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;
    :cond_0
    :goto_0
    return-void

    .line 114
    .restart local v0    # "purchaseResponse":Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;
    :cond_1
    const-string v2, "Expected PurchaseStatusResponse."

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 119
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$offerType:I

    iget v6, v0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->status:I

    move-object v7, v5

    move-wide v10, v8

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPurchaseBackgroundEvent(ILjava/lang/String;ILjava/lang/String;I[BJJ)V

    .line 123
    iget-boolean v2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$showErrors:Z

    if-eqz v2, :cond_0

    .line 124
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    const v5, 0x7f0c01e0

    invoke-virtual {v2, v5}, Lcom/google/android/finsky/FinskyApp;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 125
    .local v3, "error":Ljava/lang/String;
    iget-object v4, v0, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;->localizedErrorMessage:Ljava/lang/String;

    .line 126
    .local v4, "errorMessage":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    iget-object v5, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v7

    move-object v5, v4

    invoke-interface/range {v2 .. v7}, Lcom/google/android/finsky/utils/Notifier;->showPurchaseErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    .end local v0    # "purchaseResponse":Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;
    .end local v3    # "error":Ljava/lang/String;
    .end local v4    # "errorMessage":Ljava/lang/String;
    :cond_3
    const-string v2, "Expected PurchaseResponse."

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 92
    check-cast p1, Lcom/google/android/finsky/protos/Buy$BuyResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->onResponse(Lcom/google/android/finsky/protos/Buy$BuyResponse;)V

    return-void
.end method
