.class public final Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BuyInstruments.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/BuyInstruments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GetInitialInstrumentFlowStateResponse"
.end annotation


# instance fields
.field public createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

.field public createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

.field public hasResult:Z

.field public hasUserMessageHtml:Z

.field public result:I

.field public userMessageHtml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 360
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 361
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->clear()Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    .line 362
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 365
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->result:I

    .line 366
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->hasResult:Z

    .line 367
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->userMessageHtml:Ljava/lang/String;

    .line 368
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->hasUserMessageHtml:Z

    .line 369
    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    .line 370
    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    .line 371
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->cachedSize:I

    .line 372
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 395
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 396
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->result:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->hasResult:Z

    if-eqz v1, :cond_1

    .line 397
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->result:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 400
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->hasUserMessageHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->userMessageHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 401
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->userMessageHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 404
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    if-eqz v1, :cond_4

    .line 405
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 408
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    if-eqz v1, :cond_5

    .line 409
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 412
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 420
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 421
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 425
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 426
    :sswitch_0
    return-object p0

    .line 431
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 432
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 435
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->result:I

    .line 436
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->hasResult:Z

    goto :goto_0

    .line 442
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->userMessageHtml:Ljava/lang/String;

    .line 443
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->hasUserMessageHtml:Z

    goto :goto_0

    .line 447
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    if-nez v2, :cond_1

    .line 448
    new-instance v2, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    .line 450
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 454
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    if-nez v2, :cond_2

    .line 455
    new-instance v2, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    .line 457
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 421
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 432
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 325
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 378
    iget v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->result:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->hasResult:Z

    if-eqz v0, :cond_1

    .line 379
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->result:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 381
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->hasUserMessageHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->userMessageHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 382
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->userMessageHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 384
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    if-eqz v0, :cond_4

    .line 385
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 387
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    if-eqz v0, :cond_5

    .line 388
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 390
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 391
    return-void
.end method
