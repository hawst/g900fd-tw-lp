.class public Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
.source "AuthChallengeStep.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/finsky/auth/AuthResponseListener;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
        "<",
        "Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/CompoundButton$OnCheckedChangeListener;",
        "Lcom/google/android/finsky/auth/AuthResponseListener;",
        "Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;"
    }
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

.field private mClientLoginApi:Lcom/google/android/finsky/billing/challenge/ClientLoginApi;

.field private mErrorMessage:Ljava/lang/String;

.field private mErrorView:Landroid/widget/TextView;

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mFailedCount:I

.field private mHelpToggle:Landroid/widget/ImageView;

.field private mIsOptOutChecked:Z

.field private mIsPasswordHelpExpanded:Z

.field private mMainView:Landroid/view/View;

.field private mOptOutCheckbox:Landroid/widget/CheckBox;

.field private mOptOutInfo:Landroid/widget/TextView;

.field private mPasswordRecoveryView:Landroid/widget/TextView;

.field private mPasswordView:Landroid/widget/EditText;

.field private mPurchaseDisclaimer:Landroid/widget/TextView;

.field private final mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mUseGmsCoreForAuth:Z

.field private mUsePinBasedAuth:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;-><init>()V

    .line 111
    const/16 v0, 0x2ee

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->logClickAndVerifyGaia(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->fail()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->logClick(IZ)V

    return-void
.end method

.method private changePasswordHelpAndPurchaseDisclaimer(Z)V
    .locals 0
    .param p1, "expanded"    # Z

    .prologue
    .line 567
    iput-boolean p1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsPasswordHelpExpanded:Z

    .line 568
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->updatePasswordHelpAndPurchaseDisclaimer()V

    .line 569
    return-void
.end method

.method private fail()V
    .locals 1

    .prologue
    .line 475
    const v0, 0x7f0c00db

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->fail(I)V

    .line 476
    return-void
.end method

.method private fail(I)V
    .locals 2
    .param p1, "errorStringId"    # I

    .prologue
    .line 479
    iget v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mFailedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mFailedCount:I

    .line 480
    const/16 v0, 0x1fc

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->logBackgroundEvent(IZ)V

    .line 481
    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mFailedCount:I

    sget-object v0, Lcom/google/android/finsky/config/G;->passwordMaxFailedAttempts:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 482
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->failWithMaxAttemptsExceeded()V

    .line 492
    :goto_0
    return-void

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordView:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 487
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->showErrorMessage(Ljava/lang/String;)V

    .line 488
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->hideLoading()V

    .line 491
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordView:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->showKeyboard(Landroid/app/Activity;Landroid/widget/EditText;)V

    goto :goto_0
.end method

.method private failWithMaxAttemptsExceeded()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 495
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getRecoveryUrl()Ljava/lang/String;

    move-result-object v2

    .line 496
    .local v2, "recoveryUrl":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUsePinBasedAuth:Z

    if-eqz v3, :cond_0

    const v3, 0x7f0c00fc

    :goto_0
    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 499
    .local v1, "maxAttemptsExceededHtml":Ljava/lang/String;
    const v3, 0x7f0c01e0

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1, v6}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ErrorStep;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ErrorStep;

    move-result-object v0

    .line 501
    .local v0, "errorStep":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ErrorStep;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    .line 502
    return-void

    .line 496
    .end local v0    # "errorStep":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ErrorStep;
    .end local v1    # "maxAttemptsExceededHtml":Ljava/lang/String;
    :cond_0
    const v3, 0x7f0c00fb

    goto :goto_0
.end method

.method private getAuthContext()Lcom/google/android/finsky/analytics/PlayStore$AuthContext;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 522
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;-><init>()V

    .line 523
    .local v0, "authContext":Lcom/google/android/finsky/analytics/PlayStore$AuthContext;
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUsePinBasedAuth:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    :goto_0
    iput v1, v0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->mode:I

    .line 524
    iput-boolean v2, v0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->hasMode:Z

    .line 525
    return-object v0

    :cond_0
    move v1, v2

    .line 523
    goto :goto_0
.end method

.method private getPurchaseChallengeText(I)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "resId"    # I

    .prologue
    .line 615
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "SETTINGS_ACTIVITY_SENTINEL"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/finsky/config/G;->gaiaOptOutLearnMoreLink:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, p1, v2}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 617
    .local v1, "localized":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 618
    .local v0, "html":Ljava/lang/CharSequence;
    const-string v2, "SETTINGS_ACTIVITY_SENTINEL"

    new-instance v3, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$3;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$3;-><init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;)V

    invoke-static {v0, v2, v3}, Lcom/google/android/play/utils/UrlSpanUtils;->selfishifyUrlSpans(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/play/utils/UrlSpanUtils$Listener;)V

    .line 628
    return-object v0
.end method

.method private getRecoveryUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 277
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUsePinBasedAuth:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/finsky/config/G;->resetPinUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v0, v1

    .line 278
    .local v0, "recoveryUrl":Ljava/lang/String;
    :goto_0
    const-string v1, "%email%"

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 277
    .end local v0    # "recoveryUrl":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/google/android/finsky/config/G;->passwordRecoveryUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v0, v1

    goto :goto_0
.end method

.method private isGaiaChallengeOverrideExperimentEnabled()Z
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutCheckbox:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logBackgroundEvent(IZ)V
    .locals 3
    .param p1, "eventType"    # I
    .param p2, "operationSuccess"    # Z

    .prologue
    .line 505
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setOperationSuccess(Z)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getAuthContext()Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setAuthContext(Lcom/google/android/finsky/analytics/PlayStore$AuthContext;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 508
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 509
    return-void
.end method

.method private logClick(IZ)V
    .locals 2
    .param p1, "leafType"    # I
    .param p2, "isImeAction"    # Z

    .prologue
    const/4 v1, 0x1

    .line 512
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    .line 513
    .local v0, "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    if-eqz p2, :cond_0

    .line 514
    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->isImeAction:Z

    .line 515
    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasIsImeAction:Z

    .line 517
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getAuthContext()Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    .line 518
    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->logClick(ILcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;)V

    .line 519
    return-void
.end method

.method private logClickAndVerifyGaia(Z)V
    .locals 4
    .param p1, "isImeAction"    # Z

    .prologue
    .line 341
    const/16 v2, 0x2ef

    invoke-direct {p0, v2, p1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->logClick(IZ)V

    .line 342
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordView:Landroid/widget/EditText;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/UiUtils;->hideKeyboard(Landroid/app/Activity;Landroid/view/View;)V

    .line 343
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->showLoading()V

    .line 344
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 345
    .local v1, "userInput":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUseGmsCoreForAuth:Z

    if-eqz v2, :cond_1

    .line 346
    new-instance v0, Lcom/google/android/finsky/auth/GmsCoreAuthApi;

    invoke-direct {v0}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;-><init>()V

    .line 347
    .local v0, "authApi":Lcom/google/android/finsky/auth/GmsCoreAuthApi;
    iget-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUsePinBasedAuth:Z

    if-eqz v2, :cond_0

    .line 348
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v2, v1, p0}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;->validateUserPin(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V

    .line 355
    .end local v0    # "authApi":Lcom/google/android/finsky/auth/GmsCoreAuthApi;
    :goto_0
    return-void

    .line 350
    .restart local v0    # "authApi":Lcom/google/android/finsky/auth/GmsCoreAuthApi;
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v2, v1, p0}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;->validateUserPassword(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V

    goto :goto_0

    .line 353
    .end local v0    # "authApi":Lcom/google/android/finsky/auth/GmsCoreAuthApi;
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mClientLoginApi:Lcom/google/android/finsky/billing/challenge/ClientLoginApi;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    invoke-virtual {v2, v3, v1, p0}, Lcom/google/android/finsky/billing/challenge/ClientLoginApi;->validateUser(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)Lcom/android/volley/Request;

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;ZZ)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;
    .param p2, "useGmsCoreForAuth"    # Z
    .param p3, "usePinBasedAuth"    # Z

    .prologue
    .line 125
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 126
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v2, "AuthChallengeStep.challenge"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 128
    const-string v2, "AuthChallengeStep.useGmsCoreForAuth"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 129
    const-string v2, "AuthChallengeStep.usePinBasedAuth"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 130
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;-><init>()V

    .line 131
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->setArguments(Landroid/os/Bundle;)V

    .line 132
    return-object v1
.end method

.method private showErrorMessage(Ljava/lang/String;)V
    .locals 2
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 596
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mErrorMessage:Ljava/lang/String;

    .line 597
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mErrorView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 598
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mErrorView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 599
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mErrorView:Landroid/widget/TextView;

    invoke-static {v0, p1, v1}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 600
    return-void
.end method

.method private updatePasswordHelpAndPurchaseDisclaimer()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 572
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/finsky/config/PurchaseAuth;->getPurchaseAuthType(Ljava/lang/String;)I

    move-result v1

    .line 573
    .local v1, "purchaseAuth":I
    const/4 v0, 0x1

    .line 574
    .local v0, "canShowDisclaimer":Z
    if-nez v1, :cond_0

    .line 575
    const/4 v0, 0x0

    .line 589
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPurchaseDisclaimer:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsPasswordHelpExpanded:Z

    if-eqz v3, :cond_3

    move v3, v4

    :goto_1
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 591
    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordRecoveryView:Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsPasswordHelpExpanded:Z

    if-eqz v3, :cond_4

    move v3, v4

    :goto_2
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 592
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mHelpToggle:Landroid/widget/ImageView;

    iget-boolean v6, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsPasswordHelpExpanded:Z

    if-eqz v6, :cond_5

    :goto_3
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 593
    return-void

    .line 578
    :cond_0
    if-ne v1, v6, :cond_1

    .line 579
    const v2, 0x7f0c02d5

    .line 587
    .local v2, "resId":I
    :goto_4
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPurchaseDisclaimer:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getPurchaseChallengeText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 580
    .end local v2    # "resId":I
    :cond_1
    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    .line 581
    const v2, 0x7f0c02d6

    .restart local v2    # "resId":I
    goto :goto_4

    .line 583
    .end local v2    # "resId":I
    :cond_2
    const-string v3, "Unexpected value for PurchaseAuth message %d"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v3, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 584
    const v2, 0x7f0c02d6

    .line 585
    .restart local v2    # "resId":I
    const/4 v0, 0x0

    goto :goto_4

    .end local v2    # "resId":I
    :cond_3
    move v3, v5

    .line 589
    goto :goto_1

    :cond_4
    move v3, v5

    .line 591
    goto :goto_2

    :cond_5
    move v5, v4

    .line 592
    goto :goto_3
.end method


# virtual methods
.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 173
    const v0, 0x7f0c010b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 530
    const/16 v0, 0x65

    if-ne p1, v0, :cond_1

    .line 531
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 532
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->onAuthSuccess()V

    .line 539
    :goto_0
    return-void

    .line 534
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->fail()V

    goto :goto_0

    .line 537
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAuthFailure(I)V
    .locals 8
    .param p1, "errorType"    # I

    .prologue
    .line 405
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->isResumed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 406
    const-string v3, "Not resumed, ignoring auth challenge failure."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 472
    :goto_0
    return-void

    .line 410
    :cond_0
    const/4 v3, 0x2

    if-eq p1, v3, :cond_1

    const/4 v3, 0x6

    if-ne p1, v3, :cond_2

    .line 415
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->onAuthSuccess()V

    goto :goto_0

    .line 419
    :cond_2
    const/4 v3, 0x4

    if-ne p1, v3, :cond_4

    .line 420
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->changePasswordHelpAndPurchaseDisclaimer(Z)V

    .line 421
    iget-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUsePinBasedAuth:Z

    if-eqz v3, :cond_3

    const v6, 0x7f0c00fa

    .line 423
    .local v6, "errorStringId":I
    :goto_1
    invoke-direct {p0, v6}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->fail(I)V

    goto :goto_0

    .line 421
    .end local v6    # "errorStringId":I
    :cond_3
    const v6, 0x7f0c00f9

    goto :goto_1

    .line 427
    :cond_4
    const/4 v3, 0x3

    if-eq p1, v3, :cond_5

    .line 428
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->fail()V

    goto :goto_0

    .line 433
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    .line 434
    .local v1, "account":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 435
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 436
    .local v2, "options":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordView:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 437
    .local v7, "password":Ljava/lang/String;
    const-string v3, "password"

    invoke-virtual {v2, v3, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    new-instance v4, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$2;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$2;-><init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;)V

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManager;->confirmCredentials(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method public onAuthSuccess()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 364
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->isResumed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 365
    const-string v4, "Not resumed, ignoring auth challenge success."

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 401
    :goto_0
    return-void

    .line 369
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    .line 370
    .local v3, "purchaseFragment":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 371
    .local v0, "challengeResponse":Landroid/os/Bundle;
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseAuthenticationTypeParam:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iget v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->authenticationType:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseRetryCountParam:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mFailedCount:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    iget-boolean v4, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsOptOutChecked:Z

    if-eqz v4, :cond_2

    .line 377
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/finsky/config/PurchaseAuth;->getPurchaseAuthType(Ljava/lang/String;)I

    move-result v2

    .line 378
    .local v2, "previousSetting":I
    if-nez v2, :cond_1

    .line 379
    const-string v4, "Got through auth while opted out? Previous=%d"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-string v7, "purchase-auth-screen"

    invoke-static {v4, v8, v5, v6, v7}, Lcom/google/android/finsky/config/PurchaseAuth;->setAndLogPurchaseAuth(Ljava/lang/String;ILjava/lang/Integer;Lcom/google/android/finsky/analytics/FinskyEventLog;Ljava/lang/String;)V

    .line 385
    .end local v2    # "previousSetting":I
    :cond_2
    sget-object v4, Lcom/google/android/finsky/utils/FinskyPreferences;->lastGaiaAuthTimestamp:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v1

    .line 387
    .local v1, "lastGaiaAuthTimestamp":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Long;>;"
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/finsky/config/PurchaseAuth;->getPurchaseAuthType(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v9, :cond_4

    .line 390
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 399
    :cond_3
    :goto_1
    const/16 v4, 0x1fc

    invoke-direct {p0, v4, v9}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->logBackgroundEvent(IZ)V

    .line 400
    invoke-virtual {v3, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->answerChallenge(Landroid/os/Bundle;)Z

    goto :goto_0

    .line 395
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 396
    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    goto :goto_1
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v1, 0x0

    .line 543
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mOptOutCheckbox:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_0

    .line 545
    const/16 v0, 0x2f1

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->logClick(IZ)V

    .line 546
    iput-boolean p2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsOptOutChecked:Z

    .line 547
    if-eqz p2, :cond_1

    .line 550
    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->changePasswordHelpAndPurchaseDisclaimer(Z)V

    .line 551
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mOptOutInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 553
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mOptOutInfo:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 560
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mHelpToggle:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 561
    const/16 v0, 0x2f0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->logClick(IZ)V

    .line 562
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->changePasswordHelpAndPurchaseDisclaimer(Z)V

    .line 564
    :cond_0
    return-void
.end method

.method public onContinueButtonClicked()V
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->logClickAndVerifyGaia(Z)V

    .line 360
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 137
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "AuthChallengeStep.usePinBasedAuth"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUsePinBasedAuth:Z

    .line 138
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v0, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    iput-object v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v0, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getAuthContext()Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    .line 145
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    .line 148
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "AuthChallengeStep.challenge"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    .line 149
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "AuthChallengeStep.useGmsCoreForAuth"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUseGmsCoreForAuth:Z

    .line 150
    iput-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsOptOutChecked:Z

    .line 151
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 152
    if-eqz p1, :cond_1

    .line 153
    const-string v0, "AuthChallengeStep.retryCount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mFailedCount:I

    .line 154
    const-string v0, "AuthChallengeStep.optOutSelected"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsOptOutChecked:Z

    .line 155
    const-string v0, "AuthChallengeStep.passwordHelpExpanded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsPasswordHelpExpanded:Z

    .line 156
    const-string v0, "AuthChallengeStep.errorMessage"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mErrorMessage:Ljava/lang/String;

    .line 159
    :cond_1
    new-instance v0, Lcom/google/android/finsky/billing/challenge/ClientLoginApi;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/finsky/billing/challenge/ClientLoginApi;-><init>(Lcom/android/volley/RequestQueue;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mClientLoginApi:Lcom/google/android/finsky/billing/challenge/ClientLoginApi;

    .line 160
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 17
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 179
    const v14, 0x7f0400c9

    const/4 v15, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v14, v1, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    .line 185
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUsePinBasedAuth:Z

    if-eqz v14, :cond_3

    .line 186
    const v6, 0x7f0a024b

    .line 187
    .local v6, "editTextInputResourceId":I
    const v13, 0x7f0c02cf

    .line 188
    .local v13, "titleViewStringResourceId":I
    const v7, 0x7f0c0110

    .line 189
    .local v7, "helpToggleContentDescriptionResourceId":I
    const v10, 0x7f0c02d1

    .line 197
    .local v10, "recoveryMessageResourceId":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    invoke-virtual {v14, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/EditText;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordView:Landroid/widget/EditText;

    .line 198
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordView:Landroid/widget/EditText;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/EditText;->setVisibility(I)V

    .line 200
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordView:Landroid/widget/EditText;

    new-instance v15, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;)V

    invoke-virtual {v14, v15}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 210
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordView:Landroid/widget/EditText;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0900fc

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getColor(I)I

    move-result v15

    invoke-virtual {v14, v15}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 212
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a009c

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 213
    .local v12, "titleView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a024c

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mHelpToggle:Landroid/widget/ImageView;

    .line 215
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mHelpToggle:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mHelpToggle:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 218
    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getRecoveryUrl()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v14}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 219
    .local v9, "recoveryMessageHtml":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a024d

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordRecoveryView:Landroid/widget/TextView;

    .line 220
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordRecoveryView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 221
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordRecoveryView:Landroid/widget/TextView;

    invoke-static {v9}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a024e

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPurchaseDisclaimer:Landroid/widget/TextView;

    .line 224
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPurchaseDisclaimer:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a01cd

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mErrorView:Landroid/widget/TextView;

    .line 227
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mErrorMessage:Ljava/lang/String;

    if-eqz v14, :cond_0

    .line 228
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mErrorMessage:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->showErrorMessage(Ljava/lang/String;)V

    .line 237
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a020d

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mOptOutInfo:Landroid/widget/TextView;

    .line 238
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mOptOutInfo:Landroid/widget/TextView;

    const v15, 0x7f0c02d4

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getPurchaseChallengeText(I)Ljava/lang/CharSequence;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mOptOutInfo:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 241
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a024f

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mOptOutCheckbox:Landroid/widget/CheckBox;

    .line 242
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->isGaiaChallengeOverrideExperimentEnabled()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 244
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mOptOutCheckbox:Landroid/widget/CheckBox;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 253
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a022b

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 254
    .local v3, "challengeDescription":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaDescriptionTextHtml:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v14

    invoke-virtual {v3, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 256
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a0249

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;

    .line 258
    .local v5, "documentInfoLayout":Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->documentTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iget-object v15, v15, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->formattedPrice:Ljava/lang/String;

    invoke-virtual {v5, v14, v15}, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->setDocumentInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    invoke-virtual {v5}, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->getVisibility()I

    move-result v14

    if-nez v14, :cond_5

    const/4 v11, 0x1

    .line 262
    .local v11, "showChallengeContextView":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->instrumentDisplayTitle:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 263
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a024a

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 264
    .local v8, "instrumentTitle":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->instrumentDisplayTitle:Ljava/lang/String;

    invoke-virtual {v8, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    const/4 v14, 0x0

    invoke-virtual {v8, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 266
    const/4 v11, 0x1

    .line 269
    .end local v8    # "instrumentTitle":Landroid/widget/TextView;
    :cond_1
    if-eqz v11, :cond_2

    .line 270
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a0248

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 271
    .local v2, "challengeContextView":Landroid/view/View;
    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Landroid/view/View;->setVisibility(I)V

    .line 273
    .end local v2    # "challengeContextView":Landroid/view/View;
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    return-object v14

    .line 191
    .end local v3    # "challengeDescription":Landroid/widget/TextView;
    .end local v5    # "documentInfoLayout":Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;
    .end local v6    # "editTextInputResourceId":I
    .end local v7    # "helpToggleContentDescriptionResourceId":I
    .end local v9    # "recoveryMessageHtml":Ljava/lang/String;
    .end local v10    # "recoveryMessageResourceId":I
    .end local v11    # "showChallengeContextView":Z
    .end local v12    # "titleView":Landroid/widget/TextView;
    .end local v13    # "titleViewStringResourceId":I
    :cond_3
    const v6, 0x7f0a01fc

    .line 192
    .restart local v6    # "editTextInputResourceId":I
    const v13, 0x7f0c02ce

    .line 193
    .restart local v13    # "titleViewStringResourceId":I
    const v7, 0x7f0c010f

    .line 194
    .restart local v7    # "helpToggleContentDescriptionResourceId":I
    const v10, 0x7f0c02d0

    .restart local v10    # "recoveryMessageResourceId":I
    goto/16 :goto_0

    .line 246
    .restart local v9    # "recoveryMessageHtml":Ljava/lang/String;
    .restart local v12    # "titleView":Landroid/widget/TextView;
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    iget-object v4, v14, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutCheckbox:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    .line 247
    .local v4, "checkboxFromDfe":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mOptOutCheckbox:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 248
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mOptOutCheckbox:Landroid/widget/CheckBox;

    iget-object v15, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->description:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 249
    iget-boolean v14, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->checked:Z

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsOptOutChecked:Z

    .line 250
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mOptOutCheckbox:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsOptOutChecked:Z

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_1

    .line 261
    .end local v4    # "checkboxFromDfe":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    .restart local v3    # "challengeDescription":Landroid/widget/TextView;
    .restart local v5    # "documentInfoLayout":Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;
    :cond_5
    const/4 v11, 0x0

    goto :goto_2
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 297
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onResume()V

    .line 300
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 301
    .local v0, "context":Landroid/content/Context;
    iget-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mUsePinBasedAuth:Z

    if-eqz v2, :cond_0

    const v2, 0x7f0c02cf

    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 303
    .local v1, "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mMainView:Landroid/view/View;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 304
    return-void

    .line 301
    .end local v1    # "title":Ljava/lang/String;
    :cond_0
    const v2, 0x7f0c02ce

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 164
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 165
    const-string v0, "AuthChallengeStep.retryCount"

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mFailedCount:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 166
    const-string v0, "AuthChallengeStep.optOutSelected"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsOptOutChecked:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 167
    const-string v0, "AuthChallengeStep.passwordHelpExpanded"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mIsPasswordHelpExpanded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 168
    const-string v0, "AuthChallengeStep.errorMessage"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mErrorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 308
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onStart()V

    .line 314
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mAccountName:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/finsky/config/PurchaseAuth;->getPurchaseAuthType(Ljava/lang/String;)I

    move-result v0

    .line 315
    .local v0, "purchaseAuth":I
    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->isGaiaChallengeOverrideExperimentEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 317
    const-string v2, "PurchaseAuth changed to never prompt for password"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 320
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    .line 321
    .local v1, "purchaseFragment":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    invoke-virtual {v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->preparePurchase()V

    .line 326
    .end local v1    # "purchaseFragment":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    :goto_0
    return-void

    .line 324
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->updatePasswordHelpAndPurchaseDisclaimer()V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 284
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 288
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 291
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->mPasswordView:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->showKeyboard(Landroid/app/Activity;Landroid/widget/EditText;)V

    .line 293
    :cond_0
    return-void
.end method
