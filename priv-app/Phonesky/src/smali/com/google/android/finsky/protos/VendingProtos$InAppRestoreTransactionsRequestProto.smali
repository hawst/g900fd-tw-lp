.class public final Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InAppRestoreTransactionsRequestProto"
.end annotation


# instance fields
.field public billingApiVersion:I

.field public hasBillingApiVersion:Z

.field public hasNonce:Z

.field public hasSignatureAlgorithm:Z

.field public nonce:J

.field public signatureAlgorithm:Ljava/lang/String;

.field public signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3558
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3559
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    .line 3560
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3563
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    .line 3564
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->nonce:J

    .line 3565
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasNonce:Z

    .line 3566
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureAlgorithm:Ljava/lang/String;

    .line 3567
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasSignatureAlgorithm:Z

    .line 3568
    iput v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->billingApiVersion:I

    .line 3569
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasBillingApiVersion:Z

    .line 3570
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->cachedSize:I

    .line 3571
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 3594
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3595
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    if-eqz v1, :cond_0

    .line 3596
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3599
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasNonce:Z

    if-nez v1, :cond_1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->nonce:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 3600
    :cond_1
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->nonce:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3603
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasSignatureAlgorithm:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureAlgorithm:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 3604
    :cond_3
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureAlgorithm:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3607
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasBillingApiVersion:Z

    if-nez v1, :cond_5

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->billingApiVersion:I

    if-eqz v1, :cond_6

    .line 3608
    :cond_5
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->billingApiVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3611
    :cond_6
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 3619
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3620
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3624
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3625
    :sswitch_0
    return-object p0

    .line 3630
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    if-nez v1, :cond_1

    .line 3631
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    .line 3633
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3637
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->nonce:J

    .line 3638
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasNonce:Z

    goto :goto_0

    .line 3642
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureAlgorithm:Ljava/lang/String;

    .line 3643
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasSignatureAlgorithm:Z

    goto :goto_0

    .line 3647
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->billingApiVersion:I

    .line 3648
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasBillingApiVersion:Z

    goto :goto_0

    .line 3620
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3526
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3577
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    if-eqz v0, :cond_0

    .line 3578
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3580
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasNonce:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->nonce:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 3581
    :cond_1
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->nonce:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 3583
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasSignatureAlgorithm:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureAlgorithm:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 3584
    :cond_3
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->signatureAlgorithm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3586
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->hasBillingApiVersion:Z

    if-nez v0, :cond_5

    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->billingApiVersion:I

    if-eqz v0, :cond_6

    .line 3587
    :cond_5
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsRequestProto;->billingApiVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3589
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3590
    return-void
.end method
