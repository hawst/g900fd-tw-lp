.class public final Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Countries"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    }
.end annotation


# instance fields
.field public country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1348
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1349
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->clear()Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    .line 1350
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;
    .locals 1

    .prologue
    .line 1353
    invoke-static {}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    .line 1354
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->cachedSize:I

    .line 1355
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    .line 1374
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 1375
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 1376
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 1377
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    aget-object v0, v3, v1

    .line 1378
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    if-eqz v0, :cond_0

    .line 1379
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1376
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1384
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    .end local v1    # "i":I
    :cond_1
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x0

    .line 1392
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1393
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1397
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1398
    :sswitch_0
    return-object p0

    .line 1403
    :sswitch_1
    const/16 v5, 0x13

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1405
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    if-nez v5, :cond_2

    move v1, v4

    .line 1406
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    .line 1408
    .local v2, "newArray":[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    if-eqz v1, :cond_1

    .line 1409
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1411
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1412
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;-><init>()V

    aput-object v5, v2, v1

    .line 1413
    aget-object v5, v2, v1

    invoke-virtual {p1, v5, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 1414
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1411
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1405
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    array-length v1, v5

    goto :goto_1

    .line 1417
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;-><init>()V

    aput-object v5, v2, v1

    .line 1418
    aget-object v5, v2, v1

    invoke-virtual {p1, v5, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 1419
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    goto :goto_0

    .line 1393
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1044
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1361
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1362
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 1363
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;->country:[Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    aget-object v0, v2, v1

    .line 1364
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    if-eqz v0, :cond_0

    .line 1365
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1362
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1369
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1370
    return-void
.end method
