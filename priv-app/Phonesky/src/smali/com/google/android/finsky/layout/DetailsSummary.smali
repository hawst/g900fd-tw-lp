.class public Lcom/google/android/finsky/layout/DetailsSummary;
.super Landroid/view/ViewGroup;
.source "DetailsSummary.java"

# interfaces
.implements Lcom/google/android/finsky/layout/DetailsPartialFadeSection;
.implements Lcom/google/android/finsky/layout/DetailsPeekingSection;
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoTopSeparator;


# instance fields
.field private mBackgroundLayer:Landroid/view/View;

.field private mBylines:Landroid/view/View;

.field private final mContentMargin:I

.field private mCreatorBlock:Landroid/view/View;

.field private mCreatorPanel:Landroid/view/View;

.field private mDetailsSummaryDynamic:Landroid/view/View;

.field private mExtraLabels:Landroid/view/View;

.field private mExtraLabelsBottom:Landroid/view/View;

.field private mIsExtraLabelsInCompactMode:Z

.field private final mOldWishlistArea:Landroid/graphics/Rect;

.field private mThumbnailFrame:Landroid/view/View;

.field private mThumbnailMode:I

.field private mThumbnailVerticalPeek:I

.field private mTipperSticker:Landroid/view/View;

.field private mTitle:Landroid/view/View;

.field private final mTitleOffset:I

.field private final mWishlistArea:Landroid/graphics/Rect;

.field private mWishlistButton:Landroid/view/View;

.field private final mWishlistTouchExtend:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsSummary;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 82
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b00ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    .line 83
    const v1, 0x7f0b00dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailVerticalPeek:I

    .line 84
    const v1, 0x7f0b00ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitleOffset:I

    .line 86
    const v1, 0x7f0b00de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistTouchExtend:I

    .line 87
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistArea:Landroid/graphics/Rect;

    .line 88
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mOldWishlistArea:Landroid/graphics/Rect;

    .line 90
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailMode:I

    .line 91
    return-void
.end method


# virtual methods
.method public addParticipatingChildViewIds(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 161
    .local p1, "participatingChildViewIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const v0, 0x7f0a0178

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    const v0, 0x7f0a0177

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    const v0, 0x7f0a0179

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    const v0, 0x7f0a019f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    const v0, 0x7f0a017c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    const v0, 0x7f0a017d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    const v0, 0x7f0a017e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    const v0, 0x7f0a0183

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    const v0, 0x7f0a017f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    return-void
.end method

.method public addParticipatingChildViews(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "participatingChildViewList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitle:Landroid/view/View;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistButton:Landroid/view/View;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorPanel:Landroid/view/View;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mTipperSticker:Landroid/view/View;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    return-void
.end method

.method public getBackground()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public getTopPeekAmount()I
    .locals 2

    .prologue
    .line 124
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailVerticalPeek:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 97
    const v0, 0x7f0a0176

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    .line 98
    const v0, 0x7f0a0186

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailFrame:Landroid/view/View;

    .line 99
    const v0, 0x7f0a0178

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitle:Landroid/view/View;

    .line 100
    const v0, 0x7f0a0177

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistButton:Landroid/view/View;

    .line 101
    const v0, 0x7f0a0179

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorPanel:Landroid/view/View;

    .line 102
    const v0, 0x7f0a019f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    .line 103
    const v0, 0x7f0a017c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mTipperSticker:Landroid/view/View;

    .line 104
    const v0, 0x7f0a017d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    .line 105
    const v0, 0x7f0a017e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    .line 106
    const v0, 0x7f0a0183

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    .line 107
    const v0, 0x7f0a017f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    .line 108
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 25
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 396
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsSummary;->getWidth()I

    move-result v17

    .line 397
    .local v17, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsSummary;->getHeight()I

    move-result v9

    .line 399
    .local v9, "height":I
    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    .line 400
    .local v10, "mainContentX":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsSummary;->getPaddingTop()I

    move-result v11

    .line 401
    .local v11, "mainContentY":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailMode:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 402
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v21, v0

    add-int v11, v11, v21

    .line 406
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailFrame:Landroid/view/View;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailFrame:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_1

    .line 407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailFrame:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    .line 409
    .local v15, "thumbnailFrameWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailMode:I

    move/from16 v21, v0

    if-nez v21, :cond_b

    const/16 v16, 0x0

    .line 411
    .local v16, "thumbnailFrameX":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailFrame:Landroid/view/View;

    move-object/from16 v21, v0

    add-int v22, v16, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailFrame:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v11

    move-object/from16 v0, v21

    move/from16 v1, v16

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v1, v11, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 414
    add-int v21, v16, v15

    add-int v10, v10, v21

    .line 417
    .end local v15    # "thumbnailFrameWidth":I
    .end local v16    # "thumbnailFrameX":I
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsSummary;->getTopPeekAmount()I

    move-result v21

    add-int v11, v11, v21

    .line 418
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailMode:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_2

    .line 419
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v21, v0

    add-int v11, v11, v21

    .line 423
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistButton:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_c

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistButton:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredWidth()I

    move-result v18

    .line 425
    .local v18, "wishlistButtonWidth":I
    sub-int v21, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v22, v0

    sub-int v19, v21, v22

    .line 426
    .local v19, "wishlistButtonX":I
    move/from16 v20, v11

    .line 427
    .local v20, "wishlistButtonY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistButton:Landroid/view/View;

    move-object/from16 v21, v0

    add-int v22, v19, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistButton:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v20

    move-object/from16 v0, v21

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistButton:Landroid/view/View;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistArea:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 434
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistArea:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistTouchExtend:I

    move/from16 v22, v0

    move/from16 v0, v22

    neg-int v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistTouchExtend:I

    move/from16 v23, v0

    move/from16 v0, v23

    neg-int v0, v0

    move/from16 v23, v0

    invoke-virtual/range {v21 .. v23}, Landroid/graphics/Rect;->inset(II)V

    .line 435
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistArea:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mOldWishlistArea:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_3

    .line 437
    new-instance v21, Lcom/google/android/play/utils/PlayTouchDelegate;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistArea:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistButton:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-direct/range {v21 .. v23}, Lcom/google/android/play/utils/PlayTouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsSummary;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 438
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mOldWishlistArea:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistArea:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 446
    .end local v18    # "wishlistButtonWidth":I
    .end local v19    # "wishlistButtonX":I
    .end local v20    # "wishlistButtonY":I
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitleOffset:I

    move/from16 v21, v0

    sub-int v11, v11, v21

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitle:Landroid/view/View;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitle:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    add-int v22, v22, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitle:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v11

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v10, v11, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitle:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    add-int v11, v11, v21

    .line 452
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorPanel:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_4

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorPanel:Landroid/view/View;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorPanel:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    add-int v22, v22, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorPanel:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v11

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v10, v11, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorPanel:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    add-int v11, v11, v21

    .line 460
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTipperSticker:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getVisibility()I

    move-result v21

    if-nez v21, :cond_5

    .line 461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTipperSticker:Landroid/view/View;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTipperSticker:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    add-int v22, v22, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTipperSticker:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v11

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v10, v11, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTipperSticker:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    add-int v11, v11, v21

    .line 468
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_6

    .line 469
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v21, v0

    div-int/lit8 v21, v21, 0x2

    add-int v11, v11, v21

    .line 470
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    add-int v22, v22, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v11

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v10, v11, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    add-int v11, v11, v21

    .line 477
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getVisibility()I

    move-result v21

    if-nez v21, :cond_7

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    add-int v22, v22, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v11

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v10, v11, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 481
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    add-int v11, v11, v21

    .line 486
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mIsExtraLabelsInCompactMode:Z

    move/from16 v21, v0

    if-eqz v21, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    sub-int v8, v11, v21

    .line 490
    .local v8, "extraLabelsY":I
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsSummary;->getPaddingBottom()I

    move-result v21

    sub-int v5, v9, v21

    .line 491
    .local v5, "bottomY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_e

    .line 492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getPaddingTop()I

    move-result v21

    sub-int v5, v5, v21

    .line 493
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    sub-int v23, v5, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getMeasuredWidth()I

    move-result v24

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/view/View;->layout(IIII)V

    .line 495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    sub-int v5, v5, v21

    .line 501
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_8

    .line 502
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    .line 503
    .local v12, "summaryDynamicHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    .line 504
    .local v13, "summaryDynamicWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v21, v0

    sub-int v21, v17, v21

    sub-int v14, v21, v13

    .line 505
    .local v14, "summaryDynamicX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v21, v0

    sub-int v22, v5, v12

    add-int v23, v14, v13

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v14, v1, v2, v5}, Landroid/view/View;->layout(IIII)V

    .line 508
    sub-int v21, v5, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v22

    sub-int v8, v21, v22

    .line 512
    .end local v12    # "summaryDynamicHeight":I
    .end local v13    # "summaryDynamicWidth":I
    .end local v14    # "summaryDynamicX":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_9

    .line 513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 514
    .local v6, "extraLabelsWidth":I
    sub-int v21, v17, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v22, v0

    sub-int v7, v21, v22

    .line 515
    .local v7, "extraLabelsX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v21, v0

    add-int v22, v7, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    add-int v23, v23, v8

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v7, v8, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 519
    .end local v6    # "extraLabelsWidth":I
    .end local v7    # "extraLabelsX":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getVisibility()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_a

    .line 520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3, v9}, Landroid/view/View;->layout(IIII)V

    .line 522
    :cond_a
    return-void

    .line 409
    .end local v5    # "bottomY":I
    .end local v8    # "extraLabelsY":I
    .restart local v15    # "thumbnailFrameWidth":I
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v16, v0

    goto/16 :goto_0

    .line 441
    .end local v15    # "thumbnailFrameWidth":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mOldWishlistArea:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->setEmpty()V

    .line 442
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsSummary;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto/16 :goto_1

    :cond_d
    move v8, v11

    .line 486
    goto/16 :goto_2

    .line 497
    .restart local v5    # "bottomY":I
    .restart local v8    # "extraLabelsY":I
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v21, v0

    sub-int v5, v5, v21

    goto/16 :goto_3
.end method

.method protected onMeasure(II)V
    .locals 21
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 215
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v16

    .line 219
    .local v16, "width":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v17, v0

    mul-int/lit8 v17, v17, 0x2

    sub-int v7, v16, v17

    .line 220
    .local v7, "mainContentWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsSummary;->getTopPeekAmount()I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v18, v0

    add-int v6, v17, v18

    .line 223
    .local v6, "mainContentHeight":I
    const/4 v13, 0x0

    .line 224
    .local v13, "thumbnailHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailFrame:Landroid/view/View;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailFrame:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_1

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailFrame:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    .line 226
    .local v12, "thumbnailFrameLp":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailFrame:Landroid/view/View;

    move-object/from16 v17, v0

    iget v0, v12, Landroid/view/ViewGroup$LayoutParams;->width:I

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    invoke-static/range {v18 .. v19}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    iget v0, v12, Landroid/view/ViewGroup$LayoutParams;->height:I

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailFrame:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredWidth()I

    move-result v17

    sub-int v7, v7, v17

    .line 230
    iget v13, v12, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 231
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailMode:I

    move/from16 v17, v0

    if-eqz v17, :cond_0

    .line 233
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v17, v0

    sub-int v7, v7, v17

    .line 235
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailMode:I

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 237
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v17, v0

    add-int v13, v13, v17

    .line 241
    .end local v12    # "thumbnailFrameLp":Landroid/view/ViewGroup$LayoutParams;
    :cond_1
    move v15, v7

    .line 244
    .local v15, "titleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistButton:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistButton:Landroid/view/View;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mWishlistButton:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredWidth()I

    move-result v17

    sub-int v15, v15, v17

    .line 250
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitle:Landroid/view/View;

    move-object/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    move/from16 v0, v18

    invoke-static {v15, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitle:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitleOffset:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    add-int v6, v6, v17

    .line 254
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTitle:Landroid/view/View;

    .line 257
    .local v2, "bottomMostContentView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorPanel:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_3

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorPanel:Landroid/view/View;

    move-object/from16 v17, v0

    const/high16 v18, -0x80000000

    move/from16 v0, v18

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorPanel:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    add-int v6, v6, v17

    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorPanel:Landroid/view/View;

    .line 266
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTipperSticker:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_4

    .line 267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTipperSticker:Landroid/view/View;

    move-object/from16 v17, v0

    const/high16 v18, -0x80000000

    move/from16 v0, v18

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTipperSticker:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    add-int v6, v6, v17

    .line 270
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mTipperSticker:Landroid/view/View;

    .line 274
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_5

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    move-object/from16 v17, v0

    const/high16 v18, -0x80000000

    move/from16 v0, v18

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v18, v0

    div-int/lit8 v18, v18, 0x2

    add-int v17, v17, v18

    add-int v6, v6, v17

    .line 280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    .line 284
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    if-nez v17, :cond_6

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    move-object/from16 v17, v0

    const/high16 v18, -0x80000000

    move/from16 v0, v18

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    add-int v6, v6, v17

    .line 288
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    .line 292
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    if-nez v17, :cond_7

    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v17, v0

    const/high16 v18, -0x80000000

    move/from16 v0, v18

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 297
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getMeasuredWidth()I

    move-result v18

    add-int v3, v17, v18

    .line 299
    .local v3, "combinedWidth":I
    if-gt v3, v7, :cond_b

    const/16 v17, 0x1

    :goto_0
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/layout/DetailsSummary;->mIsExtraLabelsInCompactMode:Z

    .line 300
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mIsExtraLabelsInCompactMode:Z

    move/from16 v17, v0

    if-eqz v17, :cond_c

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v18

    sub-int v17, v17, v18

    add-int v6, v6, v17

    .line 308
    .end local v3    # "combinedWidth":I
    :cond_7
    :goto_1
    const/4 v5, 0x0

    .line 314
    .local v5, "fullHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_f

    .line 315
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v17, v0

    mul-int/lit8 v17, v17, 0x2

    sub-int v8, v16, v17

    .line 316
    .local v8, "maxSummaryDynamicWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v17, v0

    const/high16 v18, -0x80000000

    move/from16 v0, v18

    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 320
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    .line 321
    .local v10, "summaryDynamicWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 322
    .local v9, "summaryDynamicHeight":I
    move v14, v13

    .line 326
    .local v14, "thumbnailHeightForDynamicFitting":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailMode:I

    move/from16 v17, v0

    if-eqz v17, :cond_8

    .line 327
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v17, v0

    sub-int v14, v14, v17

    .line 329
    :cond_8
    if-gt v10, v7, :cond_d

    add-int v17, v6, v9

    move/from16 v0, v17

    if-gt v0, v14, :cond_d

    .line 332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    move/from16 v0, v18

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 335
    move v5, v14

    .line 369
    .end local v8    # "maxSummaryDynamicWidth":I
    .end local v9    # "summaryDynamicHeight":I
    .end local v10    # "summaryDynamicWidth":I
    .end local v14    # "thumbnailHeightForDynamicFitting":I
    :cond_9
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_10

    .line 370
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    move-object/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    move/from16 v0, v16

    move/from16 v1, v18

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 372
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    add-int v5, v5, v17

    .line 375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabelsBottom:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getPaddingTop()I

    move-result v17

    add-int v5, v5, v17

    .line 382
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v17, v0

    add-int v5, v5, v17

    .line 384
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsSummary;->getPaddingTop()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsSummary;->getPaddingBottom()I

    move-result v18

    add-int v17, v17, v18

    add-int v5, v5, v17

    .line 386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_a

    .line 387
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    move-object/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    move/from16 v0, v16

    move/from16 v1, v18

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v19

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 391
    :cond_a
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v5}, Lcom/google/android/finsky/layout/DetailsSummary;->setMeasuredDimension(II)V

    .line 392
    return-void

    .line 299
    .end local v5    # "fullHeight":I
    .restart local v3    # "combinedWidth":I
    :cond_b
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 304
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    add-int v6, v6, v17

    goto/16 :goto_1

    .line 339
    .end local v3    # "combinedWidth":I
    .restart local v5    # "fullHeight":I
    .restart local v8    # "maxSummaryDynamicWidth":I
    .restart local v9    # "summaryDynamicHeight":I
    .restart local v10    # "summaryDynamicWidth":I
    .restart local v14    # "thumbnailHeightForDynamicFitting":I
    :cond_d
    const/4 v11, 0x1

    .line 340
    .local v11, "summaryIsItsOwnLine":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    if-nez v17, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mBylines:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mExtraLabels:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getVisibility()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_e

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mCreatorBlock:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 344
    .local v4, "creatorBlockWidth":I
    add-int v17, v4, v10

    move/from16 v0, v17

    if-gt v0, v7, :cond_e

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v17, v0

    sub-int v18, v7, v4

    const/high16 v19, 0x40000000    # 2.0f

    invoke-static/range {v18 .. v19}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 350
    invoke-static {v6, v13}, Ljava/lang/Math;->max(II)I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v18, v0

    sub-int v5, v17, v18

    .line 351
    const/4 v11, 0x0

    .line 355
    .end local v4    # "creatorBlockWidth":I
    :cond_e
    if-eqz v11, :cond_9

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    move/from16 v0, v18

    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/View;->measure(II)V

    .line 361
    invoke-static {v6, v13}, Ljava/lang/Math;->max(II)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mDetailsSummaryDynamic:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getMeasuredHeight()I

    move-result v18

    add-int v5, v17, v18

    goto/16 :goto_2

    .line 366
    .end local v8    # "maxSummaryDynamicWidth":I
    .end local v9    # "summaryDynamicHeight":I
    .end local v10    # "summaryDynamicWidth":I
    .end local v11    # "summaryIsItsOwnLine":Z
    .end local v14    # "thumbnailHeightForDynamicFitting":I
    :cond_f
    invoke-static {v6, v13}, Ljava/lang/Math;->max(II)I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v18, v0

    sub-int v5, v17, v18

    goto/16 :goto_2

    .line 378
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsSummary;->mContentMargin:I

    move/from16 v17, v0

    add-int v5, v5, v17

    goto/16 :goto_3
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 197
    :cond_0
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 204
    :cond_0
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 189
    :cond_0
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1
    .param p1, "resid"    # I

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 211
    :cond_0
    return-void
.end method

.method public setPadding(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mBackgroundLayer:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/View;->setPadding(IIII)V

    .line 177
    :cond_0
    return-void
.end method

.method public setThumbnailMode(I)V
    .locals 4
    .param p1, "thumbnailMode"    # I

    .prologue
    const/4 v1, 0x1

    .line 111
    iget v2, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailMode:I

    if-eq v2, p1, :cond_0

    .line 112
    iput p1, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailMode:I

    .line 114
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSummary;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 115
    .local v0, "whiteColor":I
    iget v2, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailMode:I

    if-ne v2, v1, :cond_1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/DetailsSummary;->updatePeekBackground(IZ)V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSummary;->requestLayout()V

    .line 119
    .end local v0    # "whiteColor":I
    :cond_0
    return-void

    .line 115
    .restart local v0    # "whiteColor":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updatePeekBackground(IZ)V
    .locals 6
    .param p1, "color"    # I
    .param p2, "isPeeking"    # Z

    .prologue
    const/4 v2, 0x0

    .line 130
    if-eqz p2, :cond_0

    .line 134
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    new-instance v1, Landroid/graphics/drawable/PaintDrawable;

    invoke-direct {v1, p1}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    iget v3, p0, Lcom/google/android/finsky/layout/DetailsSummary;->mThumbnailVerticalPeek:I

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 142
    :goto_0
    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/google/android/finsky/layout/DetailsSummary;->setPadding(IIII)V

    .line 143
    return-void

    .line 137
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/DetailsSummary;->setBackgroundColor(I)V

    goto :goto_0
.end method
