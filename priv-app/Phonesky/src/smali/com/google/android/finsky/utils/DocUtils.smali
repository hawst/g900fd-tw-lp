.class public Lcom/google/android/finsky/utils/DocUtils;
.super Ljava/lang/Object;
.source "DocUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/DocUtils$1;,
        Lcom/google/android/finsky/utils/DocUtils$OfferFilter;
    }
.end annotation


# static fields
.field private static final PREFIX_TO_BACKEND:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x2

    const/4 v3, 0x6

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    .line 33
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "app"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "album"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "artist"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "book"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "device"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "magazine"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "magazineissue"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "newsedition"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "newsissue"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "movie"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "song"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "tvepisode"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "tvseason"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    const-string v1, "tvshow"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    return-void
.end method

.method public static canRate(Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/api/model/Document;)Z
    .locals 5
    .param p0, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v2, 0x1

    .line 88
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 89
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v3

    iget-object v1, v3, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 90
    .local v1, "packageName":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 91
    .local v0, "owners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 93
    .end local v0    # "owners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 91
    .restart local v0    # "owners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    .restart local v1    # "packageName":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static createDocid(IILjava/lang/String;)Lcom/google/android/finsky/protos/Common$Docid;
    .locals 2
    .param p0, "backend"    # I
    .param p1, "docType"    # I
    .param p2, "backendDocid"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 508
    new-instance v0, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    .line 509
    .local v0, "docId":Lcom/google/android/finsky/protos/Common$Docid;
    iput p0, v0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    .line 510
    iput-boolean v1, v0, Lcom/google/android/finsky/protos/Common$Docid;->hasBackend:Z

    .line 511
    iput p1, v0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    .line 512
    iput-boolean v1, v0, Lcom/google/android/finsky/protos/Common$Docid;->hasType:Z

    .line 513
    iput-object p2, v0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 514
    iput-boolean v1, v0, Lcom/google/android/finsky/protos/Common$Docid;->hasBackendDocid:Z

    .line 515
    return-object v0
.end method

.method public static docidToBackend(Ljava/lang/String;)I
    .locals 7
    .param p0, "docid"    # Ljava/lang/String;

    .prologue
    const/4 v5, -0x1

    .line 62
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 63
    .local v3, "length":I
    if-gtz v3, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v5

    .line 66
    :cond_1
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_1
    if-ge v2, v3, :cond_4

    .line 67
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 68
    .local v1, "c":C
    const/16 v6, 0x2d

    if-eq v1, v6, :cond_2

    const/16 v6, 0x3a

    if-ne v1, v6, :cond_3

    .line 70
    :cond_2
    const/4 v6, 0x0

    invoke-virtual {p0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 71
    .local v4, "scheme":Ljava/lang/String;
    sget-object v6, Lcom/google/android/finsky/utils/DocUtils;->PREFIX_TO_BACKEND:Ljava/util/Map;

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 72
    .local v0, "backend":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto :goto_0

    .line 66
    .end local v0    # "backend":Ljava/lang/Integer;
    .end local v4    # "scheme":Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 76
    .end local v1    # "c":C
    :cond_4
    const/4 v5, 0x3

    goto :goto_0
.end method

.method private static extractPackageNameForInApp(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "docId"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x3a

    .line 128
    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 129
    .local v1, "start":I
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 130
    .local v0, "end":I
    if-lez v1, :cond_0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 131
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 133
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static extractSkuForInApp(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "docId"    # Ljava/lang/String;

    .prologue
    .line 142
    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 143
    .local v0, "start":I
    if-lez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 144
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 146
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getAvailabilityRestrictionResourceId(Lcom/google/android/finsky/api/model/Document;)I
    .locals 4
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAvailabilityRestriction()I

    move-result v1

    .line 170
    .local v1, "restriction":I
    const v0, 0x7f0c0309

    .line 171
    .local v0, "resourceId":I
    packed-switch v1, :pswitch_data_0

    .line 195
    :goto_0
    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Item is not available. Reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    return v0

    .line 173
    :pswitch_1
    const v0, 0x7f0c030a

    .line 174
    goto :goto_0

    .line 176
    :pswitch_2
    const v0, 0x7f0c030b

    .line 177
    goto :goto_0

    .line 179
    :pswitch_3
    const v0, 0x7f0c030c

    .line 180
    goto :goto_0

    .line 182
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 183
    const v0, 0x7f0c030d

    goto :goto_0

    .line 185
    :cond_0
    const v0, 0x7f0c030e

    .line 187
    goto :goto_0

    .line 189
    :pswitch_5
    const v0, 0x7f0c030f

    .line 190
    goto :goto_0

    .line 192
    :pswitch_6
    const v0, 0x7f0c0310

    goto :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public static getListingOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/protos/Common$Offer;
    .locals 12
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2, "library"    # Lcom/google/android/finsky/library/Library;

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v10

    const/16 v11, 0x10

    if-eq v10, v11, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v10

    const/16 v11, 0x18

    if-ne v10, v11, :cond_7

    .line 245
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/finsky/utils/DocUtils;->getSubscriptions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Ljava/util/List;

    move-result-object v9

    .line 246
    .local v9, "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v7

    .line 247
    .local v7, "subscriptionCount":I
    if-lez v7, :cond_5

    .line 249
    const/4 v10, 0x1

    if-ne v7, v10, :cond_3

    .line 250
    const/4 v10, 0x0

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v10}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v8

    .line 267
    .local v8, "subscriptionOffers":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_1
    const/4 v10, 0x0

    invoke-static {v8, v10}, Lcom/google/android/finsky/utils/DocUtils;->getLowestPricedOffer([Lcom/google/android/finsky/protos/Common$Offer;Z)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    .line 269
    .local v0, "cheapestSubscription":Lcom/google/android/finsky/protos/Common$Offer;
    if-nez v0, :cond_2

    .line 270
    const/4 v10, 0x1

    invoke-static {v8, v10}, Lcom/google/android/finsky/utils/DocUtils;->getLowestPricedOffer([Lcom/google/android/finsky/protos/Common$Offer;Z)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    .line 272
    :cond_2
    if-eqz v0, :cond_5

    .line 290
    .end local v0    # "cheapestSubscription":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v7    # "subscriptionCount":I
    .end local v8    # "subscriptionOffers":[Lcom/google/android/finsky/protos/Common$Offer;
    .end local v9    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    :goto_0
    return-object v0

    .line 254
    .restart local v7    # "subscriptionCount":I
    .restart local v9    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    :cond_3
    const/4 v5, 0x0

    .line 255
    .local v5, "numOffers":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v7, :cond_4

    .line 256
    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v10}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v10

    array-length v10, v10

    add-int/2addr v5, v10

    .line 255
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 258
    :cond_4
    const/4 v3, 0x0

    .line 259
    .local v3, "dstPos":I
    new-array v8, v5, [Lcom/google/android/finsky/protos/Common$Offer;

    .line 260
    .restart local v8    # "subscriptionOffers":[Lcom/google/android/finsky/protos/Common$Offer;
    const/4 v4, 0x0

    :goto_2
    if-ge v4, v7, :cond_1

    .line 261
    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v10}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v6

    .line 262
    .local v6, "offers":[Lcom/google/android/finsky/protos/Common$Offer;
    const/4 v10, 0x0

    array-length v11, v6

    invoke-static {v6, v10, v8, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 263
    array-length v10, v6

    add-int/2addr v3, v10

    .line 260
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 278
    .end local v3    # "dstPos":I
    .end local v4    # "i":I
    .end local v5    # "numOffers":I
    .end local v6    # "offers":[Lcom/google/android/finsky/protos/Common$Offer;
    .end local v8    # "subscriptionOffers":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_5
    invoke-static {p0}, Lcom/google/android/finsky/utils/DocUtils;->getMagazineCurrentIssueDocument(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 279
    .local v1, "currentIssue":Lcom/google/android/finsky/api/model/Document;
    if-eqz v1, :cond_6

    .line 280
    invoke-static {v1, p1, p2}, Lcom/google/android/finsky/utils/DocUtils;->getMagazineIssueOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v2

    .line 281
    .local v2, "currentOffer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v2, :cond_6

    iget-boolean v10, v2, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-eqz v10, :cond_6

    move-object v0, v2

    .line 282
    goto :goto_0

    .line 286
    .end local v2    # "currentOffer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 290
    .end local v1    # "currentIssue":Lcom/google/android/finsky/api/model/Document;
    .end local v7    # "subscriptionCount":I
    .end local v9    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v10

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/DocUtils;->getLowestPricedOffer([Lcom/google/android/finsky/protos/Common$Offer;Z)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    goto :goto_0
.end method

.method public static getLowestPricedOffer([Lcom/google/android/finsky/protos/Common$Offer;Z)Lcom/google/android/finsky/protos/Common$Offer;
    .locals 1
    .param p0, "offers"    # [Lcom/google/android/finsky/protos/Common$Offer;
    .param p1, "freeOk"    # Z

    .prologue
    .line 320
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/finsky/utils/DocUtils;->getLowestPricedOffer([Lcom/google/android/finsky/protos/Common$Offer;ZLcom/google/android/finsky/utils/DocUtils$OfferFilter;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    return-object v0
.end method

.method public static getLowestPricedOffer([Lcom/google/android/finsky/protos/Common$Offer;ZLcom/google/android/finsky/utils/DocUtils$OfferFilter;)Lcom/google/android/finsky/protos/Common$Offer;
    .locals 12
    .param p0, "offers"    # [Lcom/google/android/finsky/protos/Common$Offer;
    .param p1, "freeOk"    # Z
    .param p2, "offerFilter"    # Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    .prologue
    .line 331
    const/4 v3, 0x0

    .line 332
    .local v3, "lowestPricedOffer":Lcom/google/android/finsky/protos/Common$Offer;
    const-wide v4, 0x7fffffffffffffffL

    .line 333
    .local v4, "lowestPrice":J
    array-length v7, p0

    .line 334
    .local v7, "offerCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_5

    .line 335
    aget-object v6, p0, v2

    .line 336
    .local v6, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    iget-boolean v9, v6, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-nez v9, :cond_1

    .line 334
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 339
    :cond_1
    iget v8, v6, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    .line 340
    .local v8, "offerType":I
    const/4 v9, 0x1

    if-eq v8, v9, :cond_2

    const/4 v9, 0x7

    if-eq v8, v9, :cond_2

    const/4 v9, 0x3

    if-eq v8, v9, :cond_2

    const/4 v9, 0x4

    if-ne v8, v9, :cond_0

    .line 344
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2, v8}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->matches(I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 348
    :cond_3
    iget-wide v0, v6, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    .line 349
    .local v0, "currentPrice":J
    if-nez p1, :cond_4

    const-wide/16 v10, 0x0

    cmp-long v9, v0, v10

    if-eqz v9, :cond_0

    .line 352
    :cond_4
    cmp-long v9, v0, v4

    if-gez v9, :cond_0

    .line 353
    move-wide v4, v0

    .line 354
    move-object v3, v6

    goto :goto_1

    .line 357
    .end local v0    # "currentPrice":J
    .end local v6    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v8    # "offerType":I
    :cond_5
    return-object v3
.end method

.method public static getMagazineCurrentIssueDocument(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/api/model/Document;
    .locals 3
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    const/16 v1, 0x18

    if-eq v0, v1, :cond_0

    .line 471
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This method should be called only on magazine docs. Passed type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 475
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 476
    const/4 v0, 0x0

    .line 478
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    goto :goto_0
.end method

.method public static getMagazineIssueOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/protos/Common$Offer;
    .locals 4
    .param p0, "issueDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2, "library"    # Lcom/google/android/finsky/library/Library;

    .prologue
    const/4 v1, 0x0

    .line 454
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    const/16 v3, 0x11

    if-eq v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    const/16 v3, 0x19

    if-eq v2, v3, :cond_1

    .line 465
    :cond_0
    :goto_0
    return-object v1

    .line 459
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 460
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    .line 461
    .local v0, "currentOffers":[Lcom/google/android/finsky/protos/Common$Offer;
    array-length v2, v0

    if-lez v2, :cond_0

    .line 462
    const/4 v1, 0x0

    aget-object v1, v0, v1

    goto :goto_0
.end method

.method public static getMusicSubscriptionDocid(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "backendDocid"    # Ljava/lang/String;

    .prologue
    .line 534
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "music-subscription_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getNumberOfValidOffers([Lcom/google/android/finsky/protos/Common$Offer;)I
    .locals 7
    .param p0, "offers"    # [Lcom/google/android/finsky/protos/Common$Offer;

    .prologue
    .line 299
    const/4 v5, 0x0

    .line 300
    .local v5, "result":I
    move-object v0, p0

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 301
    .local v3, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    iget-boolean v6, v3, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-nez v6, :cond_1

    .line 300
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 304
    :cond_1
    iget v4, v3, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    .line 305
    .local v4, "offerType":I
    const/4 v6, 0x1

    if-eq v4, v6, :cond_2

    const/4 v6, 0x7

    if-eq v4, v6, :cond_2

    const/4 v6, 0x3

    if-eq v4, v6, :cond_2

    const/4 v6, 0x4

    if-ne v4, v6, :cond_0

    .line 309
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 311
    .end local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v4    # "offerType":I
    :cond_3
    return v5
.end method

.method private static getOfferDiscountRatio(Lcom/google/android/finsky/protos/Common$Offer;)F
    .locals 10
    .param p0, "offer"    # Lcom/google/android/finsky/protos/Common$Offer;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, 0x0

    .line 371
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFullPriceMicros:Z

    if-nez v5, :cond_1

    .line 379
    :cond_0
    :goto_0
    return v4

    .line 374
    :cond_1
    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    .line 375
    .local v2, "fullMicros":J
    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    sub-long v0, v2, v6

    .line 376
    .local v0, "discountMicros":J
    cmp-long v5, v2, v8

    if-lez v5, :cond_0

    cmp-long v5, v0, v8

    if-lez v5, :cond_0

    .line 379
    long-to-float v4, v0

    long-to-float v5, v2

    div-float/2addr v4, v5

    goto :goto_0
.end method

.method public static getOfferWithLargestDiscountIfAny(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/protos/Common$Offer;
    .locals 13
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2, "library"    # Lcom/google/android/finsky/library/Library;

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v11

    const/16 v12, 0x10

    if-eq v11, v12, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v11

    const/16 v12, 0x18

    if-ne v11, v12, :cond_4

    .line 410
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/finsky/utils/DocUtils;->getSubscriptions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Ljava/util/List;

    move-result-object v10

    .line 412
    .local v10, "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    const/4 v3, 0x0

    .line 413
    .local v3, "currentLargestDiscount":F
    const/4 v4, 0x0

    .line 414
    .local v4, "currentResult":Lcom/google/android/finsky/protos/Common$Offer;
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v7

    .line 415
    .local v7, "subscriptionCount":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v7, :cond_2

    .line 416
    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/api/model/Document;

    .line 417
    .local v6, "subscription":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/finsky/utils/DocUtils;->getOfferWithLargestDiscountIfAny([Lcom/google/android/finsky/protos/Common$Offer;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v9

    .line 419
    .local v9, "subscriptionLargestDiscountOffer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v9, :cond_1

    .line 420
    invoke-static {v9}, Lcom/google/android/finsky/utils/DocUtils;->getOfferDiscountRatio(Lcom/google/android/finsky/protos/Common$Offer;)F

    move-result v8

    .line 422
    .local v8, "subscriptionDiscount":F
    cmpl-float v11, v8, v3

    if-lez v11, :cond_1

    .line 423
    move v3, v8

    .line 424
    move-object v4, v9

    .line 415
    .end local v8    # "subscriptionDiscount":F
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 430
    .end local v6    # "subscription":Lcom/google/android/finsky/api/model/Document;
    .end local v9    # "subscriptionLargestDiscountOffer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_2
    invoke-static {p0}, Lcom/google/android/finsky/utils/DocUtils;->getMagazineCurrentIssueDocument(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 431
    .local v0, "currentIssue":Lcom/google/android/finsky/api/model/Document;
    if-eqz v0, :cond_3

    .line 432
    invoke-static {v0, p1, p2}, Lcom/google/android/finsky/utils/DocUtils;->getMagazineIssueOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v2

    .line 433
    .local v2, "currentIssueOffer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v2, :cond_3

    iget-boolean v11, v2, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-eqz v11, :cond_3

    .line 434
    invoke-static {v2}, Lcom/google/android/finsky/utils/DocUtils;->getOfferDiscountRatio(Lcom/google/android/finsky/protos/Common$Offer;)F

    move-result v1

    .line 435
    .local v1, "currentIssueDiscount":F
    cmpl-float v11, v1, v3

    if-lez v11, :cond_3

    .line 436
    move v3, v1

    .line 437
    move-object v4, v2

    .line 444
    .end local v0    # "currentIssue":Lcom/google/android/finsky/api/model/Document;
    .end local v1    # "currentIssueDiscount":F
    .end local v2    # "currentIssueOffer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v3    # "currentLargestDiscount":F
    .end local v4    # "currentResult":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v5    # "i":I
    .end local v7    # "subscriptionCount":I
    .end local v10    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    :cond_3
    :goto_1
    return-object v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/finsky/utils/DocUtils;->getOfferWithLargestDiscountIfAny([Lcom/google/android/finsky/protos/Common$Offer;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v4

    goto :goto_1
.end method

.method private static getOfferWithLargestDiscountIfAny([Lcom/google/android/finsky/protos/Common$Offer;)Lcom/google/android/finsky/protos/Common$Offer;
    .locals 8
    .param p0, "offers"    # [Lcom/google/android/finsky/protos/Common$Offer;

    .prologue
    .line 387
    const/4 v1, 0x0

    .line 388
    .local v1, "currentLargestDiscount":F
    const/4 v2, 0x0

    .line 389
    .local v2, "currentResult":Lcom/google/android/finsky/protos/Common$Offer;
    move-object v0, p0

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v5, v0, v3

    .line 390
    .local v5, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    invoke-static {v5}, Lcom/google/android/finsky/utils/DocUtils;->getOfferDiscountRatio(Lcom/google/android/finsky/protos/Common$Offer;)F

    move-result v6

    .line 391
    .local v6, "offerDiscount":F
    cmpl-float v7, v6, v1

    if-lez v7, :cond_0

    .line 392
    move v1, v6

    .line 393
    move-object v2, v5

    .line 389
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 396
    .end local v5    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v6    # "offerDiscount":F
    :cond_1
    return-object v2
.end method

.method public static getPackageNameForInApp(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "inAppDocId"    # Ljava/lang/String;

    .prologue
    .line 116
    const-string v0, "inapp:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    const/4 v0, 0x0

    .line 119
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/finsky/utils/DocUtils;->extractPackageNameForInApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getPackageNameForSubscription(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "subscriptionDocId"    # Ljava/lang/String;

    .prologue
    .line 103
    const-string v0, "subs:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    const/4 v0, 0x0

    .line 106
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/finsky/utils/DocUtils;->extractPackageNameForInApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getSubscriptions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Ljava/util/List;
    .locals 6
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2, "library"    # Lcom/google/android/finsky/library/Library;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/model/Document;",
            "Lcom/google/android/finsky/api/model/DfeToc;",
            "Lcom/google/android/finsky/library/Library;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 487
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasSubscriptions()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 488
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 489
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getSubscriptionsList()Ljava/util/List;

    move-result-object v1

    .line 490
    .local v1, "docSubscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 491
    .local v0, "docSubscriptionCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 492
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/api/model/Document;

    .line 493
    .local v4, "subscription":Lcom/google/android/finsky/api/model/Document;
    invoke-static {v4, p1, p2}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v5

    array-length v5, v5

    if-lez v5, :cond_0

    .line 495
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 491
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 501
    .end local v0    # "docSubscriptionCount":I
    .end local v1    # "docSubscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    .end local v2    # "i":I
    .end local v3    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    .end local v4    # "subscription":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    :cond_2
    return-object v3
.end method

.method public static hasAutoRenewingSubscriptions(Lcom/google/android/finsky/library/Libraries;Ljava/lang/String;)Z
    .locals 6
    .param p0, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/finsky/library/Libraries;->getAccountLibraries()Ljava/util/List;

    move-result-object v0

    .line 156
    .local v0, "accountLibraries":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/library/AccountLibrary;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/library/AccountLibrary;

    .line 157
    .local v1, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-virtual {v1, p1}, Lcom/google/android/finsky/library/AccountLibrary;->getSubscriptionPurchasesForPackage(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 159
    .local v4, "subscriptionList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 160
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;

    iget-boolean v5, v5, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->isAutoRenewing:Z

    if-eqz v5, :cond_1

    .line 161
    const/4 v5, 0x1

    .line 165
    .end local v1    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    .end local v2    # "i":I
    .end local v4    # "subscriptionList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;"
    :goto_1
    return v5

    .line 159
    .restart local v1    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    .restart local v2    # "i":I
    .restart local v4    # "subscriptionList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;"
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 165
    .end local v1    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    .end local v2    # "i":I
    .end local v4    # "subscriptionList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;"
    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public static hasDiscount(Lcom/google/android/finsky/protos/Common$Offer;)Z
    .locals 2
    .param p0, "offer"    # Lcom/google/android/finsky/protos/Common$Offer;

    .prologue
    .line 383
    invoke-static {p0}, Lcom/google/android/finsky/utils/DocUtils;->getOfferDiscountRatio(Lcom/google/android/finsky/protos/Common$Offer;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInAppDocid(Lcom/google/android/finsky/protos/Common$Docid;)Z
    .locals 2
    .param p0, "docid"    # Lcom/google/android/finsky/protos/Common$Docid;

    .prologue
    .line 522
    iget v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
