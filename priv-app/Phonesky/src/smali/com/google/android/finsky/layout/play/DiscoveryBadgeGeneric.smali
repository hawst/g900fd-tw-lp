.class public Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;
.super Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
.source "DiscoveryBadgeGeneric.java"


# instance fields
.field private mPaint:Landroid/graphics/Paint;

.field private mTransformation:Lcom/google/android/play/image/AvatarCropTransformation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mPaint:Landroid/graphics/Paint;

    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 39
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->setWillNotDraw(Z)V

    .line 40
    new-instance v0, Lcom/google/android/play/image/AvatarCropTransformation;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/image/AvatarCropTransformation;-><init>(Landroid/content/res/Resources;Z)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    .line 41
    return-void
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 57
    const/16 v0, 0x709

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->onDraw(Landroid/graphics/Canvas;)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->getWidth()I

    move-result v2

    div-int/lit8 v0, v2, 0x2

    .line 48
    .local v0, "x":I
    iget v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mBadgeRadius:I

    .line 51
    .local v1, "y":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mCurrentFillColor:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 52
    int-to-float v2, v0

    int-to-float v3, v1

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mBadgeRadius:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 53
    return-void
.end method

.method protected onPreImageLoad()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    iget v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mCurrentFillColor:I

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/AvatarCropTransformation;->setFillToSizeColor(I)V

    .line 63
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mIcon:Lcom/google/android/play/image/FifeImageView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeGeneric;->mTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setBitmapTransformation(Lcom/google/android/play/image/BitmapTransformation;)V

    .line 64
    return-void
.end method
