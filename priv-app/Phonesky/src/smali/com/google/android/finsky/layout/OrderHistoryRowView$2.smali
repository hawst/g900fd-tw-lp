.class Lcom/google/android/finsky/layout/OrderHistoryRowView$2;
.super Ljava/lang/Object;
.source "OrderHistoryRowView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/OrderHistoryRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/OrderHistoryRowView;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$2;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 146
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 150
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v1, 0x8

    .line 139
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$2;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPurchaseDetailsView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$100(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$2;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mOpenButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$300(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$2;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mRefundButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$500(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    return-void
.end method
