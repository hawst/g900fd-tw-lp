.class public Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;
.super Ljava/lang/Object;
.source "PaypalPasswordEncryptor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private createEncryptedPayload([B[B[B)[B
    .locals 5
    .param p1, "certificateSerialNumber"    # [B
    .param p2, "encryptedExchangeKey"    # [B
    .param p3, "encryptedInnerMessage"    # [B

    .prologue
    .line 335
    array-length v2, p2

    array-length v3, p3

    add-int v1, v2, v3

    .line 336
    .local v1, "encryptedLength":I
    const v2, 0xffff

    if-le v1, v2, :cond_0

    .line 338
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Encrypted message is too long: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 342
    :cond_0
    array-length v2, p1

    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v1

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 344
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    sget-object v2, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 345
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 346
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 347
    int-to-char v2, v1

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putChar(C)Ljava/nio/ByteBuffer;

    .line 348
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 349
    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 350
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    return-object v2
.end method

.method private createInnerMessage(JLjava/lang/String;Ljava/lang/String;)[B
    .locals 11
    .param p1, "timestamp"    # J
    .param p3, "hashedDeviceId"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;

    .prologue
    const/16 v10, 0xff

    .line 241
    const-wide/16 v8, 0x3e8

    div-long v8, p1, v8

    long-to-int v6, v8

    .line 247
    .local v6, "timestampSeconds":I
    :try_start_0
    const-string v7, "UTF-8"

    invoke-virtual {p3, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 252
    .local v2, "hashedDeviceIdBytes":[B
    array-length v3, v2

    .line 253
    .local v3, "hashedDeviceIdNumBytes":I
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v7

    if-eq v3, v7, :cond_0

    .line 255
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Hashed device ID not 8 bits per char: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 248
    .end local v2    # "hashedDeviceIdBytes":[B
    .end local v3    # "hashedDeviceIdNumBytes":I
    :catch_0
    move-exception v1

    .line 250
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 258
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v2    # "hashedDeviceIdBytes":[B
    .restart local v3    # "hashedDeviceIdNumBytes":I
    :cond_0
    if-le v3, v10, :cond_1

    .line 260
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Hashed device ID too long: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 266
    :cond_1
    :try_start_1
    const-string v7, "UTF-16LE"

    invoke-virtual {p4, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 273
    .local v4, "passwordBytes":[B
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v5

    .line 274
    .local v5, "passwordLength":I
    if-le v5, v10, :cond_2

    .line 275
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Password too long: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 267
    .end local v4    # "passwordBytes":[B
    .end local v5    # "passwordLength":I
    :catch_1
    move-exception v1

    .line 269
    .restart local v1    # "e":Ljava/io/UnsupportedEncodingException;
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 279
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v4    # "passwordBytes":[B
    .restart local v5    # "passwordLength":I
    :cond_2
    add-int/lit8 v7, v3, 0x5

    add-int/lit8 v7, v7, 0x1

    array-length v8, v4

    add-int/2addr v7, v8

    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 281
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 282
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 283
    int-to-byte v7, v3

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 284
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 285
    int-to-byte v7, v5

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 286
    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 287
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    return-object v7
.end method

.method private createSessionKey()Ljavax/crypto/SecretKey;
    .locals 3

    .prologue
    .line 184
    :try_start_0
    const-string v2, "AES"

    invoke-static {v2}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v1

    .line 185
    .local v1, "keyGenerator":Ljavax/crypto/KeyGenerator;
    const/16 v2, 0x100

    invoke-virtual {v1, v2}, Ljavax/crypto/KeyGenerator;->init(I)V

    .line 186
    invoke-virtual {v1}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 187
    .end local v1    # "keyGenerator":Ljavax/crypto/KeyGenerator;
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private encryptExchangeKey(Ljava/security/cert/Certificate;Ljavax/crypto/SecretKey;)[B
    .locals 6
    .param p1, "certificate"    # Ljava/security/cert/Certificate;
    .param p2, "exchangeKey"    # Ljavax/crypto/SecretKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 205
    :try_start_0
    const-string v4, "RSA/ECB/PKCS1Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 214
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const/4 v4, 0x1

    :try_start_1
    invoke-virtual {p1}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_2

    .line 221
    :try_start_2
    invoke-interface {p2}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v3

    .line 222
    .local v3, "exchangeKeyBytes":[B
    invoke-virtual {v0, v3}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v2

    .line 231
    .local v2, "encryptedKey":[B
    invoke-static {v2}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->reverseArray([B)V

    .line 232
    return-object v2

    .line 206
    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    .end local v2    # "encryptedKey":[B
    .end local v3    # "exchangeKeyBytes":[B
    :catch_0
    move-exception v1

    .line 208
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 209
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 211
    .local v1, "e":Ljavax/crypto/NoSuchPaddingException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 215
    .end local v1    # "e":Ljavax/crypto/NoSuchPaddingException;
    .restart local v0    # "cipher":Ljavax/crypto/Cipher;
    :catch_2
    move-exception v1

    .line 217
    .local v1, "e":Ljava/security/InvalidKeyException;
    new-instance v4, Ljava/security/cert/CertificateException;

    invoke-direct {v4, v1}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 223
    .end local v1    # "e":Ljava/security/InvalidKeyException;
    :catch_3
    move-exception v1

    .line 225
    .local v1, "e":Ljavax/crypto/IllegalBlockSizeException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 226
    .end local v1    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_4
    move-exception v1

    .line 228
    .local v1, "e":Ljavax/crypto/BadPaddingException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method private encryptInnerMessage(Ljavax/crypto/SecretKey;[B)[B
    .locals 4
    .param p1, "exchangeKey"    # Ljavax/crypto/SecretKey;
    .param p2, "innerMessage"    # [B

    .prologue
    .line 296
    :try_start_0
    const-string v3, "AES/CBC/PKCS5Padding"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 307
    .local v0, "cipher":Ljavax/crypto/Cipher;
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    const/16 v3, 0x10

    new-array v3, v3, [B

    invoke-direct {v2, v3}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 309
    .local v2, "zeroInitializationVector":Ljavax/crypto/spec/IvParameterSpec;
    const/4 v3, 0x1

    :try_start_1
    invoke-virtual {v0, v3, p1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_1 .. :try_end_1} :catch_3

    .line 319
    :try_start_2
    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljavax/crypto/BadPaddingException; {:try_start_2 .. :try_end_2} :catch_5

    move-result-object v3

    return-object v3

    .line 297
    .end local v0    # "cipher":Ljavax/crypto/Cipher;
    .end local v2    # "zeroInitializationVector":Ljavax/crypto/spec/IvParameterSpec;
    :catch_0
    move-exception v1

    .line 300
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 301
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 304
    .local v1, "e":Ljavax/crypto/NoSuchPaddingException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 310
    .end local v1    # "e":Ljavax/crypto/NoSuchPaddingException;
    .restart local v0    # "cipher":Ljavax/crypto/Cipher;
    .restart local v2    # "zeroInitializationVector":Ljavax/crypto/spec/IvParameterSpec;
    :catch_2
    move-exception v1

    .line 312
    .local v1, "e":Ljava/security/InvalidKeyException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 313
    .end local v1    # "e":Ljava/security/InvalidKeyException;
    :catch_3
    move-exception v1

    .line 315
    .local v1, "e":Ljava/security/InvalidAlgorithmParameterException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 320
    .end local v1    # "e":Ljava/security/InvalidAlgorithmParameterException;
    :catch_4
    move-exception v1

    .line 322
    .local v1, "e":Ljavax/crypto/IllegalBlockSizeException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 323
    .end local v1    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_5
    move-exception v1

    .line 325
    .local v1, "e":Ljavax/crypto/BadPaddingException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method private getDeviceId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 96
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getX509Certificate([B)Ljava/security/cert/X509Certificate;
    .locals 3
    .param p0, "certificateBytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 363
    const-string v2, "X.509"

    invoke-static {v2}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    .line 364
    .local v0, "factory":Ljava/security/cert/CertificateFactory;
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 365
    .local v1, "inputStream":Ljava/io/ByteArrayInputStream;
    invoke-virtual {v0, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v2

    check-cast v2, Ljava/security/cert/X509Certificate;

    return-object v2
.end method

.method static reverseArray([B)V
    .locals 4
    .param p0, "array"    # [B

    .prologue
    .line 372
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p0

    div-int/lit8 v3, v3, 0x2

    if-ge v0, v3, :cond_0

    .line 373
    array-length v3, p0

    add-int/lit8 v3, v3, -0x1

    sub-int v1, v3, v0

    .line 374
    .local v1, "j":I
    aget-byte v2, p0, v0

    .line 375
    .local v2, "temp":B
    aget-byte v3, p0, v1

    aput-byte v3, p0, v0

    .line 376
    aput-byte v2, p0, v1

    .line 372
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 378
    .end local v1    # "j":I
    .end local v2    # "temp":B
    :cond_0
    return-void
.end method


# virtual methods
.method public createPaypalAuthResponse([BLjava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;
    .locals 8
    .param p1, "certificateBytes"    # [B
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "deviceIdSalt"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->getDeviceId()Ljava/lang/String;

    move-result-object v5

    .line 64
    .local v5, "deviceId":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .local v6, "timestamp":J
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    .line 65
    invoke-virtual/range {v1 .. v7}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->createPaypalAuthResponse([BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    move-result-object v0

    return-object v0
.end method

.method createPaypalAuthResponse([BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;
    .locals 9
    .param p1, "certificateBytes"    # [B
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "deviceIdSalt"    # Ljava/lang/String;
    .param p4, "deviceId"    # Ljava/lang/String;
    .param p5, "timestamp"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 78
    new-instance v6, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;-><init>()V

    .line 80
    .local v6, "response":Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;
    invoke-virtual {p0, p3, p4}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->getHashedDeviceId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 81
    .local v4, "hashedDeviceId":Ljava/lang/String;
    iput-object v4, v6, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hashedDeviceId:Ljava/lang/String;

    .line 82
    iput-boolean v7, v6, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hasHashedDeviceId:Z

    .line 84
    invoke-static {p1}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->getX509Certificate([B)Ljava/security/cert/X509Certificate;

    move-result-object v1

    .local v1, "certificate":Ljava/security/cert/X509Certificate;
    move-object v0, p0

    move-wide v2, p5

    move-object v5, p2

    .line 85
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->encrypt(Ljava/security/cert/X509Certificate;JLjava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->encryptedAuthMessage:[B

    .line 87
    iput-boolean v7, v6, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;->hasEncryptedAuthMessage:Z

    .line 89
    return-object v6
.end method

.method encrypt(Ljava/security/cert/X509Certificate;JLjava/lang/String;Ljava/lang/String;)[B
    .locals 6
    .param p1, "certificate"    # Ljava/security/cert/X509Certificate;
    .param p2, "timestamp"    # J
    .param p4, "hashedDeviceId"    # Ljava/lang/String;
    .param p5, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->getCertificateSerialNumber(Ljava/math/BigInteger;)[B

    move-result-object v0

    .line 141
    .local v0, "certificateSerialNumber":[B
    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->createSessionKey()Ljavax/crypto/SecretKey;

    move-result-object v3

    .line 143
    .local v3, "exchangeKey":Ljavax/crypto/SecretKey;
    invoke-direct {p0, p1, v3}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->encryptExchangeKey(Ljava/security/cert/Certificate;Ljavax/crypto/SecretKey;)[B

    move-result-object v1

    .line 146
    .local v1, "encryptedExchangeKey":[B
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->createInnerMessage(JLjava/lang/String;Ljava/lang/String;)[B

    move-result-object v4

    .line 148
    .local v4, "innerMessage":[B
    invoke-direct {p0, v3, v4}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->encryptInnerMessage(Ljavax/crypto/SecretKey;[B)[B

    move-result-object v2

    .line 151
    .local v2, "encryptedInnerMessage":[B
    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/finsky/billing/genericinstrument/PaypalPasswordEncryptor;->createEncryptedPayload([B[B[B)[B

    move-result-object v5

    return-object v5
.end method

.method getCertificateSerialNumber(Ljava/math/BigInteger;)[B
    .locals 6
    .param p1, "serialNumber"    # Ljava/math/BigInteger;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 160
    invoke-virtual {p1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    .line 161
    .local v0, "bytes":[B
    array-length v3, v0

    if-ge v3, v4, :cond_0

    .line 163
    new-array v1, v4, [B

    .line 164
    .local v1, "paddedSerialNumber":[B
    array-length v3, v0

    rsub-int/lit8 v3, v3, 0x8

    array-length v4, v0

    invoke-static {v0, v5, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 175
    .end local v1    # "paddedSerialNumber":[B
    :goto_0
    return-object v1

    .line 167
    :cond_0
    array-length v3, v0

    if-ne v3, v4, :cond_1

    move-object v1, v0

    .line 169
    goto :goto_0

    .line 172
    :cond_1
    new-array v2, v4, [B

    .line 173
    .local v2, "truncatedSerialNumber":[B
    array-length v3, v0

    add-int/lit8 v3, v3, -0x8

    invoke-static {v0, v3, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v1, v2

    .line 175
    goto :goto_0
.end method

.method getHashedDeviceId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "deviceIdSalt"    # Ljava/lang/String;
    .param p2, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 107
    :try_start_0
    const-string v3, "SHA-256"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 108
    .local v2, "messageDigest":Ljava/security/MessageDigest;
    const-string v3, "UTF-8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 109
    const-string v3, "UTF-8"

    invoke-virtual {p2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 110
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 114
    .local v0, "digest":[B
    const/4 v3, 0x2

    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    return-object v3

    .line 115
    .end local v0    # "digest":[B
    .end local v2    # "messageDigest":Ljava/security/MessageDigest;
    :catch_0
    move-exception v1

    .line 117
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 118
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 120
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method
