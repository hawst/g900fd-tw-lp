.class public Lcom/google/android/finsky/layout/play/PlayCirclesButton;
.super Landroid/widget/TextView;
.source "PlayCirclesButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;


# instance fields
.field private mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

.field private final mCirclesIcon:Landroid/graphics/drawable/Drawable;

.field private final mCirclesIconGap:I

.field private mOriginalLeftPadding:I

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mShowsCirclesIcon:Z

.field private final mTextPaint:Landroid/graphics/Paint;

.field private mTextWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    .line 64
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mTextPaint:Landroid/graphics/Paint;

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f02009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCirclesIcon:Landroid/graphics/drawable/Drawable;

    .line 69
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCirclesIcon:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCirclesIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCirclesIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 71
    const v1, 0x7f0b0164

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCirclesIconGap:I

    .line 72
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getPaddingLeft()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mOriginalLeftPadding:I

    .line 74
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->setWillNotDraw(Z)V

    .line 75
    return-void
.end method

.method private configure(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "circles":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/common/people/data/AudienceMember;>;"
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 134
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    move v8, v9

    :goto_0
    iput-boolean v8, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mShowsCirclesIcon:Z

    .line 135
    iget-boolean v8, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mShowsCirclesIcon:Z

    if-eqz v8, :cond_2

    const v0, 0x7f020050

    .line 139
    .local v0, "backgroundDrawableId":I
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 141
    .local v7, "resources":Landroid/content/res/Resources;
    invoke-static {p1, v7}, Lcom/google/android/finsky/utils/GPlusUtils;->getCirclesString(Ljava/util/List;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "buttonText":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mOriginalLeftPadding:I

    .line 145
    .local v4, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getPaddingRight()I

    move-result v5

    .line 146
    .local v5, "paddingRight":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getPaddingTop()I

    move-result v6

    .line 147
    .local v6, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getPaddingBottom()I

    move-result v3

    .line 148
    .local v3, "paddingBottom":I
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->setBackgroundResource(I)V

    .line 149
    iget-boolean v8, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mShowsCirclesIcon:Z

    if-eqz v8, :cond_0

    .line 152
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCirclesIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    iget v11, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCirclesIconGap:I

    add-int/2addr v8, v11

    add-int/2addr v4, v8

    .line 153
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getTextSize()F

    move-result v11

    invoke-virtual {v8, v11}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 154
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getText()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-interface {v11}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mTextWidth:F

    .line 156
    :cond_0
    invoke-virtual {p0, v4, v6, v5, v3}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->setPadding(IIII)V

    .line 159
    iget-boolean v8, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mShowsCirclesIcon:Z

    if-eqz v8, :cond_3

    move-object v2, v1

    .line 162
    .local v2, "contentDescription":Ljava/lang/String;
    :goto_2
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 163
    return-void

    .end local v0    # "backgroundDrawableId":I
    .end local v1    # "buttonText":Ljava/lang/String;
    .end local v2    # "contentDescription":Ljava/lang/String;
    .end local v3    # "paddingBottom":I
    .end local v4    # "paddingLeft":I
    .end local v5    # "paddingRight":I
    .end local v6    # "paddingTop":I
    .end local v7    # "resources":Landroid/content/res/Resources;
    :cond_1
    move v8, v10

    .line 134
    goto :goto_0

    .line 135
    :cond_2
    const v0, 0x7f02004f

    goto :goto_1

    .line 159
    .restart local v0    # "backgroundDrawableId":I
    .restart local v1    # "buttonText":Ljava/lang/String;
    .restart local v3    # "paddingBottom":I
    .restart local v4    # "paddingLeft":I
    .restart local v5    # "paddingRight":I
    .restart local v6    # "paddingTop":I
    .restart local v7    # "resources":Landroid/content/res/Resources;
    :cond_3
    const v8, 0x7f0c03b4

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v11}, Lcom/google/android/finsky/model/CirclesModel;->getTargetPersonDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "targetPerson"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "currentAccountName"    # Ljava/lang/String;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 88
    iput-object p3, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 90
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    if-nez v3, :cond_0

    .line 92
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v0

    .line 93
    .local v0, "cache":Lcom/google/android/finsky/utils/ClientMutationCache;
    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/utils/ClientMutationCache;->getCachedCirclesModel(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Lcom/google/android/finsky/model/CirclesModel;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    .line 95
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v3, p0}, Lcom/google/android/finsky/model/CirclesModel;->setCirclesModelListener(Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;)V

    .line 97
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 98
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-interface {v3}, Lcom/google/android/finsky/fragments/PageFragmentHost;->getPeopleClient()Lcom/google/android/gms/people/PeopleClient;

    move-result-object v2

    .line 99
    .local v2, "peopleClient":Lcom/google/android/gms/people/PeopleClient;
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/finsky/model/CirclesModel;->loadCircles(Landroid/content/Context;Lcom/google/android/gms/people/PeopleClient;)V

    .line 100
    invoke-virtual {p0, p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    .end local v0    # "cache":Lcom/google/android/finsky/utils/ClientMutationCache;
    .end local v1    # "context":Landroid/content/Context;
    .end local v2    # "peopleClient":Lcom/google/android/gms/people/PeopleClient;
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v3}, Lcom/google/android/finsky/model/CirclesModel;->getCircles()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->configure(Ljava/util/List;)V

    .line 109
    return-void
.end method

.method public onCirclesUpdate(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "circles":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/common/people/data/AudienceMember;>;"
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->configure(Ljava/util/List;)V

    .line 114
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 118
    if-ne p1, p0, :cond_0

    .line 119
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v0, :cond_0

    .line 120
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x118

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 123
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/model/CirclesModel;->launchCirclePicker(Landroid/support/v4/app/FragmentActivity;)V

    .line 126
    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/model/CirclesModel;->setCirclesModelListener(Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;)V

    .line 202
    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCircleModel:Lcom/google/android/finsky/model/CirclesModel;

    .line 204
    :cond_0
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    .line 205
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 167
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 169
    iget-boolean v6, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mShowsCirclesIcon:Z

    if-nez v6, :cond_0

    .line 194
    :goto_0
    return-void

    .line 174
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getHeight()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCirclesIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    sub-int/2addr v6, v7

    div-int/lit8 v3, v6, 0x2

    .line 182
    .local v3, "iconY":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getPaddingLeft()I

    move-result v4

    .line 183
    .local v4, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getPaddingRight()I

    move-result v5

    .line 184
    .local v5, "paddingRight":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCirclesIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    iget v7, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCirclesIconGap:I

    add-int v1, v6, v7

    .line 185
    .local v1, "iconWidth":I
    iget v6, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mTextWidth:F

    float-to-int v6, v6

    add-int v0, v6, v1

    .line 186
    .local v0, "contentWidth":I
    sub-int v6, v4, v1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getWidth()I

    move-result v7

    sub-int/2addr v7, v0

    sub-int/2addr v7, v4

    sub-int/2addr v7, v5

    add-int/2addr v7, v1

    div-int/lit8 v7, v7, 0x2

    add-int v2, v6, v7

    .line 189
    .local v2, "iconX":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 191
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->getScrollX()I

    move-result v6

    add-int/2addr v6, v2

    int-to-float v6, v6

    int-to-float v7, v3

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 192
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->mCirclesIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 193
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method
