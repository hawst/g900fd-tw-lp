.class public Lcom/google/android/finsky/layout/DetailsTextBlock;
.super Landroid/widget/LinearLayout;
.source "DetailsTextBlock.java"


# instance fields
.field private mBodyHeaderView:Landroid/widget/TextView;

.field private mBodyView:Lcom/google/android/play/layout/PlayTextView;

.field private final mCorpusXMargin:I

.field private final mRegularXMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsTextBlock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    sget-object v1, Lcom/android/vending/R$styleable;->DetailsTextBlock:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 40
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mRegularXMargin:I

    .line 42
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mCorpusXMargin:I

    .line 44
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 45
    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .locals 3
    .param p1, "header"    # Ljava/lang/CharSequence;
    .param p2, "body"    # Ljava/lang/CharSequence;
    .param p3, "bodyMaxLines"    # I

    .prologue
    const/4 v2, 0x0

    .line 57
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyHeaderView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyHeaderView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 64
    :goto_0
    invoke-virtual {p0, p2}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setBody(Ljava/lang/CharSequence;)V

    .line 65
    invoke-virtual {p0, p3}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setBodyMaxLines(I)V

    .line 67
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setVisibility(I)V

    .line 68
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyHeaderView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public hasBody()Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 51
    const v0, 0x7f0a018c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyHeaderView:Landroid/widget/TextView;

    .line 52
    const v0, 0x7f0a018d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    .line 53
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 54
    return-void
.end method

.method public removeCorpusStyle()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 111
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setBackgroundResource(I)V

    .line 112
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/PlayTextView;->setLastLineOverdrawColor(I)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 116
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 117
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 118
    iget v1, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mRegularXMargin:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 119
    iget v1, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mRegularXMargin:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 120
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 121
    return-void
.end method

.method public resetContent()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyHeaderView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    return-void
.end method

.method public setBody(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "body"    # Ljava/lang/CharSequence;

    .prologue
    .line 71
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    .line 77
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setBodyClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "bodyClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/PlayTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    return-void
.end method

.method public setBodyMaxLines(I)V
    .locals 1
    .param p1, "bodyMaxLines"    # I

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/PlayTextView;->setMaxLines(I)V

    .line 81
    return-void
.end method

.method public setCorpusStyle(III)V
    .locals 4
    .param p1, "backend"    # I
    .param p2, "topMargin"    # I
    .param p3, "bottomMargin"    # I

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 96
    .local v0, "context":Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getWhatsNewBackgroundDrawable(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setBackgroundResource(I)V

    .line 98
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getWhatsNewFillColor(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/layout/PlayTextView;->setLastLineOverdrawColor(I)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 103
    .local v1, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 104
    iput p3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 105
    iget v2, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mCorpusXMargin:I

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 106
    iget v2, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mCorpusXMargin:I

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 107
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 108
    return-void
.end method

.method public setEditorialStyle(II)V
    .locals 1
    .param p1, "backgroundFillColor"    # I
    .param p2, "textColor"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/PlayTextView;->setLastLineOverdrawColor(I)V

    .line 90
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0, p2}, Lcom/google/android/play/layout/PlayTextView;->setTextColor(I)V

    .line 91
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextBlock;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v0, p2}, Lcom/google/android/play/layout/PlayTextView;->setLinkTextColor(I)V

    .line 92
    return-void
.end method
