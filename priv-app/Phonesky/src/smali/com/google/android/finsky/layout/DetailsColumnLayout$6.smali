.class Lcom/google/android/finsky/layout/DetailsColumnLayout$6;
.super Ljava/lang/Object;
.source "DetailsColumnLayout.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsColumnLayout;->expandSectionIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

.field final synthetic val$initialScrollerPaddingTop:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;I)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$6;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iput p2, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$6;->val$initialScrollerPaddingTop:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 10
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    .line 380
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    .line 381
    .local v2, "fraction":F
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$6;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceTop:I
    invoke-static {v6}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$200(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v3

    .line 382
    .local v3, "initialTitlePaddingTop":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$6;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mExpandedContainerTopPadding:I
    invoke-static {v6}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$300(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v6

    sub-int v6, v3, v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    float-to-int v6, v6

    sub-int v1, v3, v6

    .line 387
    .local v1, "expanderPaddingTop":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$6;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v6, v6, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-virtual {v6, v1}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->setTopPaddingOnTopView(I)V

    .line 390
    iget v6, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$6;->val$initialScrollerPaddingTop:I

    int-to-float v6, v6

    sub-float v7, v9, v2

    mul-float/2addr v6, v7

    float-to-int v5, v6

    .line 391
    .local v5, "scrollerPaddingTop":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$6;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceBottom:I
    invoke-static {v6}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$600(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v6

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 392
    .local v4, "maxPaddingBottom":I
    int-to-float v6, v4

    sub-float v7, v9, v2

    mul-float/2addr v6, v7

    float-to-int v0, v6

    .line 393
    .local v0, "expanderPaddingBottom":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$6;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v6, v6, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedScroller:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    invoke-virtual {v6, v8, v5, v8, v0}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->setPadding(IIII)V

    .line 395
    return-void
.end method
