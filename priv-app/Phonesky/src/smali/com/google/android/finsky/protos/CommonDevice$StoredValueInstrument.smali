.class public final Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoredValueInstrument"
.end annotation


# instance fields
.field public balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

.field public hasType:Z

.field public topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1629
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1630
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->clear()Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    .line 1631
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1634
    const/16 v0, 0x20

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->type:I

    .line 1635
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->hasType:Z

    .line 1636
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    .line 1637
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    .line 1638
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->cachedSize:I

    .line 1639
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1659
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1660
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->type:I

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->hasType:Z

    if-eqz v1, :cond_1

    .line 1661
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1664
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    if-eqz v1, :cond_2

    .line 1665
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1668
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    if-eqz v1, :cond_3

    .line 1669
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1672
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1680
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1681
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1685
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1686
    :sswitch_0
    return-object p0

    .line 1691
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1692
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1695
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->type:I

    .line 1696
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->hasType:Z

    goto :goto_0

    .line 1702
    .end local v1    # "value":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    if-nez v2, :cond_1

    .line 1703
    new-instance v2, Lcom/google/android/finsky/protos/CommonDevice$Money;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CommonDevice$Money;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    .line 1705
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1709
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    if-nez v2, :cond_2

    .line 1710
    new-instance v2, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    .line 1712
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1681
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    .line 1692
    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1602
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1645
    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->type:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->hasType:Z

    if-eqz v0, :cond_1

    .line 1646
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1648
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    if-eqz v0, :cond_2

    .line 1649
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1651
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    if-eqz v0, :cond_3

    .line 1652
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1654
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1655
    return-void
.end method
