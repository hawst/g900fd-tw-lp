.class public Lcom/google/android/finsky/layout/OrderHistorySection;
.super Lcom/google/android/finsky/layout/CardLinearLayout;
.source "OrderHistorySection.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field private mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

.field protected mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mDfeList:Lcom/google/android/finsky/api/model/DfeList;

.field private mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mMoreFooter:Landroid/view/ViewGroup;

.field protected mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mOrderHistoryHolder:Landroid/view/ViewGroup;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mProgressIndicator:Landroid/view/View;

.field private mTitleView:Landroid/widget/TextView;

.field private mToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/CardLinearLayout;-><init>(Landroid/content/Context;)V

    .line 33
    const/16 v0, 0xa29

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/CardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/16 v0, 0xa29

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/OrderHistorySection;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/OrderHistorySection;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/OrderHistorySection;)Lcom/google/android/finsky/api/model/DfeList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/OrderHistorySection;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/OrderHistorySection;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/OrderHistorySection;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mTitleView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/OrderHistorySection;)Lcom/google/android/finsky/api/model/DfeToc;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/OrderHistorySection;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    return-object v0
.end method

.method private renderOrderHistoryList()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    .line 101
    iget-object v4, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    invoke-virtual {v4}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->getItemCount()I

    move-result v1

    .line 104
    .local v1, "orderHistoryCount":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mOrderHistoryHolder:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 106
    invoke-static {v6, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 108
    .local v0, "numRows":I
    const/4 v3, 0x0

    .local v3, "rowIndex":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 109
    iget-object v4, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    iget-object v5, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mOrderHistoryHolder:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3, v5}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->getView(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 110
    .local v2, "row":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mOrderHistoryHolder:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 108
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 113
    .end local v2    # "row":Landroid/view/View;
    :cond_0
    if-le v1, v6, :cond_1

    .line 114
    iget-object v4, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mMoreFooter:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 115
    iget-object v4, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mMoreFooter:Landroid/view/ViewGroup;

    new-instance v5, Lcom/google/android/finsky/layout/OrderHistorySection$1;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/layout/OrderHistorySection$1;-><init>(Lcom/google/android/finsky/layout/OrderHistorySection;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    :goto_1
    return-void

    .line 126
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mMoreFooter:Landroid/view/ViewGroup;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeList;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 11
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "dfeList"    # Lcom/google/android/finsky/api/model/DfeList;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "onRefundActionListener"    # Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;
    .param p7, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 70
    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iput-object p1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 73
    iput-object p2, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 74
    iput-object p4, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    .line 75
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 76
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 77
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v2}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 78
    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->isReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mProgressIndicator:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 80
    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mOrderHistoryHolder:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 81
    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mMoreFooter:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 98
    :goto_0
    return-void

    .line 83
    :cond_1
    new-instance v1, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/OrderHistorySection;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    iget-object v5, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    const/4 v7, 0x0

    move-object/from16 v4, p5

    move-object/from16 v6, p6

    move-object v8, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/api/model/DfeList;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    .line 85
    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mProgressIndicator:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 86
    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mOrderHistoryHolder:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 89
    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v9

    .line 90
    .local v9, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    if-eqz v9, :cond_2

    .line 91
    invoke-virtual {v9}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v10

    .line 92
    .local v10, "cookie":[B
    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v1, v10}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 94
    .end local v10    # "cookie":[B
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/OrderHistorySection;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 96
    invoke-direct {p0}, Lcom/google/android/finsky/layout/OrderHistorySection;->renderOrderHistoryList()V

    goto :goto_0
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 152
    invoke-static {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 153
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->onDestroyView()V

    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->onDestroy()V

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    .line 136
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Lcom/google/android/finsky/layout/CardLinearLayout;->onFinishInflate()V

    .line 61
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistorySection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mTitleView:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f0a0268

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistorySection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mOrderHistoryHolder:Landroid/view/ViewGroup;

    .line 63
    const v0, 0x7f0a0269

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistorySection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mMoreFooter:Landroid/view/ViewGroup;

    .line 64
    const v0, 0x7f0a0109

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistorySection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection;->mProgressIndicator:Landroid/view/View;

    .line 65
    return-void
.end method
