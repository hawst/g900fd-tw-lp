.class Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$3;
.super Ljava/lang/Object;
.source "SetupWizardRestoreAppsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getDeviceMenuOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)V
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$3;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 420
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$3;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$500(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x9cd

    iget-object v3, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$3;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 423
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$3;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;
    invoke-static {v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$1000(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->isLightTheme()Z

    move-result v0

    if-eqz v0, :cond_0

    const v9, 0x7f0d01bf

    .line 426
    .local v9, "themeId":I
    :goto_0
    new-instance v7, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 427
    .local v7, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v0, 0x7f0401af

    invoke-virtual {v7, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setLayoutId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c0386

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c0134

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setThemeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x9c5

    const/16 v3, 0x9c7

    const/16 v4, 0x9c8

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 440
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 441
    .local v6, "argumentsBundle":Landroid/os/Bundle;
    const-string v0, "SetupWizardRestoreDeviceDialogView.selectedDevicePosition"

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$3;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I
    invoke-static {v1}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$400(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)I

    move-result v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 444
    const-string v0, "SetupWizardRestoreDeviceDialogView.selectedDevices"

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$3;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mParcelableBackupDeviceInfos:[Landroid/os/Parcelable;
    invoke-static {v1}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$1100(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)[Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 448
    invoke-virtual {v7, v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setViewConfiguration(Landroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 449
    invoke-virtual {v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v8

    .line 450
    .local v8, "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$3;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "SetupWizardRestoreAppsActivity.backupDeviceDialog"

    invoke-virtual {v8, v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 451
    return-void

    .line 423
    .end local v6    # "argumentsBundle":Landroid/os/Bundle;
    .end local v7    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v8    # "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .end local v9    # "themeId":I
    :cond_0
    const v9, 0x7f0d01be

    goto :goto_0
.end method
