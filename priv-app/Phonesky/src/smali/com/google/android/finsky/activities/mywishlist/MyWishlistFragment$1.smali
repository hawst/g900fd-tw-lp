.class Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$1;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.source "MyWishlistFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$1;->this$0:Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 86
    return-void
.end method

.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 90
    const v0, 0x7f0401d8

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 91
    return-void
.end method

.method protected addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 95
    return-void
.end method

.method protected alwaysUseFloatingBackground()Z
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method protected getContentProtectionMode()I
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$1;->mContext:Landroid/content/Context;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method protected getHeaderMode()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method protected getListViewId()I
    .locals 1

    .prologue
    .line 120
    const v0, 0x7f0a03ca

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x2

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method
