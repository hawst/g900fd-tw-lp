.class public Lcom/google/android/finsky/activities/SettingsListPreference;
.super Landroid/preference/ListPreference;
.source "SettingsListPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;,
        Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 68
    move-object v0, p1

    check-cast v0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;

    .line 69
    .local v0, "savedState":Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;
    # getter for: Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entries:[Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->access$000(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;)[Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/SettingsListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 70
    # getter for: Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entryValues:[Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->access$100(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;)[Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/SettingsListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 71
    # getter for: Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->value:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->access$200(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/SettingsListPreference;->setValue(Ljava/lang/String;)V

    .line 72
    # getter for: Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->summary:Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->access$300(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/SettingsListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 73
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/preference/ListPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 74
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 78
    invoke-super {p0}, Landroid/preference/ListPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 79
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 80
    .local v0, "state":Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    # setter for: Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entries:[Ljava/lang/CharSequence;
    invoke-static {v0, v2}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->access$002(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;[Ljava/lang/CharSequence;)[Ljava/lang/CharSequence;

    .line 81
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v2

    # setter for: Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entryValues:[Ljava/lang/CharSequence;
    invoke-static {v0, v2}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->access$102(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;[Ljava/lang/CharSequence;)[Ljava/lang/CharSequence;

    .line 82
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->value:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->access$202(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;Ljava/lang/String;)Ljava/lang/String;

    .line 83
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsListPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v2

    # setter for: Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->summary:Ljava/lang/CharSequence;
    invoke-static {v0, v2}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->access$302(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 84
    return-object v0
.end method

.method public setEntriesAndValues([Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;)V
    .locals 7
    .param p1, "listEntries"    # [Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;

    .prologue
    .line 39
    array-length v3, p1

    .line 40
    .local v3, "length":I
    new-array v0, v3, [Ljava/lang/CharSequence;

    .line 41
    .local v0, "entries":[Ljava/lang/CharSequence;
    new-array v1, v3, [Ljava/lang/CharSequence;

    .line 42
    .local v1, "entryValues":[Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 43
    aget-object v4, p1, v2

    .line 44
    .local v4, "listEntry":Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsListPreference;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4}, Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;->getResource()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v2

    .line 45
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    .line 42
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 47
    .end local v4    # "listEntry":Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SettingsListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 48
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/SettingsListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 49
    return-void
.end method

.method public setValueAndUpdateSummary(Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;)V
    .locals 1
    .param p1, "entry"    # Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;

    .prologue
    .line 55
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SettingsListPreference;->setValue(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsListPreference;->updateListPreferenceSummary()V

    .line 57
    return-void
.end method

.method public updateListPreferenceSummary()V
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SettingsListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 64
    return-void
.end method
