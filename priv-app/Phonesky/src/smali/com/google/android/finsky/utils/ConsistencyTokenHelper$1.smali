.class final Lcom/google/android/finsky/utils/ConsistencyTokenHelper$1;
.super Landroid/os/AsyncTask;
.source "ConsistencyTokenHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/ConsistencyTokenHelper;->get(Landroid/content/Context;Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$listener:Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/finsky/utils/ConsistencyTokenHelper$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/finsky/utils/ConsistencyTokenHelper$1;->val$listener:Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 36
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/ConsistencyTokenHelper$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 1
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/utils/ConsistencyTokenHelper$1;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/finsky/utils/ConsistencyTokenHelper;->getBlocking(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 36
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/ConsistencyTokenHelper$1;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1
    .param p1, "checkinConsistencyToken"    # Ljava/lang/String;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/utils/ConsistencyTokenHelper$1;->val$listener:Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;->onTokenReceived(Ljava/lang/String;)V

    .line 43
    return-void
.end method
