.class Lcom/google/android/finsky/receivers/InstallerTask;
.super Ljava/lang/Object;
.source "InstallerTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/receivers/InstallerTask$10;
    }
.end annotation


# static fields
.field private static PROGRESS_INSTALLING:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

.field private static PROGRESS_NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

.field private static final SUPPORTED_PATCH_FORMATS:[Ljava/lang/String;


# instance fields
.field private mApkCompleted:J

.field private mApkSize:J

.field private final mAppStates:Lcom/google/android/finsky/appstate/AppStates;

.field private final mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

.field private mDownloadStatus:I

.field private final mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

.field private final mInstaller:Lcom/google/android/finsky/receivers/InstallerImpl;

.field private final mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

.field private mIsUpdate:Z

.field private mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

.field private mMobileDataAllowed:Z

.field private final mNotifier:Lcom/google/android/finsky/utils/Notifier;

.field private mObbMain:Lcom/google/android/finsky/download/obb/Obb;

.field private mObbMainCompleted:J

.field private mObbMainSize:J

.field private mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

.field private mObbPatchCompleted:J

.field private mObbPatchSize:J

.field private final mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

.field private mRecoveredIntoState:I

.field private mShowCompletionNotifications:Z

.field private mShowErrorNotifications:Z

.field private mShowProgress:Z

.field private mTitle:Ljava/lang/String;

.field private mTotalSize:J

.field public final packageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v2, 0x0

    .line 127
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "1"

    aput-object v1, v0, v6

    const/4 v1, 0x1

    const-string v4, "2"

    aput-object v4, v0, v1

    sput-object v0, Lcom/google/android/finsky/receivers/InstallerTask;->SUPPORTED_PATCH_FORMATS:[Ljava/lang/String;

    .line 690
    new-instance v0, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    sget-object v1, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-wide v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;-><init>(Lcom/google/android/finsky/receivers/Installer$InstallerState;JJI)V

    sput-object v0, Lcom/google/android/finsky/receivers/InstallerTask;->PROGRESS_NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    .line 692
    new-instance v0, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    sget-object v1, Lcom/google/android/finsky/receivers/Installer$InstallerState;->INSTALLING:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-wide v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;-><init>(Lcom/google/android/finsky/receivers/Installer$InstallerState;JJI)V

    sput-object v0, Lcom/google/android/finsky/receivers/InstallerTask;->PROGRESS_INSTALLING:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/finsky/receivers/InstallerImpl;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/download/DownloadQueue;Lcom/google/android/finsky/utils/Notifier;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/installer/PackageInstallerFacade;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "installer"    # Lcom/google/android/finsky/receivers/InstallerImpl;
    .param p3, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p4, "downloadQueue"    # Lcom/google/android/finsky/download/DownloadQueue;
    .param p5, "notifier"    # Lcom/google/android/finsky/utils/Notifier;
    .param p6, "installPolicies"    # Lcom/google/android/finsky/installer/InstallPolicies;
    .param p7, "packageInstaller"    # Lcom/google/android/finsky/installer/PackageInstallerFacade;

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    .line 175
    iput-object p2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstaller:Lcom/google/android/finsky/receivers/InstallerImpl;

    .line 176
    iput-object p3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    .line 177
    iput-object p4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    .line 178
    iput-object p5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    .line 179
    invoke-virtual {p3}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    .line 180
    iput-object p6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    .line 181
    iput-object p7, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    .line 182
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/appstate/InstallerDataStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/appstate/AppStates;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/receivers/InstallerTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowProgress:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/receivers/InstallerTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/receivers/InstallerTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mIsUpdate:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/finsky/receivers/InstallerTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowCompletionNotifications:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/finsky/receivers/InstallerTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowErrorNotifications:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyFailedInstall(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/finsky/receivers/InstallerTask;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/finsky/receivers/InstallerTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->reportExternalCopyFailure(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/download/DownloadQueue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .param p2, "x2"    # Z

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->processDeliveryData(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Z)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/finsky/receivers/InstallerTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->cleanExternalStorage()V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/finsky/receivers/InstallerTask;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->tryRestartWithInhibitFlag(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/finsky/receivers/InstallerTask;ILandroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/net/Uri;

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILandroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->reportGzippedApkFailure(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->startNextDownload(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/receivers/InstallerTask;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # I

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->deliveryResponseToInstallerError(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/analytics/PlayStore$AppData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p2, "x2"    # I

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/finsky/receivers/InstallerTask;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/android/volley/VolleyError;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/InstallerTask;
    .param p1, "x1"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->volleyErrorToInstallerError(Lcom/android/volley/VolleyError;)I

    move-result v0

    return v0
.end method

.method static synthetic access$900()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcom/google/android/finsky/receivers/InstallerTask;->SUPPORTED_PATCH_FORMATS:[Ljava/lang/String;

    return-object v0
.end method

.method private advanceState()V
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 719
    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v9, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 720
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v0, :cond_0

    iget-object v8, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-nez v8, :cond_2

    .line 721
    :cond_0
    const-string v7, "Unexpected missing installer data for %s"

    new-array v8, v10, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 722
    invoke-virtual {p0, v10}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 820
    :cond_1
    :goto_0
    :sswitch_0
    return-void

    .line 725
    :cond_2
    iget-object v4, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 726
    .local v4, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 816
    const-string v7, "Bad state %d for %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    iget-object v9, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 818
    invoke-virtual {p0, v10}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    goto :goto_0

    .line 728
    :sswitch_1
    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDesiredVersion()I

    move-result v1

    .line 730
    .local v1, "desiredVersion":I
    iget-object v6, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    .line 731
    .local v6, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-eqz v6, :cond_4

    iget v3, v6, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 732
    .local v3, "installedVersion":I
    :goto_1
    if-lt v3, v1, :cond_5

    .line 740
    if-le v3, v1, :cond_3

    .line 741
    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v8}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v8, v9, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDesiredVersion(Ljava/lang/String;I)V

    .line 745
    :cond_3
    const/16 v8, 0x46

    check-cast v7, Ljava/lang/String;

    invoke-direct {p0, v8, v7}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    .line 746
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V

    goto :goto_0

    .line 731
    .end local v3    # "installedVersion":I
    :cond_4
    const/4 v3, -0x1

    goto :goto_1

    .line 751
    .restart local v3    # "installedVersion":I
    :cond_5
    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->getInstalledVersionForOtherUser(Lcom/google/android/finsky/appstate/AppStates$AppState;)I

    move-result v5

    .line 752
    .local v5, "otherUserVersionCode":I
    if-lt v5, v1, :cond_7

    .line 755
    if-le v5, v1, :cond_6

    .line 756
    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v8}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v8, v9, v5}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDesiredVersion(Ljava/lang/String;I)V

    .line 760
    :cond_6
    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v2

    .line 761
    .local v2, "flags":I
    or-int/lit8 v2, v2, 0x20

    .line 762
    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    iget-object v9, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v8, v9, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    .line 763
    const/16 v8, 0x32

    check-cast v7, Ljava/lang/String;

    invoke-direct {p0, v8, v7}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    .line 764
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V

    goto :goto_0

    .line 768
    .end local v2    # "flags":I
    :cond_7
    invoke-direct {p0, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->checkValidDeliveryData(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 769
    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->requestDeliveryData(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    goto :goto_0

    .line 775
    .end local v1    # "desiredVersion":I
    .end local v3    # "installedVersion":I
    .end local v5    # "otherUserVersionCode":I
    .end local v6    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_8
    :sswitch_2
    invoke-direct {p0, v4, v11}, Lcom/google/android/finsky/receivers/InstallerTask;->processDeliveryData(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Z)V

    .line 782
    :sswitch_3
    invoke-direct {p0, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->startNextDownload(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    goto/16 :goto_0

    .line 791
    :sswitch_4
    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->startCopyFromExternalStorage(Lcom/google/android/finsky/appstate/AppStates$AppState;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 794
    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v7

    and-int/lit8 v7, v7, 0x4

    if-eqz v7, :cond_9

    .line 795
    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->startApplyingPatch(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    goto/16 :goto_0

    .line 798
    :cond_9
    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v7

    and-int/lit16 v7, v7, 0x200

    if-eqz v7, :cond_a

    .line 799
    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->startInstallingGzippedApk(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    goto/16 :goto_0

    .line 804
    :cond_a
    :sswitch_5
    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->startInstaller(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    goto/16 :goto_0

    .line 807
    :sswitch_6
    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->cleanup(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    goto/16 :goto_0

    .line 811
    :sswitch_7
    iget-object v7, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstaller:Lcom/google/android/finsky/receivers/InstallerImpl;

    invoke-virtual {v7, v0}, Lcom/google/android/finsky/receivers/InstallerImpl;->clearInstallerState(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    .line 812
    iget-object v7, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstaller:Lcom/google/android/finsky/receivers/InstallerImpl;

    invoke-virtual {v7, p0}, Lcom/google/android/finsky/receivers/InstallerImpl;->taskFinished(Lcom/google/android/finsky/receivers/InstallerTask;)V

    goto/16 :goto_0

    .line 726
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0xa -> :sswitch_2
        0x14 -> :sswitch_3
        0x19 -> :sswitch_0
        0x1e -> :sswitch_3
        0x23 -> :sswitch_0
        0x28 -> :sswitch_3
        0x2d -> :sswitch_0
        0x32 -> :sswitch_4
        0x3c -> :sswitch_5
        0x46 -> :sswitch_6
        0x50 -> :sswitch_7
    .end sparse-switch
.end method

.method private canDownloadApkToExternalStorage(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z
    .locals 14
    .param p1, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    .line 2093
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v4

    .line 2095
    .local v4, "flags":I
    const/high16 v5, 0x10000

    and-int/2addr v5, v4

    if-eqz v5, :cond_0

    .line 2096
    const/4 v5, 0x0

    .line 2140
    :goto_0
    return v5

    .line 2100
    :cond_0
    and-int/lit16 v5, v4, 0x1000

    if-eqz v5, :cond_1

    .line 2101
    const/4 v5, 0x1

    goto :goto_0

    .line 2104
    :cond_1
    and-int/lit16 v5, v4, 0x2000

    if-eqz v5, :cond_2

    .line 2105
    const/4 v5, 0x0

    goto :goto_0

    .line 2112
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v5

    iget-wide v2, v5, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    .line 2113
    .local v2, "downloadSize":J
    sget-object v5, Lcom/google/android/finsky/config/G;->downloadExternalFileSizeMinBytes:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2114
    .local v6, "minDownloadSizeForExternal":J
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAccountName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Lcom/google/android/finsky/FinskyApp;->getExperiments(Ljava/lang/String;)Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v5

    const-string v10, "cl:installer.force_through_sdcard"

    invoke-virtual {v5, v10}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2117
    const-wide/16 v6, 0x0

    .line 2119
    :cond_3
    cmp-long v5, v2, v6

    if-gez v5, :cond_4

    .line 2120
    const/4 v5, 0x0

    goto :goto_0

    .line 2124
    :cond_4
    invoke-static {}, Lcom/google/android/finsky/download/Storage;->externalStorageAvailableSpace()J

    move-result-wide v8

    .line 2125
    .local v8, "partitionAvailableBytes":J
    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-gtz v5, :cond_5

    .line 2127
    const/4 v5, 0x0

    goto :goto_0

    .line 2129
    :cond_5
    sget-object v5, Lcom/google/android/finsky/config/G;->downloadExternalFreeSpaceThresholdBytes:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long/2addr v8, v10

    .line 2133
    sget-object v5, Lcom/google/android/finsky/config/G;->downloadExternalFreeSpaceFactor:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v10, v5

    mul-long/2addr v10, v2

    const-wide/16 v12, 0x64

    div-long v0, v10, v12

    .line 2135
    .local v0, "apkSize":J
    cmp-long v5, v8, v0

    if-gez v5, :cond_6

    .line 2136
    const/4 v5, 0x0

    goto :goto_0

    .line 2140
    :cond_6
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private canDownloadPatch(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z
    .locals 28
    .param p1, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    .line 2147
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v8

    .line 2148
    .local v8, "flags":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v18

    .line 2149
    .local v18, "packageName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v4

    .line 2153
    .local v4, "deliveryData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    and-int/lit8 v23, v8, 0x4

    if-eqz v23, :cond_1

    .line 2156
    iget-object v0, v4, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    move-object/from16 v23, v0

    if-eqz v23, :cond_0

    .line 2157
    const/16 v23, 0x1

    .line 2248
    :goto_0
    return v23

    .line 2159
    :cond_0
    const-string v23, "Missing patch for %s while is_patch set in %d"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v18, v24, v25

    const/16 v25, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-object/from16 v23, v0

    and-int/lit8 v24, v8, -0x5

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    move/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    .line 2167
    :cond_1
    and-int/lit8 v23, v8, 0x8

    if-eqz v23, :cond_2

    .line 2168
    const/16 v23, 0x0

    goto :goto_0

    .line 2175
    :cond_2
    iget-object v0, v4, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    move-object/from16 v20, v0

    .line 2176
    .local v20, "patchData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;
    if-nez v20, :cond_3

    .line 2177
    const/16 v23, 0x0

    goto :goto_0

    .line 2181
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/appstate/AppStates;->getPackageStateRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v19

    .line 2182
    .local v19, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-nez v19, :cond_4

    .line 2183
    const-string v23, "no-base-app-installed"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 2184
    const/16 v23, 0x0

    goto :goto_0

    .line 2186
    :cond_4
    move-object/from16 v0, v20

    iget v7, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseVersionCode:I

    .line 2187
    .local v7, "expectedVersion":I
    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    move/from16 v23, v0

    move/from16 v0, v23

    if-eq v0, v7, :cond_5

    .line 2188
    const-string v23, "wrong-base-app-installed"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 2189
    const-string v23, "Cannot patch %s, need version %d but has %d"

    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v18, v24, v25

    const/16 v25, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    move/from16 v26, v0

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2191
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 2195
    :cond_5
    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    .line 2196
    .local v21, "returnSourceDir":[Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->isInstalledForwardLocked(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v15

    .line 2197
    .local v15, "isForwardLocked":Z
    const/16 v23, 0x0

    aget-object v22, v21, v23

    .line 2198
    .local v22, "sourceDir":Ljava/lang/String;
    if-eqz v15, :cond_6

    .line 2199
    const-string v23, "base-app-dirs-mismatch"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200
    const-string v23, "Cannot patch %s, source dir is %s"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v18, v24, v25

    const/16 v25, 0x1

    aput-object v22, v24, v25

    invoke-static/range {v23 .. v24}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2201
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 2211
    :cond_6
    invoke-static {}, Lcom/google/android/finsky/download/Storage;->dataPartitionAvailableSpace()J

    move-result-wide v10

    .line 2213
    .local v10, "freeOnData":J
    sget-object v23, Lcom/google/android/finsky/config/G;->downloadPatchFreeSpaceFactor:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Integer;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 2214
    .local v9, "freeSpacePercent":I
    int-to-long v0, v9

    move-wide/from16 v24, v0

    iget-wide v0, v4, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    move-wide/from16 v26, v0

    mul-long v24, v24, v26

    const-wide/16 v26, 0x64

    div-long v16, v24, v26

    .line 2215
    .local v16, "needFree":J
    cmp-long v23, v10, v16

    if-gez v23, :cond_7

    .line 2216
    const-string v23, "free-space"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 2217
    const-string v23, "Cannot patch %s, need %d, free %d"

    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v18, v24, v25

    const/16 v25, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2218
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 2222
    :cond_7
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2223
    .local v12, "fromFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v23

    if-nez v23, :cond_8

    .line 2224
    const-string v23, "base-file-exists"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225
    const-string v23, "Cannot patch %s, file does not exist %s"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v18, v24, v25

    const/16 v25, 0x1

    aput-object v12, v24, v25

    invoke-static/range {v23 .. v24}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2226
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 2230
    :cond_8
    :try_start_0
    new-instance v13, Ljava/io/FileInputStream;

    invoke-direct {v13, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 2231
    .local v13, "inputStream":Ljava/io/FileInputStream;
    const-wide/16 v24, -0x1

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->baseSignature:Ljava/lang/String;

    move-object/from16 v23, v0

    move-wide/from16 v0, v24

    move-object/from16 v2, v23

    invoke-static {v13, v0, v1, v2}, Lcom/google/android/finsky/utils/PackageManagerHelper;->verifyApk(Ljava/io/InputStream;JLjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2248
    const/16 v23, 0x1

    goto/16 :goto_0

    .line 2232
    .end local v13    # "inputStream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v6

    .line 2233
    .local v6, "error":Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError;
    const-string v23, "base-file-signature"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 2234
    const-string v23, "Cannot patch %s, bad hash, expect %s actual %s"

    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v18, v24, v25

    const/16 v25, 0x1

    iget-object v0, v6, Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError;->expected:Ljava/lang/String;

    move-object/from16 v26, v0

    aput-object v26, v24, v25

    const/16 v25, 0x2

    iget-object v0, v6, Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError;->actual:Ljava/lang/String;

    move-object/from16 v26, v0

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2236
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 2237
    .end local v6    # "error":Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError;
    :catch_1
    move-exception v5

    .line 2238
    .local v5, "e":Ljava/io/FileNotFoundException;
    const-string v23, "base-file-FileNotFoundException"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 2239
    const-string v23, "Cannot patch %s, FileNotFoundException, %s"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v18, v24, v25

    const/16 v25, 0x1

    aput-object v12, v24, v25

    invoke-static/range {v23 .. v24}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2240
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 2241
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v14

    .line 2242
    .local v14, "ioe":Ljava/io/IOException;
    const-string v23, "base-file-otherexception"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 2243
    const-string v23, "Cannot patch %s, unexpected exception %s"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v18, v24, v25

    const/16 v25, 0x1

    invoke-virtual {v14}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2245
    const/16 v23, 0x0

    goto/16 :goto_0
.end method

.method private cancelCleanup(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
    .locals 6
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    const/4 v5, 0x0

    .line 629
    const-string v2, "Cancel running installation of %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 631
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/finsky/download/DownloadQueue;->getByPackageName(Ljava/lang/String;)Lcom/google/android/finsky/download/Download;

    move-result-object v0

    .line 632
    .local v0, "download":Lcom/google/android/finsky/download/Download;
    if-eqz v0, :cond_0

    .line 633
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v2, v0}, Lcom/google/android/finsky/download/DownloadQueue;->cancel(Lcom/google/android/finsky/download/Download;)V

    .line 637
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->cancelSession(Ljava/lang/String;)V

    .line 640
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstaller:Lcom/google/android/finsky/receivers/InstallerImpl;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->clearInstallerState(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    .line 643
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->cleanExternalStorage()V

    .line 648
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    if-nez v2, :cond_1

    if-eqz p1, :cond_1

    iget-object v2, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 651
    iget-object v2, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-direct {p0, v2, v5}, Lcom/google/android/finsky/receivers/InstallerTask;->processDeliveryData(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Z)V

    .line 653
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v2, :cond_2

    .line 654
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->getTempFile()Ljava/io/File;

    move-result-object v1

    .line 655
    .local v1, "tempFile":Ljava/io/File;
    if-eqz v1, :cond_2

    .line 656
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 659
    .end local v1    # "tempFile":Ljava/io/File;
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v2, :cond_3

    .line 660
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->getTempFile()Ljava/io/File;

    move-result-object v1

    .line 661
    .restart local v1    # "tempFile":Ljava/io/File;
    if-eqz v1, :cond_3

    .line 662
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 667
    .end local v1    # "tempFile":Ljava/io/File;
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstaller:Lcom/google/android/finsky/receivers/InstallerImpl;

    invoke-virtual {v2, p0}, Lcom/google/android/finsky/receivers/InstallerImpl;->taskFinished(Lcom/google/android/finsky/receivers/InstallerTask;)V

    .line 668
    return-void
.end method

.method public static checkForEmptyTitle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V
    .locals 6
    .param p0, "caller"    # Ljava/lang/String;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 561
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 581
    :goto_0
    return-void

    .line 564
    :cond_0
    if-nez p3, :cond_1

    .line 565
    const-string v0, "b/11413796 - installerData is null"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 576
    :goto_1
    if-nez p2, :cond_3

    .line 577
    const-string v0, "b/11413796 - %s for %s (null title)"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p0, v3, v2

    aput-object p1, v3, v1

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 567
    :cond_1
    const-string v0, "b/11413796 - acct:  %s"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 569
    const-string v0, "b/11413796 - vers:  %d"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDesiredVersion()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 570
    const-string v0, "b/11413796 - uri:   %s"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDownloadUri()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 571
    const-string v0, "b/11413796 - flags: %d"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 572
    const-string v0, "b/11413796 - state: %d"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 573
    const-string v0, "b/11413796 - pkg:   %s"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 574
    const-string v3, "b/11413796 - data?  %b"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    .line 579
    :cond_3
    const-string v0, "b/11413796 - %s for %s (empty title)"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p0, v3, v2

    aput-object p1, v3, v1

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private checkValidDeliveryData(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z
    .locals 14
    .param p1, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    const-wide/16 v12, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 901
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v6

    if-nez v6, :cond_0

    move v6, v7

    .line 922
    :goto_0
    return v6

    .line 904
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryDataTimestampMs()J

    move-result-wide v4

    .line 905
    .local v4, "timestampMs":J
    cmp-long v6, v4, v12

    if-nez v6, :cond_1

    move v6, v8

    .line 908
    goto :goto_0

    .line 910
    :cond_1
    sget-object v6, Lcom/google/android/finsky/config/G;->deliveryDataExpirationMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    add-long v0, v4, v10

    .line 911
    .local v0, "expirationMs":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    cmp-long v6, v0, v10

    if-gez v6, :cond_3

    .line 912
    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-interface {v6, v8, v9, v12, v13}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDeliveryData(Ljava/lang/String;Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;J)V

    .line 915
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v2

    .line 916
    .local v2, "flags":I
    and-int/lit8 v3, v2, -0x5

    .line 917
    .local v3, "newFlags":I
    if-eq v3, v2, :cond_2

    .line 918
    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v6, v8, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    :cond_2
    move v6, v7

    .line 920
    goto :goto_0

    .end local v2    # "flags":I
    .end local v3    # "newFlags":I
    :cond_3
    move v6, v8

    .line 922
    goto :goto_0
.end method

.method private cleanExternalStorage()V
    .locals 8

    .prologue
    .line 1875
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/FinskyApp;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1876
    .local v1, "externalFilesDir":Ljava/io/File;
    if-eqz v1, :cond_0

    .line 1877
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 1878
    .local v3, "files":[Ljava/io/File;
    if-eqz v3, :cond_0

    .line 1879
    move-object v0, v3

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 1880
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1879
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1884
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "files":[Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    return-void
.end method

.method public static cleanObbDirectory(Lcom/google/android/finsky/download/obb/Obb;Lcom/google/android/finsky/download/obb/Obb;Ljava/lang/String;)V
    .locals 11
    .param p0, "mainObb"    # Lcom/google/android/finsky/download/obb/Obb;
    .param p1, "patchObb"    # Lcom/google/android/finsky/download/obb/Obb;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x3

    .line 1844
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 1869
    :cond_0
    return-void

    .line 1848
    :cond_1
    const/4 v3, 0x0

    .line 1849
    .local v3, "mainObbFile":Ljava/io/File;
    if-eqz p0, :cond_2

    invoke-interface {p0}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v8

    if-ne v8, v9, :cond_2

    .line 1850
    invoke-interface {p0}, Lcom/google/android/finsky/download/obb/Obb;->getFile()Ljava/io/File;

    move-result-object v3

    .line 1852
    :cond_2
    const/4 v5, 0x0

    .line 1853
    .local v5, "patchObbFile":Ljava/io/File;
    if-eqz p1, :cond_3

    invoke-interface {p1}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v8

    if-ne v8, v9, :cond_3

    .line 1854
    invoke-interface {p1}, Lcom/google/android/finsky/download/obb/Obb;->getFile()Ljava/io/File;

    move-result-object v5

    .line 1856
    :cond_3
    invoke-static {p2}, Lcom/google/android/finsky/download/obb/ObbFactory;->getParentDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 1857
    .local v4, "obbDir":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 1858
    .local v7, "targets":[Ljava/io/File;
    if-eqz v7, :cond_0

    .line 1859
    move-object v0, v7

    .local v0, "arr$":[Ljava/io/File;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v6, v0, v1

    .line 1860
    .local v6, "target":Ljava/io/File;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v6}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1859
    :cond_4
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1863
    :cond_5
    if-eqz v5, :cond_6

    invoke-virtual {v5, v6}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 1866
    :cond_6
    const-string v8, "OBB cleanup %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v6, v9, v10

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1867
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto :goto_1
.end method

.method private cleanup(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
    .locals 10
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    const/4 v9, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v8, 0x0

    .line 1758
    const/4 v1, -0x1

    .line 1759
    .local v1, "installedVersion":I
    iget-object v5, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v5, :cond_0

    .line 1760
    iget-object v5, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget v1, v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 1762
    :cond_0
    iget-object v5, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v5}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDesiredVersion()I

    move-result v5

    if-eq v1, v5, :cond_1

    .line 1764
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->cancelCleanup(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    .line 1765
    sget-object v4, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALL_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/16 v5, 0x38e

    invoke-direct {p0, v4, v5}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1825
    :goto_0
    return-void

    .line 1770
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v5}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v5

    if-eq v5, v9, :cond_2

    .line 1771
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v5}, Lcom/google/android/finsky/download/obb/Obb;->syncStateWithStorage()V

    .line 1772
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v5}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v5

    if-eq v5, v7, :cond_2

    .line 1773
    const-string v5, "Lost main obb file for %s"

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1774
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->cancelCleanup(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    .line 1775
    sget-object v5, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALL_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/16 v6, 0x38f

    invoke-direct {p0, v5, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1777
    const/16 v5, 0x38f

    invoke-direct {p0, v5, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    goto :goto_0

    .line 1781
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v5}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v5

    if-eq v5, v9, :cond_3

    .line 1782
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v5}, Lcom/google/android/finsky/download/obb/Obb;->syncStateWithStorage()V

    .line 1783
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v5}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v5

    if-eq v5, v7, :cond_3

    .line 1784
    const-string v5, "Lost patch obb file for %s"

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1785
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->cancelCleanup(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    .line 1786
    sget-object v5, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALL_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/16 v6, 0x390

    invoke-direct {p0, v5, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1788
    const/16 v5, 0x390

    invoke-direct {p0, v5, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    goto :goto_0

    .line 1794
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    iget-object v7, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-static {v5, v6, v7}, Lcom/google/android/finsky/receivers/InstallerTask;->cleanObbDirectory(Lcom/google/android/finsky/download/obb/Obb;Lcom/google/android/finsky/download/obb/Obb;Ljava/lang/String;)V

    .line 1795
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->cleanExternalStorage()V

    .line 1801
    const/16 v5, 0x50

    check-cast v4, Ljava/lang/String;

    invoke-direct {p0, v5, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    .line 1803
    sget-object v4, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALLED:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v4, v8}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1806
    iget-boolean v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mIsUpdate:Z

    if-eqz v4, :cond_4

    .line 1807
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface {v4, v5, v6, v7}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setLastUpdateTimestampMs(Ljava/lang/String;J)V

    .line 1811
    :cond_4
    iget-object v2, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 1812
    .local v2, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    const-string v4, "cleanup"

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->checkForEmptyTitle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    .line 1815
    iget-boolean v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowCompletionNotifications:Z

    if-eqz v4, :cond_5

    .line 1816
    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 1817
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getContinueUrl()Ljava/lang/String;

    move-result-object v0

    .line 1818
    .local v0, "continueUrl":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mIsUpdate:Z

    invoke-interface {v4, v3, v5, v0, v6}, Lcom/google/android/finsky/utils/Notifier;->showSuccessfulInstallMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1824
    .end local v0    # "continueUrl":Ljava/lang/String;
    .end local v3    # "title":Ljava/lang/String;
    :goto_1
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V

    goto/16 :goto_0

    .line 1820
    :cond_5
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v4}, Lcom/google/android/finsky/utils/Notifier;->hideInstallingMessage()V

    goto :goto_1
.end method

.method private static computeCertificateHash(Landroid/content/pm/PackageInfo;)Ljava/lang/String;
    .locals 2
    .param p0, "packageInfo"    # Landroid/content/pm/PackageInfo;

    .prologue
    .line 886
    iget-object v0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v0, v0

    if-eqz v0, :cond_0

    .line 887
    iget-object v0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/Sha1Util;->secureHash([B)Ljava/lang/String;

    move-result-object v0

    .line 889
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private deliveryResponseToInstallerError(I)I
    .locals 1
    .param p1, "deliveryResponseStatus"    # I

    .prologue
    .line 1075
    packed-switch p1, :pswitch_data_0

    .line 1083
    const/16 v0, 0x3af

    :goto_0
    return v0

    .line 1077
    :pswitch_0
    const/16 v0, 0x3ac

    goto :goto_0

    .line 1079
    :pswitch_1
    const/16 v0, 0x3ad

    goto :goto_0

    .line 1081
    :pswitch_2
    const/16 v0, 0x3ae

    goto :goto_0

    .line 1075
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private generateDownload(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Lcom/google/android/finsky/download/Download;
    .locals 23
    .param p1, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    .line 1326
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v17

    .line 1328
    .local v17, "deliveryData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 1329
    .local v5, "title":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 1330
    .local v6, "packageName":Ljava/lang/String;
    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    const/4 v8, 0x0

    aget-object v2, v7, v8

    .line 1336
    .local v2, "authCookie":Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;
    const/4 v9, 0x0

    .line 1337
    .local v9, "fileUri":Landroid/net/Uri;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v20

    .line 1338
    .local v20, "oldFlags":I
    move/from16 v19, v20

    .line 1341
    .local v19, "newFlags":I
    invoke-direct/range {p0 .. p1}, Lcom/google/android/finsky/receivers/InstallerTask;->canDownloadApkToExternalStorage(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1342
    move/from16 v0, v19

    or-int/lit16 v0, v0, 0x1000

    move/from16 v19, v0

    .line 1344
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    const/4 v8, 0x1

    iput-boolean v8, v7, Lcom/google/android/finsky/analytics/PlayStore$AppData;->downloadExternal:Z

    .line 1345
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    const/4 v8, 0x1

    iput-boolean v8, v7, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasDownloadExternal:Z

    .line 1348
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    .line 1349
    .local v22, "tempName":Ljava/lang/String;
    new-instance v18, Ljava/io/File;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/finsky/FinskyApp;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-direct {v0, v7, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1350
    .local v18, "externalFile":Ljava/io/File;
    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    .line 1351
    const-string v7, "Downloading %s via external storage"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v6, v8, v14

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1354
    .end local v18    # "externalFile":Ljava/io/File;
    .end local v22    # "tempName":Ljava/lang/String;
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/google/android/finsky/receivers/InstallerTask;->canDownloadPatch(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1356
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    move-object/from16 v21, v0

    .line 1357
    .local v21, "patchData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->downloadUrl:Ljava/lang/String;

    .line 1358
    .local v4, "downloadUri":Ljava/lang/String;
    const-wide/16 v10, -0x1

    .line 1359
    .local v10, "actualSize":J
    move-object/from16 v0, v21

    iget-wide v12, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->maxPatchSize:J

    .line 1361
    .local v12, "maxDownloadSize":J
    or-int/lit8 v19, v19, 0x4

    .line 1378
    .end local v21    # "patchData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;
    :goto_0
    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 1379
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    move/from16 v0, v19

    invoke-interface {v7, v6, v0}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    .line 1382
    :cond_1
    new-instance v3, Lcom/google/android/finsky/download/DownloadImpl;

    iget-object v7, v2, Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;->name:Ljava/lang/String;

    iget-object v8, v2, Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;->value:Ljava/lang/String;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mMobileDataAllowed:Z

    if-nez v15, :cond_4

    const/4 v15, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowProgress:Z

    move/from16 v16, v0

    if-nez v16, :cond_5

    const/16 v16, 0x1

    :goto_2
    invoke-direct/range {v3 .. v16}, Lcom/google/android/finsky/download/DownloadImpl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/finsky/download/obb/Obb;ZZ)V

    .line 1385
    .local v3, "download":Lcom/google/android/finsky/download/Download;
    return-object v3

    .line 1362
    .end local v3    # "download":Lcom/google/android/finsky/download/Download;
    .end local v4    # "downloadUri":Ljava/lang/String;
    .end local v10    # "actualSize":J
    .end local v12    # "maxDownloadSize":J
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/download/Storage;->dataPartitionAvailableSpace()J

    move-result-wide v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v14, v15}, Lcom/google/android/finsky/receivers/InstallerTask;->canDownloadGzippedApk(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;J)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1365
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    .line 1366
    .restart local v4    # "downloadUri":Ljava/lang/String;
    const-wide/16 v10, -0x1

    .line 1367
    .restart local v10    # "actualSize":J
    move-object/from16 v0, v17

    iget-wide v12, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadSize:J

    .line 1369
    .restart local v12    # "maxDownloadSize":J
    move/from16 v0, v19

    or-int/lit16 v0, v0, 0x200

    move/from16 v19, v0

    goto :goto_0

    .line 1372
    .end local v4    # "downloadUri":Ljava/lang/String;
    .end local v10    # "actualSize":J
    .end local v12    # "maxDownloadSize":J
    :cond_3
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadUrl:Ljava/lang/String;

    .line 1373
    .restart local v4    # "downloadUri":Ljava/lang/String;
    move-object/from16 v0, v17

    iget-wide v10, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    .line 1374
    .restart local v10    # "actualSize":J
    move-wide v12, v10

    .restart local v12    # "maxDownloadSize":J
    goto :goto_0

    .line 1382
    :cond_4
    const/4 v15, 0x0

    goto :goto_1

    :cond_5
    const/16 v16, 0x0

    goto :goto_2
.end method

.method private generateObbDownload(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Lcom/google/android/finsky/download/obb/Obb;)Lcom/google/android/finsky/download/Download;
    .locals 19
    .param p1, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .param p2, "obb"    # Lcom/google/android/finsky/download/obb/Obb;

    .prologue
    .line 1392
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v17

    .line 1394
    .local v17, "context":Landroid/content/Context;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v18

    .line 1395
    .local v18, "deliveryData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    const v4, 0x7f0c029a

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getTitle()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v7, v8

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1397
    .local v5, "title":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 1398
    .local v6, "packageName":Ljava/lang/String;
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    const/4 v7, 0x0

    aget-object v2, v4, v7

    .line 1399
    .local v2, "authCookie":Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;
    invoke-interface/range {p2 .. p2}, Lcom/google/android/finsky/download/obb/Obb;->getTempFile()Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    .line 1400
    .local v9, "fileUri":Landroid/net/Uri;
    move-object/from16 v0, v18

    iget-wide v10, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    .line 1402
    .local v10, "downloadSize":J
    new-instance v3, Lcom/google/android/finsky/download/DownloadImpl;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/finsky/download/obb/Obb;->getUrl()Ljava/lang/String;

    move-result-object v4

    iget-object v7, v2, Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;->name:Ljava/lang/String;

    iget-object v8, v2, Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;->value:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mMobileDataAllowed:Z

    if-nez v12, :cond_0

    const/4 v15, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowProgress:Z

    if-nez v12, :cond_1

    const/16 v16, 0x1

    :goto_1
    move-wide v12, v10

    move-object/from16 v14, p2

    invoke-direct/range {v3 .. v16}, Lcom/google/android/finsky/download/DownloadImpl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/finsky/download/obb/Obb;ZZ)V

    .line 1405
    .local v3, "download":Lcom/google/android/finsky/download/Download;
    return-object v3

    .line 1402
    .end local v3    # "download":Lcom/google/android/finsky/download/Download;
    :cond_0
    const/4 v15, 0x0

    goto :goto_0

    :cond_1
    const/16 v16, 0x0

    goto :goto_1
.end method

.method private getCertHashForOtherUser()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 861
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getUsers()Lcom/google/android/finsky/utils/Users;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/utils/Users;->supportsMultiUser()Z

    move-result v5

    if-nez v5, :cond_1

    .line 882
    :cond_0
    :goto_0
    return-object v3

    .line 867
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 869
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    const/16 v6, 0x2040

    invoke-virtual {v2, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 871
    .local v1, "info":Landroid/content/pm/PackageInfo;
    iget-object v5, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v6, 0x800000

    and-int/2addr v5, v6

    if-nez v5, :cond_0

    .line 875
    const-string v5, "Found %s with %d signatures installed for another user"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v8, :cond_2

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v4

    :cond_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 877
    invoke-static {v1}, Lcom/google/android/finsky/receivers/InstallerTask;->computeCertificateHash(Landroid/content/pm/PackageInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 879
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 880
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method private static getInstallFailMessageId(I)I
    .locals 4
    .param p0, "returnCode"    # I

    .prologue
    const v3, 0x7f0c0179

    const v0, 0x7f0c016c

    const v2, 0x7f0c0178

    const v1, 0x7f0c016d

    .line 1632
    sparse-switch p0, :sswitch_data_0

    .line 1693
    const/4 v0, -0x1

    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    move v0, v1

    .line 1636
    goto :goto_0

    .line 1638
    :sswitch_2
    const v0, 0x7f0c016e

    goto :goto_0

    .line 1640
    :sswitch_3
    const v0, 0x7f0c016f

    goto :goto_0

    .line 1644
    :sswitch_4
    const v0, 0x7f0c0170

    goto :goto_0

    .line 1646
    :sswitch_5
    const v0, 0x7f0c0171

    goto :goto_0

    .line 1648
    :sswitch_6
    const v0, 0x7f0c0172

    goto :goto_0

    .line 1650
    :sswitch_7
    const v0, 0x7f0c0173

    goto :goto_0

    :sswitch_8
    move v0, v1

    .line 1652
    goto :goto_0

    .line 1654
    :sswitch_9
    const v0, 0x7f0c0174

    goto :goto_0

    .line 1656
    :sswitch_a
    const v0, 0x7f0c0175

    goto :goto_0

    .line 1658
    :sswitch_b
    const v0, 0x7f0c0176

    goto :goto_0

    .line 1660
    :sswitch_c
    const v0, 0x7f0c0177

    goto :goto_0

    :sswitch_d
    move v0, v1

    .line 1662
    goto :goto_0

    :sswitch_e
    move v0, v2

    .line 1664
    goto :goto_0

    :sswitch_f
    move v0, v3

    .line 1666
    goto :goto_0

    .line 1668
    :sswitch_10
    const v0, 0x7f0c017a

    goto :goto_0

    :sswitch_11
    move v0, v3

    .line 1670
    goto :goto_0

    .line 1672
    :sswitch_12
    const v0, 0x7f0c017b

    goto :goto_0

    :sswitch_13
    move v0, v1

    .line 1674
    goto :goto_0

    :sswitch_14
    move v0, v2

    .line 1676
    goto :goto_0

    :sswitch_15
    move v0, v2

    .line 1678
    goto :goto_0

    .line 1680
    :sswitch_16
    const v0, 0x7f0c017c

    goto :goto_0

    .line 1682
    :sswitch_17
    const v0, 0x7f0c017d

    goto :goto_0

    .line 1685
    :sswitch_18
    const v0, 0x7f0c017e

    goto :goto_0

    .line 1687
    :sswitch_19
    const v0, 0x7f0c0181

    goto :goto_0

    .line 1689
    :sswitch_1a
    const v0, 0x7f0c0180

    goto :goto_0

    .line 1691
    :sswitch_1b
    const v0, 0x7f0c0182

    goto :goto_0

    .line 1632
    nop

    :sswitch_data_0
    .sparse-switch
        -0x6d -> :sswitch_15
        -0x6c -> :sswitch_14
        -0x6b -> :sswitch_13
        -0x6a -> :sswitch_12
        -0x69 -> :sswitch_11
        -0x68 -> :sswitch_10
        -0x67 -> :sswitch_f
        -0x65 -> :sswitch_e
        -0x64 -> :sswitch_d
        -0x17 -> :sswitch_1b
        -0x16 -> :sswitch_1a
        -0x15 -> :sswitch_19
        -0x14 -> :sswitch_18
        -0x13 -> :sswitch_18
        -0x12 -> :sswitch_17
        -0x11 -> :sswitch_16
        -0x10 -> :sswitch_c
        -0xe -> :sswitch_b
        -0xd -> :sswitch_a
        -0xc -> :sswitch_9
        -0xb -> :sswitch_8
        -0xa -> :sswitch_7
        -0x9 -> :sswitch_6
        -0x8 -> :sswitch_5
        -0x7 -> :sswitch_4
        -0x5 -> :sswitch_0
        -0x4 -> :sswitch_3
        -0x3 -> :sswitch_2
        -0x2 -> :sswitch_1
        -0x1 -> :sswitch_0
    .end sparse-switch
.end method

.method private getInstalledVersionForOtherUser(Lcom/google/android/finsky/appstate/AppStates$AppState;)I
    .locals 8
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    const/4 v3, -0x1

    .line 833
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getUsers()Lcom/google/android/finsky/utils/Users;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/utils/Users;->supportsMultiUser()Z

    move-result v4

    if-nez v4, :cond_1

    .line 856
    :cond_0
    :goto_0
    return v3

    .line 837
    :cond_1
    iget-object v4, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-nez v4, :cond_0

    .line 841
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 843
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    const/16 v5, 0x2000

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 845
    .local v1, "info":Landroid/content/pm/PackageInfo;
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v5, 0x800000

    and-int/2addr v4, v5

    if-nez v4, :cond_0

    .line 849
    const-string v4, "Found %s version %d installed for another user"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 851
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 853
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 854
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method private getInstallerListener(Landroid/net/Uri;)Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1481
    new-instance v0, Lcom/google/android/finsky/receivers/InstallerTask$4;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask$4;-><init>(Lcom/google/android/finsky/receivers/InstallerTask;Landroid/net/Uri;)V

    return-object v0
.end method

.method static isGzippedPatch(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z
    .locals 4
    .param p0, "data"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    const/4 v1, 0x0

    .line 2830
    if-nez p0, :cond_1

    .line 2834
    :cond_0
    :goto_0
    return v1

    .line 2833
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v0

    .line 2834
    .local v0, "deliveryData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    iget v2, v2, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;->patchFormat:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isInstalledForwardLocked(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnSourceDir"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 2855
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 2857
    .local v2, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v2, p1, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2862
    .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v4, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 2863
    .local v4, "sourceDir":Ljava/lang/String;
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 2865
    .local v3, "publicSourceDir":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 2866
    aput-object v4, p2, v5

    .line 2869
    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2871
    :cond_1
    const/4 v5, 0x1

    .line 2874
    .end local v0    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "publicSourceDir":Ljava/lang/String;
    .end local v4    # "sourceDir":Ljava/lang/String;
    :cond_2
    :goto_0
    return v5

    .line 2858
    :catch_0
    move-exception v1

    .line 2860
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method private notifyFailedInstall(Ljava/lang/String;I)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I

    .prologue
    .line 1605
    invoke-static {p2}, Lcom/google/android/finsky/receivers/InstallerTask;->getInstallFailMessageId(I)I

    move-result v3

    .line 1606
    .local v3, "messageId":I
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    .line 1607
    .local v0, "appContext":Landroid/content/Context;
    if-ltz v3, :cond_0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1610
    .local v2, "message":Ljava/lang/String;
    :goto_0
    packed-switch p2, :pswitch_data_0

    .line 1616
    const/4 v1, -0x1

    .line 1620
    .local v1, "internalReturnCode":I
    :goto_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v4

    .line 1626
    .local v4, "notifier":Lcom/google/android/finsky/utils/Notifier;
    invoke-interface {v4, p1}, Lcom/google/android/finsky/utils/Notifier;->hideAllMessagesForPackage(Ljava/lang/String;)V

    .line 1627
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTitle:Ljava/lang/String;

    invoke-interface {v4, v5, p1, v2, v1}, Lcom/google/android/finsky/utils/Notifier;->showInstallationFailureMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1629
    return-void

    .line 1607
    .end local v1    # "internalReturnCode":I
    .end local v2    # "message":Ljava/lang/String;
    .end local v4    # "notifier":Lcom/google/android/finsky/utils/Notifier;
    :cond_0
    const v5, 0x7f0c017f

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1612
    .restart local v2    # "message":Ljava/lang/String;
    :pswitch_0
    const/4 v1, 0x1

    .line 1614
    .restart local v1    # "internalReturnCode":I
    goto :goto_1

    .line 1610
    nop

    :pswitch_data_0
    .packed-switch -0x68
        :pswitch_0
    .end packed-switch
.end method

.method private notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 2
    .param p1, "event"    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p2, "statusCode"    # I

    .prologue
    .line 2042
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstaller:Lcom/google/android/finsky/receivers/InstallerImpl;

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/finsky/receivers/InstallerImpl;->notifyListeners(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 2043
    return-void
.end method

.method private populateFields(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
    .locals 7
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 531
    iget-object v1, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 532
    .local v1, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    iget-object v2, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v2, :cond_3

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mIsUpdate:Z

    .line 533
    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getTitle()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTitle:Ljava/lang/String;

    .line 534
    const-string v2, "populateFields"

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTitle:Ljava/lang/String;

    invoke-static {v2, v5, v6, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->checkForEmptyTitle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    .line 536
    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v0

    .line 537
    .local v0, "flags":I
    and-int/lit8 v2, v0, 0x10

    if-nez v2, :cond_4

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowProgress:Z

    .line 538
    and-int/lit8 v2, v0, 0x1

    if-nez v2, :cond_5

    move v2, v3

    :goto_2
    iput-boolean v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowErrorNotifications:Z

    .line 539
    and-int/lit16 v2, v0, 0x80

    if-nez v2, :cond_0

    move v4, v3

    :cond_0
    iput-boolean v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowCompletionNotifications:Z

    .line 541
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .line 542
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDesiredVersion()I

    move-result v4

    iput v4, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 543
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    .line 544
    iget-object v2, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v2, :cond_1

    .line 545
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iget-object v4, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget v4, v4, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    iput v4, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    .line 546
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    .line 547
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iget-object v4, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget-boolean v4, v4, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isSystemApp:Z

    iput-boolean v4, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    .line 548
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    .line 550
    :cond_1
    and-int/lit16 v2, v0, 0x1000

    if-eqz v2, :cond_2

    .line 551
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->downloadExternal:Z

    .line 552
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput-boolean v3, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasDownloadExternal:Z

    .line 554
    :cond_2
    return-void

    .end local v0    # "flags":I
    :cond_3
    move v2, v4

    .line 532
    goto :goto_0

    .restart local v0    # "flags":I
    :cond_4
    move v2, v4

    .line 537
    goto :goto_1

    :cond_5
    move v2, v4

    .line 538
    goto :goto_2
.end method

.method private processDeliveryData(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Z)V
    .locals 10
    .param p1, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .param p2, "canSetMobileDataAllowed"    # Z

    .prologue
    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-wide/16 v8, 0x0

    .line 1122
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v0

    .line 1125
    .local v0, "deliveryData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    iget-wide v6, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    iput-wide v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mApkSize:J

    .line 1126
    iput-wide v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMainSize:J

    .line 1127
    iput-wide v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatchSize:J

    .line 1128
    iget-wide v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mApkSize:J

    iput-wide v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTotalSize:J

    .line 1131
    iput-wide v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mApkCompleted:J

    .line 1132
    iput-wide v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMainCompleted:J

    .line 1133
    iput-wide v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatchCompleted:J

    .line 1136
    iget-object v2, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1137
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v4}, Lcom/google/android/finsky/local/AssetUtils;->extractObb(Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;Ljava/lang/String;Z)Lcom/google/android/finsky/download/obb/Obb;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    .line 1138
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v2, :cond_0

    .line 1139
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->syncStateWithStorage()V

    .line 1140
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 1141
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->getSize()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMainSize:J

    .line 1142
    iget-wide v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTotalSize:J

    iget-wide v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMainSize:J

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTotalSize:J

    .line 1145
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v3}, Lcom/google/android/finsky/local/AssetUtils;->extractObb(Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;Ljava/lang/String;Z)Lcom/google/android/finsky/download/obb/Obb;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    .line 1146
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v2, :cond_1

    .line 1147
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->syncStateWithStorage()V

    .line 1148
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 1149
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->getSize()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatchSize:J

    .line 1150
    iget-wide v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTotalSize:J

    iget-wide v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatchSize:J

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTotalSize:J

    .line 1158
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v1

    .line 1159
    .local v1, "flags":I
    and-int/lit16 v2, v1, 0x800

    if-eqz v2, :cond_3

    .line 1160
    iput-boolean v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mMobileDataAllowed:Z

    .line 1180
    :cond_2
    :goto_0
    return-void

    .line 1167
    :cond_3
    and-int/lit8 v2, v1, 0x2

    if-eqz v2, :cond_4

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mMobileDataAllowed:Z

    .line 1174
    if-eqz p2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mMobileDataAllowed:Z

    if-nez v2, :cond_2

    .line 1175
    iget-wide v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTotalSize:J

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    invoke-virtual {v2}, Lcom/google/android/finsky/installer/InstallPolicies;->getMaxBytesOverMobileRecommended()J

    move-result-wide v8

    cmp-long v2, v6, v8

    if-gez v2, :cond_5

    :goto_2
    iput-boolean v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mMobileDataAllowed:Z

    .line 1176
    iget-boolean v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mMobileDataAllowed:Z

    if-eqz v2, :cond_2

    .line 1177
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstaller:Lcom/google/android/finsky/receivers/InstallerImpl;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/receivers/InstallerImpl;->setMobileDataAllowed(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v2, v4

    .line 1167
    goto :goto_1

    :cond_5
    move v3, v4

    .line 1175
    goto :goto_2
.end method

.method private recoverApk(Lcom/google/android/finsky/appstate/AppStates$AppState;Landroid/net/Uri;III)Z
    .locals 7
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .param p3, "status"    # I
    .param p4, "desired"    # I
    .param p5, "installed"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 443
    if-gt p4, p5, :cond_0

    .line 444
    const-string v3, "Recovery of %s skipped because desired= %d installed= %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 476
    :goto_0
    return v1

    .line 453
    :cond_0
    invoke-static {p3}, Lcom/google/android/finsky/download/DownloadManagerConstants;->isStatusCompleted(I)Z

    move-result v3

    if-nez v3, :cond_1

    const/16 v3, 0xc6

    if-eq p3, v3, :cond_1

    .line 456
    const-string v3, "Recovery of %s into downloading APK state"

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 457
    iget-object v1, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-direct {p0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->generateDownload(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Lcom/google/android/finsky/download/Download;

    move-result-object v0

    .line 458
    .local v0, "download":Lcom/google/android/finsky/download/Download;
    invoke-interface {v0, p2}, Lcom/google/android/finsky/download/Download;->setContentUri(Landroid/net/Uri;)V

    .line 459
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v1, v0}, Lcom/google/android/finsky/download/DownloadQueue;->addRecoveredDownload(Lcom/google/android/finsky/download/Download;)V

    move v1, v2

    .line 460
    goto :goto_0

    .line 463
    .end local v0    # "download":Lcom/google/android/finsky/download/Download;
    :cond_1
    invoke-static {p3}, Lcom/google/android/finsky/download/DownloadManagerConstants;->isStatusSuccess(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 465
    const-string v3, "Recovery of %s into ready to install state"

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 466
    const/16 v1, 0x32

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    .line 467
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V

    move v1, v2

    .line 468
    goto :goto_0

    .line 472
    :cond_2
    const-string v3, "Recovery of %s into error state, status= %d"

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 473
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 474
    sget-object v2, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v2, p3}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 475
    const/4 v2, 0x0

    invoke-direct {p0, p3, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private recoverInstalling(Lcom/google/android/finsky/appstate/AppStates$AppState;Landroid/net/Uri;III)Z
    .locals 5
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .param p3, "status"    # I
    .param p4, "desired"    # I
    .param p5, "installed"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 493
    if-ge p4, p5, :cond_0

    .line 495
    const-string v0, "Recovery of %s skipped because desired= %d installed= %d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x2

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 513
    :goto_0
    return v4

    .line 499
    :cond_0
    if-ne p4, p5, :cond_1

    .line 501
    const-string v0, "Recovery of %s - installation seems complete"

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 502
    const/16 v0, 0x46

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    .line 503
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V

    goto :goto_0

    .line 509
    :cond_1
    const-string v0, "Recovery of %s with incomplete installation"

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 510
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 511
    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->UNINSTALLED:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v0, p3}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    goto :goto_0
.end method

.method private recoverObb(Lcom/google/android/finsky/appstate/AppStates$AppState;Landroid/net/Uri;IIIZ)Z
    .locals 9
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .param p3, "status"    # I
    .param p4, "desired"    # I
    .param p5, "installed"    # I
    .param p6, "isPatch"    # Z

    .prologue
    .line 377
    if-eqz p6, :cond_0

    const-string v4, "Patch"

    .line 379
    .local v4, "tag":Ljava/lang/String;
    :goto_0
    if-gt p4, p5, :cond_1

    .line 380
    const-string v5, "Recovery of %s %s Obb skipped because desired= %d installed= %d"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v4, v6, v7

    const/4 v7, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 382
    const/4 v5, 0x0

    .line 431
    :goto_1
    return v5

    .line 377
    .end local v4    # "tag":Ljava/lang/String;
    :cond_0
    const-string v4, "Main"

    goto :goto_0

    .line 389
    .restart local v4    # "tag":Ljava/lang/String;
    :cond_1
    invoke-static {p3}, Lcom/google/android/finsky/download/DownloadManagerConstants;->isStatusCompleted(I)Z

    move-result v5

    if-nez v5, :cond_3

    const/16 v5, 0xc6

    if-eq p3, v5, :cond_3

    .line 392
    const-string v5, "Recovery of %s %s Obb into downloading OBB state"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 393
    if-eqz p6, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    .line 394
    .local v3, "obb":Lcom/google/android/finsky/download/obb/Obb;
    :goto_2
    iget-object v5, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-direct {p0, v5, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->generateObbDownload(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Lcom/google/android/finsky/download/obb/Obb;)Lcom/google/android/finsky/download/Download;

    move-result-object v1

    .line 395
    .local v1, "download":Lcom/google/android/finsky/download/Download;
    invoke-interface {v1, p2}, Lcom/google/android/finsky/download/Download;->setContentUri(Landroid/net/Uri;)V

    .line 396
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v5, v1}, Lcom/google/android/finsky/download/DownloadQueue;->addRecoveredDownload(Lcom/google/android/finsky/download/Download;)V

    .line 397
    const/4 v5, 0x1

    goto :goto_1

    .line 393
    .end local v1    # "download":Lcom/google/android/finsky/download/Download;
    .end local v3    # "obb":Lcom/google/android/finsky/download/obb/Obb;
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    goto :goto_2

    .line 400
    :cond_3
    invoke-static {p3}, Lcom/google/android/finsky/download/DownloadManagerConstants;->isStatusSuccess(I)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 403
    if-eqz p6, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    .line 404
    .restart local v3    # "obb":Lcom/google/android/finsky/download/obb/Obb;
    :goto_3
    invoke-interface {v3}, Lcom/google/android/finsky/download/obb/Obb;->syncStateWithStorage()V

    .line 405
    invoke-interface {v3}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_6

    .line 406
    invoke-interface {v3}, Lcom/google/android/finsky/download/obb/Obb;->finalizeTempFile()Z

    move-result v5

    if-nez v5, :cond_6

    .line 407
    const-string v5, "Recovery of %s %s Obb skipped - finalize failed"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 409
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 410
    if-eqz p6, :cond_5

    const/16 v0, 0x390

    .line 412
    .local v0, "code":I
    :goto_4
    sget-object v5, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v5, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 413
    const/4 v5, 0x0

    invoke-direct {p0, v0, v5}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    .line 414
    const/4 v5, 0x0

    goto :goto_1

    .line 403
    .end local v0    # "code":I
    .end local v3    # "obb":Lcom/google/android/finsky/download/obb/Obb;
    :cond_4
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    goto :goto_3

    .line 410
    .restart local v3    # "obb":Lcom/google/android/finsky/download/obb/Obb;
    :cond_5
    const/16 v0, 0x38f

    goto :goto_4

    .line 419
    :cond_6
    const-string v5, "Recovery of %s %s Obb into ready to install state"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 420
    if-eqz p6, :cond_7

    const/16 v2, 0x28

    .line 421
    .local v2, "newState":I
    :goto_5
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v2, v5}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    .line 422
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V

    .line 423
    const/4 v5, 0x1

    goto/16 :goto_1

    .line 420
    .end local v2    # "newState":I
    :cond_7
    const/16 v2, 0x1e

    goto :goto_5

    .line 427
    .end local v3    # "obb":Lcom/google/android/finsky/download/obb/Obb;
    :cond_8
    const-string v5, "Recovery of %s %s Obb into error state, status= %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v4, v6, v7

    const/4 v7, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 428
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 429
    sget-object v5, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v5, p3}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 430
    const/4 v5, 0x0

    invoke-direct {p0, p3, v5}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    .line 431
    const/4 v5, 0x0

    goto/16 :goto_1
.end method

.method private reportExternalCopyFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 2313
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x7c

    const/16 v4, 0x3c3

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 2316
    return-void
.end method

.method private reportGzippedApkFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 2336
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x7b

    const/16 v4, 0x3c2

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 2339
    return-void
.end method

.method private reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 2325
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x6c

    const/16 v4, 0x395

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 2328
    return-void
.end method

.method private requestDeliveryData(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
    .locals 23
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    .line 929
    const-string v2, "requestDeliveryData"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mTitle:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-static {v2, v4, v5, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->checkForEmptyTitle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    .line 931
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-object/from16 v20, v0

    .line 932
    .local v20, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 933
    .local v3, "packageName":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDesiredVersion()I

    move-result v9

    .line 934
    .local v9, "packageVersion":I
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v2

    const/high16 v4, 0x10000

    and-int/2addr v2, v4

    if-eqz v2, :cond_2

    const/16 v21, 0x1

    .line 936
    .local v21, "isEarlyUpdate":Z
    :goto_0
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryToken()Ljava/lang/String;

    move-result-object v12

    .line 939
    .local v12, "deliveryToken":Ljava/lang/String;
    const/16 v17, 0x0

    .line 940
    .local v17, "account":Landroid/accounts/Account;
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAccountForUpdate()Ljava/lang/String;

    move-result-object v18

    .line 941
    .local v18, "accountName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    .line 942
    .local v8, "finskyApp":Lcom/google/android/finsky/FinskyApp;
    if-nez v21, :cond_3

    .line 943
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 944
    move-object/from16 v0, v18

    invoke-static {v0, v8}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v17

    .line 945
    if-nez v17, :cond_0

    .line 947
    const-string v2, "Account %s for update of %s no longer exists."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v3, v4, v5

    invoke-static {v2, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 949
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setAccountForUpdate(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    :cond_0
    if-nez v17, :cond_1

    .line 953
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAccountName()Ljava/lang/String;

    move-result-object v18

    .line 954
    move-object/from16 v0, v18

    invoke-static {v0, v8}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v17

    .line 956
    :cond_1
    if-nez v17, :cond_3

    .line 958
    const-string v2, "Invalid account %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v18, v4, v5

    invoke-static {v2, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 959
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 960
    sget-object v2, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/16 v4, 0x38a

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 962
    const/16 v2, 0x38a

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    .line 1069
    :goto_1
    return-void

    .line 934
    .end local v8    # "finskyApp":Lcom/google/android/finsky/FinskyApp;
    .end local v12    # "deliveryToken":Ljava/lang/String;
    .end local v17    # "account":Landroid/accounts/Account;
    .end local v18    # "accountName":Ljava/lang/String;
    .end local v21    # "isEarlyUpdate":Z
    :cond_2
    const/16 v21, 0x0

    goto :goto_0

    .line 966
    .restart local v8    # "finskyApp":Lcom/google/android/finsky/FinskyApp;
    .restart local v12    # "deliveryToken":Ljava/lang/String;
    .restart local v17    # "account":Landroid/accounts/Account;
    .restart local v18    # "accountName":Ljava/lang/String;
    .restart local v21    # "isEarlyUpdate":Z
    :cond_3
    move-object/from16 v15, v17

    .line 967
    .local v15, "finalAccount":Landroid/accounts/Account;
    move-object/from16 v16, v18

    .line 969
    .local v16, "finalAccountName":Ljava/lang/String;
    const/16 v22, 0x0

    .line 970
    .local v22, "previousVersion":Ljava/lang/Integer;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v2, :cond_4

    sget-object v2, Lcom/google/android/finsky/config/G;->downloadSendBaseVersionCode:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 974
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget v2, v2, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    .line 976
    :cond_4
    move-object/from16 v10, v22

    .line 978
    .local v10, "finalPreviousVersion":Ljava/lang/Integer;
    const/16 v19, 0x0

    .line 979
    .local v19, "certificateHash":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v2, :cond_5

    .line 981
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget-object v2, v2, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->certificateHashes:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v19, v2, v4

    .line 988
    :goto_2
    move-object/from16 v11, v19

    .line 994
    .local v11, "finalCertificateHash":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-interface/range {v2 .. v7}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->reportProgress(Ljava/lang/String;JJ)V

    .line 996
    new-instance v13, Lcom/google/android/finsky/receivers/InstallerTask$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v3}, Lcom/google/android/finsky/receivers/InstallerTask$1;-><init>(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;)V

    .line 1021
    .local v13, "responseListener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;>;"
    new-instance v14, Lcom/google/android/finsky/receivers/InstallerTask$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v3}, Lcom/google/android/finsky/receivers/InstallerTask$2;-><init>(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;)V

    .line 1039
    .local v14, "errorListener":Lcom/android/volley/Response$ErrorListener;
    new-instance v4, Lcom/google/android/finsky/receivers/InstallerTask$3;

    move-object/from16 v5, p0

    move/from16 v6, v21

    move-object v7, v3

    invoke-direct/range {v4 .. v16}, Lcom/google/android/finsky/receivers/InstallerTask$3;-><init>(Lcom/google/android/finsky/receivers/InstallerTask;ZLjava/lang/String;Lcom/google/android/finsky/FinskyApp;ILjava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-static {v8, v4}, Lcom/google/android/finsky/utils/ConsistencyTokenHelper;->get(Landroid/content/Context;Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;)V

    .line 1068
    const/16 v2, 0xa

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDownloadUri()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    goto :goto_1

    .line 986
    .end local v11    # "finalCertificateHash":Ljava/lang/String;
    .end local v13    # "responseListener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;>;"
    .end local v14    # "errorListener":Lcom/android/volley/Response$ErrorListener;
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/receivers/InstallerTask;->getCertHashForOtherUser()Ljava/lang/String;

    move-result-object v19

    goto :goto_2
.end method

.method private requireExternalStorageOrCancel(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z
    .locals 8
    .param p1, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 1306
    invoke-static {}, Lcom/google/android/finsky/download/Storage;->externalStorageAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1319
    :goto_0
    return v4

    .line 1309
    :cond_0
    const-string v0, "Cancel download of %s because no external storage for OBB"

    new-array v1, v7, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1310
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 1311
    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/16 v1, 0x385

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1313
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x70

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    const-string v3, "obb-no-external-storage"

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1316
    iget-boolean v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowErrorNotifications:Z

    if-eqz v0, :cond_1

    .line 1317
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/utils/Notifier;->showExternalStorageMissing(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move v4, v7

    .line 1319
    goto :goto_0
.end method

.method private requireInternalStorageOrCancel(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z
    .locals 10
    .param p1, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 1278
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v0

    iget-wide v8, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    .line 1279
    .local v8, "apkSize":J
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v0, v8, v9, v1, v2}, Lcom/google/android/finsky/installer/InstallPolicies;->isFreeSpaceSufficient(JLjava/io/File;Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1293
    :goto_0
    return v4

    .line 1283
    :cond_0
    const-string v0, "Cancel download of %s because insufficient free space"

    new-array v1, v7, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1284
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 1285
    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/16 v1, 0x38c

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1287
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x70

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    const-string v3, "no-internal-storage"

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1290
    iget-boolean v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowErrorNotifications:Z

    if-eqz v0, :cond_1

    .line 1291
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/utils/Notifier;->showInternalStorageFull(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move v4, v7

    .line 1293
    goto :goto_0
.end method

.method private setInstallerState(ILandroid/net/Uri;)V
    .locals 1
    .param p1, "newState"    # I
    .param p2, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 2074
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2075
    .local v0, "uriString":Ljava/lang/String;
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    .line 2076
    return-void

    .line 2074
    .end local v0    # "uriString":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setInstallerState(ILjava/lang/String;)V
    .locals 2
    .param p1, "newState"    # I
    .param p2, "contentUri"    # Ljava/lang/String;

    .prologue
    .line 2053
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setInstallerState(Ljava/lang/String;ILjava/lang/String;)V

    .line 2068
    return-void
.end method

.method private showDownloadNotification(ILjava/lang/String;)V
    .locals 6
    .param p1, "errorCode"    # I
    .param p2, "serverMessage"    # Ljava/lang/String;

    .prologue
    .line 594
    iget-boolean v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowErrorNotifications:Z

    if-eqz v0, :cond_0

    .line 595
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTitle:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mIsUpdate:Z

    move v3, p1

    move-object v4, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/utils/Notifier;->showDownloadErrorMessage(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    .line 598
    :cond_0
    return-void
.end method

.method private startActivation(Lcom/google/android/finsky/appstate/AppStates$AppState;)Z
    .locals 4
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    const/4 v2, 0x0

    .line 1534
    iget-object v0, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 1535
    .local v0, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v1

    and-int/lit8 v1, v1, 0x20

    if-nez v1, :cond_0

    move v1, v2

    .line 1586
    :goto_0
    return v1

    .line 1539
    :cond_0
    new-instance v1, Lcom/google/android/finsky/receivers/InstallerTask$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask$5;-><init>(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    new-array v3, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/receivers/InstallerTask$5;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1581
    const/16 v3, 0x3c

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v3, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    .line 1584
    sget-object v1, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALLING:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1586
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private startApplyingPatch(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
    .locals 8
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    const/4 v7, 0x0

    .line 2499
    iget-object v2, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 2500
    .local v2, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDownloadUri()Ljava/lang/String;

    move-result-object v1

    .line 2501
    .local v1, "downloadUriString":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2504
    .local v0, "downloadUri":Landroid/net/Uri;
    const/16 v4, 0x37

    invoke-direct {p0, v4, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILandroid/net/Uri;)V

    .line 2506
    const-string v4, "Prepare to patch %s from %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v6, v5, v7

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2508
    new-instance v3, Lcom/google/android/finsky/receivers/InstallerTask$8;

    invoke-direct {v3, p0, v2, v0, v1}, Lcom/google/android/finsky/receivers/InstallerTask$8;-><init>(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Landroid/net/Uri;Ljava/lang/String;)V

    .line 2667
    .local v3, "patchTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/io/File;>;"
    new-array v4, v7, [Ljava/lang/Void;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 2668
    return-void
.end method

.method private startCopyFromExternalStorage(Lcom/google/android/finsky/appstate/AppStates$AppState;)Z
    .locals 10
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2350
    iget-object v4, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 2351
    .local v4, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v3

    .line 2356
    .local v3, "flags":I
    and-int/lit16 v7, v3, 0x1000

    if-nez v7, :cond_1

    .line 2492
    :cond_0
    :goto_0
    return v5

    .line 2359
    :cond_1
    and-int/lit8 v7, v3, 0x4

    if-nez v7, :cond_0

    .line 2362
    and-int/lit16 v7, v3, 0x200

    if-nez v7, :cond_0

    .line 2367
    invoke-virtual {v4}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDownloadUri()Ljava/lang/String;

    move-result-object v2

    .line 2368
    .local v2, "downloadUriString":Ljava/lang/String;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2369
    .local v1, "downloadUri":Landroid/net/Uri;
    const/16 v7, 0x34

    invoke-direct {p0, v7, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILandroid/net/Uri;)V

    .line 2371
    const-string v7, "Prepare to copy %s from %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v9, v8, v5

    aput-object v2, v8, v6

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2374
    new-instance v0, Lcom/google/android/finsky/receivers/InstallerTask$7;

    invoke-direct {v0, p0, v4, v1}, Lcom/google/android/finsky/receivers/InstallerTask$7;-><init>(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Landroid/net/Uri;)V

    .line 2490
    .local v0, "copyTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/io/File;>;"
    new-array v5, v5, [Ljava/lang/Void;

    invoke-static {v0, v5}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    move v5, v6

    .line 2492
    goto :goto_0
.end method

.method private startInstaller(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
    .locals 20
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    .line 1417
    invoke-direct/range {p0 .. p1}, Lcom/google/android/finsky/receivers/InstallerTask;->startActivation(Lcom/google/android/finsky/appstate/AppStates$AppState;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1477
    :goto_0
    return-void

    .line 1421
    :cond_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-object/from16 v16, v0

    .line 1422
    .local v16, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v12

    .line 1426
    .local v12, "deliveryData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v2, :cond_1

    .line 1427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->syncStateWithStorage()V

    .line 1428
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v17

    .line 1429
    .local v17, "obbState":I
    const/4 v2, 0x5

    move/from16 v0, v17

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    move/from16 v0, v17

    if-eq v0, v2, :cond_1

    .line 1430
    const-string v2, "Can\'t find main obb file for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1431
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 1432
    sget-object v2, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/16 v3, 0x38f

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1434
    const/16 v2, 0x38f

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    .line 1438
    .end local v17    # "obbState":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v2, :cond_2

    .line 1439
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->syncStateWithStorage()V

    .line 1440
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v17

    .line 1441
    .restart local v17    # "obbState":I
    const/4 v2, 0x5

    move/from16 v0, v17

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    move/from16 v0, v17

    if-eq v0, v2, :cond_2

    .line 1442
    const-string v2, "Can\'t find patch obb file for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1443
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 1444
    sget-object v2, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/16 v3, 0x390

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1446
    const/16 v2, 0x390

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    .line 1450
    .end local v17    # "obbState":I
    :cond_2
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getTitle()Ljava/lang/String;

    move-result-object v18

    .line 1451
    .local v18, "title":Ljava/lang/String;
    const-string v2, "startInstaller"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->checkForEmptyTitle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    .line 1452
    iget-wide v14, v12, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    .line 1453
    .local v14, "expectedSize":J
    iget-object v13, v12, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->signature:Ljava/lang/String;

    .line 1454
    .local v13, "expectedSignature":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDownloadUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .line 1455
    .local v19, "uri":Landroid/net/Uri;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mIsUpdate:Z

    .line 1460
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v2, Lcom/google/android/finsky/config/G;->preserveForwardLockApiMin:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lt v3, v2, :cond_4

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v2, Lcom/google/android/finsky/config/G;->preserveForwardLockApiMax:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gt v3, v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->isInstalledForwardLocked(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v9, 0x1

    .line 1465
    .local v9, "forwardLock":Z
    :goto_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    const/16 v3, 0x6a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1469
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/receivers/InstallerTask;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->getInstallerListener(Landroid/net/Uri;)Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    move-result-object v11

    move-object/from16 v4, v19

    move-object/from16 v5, v18

    move-wide v6, v14

    move-object v8, v13

    invoke-interface/range {v3 .. v11}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->install(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;ZLjava/lang/String;Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V

    .line 1473
    const/16 v2, 0x3c

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDownloadUri()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    .line 1476
    sget-object v2, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALLING:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    goto/16 :goto_0

    .line 1455
    .end local v9    # "forwardLock":Z
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 1460
    :cond_4
    const/4 v9, 0x0

    goto :goto_2
.end method

.method private startInstallingGzippedApk(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
    .locals 8
    .param p1, "appState"    # Lcom/google/android/finsky/appstate/AppStates$AppState;

    .prologue
    const/4 v7, 0x0

    .line 2671
    iget-object v3, p1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 2672
    .local v3, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDownloadUri()Ljava/lang/String;

    move-result-object v1

    .line 2673
    .local v1, "downloadUriString":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2676
    .local v0, "downloadUri":Landroid/net/Uri;
    const/16 v4, 0x3a

    invoke-direct {p0, v4, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILandroid/net/Uri;)V

    .line 2678
    const-string v4, "Prepare to unzip %s from %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v6, v5, v7

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2680
    new-instance v2, Lcom/google/android/finsky/receivers/InstallerTask$9;

    invoke-direct {v2, p0, v3, v0}, Lcom/google/android/finsky/receivers/InstallerTask$9;-><init>(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Landroid/net/Uri;)V

    .line 2806
    .local v2, "gzipTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/io/File;>;"
    new-array v4, v7, [Ljava/lang/Void;

    invoke-static {v2, v4}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 2807
    return-void
.end method

.method private startNextDownload(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V
    .locals 14
    .param p1, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    const/4 v1, 0x4

    const/4 v5, 0x1

    const/16 v4, 0x391

    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 1192
    const/4 v11, 0x0

    .line 1193
    .local v11, "download":Lcom/google/android/finsky/download/Download;
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v13

    .line 1194
    .local v13, "state":I
    const/16 v0, 0x14

    if-ge v13, v0, :cond_0

    .line 1196
    const/16 v13, 0x14

    .line 1198
    :cond_0
    const/4 v12, -0x1

    .line 1202
    .local v12, "nextState":I
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->requireInternalStorageOrCancel(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1269
    :cond_1
    :goto_0
    return-void

    .line 1207
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->encryptionParams:Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    if-eqz v0, :cond_3

    .line 1208
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 1209
    .local v2, "packageName":Ljava/lang/String;
    const-string v0, "Server sent encryption params for %s"

    new-array v1, v5, [Ljava/lang/Object;

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1210
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 1211
    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v0, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1213
    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    .line 1214
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x70

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    goto :goto_0

    .line 1221
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_3
    sparse-switch v13, :sswitch_data_0

    .line 1254
    :goto_1
    if-ltz v12, :cond_6

    .line 1256
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v0, v11}, Lcom/google/android/finsky/download/DownloadQueue;->add(Lcom/google/android/finsky/download/Download;)V

    .line 1257
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v4

    const/16 v5, 0x64

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v10, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-object v7, v3

    move-object v9, v3

    invoke-virtual/range {v4 .. v10}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1260
    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v12, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    goto :goto_0

    .line 1223
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v0, :cond_4

    .line 1224
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->requireExternalStorageOrCancel(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1227
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v0}, Lcom/google/android/finsky/download/obb/Obb;->syncStateWithStorage()V

    .line 1228
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v0}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 1229
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v0}, Lcom/google/android/finsky/download/obb/Obb;->getTempFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1230
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->generateObbDownload(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Lcom/google/android/finsky/download/obb/Obb;)Lcom/google/android/finsky/download/Download;

    move-result-object v11

    .line 1231
    const/16 v12, 0x14

    .line 1232
    goto :goto_1

    .line 1237
    :cond_4
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v0, :cond_5

    .line 1238
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->requireExternalStorageOrCancel(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1241
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v0}, Lcom/google/android/finsky/download/obb/Obb;->syncStateWithStorage()V

    .line 1242
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v0}, Lcom/google/android/finsky/download/obb/Obb;->getState()I

    move-result v0

    if-ne v0, v1, :cond_5

    .line 1243
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v0}, Lcom/google/android/finsky/download/obb/Obb;->getTempFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1244
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->generateObbDownload(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Lcom/google/android/finsky/download/obb/Obb;)Lcom/google/android/finsky/download/Download;

    move-result-object v11

    .line 1245
    const/16 v12, 0x1e

    .line 1246
    goto :goto_1

    .line 1251
    :cond_5
    :sswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->generateDownload(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Lcom/google/android/finsky/download/Download;

    move-result-object v11

    .line 1252
    const/16 v12, 0x28

    goto :goto_1

    .line 1262
    :cond_6
    const-string v0, "Unexpected download start states for %s: %d %d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v4, v1, v8

    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v5

    const/4 v4, 0x2

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1264
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 1265
    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/16 v1, 0x386

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1267
    const/16 v0, 0x386

    invoke-direct {p0, v0, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 1221
    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_0
        0x1e -> :sswitch_1
        0x28 -> :sswitch_2
    .end sparse-switch
.end method

.method private tryRestartWithInhibitFlag(II)Z
    .locals 8
    .param p1, "testAndClearFlag"    # I
    .param p2, "inhibitFlag"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2884
    const-string v5, "Retry download of %s (inhibit %d)"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v7, v6, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2885
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    .line 2886
    .local v2, "installerDataStore":Lcom/google/android/finsky/appstate/InstallerDataStore;
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v2, v5}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v1

    .line 2887
    .local v1, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v0

    .line 2888
    .local v0, "flags":I
    and-int v5, v0, p1

    if-eqz v5, :cond_1

    .line 2889
    .local v3, "wasFlagSet":Z
    :goto_0
    if-eqz v3, :cond_0

    .line 2891
    or-int/2addr v0, p2

    .line 2892
    xor-int/lit8 v4, p1, -0x1

    and-int/2addr v0, v4

    .line 2893
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v2, v4, v0}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    .line 2895
    const/16 v5, 0x28

    const/4 v4, 0x0

    check-cast v4, Ljava/lang/String;

    invoke-direct {p0, v5, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    .line 2896
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V

    .line 2898
    :cond_0
    return v3

    .end local v3    # "wasFlagSet":Z
    :cond_1
    move v3, v4

    .line 2888
    goto :goto_0
.end method

.method private volleyErrorToInstallerError(Lcom/android/volley/VolleyError;)I
    .locals 1
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 1091
    instance-of v0, p1, Lcom/android/volley/AuthFailureError;

    if-eqz v0, :cond_0

    .line 1092
    const/16 v0, 0x398

    .line 1115
    :goto_0
    return v0

    .line 1094
    :cond_0
    instance-of v0, p1, Lcom/google/android/volley/DisplayMessageError;

    if-eqz v0, :cond_1

    .line 1095
    const/16 v0, 0x399

    goto :goto_0

    .line 1097
    :cond_1
    instance-of v0, p1, Lcom/google/android/finsky/api/DfeServerError;

    if-eqz v0, :cond_2

    .line 1098
    const/16 v0, 0x39a

    goto :goto_0

    .line 1100
    :cond_2
    instance-of v0, p1, Lcom/android/volley/NetworkError;

    if-eqz v0, :cond_3

    .line 1101
    const/16 v0, 0x39b

    goto :goto_0

    .line 1103
    :cond_3
    instance-of v0, p1, Lcom/android/volley/NoConnectionError;

    if-eqz v0, :cond_4

    .line 1104
    const/16 v0, 0x39c

    goto :goto_0

    .line 1106
    :cond_4
    instance-of v0, p1, Lcom/android/volley/ParseError;

    if-eqz v0, :cond_5

    .line 1107
    const/16 v0, 0x39d

    goto :goto_0

    .line 1109
    :cond_5
    instance-of v0, p1, Lcom/android/volley/ServerError;

    if-eqz v0, :cond_6

    .line 1110
    const/16 v0, 0x39e

    goto :goto_0

    .line 1112
    :cond_6
    instance-of v0, p1, Lcom/android/volley/TimeoutError;

    if-eqz v0, :cond_7

    .line 1113
    const/16 v0, 0x39f

    goto :goto_0

    .line 1115
    :cond_7
    const/16 v0, 0x3a0

    goto :goto_0
.end method


# virtual methods
.method public addAppShortcut()V
    .locals 17

    .prologue
    .line 1697
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    .line 1699
    .local v3, "context":Landroid/content/Context;
    new-instance v1, Lcom/google/android/finsky/activities/AppActionAnalyzer;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v14

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v15

    invoke-direct {v1, v13, v14, v15}, Lcom/google/android/finsky/activities/AppActionAnalyzer;-><init>(Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V

    .line 1702
    .local v1, "actionAnalyzer":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    iget-boolean v13, v1, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isLaunchable:Z

    if-nez v13, :cond_0

    .line 1703
    const-string v13, "Skip shortcut for non-launchable %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1741
    :goto_0
    return-void

    .line 1707
    :cond_0
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 1711
    .local v10, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    const/4 v14, 0x0

    invoke-virtual {v10, v13, v14}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 1715
    .local v2, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v10, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v11

    .line 1716
    .local v11, "res":Landroid/content/res/Resources;
    iget v13, v2, Landroid/content/pm/ApplicationInfo;->icon:I

    invoke-virtual {v11, v13}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v6

    .line 1717
    .local v6, "iconResourceName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v13}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    .line 1718
    .local v9, "launchIntent":Landroid/content/Intent;
    invoke-virtual {v9}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v10, v13, v14}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v13

    iget v8, v13, Landroid/content/pm/ActivityInfo;->labelRes:I

    .line 1719
    .local v8, "labelResourceId":I
    if-eqz v8, :cond_1

    invoke-virtual {v11, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1723
    .local v12, "shortcutName":Ljava/lang/CharSequence;
    :goto_1
    new-instance v7, Landroid/content/Intent;

    const-string v13, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v7, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1724
    .local v7, "installShortcut":Landroid/content/Intent;
    const-string v13, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v7, v13, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 1725
    const-string v13, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v7, v13, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1726
    new-instance v5, Landroid/content/Intent$ShortcutIconResource;

    invoke-direct {v5}, Landroid/content/Intent$ShortcutIconResource;-><init>()V

    .line 1727
    .local v5, "icon":Landroid/content/Intent$ShortcutIconResource;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    iput-object v13, v5, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    .line 1728
    iput-object v6, v5, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    .line 1729
    const-string v13, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v7, v13, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1730
    const-string v13, "duplicate"

    const/4 v14, 0x0

    invoke-virtual {v7, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1732
    invoke-virtual {v3, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1733
    const-string v13, "Requested shortcut for %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1734
    .end local v2    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v5    # "icon":Landroid/content/Intent$ShortcutIconResource;
    .end local v6    # "iconResourceName":Ljava/lang/String;
    .end local v7    # "installShortcut":Landroid/content/Intent;
    .end local v8    # "labelResourceId":I
    .end local v9    # "launchIntent":Landroid/content/Intent;
    .end local v11    # "res":Landroid/content/res/Resources;
    .end local v12    # "shortcutName":Ljava/lang/CharSequence;
    :catch_0
    move-exception v4

    .line 1736
    .local v4, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v13, "Unable to load resources for %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1719
    .end local v4    # "e":Landroid/content/res/Resources$NotFoundException;
    .restart local v2    # "ai":Landroid/content/pm/ApplicationInfo;
    .restart local v6    # "iconResourceName":Ljava/lang/String;
    .restart local v8    # "labelResourceId":I
    .restart local v9    # "launchIntent":Landroid/content/Intent;
    .restart local v11    # "res":Landroid/content/res/Resources;
    :cond_1
    :try_start_1
    invoke-virtual {v10, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v12

    goto :goto_1

    .line 1737
    .end local v2    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v6    # "iconResourceName":Ljava/lang/String;
    .end local v8    # "labelResourceId":I
    .end local v9    # "launchIntent":Landroid/content/Intent;
    .end local v11    # "res":Landroid/content/res/Resources;
    :catch_1
    move-exception v4

    .line 1739
    .local v4, "e":Ljava/lang/Exception;
    const-string v13, "Unable to add shortcut for %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v4, v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method canDownloadGzippedApk(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;J)Z
    .locals 12
    .param p1, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .param p2, "freeOnData"    # J

    .prologue
    .line 2256
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFlags()I

    move-result v1

    .line 2257
    .local v1, "flags":I
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 2258
    .local v4, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v0

    .line 2262
    .local v0, "deliveryData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    and-int/lit16 v5, v1, 0x200

    if-eqz v5, :cond_1

    .line 2263
    iget-object v5, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 2264
    const/4 v5, 0x1

    .line 2303
    :goto_0
    return v5

    .line 2266
    :cond_0
    const-string v5, "Missing gzipped apk for %s while apk_is_gzipped set in %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    const/4 v7, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2268
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    and-int/lit16 v6, v1, -0x201

    invoke-interface {v5, v4, v6}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    .line 2275
    :cond_1
    and-int/lit16 v5, v1, 0x400

    if-eqz v5, :cond_2

    .line 2276
    const/4 v5, 0x0

    goto :goto_0

    .line 2284
    :cond_2
    iget-object v5, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2285
    const/4 v5, 0x0

    goto :goto_0

    .line 2295
    :cond_3
    const-wide/16 v6, 0x6e

    iget-wide v8, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    iget-wide v10, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadSize:J

    add-long/2addr v8, v10

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x64

    div-long v2, v6, v8

    .line 2296
    .local v2, "needFree":J
    cmp-long v5, p2, v2

    if-gez v5, :cond_4

    .line 2297
    const-string v5, "free-space"

    invoke-direct {p0, v4, v5}, Lcom/google/android/finsky/receivers/InstallerTask;->reportGzippedApkFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 2298
    const-string v5, "Cannot use gzipped apk %s, need %d, free %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2300
    const/4 v5, 0x0

    goto :goto_0

    .line 2303
    :cond_4
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public cancel(Z)V
    .locals 5
    .param p1, "sendNotify"    # Z

    .prologue
    const/4 v4, 0x0

    .line 607
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 611
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v1

    const/16 v2, 0x3c

    if-lt v1, v2, :cond_1

    .line 613
    const-string v1, "Cannot cancel installing %s - too late"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 623
    :cond_0
    :goto_0
    return-void

    .line 617
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->cancelCleanup(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    .line 620
    if-eqz p1, :cond_0

    .line 621
    sget-object v1, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_CANCELLED:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v1, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    goto :goto_0
.end method

.method public getAppData()Lcom/google/android/finsky/analytics/PlayStore$AppData;
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    return-object v0
.end method

.method getPatch(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Ljava/io/InputStream;
    .locals 9
    .param p1, "appContext"    # Landroid/content/Context;
    .param p2, "downloadUri"    # Landroid/net/Uri;
    .param p3, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2812
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, p2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 2813
    .local v2, "patchStream":Ljava/io/InputStream;
    invoke-static {p3}, Lcom/google/android/finsky/receivers/InstallerTask;->isGzippedPatch(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2814
    new-instance v3, Ljava/util/zip/GZIPInputStream;

    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-direct {v5, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v5}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .end local v2    # "patchStream":Ljava/io/InputStream;
    .local v3, "patchStream":Ljava/io/InputStream;
    move-object v2, v3

    .line 2824
    .end local v3    # "patchStream":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return-object v2

    .line 2817
    :catch_0
    move-exception v1

    .line 2818
    .local v1, "e1":Ljava/io/FileNotFoundException;
    const-string v5, "FileNotFoundException %s %s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p2, v6, v7

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2819
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    const-string v6, "patch-FileNotFoundException"

    invoke-direct {p0, v5, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    .line 2820
    goto :goto_0

    .line 2821
    .end local v1    # "e1":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 2822
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "IOException %s %s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p2, v6, v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2823
    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    const-string v6, "patch-IOException"

    invoke-direct {p0, v5, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->reportPatchFailure(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    .line 2824
    goto :goto_0
.end method

.method public getProgress()Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;
    .locals 7

    .prologue
    .line 696
    sget-object v0, Lcom/google/android/finsky/receivers/InstallerTask$10;->$SwitchMap$com$google$android$finsky$receivers$Installer$InstallerState:[I

    invoke-virtual {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->getState()Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/receivers/Installer$InstallerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 706
    iget-wide v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mApkCompleted:J

    iget-wide v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMainCompleted:J

    add-long/2addr v0, v4

    iget-wide v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatchCompleted:J

    add-long v2, v0, v4

    .line 708
    .local v2, "bytesCompleted":J
    new-instance v0, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    sget-object v1, Lcom/google/android/finsky/receivers/Installer$InstallerState;->DOWNLOADING:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    iget-wide v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTotalSize:J

    iget v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadStatus:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;-><init>(Lcom/google/android/finsky/receivers/Installer$InstallerState;JJI)V

    .end local v2    # "bytesCompleted":J
    :goto_0
    return-object v0

    .line 698
    :pswitch_0
    sget-object v0, Lcom/google/android/finsky/receivers/InstallerTask;->PROGRESS_NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    goto :goto_0

    .line 700
    :pswitch_1
    sget-object v0, Lcom/google/android/finsky/receivers/InstallerTask;->PROGRESS_INSTALLING:Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    goto :goto_0

    .line 696
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getState()Lcom/google/android/finsky/receivers/Installer$InstallerState;
    .locals 3

    .prologue
    .line 671
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 672
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v1, :cond_0

    .line 673
    iget-object v1, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 684
    sget-object v1, Lcom/google/android/finsky/receivers/Installer$InstallerState;->DOWNLOADING:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    .line 687
    :goto_0
    return-object v1

    .line 677
    :sswitch_0
    sget-object v1, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    goto :goto_0

    .line 682
    :sswitch_1
    sget-object v1, Lcom/google/android/finsky/receivers/Installer$InstallerState;->INSTALLING:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    goto :goto_0

    .line 687
    :cond_0
    sget-object v1, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    goto :goto_0

    .line 673
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x34 -> :sswitch_1
        0x37 -> :sswitch_1
        0x3a -> :sswitch_1
        0x3c -> :sswitch_1
        0x46 -> :sswitch_0
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method public onComplete(Lcom/google/android/finsky/download/Download;)V
    .locals 11
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1929
    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v9, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 1930
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    iget-object v2, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 1931
    .local v2, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    const/4 v5, -0x1

    .line 1932
    .local v5, "newState":I
    const/16 v1, 0x388

    .line 1933
    .local v1, "errorCode":I
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->isObb()Z

    move-result v3

    .line 1934
    .local v3, "internalIsObb":Z
    if-eqz v3, :cond_1

    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getObb()Lcom/google/android/finsky/download/obb/Obb;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/finsky/download/obb/Obb;->isPatch()Z

    move-result v8

    if-nez v8, :cond_1

    move v4, v6

    .line 1935
    .local v4, "internalIsObbMain":Z
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 1962
    :cond_0
    :goto_1
    if-ltz v5, :cond_4

    .line 1964
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILandroid/net/Uri;)V

    .line 1965
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V

    .line 1973
    :goto_2
    return-void

    .end local v4    # "internalIsObbMain":Z
    :cond_1
    move v4, v7

    .line 1934
    goto :goto_0

    .line 1937
    .restart local v4    # "internalIsObbMain":Z
    :sswitch_0
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    .line 1938
    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMain:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v8}, Lcom/google/android/finsky/download/obb/Obb;->finalizeTempFile()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1939
    const/16 v5, 0x1e

    goto :goto_1

    .line 1941
    :cond_2
    const-string v8, "Can\'t finalize main obb file for %s"

    new-array v9, v6, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v10, v9, v7

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1942
    const/16 v1, 0x38f

    goto :goto_1

    .line 1947
    :sswitch_1
    if-eqz v3, :cond_0

    if-nez v4, :cond_0

    .line 1948
    iget-object v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatch:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v8}, Lcom/google/android/finsky/download/obb/Obb;->finalizeTempFile()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1949
    const/16 v5, 0x28

    goto :goto_1

    .line 1951
    :cond_3
    const-string v8, "Can\'t finalize patch obb file for %s"

    new-array v9, v6, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v10, v9, v7

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1952
    const/16 v1, 0x390

    goto :goto_1

    .line 1957
    :sswitch_2
    if-nez v3, :cond_0

    .line 1958
    const/16 v5, 0x32

    goto :goto_1

    .line 1967
    :cond_4
    const-string v8, "Unexpected download completion states for %s: %d %d %b %b"

    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v10, v9, v7

    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v6

    const/4 v6, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v6

    const/4 v6, 0x3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v6

    const/4 v6, 0x4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1969
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 1970
    sget-object v6, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v6, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1971
    const/4 v6, 0x0

    invoke-direct {p0, v1, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    goto :goto_2

    .line 1935
    nop

    :sswitch_data_0
    .sparse-switch
        0x19 -> :sswitch_0
        0x23 -> :sswitch_1
        0x2d -> :sswitch_2
    .end sparse-switch
.end method

.method public onError(Lcom/google/android/finsky/download/Download;I)V
    .locals 4
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;
    .param p2, "httpStatus"    # I

    .prologue
    const/4 v1, 0x0

    .line 1981
    const/16 v2, 0x1a4

    if-eq p2, v2, :cond_0

    const/16 v2, 0x1f4

    if-lt p2, v2, :cond_2

    const/16 v2, 0x257

    if-gt p2, v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 1982
    .local v0, "restartableError":Z
    :goto_0
    if-eqz v0, :cond_4

    .line 1983
    const/4 v2, 0x4

    const/16 v3, 0x8

    invoke-direct {p0, v2, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->tryRestartWithInhibitFlag(II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2012
    :cond_1
    :goto_1
    return-void

    .end local v0    # "restartableError":Z
    :cond_2
    move v0, v1

    .line 1981
    goto :goto_0

    .line 1987
    .restart local v0    # "restartableError":Z
    :cond_3
    const/16 v2, 0x200

    const/16 v3, 0x400

    invoke-direct {p0, v2, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->tryRestartWithInhibitFlag(II)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1993
    :cond_4
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 1997
    const/16 v1, 0xc6

    if-ne p2, v1, :cond_7

    .line 1998
    iget-boolean v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mShowErrorNotifications:Z

    if-eqz v1, :cond_5

    .line 2000
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->isObb()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2001
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTitle:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/finsky/utils/Notifier;->showExternalStorageFull(Ljava/lang/String;Ljava/lang/String;)V

    .line 2011
    :cond_5
    :goto_2
    sget-object v1, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v1, p2}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    goto :goto_1

    .line 2003
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTitle:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/finsky/utils/Notifier;->showInternalStorageFull(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2008
    :cond_7
    const/4 v1, 0x0

    invoke-direct {p0, p2, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    goto :goto_2
.end method

.method public onProgress(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/DownloadProgress;)V
    .locals 7
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;
    .param p2, "progress"    # Lcom/google/android/finsky/download/DownloadProgress;

    .prologue
    const-wide/16 v4, 0x0

    .line 2016
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->isObb()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2017
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getObb()Lcom/google/android/finsky/download/obb/Obb;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/finsky/download/obb/Obb;->isPatch()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2018
    iget-wide v0, p2, Lcom/google/android/finsky/download/DownloadProgress;->bytesCompleted:J

    iput-wide v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatchCompleted:J

    .line 2032
    :cond_0
    :goto_0
    iget v0, p2, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    iput v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadStatus:I

    .line 2033
    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOADING:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 2036
    iget-wide v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mApkCompleted:J

    iget-wide v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMainCompleted:J

    add-long/2addr v0, v4

    iget-wide v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbPatchCompleted:J

    add-long v2, v0, v4

    .line 2037
    .local v2, "bytesCompleted":J
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mTotalSize:J

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->reportProgress(Ljava/lang/String;JJ)V

    .line 2038
    return-void

    .line 2020
    .end local v2    # "bytesCompleted":J
    :cond_1
    iget-wide v0, p2, Lcom/google/android/finsky/download/DownloadProgress;->bytesCompleted:J

    iput-wide v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mObbMainCompleted:J

    goto :goto_0

    .line 2023
    :cond_2
    iget-wide v0, p2, Lcom/google/android/finsky/download/DownloadProgress;->bytesCompleted:J

    iput-wide v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mApkCompleted:J

    .line 2025
    iget-wide v0, p2, Lcom/google/android/finsky/download/DownloadProgress;->bytesCompleted:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 2026
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v6

    .line 2027
    .local v6, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v6}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFirstDownloadMs()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 2028
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v0, v1, v4, v5}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFirstDownloadMs(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public onStart(Lcom/google/android/finsky/download/Download;)V
    .locals 9
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    const/16 v8, 0x387

    const/4 v7, 0x0

    .line 1888
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 1889
    .local v2, "packageName":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v3, v2}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v3

    iget-object v0, v3, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 1893
    .local v0, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    const/4 v1, -0x1

    .line 1894
    .local v1, "newState":I
    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 1908
    :goto_0
    if-ltz v1, :cond_0

    .line 1910
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILandroid/net/Uri;)V

    .line 1911
    sget-object v3, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOADING:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v3, v7}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1920
    :goto_1
    return-void

    .line 1897
    :sswitch_0
    const/16 v1, 0x19

    .line 1898
    goto :goto_0

    .line 1901
    :sswitch_1
    const/16 v1, 0x23

    .line 1902
    goto :goto_0

    .line 1905
    :sswitch_2
    const/16 v1, 0x2d

    goto :goto_0

    .line 1913
    :cond_0
    const-string v3, "Unexpected download start states for %s: %d %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v7

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1915
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 1916
    sget-object v3, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v3, v8}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1918
    const/4 v3, 0x0

    invoke-direct {p0, v8, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    goto :goto_1

    .line 1894
    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1e -> :sswitch_1
        0x23 -> :sswitch_1
        0x28 -> :sswitch_2
        0x2d -> :sswitch_2
    .end sparse-switch
.end method

.method public recover(Landroid/net/Uri;I)Z
    .locals 10
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "status"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 302
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v1

    .line 303
    .local v1, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    iget-object v7, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 304
    .local v7, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v0

    if-nez v0, :cond_1

    .line 306
    :cond_0
    const-string v0, "Recovery of %s skipped because incomplete installerdata"

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 364
    :goto_0
    return v6

    .line 311
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->populateFields(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    .line 314
    iget-object v0, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-direct {p0, v0, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->processDeliveryData(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Z)V

    .line 317
    iget-object v0, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget v5, v0, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 319
    .local v5, "installed":I
    :goto_1
    invoke-virtual {v7}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDesiredVersion()I

    move-result v4

    .line 321
    .local v4, "desired":I
    invoke-virtual {v7}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v8

    .line 322
    .local v8, "state":I
    iput v8, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mRecoveredIntoState:I

    .line 323
    sparse-switch v8, :sswitch_data_0

    .line 363
    const-string v0, "Recovery of %s skipped because state= %d"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 317
    .end local v4    # "desired":I
    .end local v5    # "installed":I
    .end local v8    # "state":I
    :cond_2
    const/4 v5, -0x1

    goto :goto_1

    .restart local v4    # "desired":I
    .restart local v5    # "installed":I
    .restart local v8    # "state":I
    :sswitch_0
    move-object v0, p0

    move-object v2, p1

    move v3, p2

    .line 325
    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/receivers/InstallerTask;->recoverObb(Lcom/google/android/finsky/appstate/AppStates$AppState;Landroid/net/Uri;IIIZ)Z

    move-result v6

    goto :goto_0

    :sswitch_1
    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v6, v9

    .line 327
    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/receivers/InstallerTask;->recoverObb(Lcom/google/android/finsky/appstate/AppStates$AppState;Landroid/net/Uri;IIIZ)Z

    move-result v6

    goto :goto_0

    :sswitch_2
    move-object v0, p0

    move-object v2, p1

    move v3, p2

    .line 329
    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/receivers/InstallerTask;->recoverApk(Lcom/google/android/finsky/appstate/AppStates$AppState;Landroid/net/Uri;III)Z

    move-result v6

    goto :goto_0

    .line 338
    :sswitch_3
    const-string v0, "Recovery of %s skipped because state= %d"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 339
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    goto :goto_0

    .line 349
    :sswitch_4
    const-string v0, "Recovery of %s skipped because state= %d"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    goto :goto_0

    .line 353
    :sswitch_5
    const-string v0, "Recovery of %s skipped because state= %d"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 354
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    goto/16 :goto_0

    :sswitch_6
    move-object v0, p0

    move-object v2, p1

    move v3, p2

    .line 358
    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/receivers/InstallerTask;->recoverInstalling(Lcom/google/android/finsky/appstate/AppStates$AppState;Landroid/net/Uri;III)Z

    move-result v6

    goto/16 :goto_0

    .line 323
    :sswitch_data_0
    .sparse-switch
        0x19 -> :sswitch_0
        0x23 -> :sswitch_1
        0x2d -> :sswitch_2
        0x32 -> :sswitch_6
        0x34 -> :sswitch_3
        0x37 -> :sswitch_4
        0x3a -> :sswitch_5
        0x3c -> :sswitch_6
    .end sparse-switch
.end method

.method releaseInstalledUri(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1596
    const-string v1, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1597
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1598
    .local v0, "installFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1602
    .end local v0    # "installFile":Ljava/io/File;
    :goto_0
    return-void

    .line 1600
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v1, p1}, Lcom/google/android/finsky/download/DownloadQueue;->release(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public start()V
    .locals 11

    .prologue
    const/16 v10, 0x38b

    const/16 v9, 0x389

    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 195
    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 196
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v0, :cond_0

    iget-object v4, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-nez v4, :cond_1

    .line 197
    :cond_0
    const-string v4, "Unexpected missing installer data for %s"

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 277
    :goto_0
    return-void

    .line 201
    :cond_1
    iget-object v1, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 202
    .local v1, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getInstallerState()I

    move-result v3

    .line 205
    .local v3, "state":I
    invoke-direct {p0, v0}, Lcom/google/android/finsky/receivers/InstallerTask;->populateFields(Lcom/google/android/finsky/appstate/AppStates$AppState;)V

    .line 209
    if-lez v3, :cond_2

    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 210
    invoke-direct {p0, v1, v7}, Lcom/google/android/finsky/receivers/InstallerTask;->processDeliveryData(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Z)V

    .line 213
    :cond_2
    const/4 v2, -0x1

    .line 214
    .local v2, "newState":I
    iput v3, p0, Lcom/google/android/finsky/receivers/InstallerTask;->mRecoveredIntoState:I

    .line 215
    sparse-switch v3, :sswitch_data_0

    .line 269
    const-string v4, "Unknown state %d for %s"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    goto :goto_0

    .line 219
    :sswitch_0
    const/4 v2, 0x0

    .line 273
    :goto_1
    :sswitch_1
    const/4 v4, -0x1

    if-eq v2, v4, :cond_3

    if-eq v2, v3, :cond_3

    .line 274
    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDownloadUri()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V

    .line 276
    :cond_3
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V

    goto :goto_0

    .line 224
    :sswitch_2
    const/16 v2, 0xa

    .line 225
    goto :goto_1

    .line 229
    :sswitch_3
    const-string v4, "Cannot restart %s from downloading state %d"

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 230
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 231
    sget-object v4, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v4, v9}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 233
    const/4 v4, 0x0

    invoke-direct {p0, v9, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    goto :goto_0

    .line 237
    :sswitch_4
    const/16 v2, 0x3c

    .line 238
    goto :goto_1

    .line 255
    :sswitch_5
    const-string v4, "Restarting %d for %s"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 257
    sget-object v4, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    invoke-direct {p0, v4, v10}, Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 259
    const/4 v4, 0x0

    invoke-direct {p0, v10, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 264
    :sswitch_6
    const/16 v2, 0x46

    .line 265
    goto :goto_1

    .line 215
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0xa -> :sswitch_0
        0x14 -> :sswitch_2
        0x19 -> :sswitch_3
        0x1e -> :sswitch_2
        0x23 -> :sswitch_3
        0x28 -> :sswitch_2
        0x2d -> :sswitch_3
        0x32 -> :sswitch_4
        0x34 -> :sswitch_5
        0x37 -> :sswitch_5
        0x3a -> :sswitch_5
        0x3c -> :sswitch_6
        0x46 -> :sswitch_6
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2080
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    return-object v0
.end method
