.class public final Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckLicenseResponseProto"
.end annotation


# instance fields
.field public hasResponseCode:Z

.field public hasSignature:Z

.field public hasSignedData:Z

.field public responseCode:I

.field public signature:Ljava/lang/String;

.field public signedData:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2270
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2271
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    .line 2272
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2275
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->responseCode:I

    .line 2276
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasResponseCode:Z

    .line 2277
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signedData:Ljava/lang/String;

    .line 2278
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasSignedData:Z

    .line 2279
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signature:Ljava/lang/String;

    .line 2280
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasSignature:Z

    .line 2281
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->cachedSize:I

    .line 2282
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2302
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2303
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasResponseCode:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->responseCode:I

    if-eqz v1, :cond_1

    .line 2304
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->responseCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2307
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasSignedData:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signedData:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2308
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signedData:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2311
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasSignature:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signature:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2312
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signature:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2315
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2323
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2324
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2328
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2329
    :sswitch_0
    return-object p0

    .line 2334
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->responseCode:I

    .line 2335
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasResponseCode:Z

    goto :goto_0

    .line 2339
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signedData:Ljava/lang/String;

    .line 2340
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasSignedData:Z

    goto :goto_0

    .line 2344
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signature:Ljava/lang/String;

    .line 2345
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasSignature:Z

    goto :goto_0

    .line 2324
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2241
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2288
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasResponseCode:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->responseCode:I

    if-eqz v0, :cond_1

    .line 2289
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->responseCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2291
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasSignedData:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signedData:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2292
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signedData:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2294
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->hasSignature:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signature:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2295
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;->signature:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2297
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2298
    return-void
.end method
