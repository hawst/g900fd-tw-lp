.class public Lcom/google/android/finsky/utils/PurchaseInitiator;
.super Ljava/lang/Object;
.source "PurchaseInitiator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;
    }
.end annotation


# direct methods
.method static synthetic access$000(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/accounts/Account;
    .param p1, "x1"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;
    .param p4, "x4"    # Z
    .param p5, "x5"    # Ljava/lang/String;
    .param p6, "x6"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-static/range {p0 .. p6}, Lcom/google/android/finsky/utils/PurchaseInitiator;->processPurchaseStatusResponse(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static createFreePurchaseErrorListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;IZ)Lcom/android/volley/Response$ErrorListener;
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "offerType"    # I
    .param p3, "showErrors"    # Z

    .prologue
    .line 210
    new-instance v0, Lcom/google/android/finsky/utils/PurchaseInitiator$3;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/finsky/utils/PurchaseInitiator$3;-><init>(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;IZ)V

    return-object v0
.end method

.method private static createFreePurchaseListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;ZZ)Lcom/android/volley/Response$Listener;
    .locals 8
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "offerType"    # I
    .param p3, "continueUrl"    # Ljava/lang/String;
    .param p4, "successListener"    # Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;
    .param p5, "initiateAppDownload"    # Z
    .param p6, "showErrors"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/android/finsky/api/model/Document;",
            "I",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;",
            "ZZ)",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Lcom/google/android/finsky/protos/Buy$BuyResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p5

    move-object v6, p4

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/utils/PurchaseInitiator$1;-><init>(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;ZLcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;Z)V

    return-object v0
.end method

.method public static initiateDownload(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)V
    .locals 8
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v5, 0x0

    .line 188
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    if-nez v0, :cond_0

    .line 189
    const-string v0, "Document does not contain AppDetails, cannot download: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v2

    iget v2, v2, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    iget-object v3, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    const-string v6, "single_install"

    const/4 v7, 0x2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/finsky/receivers/Installer;->requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 201
    return-void
.end method

.method public static makeFreePurchase(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;ZZ)V
    .locals 14
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "offerType"    # I
    .param p3, "continueUrl"    # Ljava/lang/String;
    .param p4, "successListener"    # Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;
    .param p5, "initiateAppDownload"    # Z
    .param p6, "showErrors"    # Z

    .prologue
    .line 63
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v3

    .line 65
    .local v3, "eventLogger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    const/16 v4, 0x12c

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const-wide/16 v12, -0x1

    move/from16 v6, p2

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPurchaseBackgroundEvent(ILjava/lang/String;ILjava/lang/String;I[BJJ)V

    .line 68
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    iget-object v4, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v4

    const/4 v7, 0x0

    invoke-static/range {p0 .. p6}, Lcom/google/android/finsky/utils/PurchaseInitiator;->createFreePurchaseListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;ZZ)Lcom/android/volley/Response$Listener;

    move-result-object v8

    move/from16 v0, p2

    move/from16 v1, p6

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/finsky/utils/PurchaseInitiator;->createFreePurchaseErrorListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;IZ)Lcom/android/volley/Response$ErrorListener;

    move-result-object v9

    move-object v5, p1

    move/from16 v6, p2

    invoke-interface/range {v4 .. v9}, Lcom/google/android/finsky/api/DfeApi;->makePurchase(Lcom/google/android/finsky/api/model/Document;ILjava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 74
    return-void
.end method

.method private static processPurchaseStatusResponse(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "continueUrl"    # Ljava/lang/String;
    .param p3, "response"    # Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;
    .param p4, "initiateAppDownload"    # Z
    .param p5, "debugEventName"    # Ljava/lang/String;
    .param p6, "deliveryToken"    # Ljava/lang/String;

    .prologue
    .line 155
    new-instance v0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;

    move-object v1, p3

    move v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v5, p6

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/utils/PurchaseInitiator$2;-><init>(Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;ZLcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;)V

    .line 178
    .local v0, "postLibraryUpdateCallback":Ljava/lang/Runnable;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v7

    .line 179
    .local v7, "replicators":Lcom/google/android/finsky/library/LibraryReplicators;
    iget-object v1, p3, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v1, :cond_0

    .line 180
    iget-object v1, p3, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-interface {v7, p0, v1, p5}, Lcom/google/android/finsky/library/LibraryReplicators;->applyLibraryUpdate(Landroid/accounts/Account;Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;)V

    .line 181
    invoke-interface {v7, v0}, Lcom/google/android/finsky/library/LibraryReplicators;->flushLibraryUpdates(Ljava/lang/Runnable;)V

    .line 185
    :goto_0
    return-void

    .line 183
    :cond_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
