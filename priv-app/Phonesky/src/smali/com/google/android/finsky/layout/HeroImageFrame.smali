.class public Lcom/google/android/finsky/layout/HeroImageFrame;
.super Landroid/view/ViewGroup;
.source "HeroImageFrame.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/HeroImageFrame;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 7
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroImageFrame;->getPaddingTop()I

    move-result v3

    .line 46
    .local v3, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroImageFrame;->getChildCount()I

    move-result v1

    .line 47
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 48
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/HeroImageFrame;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 50
    .local v0, "child":Landroid/view/View;
    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 47
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 53
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 27
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 28
    .local v5, "width":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 32
    .local v3, "height":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HeroImageFrame;->getChildCount()I

    move-result v0

    .line 33
    .local v0, "childCount":I
    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 34
    .local v2, "childWidthSpec":I
    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 35
    .local v1, "childHeightSpec":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v0, :cond_0

    .line 36
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/HeroImageFrame;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v2, v1}, Landroid/view/View;->measure(II)V

    .line 35
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {p0, v5, v3}, Lcom/google/android/finsky/layout/HeroImageFrame;->setMeasuredDimension(II)V

    .line 40
    return-void
.end method
