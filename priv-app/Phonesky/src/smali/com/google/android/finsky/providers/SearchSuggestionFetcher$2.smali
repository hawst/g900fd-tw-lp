.class Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;
.super Ljava/lang/Object;
.source "SearchSuggestionFetcher.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->makeRequest(Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/providers/SearchSuggestionFetcher;

.field final synthetic val$refCountedListener:Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/providers/SearchSuggestionFetcher;Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;->this$0:Lcom/google/android/finsky/providers/SearchSuggestionFetcher;

    iput-object p2, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;->val$refCountedListener:Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;)V
    .locals 14
    .param p1, "response"    # Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    .prologue
    const/4 v1, 0x3

    .line 180
    iget-object v0, p1, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    array-length v2, v0

    .line 181
    .local v2, "responseCount":I
    iget-object v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;->this$0:Lcom/google/android/finsky/providers/SearchSuggestionFetcher;

    iget-object v0, v0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mSuggestionHandler:Lcom/google/android/finsky/providers/SuggestionHandler;

    iget-object v3, p1, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->serverLogsCookie:[B

    iget-object v7, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;->this$0:Lcom/google/android/finsky/providers/SearchSuggestionFetcher;

    iget-wide v4, v7, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mStartTimeMs:J

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/providers/SuggestionHandler;->sendSuggestionsReceivedLog(II[BJ)V

    .line 185
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_2

    .line 186
    iget-object v0, p1, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    aget-object v13, v0, v4

    .line 187
    .local v13, "suggestion":Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    iget v0, v13, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->type:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;->this$0:Lcom/google/android/finsky/providers/SearchSuggestionFetcher;

    iget-object v0, v0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mSuggestionHandler:Lcom/google/android/finsky/providers/SuggestionHandler;

    const v3, 0x108004f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v7, v13, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->suggestedQuery:Ljava/lang/String;

    iget-object v8, v13, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->serverLogsCookie:[B

    invoke-virtual {v0, v4, v3, v7, v8}, Lcom/google/android/finsky/providers/SuggestionHandler;->addRow(ILjava/lang/Object;Ljava/lang/String;[B)V

    .line 185
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 191
    :cond_0
    iget v0, v13, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->type:I

    if-ne v0, v1, :cond_1

    .line 193
    iget-object v11, v13, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->navSuggestion:Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    .line 194
    .local v11, "navSuggestion":Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;
    iget-object v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;->this$0:Lcom/google/android/finsky/providers/SearchSuggestionFetcher;

    iget-object v3, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;->val$refCountedListener:Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;

    # invokes: Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->getIconUri(Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;)Landroid/net/Uri;
    invoke-static {v0, v11, v3}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->access$000(Lcom/google/android/finsky/providers/SearchSuggestionFetcher;Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;)Landroid/net/Uri;

    move-result-object v5

    .line 195
    .local v5, "iconUri":Landroid/net/Uri;
    iget-object v6, v11, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->description:Ljava/lang/String;

    .line 196
    .local v6, "name":Ljava/lang/String;
    iget-object v12, v11, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->docId:Ljava/lang/String;

    .line 197
    .local v12, "packageName":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https://market.android.com/details?id="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&feature=sugg"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 199
    .local v9, "uri":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;->this$0:Lcom/google/android/finsky/providers/SearchSuggestionFetcher;

    iget-object v3, v0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->mSuggestionHandler:Lcom/google/android/finsky/providers/SuggestionHandler;

    const/4 v7, 0x0

    const-string v8, "com.google.android.finsky.NAVIGATIONAL_SUGGESTION"

    iget-object v10, v13, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->serverLogsCookie:[B

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/finsky/providers/SuggestionHandler;->addRow(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    goto :goto_1

    .line 203
    .end local v5    # "iconUri":Landroid/net/Uri;
    .end local v6    # "name":Ljava/lang/String;
    .end local v9    # "uri":Ljava/lang/String;
    .end local v11    # "navSuggestion":Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;
    .end local v12    # "packageName":Ljava/lang/String;
    :cond_1
    const-string v0, "Unknown suggestion type %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v13, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->type:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v7

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 206
    .end local v13    # "suggestion":Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;->val$refCountedListener:Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$RefCountedOnCompleteListener;->onComplete()V

    .line 207
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 177
    check-cast p1, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher$2;->onResponse(Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;)V

    return-void
.end method
