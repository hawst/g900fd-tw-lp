.class Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;
.super Ljava/lang/Object;
.source "OrderHistoryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->rebindAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)V
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->access$000(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;
    invoke-static {v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->access$100(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->access$000(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;
    invoke-static {v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->access$100(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 297
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->access$200(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mAdapter:Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;
    invoke-static {v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->access$100(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    invoke-static {v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->access$000(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->access$200(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->onRestoreInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V

    .line 301
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    # getter for: Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->access$200(Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 304
    :cond_0
    return-void
.end method
