.class Lcom/google/android/finsky/services/LicensingService$1;
.super Lcom/android/vending/licensing/ILicensingService$Stub;
.source "LicensingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/services/LicensingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/LicensingService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/LicensingService;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/finsky/services/LicensingService$1;->this$0:Lcom/google/android/finsky/services/LicensingService;

    invoke-direct {p0}, Lcom/android/vending/licensing/ILicensingService$Stub;-><init>()V

    return-void
.end method

.method private checkLicense(JLjava/lang/String;Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;)V
    .locals 5
    .param p1, "nonce"    # J
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/android/vending/licensing/ILicenseResultListener;
    .param p5, "versionCode"    # I
    .param p6, "acountName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 122
    new-instance v0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;-><init>()V

    .line 123
    .local v0, "licenseRequest":Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;
    iput-object p3, v0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->packageName:Ljava/lang/String;

    .line 124
    iput-boolean v1, v0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasPackageName:Z

    .line 125
    iput p5, v0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->versionCode:I

    .line 126
    iput-boolean v1, v0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasVersionCode:Z

    .line 127
    iput-wide p1, v0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->nonce:J

    .line 128
    iput-boolean v1, v0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasNonce:Z

    .line 130
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1, p6}, Lcom/google/android/finsky/FinskyApp;->getVendingApi(Ljava/lang/String;)Lcom/google/android/vending/remoting/api/VendingApi;

    move-result-object v1

    new-instance v2, Lcom/google/android/finsky/services/LicensingService$1$1;

    invoke-direct {v2, p0, p4}, Lcom/google/android/finsky/services/LicensingService$1$1;-><init>(Lcom/google/android/finsky/services/LicensingService$1;Lcom/android/vending/licensing/ILicenseResultListener;)V

    new-instance v3, Lcom/google/android/finsky/services/LicensingService$1$2;

    invoke-direct {v3, p0, p4}, Lcom/google/android/finsky/services/LicensingService$1$2;-><init>(Lcom/google/android/finsky/services/LicensingService$1;Lcom/android/vending/licensing/ILicenseResultListener;)V

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/vending/remoting/api/VendingApi;->checkLicense(Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 146
    return-void
.end method


# virtual methods
.method public checkLicense(JLjava/lang/String;Lcom/android/vending/licensing/ILicenseResultListener;)V
    .locals 23
    .param p1, "nonce"    # J
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/android/vending/licensing/ILicenseResultListener;

    .prologue
    .line 60
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/services/LicensingService$1;->this$0:Lcom/google/android/finsky/services/LicensingService;

    invoke-virtual {v3}, Lcom/google/android/finsky/services/LicensingService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v22

    .line 61
    .local v22, "packageInfo":Landroid/content/pm/PackageInfo;
    move-object/from16 v0, v22

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {}, Lcom/google/android/finsky/services/LicensingService$1;->getCallingUid()I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 62
    const/16 v3, 0x103

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    # invokes: Lcom/google/android/finsky/services/LicensingService;->notifyListener(Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v3, v4, v5}, Lcom/google/android/finsky/services/LicensingService;->access$000(Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;Ljava/lang/String;)V

    .line 118
    .end local v22    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 65
    .restart local v22    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    move-object/from16 v0, v22

    iget v8, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .local v8, "versionCode":I
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v10

    .line 74
    .local v10, "appStates":Lcom/google/android/finsky/appstate/AppStates;
    invoke-virtual {v10}, Lcom/google/android/finsky/appstate/AppStates;->blockingLoad()V

    .line 76
    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v2

    .line 77
    .local v2, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-nez v2, :cond_1

    .line 78
    const-string v3, "Unexpected null appState for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p3, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    const/16 v3, 0x102

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    # invokes: Lcom/google/android/finsky/services/LicensingService;->notifyListener(Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v3, v4, v5}, Lcom/google/android/finsky/services/LicensingService;->access$000(Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    .end local v2    # "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    .end local v8    # "versionCode":I
    .end local v10    # "appStates":Lcom/google/android/finsky/appstate/AppStates;
    .end local v22    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v18

    .line 67
    .local v18, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/16 v3, 0x102

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    # invokes: Lcom/google/android/finsky/services/LicensingService;->notifyListener(Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v3, v4, v5}, Lcom/google/android/finsky/services/LicensingService;->access$000(Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 82
    .end local v18    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    .restart local v8    # "versionCode":I
    .restart local v10    # "appStates":Lcom/google/android/finsky/appstate/AppStates;
    .restart local v22    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    iget-object v3, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v3, :cond_2

    .line 84
    iget-object v3, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAccountName()Ljava/lang/String;

    move-result-object v9

    .line 85
    .local v9, "accountName":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    .line 86
    invoke-direct/range {v3 .. v9}, Lcom/google/android/finsky/services/LicensingService$1;->checkLicense(JLjava/lang/String;Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;)V

    goto :goto_0

    .line 92
    .end local v9    # "accountName":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v20

    .line 93
    .local v20, "libraries":Lcom/google/android/finsky/library/Libraries;
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/library/Libraries;->blockingLoad()V

    .line 95
    iget-object v3, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget-object v3, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->certificateHashes:[Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v3}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v21

    .line 97
    .local v21, "ownerAccounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 100
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/accounts/Account;

    iget-object v0, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v11, p0

    move-wide/from16 v12, p1

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move/from16 v16, v8

    invoke-direct/range {v11 .. v17}, Lcom/google/android/finsky/services/LicensingService$1;->checkLicense(JLjava/lang/String;Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 109
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/services/LicensingService$1;->this$0:Lcom/google/android/finsky/services/LicensingService;

    invoke-static {v3}, Lcom/google/android/finsky/api/AccountHandler;->getFirstAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v19

    .line 111
    .local v19, "firstAccount":Landroid/accounts/Account;
    if-eqz v19, :cond_4

    .line 112
    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v11, p0

    move-wide/from16 v12, p1

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move/from16 v16, v8

    invoke-direct/range {v11 .. v17}, Lcom/google/android/finsky/services/LicensingService$1;->checkLicense(JLjava/lang/String;Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 117
    :cond_4
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    # invokes: Lcom/google/android/finsky/services/LicensingService;->notifyListener(Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v3, v4, v5}, Lcom/google/android/finsky/services/LicensingService;->access$000(Lcom/android/vending/licensing/ILicenseResultListener;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
