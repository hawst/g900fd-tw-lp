.class Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment$1;
.super Ljava/lang/Object;
.source "DatePickerDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;

.field final synthetic val$listener:Landroid/app/DatePickerDialog$OnDateSetListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;Landroid/app/DatePickerDialog$OnDateSetListener;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;

    iput-object p2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment$1;->val$listener:Landroid/app/DatePickerDialog$OnDateSetListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 73
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment$1;->val$listener:Landroid/app/DatePickerDialog$OnDateSetListener;

    if-eqz v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->mDatePickerDialog:Landroid/app/DatePickerDialog;
    invoke-static {v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;->access$000(Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment;)Landroid/app/DatePickerDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v0

    .line 75
    .local v0, "dp":Landroid/widget/DatePicker;
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/DatePickerDialogFragment$1;->val$listener:Landroid/app/DatePickerDialog$OnDateSetListener;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getYear()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getMonth()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v4

    invoke-interface {v1, v0, v2, v3, v4}, Landroid/app/DatePickerDialog$OnDateSetListener;->onDateSet(Landroid/widget/DatePicker;III)V

    .line 80
    .end local v0    # "dp":Landroid/widget/DatePicker;
    :goto_0
    return-void

    .line 78
    :cond_0
    const-string v1, "No listener found."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
