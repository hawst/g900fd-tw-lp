.class public Lcom/google/android/finsky/layout/DetailsBylinesSection;
.super Landroid/widget/LinearLayout;
.source "DetailsBylinesSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;
    }
.end annotation


# instance fields
.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mHeaderView:Lcom/google/android/finsky/layout/DecoratedTextView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mListingLayout:Landroid/widget/LinearLayout;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsBylinesSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mInflater:Landroid/view/LayoutInflater;

    .line 87
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/DetailsBylinesSection;)Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsBylinesSection;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method private getEmailLinkEntry(ILjava/lang/String;I)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;
    .locals 4
    .param p1, "titleId"    # I
    .param p2, "link"    # Ljava/lang/String;
    .param p3, "iconId"    # I

    .prologue
    .line 314
    const-string v2, "mailto"

    const/4 v3, 0x0

    invoke-static {v2, p2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 315
    .local v1, "emailUri":Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/finsky/utils/IntentUtils;->createSendIntentForUrl(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 316
    .local v0, "clickIntent":Landroid/content/Intent;
    const-string v2, "android.intent.extra.SUBJECT"

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 317
    new-instance v2, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    new-instance v3, Lcom/google/android/finsky/layout/DetailsBylinesSection$3;

    invoke-direct {v3, p0, v0}, Lcom/google/android/finsky/layout/DetailsBylinesSection$3;-><init>(Lcom/google/android/finsky/layout/DetailsBylinesSection;Landroid/content/Intent;)V

    invoke-direct {v2, p1, p3, v3}, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;-><init>(IILandroid/view/View$OnClickListener;)V

    return-object v2
.end method

.method private getWebLinkEntry(ILjava/lang/String;II)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;
    .locals 3
    .param p1, "titleId"    # I
    .param p2, "link"    # Ljava/lang/String;
    .param p3, "iconId"    # I
    .param p4, "clickEventType"    # I

    .prologue
    .line 301
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/IntentUtils;->createViewIntentForUrl(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 302
    .local v0, "clickIntent":Landroid/content/Intent;
    new-instance v1, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    new-instance v2, Lcom/google/android/finsky/layout/DetailsBylinesSection$2;

    invoke-direct {v2, p0, v0, p4}, Lcom/google/android/finsky/layout/DetailsBylinesSection$2;-><init>(Lcom/google/android/finsky/layout/DetailsBylinesSection;Landroid/content/Intent;I)V

    invoke-direct {v1, p1, p3, v2}, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;-><init>(IILandroid/view/View$OnClickListener;)V

    return-object v1
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 39
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "hasDetailsLoaded"    # Z
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 100
    if-nez p2, :cond_1

    .line 101
    const/16 v34, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->setVisibility(I)V

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    const/16 v34, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->setVisibility(I)V

    .line 106
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mListingLayout:Landroid/widget/LinearLayout;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 108
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getContext()Landroid/content/Context;

    move-result-object v17

    .line 109
    .local v17, "context":Landroid/content/Context;
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 110
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 112
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mListingLayout:Landroid/widget/LinearLayout;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 114
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v11

    .line 115
    .local v11, "bitmapLoader":Lcom/google/android/play/image/BitmapLoader;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v20

    .line 116
    .local v20, "documentType":I
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v13

    .line 118
    .local v13, "bylineEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;>;"
    sparse-switch v20, :sswitch_data_0

    .line 267
    :cond_2
    :goto_1
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v34

    if-eqz v34, :cond_f

    .line 268
    const/16 v34, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->setVisibility(I)V

    goto :goto_0

    .line 120
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mHeaderView:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v34, v0

    const v35, 0x7f0c0212

    move-object/from16 v0, v17

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mHeaderView:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v35, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-static {v0, v11, v1}, Lcom/google/android/finsky/utils/BadgeUtils;->configureCreatorBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    .line 124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v7

    .line 125
    .local v7, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    iget-object v0, v7, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerWebsite:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_3

    .line 126
    const v34, 0x7f0c022f

    iget-object v0, v7, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerWebsite:Ljava/lang/String;

    move-object/from16 v35, v0

    const v36, 0x7f0200a2

    const/16 v37, 0x72

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v35

    move/from16 v3, v36

    move/from16 v4, v37

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getWebLinkEntry(ILjava/lang/String;II)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_3
    iget-object v0, v7, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerEmail:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_4

    .line 131
    const v34, 0x7f0c0230

    iget-object v0, v7, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerEmail:Ljava/lang/String;

    move-object/from16 v35, v0

    const v36, 0x7f02009e

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v35

    move/from16 v3, v36

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getEmailLinkEntry(ILjava/lang/String;I)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/finsky/api/model/Document;->getPrivacyPolicyUrl()Ljava/lang/String;

    move-result-object v26

    .line 135
    .local v26, "privacyPolicyUrl":Ljava/lang/String;
    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_5

    .line 136
    const v34, 0x7f0c0231

    const v35, 0x7f0200a1

    const/16 v36, 0x74

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v26

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getWebLinkEntry(ILjava/lang/String;II)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    :cond_5
    new-instance v25, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    const v34, 0x7f0c0232

    const v35, 0x7f0200a0

    new-instance v36, Lcom/google/android/finsky/layout/DetailsBylinesSection$1;

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/DetailsBylinesSection$1;-><init>(Lcom/google/android/finsky/layout/DetailsBylinesSection;Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;)V

    move-object/from16 v0, v25

    move/from16 v1, v34

    move/from16 v2, v35

    move-object/from16 v3, v36

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;-><init>(IILandroid/view/View$OnClickListener;)V

    .line 154
    .local v25, "permissionEntry":Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;
    move-object/from16 v0, v25

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 159
    .end local v7    # "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    .end local v25    # "permissionEntry":Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;
    .end local v26    # "privacyPolicyUrl":Ljava/lang/String;
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v19

    .line 160
    .local v19, "developerDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerWebsite:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_6

    .line 161
    const v34, 0x7f0c022f

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerWebsite:Ljava/lang/String;

    move-object/from16 v35, v0

    const v36, 0x7f0200a2

    const/16 v37, 0x72

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v35

    move/from16 v3, v36

    move/from16 v4, v37

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getWebLinkEntry(ILjava/lang/String;II)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_6
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerEmail:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_2

    .line 166
    const v34, 0x7f0c0230

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerEmail:Ljava/lang/String;

    move-object/from16 v35, v0

    const v36, 0x7f02009e

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v35

    move/from16 v3, v36

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getEmailLinkEntry(ILjava/lang/String;I)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 174
    .end local v19    # "developerDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mHeaderView:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v34, v0

    const v35, 0x7f0c0218

    move-object/from16 v0, v17

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/finsky/api/model/Document;->getTalentDetails()Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    move-result-object v30

    .line 177
    .local v30, "talentDetails":Lcom/google/android/finsky/protos/DocDetails$TalentDetails;
    if-eqz v30, :cond_2

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    move-object/from16 v34, v0

    if-eqz v34, :cond_2

    .line 178
    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    move-object/from16 v31, v0

    .line 180
    .local v31, "talentLinks":Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;
    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    array-length v0, v0

    move/from16 v34, v0

    if-lez v34, :cond_8

    .line 181
    move-object/from16 v0, v31

    iget-object v8, v0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .local v8, "arr$":[Lcom/google/android/finsky/protos/DocAnnotations$Link;
    array-length v0, v8

    move/from16 v24, v0

    .local v24, "len$":I
    const/16 v22, 0x0

    .local v22, "i$":I
    :goto_2
    move/from16 v0, v22

    move/from16 v1, v24

    if-ge v0, v1, :cond_8

    aget-object v32, v8, v22

    .line 182
    .local v32, "websiteUrl":Lcom/google/android/finsky/protos/DocAnnotations$Link;
    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_7

    .line 183
    const v34, 0x7f0c022f

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    move-object/from16 v35, v0

    const v36, 0x7f0200a2

    const/16 v37, 0x75

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v35

    move/from16 v3, v36

    move/from16 v4, v37

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getWebLinkEntry(ILjava/lang/String;II)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    :cond_7
    add-int/lit8 v22, v22, 0x1

    goto :goto_2

    .line 189
    .end local v8    # "arr$":[Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .end local v22    # "i$":I
    .end local v24    # "len$":I
    .end local v32    # "websiteUrl":Lcom/google/android/finsky/protos/DocAnnotations$Link;
    :cond_8
    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-object/from16 v34, v0

    if-eqz v34, :cond_9

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_9

    .line 191
    const v34, 0x7f0c0230

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    move-object/from16 v35, v0

    const v36, 0x7f02009f

    const/16 v37, 0x77

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v35

    move/from16 v3, v36

    move/from16 v4, v37

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getWebLinkEntry(ILjava/lang/String;II)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    :cond_9
    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-object/from16 v34, v0

    if-eqz v34, :cond_2

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_2

    .line 198
    const v34, 0x7f0c0230

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    move-object/from16 v35, v0

    const v36, 0x7f0200a3

    const/16 v37, 0x76

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v35

    move/from16 v3, v36

    move/from16 v4, v37

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getWebLinkEntry(ILjava/lang/String;II)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 209
    .end local v30    # "talentDetails":Lcom/google/android/finsky/protos/DocDetails$TalentDetails;
    .end local v31    # "talentLinks":Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mHeaderView:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v34, v0

    const v35, 0x7f0c0218

    move-object/from16 v0, v17

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/google/android/finsky/api/model/Document;->getArtistDetails()Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    move-result-object v9

    .line 212
    .local v9, "artistDetails":Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    if-eqz v9, :cond_2

    iget-object v0, v9, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    move-object/from16 v34, v0

    if-eqz v34, :cond_2

    .line 213
    iget-object v10, v9, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    .line 214
    .local v10, "artistLinks":Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;
    iget-object v0, v10, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    array-length v0, v0

    move/from16 v34, v0

    if-lez v34, :cond_b

    .line 215
    iget-object v8, v10, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    .local v8, "arr$":[Ljava/lang/String;
    array-length v0, v8

    move/from16 v24, v0

    .restart local v24    # "len$":I
    const/16 v22, 0x0

    .restart local v22    # "i$":I
    :goto_3
    move/from16 v0, v22

    move/from16 v1, v24

    if-ge v0, v1, :cond_b

    aget-object v32, v8, v22

    .line 216
    .local v32, "websiteUrl":Ljava/lang/String;
    invoke-static/range {v32 .. v32}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_a

    .line 217
    const v34, 0x7f0c022f

    const v35, 0x7f0200a2

    const/16 v36, 0x75

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v32

    move/from16 v3, v35

    move/from16 v4, v36

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getWebLinkEntry(ILjava/lang/String;II)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_a
    add-int/lit8 v22, v22, 0x1

    goto :goto_3

    .line 223
    .end local v8    # "arr$":[Ljava/lang/String;
    .end local v22    # "i$":I
    .end local v24    # "len$":I
    .end local v32    # "websiteUrl":Ljava/lang/String;
    :cond_b
    iget-object v0, v10, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->youtubeChannelUrl:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_c

    .line 224
    const v34, 0x7f0c0233

    iget-object v0, v10, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->youtubeChannelUrl:Ljava/lang/String;

    move-object/from16 v35, v0

    const v36, 0x7f0200a3

    const/16 v37, 0x76

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v35

    move/from16 v3, v36

    move/from16 v4, v37

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getWebLinkEntry(ILjava/lang/String;II)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    :cond_c
    iget-object v0, v10, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->googlePlusProfileUrl:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_2

    .line 229
    const v34, 0x7f0c0234

    iget-object v0, v10, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->googlePlusProfileUrl:Ljava/lang/String;

    move-object/from16 v35, v0

    const v36, 0x7f02009f

    const/16 v37, 0x77

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v35

    move/from16 v3, v36

    move/from16 v4, v37

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getWebLinkEntry(ILjava/lang/String;II)Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 238
    .end local v9    # "artistDetails":Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    .end local v10    # "artistLinks":Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mHeaderView:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v34, v0

    const/16 v35, 0x8

    invoke-virtual/range {v34 .. v35}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    .line 240
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getAlbumDetails()Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    move-result-object v5

    .line 241
    .local v5, "albumDetailWrapper":Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;
    if-eqz v5, :cond_2

    .line 242
    iget-object v6, v5, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    .line 243
    .local v6, "albumDetails":Lcom/google/android/finsky/protos/DocDetails$MusicDetails;
    iget-object v0, v6, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_d

    .line 247
    iget-object v0, v6, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v34

    if-nez v34, :cond_e

    iget-object v0, v6, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->length()I

    move-result v34

    const/16 v35, 0x4

    move/from16 v0, v34

    move/from16 v1, v35

    if-lt v0, v1, :cond_e

    .line 249
    iget-object v0, v6, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    const/16 v36, 0x4

    invoke-virtual/range {v34 .. v36}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v33

    .line 250
    .local v33, "year":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getContext()Landroid/content/Context;

    move-result-object v34

    const v35, 0x7f0c03e3

    const/16 v36, 0x2

    move/from16 v0, v36

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v36, v0

    const/16 v37, 0x0

    aput-object v33, v36, v37

    const/16 v37, 0x1

    iget-object v0, v6, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    move-object/from16 v38, v0

    aput-object v38, v36, v37

    invoke-virtual/range {v34 .. v36}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 257
    .end local v33    # "year":Ljava/lang/String;
    .local v18, "copyrightText":Ljava/lang/String;
    :goto_4
    new-instance v34, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    .end local v18    # "copyrightText":Ljava/lang/String;
    :cond_d
    iget-object v0, v6, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    array-length v0, v0

    move/from16 v34, v0

    if-lez v34, :cond_2

    .line 260
    new-instance v34, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    const-string v35, ","

    iget-object v0, v6, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    move-object/from16 v36, v0

    invoke-static/range {v35 .. v36}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v34

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 254
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getContext()Landroid/content/Context;

    move-result-object v34

    const v35, 0x7f0c03e2

    const/16 v36, 0x1

    move/from16 v0, v36

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v36, v0

    const/16 v37, 0x0

    iget-object v0, v6, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    move-object/from16 v38, v0

    aput-object v38, v36, v37

    invoke-virtual/range {v34 .. v36}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .restart local v18    # "copyrightText":Ljava/lang/String;
    goto :goto_4

    .line 272
    .end local v5    # "albumDetailWrapper":Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;
    .end local v6    # "albumDetails":Lcom/google/android/finsky/protos/DocDetails$MusicDetails;
    .end local v18    # "copyrightText":Ljava/lang/String;
    :cond_f
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v15

    .line 274
    .local v15, "bylinesEntryCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->getResources()Landroid/content/res/Resources;

    move-result-object v34

    const v35, 0x7f0e0015

    invoke-virtual/range {v34 .. v35}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v23

    .line 275
    .local v23, "itemsInRow":I
    move/from16 v0, v23

    invoke-static {v15, v0}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v28

    .line 277
    .local v28, "rowCount":I
    const/16 v29, 0x0

    .local v29, "rowIndex":I
    :goto_5
    move/from16 v0, v29

    move/from16 v1, v28

    if-ge v0, v1, :cond_0

    .line 278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v34, v0

    const v35, 0x7f040057

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mListingLayout:Landroid/widget/LinearLayout;

    move-object/from16 v36, v0

    const/16 v37, 0x0

    invoke-virtual/range {v34 .. v37}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/view/ViewGroup;

    .line 280
    .local v27, "row":Landroid/view/ViewGroup;
    const/16 v16, 0x0

    .local v16, "colIndex":I
    :goto_6
    move/from16 v0, v16

    move/from16 v1, v23

    if-ge v0, v1, :cond_11

    .line 281
    mul-int v34, v23, v29

    add-int v21, v34, v16

    .line 282
    .local v21, "entryIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v34, v0

    const v35, 0x7f040055

    const/16 v36, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v35

    move-object/from16 v2, v27

    move/from16 v3, v36

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/finsky/layout/DetailsBylinesCell;

    .line 285
    .local v12, "bylineCell":Lcom/google/android/finsky/layout/DetailsBylinesCell;
    move/from16 v0, v21

    if-lt v0, v15, :cond_10

    .line 287
    const/16 v34, 0x4

    move/from16 v0, v34

    invoke-virtual {v12, v0}, Lcom/google/android/finsky/layout/DetailsBylinesCell;->setVisibility(I)V

    .line 293
    :goto_7
    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 280
    add-int/lit8 v16, v16, 0x1

    goto :goto_6

    .line 289
    :cond_10
    move/from16 v0, v21

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    .line 290
    .local v14, "bylineEntry":Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;
    invoke-virtual {v12, v14}, Lcom/google/android/finsky/layout/DetailsBylinesCell;->populate(Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;)V

    goto :goto_7

    .line 295
    .end local v12    # "bylineCell":Lcom/google/android/finsky/layout/DetailsBylinesCell;
    .end local v14    # "bylineEntry":Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;
    .end local v21    # "entryIndex":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mListingLayout:Landroid/widget/LinearLayout;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 277
    add-int/lit8 v29, v29, 0x1

    goto :goto_5

    .line 118
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_4
        0x3 -> :sswitch_3
        0x8 -> :sswitch_1
        0x1e -> :sswitch_2
    .end sparse-switch
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 93
    const v0, 0x7f0a0147

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mListingLayout:Landroid/widget/LinearLayout;

    .line 94
    const v0, 0x7f0a014f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsBylinesSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DecoratedTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection;->mHeaderView:Lcom/google/android/finsky/layout/DecoratedTextView;

    .line 95
    return-void
.end method
