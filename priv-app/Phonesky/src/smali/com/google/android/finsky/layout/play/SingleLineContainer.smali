.class public Lcom/google/android/finsky/layout/play/SingleLineContainer;
.super Landroid/widget/LinearLayout;
.source "SingleLineContainer.java"


# instance fields
.field private mFlexibleChild:Landroid/view/View;

.field private final mFlexibleChildId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    sget-object v1, Lcom/android/vending/R$styleable;->SingleLineContainer:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 49
    .local v0, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/SingleLineContainer;->mFlexibleChildId:I

    .line 51
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 52
    return-void
.end method

.method private isCenteredVertically(Landroid/view/View;)Z
    .locals 5
    .param p1, "childView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getBaseline()I

    move-result v0

    .line 64
    .local v0, "childBaseline":I
    const/4 v4, -0x1

    if-ne v0, v4, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v3

    .line 67
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 69
    .local v1, "childLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v4, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    and-int/lit8 v2, v4, 0x70

    .line 70
    .local v2, "verticalGravity":I
    const/16 v4, 0x10

    if-eq v2, v4, :cond_0

    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 57
    iget v0, p0, Lcom/google/android/finsky/layout/play/SingleLineContainer;->mFlexibleChildId:I

    if-eqz v0, :cond_0

    .line 58
    iget v0, p0, Lcom/google/android/finsky/layout/play/SingleLineContainer;->mFlexibleChildId:I

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/SingleLineContainer;->mFlexibleChild:Landroid/view/View;

    .line 60
    :cond_0
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 14
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getPaddingTop()I

    move-result v10

    .line 147
    .local v10, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getPaddingBottom()I

    move-result v9

    .line 148
    .local v9, "paddingBottom":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getHeight()I

    move-result v7

    .line 149
    .local v7, "height":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getChildCount()I

    move-result v2

    .line 152
    .local v2, "childCount":I
    const/4 v0, 0x0

    .line 153
    .local v0, "baseline":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v2, :cond_1

    .line 154
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 155
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_0

    .line 153
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 159
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getBaseline()I

    move-result v12

    invoke-static {v0, v12}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1

    .line 165
    .end local v1    # "child":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getPaddingLeft()I

    move-result v11

    .line 166
    .local v11, "x":I
    const/4 v8, 0x0

    :goto_2
    if-ge v8, v2, :cond_4

    .line 167
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 168
    .restart local v1    # "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_2

    .line 166
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 172
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 173
    .local v5, "childWidth":I
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 174
    .local v3, "childHeight":I
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 176
    .local v4, "childLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v12, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v11, v12

    .line 178
    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->isCenteredVertically(Landroid/view/View;)Z

    move-result v12

    if-eqz v12, :cond_3

    sub-int v12, v7, v10

    sub-int/2addr v12, v9

    sub-int/2addr v12, v3

    div-int/lit8 v12, v12, 0x2

    add-int v6, v10, v12

    .line 181
    .local v6, "childY":I
    :goto_4
    add-int v12, v11, v5

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    add-int/2addr v13, v6

    invoke-virtual {v1, v11, v6, v12, v13}, Landroid/view/View;->layout(IIII)V

    .line 183
    iget v12, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v12, v5

    add-int/2addr v11, v12

    goto :goto_3

    .line 178
    .end local v6    # "childY":I
    :cond_3
    add-int v12, v10, v0

    invoke-virtual {v1}, Landroid/view/View;->getBaseline()I

    move-result v13

    sub-int v6, v12, v13

    goto :goto_4

    .line 185
    .end local v1    # "child":Landroid/view/View;
    .end local v3    # "childHeight":I
    .end local v4    # "childLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v5    # "childWidth":I
    :cond_4
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 19
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 75
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 76
    .local v9, "fullWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getPaddingLeft()I

    move-result v15

    sub-int v15, v9, v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getPaddingRight()I

    move-result v16

    sub-int v1, v15, v16

    .line 78
    .local v1, "availableWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getChildCount()I

    move-result v5

    .line 82
    .local v5, "childCount":I
    const/4 v2, 0x0

    .line 83
    .local v2, "baseline":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    if-ge v12, v5, :cond_2

    .line 84
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 85
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v15

    const/16 v16, 0x8

    move/from16 v0, v16

    if-ne v15, v0, :cond_1

    .line 83
    :cond_0
    :goto_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 89
    :cond_1
    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Landroid/view/View;->measure(II)V

    .line 91
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->isCenteredVertically(Landroid/view/View;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 92
    invoke-virtual {v3}, Landroid/view/View;->getBaseline()I

    move-result v15

    invoke-static {v2, v15}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_1

    .line 98
    .end local v3    # "child":Landroid/view/View;
    :cond_2
    const/4 v10, 0x0

    .line 99
    .local v10, "heightBelowBaseline":I
    const/4 v14, 0x0

    .line 100
    .local v14, "totalWidth":I
    const/4 v11, 0x0

    .line 101
    .local v11, "heightOfTallestChildWithNoBaseline":I
    const/4 v12, 0x0

    :goto_2
    if-ge v12, v5, :cond_5

    .line 102
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 103
    .restart local v3    # "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v15

    const/16 v16, 0x8

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 101
    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 107
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 108
    .local v7, "childMeasuredHeight":I
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->isCenteredVertically(Landroid/view/View;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 110
    invoke-static {v11, v7}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 119
    :goto_4
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 121
    .local v6, "childLp":Landroid/widget/LinearLayout$LayoutParams;
    iget v15, v6, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v16

    add-int v15, v15, v16

    iget v0, v6, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    move/from16 v16, v0

    add-int v15, v15, v16

    add-int/2addr v14, v15

    goto :goto_3

    .line 114
    .end local v6    # "childLp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_4
    invoke-virtual {v3}, Landroid/view/View;->getBaseline()I

    move-result v4

    .line 115
    .local v4, "childBaseline":I
    sub-int v15, v7, v4

    invoke-static {v10, v15}, Ljava/lang/Math;->max(II)I

    move-result v10

    goto :goto_4

    .line 124
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "childBaseline":I
    .end local v7    # "childMeasuredHeight":I
    :cond_5
    add-int v15, v2, v10

    invoke-static {v15, v11}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 126
    .local v13, "totalHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getPaddingTop()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->getPaddingBottom()I

    move-result v16

    add-int v15, v15, v16

    add-int/2addr v13, v15

    .line 129
    if-le v14, v1, :cond_6

    .line 131
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/SingleLineContainer;->mFlexibleChild:Landroid/view/View;

    if-eqz v15, :cond_6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/SingleLineContainer;->mFlexibleChild:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getVisibility()I

    move-result v15

    const/16 v16, 0x8

    move/from16 v0, v16

    if-eq v15, v0, :cond_6

    .line 132
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/SingleLineContainer;->mFlexibleChild:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    sub-int v16, v14, v1

    sub-int v8, v15, v16

    .line 134
    .local v8, "flexibleChildWidth":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/SingleLineContainer;->mFlexibleChild:Landroid/view/View;

    const/high16 v16, 0x40000000    # 2.0f

    move/from16 v0, v16

    invoke-static {v8, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/SingleLineContainer;->mFlexibleChild:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v17

    const/high16 v18, 0x40000000    # 2.0f

    invoke-static/range {v17 .. v18}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v17

    invoke-virtual/range {v15 .. v17}, Landroid/view/View;->measure(II)V

    .line 141
    .end local v8    # "flexibleChildWidth":I
    :cond_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v13}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->setMeasuredDimension(II)V

    .line 142
    return-void
.end method
