.class public Lcom/google/android/finsky/layout/ObservableScrollView;
.super Landroid/widget/ScrollView;
.source "ObservableScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;
    }
.end annotation


# static fields
.field private static final HAS_IN_LAYOUT_METHOD:Z


# instance fields
.field private mIsInLayout:Z

.field private mOnScrollListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/layout/ObservableScrollView;->HAS_IN_LAYOUT_METHOD:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/ObservableScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method


# virtual methods
.method public addOnScrollListener(Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;)V
    .locals 1
    .param p1, "onScrollListener"    # Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/finsky/layout/ObservableScrollView;->mOnScrollListeners:Ljava/util/List;

    if-nez v0, :cond_0

    .line 45
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/ObservableScrollView;->mOnScrollListeners:Ljava/util/List;

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/ObservableScrollView;->mOnScrollListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    return-void
.end method

.method public isViewInLayout()Z
    .locals 1

    .prologue
    .line 80
    sget-boolean v0, Lcom/google/android/finsky/layout/ObservableScrollView;->HAS_IN_LAYOUT_METHOD:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ObservableScrollView;->isInLayout()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/ObservableScrollView;->mIsInLayout:Z

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/ObservableScrollView;->mIsInLayout:Z

    .line 71
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/ObservableScrollView;->mIsInLayout:Z

    .line 73
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 3
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 58
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 59
    iget-object v2, p0, Lcom/google/android/finsky/layout/ObservableScrollView;->mOnScrollListeners:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 60
    iget-object v2, p0, Lcom/google/android/finsky/layout/ObservableScrollView;->mOnScrollListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 62
    .local v1, "listenerCount":I
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 63
    iget-object v2, p0, Lcom/google/android/finsky/layout/ObservableScrollView;->mOnScrollListeners:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;

    invoke-interface {v2, p1, p2}, Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;->onScroll(II)V

    .line 62
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 66
    .end local v0    # "i":I
    .end local v1    # "listenerCount":I
    :cond_0
    return-void
.end method

.method public removeOnScrollListener(Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;)V
    .locals 1
    .param p1, "scrollListener"    # Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/layout/ObservableScrollView;->mOnScrollListeners:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/layout/ObservableScrollView;->mOnScrollListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 54
    :cond_0
    return-void
.end method
