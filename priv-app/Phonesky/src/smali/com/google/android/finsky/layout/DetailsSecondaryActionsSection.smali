.class public Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;
.super Landroid/widget/LinearLayout;
.source "DetailsSecondaryActionsSection.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
.implements Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;


# static fields
.field private static final sCountFormatter:Ljava/text/NumberFormat;


# instance fields
.field mDetailsUrl:Ljava/lang/String;

.field mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field mDoc:Lcom/google/android/finsky/api/model/Document;

.field mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mPlusOneButton:Landroid/view/View;

.field private mPlusOneIcon:Landroid/widget/TextView;

.field private mPlusOneSet:Z

.field private mPlusOneTotal:J

.field private mShareButton:Landroid/view/View;

.field private mShowWishlist:Z

.field private mWishlistButton:Landroid/view/View;

.field private mWishlistButtonIcon:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->sCountFormatter:Ljava/text/NumberFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    const/16 v0, 0x71c

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 75
    return-void
.end method

.method private rebindPlusOneButton()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 207
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 211
    .local v2, "resources":Landroid/content/res/Resources;
    iget-wide v4, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneTotal:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    const-wide/16 v0, 0x1

    .line 212
    .local v0, "displayCount":J
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneIcon:Landroid/widget/TextView;

    const v4, 0x7f0c03f9

    new-array v5, v9, [Ljava/lang/Object;

    sget-object v6, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->sCountFormatter:Ljava/text/NumberFormat;

    invoke-virtual {v6, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-boolean v3, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneSet:Z

    if-eqz v3, :cond_1

    .line 217
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneIcon:Landroid/widget/TextView;

    const v4, 0x7f02019f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 218
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneIcon:Landroid/widget/TextView;

    const v4, 0x7f09009c

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 220
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneButton:Landroid/view/View;

    const v4, 0x7f0c03fc

    new-array v5, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneTotal:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 231
    :goto_1
    return-void

    .line 211
    .end local v0    # "displayCount":J
    :cond_0
    iget-wide v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneTotal:J

    goto :goto_0

    .line 224
    .restart local v0    # "displayCount":J
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneIcon:Landroid/widget/TextView;

    const v4, 0x7f02019e

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 225
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneIcon:Landroid/widget/TextView;

    const v4, 0x7f09005a

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 227
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneButton:Landroid/view/View;

    const v4, 0x7f0c03fb

    new-array v5, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneTotal:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private rebindWishlistButton(Z)V
    .locals 3
    .param p1, "isInWishlist"    # Z

    .prologue
    .line 194
    if-eqz p1, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mWishlistButtonIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getWishlistOnDrawable(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mWishlistButton:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0257

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 204
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mWishlistButtonIcon:Landroid/widget/ImageView;

    const v1, 0x7f0200fc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mWishlistButton:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0256

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private sendPlusOneToggleAccessibilityEvent(Z)V
    .locals 3
    .param p1, "setByUser"    # Z

    .prologue
    .line 243
    if-eqz p1, :cond_0

    .line 244
    const v1, 0x7f0c032c

    .line 248
    .local v1, "eventTextId":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 249
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p0}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 250
    return-void

    .line 246
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "eventTextId":I
    :cond_0
    const v1, 0x7f0c032d

    .restart local v1    # "eventTextId":I
    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;ZLjava/lang/String;Landroid/os/Bundle;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 6
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "hasDetailsLoaded"    # Z
    .param p3, "detailsUrl"    # Ljava/lang/String;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 90
    if-nez p2, :cond_0

    .line 91
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->setVisibility(I)V

    .line 132
    :goto_0
    return-void

    .line 95
    :cond_0
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->setVisibility(I)V

    .line 97
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 98
    iput-object p6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 99
    iput-object p5, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 100
    iput-object p3, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDetailsUrl:Ljava/lang/String;

    .line 103
    invoke-static {p1, p5}, Lcom/google/android/finsky/utils/WishlistHelper;->shouldHideWishlistAction(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mShowWishlist:Z

    .line 104
    iget-boolean v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mShowWishlist:Z

    if-eqz v2, :cond_3

    .line 105
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mWishlistButton:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 106
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/finsky/utils/WishlistHelper;->isInWishlist(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Z

    move-result v0

    .line 108
    .local v0, "isInWishlist":Z
    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->rebindWishlistButton(Z)V

    .line 109
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mWishlistButton:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    .end local v0    # "isInWishlist":Z
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mShareButton:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasPlusOneData()Z

    move-result v2

    if-nez v2, :cond_4

    .line 122
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v2, v3

    .line 103
    goto :goto_1

    .line 111
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mWishlistButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 124
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 125
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getPlusOneData()Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    move-result-object v1

    .line 126
    .local v1, "plusOneData":Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;
    const-string v2, "DetailsSecondaryActionsSection.plusOneSet"

    iget-boolean v3, v1, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    invoke-virtual {p4, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneSet:Z

    .line 128
    const-string v2, "DetailsSecondaryActionsSection.plusOneCount"

    iget-wide v4, v1, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    invoke-virtual {p4, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneTotal:J

    .line 129
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->rebindPlusOneButton()V

    goto :goto_0
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 276
    invoke-static {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 277
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 0

    .prologue
    .line 254
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 255
    invoke-static {p0}, Lcom/google/android/finsky/utils/WishlistHelper;->addWishlistStatusListener(Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;)V

    .line 256
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const-wide/16 v10, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v9, 0x0

    .line 141
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mWishlistButton:Landroid/view/View;

    if-ne p1, v6, :cond_0

    .line 142
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/WishlistHelper;->isInWishlist(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Z

    move-result v2

    .line 145
    .local v2, "isInWishlist":Z
    if-eqz v2, :cond_3

    .line 146
    const/16 v3, 0xcd

    .line 151
    .local v3, "playStoreUiElementType":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-static {p0, v6, v7}, Lcom/google/android/finsky/utils/WishlistHelper;->processWishlistClick(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;)V

    .line 152
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v6

    invoke-virtual {v6, v3, v9, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 155
    .end local v2    # "isInWishlist":Z
    .end local v3    # "playStoreUiElementType":I
    :cond_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mShareButton:Landroid/view/View;

    if-ne p1, v6, :cond_1

    .line 156
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 157
    .local v0, "context":Landroid/content/Context;
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v0, v6}, Lcom/google/android/finsky/utils/IntentUtils;->buildShareIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;)Landroid/content/Intent;

    move-result-object v1

    .line 158
    .local v1, "intent":Landroid/content/Intent;
    const v6, 0x7f0c0277

    new-array v7, v4, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 160
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v6

    const/16 v7, 0xca

    invoke-virtual {v6, v7, v9, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 164
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneButton:Landroid/view/View;

    if-ne p1, v6, :cond_2

    .line 165
    iget-boolean v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneSet:Z

    if-eqz v6, :cond_4

    .line 166
    iget-wide v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneTotal:J

    sub-long/2addr v6, v10

    iput-wide v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneTotal:J

    .line 170
    :goto_1
    iget-boolean v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneSet:Z

    if-nez v6, :cond_5

    :goto_2
    iput-boolean v4, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneSet:Z

    .line 171
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneSet:Z

    new-instance v7, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection$1;

    invoke-direct {v7, p0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection$1;-><init>(Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;)V

    new-instance v8, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection$2;

    invoke-direct {v8, p0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection$2;-><init>(Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;)V

    invoke-interface {v4, v5, v6, v7, v8}, Lcom/google/android/finsky/api/DfeApi;->setPlusOne(Ljava/lang/String;ZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 186
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->rebindPlusOneButton()V

    .line 187
    iget-boolean v4, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneSet:Z

    invoke-direct {p0, v4}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->sendPlusOneToggleAccessibilityEvent(Z)V

    .line 188
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v4

    const/16 v5, 0xd0

    invoke-virtual {v4, v5, v9, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 191
    :cond_2
    return-void

    .line 148
    .restart local v2    # "isInWishlist":Z
    :cond_3
    const/16 v3, 0xcc

    .restart local v3    # "playStoreUiElementType":I
    goto/16 :goto_0

    .line 168
    .end local v2    # "isInWishlist":Z
    .end local v3    # "playStoreUiElementType":I
    :cond_4
    iget-wide v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneTotal:J

    add-long/2addr v6, v10

    iput-wide v6, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneTotal:J

    goto :goto_1

    :cond_5
    move v4, v5

    .line 170
    goto :goto_2
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 260
    invoke-static {p0}, Lcom/google/android/finsky/utils/WishlistHelper;->removeWishlistStatusListener(Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;)V

    .line 261
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 262
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 80
    const v0, 0x7f0a015e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mWishlistButton:Landroid/view/View;

    .line 81
    const v0, 0x7f0a0160

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mWishlistButtonIcon:Landroid/widget/ImageView;

    .line 82
    const v0, 0x7f0a0161

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mShareButton:Landroid/view/View;

    .line 83
    const v0, 0x7f0a0162

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneButton:Landroid/view/View;

    .line 84
    const v0, 0x7f0a0163

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneIcon:Landroid/widget/TextView;

    .line 85
    return-void
.end method

.method public onSavedInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 135
    const-string v0, "DetailsSecondaryActionsSection.plusOneSet"

    iget-boolean v1, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneSet:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 136
    const-string v0, "DetailsSecondaryActionsSection.plusOneCount"

    iget-wide v2, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mPlusOneTotal:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 137
    return-void
.end method

.method public onWishlistStatusChanged(Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "isInWishlist"    # Z
    .param p3, "isCommitted"    # Z

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mShowWishlist:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    invoke-direct {p0, p2}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->rebindWishlistButton(Z)V

    .line 239
    :cond_0
    return-void
.end method
