.class public Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;
.super Ljava/lang/Object;
.source "DfeApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/api/DfeApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GaiaAuthParameters"
.end annotation


# instance fields
.field private final mLastAuthTimestamp:J

.field private final mPurchaseAuth:I


# direct methods
.method public constructor <init>(JI)V
    .locals 1
    .param p1, "lastAuthTimestamp"    # J
    .param p3, "purchaseAuth"    # I

    .prologue
    .line 1192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1193
    iput-wide p1, p0, Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;->mLastAuthTimestamp:J

    .line 1194
    iput p3, p0, Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;->mPurchaseAuth:I

    .line 1195
    return-void
.end method


# virtual methods
.method public addToRequest(Lcom/google/android/finsky/api/DfeApi$DfePostRequest;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/DfeApi$DfePostRequest",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1198
    .local p1, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<*>;"
    iget v0, p0, Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;->mPurchaseAuth:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1200
    iget-wide v0, p0, Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;->mLastAuthTimestamp:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1201
    const-string v0, "pclats"

    iget-wide v2, p0, Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;->mLastAuthTimestamp:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    :cond_0
    const-string v0, "pcauth"

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1211
    :cond_1
    :goto_0
    return-void

    .line 1206
    :cond_2
    iget v0, p0, Lcom/google/android/finsky/api/DfeApi$GaiaAuthParameters;->mPurchaseAuth:I

    if-nez v0, :cond_1

    .line 1207
    const-string v0, "pcauth"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
