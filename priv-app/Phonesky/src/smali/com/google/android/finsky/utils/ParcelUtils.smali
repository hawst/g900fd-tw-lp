.class public Lcom/google/android/finsky/utils/ParcelUtils;
.super Ljava/lang/Object;
.source "ParcelUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/ParcelUtils$CacheVersionException;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method public static declared-synchronized readFromDisk(Ljava/io/File;)Landroid/os/Parcelable;
    .locals 11
    .param p0, "cacheFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Ljava/io/File;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 56
    const-class v7, Lcom/google/android/finsky/utils/ParcelUtils;

    monitor-enter v7

    const/4 v0, 0x0

    .line 57
    .local v0, "cachedObject":Landroid/os/Parcelable;, "TT;"
    const/4 v3, 0x0

    .line 59
    .local v3, "ois":Ljava/io/DataInputStream;
    :try_start_0
    new-instance v4, Ljava/io/DataInputStream;

    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v6}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lcom/google/android/finsky/utils/ParcelUtils$CacheVersionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 60
    .end local v3    # "ois":Ljava/io/DataInputStream;
    .local v4, "ois":Ljava/io/DataInputStream;
    :try_start_1
    invoke-static {v4}, Lcom/google/android/finsky/utils/ParcelUtils;->readObject(Ljava/io/DataInputStream;)Landroid/os/Parcelable;
    :try_end_1
    .catch Lcom/google/android/finsky/utils/ParcelUtils$CacheVersionException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v2

    .line 61
    .local v2, "obj":Landroid/os/Parcelable;, "TT;"
    if-eqz v2, :cond_0

    .line 62
    move-object v0, v2

    .line 72
    :cond_0
    :try_start_2
    invoke-static {v4}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-object v3, v4

    .line 74
    .end local v2    # "obj":Landroid/os/Parcelable;, "TT;"
    .end local v4    # "ois":Ljava/io/DataInputStream;
    .restart local v3    # "ois":Ljava/io/DataInputStream;
    :goto_0
    monitor-exit v7

    return-object v0

    .line 64
    :catch_0
    move-exception v1

    .line 65
    .local v1, "e":Lcom/google/android/finsky/utils/ParcelUtils$CacheVersionException;
    :goto_1
    :try_start_3
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 66
    const-string v6, "Version mismatch in %s: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v1}, Lcom/google/android/finsky/utils/ParcelUtils$CacheVersionException;->getMessage()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 72
    :try_start_4
    invoke-static {v3}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 56
    .end local v1    # "e":Lcom/google/android/finsky/utils/ParcelUtils$CacheVersionException;
    :catchall_0
    move-exception v6

    :goto_2
    monitor-exit v7

    throw v6

    .line 67
    :catch_1
    move-exception v5

    .line 68
    .local v5, "t":Ljava/lang/Throwable;
    :goto_3
    :try_start_5
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 69
    const-string v6, "Unable to read %s from cache: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v5}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 72
    :try_start_6
    invoke-static {v3}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v5    # "t":Ljava/lang/Throwable;
    :catchall_1
    move-exception v6

    :goto_4
    invoke-static {v3}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    throw v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .end local v3    # "ois":Ljava/io/DataInputStream;
    .restart local v4    # "ois":Ljava/io/DataInputStream;
    :catchall_2
    move-exception v6

    move-object v3, v4

    .end local v4    # "ois":Ljava/io/DataInputStream;
    .restart local v3    # "ois":Ljava/io/DataInputStream;
    goto :goto_4

    .line 67
    .end local v3    # "ois":Ljava/io/DataInputStream;
    .restart local v4    # "ois":Ljava/io/DataInputStream;
    :catch_2
    move-exception v5

    move-object v3, v4

    .end local v4    # "ois":Ljava/io/DataInputStream;
    .restart local v3    # "ois":Ljava/io/DataInputStream;
    goto :goto_3

    .line 64
    .end local v3    # "ois":Ljava/io/DataInputStream;
    .restart local v4    # "ois":Ljava/io/DataInputStream;
    :catch_3
    move-exception v1

    move-object v3, v4

    .end local v4    # "ois":Ljava/io/DataInputStream;
    .restart local v3    # "ois":Ljava/io/DataInputStream;
    goto :goto_1

    .line 56
    .end local v3    # "ois":Ljava/io/DataInputStream;
    .restart local v2    # "obj":Landroid/os/Parcelable;, "TT;"
    .restart local v4    # "ois":Ljava/io/DataInputStream;
    :catchall_3
    move-exception v6

    move-object v3, v4

    .end local v4    # "ois":Ljava/io/DataInputStream;
    .restart local v3    # "ois":Ljava/io/DataInputStream;
    goto :goto_2
.end method

.method private static readObject(Ljava/io/DataInputStream;)Landroid/os/Parcelable;
    .locals 7
    .param p0, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Ljava/io/DataInputStream;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 91
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    .line 93
    const-class v5, Lcom/google/android/finsky/utils/ParcelUtils;

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 94
    .local v1, "classLoader":Ljava/lang/ClassLoader;
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    .line 95
    .local v2, "length":I
    new-array v0, v2, [B

    .line 96
    .local v0, "buffer":[B
    invoke-virtual {p0, v0}, Ljava/io/DataInputStream;->read([B)I

    .line 97
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 98
    .local v4, "p":Landroid/os/Parcel;
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 99
    array-length v5, v0

    invoke-virtual {v4, v0, v6, v5}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 100
    invoke-virtual {v4, v6}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 101
    const/4 v3, 0x0

    .line 103
    .local v3, "object":Landroid/os/Parcelable;, "TT;"
    :try_start_0
    invoke-virtual {v4, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 105
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 107
    return-object v3

    .line 105
    :catchall_0
    move-exception v5

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    throw v5
.end method

.method private static writeObject(Ljava/io/DataOutputStream;Landroid/os/Parcelable;)V
    .locals 3
    .param p0, "out"    # Ljava/io/DataOutputStream;
    .param p1, "object"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 80
    .local v1, "p":Landroid/os/Parcel;
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 81
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 82
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    .line 83
    .local v0, "marshalled":[B
    array-length v2, v0

    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 84
    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 88
    return-void

    .line 86
    .end local v0    # "marshalled":[B
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v2
.end method

.method public static declared-synchronized writeToDisk(Ljava/io/File;Landroid/os/Parcelable;)V
    .locals 4
    .param p0, "cacheFile"    # Ljava/io/File;
    .param p1, "object"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    const-class v3, Lcom/google/android/finsky/utils/ParcelUtils;

    monitor-enter v3

    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    .line 36
    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 38
    const/4 v0, 0x0

    .line 40
    .local v0, "dos":Ljava/io/DataOutputStream;
    :try_start_1
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 41
    .end local v0    # "dos":Ljava/io/DataOutputStream;
    .local v1, "dos":Ljava/io/DataOutputStream;
    :try_start_2
    invoke-static {v1, p1}, Lcom/google/android/finsky/utils/ParcelUtils;->writeObject(Ljava/io/DataOutputStream;Landroid/os/Parcelable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 43
    :try_start_3
    invoke-static {v1}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 45
    monitor-exit v3

    return-void

    .line 43
    .end local v1    # "dos":Ljava/io/DataOutputStream;
    .restart local v0    # "dos":Ljava/io/DataOutputStream;
    :catchall_0
    move-exception v2

    :goto_0
    :try_start_4
    invoke-static {v0}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 35
    .end local v0    # "dos":Ljava/io/DataOutputStream;
    :catchall_1
    move-exception v2

    monitor-exit v3

    throw v2

    .line 43
    .restart local v1    # "dos":Ljava/io/DataOutputStream;
    :catchall_2
    move-exception v2

    move-object v0, v1

    .end local v1    # "dos":Ljava/io/DataOutputStream;
    .restart local v0    # "dos":Ljava/io/DataOutputStream;
    goto :goto_0
.end method
