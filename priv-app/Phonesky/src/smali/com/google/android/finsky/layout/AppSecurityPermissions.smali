.class public Lcom/google/android/finsky/layout/AppSecurityPermissions;
.super Landroid/widget/LinearLayout;
.source "AppSecurityPermissions.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mExpansionState:I

.field private mPackageTitle:Ljava/lang/String;

.field private mPermissionAdapter:Lcom/google/android/finsky/layout/PermissionAdapter;

.field private final mPermissionViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/AppSecurityPermissions;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPermissionViews:Ljava/util/List;

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mExpansionState:I

    .line 46
    return-void
.end method

.method private showPermissions()V
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 78
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->removeAllViews()V

    .line 79
    iget-object v8, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPermissionViews:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 81
    iget-object v8, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPermissionAdapter:Lcom/google/android/finsky/layout/PermissionAdapter;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/PermissionAdapter;->getCount()I

    move-result v0

    .line 83
    .local v0, "count":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPermissionAdapter:Lcom/google/android/finsky/layout/PermissionAdapter;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/PermissionAdapter;->showTheNoPermissionMessage()Z

    move-result v8

    if-nez v8, :cond_0

    .line 84
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 85
    iget-object v8, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPermissionAdapter:Lcom/google/android/finsky/layout/PermissionAdapter;

    const/4 v9, 0x0

    invoke-virtual {v8, v1, v9, p0}, Lcom/google/android/finsky/layout/PermissionAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 86
    .local v7, "view":Landroid/view/View;
    iget-object v8, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPermissionViews:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->addView(Landroid/view/View;)V

    .line 84
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 90
    .end local v1    # "i":I
    .end local v7    # "view":Landroid/view/View;
    :cond_0
    iget-object v8, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mContext:Landroid/content/Context;

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f0400e2

    invoke-virtual {v8, v9, p0, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 92
    .local v3, "noPermissions":Landroid/widget/TextView;
    sget-object v8, Lcom/google/android/finsky/config/G;->permissionBucketsLearnMoreLink:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v8}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 93
    .local v2, "learnMoreLink":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 94
    iget-object v8, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPermissionAdapter:Lcom/google/android/finsky/layout/PermissionAdapter;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/PermissionAdapter;->isAppInstalled()Z

    move-result v8

    if-eqz v8, :cond_2

    const v6, 0x7f0c027d

    .line 97
    .local v6, "stringId":I
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPackageTitle:Ljava/lang/String;

    aput-object v10, v9, v12

    aput-object v2, v9, v11

    invoke-virtual {v8, v6, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    .line 99
    .local v4, "noPermsSeq":Ljava/lang/CharSequence;
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 108
    .end local v4    # "noPermsSeq":Ljava/lang/CharSequence;
    .end local v6    # "stringId":I
    :goto_2
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->addView(Landroid/view/View;)V

    .line 110
    .end local v2    # "learnMoreLink":Ljava/lang/String;
    .end local v3    # "noPermissions":Landroid/widget/TextView;
    :cond_1
    return-void

    .line 94
    .restart local v2    # "learnMoreLink":Ljava/lang/String;
    .restart local v3    # "noPermissions":Landroid/widget/TextView;
    :cond_2
    const v6, 0x7f0c027b

    goto :goto_1

    .line 103
    :cond_3
    iget-object v8, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPermissionAdapter:Lcom/google/android/finsky/layout/PermissionAdapter;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/PermissionAdapter;->isAppInstalled()Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mContext:Landroid/content/Context;

    const v9, 0x7f0c027e

    new-array v10, v11, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPackageTitle:Ljava/lang/String;

    aput-object v11, v10, v12

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 106
    .local v5, "noPermsStr":Ljava/lang/String;
    :goto_3
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 103
    .end local v5    # "noPermsStr":Ljava/lang/String;
    :cond_4
    iget-object v8, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mContext:Landroid/content/Context;

    const v9, 0x7f0c027a

    new-array v10, v11, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPackageTitle:Ljava/lang/String;

    aput-object v11, v10, v12

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_3
.end method


# virtual methods
.method public bindInfo(Lcom/google/android/finsky/layout/PermissionAdapter;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "adapter"    # Lcom/google/android/finsky/layout/PermissionAdapter;
    .param p2, "packageTitle"    # Ljava/lang/String;
    .param p3, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mContext:Landroid/content/Context;

    .line 62
    iput-object p2, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPackageTitle:Ljava/lang/String;

    .line 64
    iput-object p1, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPermissionAdapter:Lcom/google/android/finsky/layout/PermissionAdapter;

    .line 65
    iget v0, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mExpansionState:I

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPackageTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p3, v0, v1}, Lcom/google/android/finsky/utils/ExpandableUtils;->getSavedExpansionState(Landroid/os/Bundle;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mExpansionState:I

    .line 70
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->showPermissions()V

    .line 71
    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mPackageTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/layout/AppSecurityPermissions;->mExpansionState:I

    invoke-static {p1, v0, v1}, Lcom/google/android/finsky/utils/ExpandableUtils;->saveExpansionState(Landroid/os/Bundle;Ljava/lang/String;I)V

    .line 75
    return-void
.end method
