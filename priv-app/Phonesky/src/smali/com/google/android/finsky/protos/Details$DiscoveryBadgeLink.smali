.class public final Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Details.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Details;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DiscoveryBadgeLink"
.end annotation


# instance fields
.field public criticReviewsUrl:Ljava/lang/String;

.field public hasCriticReviewsUrl:Z

.field public hasUserReviewsUrl:Z

.field public link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

.field public userReviewsUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1086
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1087
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->clear()Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    .line 1088
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1091
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 1092
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->userReviewsUrl:Ljava/lang/String;

    .line 1093
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasUserReviewsUrl:Z

    .line 1094
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->criticReviewsUrl:Ljava/lang/String;

    .line 1095
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasCriticReviewsUrl:Z

    .line 1096
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->cachedSize:I

    .line 1097
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1117
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1118
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v1, :cond_0

    .line 1119
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1122
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasUserReviewsUrl:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->userReviewsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1123
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->userReviewsUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1126
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasCriticReviewsUrl:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->criticReviewsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1127
    :cond_3
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->criticReviewsUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1130
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1138
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1139
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1143
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1144
    :sswitch_0
    return-object p0

    .line 1149
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v1, :cond_1

    .line 1150
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$Link;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 1152
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1156
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->userReviewsUrl:Ljava/lang/String;

    .line 1157
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasUserReviewsUrl:Z

    goto :goto_0

    .line 1161
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->criticReviewsUrl:Ljava/lang/String;

    .line 1162
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasCriticReviewsUrl:Z

    goto :goto_0

    .line 1139
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1058
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v0, :cond_0

    .line 1104
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1106
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasUserReviewsUrl:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->userReviewsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1107
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->userReviewsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1109
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->hasCriticReviewsUrl:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->criticReviewsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1110
    :cond_3
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Details$DiscoveryBadgeLink;->criticReviewsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1112
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1113
    return-void
.end method
