.class public Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;
.super Landroid/widget/RelativeLayout;
.source "RottenTomatoesReviewsHeader.java"


# instance fields
.field private mFavorablePercentBar:Lcom/google/android/finsky/layout/RottenTomatoesMeter;

.field private mFavorablePercentValue:Landroid/widget/TextView;

.field private final mHalfSeparatorThickness:I

.field private mReviewsCount:Landroid/widget/TextView;

.field private mSentimentImage:Lcom/google/android/play/image/FifeImageView;

.field private final mSeparatorInset:I

.field private final mSeparatorPaint:Landroid/graphics/Paint;

.field private mSource:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 46
    .local v0, "res":Landroid/content/res/Resources;
    const v2, 0x7f0b0052

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 48
    .local v1, "separatorThickness":I
    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mHalfSeparatorThickness:I

    .line 49
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSeparatorPaint:Landroid/graphics/Paint;

    .line 50
    iget-object v2, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSeparatorPaint:Landroid/graphics/Paint;

    const v3, 0x7f090053

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 51
    iget-object v2, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSeparatorPaint:Landroid/graphics/Paint;

    int-to-float v3, v1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 52
    const v2, 0x7f0b00ec

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSeparatorInset:I

    .line 54
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->setWillNotDraw(Z)V

    .line 55
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 6
    .param p1, "reviewsResponse"    # Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    const/4 v5, 0x0

    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mTitle:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSentimentImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v1, p1, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->aggregateSentiment:Lcom/google/android/finsky/protos/Common$Image;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->aggregateSentiment:Lcom/google/android/finsky/protos/Common$Image;

    iget-boolean v2, v2, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v0, v1, v2, p4}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mFavorablePercentValue:Landroid/widget/TextView;

    iget v1, p1, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->percentFavorable:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-boolean v0, p1, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasTotalNumReviews:Z

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mReviewsCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c024f

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p1, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->totalNumReviews:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mReviewsCount:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 87
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mFavorablePercentBar:Lcom/google/android/finsky/layout/RottenTomatoesMeter;

    iget v1, p1, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->percentFavorable:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->setPercentValue(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSource:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->sourceText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p1, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->source:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSource:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader$1;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader$1;-><init>(Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;Lcom/google/android/finsky/api/model/DfeToc;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    :goto_1
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mReviewsCount:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSource:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mHalfSeparatorThickness:I

    sub-int v6, v0, v1

    .line 108
    .local v6, "bottomY":I
    iget v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSeparatorInset:I

    int-to-float v1, v0

    int-to-float v2, v6

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->getWidth()I

    move-result v0

    iget v3, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSeparatorInset:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSeparatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 110
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 61
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mTitle:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f0a036a

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSentimentImage:Lcom/google/android/play/image/FifeImageView;

    .line 63
    const v0, 0x7f0a036d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mFavorablePercentValue:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0a036f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mReviewsCount:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0a0370

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mFavorablePercentBar:Lcom/google/android/finsky/layout/RottenTomatoesMeter;

    .line 66
    const v0, 0x7f0a0371

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->mSource:Landroid/widget/TextView;

    .line 67
    return-void
.end method
