.class final Lcom/google/android/finsky/utils/RateReviewHelper$3;
.super Ljava/lang/Object;
.source "RateReviewHelper.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/RateReviewHelper;->updateReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Rev$ReviewResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

.field final synthetic val$docDetailsUrl:Ljava/lang/String;

.field final synthetic val$finalContent:Ljava/lang/String;

.field final synthetic val$finalTitle:Ljava/lang/String;

.field final synthetic val$rateListener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

.field final synthetic val$rating:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iput-object p2, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$docDetailsUrl:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$rateListener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    iput p4, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$rating:I

    iput-object p5, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$finalTitle:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$finalContent:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/Rev$ReviewResponse;)V
    .locals 4
    .param p1, "response"    # Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v1, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$docDetailsUrl:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->invalidateDetailsCache(Ljava/lang/String;Z)V

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-static {v0}, Lcom/google/android/finsky/utils/MyPeoplePageHelper;->onMutationOccurred(Lcom/google/android/finsky/api/DfeApi;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$rateListener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$rateListener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    iget v1, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$rating:I

    iget-object v2, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$finalTitle:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/utils/RateReviewHelper$3;->val$finalContent:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;->onRateReviewCommitted(ILjava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 147
    check-cast p1, Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/RateReviewHelper$3;->onResponse(Lcom/google/android/finsky/protos/Rev$ReviewResponse;)V

    return-void
.end method
