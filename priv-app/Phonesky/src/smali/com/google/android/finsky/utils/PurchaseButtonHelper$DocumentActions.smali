.class public Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;
.super Ljava/lang/Object;
.source "PurchaseButtonHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/PurchaseButtonHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DocumentActions"
.end annotation


# instance fields
.field public actionCount:I

.field public final actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

.field public backend:I

.field public displayAsOwned:Z

.field public listingOfferFullText:Ljava/lang/String;

.field public listingOfferText:Ljava/lang/String;

.field public status:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    new-array v1, v3, [Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    iput-object v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    .line 161
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 162
    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    new-instance v2, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    invoke-direct {v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;-><init>()V

    aput-object v2, v1, v0

    .line 161
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 164
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->reset()V

    .line 165
    return-void
.end method

.method private addAction(ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V
    .locals 4
    .param p1, "actionType"    # I
    .param p2, "offerText"    # Ljava/lang/String;
    .param p3, "offerFullText"    # Ljava/lang/String;
    .param p4, "offerType"    # I
    .param p5, "offerFilter"    # Lcom/google/android/finsky/utils/DocUtils$OfferFilter;
    .param p6, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p7, "account"    # Landroid/accounts/Account;

    .prologue
    .line 201
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    iget v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->reset()V

    .line 203
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    iget v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    aget-object v0, v0, v1

    iput p1, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    .line 204
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    iget v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    aget-object v0, v0, v1

    iput-object p2, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerText:Ljava/lang/String;

    .line 205
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    iget v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    aget-object v0, v0, v1

    iput-object p3, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerFullText:Ljava/lang/String;

    .line 206
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    iget v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    aget-object v0, v0, v1

    iput p4, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerType:I

    .line 207
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    iget v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    aget-object v0, v0, v1

    iput-object p5, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    .line 208
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    iget v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    aget-object v0, v0, v1

    iput-object p6, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->document:Lcom/google/android/finsky/api/model/Document;

    .line 209
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    iget v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    aget-object v0, v0, v1

    iput-object p7, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->account:Landroid/accounts/Account;

    .line 210
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    .line 219
    :goto_0
    return-void

    .line 217
    :cond_0
    const-string v0, "Trying to add action %d but no more room for actions"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public addAction(ILcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V
    .locals 8
    .param p1, "actionType"    # I
    .param p2, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v2, 0x0

    .line 246
    const/4 v4, -0x1

    move-object v0, p0

    move v1, p1

    move-object v3, v2

    move-object v5, v2

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 247
    return-void
.end method

.method public addActionFromOfferList(I[Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)I
    .locals 20
    .param p1, "actionType"    # I
    .param p2, "offers"    # [Lcom/google/android/finsky/protos/Common$Offer;
    .param p3, "offerFilter"    # Lcom/google/android/finsky/utils/DocUtils$OfferFilter;
    .param p4, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "account"    # Landroid/accounts/Account;

    .prologue
    .line 259
    const/16 v17, 0x0

    .line 260
    .local v17, "numMatches":I
    const/16 v19, 0x0

    .line 261
    .local v19, "offerMatch":Lcom/google/android/finsky/protos/Common$Offer;
    move-object/from16 v13, p2

    .local v13, "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    array-length v0, v13

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_0
    move/from16 v0, v16

    if-ge v15, v0, :cond_1

    aget-object v18, v13, v15

    .line 262
    .local v18, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    move-object/from16 v0, v18

    iget v5, v0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->matches(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 263
    move-object/from16 v19, v18

    .line 264
    add-int/lit8 v17, v17, 0x1

    .line 261
    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 267
    .end local v18    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_1
    if-nez v17, :cond_2

    .line 268
    const/16 v17, 0x0

    .line 279
    .end local v17    # "numMatches":I
    :goto_1
    return v17

    .line 269
    .restart local v17    # "numMatches":I
    :cond_2
    const/4 v5, 0x1

    move/from16 v0, v17

    if-ne v0, v5, :cond_3

    .line 270
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addOfferAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 271
    const/16 v17, 0x1

    goto :goto_1

    .line 273
    :cond_3
    const/4 v5, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v5, v1}, Lcom/google/android/finsky/utils/DocUtils;->getLowestPricedOffer([Lcom/google/android/finsky/protos/Common$Offer;ZLcom/google/android/finsky/utils/DocUtils$OfferFilter;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v14

    .line 274
    .local v14, "displayOffer":Lcom/google/android/finsky/protos/Common$Offer;
    iget-object v7, v14, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper;->shouldAddFullText(Lcom/google/android/finsky/protos/Common$Offer;)Z
    invoke-static {v14}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->access$000(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v8, v14, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    :goto_2
    const/4 v9, 0x0

    move-object/from16 v5, p0

    move/from16 v6, p1

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    invoke-direct/range {v5 .. v12}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_1

    :cond_4
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public addDisambiguationAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V
    .locals 8
    .param p1, "actionType"    # I
    .param p2, "offer"    # Lcom/google/android/finsky/protos/Common$Offer;
    .param p3, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p4, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v5, 0x0

    .line 237
    iget-object v2, p2, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper;->shouldAddFullText(Lcom/google/android/finsky/protos/Common$Offer;)Z
    invoke-static {p2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->access$000(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v3, p2, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    :goto_0
    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 240
    return-void

    :cond_0
    move-object v3, v5

    .line 237
    goto :goto_0
.end method

.method public addOfferAction(ILcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V
    .locals 8
    .param p1, "actionType"    # I
    .param p2, "offer"    # Lcom/google/android/finsky/protos/Common$Offer;
    .param p3, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p4, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v5, 0x0

    .line 226
    iget-object v2, p2, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper;->shouldAddFullText(Lcom/google/android/finsky/protos/Common$Offer;)Z
    invoke-static {p2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->access$000(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v3, p2, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    :goto_0
    iget v4, p2, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    move-object v0, p0

    move v1, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->addAction(ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 229
    return-void

    :cond_0
    move-object v3, v5

    .line 226
    goto :goto_0
.end method

.method public getAction(I)Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 284
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    if-ge p1, v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    aget-object v0, v0, p1

    .line 288
    :goto_0
    return-object v0

    .line 287
    :cond_0
    const-string v0, "Request for invalid action #%d (%d available actions)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAction()Z
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStatus()Z
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 171
    iput-boolean v2, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->displayAsOwned:Z

    .line 172
    iput v2, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->status:I

    .line 173
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    .line 174
    iput v2, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    .line 175
    iput-object v3, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->listingOfferText:Ljava/lang/String;

    .line 176
    iput-object v3, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->listingOfferFullText:Ljava/lang/String;

    .line 177
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 178
    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->reset()V

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 294
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    if-ge v0, v2, :cond_1

    .line 295
    if-eqz v0, :cond_0

    .line 296
    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 298
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actions:[Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 294
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 300
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
