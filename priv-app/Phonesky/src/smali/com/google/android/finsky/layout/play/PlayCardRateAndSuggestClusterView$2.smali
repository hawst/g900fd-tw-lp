.class Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$2;
.super Ljava/lang/Object;
.source "PlayCardRateAndSuggestClusterView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$2;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRate(IZ)V
    .locals 3
    .param p1, "rating"    # I
    .param p2, "isCommitted"    # Z

    .prologue
    const/4 v2, 0x0

    .line 371
    if-eqz p2, :cond_0

    .line 372
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$2;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    # invokes: Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->syncIndexOfItemToRate(Z)V
    invoke-static {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->access$100(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;Z)V

    .line 373
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$2;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    # invokes: Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->syncState(Z)V
    invoke-static {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->access$200(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;Z)V

    .line 377
    :goto_0
    return-void

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$2;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->setState(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->access$000(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;IZ)V

    goto :goto_0
.end method

.method public onRateCleared()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 381
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$2;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    # invokes: Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->setState(IZ)V
    invoke-static {v0, v1, v1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->access$000(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;IZ)V

    .line 382
    return-void
.end method
