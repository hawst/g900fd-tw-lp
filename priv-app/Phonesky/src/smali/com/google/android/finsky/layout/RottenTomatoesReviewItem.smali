.class public Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;
.super Landroid/widget/RelativeLayout;
.source "RottenTomatoesReviewItem.java"


# instance fields
.field private mAuthor:Landroid/widget/TextView;

.field private mComment:Landroid/widget/TextView;

.field private mExternalLinkAction:Landroid/view/View;

.field private mSentimentImage:Lcom/google/android/play/image/FifeImageView;

.field private mSource:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/protos/DocumentV2$Review;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 3
    .param p1, "review"    # Lcom/google/android/finsky/protos/DocumentV2$Review;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mSentimentImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v1, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->sentiment:Lcom/google/android/finsky/protos/Common$Image;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->sentiment:Lcom/google/android/finsky/protos/Common$Image;

    iget-boolean v2, v2, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v0, v1, v2, p3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 54
    iget-object v0, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mExternalLinkAction:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 68
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mComment:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mAuthor:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->authorName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mSource:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mExternalLinkAction:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 58
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mExternalLinkAction:Landroid/view/View;

    new-instance v1, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem$1;-><init>(Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;Lcom/google/android/finsky/protos/DocumentV2$Review;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 42
    const v0, 0x7f0a036a

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mSentimentImage:Lcom/google/android/play/image/FifeImageView;

    .line 43
    const v0, 0x7f0a036b

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mExternalLinkAction:Landroid/view/View;

    .line 44
    const v0, 0x7f0a036c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mComment:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f0a0356

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mAuthor:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f0a0359

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->mSource:Landroid/widget/TextView;

    .line 47
    return-void
.end method
