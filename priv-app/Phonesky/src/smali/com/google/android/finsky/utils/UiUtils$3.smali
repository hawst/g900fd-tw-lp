.class final Lcom/google/android/finsky/utils/UiUtils$3;
.super Ljava/lang/Object;
.source "UiUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/UiUtils;->fadeOutCluster(Landroid/view/View;Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cluster:Landroid/view/View;

.field final synthetic val$listener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/google/android/finsky/utils/UiUtils$3;->val$cluster:Landroid/view/View;

    iput-object p2, p0, Lcom/google/android/finsky/utils/UiUtils$3;->val$listener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 342
    iget-object v1, p0, Lcom/google/android/finsky/utils/UiUtils$3;->val$cluster:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-wide/16 v2, 0x96

    new-instance v4, Lcom/google/android/finsky/utils/UiUtils$3$1;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/utils/UiUtils$3$1;-><init>(Lcom/google/android/finsky/utils/UiUtils$3;)V

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 352
    .local v0, "fadeOutAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/google/android/finsky/utils/UiUtils$3;->val$cluster:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 353
    return-void
.end method
