.class public Lcom/google/android/finsky/adapters/ContainerViewSpinnerAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ContainerViewSpinnerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/finsky/protos/Containers$ContainerView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lcom/google/android/finsky/protos/Containers$ContainerView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "containerViews"    # [Lcom/google/android/finsky/protos/Containers$ContainerView;

    .prologue
    .line 19
    const v0, 0x1090009

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 20
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 40
    if-nez p2, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/ContainerViewSpinnerAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x1090009

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 45
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/ContainerViewSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Containers$ContainerView;

    .line 46
    .local v0, "item":Lcom/google/android/finsky/protos/Containers$ContainerView;
    const v2, 0x1020014

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 47
    .local v1, "textView":Landroid/widget/TextView;
    iget-object v2, v0, Lcom/google/android/finsky/protos/Containers$ContainerView;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    return-object p2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/ContainerViewSpinnerAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040198

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 29
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/ContainerViewSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Containers$ContainerView;

    .local v0, "item":Lcom/google/android/finsky/protos/Containers$ContainerView;
    move-object v1, p2

    .line 31
    check-cast v1, Landroid/widget/TextView;

    .line 32
    .local v1, "spinnerText":Landroid/widget/TextView;
    iget-object v2, v0, Lcom/google/android/finsky/protos/Containers$ContainerView;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    return-object p2
.end method
