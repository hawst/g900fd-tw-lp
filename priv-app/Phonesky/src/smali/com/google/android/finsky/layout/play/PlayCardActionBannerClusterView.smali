.class public Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterView;
.source "PlayCardActionBannerClusterView.java"


# instance fields
.field private final mContentVerticalMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 28
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b0153

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;->mContentVerticalMargin:I

    .line 29
    return-void
.end method


# virtual methods
.method public configureExtraContent(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$DocV2;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/view/View$OnClickListener;)V
    .locals 10
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p4, "primaryPerson"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p5, "secondaryPersons"    # [Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p6, "buttonText"    # Ljava/lang/String;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p8, "exploreClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;

    .line 46
    .local v0, "actionBannerContent":Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 48
    .local v9, "contentLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;->mContentVerticalMargin:I

    iput v1, v9, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 49
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;->mContentVerticalMargin:I

    iput v1, v9, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    .line 51
    invoke-virtual/range {v0 .. v8}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterViewContent;->configureExtraContent(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$DocV2;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/view/View$OnClickListener;)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;->logEmptyClusterImpression()V

    .line 57
    return-void
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 34
    const/16 v0, 0x19e

    return v0
.end method
