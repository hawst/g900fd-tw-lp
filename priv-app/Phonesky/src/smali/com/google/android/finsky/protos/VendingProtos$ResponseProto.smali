.class public final Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResponseProto"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    }
.end annotation


# instance fields
.field public pendingNotifications:Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

.field public response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5657
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5658
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;

    .line 5659
    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 5752
    new-instance v0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;
    .locals 1

    .prologue
    .line 5662
    invoke-static {}, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    .line 5663
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->pendingNotifications:Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    .line 5664
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->cachedSize:I

    .line 5665
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 5687
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 5688
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 5689
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 5690
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    aget-object v0, v3, v1

    .line 5691
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    if-eqz v0, :cond_0

    .line 5692
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5689
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5697
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    .end local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->pendingNotifications:Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    if-eqz v3, :cond_2

    .line 5698
    const/16 v3, 0x26

    iget-object v4, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->pendingNotifications:Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5701
    :cond_2
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 5709
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 5710
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 5714
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 5715
    :sswitch_0
    return-object p0

    .line 5720
    :sswitch_1
    const/16 v5, 0xb

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 5722
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    if-nez v5, :cond_2

    move v1, v4

    .line 5723
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    .line 5725
    .local v2, "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    if-eqz v1, :cond_1

    .line 5726
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5728
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 5729
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;-><init>()V

    aput-object v5, v2, v1

    .line 5730
    aget-object v5, v2, v1

    invoke-virtual {p1, v5, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 5731
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 5728
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5722
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    array-length v1, v5

    goto :goto_1

    .line 5734
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;-><init>()V

    aput-object v5, v2, v1

    .line 5735
    aget-object v5, v2, v1

    invoke-virtual {p1, v5, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 5736
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    goto :goto_0

    .line 5740
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->pendingNotifications:Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    if-nez v5, :cond_4

    .line 5741
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->pendingNotifications:Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    .line 5743
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->pendingNotifications:Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5710
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb -> :sswitch_1
        0x132 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5349
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5671
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 5672
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 5673
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->response:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    aget-object v0, v2, v1

    .line 5674
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    if-eqz v0, :cond_0

    .line 5675
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5672
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5679
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->pendingNotifications:Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    if-eqz v2, :cond_2

    .line 5680
    const/16 v2, 0x26

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;->pendingNotifications:Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5682
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 5683
    return-void
.end method
