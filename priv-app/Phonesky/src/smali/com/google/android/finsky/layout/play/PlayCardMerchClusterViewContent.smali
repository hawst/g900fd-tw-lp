.class public Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;
.source "PlayCardMerchClusterViewContent.java"

# interfaces
.implements Lcom/google/android/finsky/adapters/Recyclable;
.implements Lcom/google/android/play/image/FifeImageView$OnLoadedListener;


# instance fields
.field private final mFallbackMerchColor:I

.field private mMerchColor:I

.field private mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

.field private mMerchImagePosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mFallbackMerchColor:I

    .line 47
    return-void
.end method

.method private clearImageFadingEdge()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->clearFadingEdges()V

    .line 103
    return-void
.end method

.method private configureImageFadingEdge()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 93
    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImagePosition:I

    if-ne v4, v2, :cond_0

    move v0, v2

    .line 95
    .local v0, "isFadingLeftEdge":Z
    :goto_0
    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImagePosition:I

    if-nez v4, :cond_1

    move v1, v2

    .line 97
    .local v1, "isFadingRightEdge":Z
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x4

    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchColor:I

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->configureFadingEdges(ZZII)V

    .line 99
    return-void

    .end local v0    # "isFadingLeftEdge":Z
    .end local v1    # "isFadingRightEdge":Z
    :cond_0
    move v0, v3

    .line 93
    goto :goto_0

    .restart local v0    # "isFadingLeftEdge":Z
    :cond_1
    move v1, v3

    .line 95
    goto :goto_1
.end method


# virtual methods
.method public configureMerch(Lcom/google/android/play/image/BitmapLoader;ILcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 4
    .param p1, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p2, "merchImagePosition"    # I
    .param p3, "merchImage"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p4, "clusterTitle"    # Ljava/lang/String;
    .param p5, "merchClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v0, 0x1

    .line 66
    if-eqz p2, :cond_0

    if-eq p2, v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Merch image position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mFallbackMerchColor:I

    invoke-static {p3, v1}, Lcom/google/android/finsky/utils/UiUtils;->getFillColor(Lcom/google/android/finsky/protos/Common$Image;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchColor:I

    .line 74
    iput p2, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImagePosition:I

    .line 76
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->setOnLoadedListener(Lcom/google/android/play/image/FifeImageView$OnLoadedListener;)V

    .line 77
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    iget-object v2, p3, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v3, p3, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 78
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 81
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->configureImageFadingEdge()V

    .line 86
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v1, p5}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    if-eqz p5, :cond_2

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->setClickable(Z)V

    .line 88
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    if-eqz p5, :cond_3

    .end local p4    # "clusterTitle":Ljava/lang/String;
    :goto_2
    invoke-virtual {v0, p4}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 89
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchColor:I

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->setBackgroundColor(I)V

    .line 90
    return-void

    .line 83
    .restart local p4    # "clusterTitle":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->clearImageFadingEdge()V

    goto :goto_0

    .line 87
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 88
    :cond_3
    const/4 p4, 0x0

    goto :goto_2
.end method

.method protected getIndexOfFirstCard()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onFinishInflate()V

    .line 53
    const v0, 0x7f0a02cb

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/FadingEdgeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    .line 54
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 12
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 132
    invoke-super/range {p0 .. p5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onLayout(ZIIII)V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->getWidth()I

    move-result v8

    .line 135
    .local v8, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->getHeight()I

    move-result v0

    .line 137
    .local v0, "height":I
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->getMeasuredWidth()I

    move-result v4

    .line 138
    .local v4, "merchImageWidth":I
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->getMeasuredHeight()I

    move-result v3

    .line 139
    .local v3, "merchImageHeight":I
    if-lez v4, :cond_2

    .line 140
    mul-int/lit8 v9, v3, 0x3

    div-int/lit8 v1, v9, 0x4

    .line 141
    .local v1, "imageFocalPointX":I
    iget v9, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImagePosition:I

    if-nez v9, :cond_0

    .line 142
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->getLeadingGap(I)I

    move-result v2

    .line 143
    .local v2, "leadingGap":I
    div-int/lit8 v9, v2, 0x2

    sub-int v5, v9, v1

    .line 145
    .local v5, "offset":I
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    const/4 v10, 0x0

    add-int v11, v5, v4

    invoke-virtual {v9, v5, v10, v11, v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->layout(IIII)V

    .line 160
    .end local v1    # "imageFocalPointX":I
    .end local v2    # "leadingGap":I
    .end local v5    # "offset":I
    :goto_0
    return-void

    .line 147
    .restart local v1    # "imageFocalPointX":I
    :cond_0
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->getTrailingGap(I)I

    move-result v7

    .line 148
    .local v7, "trailingGap":I
    div-int/lit8 v9, v7, 0x2

    sub-int v9, v8, v9

    sub-int v5, v9, v1

    .line 149
    .restart local v5    # "offset":I
    add-int v6, v5, v4

    .line 150
    .local v6, "rightEdge":I
    if-ge v6, v8, :cond_1

    .line 152
    sub-int v9, v8, v6

    add-int/2addr v5, v9

    .line 155
    :cond_1
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    const/4 v10, 0x0

    add-int v11, v5, v4

    invoke-virtual {v9, v5, v10, v11, v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->layout(IIII)V

    goto :goto_0

    .line 158
    .end local v1    # "imageFocalPointX":I
    .end local v5    # "offset":I
    .end local v6    # "rightEdge":I
    .end local v7    # "trailingGap":I
    :cond_2
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11, v4, v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->layout(IIII)V

    goto :goto_0
.end method

.method public onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->configureImageFadingEdge()V

    .line 108
    return-void
.end method

.method public onLoadedAndFadedIn(Lcom/google/android/play/image/FifeImageView;)V
    .locals 0
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;

    .prologue
    .line 112
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 116
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onMeasure(II)V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->getMeasuredHeight()I

    move-result v0

    .line 120
    .local v0, "height":I
    const v2, 0x3fe38e39

    int-to-float v3, v0

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 121
    .local v1, "merchImageWidth":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->measure(II)V

    .line 123
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->isLoaded()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->configureImageFadingEdge()V

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->clearImageFadingEdge()V

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->clearImage()V

    .line 165
    return-void
.end method
