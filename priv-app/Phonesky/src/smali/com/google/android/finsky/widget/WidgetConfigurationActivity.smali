.class public Lcom/google/android/finsky/widget/WidgetConfigurationActivity;
.super Landroid/app/Activity;
.source "WidgetConfigurationActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/widget/WidgetConfigurationActivity$Holder;,
        Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 166
    return-void
.end method

.method private getCorpusName(I)Ljava/lang/String;
    .locals 3
    .param p1, "backend"    # I

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "name_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v12, 0x7f0401d4

    invoke-virtual {p0, v12}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity;->setContentView(I)V

    .line 50
    const/4 v12, 0x0

    invoke-virtual {p0, v12}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity;->setResult(I)V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 53
    .local v8, "intent":Landroid/content/Intent;
    const-string v12, "dialog_title"

    const v13, 0x7f0c01ac

    invoke-virtual {v8, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    invoke-virtual {p0, v12}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity;->setTitle(I)V

    .line 55
    const v12, 0x7f0a00b8

    invoke-virtual {p0, v12}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/GridView;

    .line 57
    .local v3, "container":Landroid/widget/GridView;
    const-string v12, "dfeToc"

    invoke-virtual {v8, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v11

    check-cast v11, Lcom/google/android/finsky/api/model/DfeToc;

    .line 58
    .local v11, "toc":Lcom/google/android/finsky/api/model/DfeToc;
    invoke-virtual {v11}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpusList()Ljava/util/List;

    move-result-object v5

    .line 60
    .local v5, "corpusList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Toc$CorpusMetadata;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v12}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v6

    .line 62
    .local v6, "enabledCorpora":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Toc$CorpusMetadata;>;"
    const-string v12, "enableMultiCorpus"

    const/4 v13, 0x1

    invoke-virtual {v8, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 63
    new-instance v1, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;-><init>()V

    .line 64
    .local v1, "all":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    const/4 v12, 0x0

    iput v12, v1, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    .line 65
    const/4 v12, 0x1

    iput-boolean v12, v1, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasBackend:Z

    .line 66
    const/4 v12, 0x0

    invoke-direct {p0, v12}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity;->getCorpusName(I)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v1, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    .line 67
    const/4 v12, 0x1

    iput-boolean v12, v1, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasName:Z

    .line 68
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    .end local v1    # "all":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    :cond_0
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    .line 72
    .local v4, "corpus":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "backend_"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v4, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 73
    .local v9, "key":Ljava/lang/String;
    const/4 v12, 0x1

    invoke-virtual {v8, v9, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 74
    iget v12, v4, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    invoke-direct {p0, v12}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity;->getCorpusName(I)Ljava/lang/String;

    move-result-object v10

    .line 75
    .local v10, "name":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 76
    iput-object v10, v4, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    .line 77
    const/4 v12, 0x1

    iput-boolean v12, v4, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasName:Z

    .line 79
    :cond_2
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 83
    .end local v4    # "corpus":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    .end local v9    # "key":Ljava/lang/String;
    .end local v10    # "name":Ljava/lang/String;
    :cond_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v12

    const/4 v13, 0x3

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v12

    invoke-virtual {v3, v12}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 85
    const-string v12, "appWidgetId"

    const/4 v13, -0x1

    invoke-virtual {v8, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 86
    .local v2, "appWidgetId":I
    new-instance v0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;

    invoke-direct {v0, p0, v6, v2}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;I)V

    .line 87
    .local v0, "adapter":Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;
    invoke-virtual {v3, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 88
    return-void
.end method
