.class public final Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ResponseMessages.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ResponseMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreFetch"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;


# instance fields
.field public etag:Ljava/lang/String;

.field public hasEtag:Z

.field public hasResponse:Z

.field public hasSoftTtl:Z

.field public hasTtl:Z

.field public hasUrl:Z

.field public response:[B

.field public softTtl:J

.field public ttl:J

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->clear()Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    .line 170
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    .locals 2

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->_emptyArray:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    if-nez v0, :cond_1

    .line 138
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 140
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->_emptyArray:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    sput-object v0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->_emptyArray:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    .line 143
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->_emptyArray:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    return-object v0

    .line 143
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 173
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->url:Ljava/lang/String;

    .line 174
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasUrl:Z

    .line 175
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->response:[B

    .line 176
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasResponse:Z

    .line 177
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->etag:Ljava/lang/String;

    .line 178
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasEtag:Z

    .line 179
    iput-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->ttl:J

    .line 180
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasTtl:Z

    .line 181
    iput-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->softTtl:J

    .line 182
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasSoftTtl:Z

    .line 183
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->cachedSize:I

    .line 184
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 210
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 211
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->url:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 212
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasResponse:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->response:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_3

    .line 216
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->response:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasEtag:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->etag:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 220
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->etag:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasTtl:Z

    if-nez v1, :cond_6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->ttl:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 224
    :cond_6
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->ttl:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasSoftTtl:Z

    if-nez v1, :cond_8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->softTtl:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 228
    :cond_8
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->softTtl:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 239
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 240
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 244
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 245
    :sswitch_0
    return-object p0

    .line 250
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->url:Ljava/lang/String;

    .line 251
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasUrl:Z

    goto :goto_0

    .line 255
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->response:[B

    .line 256
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasResponse:Z

    goto :goto_0

    .line 260
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->etag:Ljava/lang/String;

    .line 261
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasEtag:Z

    goto :goto_0

    .line 265
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->ttl:J

    .line 266
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasTtl:Z

    goto :goto_0

    .line 270
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->softTtl:J

    .line 271
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasSoftTtl:Z

    goto :goto_0

    .line 240
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 190
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->url:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 191
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->url:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 193
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasResponse:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->response:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_3

    .line 194
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->response:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 196
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasEtag:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->etag:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 197
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->etag:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 199
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasTtl:Z

    if-nez v0, :cond_6

    iget-wide v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->ttl:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 200
    :cond_6
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->ttl:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 202
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->hasSoftTtl:Z

    if-nez v0, :cond_8

    iget-wide v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->softTtl:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    .line 203
    :cond_8
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->softTtl:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 205
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 206
    return-void
.end method
