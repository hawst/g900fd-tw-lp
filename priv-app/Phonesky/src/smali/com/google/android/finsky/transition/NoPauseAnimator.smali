.class public Lcom/google/android/finsky/transition/NoPauseAnimator;
.super Landroid/animation/Animator;
.source "NoPauseAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;
    }
.end annotation


# instance fields
.field private final mAnimator:Landroid/animation/Animator;

.field private final mListeners:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap",
            "<",
            "Landroid/animation/Animator$AnimatorListener;",
            "Landroid/animation/Animator$AnimatorListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/animation/Animator;-><init>()V

    .line 24
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mListeners:Landroid/util/ArrayMap;

    .line 28
    iput-object p1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    .line 29
    return-void
.end method


# virtual methods
.method public addListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/transition/NoPauseAnimator$AnimatorListenerWrapper;-><init>(Landroid/animation/Animator;Landroid/animation/Animator$AnimatorListener;)V

    .line 34
    .local v0, "wrapper":Landroid/animation/Animator$AnimatorListener;
    iget-object v1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mListeners:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 35
    iget-object v1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mListeners:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iget-object v1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 38
    :cond_0
    return-void
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 43
    return-void
.end method

.method public end()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 48
    return-void
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->getDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getInterpolator()Landroid/animation/TimeInterpolator;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v0

    return-object v0
.end method

.method public getListeners()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator$AnimatorListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mListeners:Landroid/util/ArrayMap;

    invoke-virtual {v1}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getStartDelay()J
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->getStartDelay()J

    move-result-wide v0

    return-wide v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isPaused()Z

    move-result v0

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    return v0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    return v0
.end method

.method public removeAllListeners()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mListeners:Landroid/util/ArrayMap;

    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V

    .line 88
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->removeAllListeners()V

    .line 89
    return-void
.end method

.method public removeListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 93
    iget-object v1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mListeners:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 94
    .local v0, "wrapper":Landroid/animation/Animator$AnimatorListener;
    if-eqz v0, :cond_0

    .line 95
    iget-object v1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mListeners:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    iget-object v1, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v1, v0}, Landroid/animation/Animator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 98
    :cond_0
    return-void
.end method

.method public setDuration(J)Landroid/animation/Animator;
    .locals 1
    .param p1, "durationMS"    # J

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 103
    return-object p0
.end method

.method public setInterpolator(Landroid/animation/TimeInterpolator;)V
    .locals 1
    .param p1, "timeInterpolator"    # Landroid/animation/TimeInterpolator;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, p1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 109
    return-void
.end method

.method public setStartDelay(J)V
    .locals 1
    .param p1, "delayMS"    # J

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 114
    return-void
.end method

.method public setTarget(Ljava/lang/Object;)V
    .locals 1
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, p1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 119
    return-void
.end method

.method public setupEndValues()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->setupEndValues()V

    .line 124
    return-void
.end method

.method public setupStartValues()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->setupStartValues()V

    .line 129
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/transition/NoPauseAnimator;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 134
    return-void
.end method
