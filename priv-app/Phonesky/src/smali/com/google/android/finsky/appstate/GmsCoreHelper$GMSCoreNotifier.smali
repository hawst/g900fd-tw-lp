.class public Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;
.super Ljava/lang/Object;
.source "GmsCoreHelper.java"

# interfaces
.implements Lcom/google/android/finsky/receivers/PackageMonitorReceiver$PackageStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/appstate/GmsCoreHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GMSCoreNotifier"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228
    iput-object p1, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;->mContext:Landroid/content/Context;

    .line 229
    return-void
.end method

.method private static reconnectNlp(Landroid/content/Context;Ljava/lang/String;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 309
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v6, Lcom/google/android/finsky/config/G;->nlpReinstallSdkVersionMin:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ge v7, v6, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v6, Lcom/google/android/finsky/config/G;->nlpReinstallSdkVersionMax:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-gt v7, v6, :cond_0

    .line 316
    # getter for: Lcom/google/android/finsky/appstate/GmsCoreHelper;->NLP_PACKAGE_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->access$000()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 320
    # getter for: Lcom/google/android/finsky/appstate/GmsCoreHelper;->NLP_SHARED_USER_ID:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->access$100()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 326
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 328
    .local v4, "packageInfo":Landroid/content/pm/PackageInfo;
    # getter for: Lcom/google/android/finsky/appstate/GmsCoreHelper;->NLP_SHARED_USER_ID:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->access$100()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v4, Landroid/content/pm/PackageInfo;->sharedUserId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v6

    if-eqz v6, :cond_0

    .line 336
    const-string v6, "Found shared UID match between NLP and %s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object p1, v7, v9

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 341
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    # getter for: Lcom/google/android/finsky/appstate/GmsCoreHelper;->NLP_PACKAGE_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->access$000()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 343
    .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    new-instance v0, Ljava/io/File;

    iget-object v6, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 344
    .local v0, "apkFile":Ljava/io/File;
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 346
    .local v5, "source":Landroid/net/Uri;
    new-instance v3, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier$2;

    invoke-direct {v3}, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier$2;-><init>()V

    .line 356
    .local v3, "observer":Lcom/google/android/finsky/utils/PackageManagerUtils$PackageInstallObserver;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    const/4 v7, 0x2

    invoke-static {v6, v5, v3, v7}, Lcom/google/android/finsky/utils/PackageManagerUtils;->installPackage(Landroid/content/Context;Landroid/net/Uri;Lcom/google/android/finsky/utils/PackageManagerUtils$PackageInstallObserver;I)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 358
    .end local v0    # "apkFile":Ljava/io/File;
    .end local v1    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "observer":Lcom/google/android/finsky/utils/PackageManagerUtils$PackageInstallObserver;
    .end local v5    # "source":Landroid/net/Uri;
    :catch_0
    move-exception v2

    .line 359
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NameNotFoundException getting info for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # getter for: Lcom/google/android/finsky/appstate/GmsCoreHelper;->NLP_PACKAGE_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->access$000()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 331
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_1
    move-exception v2

    .line 332
    .restart local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "NameNotFoundException getting info for %s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object p1, v7, v9

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private setAutoUpdate(Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "newState"    # Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;
    .param p3, "reason"    # Ljava/lang/String;

    .prologue
    .line 287
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v5

    .line 288
    .local v5, "appStates":Lcom/google/android/finsky/appstate/AppStates;
    new-instance v0, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier$1;-><init>(Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;)V

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    .line 296
    return-void
.end method


# virtual methods
.method public onPackageAdded(Ljava/lang/String;)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 233
    invoke-static {p1}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->isGmsCore(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 235
    new-instance v3, Landroid/content/Intent;

    const-string v7, "com.google.android.gms.GMS_UPDATED"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 236
    .local v3, "gmsInstalledIntent":Landroid/content/Intent;
    const-string v7, "com.google.android.gms"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    iget-object v7, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 245
    :try_start_0
    iget-object v7, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, p1, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 250
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget v2, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    .line 251
    .local v2, "flags":I
    and-int/lit8 v7, v2, 0x1

    if-eqz v7, :cond_1

    move v4, v5

    .line 252
    .local v4, "isSystem":Z
    :goto_0
    and-int/lit16 v7, v2, 0x80

    if-eqz v7, :cond_2

    .line 253
    .local v5, "isUpdatedSystem":Z
    :goto_1
    if-eqz v4, :cond_3

    if-nez v5, :cond_3

    .line 256
    sget-object v6, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->DISABLED:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    const-string v7, "downgrade"

    invoke-direct {p0, p1, v6, v7}, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;->setAutoUpdate(Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;Ljava/lang/String;)V

    .line 263
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "flags":I
    .end local v3    # "gmsInstalledIntent":Landroid/content/Intent;
    .end local v4    # "isSystem":Z
    .end local v5    # "isUpdatedSystem":Z
    :cond_0
    :goto_2
    iget-object v6, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;->mContext:Landroid/content/Context;

    invoke-static {v6, p1}, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;->reconnectNlp(Landroid/content/Context;Ljava/lang/String;)V

    .line 264
    :goto_3
    return-void

    .line 246
    .restart local v3    # "gmsInstalledIntent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 247
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, "NameNotFoundException getting info for %s"

    new-array v8, v5, [Ljava/lang/Object;

    aput-object p1, v8, v6

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v2    # "flags":I
    :cond_1
    move v4, v6

    .line 251
    goto :goto_0

    .restart local v4    # "isSystem":Z
    :cond_2
    move v5, v6

    .line 252
    goto :goto_1

    .line 259
    .restart local v5    # "isUpdatedSystem":Z
    :cond_3
    sget-object v6, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->USE_GLOBAL:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    const-string v7, "install/update"

    invoke-direct {p0, p1, v6, v7}, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;->setAutoUpdate(Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onPackageAvailabilityChanged([Ljava/lang/String;Z)V
    .locals 0
    .param p1, "packageNames"    # [Ljava/lang/String;
    .param p2, "available"    # Z

    .prologue
    .line 280
    return-void
.end method

.method public onPackageChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;->reconnectNlp(Landroid/content/Context;Ljava/lang/String;)V

    .line 277
    return-void
.end method

.method public onPackageFirstLaunch(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 283
    return-void
.end method

.method public onPackageRemoved(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "replacing"    # Z

    .prologue
    .line 268
    if-nez p2, :cond_0

    invoke-static {p1}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->isGmsCore(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    sget-object v0, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->DISABLED:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    const-string v1, "removed"

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;->setAutoUpdate(Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;Ljava/lang/String;)V

    .line 272
    :cond_0
    return-void
.end method
