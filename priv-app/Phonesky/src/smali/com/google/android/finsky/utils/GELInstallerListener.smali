.class public Lcom/google/android/finsky/utils/GELInstallerListener;
.super Ljava/lang/Object;
.source "GELInstallerListener.java"

# interfaces
.implements Lcom/google/android/finsky/installer/InstallerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/GELInstallerListener$1;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/receivers/Installer;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "installer"    # Lcom/google/android/finsky/receivers/Installer;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/finsky/utils/GELInstallerListener;->mContext:Landroid/content/Context;

    .line 43
    invoke-interface {p2, p0}, Lcom/google/android/finsky/receivers/Installer;->addListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 44
    return-void
.end method


# virtual methods
.method public onInstallPackageEvent(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p3, "statusCode"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 49
    const/4 v0, 0x0

    .line 50
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/google/android/finsky/utils/GELInstallerListener$1;->$SwitchMap$com$google$android$finsky$installer$InstallerListener$InstallerPackageEvent:[I

    invoke-virtual {p2}, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    .line 78
    :goto_0
    if-eqz v0, :cond_1

    .line 79
    sget-object v1, Lcom/google/android/finsky/config/G;->gelPackageName:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    const-string v1, "package"

    const/4 v4, 0x0

    invoke-static {v1, p1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 81
    sget-boolean v1, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 82
    const-string v1, "GEL broadcast uri=[%s], action=[%s], for package=[%s]"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    const/4 v2, 0x2

    aput-object p1, v4, v2

    invoke-static {v1, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/utils/GELInstallerListener;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 87
    :cond_1
    return-void

    .line 53
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string v1, "com.android.launcher.action.ACTION_PACKAGE_ENQUEUED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 55
    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 58
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string v1, "com.android.launcher.action.ACTION_PACKAGE_DOWNLOADING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 60
    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 63
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string v1, "com.android.launcher.action.ACTION_PACKAGE_INSTALLING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 64
    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 70
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string v1, "com.android.launcher.action.ACTION_PACKAGE_DEQUEUED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 72
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v4, "com.android.launcher.action.INSTALL_COMPLETED"

    sget-object v1, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALLED:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    if-ne p2, v1, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
