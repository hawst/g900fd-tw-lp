.class Lcom/google/android/finsky/activities/SearchFragment$2;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/SearchFragment;->rebindAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/SearchFragment;

.field final synthetic val$isRestoring:Z


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/SearchFragment;Z)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    iput-boolean p2, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->val$isRestoring:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/SearchFragment;->access$000(Lcom/google/android/finsky/activities/SearchFragment;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;
    invoke-static {v0}, Lcom/google/android/finsky/activities/SearchFragment;->access$100(Lcom/google/android/finsky/activities/SearchFragment;)Lcom/google/android/finsky/adapters/CardListAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/SearchFragment;->access$000(Lcom/google/android/finsky/activities/SearchFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;
    invoke-static {v1}, Lcom/google/android/finsky/activities/SearchFragment;->access$100(Lcom/google/android/finsky/activities/SearchFragment;)Lcom/google/android/finsky/adapters/CardListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/SearchFragment;->access$000(Lcom/google/android/finsky/activities/SearchFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;
    invoke-static {v1}, Lcom/google/android/finsky/activities/SearchFragment;->access$100(Lcom/google/android/finsky/activities/SearchFragment;)Lcom/google/android/finsky/adapters/CardListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 245
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->val$isRestoring:Z

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;
    invoke-static {v0}, Lcom/google/android/finsky/activities/SearchFragment;->access$100(Lcom/google/android/finsky/activities/SearchFragment;)Lcom/google/android/finsky/adapters/CardListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/google/android/finsky/activities/SearchFragment;->access$000(Lcom/google/android/finsky/activities/SearchFragment;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mListViewRestoreBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/finsky/activities/SearchFragment;->access$200(Lcom/google/android/finsky/activities/SearchFragment;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/adapters/CardListAdapter;->onRestoreInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mListViewRestoreBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/finsky/activities/SearchFragment;->access$200(Lcom/google/android/finsky/activities/SearchFragment;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/SearchFragment;->access$000(Lcom/google/android/finsky/activities/SearchFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment$2;->this$0:Lcom/google/android/finsky/activities/SearchFragment;

    # getter for: Lcom/google/android/finsky/activities/SearchFragment;->mDataView:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/google/android/finsky/activities/SearchFragment;->access$300(Lcom/google/android/finsky/activities/SearchFragment;)Landroid/view/ViewGroup;

    move-result-object v1

    const v2, 0x7f0a026e

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 257
    :cond_1
    return-void
.end method
