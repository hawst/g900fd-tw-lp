.class public final Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddressFormInput"
.end annotation


# instance fields
.field public address:Lcom/google/android/finsky/protos/BillingAddress$Address;

.field public addressType:I

.field public hasAddressType:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2260
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2261
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->clear()Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

    .line 2262
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2265
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 2266
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->addressType:I

    .line 2267
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->hasAddressType:Z

    .line 2268
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->cachedSize:I

    .line 2269
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2286
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2287
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v1, :cond_0

    .line 2288
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2291
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->addressType:I

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->hasAddressType:Z

    if-eqz v1, :cond_2

    .line 2292
    :cond_1
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->addressType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2295
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2303
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2304
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2308
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2309
    :sswitch_0
    return-object p0

    .line 2314
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-nez v2, :cond_1

    .line 2315
    new-instance v2, Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/BillingAddress$Address;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 2317
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2321
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2322
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2326
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->addressType:I

    .line 2327
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->hasAddressType:Z

    goto :goto_0

    .line 2304
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    .line 2322
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2236
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2275
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v0, :cond_0

    .line 2276
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2278
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->addressType:I

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->hasAddressType:Z

    if-eqz v0, :cond_2

    .line 2279
    :cond_1
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;->addressType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2281
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2282
    return-void
.end method
