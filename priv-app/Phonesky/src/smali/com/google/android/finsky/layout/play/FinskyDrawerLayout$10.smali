.class Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$10;
.super Ljava/lang/Object;
.source "FinskyDrawerLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->createSecondaryActions(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 496
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$10;->this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    iput-object p2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$10;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 499
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$10;->this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    const/16 v3, 0x6d

    # invokes: Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->logMenuClickEvent(I)V
    invoke-static {v2, v3}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->access$500(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;I)V

    .line 500
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    .line 501
    .local v0, "accountName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 502
    const-string v2, "Redeem chosen with no current account."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 508
    :goto_0
    return-void

    .line 505
    :cond_0
    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->createIntent(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 507
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$10;->val$context:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
