.class public Lcom/google/android/finsky/installer/InstallPolicies;
.super Ljava/lang/Object;
.source "InstallPolicies.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    }
.end annotation


# static fields
.field private static final DEBUG_FORCE_LARGE_SIZE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEBUG_FORCE_PERMISSIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SUPPORTS_MOBILE_HOTSPOT:Z


# instance fields
.field private final mAppStates:Lcom/google/android/finsky/appstate/AppStates;

.field private final mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private mMaxBytesOverMobile:J

.field private mMaxBytesOverMobileRecommended:J

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 62
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/installer/InstallPolicies;->SUPPORTS_MOBILE_HOTSPOT:Z

    .line 75
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/installer/InstallPolicies;->DEBUG_FORCE_LARGE_SIZE:Ljava/util/List;

    .line 77
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/installer/InstallPolicies;->DEBUG_FORCE_PERMISSIONS:Ljava/util/List;

    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V
    .locals 2
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p3, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p4, "libraries"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/finsky/installer/InstallPolicies;->setMobileDownloadThresholds(Landroid/content/ContentResolver;)V

    .line 112
    iput-object p2, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 113
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 115
    iput-object p3, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    .line 116
    iput-object p4, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 117
    return-void
.end method

.method private getAutoUpdateSizeLimit()J
    .locals 2

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/google/android/finsky/installer/InstallPolicies;->isMobileNetwork()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/installer/InstallPolicies;->getMaxBytesOverMobileRecommended()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method public static getForegroundPackages(Landroid/content/Context;)Ljava/util/Set;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v14, 0x1

    .line 622
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v5

    .line 623
    .local v5, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-string v13, "activity"

    invoke-virtual {p0, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 627
    .local v0, "am":Landroid/app/ActivityManager;
    const-string v13, "power"

    invoke-virtual {p0, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PowerManager;

    .line 628
    .local v4, "pm":Landroid/os/PowerManager;
    invoke-virtual {v4}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 629
    invoke-virtual {v0, v14, v14}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v12

    .line 630
    .local v12, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v13

    if-lez v13, :cond_0

    .line 631
    const/4 v13, 0x0

    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/ActivityManager$RecentTaskInfo;

    .line 632
    .local v11, "task":Landroid/app/ActivityManager$RecentTaskInfo;
    iget-object v13, v11, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    if-eqz v13, :cond_0

    .line 633
    iget-object v13, v11, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v13}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 634
    .local v1, "baseComponent":Landroid/content/ComponentName;
    if-eqz v1, :cond_0

    .line 635
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 636
    .local v2, "basePackage":Ljava/lang/String;
    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 646
    .end local v1    # "baseComponent":Landroid/content/ComponentName;
    .end local v2    # "basePackage":Ljava/lang/String;
    .end local v11    # "task":Landroid/app/ActivityManager$RecentTaskInfo;
    .end local v12    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    :cond_0
    sget-object v13, Lcom/google/android/finsky/config/G;->autoUpdateExcludeForegroundServices:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v13}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 647
    const v13, 0x7fffffff

    invoke-virtual {v0, v13}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v10

    .line 648
    .local v10, "services":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v10, :cond_2

    .line 649
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v8

    .line 650
    .local v8, "serviceCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v8, :cond_2

    .line 651
    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 652
    .local v6, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-boolean v13, v6, Landroid/app/ActivityManager$RunningServiceInfo;->foreground:Z

    if-eqz v13, :cond_1

    .line 653
    iget-object v7, v6, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    .line 654
    .local v7, "serviceComponent":Landroid/content/ComponentName;
    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    .line 655
    .local v9, "servicePackage":Ljava/lang/String;
    invoke-interface {v5, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 650
    .end local v7    # "serviceComponent":Landroid/content/ComponentName;
    .end local v9    # "servicePackage":Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 661
    .end local v3    # "i":I
    .end local v6    # "service":Landroid/app/ActivityManager$RunningServiceInfo;
    .end local v8    # "serviceCount":I
    .end local v10    # "services":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_2
    return-object v5
.end method

.method private getInstallWarningsForDocument(JLjava/lang/String;Lcom/google/android/finsky/protos/DocDetails$AppDetails;Z)Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    .locals 15
    .param p1, "downloadSizeLimit"    # J
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "appDetails"    # Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    .param p5, "treatDisabledUpdatesAsWarnings"    # Z

    .prologue
    .line 507
    new-instance v5, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;

    invoke-direct {v5}, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;-><init>()V

    .line 509
    .local v5, "installWarnings":Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    sget-object v11, Lcom/google/android/finsky/installer/InstallPolicies;->DEBUG_FORCE_LARGE_SIZE:Ljava/util/List;

    move-object/from16 v0, p3

    invoke-interface {v11, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 510
    const-string v11, "Forcing true for size limit for package %s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object p3, v12, v13

    invoke-static {v11, v12}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 511
    const/4 v11, 0x1

    iput-boolean v11, v5, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->largeDownload:Z

    .line 515
    :cond_0
    move-object/from16 v0, p4

    iget-boolean v11, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasInstallationSize:Z

    if-eqz v11, :cond_5

    move-object/from16 v0, p4

    iget-wide v6, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->installationSize:J

    .line 516
    .local v6, "installSize":J
    :goto_0
    cmp-long v11, v6, p1

    if-ltz v11, :cond_1

    .line 527
    const/4 v11, 0x1

    iput-boolean v11, v5, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->largeDownload:Z

    .line 530
    :cond_1
    iget-object v11, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v0, p4

    iget-object v12, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v2

    .line 531
    .local v2, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v2, :cond_2

    iget-object v11, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-nez v11, :cond_6

    :cond_2
    const/4 v9, 0x1

    .line 535
    .local v9, "isNewInstall":Z
    :goto_1
    if-eqz v9, :cond_7

    .line 536
    const/4 v11, 0x1

    iput-boolean v11, v5, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->newPermissions:Z

    .line 558
    :cond_3
    :goto_2
    if-nez v9, :cond_4

    if-eqz p5, :cond_4

    .line 559
    iget-object v11, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v11, :cond_4

    iget-object v11, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v11}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAutoUpdate()Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    move-result-object v11

    sget-object v12, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->DISABLED:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    if-ne v11, v12, :cond_4

    .line 561
    const/4 v11, 0x1

    iput-boolean v11, v5, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->autoUpdateDisabled:Z

    .line 565
    :cond_4
    return-object v5

    .line 515
    .end local v2    # "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    .end local v6    # "installSize":J
    .end local v9    # "isNewInstall":Z
    :cond_5
    const-wide/16 v6, 0x0

    goto :goto_0

    .line 531
    .restart local v2    # "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    .restart local v6    # "installSize":J
    :cond_6
    const/4 v9, 0x0

    goto :goto_1

    .line 539
    .restart local v9    # "isNewInstall":Z
    :cond_7
    iget-object v11, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mPackageManager:Landroid/content/pm/PackageManager;

    move-object/from16 v0, p3

    invoke-static {v11, v0}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->getPackageInfo(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v10

    .line 541
    .local v10, "packageInfo":Landroid/content/pm/PackageInfo;
    invoke-static {v10}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->loadLocalAssetPermissions(Landroid/content/pm/PackageInfo;)Ljava/util/Set;

    move-result-object v8

    .line 543
    .local v8, "installedPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v11, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v11, :cond_9

    iget-object v11, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v11}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPermissionsVersion()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_9

    const/4 v4, 0x1

    .line 546
    .local v4, "hasAcceptedBuckets":Z
    :goto_3
    move-object/from16 v0, p4

    iget-object v11, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    invoke-static {v11, v8, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer;->getPermissionBuckets([Ljava/lang/String;Ljava/util/Set;Z)Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    move-result-object v3

    .line 551
    .local v3, "data":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;
    sget-object v11, Lcom/google/android/finsky/installer/InstallPolicies;->DEBUG_FORCE_PERMISSIONS:Ljava/util/List;

    move-object/from16 v0, p3

    invoke-interface {v11, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_8

    iget-object v11, v3, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mNewPermissions:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-gtz v11, :cond_8

    iget-boolean v11, v3, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mForcePermissionPrompt:Z

    if-eqz v11, :cond_3

    .line 553
    :cond_8
    const/4 v11, 0x1

    iput-boolean v11, v5, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->newPermissions:Z

    goto :goto_2

    .line 543
    .end local v3    # "data":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;
    .end local v4    # "hasAcceptedBuckets":Z
    :cond_9
    const/4 v4, 0x0

    goto :goto_3
.end method

.method private getStorageLowBytes(JLandroid/content/ContentResolver;)J
    .locals 15
    .param p1, "partitionTotalBytes"    # J
    .param p3, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 240
    sget-object v7, Lcom/google/android/finsky/config/G;->downloadFreeSpaceThresholdBytes:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v7}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 241
    .local v2, "finskyMinFreeBytes":J
    const-wide/16 v10, 0x0

    cmp-long v7, v2, v10

    if-lez v7, :cond_0

    .line 267
    .end local v2    # "finskyMinFreeBytes":J
    :goto_0
    return-wide v2

    .line 246
    .restart local v2    # "finskyMinFreeBytes":J
    :cond_0
    const/4 v6, 0x0

    .line 247
    .local v6, "minFreePercent":I
    const-wide/16 v4, 0x0

    .line 250
    .local v4, "minFreeBytes":J
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x11

    if-lt v7, v10, :cond_1

    .line 251
    const-string v7, "sys_storage_threshold_percentage"

    const/16 v10, 0xa

    move-object/from16 v0, p3

    invoke-static {v0, v7, v10}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 253
    const-string v7, "sys_storage_threshold_max_bytes"

    const-wide/32 v10, 0x1f400000

    move-object/from16 v0, p3

    invoke-static {v0, v7, v10, v11}, Landroid/provider/Settings$Global;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    .line 266
    :goto_1
    int-to-long v10, v6

    mul-long v10, v10, p1

    const-wide/16 v12, 0x64

    div-long v8, v10, v12

    .line 267
    .local v8, "minFreePercentInBytes":J
    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    goto :goto_0

    .line 256
    .end local v8    # "minFreePercentInBytes":J
    :cond_1
    const-string v7, "sys_storage_threshold_percentage"

    const/16 v10, 0xa

    move-object/from16 v0, p3

    invoke-static {v0, v7, v10}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 258
    const-string v7, "sys_storage_threshold_max_bytes"

    const-wide/32 v10, 0x1f400000

    move-object/from16 v0, p3

    invoke-static {v0, v7, v10, v11}, Landroid/provider/Settings$Secure;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    goto :goto_1
.end method

.method private setMobileDownloadThresholds(Landroid/content/ContentResolver;)V
    .locals 6
    .param p1, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    const-wide/16 v4, 0x0

    .line 189
    sget-object v2, Lcom/google/android/finsky/config/G;->downloadBytesOverMobileMaximum:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mMaxBytesOverMobile:J

    .line 190
    sget-object v2, Lcom/google/android/finsky/config/G;->downloadBytesOverMobileRecommended:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mMaxBytesOverMobileRecommended:J

    .line 194
    :try_start_0
    const-string v2, "download_manager_max_bytes_over_mobile"

    invoke-static {p1, v2}, Landroid/provider/Settings$Secure;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v0

    .line 196
    .local v0, "newLimit":J
    cmp-long v2, v0, v4

    if-lez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mMaxBytesOverMobile:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 197
    iput-wide v0, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mMaxBytesOverMobile:J
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 202
    .end local v0    # "newLimit":J
    :cond_0
    :goto_0
    :try_start_1
    const-string v2, "download_manager_recommended_max_bytes_over_mobile"

    invoke-static {p1, v2}, Landroid/provider/Settings$Secure;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v0

    .line 204
    .restart local v0    # "newLimit":J
    cmp-long v2, v0, v4

    if-lez v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mMaxBytesOverMobileRecommended:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 205
    iput-wide v0, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mMaxBytesOverMobileRecommended:J
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 211
    .end local v0    # "newLimit":J
    :cond_1
    :goto_1
    iget-wide v2, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mMaxBytesOverMobileRecommended:J

    iget-wide v4, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mMaxBytesOverMobile:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mMaxBytesOverMobileRecommended:J

    .line 213
    return-void

    .line 207
    :catch_0
    move-exception v2

    goto :goto_1

    .line 199
    :catch_1
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method public canUpdateApp(Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;Lcom/google/android/finsky/api/model/Document;)Z
    .locals 8
    .param p1, "packageState"    # Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    .param p2, "app"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 336
    if-nez p1, :cond_1

    .line 359
    :cond_0
    :goto_0
    return v3

    .line 340
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v5}, Lcom/google/android/finsky/library/Libraries;->isLoaded()Z

    move-result v5

    if-nez v5, :cond_2

    .line 341
    const-string v4, "Library not loaded."

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 344
    :cond_2
    iget v0, p1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 345
    .local v0, "installedVersion":I
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v5

    iget v2, v5, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    .line 346
    .local v2, "serverAssetVersion":I
    iget-boolean v5, p1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabled:Z

    if-nez v5, :cond_0

    .line 350
    iget-object v1, p1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->packageName:Ljava/lang/String;

    .line 351
    .local v1, "packageName":Ljava/lang/String;
    if-le v2, v0, :cond_0

    .line 352
    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {p2, v5, v6}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 353
    const-string v5, "Cannot update unavailable app: pkg=%s,restriction=%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v3

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getAvailabilityRestriction()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v3, v4

    .line 357
    goto :goto_0
.end method

.method public getApplicationsEligibleForAutoUpdate(Ljava/util/List;)Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 374
    .local p1, "docsFromServer":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/installer/InstallPolicies;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v3}, Lcom/google/android/finsky/library/Libraries;->isLoaded()Z

    move-result v3

    if-nez v3, :cond_1

    .line 375
    const-string v3, "Library not loaded."

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v3, v8}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 376
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v17

    .line 431
    :cond_0
    return-object v17

    .line 379
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/installer/InstallPolicies;->getAutoUpdateSizeLimit()J

    move-result-wide v4

    .line 382
    .local v4, "autoUpdateSizeLimit":J
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/installer/InstallPolicies;->getForegroundPackages(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v18

    .line 384
    .local v18, "foregroundPackages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v17

    .line 385
    .local v17, "docsToUpdate":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/finsky/api/model/Document;

    .line 387
    .local v16, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v7

    .line 388
    .local v7, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    iget-object v6, v7, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 389
    .local v6, "packageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/installer/InstallPolicies;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v3, v6}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v2

    .line 392
    .local v2, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v2, :cond_3

    iget-object v3, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-nez v3, :cond_4

    .line 393
    :cond_3
    const-string v3, "Server thinks we have an asset that we don\'t have : %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    invoke-static {v3, v8}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 396
    :cond_4
    iget v3, v7, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    iget-object v8, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget v8, v8, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    if-le v3, v8, :cond_2

    .line 401
    const/4 v8, 0x1

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/finsky/installer/InstallPolicies;->getInstallWarningsForDocument(JLjava/lang/String;Lcom/google/android/finsky/protos/DocDetails$AppDetails;Z)Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;

    move-result-object v15

    .line 403
    .local v15, "autoUpdateWarnings":Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    invoke-virtual {v15}, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->warningRequired()Z

    move-result v3

    if-nez v3, :cond_2

    .line 408
    sget-object v3, Lcom/google/android/finsky/config/G;->autoUpdateExcludeRunningPackagesPre:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 409
    move-object/from16 v0, v18

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 411
    const-string v3, "Exclude auto-update for foreground package: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    invoke-static {v3, v8}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 412
    new-instance v14, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-direct {v14}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    .line 413
    .local v14, "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    iget v3, v7, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    iput v3, v14, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 414
    const/4 v3, 0x1

    iput-boolean v3, v14, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    .line 415
    iget-object v3, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget v3, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    iput v3, v14, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    .line 416
    const/4 v3, 0x1

    iput-boolean v3, v14, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    .line 417
    iget-object v3, v2, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget-boolean v3, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isSystemApp:Z

    iput-boolean v3, v14, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    .line 418
    const/4 v3, 0x1

    iput-boolean v3, v14, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    .line 419
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v8

    const/16 v9, 0x79

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v10, v6

    invoke-virtual/range {v8 .. v14}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    goto/16 :goto_0

    .line 428
    .end local v14    # "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    :cond_5
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public getApplicationsEligibleForNewUpdateNotification(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590
    .local p1, "docsToUpdate":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 592
    .local v3, "docsToNotify":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    .line 593
    .local v2, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    .line 594
    .local v0, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    iget-object v6, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v6}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v6

    iget-object v7, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v5

    .line 596
    .local v5, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    iget-object v6, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v7, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v1

    .line 597
    .local v1, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    iget-object v6, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v6, :cond_1

    iget v6, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    invoke-virtual {v5}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getLastNotifiedVersion()I

    move-result v7

    if-le v6, v7, :cond_0

    .line 599
    :cond_1
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 602
    .end local v0    # "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    .end local v1    # "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    .end local v2    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v5    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :cond_2
    return-object v3
.end method

.method public getApplicationsWithUpdates(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 321
    .local p1, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 322
    .local v2, "outdated":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 323
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v5

    iget-object v3, v5, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 324
    .local v3, "packageName":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v5}, Lcom/google/android/finsky/appstate/AppStates;->getPackageStateRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v5

    invoke-interface {v5, v3}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v4

    .line 325
    .local v4, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    invoke-virtual {p0, v4, v0}, Lcom/google/android/finsky/installer/InstallPolicies;->canUpdateApp(Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;Lcom/google/android/finsky/api/model/Document;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 326
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 329
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_1
    return-object v2
.end method

.method public getAppsThatRequireUpdateWarnings(Ljava/util/List;Z)Ljava/util/List;
    .locals 10
    .param p2, "treatDisabledUpdatesAsWarnings"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 482
    .local p1, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/installer/InstallPolicies;->getAutoUpdateSizeLimit()J

    move-result-wide v2

    .line 483
    .local v2, "autoUpdateSizeLimit":J
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 484
    .local v0, "appsThatRequireUpdateWarnings":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/api/model/Document;

    .line 485
    .local v8, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v5

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/finsky/installer/InstallPolicies;->getInstallWarningsForDocument(JLjava/lang/String;Lcom/google/android/finsky/protos/DocDetails$AppDetails;Z)Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;

    move-result-object v7

    .line 488
    .local v7, "autoUpdateWarnings":Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    invoke-virtual {v7}, Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;->warningRequired()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 489
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 492
    .end local v7    # "autoUpdateWarnings":Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    .end local v8    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    return-object v0
.end method

.method public getInstallWarningsForDocument(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    .locals 1
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 445
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/installer/InstallPolicies;->getUpdateWarningsForDocument(Lcom/google/android/finsky/api/model/Document;Z)Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;

    move-result-object v0

    return-object v0
.end method

.method public getMaxBytesOverMobile()J
    .locals 2

    .prologue
    .line 181
    iget-wide v0, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mMaxBytesOverMobile:J

    return-wide v0
.end method

.method public getMaxBytesOverMobileRecommended()J
    .locals 2

    .prologue
    .line 173
    iget-wide v0, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mMaxBytesOverMobileRecommended:J

    return-wide v0
.end method

.method public getUpdateWarningsForDocument(Lcom/google/android/finsky/api/model/Document;Z)Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    .locals 7
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "treatDisabledUpdatesAsWarnings"    # Z

    .prologue
    .line 465
    invoke-direct {p0}, Lcom/google/android/finsky/installer/InstallPolicies;->getAutoUpdateSizeLimit()J

    move-result-wide v2

    .line 466
    .local v2, "downloadSizeLimit":J
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v5

    move-object v1, p0

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/finsky/installer/InstallPolicies;->getInstallWarningsForDocument(JLjava/lang/String;Lcom/google/android/finsky/protos/DocDetails$AppDetails;Z)Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;

    move-result-object v0

    return-object v0
.end method

.method public hasMobileNetwork()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 156
    iget-object v2, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 157
    .local v0, "info":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public hasNetwork()Z
    .locals 2

    .prologue
    .line 164
    iget-object v1, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 165
    .local v0, "info":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFreeSpaceSufficient(JLjava/io/File;Landroid/content/ContentResolver;)Z
    .locals 19
    .param p1, "apkSize"    # J
    .param p3, "path"    # Ljava/io/File;
    .param p4, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 285
    new-instance v10, Landroid/os/StatFs;

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 288
    .local v10, "partitionStats":Landroid/os/StatFs;
    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v14, 0x12

    if-lt v11, v14, :cond_0

    .line 289
    invoke-virtual {v10}, Landroid/os/StatFs;->getAvailableBytes()J

    move-result-wide v8

    .line 290
    .local v8, "partitionAvailableBytes":J
    invoke-virtual {v10}, Landroid/os/StatFs;->getTotalBytes()J

    move-result-wide v12

    .line 298
    .local v12, "partitionTotalBytes":J
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v12, v13, v1}, Lcom/google/android/finsky/installer/InstallPolicies;->getStorageLowBytes(JLandroid/content/ContentResolver;)J

    move-result-wide v6

    .line 301
    .local v6, "minimumFreeBytes":J
    sget-object v11, Lcom/google/android/finsky/config/G;->downloadFreeSpaceApkSizeFactor:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v11}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    int-to-long v14, v11

    mul-long v14, v14, p1

    const-wide/16 v16, 0x64

    div-long p1, v14, v16

    .line 306
    sub-long v4, v8, p1

    .line 307
    .local v4, "freeAfterDownload":J
    cmp-long v11, v4, v6

    if-ltz v11, :cond_1

    const/4 v11, 0x1

    :goto_1
    return v11

    .line 292
    .end local v4    # "freeAfterDownload":J
    .end local v6    # "minimumFreeBytes":J
    .end local v8    # "partitionAvailableBytes":J
    .end local v12    # "partitionTotalBytes":J
    :cond_0
    invoke-virtual {v10}, Landroid/os/StatFs;->getBlockSize()I

    move-result v11

    int-to-long v2, v11

    .line 293
    .local v2, "blockSize":J
    invoke-virtual {v10}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v11

    int-to-long v14, v11

    mul-long v8, v2, v14

    .line 294
    .restart local v8    # "partitionAvailableBytes":J
    invoke-virtual {v10}, Landroid/os/StatFs;->getBlockCount()I

    move-result v11

    int-to-long v14, v11

    mul-long v12, v2, v14

    .restart local v12    # "partitionTotalBytes":J
    goto :goto_0

    .line 307
    .end local v2    # "blockSize":J
    .restart local v4    # "freeAfterDownload":J
    .restart local v6    # "minimumFreeBytes":J
    :cond_1
    const/4 v11, 0x0

    goto :goto_1
.end method

.method public isMobileHotspot()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 144
    sget-boolean v1, Lcom/google/android/finsky/installer/InstallPolicies;->SUPPORTS_MOBILE_HOTSPOT:Z

    if-eqz v1, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/google/android/finsky/installer/InstallPolicies;->isWifiNetwork()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 147
    :cond_0
    return v0
.end method

.method public isMobileNetwork()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 124
    iget-object v2, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 125
    .local v0, "info":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isWifiNetwork()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 132
    iget-object v2, p0, Lcom/google/android/finsky/installer/InstallPolicies;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 133
    .local v0, "wifiInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
