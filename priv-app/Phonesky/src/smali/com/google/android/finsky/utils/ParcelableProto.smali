.class public Lcom/google/android/finsky/utils/ParcelableProto;
.super Ljava/lang/Object;
.source "ParcelableProto.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/utils/ParcelableProto",
            "<",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mPayload:Lcom/google/protobuf/nano/MessageNano;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mSerialized:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lcom/google/android/finsky/utils/ParcelableProto$1;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/ParcelableProto$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/ParcelableProto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/nano/MessageNano;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lcom/google/android/finsky/utils/ParcelableProto;, "Lcom/google/android/finsky/utils/ParcelableProto<TT;>;"
    .local p1, "payload":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/utils/ParcelableProto;->mSerialized:[B

    .line 23
    iput-object p1, p0, Lcom/google/android/finsky/utils/ParcelableProto;->mPayload:Lcom/google/protobuf/nano/MessageNano;

    .line 24
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/nano/MessageNano;Lcom/google/android/finsky/utils/ParcelableProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/protobuf/nano/MessageNano;
    .param p2, "x1"    # Lcom/google/android/finsky/utils/ParcelableProto$1;

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/finsky/utils/ParcelableProto;, "Lcom/google/android/finsky/utils/ParcelableProto<TT;>;"
    invoke-direct {p0, p1}, Lcom/google/android/finsky/utils/ParcelableProto;-><init>(Lcom/google/protobuf/nano/MessageNano;)V

    return-void
.end method

.method public static forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(TT;)",
            "Lcom/google/android/finsky/utils/ParcelableProto",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "payload":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    new-instance v0, Lcom/google/android/finsky/utils/ParcelableProto;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/utils/ParcelableProto;-><init>(Lcom/google/protobuf/nano/MessageNano;)V

    return-object v0
.end method

.method public static getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2
    .param p0, "b"    # Landroid/os/Bundle;
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/ParcelableProto;

    .line 61
    .local v0, "wrapper":Lcom/google/android/finsky/utils/ParcelableProto;, "Lcom/google/android/finsky/utils/ParcelableProto<TT;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getPayload()Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2
    .param p0, "i"    # Landroid/content/Intent;
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/ParcelableProto;

    .line 42
    .local v0, "wrapper":Lcom/google/android/finsky/utils/ParcelableProto;, "Lcom/google/android/finsky/utils/ParcelableProto<TT;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getPayload()Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getProtoFromParcel(Landroid/os/Parcel;)Lcom/google/protobuf/nano/MessageNano;
    .locals 2
    .param p0, "source"    # Landroid/os/Parcel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Landroid/os/Parcel;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 50
    const-class v1, Lcom/google/android/finsky/utils/ParcelableProto;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/ParcelableProto;

    .line 51
    .local v0, "wrapper":Lcom/google/android/finsky/utils/ParcelableProto;, "Lcom/google/android/finsky/utils/ParcelableProto<TT;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getPayload()Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 70
    .local p0, "this":Lcom/google/android/finsky/utils/ParcelableProto;, "Lcom/google/android/finsky/utils/ParcelableProto<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public getPayload()Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 65
    .local p0, "this":Lcom/google/android/finsky/utils/ParcelableProto;, "Lcom/google/android/finsky/utils/ParcelableProto<TT;>;"
    iget-object v0, p0, Lcom/google/android/finsky/utils/ParcelableProto;->mPayload:Lcom/google/protobuf/nano/MessageNano;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 75
    .local p0, "this":Lcom/google/android/finsky/utils/ParcelableProto;, "Lcom/google/android/finsky/utils/ParcelableProto<TT;>;"
    iget-object v0, p0, Lcom/google/android/finsky/utils/ParcelableProto;->mPayload:Lcom/google/protobuf/nano/MessageNano;

    if-nez v0, :cond_0

    .line 77
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/ParcelableProto;->mSerialized:[B

    if-nez v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/google/android/finsky/utils/ParcelableProto;->mPayload:Lcom/google/protobuf/nano/MessageNano;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/utils/ParcelableProto;->mSerialized:[B

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/utils/ParcelableProto;->mSerialized:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    iget-object v0, p0, Lcom/google/android/finsky/utils/ParcelableProto;->mSerialized:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 85
    iget-object v0, p0, Lcom/google/android/finsky/utils/ParcelableProto;->mPayload:Lcom/google/protobuf/nano/MessageNano;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
