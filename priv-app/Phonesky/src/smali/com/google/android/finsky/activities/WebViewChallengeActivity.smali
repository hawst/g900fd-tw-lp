.class public Lcom/google/android/finsky/activities/WebViewChallengeActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "WebViewChallengeActivity.java"

# interfaces
.implements Lcom/google/android/finsky/layout/ButtonBar$ClickListener;


# instance fields
.field private mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

.field private mIsFirstPageLoad:Z

.field private mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private final mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

.field private mNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 48
    new-instance v0, Lcom/google/android/finsky/activities/WebViewChallengeActivity$1;

    invoke-direct {v0, p0, p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity$1;-><init>(Lcom/google/android/finsky/activities/WebViewChallengeActivity;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mIsFirstPageLoad:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/WebViewChallengeActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->cancel(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/WebViewChallengeActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->checkUrlAndLog(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/WebViewChallengeActivity;)Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/WebViewChallengeActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->onTargetUrlMatch(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/WebViewChallengeActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mIsFirstPageLoad:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/finsky/activities/WebViewChallengeActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/WebViewChallengeActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mIsFirstPageLoad:Z

    return p1
.end method

.method private cancel(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 275
    const/4 v0, 0x0

    .line 277
    .local v0, "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    if-eqz p1, :cond_0

    .line 279
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .end local v0    # "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    .line 280
    .restart local v0    # "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->host:Ljava/lang/String;

    .line 281
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasHost:Z

    .line 283
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v2, 0x10c

    iget-object v3, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEventWithClientCookie(ILcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 285
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->setResult(I)V

    .line 286
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->finish()V

    .line 287
    return-void
.end method

.method private checkUrlAndLog(Ljava/lang/String;)Z
    .locals 7
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 225
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 226
    .local v2, "uri":Landroid/net/Uri;
    const-string v5, "https"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "data"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 229
    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 230
    .local v1, "host":Ljava/lang/String;
    const-string v5, "Detected non-HTTPS resource from host: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    invoke-static {v5, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    new-instance v4, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    const/16 v5, 0x1f8

    invoke-direct {v4, v5}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setHost(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 234
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    iget-object v4, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 237
    .end local v0    # "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .end local v1    # "host":Ljava/lang/String;
    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method public static createIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;)Landroid/content/Intent;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "backupTitle"    # Ljava/lang/String;
    .param p2, "webViewChallenge"    # Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    .prologue
    .line 73
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/activities/WebViewChallengeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 74
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    const-string v1, "backupTitle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    const-string v1, "challenge"

    invoke-static {p2}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 77
    return-object v0
.end method

.method private onTargetUrlMatch(Ljava/lang/String;)V
    .locals 9
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    .line 241
    const-string v5, "Matched URL: %s"

    new-array v6, v8, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 242
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 244
    .local v4, "uri":Landroid/net/Uri;
    const/4 v1, 0x0

    .line 245
    .local v1, "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 246
    .local v2, "hostName":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 247
    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .end local v1    # "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    .line 248
    .restart local v1    # "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    iput-object v2, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->host:Ljava/lang/String;

    .line 249
    iput-boolean v8, v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasHost:Z

    .line 251
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v6, 0x10b

    iget-object v7, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    invoke-virtual {v5, v6, v1, v7}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEventWithClientCookie(ILcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 254
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 255
    .local v3, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 256
    .local v0, "challengeResponse":Landroid/os/Bundle;
    iget-object v5, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->responseTargetUrlParam:Ljava/lang/String;

    invoke-virtual {v0, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string v5, "challenge_response"

    invoke-virtual {v3, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 259
    const/4 v5, -0x1

    invoke-virtual {p0, v5, v3}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->setResult(ILandroid/content/Intent;)V

    .line 260
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->finish()V

    .line 261
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 306
    :goto_0
    return-void

    .line 304
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->onNegativeButtonClick()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 82
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "challenge"

    invoke-static {v9, v10}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v9

    check-cast v9, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    iput-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    .line 83
    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    iget-boolean v9, v9, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasTitle:Z

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    iget-object v7, v9, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->title:Ljava/lang/String;

    .line 86
    .local v7, "title":Ljava/lang/String;
    :goto_0
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 87
    invoke-virtual {p0, v12}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->requestWindowFeature(I)Z

    .line 90
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    const v9, 0x7f04003d

    invoke-virtual {p0, v9}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->setContentView(I)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "authAccount"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "accountName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 98
    new-instance v1, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-direct {v1, v9, p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/support/v7/app/ActionBarActivity;)V

    .line 99
    .local v1, "actionBarHelper":Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;
    invoke-virtual {v1, v11, v11}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateCurrentBackendId(IZ)V

    .line 101
    const v9, 0x7f0a0114

    invoke-virtual {p0, v9}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/ButtonBar;

    .line 102
    .local v3, "buttonBar":Lcom/google/android/finsky/layout/ButtonBar;
    invoke-virtual {v3, v11}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonVisible(Z)V

    .line 103
    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    iget-boolean v9, v9, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->hasCancelButtonDisplayLabel:Z

    if-eqz v9, :cond_5

    .line 104
    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    iget-object v9, v9, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->cancelButtonDisplayLabel:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lcom/google/android/finsky/layout/ButtonBar;->setNegativeButtonTitle(Ljava/lang/String;)V

    .line 108
    :goto_1
    invoke-virtual {v3, p0}, Lcom/google/android/finsky/layout/ButtonBar;->setClickListener(Lcom/google/android/finsky/layout/ButtonBar$ClickListener;)V

    .line 110
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 111
    invoke-virtual {v1, v7}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateDefaultTitle(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 115
    :cond_1
    const v9, 0x7f0a0111

    invoke-virtual {p0, v9}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/webkit/WebView;

    iput-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    .line 118
    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v10, Lcom/google/android/finsky/activities/WebViewChallengeActivity$2;

    invoke-direct {v10, p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity$2;-><init>(Lcom/google/android/finsky/activities/WebViewChallengeActivity;)V

    invoke-virtual {v9, v10}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 132
    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v9}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 133
    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v9}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 135
    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v10, Lcom/google/android/finsky/activities/WebViewChallengeActivity$3;

    invoke-direct {v10, p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity$3;-><init>(Lcom/google/android/finsky/activities/WebViewChallengeActivity;)V

    invoke-virtual {v9, v10}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 193
    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    iget-object v6, v9, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;->startUrl:Ljava/lang/String;

    .line 194
    .local v6, "startUrl":Ljava/lang/String;
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 198
    .local v8, "uri":Landroid/net/Uri;
    const/4 v4, 0x0

    .line 199
    .local v4, "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    invoke-virtual {v8}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    .line 200
    .local v5, "hostName":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 201
    new-instance v4, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .end local v4    # "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    invoke-direct {v4}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    .line 202
    .restart local v4    # "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    iput-object v5, v4, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->host:Ljava/lang/String;

    .line 203
    iput-boolean v12, v4, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasHost:Z

    .line 205
    :cond_2
    new-instance v9, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v10, 0x13c

    invoke-direct {v9, v10, v13, v4, v13}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    .line 208
    if-nez p1, :cond_6

    .line 209
    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v10, 0x0

    iget-object v12, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    invoke-virtual {v9, v10, v11, v12}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 210
    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v9, v6}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 217
    :cond_3
    :goto_2
    return-void

    .line 83
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "actionBarHelper":Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;
    .end local v3    # "buttonBar":Lcom/google/android/finsky/layout/ButtonBar;
    .end local v4    # "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    .end local v5    # "hostName":Ljava/lang/String;
    .end local v6    # "startUrl":Ljava/lang/String;
    .end local v7    # "title":Ljava/lang/String;
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "backupTitle"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 106
    .restart local v0    # "accountName":Ljava/lang/String;
    .restart local v1    # "actionBarHelper":Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;
    .restart local v3    # "buttonBar":Lcom/google/android/finsky/layout/ButtonBar;
    .restart local v7    # "title":Ljava/lang/String;
    :cond_5
    const/16 v9, 0x8

    invoke-virtual {v3, v9}, Lcom/google/android/finsky/layout/ButtonBar;->setVisibility(I)V

    goto/16 :goto_1

    .line 212
    .restart local v4    # "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    .restart local v5    # "hostName":Ljava/lang/String;
    .restart local v6    # "startUrl":Ljava/lang/String;
    .restart local v8    # "uri":Landroid/net/Uri;
    :cond_6
    const-string v9, "webview_state"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 213
    .local v2, "b":Landroid/os/Bundle;
    if-eqz v2, :cond_3

    .line 214
    iget-object v9, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v9, v2}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    goto :goto_2
.end method

.method public onNegativeButtonClick()V
    .locals 1

    .prologue
    .line 270
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->cancel(Ljava/lang/String;)V

    .line 271
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 310
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 311
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->onNegativeButtonClick()V

    .line 312
    const/4 v0, 0x1

    .line 314
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPositiveButtonClick()V
    .locals 0

    .prologue
    .line 266
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 291
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 292
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 293
    .local v0, "b":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/google/android/finsky/activities/WebViewChallengeActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 294
    const-string v1, "webview_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 295
    return-void
.end method
