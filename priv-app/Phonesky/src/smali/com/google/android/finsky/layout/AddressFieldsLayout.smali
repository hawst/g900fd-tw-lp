.class public abstract Lcom/google/android/finsky/layout/AddressFieldsLayout;
.super Landroid/widget/FrameLayout;
.source "AddressFieldsLayout.java"


# instance fields
.field protected mFieldContainer:Landroid/widget/LinearLayout;

.field protected mProgressBar:Landroid/widget/ProgressBar;

.field protected mUpperRightProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method


# virtual methods
.method public disableOneFieldMode()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public abstract hideUpperRightProgressBar()V
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 41
    const v0, 0x7f0a00b6

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayout;->mProgressBar:Landroid/widget/ProgressBar;

    .line 42
    const v0, 0x7f0a00b5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayout;->mFieldContainer:Landroid/widget/LinearLayout;

    .line 43
    const v0, 0x7f0a00b7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayout;->mUpperRightProgressBar:Landroid/widget/ProgressBar;

    .line 44
    return-void
.end method

.method public abstract setFields(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setOnHeightOffsetChangedListener(Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;)V
.end method

.method public abstract showFields()V
.end method

.method public abstract showProgressBar()V
.end method

.method public abstract showUpperRightProgressBar()V
.end method
