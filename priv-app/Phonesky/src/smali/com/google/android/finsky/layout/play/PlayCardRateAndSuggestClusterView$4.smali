.class Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$4;
.super Lcom/google/android/finsky/utils/PlayAnimationUtils$AnimationListenerAdapter;
.source "PlayCardRateAndSuggestClusterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->transitionToEmptyState(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;)V
    .locals 0

    .prologue
    .line 529
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$4;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    invoke-direct {p0}, Lcom/google/android/finsky/utils/PlayAnimationUtils$AnimationListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$4;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    # getter for: Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;
    invoke-static {v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->access$300(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;)Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->setVisibility(I)V

    .line 535
    sget-boolean v0, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_ANIMATION:Z

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$4;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$4;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    # getter for: Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;
    invoke-static {v1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->access$400(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;)Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    move-result-object v1

    const-wide/16 v2, 0x9c4

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/finsky/utils/UiUtils;->fadeOutCluster(Landroid/view/View;Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;J)V

    .line 539
    :cond_0
    return-void
.end method
