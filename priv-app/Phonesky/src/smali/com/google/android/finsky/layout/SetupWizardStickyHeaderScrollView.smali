.class public Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;
.super Landroid/widget/ScrollView;
.source "SetupWizardStickyHeaderScrollView.java"


# instance fields
.field private mStatusBarInset:I

.field private mSticky:Landroid/view/View;

.field private mStickyContainer:Landroid/view/View;

.field private mStickyRect:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStatusBarInset:I

    .line 35
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyRect:Landroid/graphics/RectF;

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStatusBarInset:I

    .line 35
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyRect:Landroid/graphics/RectF;

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStatusBarInset:I

    .line 35
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyRect:Landroid/graphics/RectF;

    .line 48
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyRect:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    neg-float v0, v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    neg-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyContainer:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 69
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 75
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 76
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->draw(Landroid/graphics/Canvas;)V

    .line 77
    iget-object v5, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mSticky:Landroid/view/View;

    if-eqz v5, :cond_1

    .line 78
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 80
    .local v3, "saveCount":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyContainer:Landroid/view/View;

    if-eqz v5, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyContainer:Landroid/view/View;

    .line 82
    .local v1, "drawTarget":Landroid/view/View;
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyContainer:Landroid/view/View;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mSticky:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v0

    .line 84
    .local v0, "drawOffset":I
    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->getScrollY()I

    move-result v6

    sub-int v2, v5, v6

    .line 85
    .local v2, "drawTop":I
    add-int v5, v2, v0

    iget v6, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStatusBarInset:I

    if-lt v5, v6, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v5

    if-nez v5, :cond_4

    .line 87
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyRect:Landroid/graphics/RectF;

    neg-int v6, v0

    iget v7, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStatusBarInset:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v8

    sub-int/2addr v8, v0

    iget v9, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStatusBarInset:I

    add-int/2addr v8, v9

    int-to-float v8, v8

    invoke-virtual {v5, v10, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 89
    neg-int v5, v2

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v5, v6

    invoke-virtual {p1, v10, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 90
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v6

    invoke-virtual {p1, v4, v4, v5, v6}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 91
    invoke-virtual {v1, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 95
    :goto_2
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 98
    .end local v0    # "drawOffset":I
    .end local v1    # "drawTarget":Landroid/view/View;
    .end local v2    # "drawTop":I
    .end local v3    # "saveCount":I
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 99
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->onDrawScrollBars(Landroid/graphics/Canvas;)V

    .line 100
    return-void

    .line 80
    .restart local v3    # "saveCount":I
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mSticky:Landroid/view/View;

    goto :goto_0

    .restart local v1    # "drawTarget":Landroid/view/View;
    :cond_3
    move v0, v4

    .line 82
    goto :goto_1

    .line 93
    .restart local v0    # "drawOffset":I
    .restart local v2    # "drawTop":I
    :cond_4
    iget-object v4, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->setEmpty()V

    goto :goto_2
.end method

.method public onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 1
    .param p1, "insets"    # Landroid/view/WindowInsets;

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->getFitsSystemWindows()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStatusBarInset:I

    .line 107
    :cond_0
    return-object p1
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 52
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    .line 53
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mSticky:Landroid/view/View;

    if-nez v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->updateStickyView()V

    .line 56
    :cond_0
    return-void
.end method

.method public updateStickyView()V
    .locals 1

    .prologue
    .line 59
    const-string v0, "sticky"

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mSticky:Landroid/view/View;

    .line 60
    const-string v0, "stickyContainer"

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardStickyHeaderScrollView;->mStickyContainer:Landroid/view/View;

    .line 61
    return-void
.end method
