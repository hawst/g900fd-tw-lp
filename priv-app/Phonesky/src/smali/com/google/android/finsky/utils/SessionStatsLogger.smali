.class public Lcom/google/android/finsky/utils/SessionStatsLogger;
.super Ljava/lang/Object;
.source "SessionStatsLogger.java"


# static fields
.field private static sHaveLoggedSessionStats:Z


# direct methods
.method public static logSessionStatsIfNecessary(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    sget-boolean v1, Lcom/google/android/finsky/utils/SessionStatsLogger;->sHaveLoggedSessionStats:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/finsky/config/G;->enableSessionStatsLog:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 40
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/finsky/utils/SessionStatsLogger;->sHaveLoggedSessionStats:Z

    .line 36
    :try_start_0
    invoke-static {p0}, Lcom/google/android/finsky/utils/SessionStatsLogger;->logSessionStatsImpl(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Fatal exception while logging session stats"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static logSessionStatsImpl(Landroid/content/Context;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x0

    const/4 v13, -0x1

    const/4 v10, 0x1

    .line 45
    new-instance v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    invoke-direct {v7}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;-><init>()V

    .line 47
    .local v7, "sessionData":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;
    sget-object v9, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v9}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 48
    .local v3, "isGlobalAutoUpdateEnabled":Z
    iput-boolean v3, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateEnabled:Z

    .line 49
    iput-boolean v10, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateEnabled:Z

    .line 51
    sget-object v9, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v9}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    iput-boolean v9, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateOverWifiOnly:Z

    .line 52
    iput-boolean v10, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateOverWifiOnly:Z

    .line 54
    sget-object v9, Lcom/google/android/finsky/utils/FinskyPreferences;->autoUpdateMigrationDialogShownCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v9}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->autoUpdateCleanupDialogNumTimesShown:I

    .line 56
    iput-boolean v10, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasAutoUpdateCleanupDialogNumTimesShown:Z

    .line 59
    invoke-static {p0}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 60
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 61
    array-length v9, v0

    iput v9, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->numAccountsOnDevice:I

    .line 62
    iput-boolean v10, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNumAccountsOnDevice:Z

    .line 68
    :cond_0
    invoke-static {p0}, Lcom/google/android/finsky/receivers/NetworkStateChangedReceiver;->getCachedNetworkInfo(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 69
    .local v2, "info":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_1

    .line 70
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v9

    iput v9, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkType:I

    .line 71
    iput-boolean v10, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNetworkType:Z

    .line 73
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v9

    iput v9, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->networkSubType:I

    .line 74
    iput-boolean v10, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasNetworkSubType:Z

    .line 77
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "currentAccountName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 79
    invoke-static {v1}, Lcom/google/android/finsky/config/PurchaseAuth;->getPurchaseAuthType(Ljava/lang/String;)I

    move-result v9

    iput v9, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->purchaseAuthType:I

    .line 80
    iput-boolean v10, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasPurchaseAuthType:Z

    .line 84
    :cond_2
    sget-object v9, Lcom/google/android/finsky/utils/FinskyPreferences;->contentFilterLevel:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v9}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->contentFilterLevel:I

    .line 85
    iput-boolean v10, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasContentFilterLevel:Z

    .line 89
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0x11

    if-lt v9, v12, :cond_4

    .line 90
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v12, "install_non_market_apps"

    invoke-static {v9, v12, v13}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    .line 97
    .local v8, "unknownSourcesValue":I
    :goto_0
    if-ne v8, v13, :cond_5

    .line 98
    const-string v9, "Couldn\'t obtain INSTALL_NON_MARKET_APPS value"

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v9, v11}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    :goto_1
    invoke-static {v1}, Lcom/google/android/finsky/billing/PromptForFopHelper;->getSessionLoggingData(Ljava/lang/String;)Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

    move-result-object v9

    iput-object v9, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->promptForFopData:Lcom/google/android/finsky/analytics/PlayStore$PromptForFopData;

    .line 108
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const-string v11, "config_downloadDataDirSize"

    const-string v12, "integer"

    const-string v13, "android"

    invoke-virtual {v9, v11, v12, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 110
    .local v6, "resId":I
    if-eqz v6, :cond_3

    .line 111
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->downloadDataDirSizeMb:I

    .line 112
    iput-boolean v10, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasDownloadDataDirSizeMb:Z

    .line 115
    :cond_3
    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "download_manager_max_bytes_over_mobile"

    invoke-static {v9, v10}, Landroid/provider/Settings$Secure;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v4

    .line 118
    .local v4, "limit":J
    iput-wide v4, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->recommendedMaxDownloadOverMobileBytes:J

    .line 119
    const/4 v9, 0x1

    iput-boolean v9, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasRecommendedMaxDownloadOverMobileBytes:Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    .end local v4    # "limit":J
    :goto_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v9

    invoke-virtual {v9, v7}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logSessionData(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;)V

    .line 124
    return-void

    .line 94
    .end local v6    # "resId":I
    .end local v8    # "unknownSourcesValue":I
    :cond_4
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v12, "install_non_market_apps"

    invoke-static {v9, v12, v13}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    .restart local v8    # "unknownSourcesValue":I
    goto :goto_0

    .line 100
    :cond_5
    if-eqz v8, :cond_6

    move v9, v10

    :goto_3
    iput-boolean v9, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->allowUnknownSources:Z

    .line 101
    iput-boolean v10, v7, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;->hasAllowUnknownSources:Z

    goto :goto_1

    :cond_6
    move v9, v11

    .line 100
    goto :goto_3

    .line 120
    .restart local v6    # "resId":I
    :catch_0
    move-exception v9

    goto :goto_2
.end method
