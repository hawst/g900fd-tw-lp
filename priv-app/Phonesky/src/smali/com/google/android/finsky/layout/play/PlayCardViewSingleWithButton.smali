.class public Lcom/google/android/finsky/layout/play/PlayCardViewSingleWithButton;
.super Lcom/google/android/play/layout/PlayCardViewBase;
.source "PlayCardViewSingleWithButton.java"


# instance fields
.field protected mActionButton:Lcom/google/android/play/layout/PlayActionButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewSingleWithButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method


# virtual methods
.method public bindLoading()V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->bindLoading()V

    .line 44
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSingleWithButton;->mActionButton:Lcom/google/android/play/layout/PlayActionButton;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSingleWithButton;->mActionButton:Lcom/google/android/play/layout/PlayActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 47
    :cond_0
    return-void
.end method

.method public getCardType()I
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0xe

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->onFinishInflate()V

    .line 38
    const v0, 0x7f0a02c3

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewSingleWithButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSingleWithButton;->mActionButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 39
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 51
    invoke-virtual {p0, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewSingleWithButton;->measureThumbnailSpanningHeight(I)V

    .line 53
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;->onMeasure(II)V

    .line 54
    return-void
.end method
