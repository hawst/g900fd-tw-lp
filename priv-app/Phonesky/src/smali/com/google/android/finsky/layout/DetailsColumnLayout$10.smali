.class Lcom/google/android/finsky/layout/DetailsColumnLayout$10;
.super Ljava/lang/Object;
.source "DetailsColumnLayout.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsColumnLayout;->collapseCurrentlyExpandedSectionIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;)V
    .locals 0

    .prologue
    .line 540
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$10;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 7
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 543
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    .line 544
    .local v2, "fraction":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$10;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v5, v5, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildCount()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 545
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$10;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v5, v5, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v5, v3}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 546
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    .line 547
    .local v1, "childId":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$10;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsAbove:Ljava/util/Set;
    invoke-static {v5}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$400(Lcom/google/android/finsky/layout/DetailsColumnLayout;)Ljava/util/Set;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 548
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$10;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceTop:I
    invoke-static {v5}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$200(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    float-to-int v4, v5

    .line 549
    .local v4, "newTraveledDist":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$10;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceTop:I
    invoke-static {v5}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$200(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v5

    sub-int v5, v4, v5

    int-to-float v5, v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 551
    .end local v4    # "newTraveledDist":I
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$10;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsBelow:Ljava/util/Set;
    invoke-static {v5}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$500(Lcom/google/android/finsky/layout/DetailsColumnLayout;)Ljava/util/Set;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 552
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$10;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceBottom:I
    invoke-static {v5}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$600(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    float-to-int v4, v5

    .line 553
    .restart local v4    # "newTraveledDist":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$10;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceBottom:I
    invoke-static {v5}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$600(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v5

    sub-int/2addr v5, v4

    int-to-float v5, v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 544
    .end local v4    # "newTraveledDist":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 556
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childId":I
    :cond_2
    return-void
.end method
