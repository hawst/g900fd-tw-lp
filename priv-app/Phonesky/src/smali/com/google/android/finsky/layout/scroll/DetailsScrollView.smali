.class public Lcom/google/android/finsky/layout/scroll/DetailsScrollView;
.super Lcom/google/android/finsky/layout/ObservableScrollView;
.source "DetailsScrollView.java"

# interfaces
.implements Lcom/google/android/play/image/FifeImageView$OnLoadedListener;


# instance fields
.field private mAutoStartExpandTransition:Z

.field private mCollapserAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

.field private final mDefaultHeroImageHeight:I

.field private mDocBackend:I

.field private mExpanderAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

.field private mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

.field private mParent:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/ObservableScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mDefaultHeroImageHeight:I

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mDefaultHeroImageHeight:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)Lcom/google/android/finsky/layout/HeroGraphicView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mDocBackend:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mCollapserAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/scroll/DetailsScrollView;
    .param p1, "x1"    # Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mCollapserAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/scroll/DetailsScrollView;
    .param p1, "x1"    # Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->cancelScrollAnimationIfWaiting(Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mParent:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mExpanderAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    return-object v0
.end method

.method private cancelScrollAnimationIfWaiting(Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;)V
    .locals 1
    .param p1, "scrollAnimation"    # Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    .prologue
    .line 174
    invoke-virtual {p1}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->hasStartedScrolling()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    invoke-virtual {p1}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->cancel()V

    .line 177
    :cond_0
    return-void
.end method


# virtual methods
.method public bindDetailsHero(Landroid/view/ViewGroup;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/HeroGraphicView;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZZ)V
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "heroGraphicView"    # Lcom/google/android/finsky/layout/HeroGraphicView;
    .param p4, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p5, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p6, "autoStartExpandTransition"    # Z
    .param p7, "startInExpandedMode"    # Z

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mParent:Landroid/view/ViewGroup;

    .line 66
    iput-object p3, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    .line 67
    iput-boolean p6, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mAutoStartExpandTransition:Z

    .line 68
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mDocBackend:I

    .line 69
    if-nez p7, :cond_0

    .line 70
    invoke-virtual {p3}, Lcom/google/android/finsky/layout/HeroGraphicView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$1;

    invoke-direct {v1, p0, p3}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$1;-><init>(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;Lcom/google/android/finsky/layout/HeroGraphicView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 85
    :cond_0
    const/4 v4, 0x1

    move-object v0, p3

    move-object v1, p2

    move-object v2, p4

    move-object v3, p5

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/HeroGraphicView;->bindDetailsDefault(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZLcom/google/android/play/image/FifeImageView$OnLoadedListener;)V

    .line 86
    return-void
.end method

.method public cancelScrollAnimationsIfRunning()V
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mAutoStartExpandTransition:Z

    .line 184
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mExpanderAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mExpanderAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mExpanderAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->cancel()V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mCollapserAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mCollapserAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mCollapserAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->cancel()V

    .line 190
    :cond_1
    return-void
.end method

.method public onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
    .locals 11
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 92
    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mHeroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    new-instance v2, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$2;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$2;-><init>(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)V

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/HeroGraphicView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-boolean v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mAutoStartExpandTransition:Z

    if-nez v1, :cond_0

    .line 166
    :goto_0
    return-void

    .line 112
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v10

    .line 113
    .local v10, "imageWidth":I
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 114
    .local v0, "aspectRatio":F
    int-to-float v1, v10

    div-float/2addr v1, v0

    float-to-int v9, v1

    .line 115
    .local v9, "imageHeight":I
    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x12c

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->getScrollY()I

    move-result v1

    neg-int v6, v1

    new-instance v7, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;

    invoke-direct {v7, p0, v9}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$3;-><init>(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;I)V

    move-object v1, p0

    invoke-static/range {v1 .. v7}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getVerticalScrollByAnimation(Landroid/widget/ScrollView;JJILandroid/view/animation/Animation$AnimationListener;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mExpanderAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    .line 154
    new-instance v8, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$4;

    invoke-direct {v8, p0}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$4;-><init>(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)V

    .line 161
    .local v8, "expanderGuard":Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->addOnScrollListener(Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;)V

    .line 165
    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mParent:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mExpanderAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public onLoadedAndFadedIn(Lcom/google/android/play/image/FifeImageView;)V
    .locals 0
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;

    .prologue
    .line 170
    return-void
.end method
