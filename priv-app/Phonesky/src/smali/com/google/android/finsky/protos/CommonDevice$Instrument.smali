.class public final Lcom/google/android/finsky/protos/CommonDevice$Instrument;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Instrument"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;


# instance fields
.field public billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

.field public billingAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

.field public carrierBilling:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

.field public carrierBillingStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

.field public creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

.field public disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

.field public displayTitle:Ljava/lang/String;

.field public externalInstrumentId:Ljava/lang/String;

.field public hasDisplayTitle:Z

.field public hasExternalInstrumentId:Z

.field public hasInstrumentFamily:Z

.field public hasStatusDescription:Z

.field public hasVersion:Z

.field public iconImage:Lcom/google/android/finsky/protos/Common$Image;

.field public instrumentFamily:I

.field public statusDescription:Ljava/lang/String;

.field public storedValue:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

.field public topupInfoDeprecated:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

.field public version:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 196
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->clear()Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 197
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 2

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-nez v0, :cond_1

    .line 138
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 140
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    sput-object v0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 143
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    return-object v0

    .line 143
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 200
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId:Ljava/lang/String;

    .line 201
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasExternalInstrumentId:Z

    .line 202
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle:Ljava/lang/String;

    .line 203
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasDisplayTitle:Z

    .line 204
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 205
    iput v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    .line 206
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasInstrumentFamily:Z

    .line 207
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 208
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    .line 209
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    .line 210
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    .line 211
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    .line 212
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    .line 213
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->topupInfoDeprecated:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    .line 214
    iput v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->version:I

    .line 215
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasVersion:Z

    .line 216
    invoke-static {}, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->emptyArray()[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    .line 217
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->statusDescription:Ljava/lang/String;

    .line 218
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasStatusDescription:Z

    .line 219
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->cachedSize:I

    .line 220
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 278
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 279
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasExternalInstrumentId:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 280
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 283
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v3, :cond_2

    .line 284
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 287
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    if-eqz v3, :cond_3

    .line 288
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 291
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    if-eqz v3, :cond_4

    .line 292
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 295
    :cond_4
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    if-eqz v3, :cond_5

    .line 296
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 299
    :cond_5
    iget v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    if-nez v3, :cond_6

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasInstrumentFamily:Z

    if-eqz v3, :cond_7

    .line 300
    :cond_6
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 303
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    if-eqz v3, :cond_8

    .line 304
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 307
    :cond_8
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasDisplayTitle:Z

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 308
    :cond_9
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 311
    :cond_a
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->topupInfoDeprecated:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    if-eqz v3, :cond_b

    .line 312
    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->topupInfoDeprecated:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 315
    :cond_b
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasVersion:Z

    if-nez v3, :cond_c

    iget v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->version:I

    if-eqz v3, :cond_d

    .line 316
    :cond_c
    const/16 v3, 0xa

    iget v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->version:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 319
    :cond_d
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    if-eqz v3, :cond_e

    .line 320
    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 323
    :cond_e
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    array-length v3, v3

    if-lez v3, :cond_10

    .line 324
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    array-length v3, v3

    if-ge v1, v3, :cond_10

    .line 325
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    aget-object v0, v3, v1

    .line 326
    .local v0, "element":Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    if-eqz v0, :cond_f

    .line 327
    const/16 v3, 0xc

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 324
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 332
    .end local v0    # "element":Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    .end local v1    # "i":I
    :cond_10
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasStatusDescription:Z

    if-nez v3, :cond_11

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->statusDescription:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    .line 333
    :cond_11
    const/16 v3, 0xd

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->statusDescription:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 336
    :cond_12
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v3, :cond_13

    .line 337
    const/16 v3, 0xe

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 340
    :cond_13
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 348
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 349
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 353
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 354
    :sswitch_0
    return-object p0

    .line 359
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId:Ljava/lang/String;

    .line 360
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasExternalInstrumentId:Z

    goto :goto_0

    .line 364
    :sswitch_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-nez v6, :cond_1

    .line 365
    new-instance v6, Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/BillingAddress$Address;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 367
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 371
    :sswitch_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    if-nez v6, :cond_2

    .line 372
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    .line 374
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 378
    :sswitch_4
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    if-nez v6, :cond_3

    .line 379
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    .line 381
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 385
    :sswitch_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    if-nez v6, :cond_4

    .line 386
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    .line 388
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 392
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 393
    .local v4, "value":I
    sparse-switch v4, :sswitch_data_1

    goto :goto_0

    .line 405
    :sswitch_7
    iput v4, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    .line 406
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasInstrumentFamily:Z

    goto :goto_0

    .line 412
    .end local v4    # "value":I
    :sswitch_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    if-nez v6, :cond_5

    .line 413
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    .line 415
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 419
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle:Ljava/lang/String;

    .line 420
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasDisplayTitle:Z

    goto/16 :goto_0

    .line 424
    :sswitch_a
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->topupInfoDeprecated:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    if-nez v6, :cond_6

    .line 425
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->topupInfoDeprecated:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    .line 427
    :cond_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->topupInfoDeprecated:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 431
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->version:I

    .line 432
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasVersion:Z

    goto/16 :goto_0

    .line 436
    :sswitch_c
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    if-nez v6, :cond_7

    .line 437
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    .line 439
    :cond_7
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 443
    :sswitch_d
    const/16 v6, 0x62

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 445
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    if-nez v6, :cond_9

    move v1, v5

    .line 446
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    .line 448
    .local v2, "newArray":[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    if-eqz v1, :cond_8

    .line 449
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 451
    :cond_8
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_a

    .line 452
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;-><init>()V

    aput-object v6, v2, v1

    .line 453
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 454
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 451
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 445
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    :cond_9
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    array-length v1, v6

    goto :goto_1

    .line 457
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    :cond_a
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;-><init>()V

    aput-object v6, v2, v1

    .line 458
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 459
    iput-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    goto/16 :goto_0

    .line 463
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->statusDescription:Ljava/lang/String;

    .line 464
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasStatusDescription:Z

    goto/16 :goto_0

    .line 468
    :sswitch_f
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v6, :cond_b

    .line 469
    new-instance v6, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 471
    :cond_b
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 349
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
        0x50 -> :sswitch_b
        0x5a -> :sswitch_c
        0x62 -> :sswitch_d
        0x6a -> :sswitch_e
        0x72 -> :sswitch_f
    .end sparse-switch

    .line 393
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_7
        0x1 -> :sswitch_7
        0x2 -> :sswitch_7
        0x3 -> :sswitch_7
        0x4 -> :sswitch_7
        0x5 -> :sswitch_7
        0x6 -> :sswitch_7
        0x7 -> :sswitch_7
        0x8 -> :sswitch_7
        0x9 -> :sswitch_7
        0x64 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasExternalInstrumentId:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 227
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 229
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v2, :cond_2

    .line 230
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 232
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    if-eqz v2, :cond_3

    .line 233
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 235
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    if-eqz v2, :cond_4

    .line 236
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 238
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    if-eqz v2, :cond_5

    .line 239
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 241
    :cond_5
    iget v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    if-nez v2, :cond_6

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasInstrumentFamily:Z

    if-eqz v2, :cond_7

    .line 242
    :cond_6
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 244
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    if-eqz v2, :cond_8

    .line 245
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 247
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasDisplayTitle:Z

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 248
    :cond_9
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 250
    :cond_a
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->topupInfoDeprecated:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    if-eqz v2, :cond_b

    .line 251
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->topupInfoDeprecated:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 253
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasVersion:Z

    if-nez v2, :cond_c

    iget v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->version:I

    if-eqz v2, :cond_d

    .line 254
    :cond_c
    const/16 v2, 0xa

    iget v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->version:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 256
    :cond_d
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    if-eqz v2, :cond_e

    .line 257
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 259
    :cond_e
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    array-length v2, v2

    if-lez v2, :cond_10

    .line 260
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_10

    .line 261
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    aget-object v0, v2, v1

    .line 262
    .local v0, "element":Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    if-eqz v0, :cond_f

    .line 263
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 260
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 267
    .end local v0    # "element":Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    .end local v1    # "i":I
    :cond_10
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasStatusDescription:Z

    if-nez v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->statusDescription:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 268
    :cond_11
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->statusDescription:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 270
    :cond_12
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v2, :cond_13

    .line 271
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 273
    :cond_13
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 274
    return-void
.end method
