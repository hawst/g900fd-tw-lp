.class public final Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstallAsset"
.end annotation


# instance fields
.field public assetId:Ljava/lang/String;

.field public assetName:Ljava/lang/String;

.field public assetPackage:Ljava/lang/String;

.field public assetSignature:Ljava/lang/String;

.field public assetSize:J

.field public assetType:Ljava/lang/String;

.field public blobUrl:Ljava/lang/String;

.field public downloadAuthCookieName:Ljava/lang/String;

.field public downloadAuthCookieValue:Ljava/lang/String;

.field public forwardLocked:Z

.field public hasAssetId:Z

.field public hasAssetName:Z

.field public hasAssetPackage:Z

.field public hasAssetSignature:Z

.field public hasAssetSize:Z

.field public hasAssetType:Z

.field public hasBlobUrl:Z

.field public hasDownloadAuthCookieName:Z

.field public hasDownloadAuthCookieValue:Z

.field public hasForwardLocked:Z

.field public hasPostInstallRefundWindowMillis:Z

.field public hasRefundTimeoutMillis:Z

.field public hasSecured:Z

.field public hasVersionCode:Z

.field public postInstallRefundWindowMillis:J

.field public refundTimeoutMillis:J

.field public secured:Z

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1745
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1746
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->clear()Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;

    .line 1747
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1750
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetId:Ljava/lang/String;

    .line 1751
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetId:Z

    .line 1752
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetName:Ljava/lang/String;

    .line 1753
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetName:Z

    .line 1754
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetType:Ljava/lang/String;

    .line 1755
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetType:Z

    .line 1756
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetPackage:Ljava/lang/String;

    .line 1757
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetPackage:Z

    .line 1758
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->blobUrl:Ljava/lang/String;

    .line 1759
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasBlobUrl:Z

    .line 1760
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSignature:Ljava/lang/String;

    .line 1761
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetSignature:Z

    .line 1762
    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSize:J

    .line 1763
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetSize:Z

    .line 1764
    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->refundTimeoutMillis:J

    .line 1765
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasRefundTimeoutMillis:Z

    .line 1766
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->forwardLocked:Z

    .line 1767
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasForwardLocked:Z

    .line 1768
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->secured:Z

    .line 1769
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasSecured:Z

    .line 1770
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->versionCode:I

    .line 1771
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasVersionCode:Z

    .line 1772
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieName:Ljava/lang/String;

    .line 1773
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasDownloadAuthCookieName:Z

    .line 1774
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieValue:Ljava/lang/String;

    .line 1775
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasDownloadAuthCookieValue:Z

    .line 1776
    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->postInstallRefundWindowMillis:J

    .line 1777
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasPostInstallRefundWindowMillis:Z

    .line 1778
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->cachedSize:I

    .line 1779
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1832
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1833
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetId:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1834
    :cond_0
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1837
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetName:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1838
    :cond_2
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1841
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetType:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1842
    :cond_4
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetType:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1845
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetPackage:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetPackage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1846
    :cond_6
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetPackage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1849
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasBlobUrl:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->blobUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1850
    :cond_8
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->blobUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1853
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetSignature:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSignature:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1854
    :cond_a
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSignature:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1857
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetSize:Z

    if-nez v1, :cond_c

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSize:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_d

    .line 1858
    :cond_c
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSize:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1861
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasRefundTimeoutMillis:Z

    if-nez v1, :cond_e

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->refundTimeoutMillis:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_f

    .line 1862
    :cond_e
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->refundTimeoutMillis:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1865
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasForwardLocked:Z

    if-nez v1, :cond_10

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->forwardLocked:Z

    if-eqz v1, :cond_11

    .line 1866
    :cond_10
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->forwardLocked:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1869
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasSecured:Z

    if-nez v1, :cond_12

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->secured:Z

    if-eqz v1, :cond_13

    .line 1870
    :cond_12
    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->secured:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1873
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasVersionCode:Z

    if-nez v1, :cond_14

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->versionCode:I

    if-eqz v1, :cond_15

    .line 1874
    :cond_14
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->versionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1877
    :cond_15
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasDownloadAuthCookieName:Z

    if-nez v1, :cond_16

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 1878
    :cond_16
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1881
    :cond_17
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasDownloadAuthCookieValue:Z

    if-nez v1, :cond_18

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieValue:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 1882
    :cond_18
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieValue:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1885
    :cond_19
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasPostInstallRefundWindowMillis:Z

    if-nez v1, :cond_1a

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->postInstallRefundWindowMillis:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1b

    .line 1886
    :cond_1a
    const/16 v1, 0x10

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->postInstallRefundWindowMillis:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1889
    :cond_1b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1897
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1898
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1902
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1903
    :sswitch_0
    return-object p0

    .line 1908
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetId:Ljava/lang/String;

    .line 1909
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetId:Z

    goto :goto_0

    .line 1913
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetName:Ljava/lang/String;

    .line 1914
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetName:Z

    goto :goto_0

    .line 1918
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetType:Ljava/lang/String;

    .line 1919
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetType:Z

    goto :goto_0

    .line 1923
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetPackage:Ljava/lang/String;

    .line 1924
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetPackage:Z

    goto :goto_0

    .line 1928
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->blobUrl:Ljava/lang/String;

    .line 1929
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasBlobUrl:Z

    goto :goto_0

    .line 1933
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSignature:Ljava/lang/String;

    .line 1934
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetSignature:Z

    goto :goto_0

    .line 1938
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSize:J

    .line 1939
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetSize:Z

    goto :goto_0

    .line 1943
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->refundTimeoutMillis:J

    .line 1944
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasRefundTimeoutMillis:Z

    goto :goto_0

    .line 1948
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->forwardLocked:Z

    .line 1949
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasForwardLocked:Z

    goto :goto_0

    .line 1953
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->secured:Z

    .line 1954
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasSecured:Z

    goto :goto_0

    .line 1958
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->versionCode:I

    .line 1959
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasVersionCode:Z

    goto :goto_0

    .line 1963
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieName:Ljava/lang/String;

    .line 1964
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasDownloadAuthCookieName:Z

    goto :goto_0

    .line 1968
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieValue:Ljava/lang/String;

    .line 1969
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasDownloadAuthCookieValue:Z

    goto/16 :goto_0

    .line 1973
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->postInstallRefundWindowMillis:J

    .line 1974
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasPostInstallRefundWindowMillis:Z

    goto/16 :goto_0

    .line 1898
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x80 -> :sswitch_e
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1672
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 1785
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetId:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1786
    :cond_0
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1788
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetName:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1789
    :cond_2
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1791
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetType:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetType:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1792
    :cond_4
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1794
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetPackage:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetPackage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1795
    :cond_6
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetPackage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1797
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasBlobUrl:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->blobUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1798
    :cond_8
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->blobUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1800
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetSignature:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSignature:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1801
    :cond_a
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSignature:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1803
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasAssetSize:Z

    if-nez v0, :cond_c

    iget-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSize:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_d

    .line 1804
    :cond_c
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->assetSize:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1806
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasRefundTimeoutMillis:Z

    if-nez v0, :cond_e

    iget-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->refundTimeoutMillis:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_f

    .line 1807
    :cond_e
    const/16 v0, 0x9

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->refundTimeoutMillis:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1809
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasForwardLocked:Z

    if-nez v0, :cond_10

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->forwardLocked:Z

    if-eqz v0, :cond_11

    .line 1810
    :cond_10
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->forwardLocked:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1812
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasSecured:Z

    if-nez v0, :cond_12

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->secured:Z

    if-eqz v0, :cond_13

    .line 1813
    :cond_12
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->secured:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1815
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasVersionCode:Z

    if-nez v0, :cond_14

    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->versionCode:I

    if-eqz v0, :cond_15

    .line 1816
    :cond_14
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1818
    :cond_15
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasDownloadAuthCookieName:Z

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 1819
    :cond_16
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1821
    :cond_17
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasDownloadAuthCookieValue:Z

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieValue:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 1822
    :cond_18
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->downloadAuthCookieValue:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1824
    :cond_19
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->hasPostInstallRefundWindowMillis:Z

    if-nez v0, :cond_1a

    iget-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->postInstallRefundWindowMillis:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1b

    .line 1825
    :cond_1a
    const/16 v0, 0x10

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;->postInstallRefundWindowMillis:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1827
    :cond_1b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1828
    return-void
.end method
