.class public final Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocAnnotations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocAnnotations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseHistoryDetails"
.end annotation


# instance fields
.field public hasPurchaseDetailsHtml:Z

.field public hasPurchaseStatus:Z

.field public hasPurchaseTimestampMsec:Z

.field public offer:Lcom/google/android/finsky/protos/Common$Offer;

.field public purchaseDetailsHtml:Ljava/lang/String;

.field public purchaseStatus:Ljava/lang/String;

.field public purchaseTimestampMsec:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 921
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 922
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->clear()Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    .line 923
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 926
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseTimestampMsec:J

    .line 927
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseTimestampMsec:Z

    .line 928
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseDetailsHtml:Ljava/lang/String;

    .line 929
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseDetailsHtml:Z

    .line 930
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    .line 931
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseStatus:Ljava/lang/String;

    .line 932
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseStatus:Z

    .line 933
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->cachedSize:I

    .line 934
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 957
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 958
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseTimestampMsec:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseTimestampMsec:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 959
    :cond_0
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseTimestampMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 962
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseDetailsHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseDetailsHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 963
    :cond_2
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseDetailsHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 966
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v1, :cond_4

    .line 967
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 970
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseStatus:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseStatus:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 971
    :cond_5
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseStatus:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 974
    :cond_6
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 982
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 983
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 987
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 988
    :sswitch_0
    return-object p0

    .line 993
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseTimestampMsec:J

    .line 994
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseTimestampMsec:Z

    goto :goto_0

    .line 998
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseDetailsHtml:Ljava/lang/String;

    .line 999
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseDetailsHtml:Z

    goto :goto_0

    .line 1003
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v1, :cond_1

    .line 1004
    new-instance v1, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    .line 1006
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1010
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseStatus:Ljava/lang/String;

    .line 1011
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseStatus:Z

    goto :goto_0

    .line 983
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 889
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 940
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseTimestampMsec:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseTimestampMsec:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 941
    :cond_0
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 943
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseDetailsHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseDetailsHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 944
    :cond_2
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseDetailsHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 946
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v0, :cond_4

    .line 947
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 949
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseStatus:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseStatus:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 950
    :cond_5
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseStatus:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 952
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 953
    return-void
.end method
