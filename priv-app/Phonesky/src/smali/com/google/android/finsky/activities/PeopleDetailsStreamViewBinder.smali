.class public Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;
.super Lcom/google/android/finsky/fragments/DetailsViewBinder;
.source "PeopleDetailsStreamViewBinder.java"

# interfaces
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# instance fields
.field private mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

.field private mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

.field private mDfeList:Lcom/google/android/finsky/api/model/DfeList;

.field private mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private mIsAdapterSet:Z

.field private mLastRequestTimeMs:J

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

.field private mRecyclerViewRestoreBundle:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/DetailsViewBinder;-><init>()V

    .line 47
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->requestData()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method private getStreamList()Lcom/google/android/finsky/api/model/DfeList;
    .locals 4

    .prologue
    .line 117
    new-instance v0, Lcom/google/android/finsky/api/model/DfeList;

    iget-object v1, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getCoreContentListUrl()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/api/model/DfeList;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V

    return-object v0
.end method

.method private rebindAdapter()V
    .locals 11

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    if-nez v0, :cond_1

    .line 146
    const-string v0, "List view null, ignoring."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->hasRestoreData(Landroid/os/Bundle;)Z

    move-result v9

    .line 151
    .local v9, "isRestoring":Z
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    if-nez v0, :cond_2

    .line 153
    new-instance v0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v4, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v5, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v6, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v7, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    iget-object v8, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    iget-object v10, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;-><init>(Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/DfeList;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    .line 158
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mIsAdapterSet:Z

    if-nez v0, :cond_3

    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mIsAdapterSet:Z

    .line 162
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    new-instance v1, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;

    invoke-direct {v1, p0, v9}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;-><init>(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 178
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->updateAdapterData(Lcom/google/android/finsky/api/model/ContainerList;)V

    goto :goto_0
.end method

.method private requestData()V
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->clearDfeList()V

    .line 109
    invoke-direct {p0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->getStreamList()Lcom/google/android/finsky/api/model/DfeList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    .line 110
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    .line 113
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mLastRequestTimeMs:J

    .line 114
    return-void
.end method


# virtual methods
.method public bind(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;)V
    .locals 5
    .param p1, "streamContainer"    # Landroid/view/View;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 74
    const/4 v2, 0x0

    invoke-super {p0, p1, p2, v2}, Lcom/google/android/finsky/fragments/DetailsViewBinder;->bind(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)V

    .line 75
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    if-nez v2, :cond_0

    .line 77
    new-instance v2, Lcom/google/android/finsky/layout/LayoutSwitcher;

    const v3, 0x7f0a02aa

    new-instance v4, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$1;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$1;-><init>(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)V

    invoke-direct {v2, p1, v3, v4}, Lcom/google/android/finsky/layout/LayoutSwitcher;-><init>(Landroid/view/View;ILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;)V

    iput-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    .line 88
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getPersonDetails()Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    move-result-object v1

    .line 89
    .local v1, "personDetails":Lcom/google/android/finsky/protos/DocDetails$PersonDetails;
    if-eqz v1, :cond_3

    iget-boolean v2, v1, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->personIsRequester:Z

    if-eqz v2, :cond_3

    const/4 v0, 0x1

    .line 90
    .local v0, "isOwnProfile":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 91
    iget-wide v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mLastRequestTimeMs:J

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/MyPeoplePageHelper;->hasMutationOccurredSince(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 92
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->clearDfeList()V

    .line 94
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    invoke-virtual {v2}, Landroid/os/Bundle;->clear()V

    .line 98
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeList;->isReady()Z

    move-result v2

    if-nez v2, :cond_4

    .line 99
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToLoadingMode()V

    .line 100
    invoke-direct {p0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->requestData()V

    .line 105
    :goto_1
    return-void

    .line 89
    .end local v0    # "isOwnProfile":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 102
    .restart local v0    # "isOwnProfile":Z
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToDataMode()V

    .line 103
    invoke-direct {p0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->rebindAdapter()V

    goto :goto_1
.end method

.method protected clearDfeList()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    .line 126
    :cond_0
    return-void
.end method

.method public init(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "api"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p5, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 52
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/fragments/DetailsViewBinder;->init(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 53
    iput-object p4, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 54
    iput-object p5, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 55
    iput-object p6, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    .line 56
    iput-object p7, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 57
    return-void
.end method

.method public initRecyclerView(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView;
    .locals 3
    .param p1, "container"    # Landroid/view/View;

    .prologue
    .line 60
    const v0, 0x7f0a02aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    iput-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    .line 62
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    if-nez v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    new-instance v1, Lcom/google/android/finsky/adapters/EmptyRecyclerViewAdapter;

    invoke-direct {v1}, Lcom/google/android/finsky/adapters/EmptyRecyclerViewAdapter;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    return-object v0
.end method

.method public onDataChanged()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToDataMode()V

    .line 133
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->rebindAdapter()V

    .line 134
    return-void
.end method

.method public onDestroyView()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 197
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    if-eqz v2, :cond_0

    .line 199
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    iget-object v3, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    iget-object v4, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->onSaveInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V

    .line 202
    :cond_0
    iput-object v5, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    .line 203
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    if-eqz v2, :cond_1

    .line 204
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->onDestroyView()V

    .line 205
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->onDestroy()V

    .line 206
    iput-object v5, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    .line 207
    iput-boolean v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mIsAdapterSet:Z

    .line 212
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v2, :cond_3

    .line 213
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getPersonDetails()Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    move-result-object v1

    .line 214
    .local v1, "personDetails":Lcom/google/android/finsky/protos/DocDetails$PersonDetails;
    if-eqz v1, :cond_2

    iget-boolean v2, v1, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->personIsRequester:Z

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    .line 215
    .local v0, "isOwnProfile":Z
    :cond_2
    if-eqz v0, :cond_3

    .line 216
    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeList;->getListPageUrls()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/MyPeoplePageHelper;->addPeoplePageListUrls(Ljava/util/List;)V

    .line 220
    .end local v0    # "isOwnProfile":Z
    .end local v1    # "personDetails":Lcom/google/android/finsky/protos/DocDetails$PersonDetails;
    :cond_3
    iput-object v5, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    .line 221
    return-void
.end method
