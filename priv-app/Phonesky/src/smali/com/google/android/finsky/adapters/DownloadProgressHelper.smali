.class public Lcom/google/android/finsky/adapters/DownloadProgressHelper;
.super Ljava/lang/Object;
.source "DownloadProgressHelper.java"


# static fields
.field private static sDownloadStatusFormatBytes:Ljava/lang/CharSequence;

.field private static sDownloadStatusFormatPercents:Ljava/lang/CharSequence;


# direct methods
.method public static configureDownloadProgressUi(Landroid/content/Context;Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ProgressBar;)V
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "progressReport"    # Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;
    .param p2, "downloadingBytes"    # Landroid/widget/TextView;
    .param p3, "downloadingPercentage"    # Landroid/widget/TextView;
    .param p4, "progressBar"    # Landroid/widget/ProgressBar;

    .prologue
    .line 40
    invoke-static {p0}, Lcom/google/android/finsky/adapters/DownloadProgressHelper;->initializeStrings(Landroid/content/Context;)V

    .line 42
    const-string v5, " "

    invoke-virtual {p3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    const-string v5, " "

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    iget-object v5, p1, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->installerState:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    sget-object v6, Lcom/google/android/finsky/receivers/Installer$InstallerState;->DOWNLOADING:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    if-eq v5, v6, :cond_1

    .line 45
    const/4 v5, 0x1

    invoke-virtual {p4, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 47
    iget-object v5, p1, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->installerState:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    sget-object v6, Lcom/google/android/finsky/receivers/Installer$InstallerState;->INSTALLING:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    if-ne v5, v6, :cond_0

    .line 48
    const v5, 0x7f0c029d

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    iget-wide v6, p1, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->bytesCompleted:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    iget-wide v6, p1, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->bytesTotal:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    iget-wide v6, p1, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->bytesCompleted:J

    iget-wide v8, p1, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->bytesTotal:J

    cmp-long v5, v6, v8

    if-gtz v5, :cond_2

    const/4 v0, 0x1

    .line 59
    .local v0, "bytesValid":Z
    :goto_1
    const/4 v1, 0x0

    .line 60
    .local v1, "percent":I
    if-eqz v0, :cond_3

    .line 61
    iget-wide v6, p1, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->bytesCompleted:J

    const-wide/16 v8, 0x64

    mul-long/2addr v6, v8

    iget-wide v8, p1, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->bytesTotal:J

    div-long/2addr v6, v8

    long-to-int v1, v6

    .line 62
    const/4 v5, 0x0

    invoke-virtual {p4, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 63
    invoke-virtual {p4, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 69
    :goto_2
    iget v4, p1, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->downloadStatus:I

    .line 70
    .local v4, "status":I
    const/16 v5, 0xc3

    if-ne v4, v5, :cond_4

    .line 71
    const v5, 0x7f0c02a5

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 55
    .end local v0    # "bytesValid":Z
    .end local v1    # "percent":I
    .end local v4    # "status":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 65
    .restart local v0    # "bytesValid":Z
    .restart local v1    # "percent":I
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p4, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_2

    .line 72
    .restart local v4    # "status":I
    :cond_4
    const/16 v5, 0xc4

    if-ne v4, v5, :cond_5

    .line 73
    const v5, 0x7f0c02a6

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 74
    :cond_5
    if-eqz v0, :cond_6

    .line 75
    sget-object v5, Lcom/google/android/finsky/adapters/DownloadProgressHelper;->sDownloadStatusFormatPercents:Ljava/lang/CharSequence;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/CharSequence;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 77
    .local v3, "progressStringPercents":Ljava/lang/CharSequence;
    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    sget-object v5, Lcom/google/android/finsky/adapters/DownloadProgressHelper;->sDownloadStatusFormatBytes:Ljava/lang/CharSequence;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/CharSequence;

    const/4 v7, 0x0

    iget-wide v8, p1, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->bytesCompleted:J

    invoke-static {p0, v8, v9}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-wide v8, p1, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->bytesTotal:J

    invoke-static {p0, v8, v9}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 81
    .local v2, "progressStringBytes":Ljava/lang/CharSequence;
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 84
    .end local v2    # "progressStringBytes":Ljava/lang/CharSequence;
    .end local v3    # "progressStringPercents":Ljava/lang/CharSequence;
    :cond_6
    const v5, 0x7f0c02a4

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0
.end method

.method private static initializeStrings(Landroid/content/Context;)V
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/finsky/adapters/DownloadProgressHelper;->sDownloadStatusFormatPercents:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 28
    const v0, 0x7f0c029b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/adapters/DownloadProgressHelper;->sDownloadStatusFormatPercents:Ljava/lang/CharSequence;

    .line 31
    :cond_0
    sget-object v0, Lcom/google/android/finsky/adapters/DownloadProgressHelper;->sDownloadStatusFormatBytes:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    .line 32
    const v0, 0x7f0c029c

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/adapters/DownloadProgressHelper;->sDownloadStatusFormatBytes:Ljava/lang/CharSequence;

    .line 35
    :cond_1
    return-void
.end method
