.class public final Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SignatureHashProto"
.end annotation


# instance fields
.field public hasHash:Z

.field public hasPackageName:Z

.field public hasVersionCode:Z

.field public hash:[B

.field public packageName:Ljava/lang/String;

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3327
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3328
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    .line 3329
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3332
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->packageName:Ljava/lang/String;

    .line 3333
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasPackageName:Z

    .line 3334
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->versionCode:I

    .line 3335
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasVersionCode:Z

    .line 3336
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hash:[B

    .line 3337
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasHash:Z

    .line 3338
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->cachedSize:I

    .line 3339
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3359
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3360
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasPackageName:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->packageName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3361
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->packageName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3364
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasVersionCode:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->versionCode:I

    if-eqz v1, :cond_3

    .line 3365
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->versionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3368
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasHash:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hash:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    .line 3369
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hash:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 3372
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 3380
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3381
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3385
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3386
    :sswitch_0
    return-object p0

    .line 3391
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->packageName:Ljava/lang/String;

    .line 3392
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasPackageName:Z

    goto :goto_0

    .line 3396
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->versionCode:I

    .line 3397
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasVersionCode:Z

    goto :goto_0

    .line 3401
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hash:[B

    .line 3402
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasHash:Z

    goto :goto_0

    .line 3381
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3298
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3345
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasPackageName:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->packageName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3346
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3348
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasVersionCode:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->versionCode:I

    if-eqz v0, :cond_3

    .line 3349
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3351
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hasHash:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hash:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3352
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;->hash:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 3354
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3355
    return-void
.end method
