.class Lcom/google/android/finsky/adapters/AggregatedAdapter$IndexTranslation;
.super Ljava/lang/Object;
.source "AggregatedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/adapters/AggregatedAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IndexTranslation"
.end annotation


# instance fields
.field private targetAdapter:Landroid/widget/ListAdapter;

.field private translatedIndex:I


# direct methods
.method private constructor <init>(Landroid/widget/ListAdapter;I)V
    .locals 0
    .param p1, "targetAdapter"    # Landroid/widget/ListAdapter;
    .param p2, "translatedIndex"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/finsky/adapters/AggregatedAdapter$IndexTranslation;->targetAdapter:Landroid/widget/ListAdapter;

    .line 44
    iput p2, p0, Lcom/google/android/finsky/adapters/AggregatedAdapter$IndexTranslation;->translatedIndex:I

    .line 45
    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/ListAdapter;ILcom/google/android/finsky/adapters/AggregatedAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/ListAdapter;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/google/android/finsky/adapters/AggregatedAdapter$1;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/adapters/AggregatedAdapter$IndexTranslation;-><init>(Landroid/widget/ListAdapter;I)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/adapters/AggregatedAdapter$IndexTranslation;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/AggregatedAdapter$IndexTranslation;

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/finsky/adapters/AggregatedAdapter$IndexTranslation;->translatedIndex:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/adapters/AggregatedAdapter$IndexTranslation;)Landroid/widget/ListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/AggregatedAdapter$IndexTranslation;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/finsky/adapters/AggregatedAdapter$IndexTranslation;->targetAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method
