.class public Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter$TabType;
.super Ljava/lang/Object;
.source "MyAppsTabbedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TabType"
.end annotation


# instance fields
.field public slidingPanelTab:Lcom/google/android/finsky/activities/myapps/MyAppsTab;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/activities/myapps/MyAppsTab",
            "<*>;"
        }
    .end annotation
.end field

.field private tabBundle:Lcom/google/android/finsky/utils/ObjectMap;

.field public final type:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter$TabType;->type:I

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter$TabType;)Lcom/google/android/finsky/utils/ObjectMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter$TabType;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter$TabType;->tabBundle:Lcom/google/android/finsky/utils/ObjectMap;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter$TabType;Lcom/google/android/finsky/utils/ObjectMap;)Lcom/google/android/finsky/utils/ObjectMap;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter$TabType;
    .param p1, "x1"    # Lcom/google/android/finsky/utils/ObjectMap;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter$TabType;->tabBundle:Lcom/google/android/finsky/utils/ObjectMap;

    return-object p1
.end method
