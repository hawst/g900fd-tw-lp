.class Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;
.super Ljava/lang/Object;
.source "AbsHorizontalStrip.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/AbsHorizontalStrip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimationRunnable"
.end annotation


# instance fields
.field private mDurationSec:F

.field private mStartTimeNs:J

.field private mVelocity:F

.field final synthetic this$0:Lcom/google/android/finsky/layout/AbsHorizontalStrip;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/layout/AbsHorizontalStrip;FJ)V
    .locals 3
    .param p2, "velocity"    # F
    .param p3, "durationMs"    # J

    .prologue
    .line 461
    iput-object p1, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->this$0:Lcom/google/android/finsky/layout/AbsHorizontalStrip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 462
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->mStartTimeNs:J

    .line 463
    long-to-float v0, p3

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->mDurationSec:F

    .line 464
    iput p2, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->mVelocity:F

    .line 465
    return-void
.end method

.method private scheduleFrame()V
    .locals 2

    .prologue
    .line 493
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 494
    iget-object v0, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->this$0:Lcom/google/android/finsky/layout/AbsHorizontalStrip;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/AbsHorizontalStrip;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 498
    :goto_0
    return-void

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->this$0:Lcom/google/android/finsky/layout/AbsHorizontalStrip;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/AbsHorizontalStrip;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 469
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->mStartTimeNs:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    const v3, 0x4e6e6b28    # 1.0E9f

    div-float v0, v2, v3

    .line 470
    .local v0, "elapsedSec":F
    iget v2, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->mDurationSec:F

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 471
    iget v0, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->mDurationSec:F

    .line 475
    :goto_0
    iget v2, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->mVelocity:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    mul-float/2addr v2, v0

    iget-object v3, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->this$0:Lcom/google/android/finsky/layout/AbsHorizontalStrip;

    iget v3, v3, Lcom/google/android/finsky/layout/AbsHorizontalStrip;->mDeceleration:F

    mul-float/2addr v3, v0

    mul-float/2addr v3, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float v1, v2, v3

    .line 477
    .local v1, "travelled":F
    iget v2, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->mVelocity:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 478
    neg-float v1, v1

    .line 480
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->this$0:Lcom/google/android/finsky/layout/AbsHorizontalStrip;

    iget-object v3, p0, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->this$0:Lcom/google/android/finsky/layout/AbsHorizontalStrip;

    iget v3, v3, Lcom/google/android/finsky/layout/AbsHorizontalStrip;->mOriginalPixelOffsetOfFirstChild:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    # invokes: Lcom/google/android/finsky/layout/AbsHorizontalStrip;->updateScrollPosition(F)V
    invoke-static {v2, v3}, Lcom/google/android/finsky/layout/AbsHorizontalStrip;->access$000(Lcom/google/android/finsky/layout/AbsHorizontalStrip;F)V

    .line 481
    return-void

    .line 473
    .end local v1    # "travelled":F
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->scheduleFrame()V

    goto :goto_0
.end method

.method public start()V
    .locals 0

    .prologue
    .line 484
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AbsHorizontalStrip$AnimationRunnable;->scheduleFrame()V

    .line 485
    return-void
.end method
