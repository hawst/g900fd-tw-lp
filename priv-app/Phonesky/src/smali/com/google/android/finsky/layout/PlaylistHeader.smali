.class public Lcom/google/android/finsky/layout/PlaylistHeader;
.super Landroid/widget/RelativeLayout;
.source "PlaylistHeader.java"


# instance fields
.field private mHeader:Landroid/view/View;

.field private final mMinFullHeight:I

.field private mPlaylistControl:Landroid/view/View;

.field private mSubheader:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/PlaylistHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mMinFullHeight:I

    .line 36
    return-void
.end method

.method private isCompactMode()Z
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mSubheader:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mPlaylistControl:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 42
    const v0, 0x7f0a0173

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/PlaylistHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    .line 43
    const v0, 0x7f0a0172

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/PlaylistHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mSubheader:Landroid/view/View;

    .line 44
    const v0, 0x7f0a025e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/PlaylistHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mPlaylistControl:Landroid/view/View;

    .line 45
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 13
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/android/finsky/layout/PlaylistHeader;->isCompactMode()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 93
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/PlaylistHeader;->getHeight()I

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/PlaylistHeader;->getPaddingBottom()I

    move-result v10

    sub-int v2, v9, v10

    .line 94
    .local v2, "headerBottom":I
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    sub-int v11, v2, v11

    iget-object v12, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    invoke-virtual {v9, v10, v11, v12, v2}, Landroid/view/View;->layout(IIII)V

    .line 127
    .end local v2    # "headerBottom":I
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/PlaylistHeader;->getPaddingTop()I

    move-result v4

    .line 98
    .local v4, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/PlaylistHeader;->getPaddingTop()I

    move-result v3

    .line 100
    .local v3, "paddingBottom":I
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 101
    .local v0, "contentHeight":I
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mSubheader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v9

    const/16 v10, 0x8

    if-eq v9, v10, :cond_2

    .line 102
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mSubheader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v0, v9

    .line 105
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/PlaylistHeader;->getHeight()I

    move-result v9

    sub-int/2addr v9, v4

    sub-int/2addr v9, v0

    sub-int/2addr v9, v3

    div-int/lit8 v9, v9, 0x2

    add-int v1, v4, v9

    .line 108
    .local v1, "contentTop":I
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    iget-object v12, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v1

    invoke-virtual {v9, v10, v1, v11, v12}, Landroid/view/View;->layout(IIII)V

    .line 111
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mPlaylistControl:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 113
    .local v6, "playlistControlLp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/PlaylistHeader;->getWidth()I

    move-result v9

    iget v10, v6, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v9, v10

    iget-object v10, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mPlaylistControl:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    sub-int v5, v9, v10

    .line 115
    .local v5, "playlistControlLeft":I
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getBaseline()I

    move-result v9

    add-int/2addr v9, v1

    iget-object v10, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mPlaylistControl:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getBaseline()I

    move-result v10

    sub-int v7, v9, v10

    .line 117
    .local v7, "playlistControlTop":I
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mPlaylistControl:Landroid/view/View;

    iget-object v10, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mPlaylistControl:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    add-int/2addr v10, v5

    iget-object v11, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mPlaylistControl:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v11, v7

    invoke-virtual {v9, v5, v7, v10, v11}, Landroid/view/View;->layout(IIII)V

    .line 121
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mSubheader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v9

    const/16 v10, 0x8

    if-eq v9, v10, :cond_0

    .line 122
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    move-result v8

    .line 123
    .local v8, "subheaderTop":I
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mSubheader:Landroid/view/View;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mSubheader:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    iget-object v12, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mSubheader:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v8

    invoke-virtual {v9, v10, v8, v11, v12}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v11, 0x0

    .line 54
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/PlaylistHeader;->getPaddingTop()I

    move-result v7

    .line 55
    .local v7, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/PlaylistHeader;->getPaddingBottom()I

    move-result v6

    .line 57
    .local v6, "paddingBottom":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 59
    .local v0, "availableWidth":I
    invoke-direct {p0}, Lcom/google/android/finsky/layout/PlaylistHeader;->isCompactMode()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 60
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v9, p1, v11}, Landroid/view/View;->measure(II)V

    .line 62
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 63
    .local v2, "headerHeight":I
    add-int v9, v2, v7

    add-int v1, v9, v6

    .line 64
    .local v1, "fullContentHeight":I
    iget v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mMinFullHeight:I

    if-le v1, v9, :cond_0

    move v5, v1

    .line 68
    .local v5, "height":I
    :goto_0
    invoke-virtual {p0, v0, v5}, Lcom/google/android/finsky/layout/PlaylistHeader;->setMeasuredDimension(II)V

    .line 88
    .end local v1    # "fullContentHeight":I
    .end local v2    # "headerHeight":I
    :goto_1
    return-void

    .line 64
    .end local v5    # "height":I
    .restart local v1    # "fullContentHeight":I
    .restart local v2    # "headerHeight":I
    :cond_0
    add-int v9, v7, v2

    iget v10, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mMinFullHeight:I

    sub-int/2addr v10, v2

    sub-int/2addr v10, v7

    sub-int/2addr v10, v6

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    add-int v5, v9, v6

    goto :goto_0

    .line 70
    .end local v1    # "fullContentHeight":I
    .end local v2    # "headerHeight":I
    :cond_1
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mPlaylistControl:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 72
    .local v8, "playlistControlLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mPlaylistControl:Landroid/view/View;

    invoke-virtual {v9, v11, v11}, Landroid/view/View;->measure(II)V

    .line 73
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mPlaylistControl:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    sub-int v9, v0, v9

    iget v10, v8, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v3, v9, v10

    .line 75
    .local v3, "headerWidth":I
    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 77
    .local v4, "headerWidthSpec":I
    move v5, v7

    .line 78
    .restart local v5    # "height":I
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v9, v4, v11}, Landroid/view/View;->measure(II)V

    .line 79
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mHeader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v5, v9

    .line 80
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mSubheader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v9

    const/16 v10, 0x8

    if-eq v9, v10, :cond_2

    .line 81
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mSubheader:Landroid/view/View;

    invoke-virtual {v9, v4, v11}, Landroid/view/View;->measure(II)V

    .line 82
    iget-object v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mSubheader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v5, v9

    .line 84
    :cond_2
    add-int/2addr v5, v6

    .line 85
    iget v9, p0, Lcom/google/android/finsky/layout/PlaylistHeader;->mMinFullHeight:I

    invoke-static {v5, v9}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 86
    invoke-virtual {p0, v0, v5}, Lcom/google/android/finsky/layout/PlaylistHeader;->setMeasuredDimension(II)V

    goto :goto_1
.end method
