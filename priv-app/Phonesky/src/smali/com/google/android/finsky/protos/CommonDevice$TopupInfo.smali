.class public final Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TopupInfo"
.end annotation


# instance fields
.field public hasOptionsContainerDocidDeprecated:Z

.field public hasOptionsListUrl:Z

.field public hasSubtitle:Z

.field public optionsContainerDocid:Lcom/google/android/finsky/protos/Common$Docid;

.field public optionsContainerDocidDeprecated:Ljava/lang/String;

.field public optionsListUrl:Ljava/lang/String;

.field public subtitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1763
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1764
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->clear()Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    .line 1765
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1768
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocidDeprecated:Ljava/lang/String;

    .line 1769
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasOptionsContainerDocidDeprecated:Z

    .line 1770
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 1771
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsListUrl:Ljava/lang/String;

    .line 1772
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasOptionsListUrl:Z

    .line 1773
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->subtitle:Ljava/lang/String;

    .line 1774
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasSubtitle:Z

    .line 1775
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->cachedSize:I

    .line 1776
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1799
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1800
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasOptionsContainerDocidDeprecated:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocidDeprecated:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1801
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocidDeprecated:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1804
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasOptionsListUrl:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsListUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1805
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsListUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1808
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasSubtitle:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->subtitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1809
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->subtitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1812
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v1, :cond_6

    .line 1813
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1816
    :cond_6
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1824
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1825
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1829
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1830
    :sswitch_0
    return-object p0

    .line 1835
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocidDeprecated:Ljava/lang/String;

    .line 1836
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasOptionsContainerDocidDeprecated:Z

    goto :goto_0

    .line 1840
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsListUrl:Ljava/lang/String;

    .line 1841
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasOptionsListUrl:Z

    goto :goto_0

    .line 1845
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->subtitle:Ljava/lang/String;

    .line 1846
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasSubtitle:Z

    goto :goto_0

    .line 1850
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v1, :cond_1

    .line 1851
    new-instance v1, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 1853
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1825
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1731
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1782
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasOptionsContainerDocidDeprecated:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocidDeprecated:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1783
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocidDeprecated:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1785
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasOptionsListUrl:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsListUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1786
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsListUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1788
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->hasSubtitle:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->subtitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1789
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->subtitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1791
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v0, :cond_6

    .line 1792
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsContainerDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1794
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1795
    return-void
.end method
