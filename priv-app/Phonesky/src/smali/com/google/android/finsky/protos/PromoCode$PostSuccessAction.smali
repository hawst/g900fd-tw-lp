.class public final Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PromoCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/PromoCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PostSuccessAction"
.end annotation


# instance fields
.field public installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

.field public openContainer:Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

.field public openDoc:Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

.field public openHome:Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;

.field public purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1073
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1074
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->clear()Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    .line 1075
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1078
    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openDoc:Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

    .line 1079
    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openHome:Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;

    .line 1080
    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    .line 1081
    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    .line 1082
    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openContainer:Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

    .line 1083
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->cachedSize:I

    .line 1084
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1110
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1111
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openDoc:Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

    if-eqz v1, :cond_0

    .line 1112
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openDoc:Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1115
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openHome:Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;

    if-eqz v1, :cond_1

    .line 1116
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openHome:Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1119
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    if-eqz v1, :cond_2

    .line 1120
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1123
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    if-eqz v1, :cond_3

    .line 1124
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1127
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openContainer:Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

    if-eqz v1, :cond_4

    .line 1128
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openContainer:Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1131
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1139
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1140
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1144
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1145
    :sswitch_0
    return-object p0

    .line 1150
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openDoc:Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

    if-nez v1, :cond_1

    .line 1151
    new-instance v1, Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openDoc:Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

    .line 1153
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openDoc:Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1157
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openHome:Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;

    if-nez v1, :cond_2

    .line 1158
    new-instance v1, Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openHome:Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;

    .line 1160
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openHome:Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1164
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    if-nez v1, :cond_3

    .line 1165
    new-instance v1, Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    .line 1167
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1171
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    if-nez v1, :cond_4

    .line 1172
    new-instance v1, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    .line 1174
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1178
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openContainer:Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

    if-nez v1, :cond_5

    .line 1179
    new-instance v1, Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openContainer:Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

    .line 1181
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openContainer:Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1140
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1041
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openDoc:Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

    if-eqz v0, :cond_0

    .line 1091
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openDoc:Lcom/google/android/finsky/protos/PromoCode$OpenDocAction;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1093
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openHome:Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;

    if-eqz v0, :cond_1

    .line 1094
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openHome:Lcom/google/android/finsky/protos/PromoCode$OpenHomeAction;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1096
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    if-eqz v0, :cond_2

    .line 1097
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->installApp:Lcom/google/android/finsky/protos/PromoCode$InstallAppAction;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1099
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    if-eqz v0, :cond_3

    .line 1100
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->purchaseFlow:Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1102
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openContainer:Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

    if-eqz v0, :cond_4

    .line 1103
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;->openContainer:Lcom/google/android/finsky/protos/PromoCode$OpenContainerDocumentAction;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1105
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1106
    return-void
.end method
