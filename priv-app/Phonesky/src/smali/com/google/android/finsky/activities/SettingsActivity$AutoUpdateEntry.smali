.class final enum Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
.super Ljava/lang/Enum;
.source "SettingsActivity.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/activities/SettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "AutoUpdateEntry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;",
        ">;",
        "Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

.field public static final enum AUTO_UPDATE_ALWAYS:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

.field public static final enum AUTO_UPDATE_NEVER:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

.field public static final enum AUTO_UPDATE_WIFI:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;


# instance fields
.field private final mResource:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 58
    new-instance v0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    const-string v1, "AUTO_UPDATE_NEVER"

    const v2, 0x7f0c031b

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->AUTO_UPDATE_NEVER:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    .line 59
    new-instance v0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    const-string v1, "AUTO_UPDATE_ALWAYS"

    const v2, 0x7f0c031c

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->AUTO_UPDATE_ALWAYS:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    .line 60
    new-instance v0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    const-string v1, "AUTO_UPDATE_WIFI"

    const v2, 0x7f0c031d

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->AUTO_UPDATE_WIFI:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    .line 57
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    sget-object v1, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->AUTO_UPDATE_NEVER:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->AUTO_UPDATE_ALWAYS:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->AUTO_UPDATE_WIFI:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->$VALUES:[Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 65
    iput p3, p0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->mResource:I

    .line 66
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 57
    const-class v0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    return-object v0
.end method

.method public static values()[Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->$VALUES:[Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    invoke-virtual {v0}, [Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    return-object v0
.end method


# virtual methods
.method public getResource()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->mResource:I

    return v0
.end method
