.class Lcom/google/android/finsky/layout/AddCreditCardFields$1;
.super Ljava/lang/Object;
.source "AddCreditCardFields.java"

# interfaces
.implements Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/AddCreditCardFields;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/AddCreditCardFields;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/AddCreditCardFields;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$1;->this$0:Lcom/google/android/finsky/layout/AddCreditCardFields;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNumberEntered()V
    .locals 4

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$1;->this$0:Lcom/google/android/finsky/layout/AddCreditCardFields;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->onNumberEntered()V

    .line 97
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$1;->this$0:Lcom/google/android/finsky/layout/AddCreditCardFields;

    iget-object v0, v0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mMonthField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/UiUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$1;->this$0:Lcom/google/android/finsky/layout/AddCreditCardFields;

    # getter for: Lcom/google/android/finsky/layout/AddCreditCardFields;->mAutoFocusHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->access$000(Lcom/google/android/finsky/layout/AddCreditCardFields;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/layout/AddCreditCardFields$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/AddCreditCardFields$1$1;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFields$1;)V

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFields$1;->this$0:Lcom/google/android/finsky/layout/AddCreditCardFields;

    iget-object v0, v0, Lcom/google/android/finsky/layout/AddCreditCardFields;->mMonthField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0
.end method
