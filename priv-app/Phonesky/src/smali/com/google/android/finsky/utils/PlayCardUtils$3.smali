.class final Lcom/google/android/finsky/utils/PlayCardUtils$3;
.super Ljava/lang/Object;
.source "PlayCardUtils.java"

# interfaces
.implements Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker$CardLifecycleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/PlayCardUtils;->initializeCardTrackers()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCardAttachedToWindow(Lcom/google/android/play/layout/PlayCardViewBase;)V
    .locals 1
    .param p1, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;

    .prologue
    .line 158
    # getter for: Lcom/google/android/finsky/utils/PlayCardUtils;->sWindowAttachedCards:Ljava/util/Set;
    invoke-static {}, Lcom/google/android/finsky/utils/PlayCardUtils;->access$100()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 159
    return-void
.end method

.method public onCardDetachedFromWindow(Lcom/google/android/play/layout/PlayCardViewBase;)V
    .locals 1
    .param p1, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;

    .prologue
    .line 163
    # getter for: Lcom/google/android/finsky/utils/PlayCardUtils;->sWindowAttachedCards:Ljava/util/Set;
    invoke-static {}, Lcom/google/android/finsky/utils/PlayCardUtils;->access$100()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 164
    return-void
.end method
