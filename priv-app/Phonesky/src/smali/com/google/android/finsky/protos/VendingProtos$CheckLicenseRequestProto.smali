.class public final Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckLicenseRequestProto"
.end annotation


# instance fields
.field public hasNonce:Z

.field public hasPackageName:Z

.field public hasVersionCode:Z

.field public nonce:J

.field public packageName:Ljava/lang/String;

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2147
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2148
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    .line 2149
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2152
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->packageName:Ljava/lang/String;

    .line 2153
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasPackageName:Z

    .line 2154
    iput v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->versionCode:I

    .line 2155
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasVersionCode:Z

    .line 2156
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->nonce:J

    .line 2157
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasNonce:Z

    .line 2158
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->cachedSize:I

    .line 2159
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 2179
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2180
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasPackageName:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->packageName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2181
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->packageName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2184
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasVersionCode:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->versionCode:I

    if-eqz v1, :cond_3

    .line 2185
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->versionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2188
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasNonce:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->nonce:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 2189
    :cond_4
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->nonce:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2192
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2200
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2201
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2205
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2206
    :sswitch_0
    return-object p0

    .line 2211
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->packageName:Ljava/lang/String;

    .line 2212
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasPackageName:Z

    goto :goto_0

    .line 2216
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->versionCode:I

    .line 2217
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasVersionCode:Z

    goto :goto_0

    .line 2221
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->nonce:J

    .line 2222
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasNonce:Z

    goto :goto_0

    .line 2201
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2118
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2165
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasPackageName:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->packageName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2166
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2168
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasVersionCode:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->versionCode:I

    if-eqz v0, :cond_3

    .line 2169
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2171
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->hasNonce:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->nonce:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 2172
    :cond_4
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseRequestProto;->nonce:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2174
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2175
    return-void
.end method
