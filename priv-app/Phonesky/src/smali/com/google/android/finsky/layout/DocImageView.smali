.class public Lcom/google/android/finsky/layout/DocImageView;
.super Lcom/google/android/play/image/FifeImageView;
.source "DocImageView.java"


# instance fields
.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mImageTypes:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DocImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/image/FifeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method


# virtual methods
.method public varargs bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V
    .locals 7
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "imageTypes"    # [I

    .prologue
    const/4 v4, 0x0

    .line 40
    iget-object v5, p0, Lcom/google/android/finsky/layout/DocImageView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/finsky/layout/DocImageView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-ne v5, p1, :cond_1

    iget-object v5, p0, Lcom/google/android/finsky/layout/DocImageView;->mImageTypes:[I

    invoke-static {v5, p3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v2, 0x1

    .line 43
    .local v2, "isSameData":Z
    :goto_0
    if-nez v2, :cond_0

    .line 44
    iput-object p1, p0, Lcom/google/android/finsky/layout/DocImageView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 45
    iput-object p3, p0, Lcom/google/android/finsky/layout/DocImageView;->mImageTypes:[I

    .line 48
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DocImageView;->getWidth()I

    move-result v3

    .line 49
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DocImageView;->getHeight()I

    move-result v0

    .line 50
    .local v0, "height":I
    if-lez v0, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/layout/DocImageView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v6, p0, Lcom/google/android/finsky/layout/DocImageView;->mImageTypes:[I

    invoke-static {v5, v4, v0, v6}, Lcom/google/android/finsky/utils/image/ThumbnailUtils;->getImageFromDocument(Lcom/google/android/finsky/api/model/Document;II[I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v1

    .line 53
    .local v1, "image":Lcom/google/android/finsky/protos/Common$Image;
    :goto_1
    if-eqz v1, :cond_3

    .line 54
    iget-object v4, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v5, v1, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {p0, v4, v5, p2}, Lcom/google/android/finsky/layout/DocImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 59
    .end local v0    # "height":I
    .end local v1    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v3    # "width":I
    :cond_0
    :goto_2
    return-void

    .end local v2    # "isSameData":Z
    :cond_1
    move v2, v4

    .line 40
    goto :goto_0

    .line 50
    .restart local v0    # "height":I
    .restart local v2    # "isSameData":Z
    .restart local v3    # "width":I
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/layout/DocImageView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v6, p0, Lcom/google/android/finsky/layout/DocImageView;->mImageTypes:[I

    invoke-static {v5, v3, v4, v6}, Lcom/google/android/finsky/utils/image/ThumbnailUtils;->getImageFromDocument(Lcom/google/android/finsky/api/model/Document;II[I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v1

    goto :goto_1

    .line 56
    .restart local v1    # "image":Lcom/google/android/finsky/protos/Common$Image;
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DocImageView;->clearImage()V

    goto :goto_2
.end method
