.class Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;
.super Lcom/google/android/finsky/utils/PlayAnimationUtils$AnimationListenerAdapter;
.source "PlayCardRateAndSuggestClusterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->syncState(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

.field final synthetic val$rateCard:Lcom/google/android/finsky/layout/play/PlayCardViewRate;

.field final synthetic val$rateContentAspectRatio:F


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;Lcom/google/android/finsky/layout/play/PlayCardViewRate;F)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;->val$rateCard:Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    iput p3, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;->val$rateContentAspectRatio:F

    invoke-direct {p0}, Lcom/google/android/finsky/utils/PlayAnimationUtils$AnimationListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 6
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v5, 0x0

    .line 292
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;->val$rateCard:Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    invoke-virtual {v1, v5}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setState(I)V

    .line 294
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    iget-object v1, v1, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    invoke-virtual {v1, v5, v5, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->bindCardAt(IILcom/google/android/finsky/layout/play/PlayCardDismissListener;)V

    .line 298
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;->val$rateCard:Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;->val$rateContentAspectRatio:F

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setThumbnailAspectRatio(F)V

    .line 300
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-wide/16 v2, 0x96

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 302
    .local v0, "fadeInAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;->val$rateCard:Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->startAnimation(Landroid/view/animation/Animation;)V

    .line 304
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    # invokes: Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->setState(IZ)V
    invoke-static {v1, v5, v5}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->access$000(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;IZ)V

    .line 305
    return-void
.end method
