.class public final Lcom/google/android/finsky/protos/Purchase$ClientCart;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Purchase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Purchase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientCart"
.end annotation


# instance fields
.field public addInstrumentPromptHtml:Ljava/lang/String;

.field public applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

.field public appliedVoucherIndex:[I

.field public buttonText:Ljava/lang/String;

.field public completePurchaseChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

.field public detailHtml:[Ljava/lang/String;

.field public extendedDetailHtml:[Ljava/lang/String;

.field public footerHtml:Ljava/lang/String;

.field public formattedOriginalPrice:Ljava/lang/String;

.field public formattedPrice:Ljava/lang/String;

.field public hasAddInstrumentPromptHtml:Z

.field public hasButtonText:Z

.field public hasFooterHtml:Z

.field public hasFormattedOriginalPrice:Z

.field public hasFormattedPrice:Z

.field public hasPriceByline:Z

.field public hasPurchaseContextToken:Z

.field public hasTitle:Z

.field public instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

.field public priceByline:Ljava/lang/String;

.field public purchaseContextToken:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 338
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 339
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->clear()Lcom/google/android/finsky/protos/Purchase$ClientCart;

    .line 340
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 343
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title:Ljava/lang/String;

    .line 344
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasTitle:Z

    .line 345
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedPrice:Ljava/lang/String;

    .line 346
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedPrice:Z

    .line 347
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedOriginalPrice:Ljava/lang/String;

    .line 348
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedOriginalPrice:Z

    .line 349
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->priceByline:Ljava/lang/String;

    .line 350
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPriceByline:Z

    .line 351
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->purchaseContextToken:Ljava/lang/String;

    .line 352
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPurchaseContextToken:Z

    .line 353
    iput-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 354
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    .line 355
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    .line 356
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->footerHtml:Ljava/lang/String;

    .line 357
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFooterHtml:Z

    .line 358
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addInstrumentPromptHtml:Ljava/lang/String;

    .line 359
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasAddInstrumentPromptHtml:Z

    .line 360
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->buttonText:Ljava/lang/String;

    .line 361
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasButtonText:Z

    .line 362
    iput-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 363
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 364
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    .line 365
    invoke-static {}, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->emptyArray()[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    .line 366
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->cachedSize:I

    .line 367
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 445
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 446
    .local v4, "size":I
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasTitle:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 447
    :cond_0
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 450
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedPrice:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedPrice:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 451
    :cond_2
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedPrice:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 454
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPurchaseContextToken:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->purchaseContextToken:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 455
    :cond_4
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->purchaseContextToken:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 458
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-eqz v5, :cond_6

    .line 459
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 462
    :cond_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_9

    .line 463
    const/4 v0, 0x0

    .line 464
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 465
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_8

    .line 466
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 467
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_7

    .line 468
    add-int/lit8 v0, v0, 0x1

    .line 469
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 465
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 473
    .end local v2    # "element":Ljava/lang/String;
    :cond_8
    add-int/2addr v4, v1

    .line 474
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 476
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_9
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFooterHtml:Z

    if-nez v5, :cond_a

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->footerHtml:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 477
    :cond_a
    const/4 v5, 0x6

    iget-object v6, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->footerHtml:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 480
    :cond_b
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasAddInstrumentPromptHtml:Z

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addInstrumentPromptHtml:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 481
    :cond_c
    const/4 v5, 0x7

    iget-object v6, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addInstrumentPromptHtml:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 484
    :cond_d
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasButtonText:Z

    if-nez v5, :cond_e

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->buttonText:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 485
    :cond_e
    const/16 v5, 0x8

    iget-object v6, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->buttonText:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 488
    :cond_f
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-eqz v5, :cond_10

    .line 489
    const/16 v5, 0x9

    iget-object v6, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 492
    :cond_10
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPriceByline:Z

    if-nez v5, :cond_11

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->priceByline:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 493
    :cond_11
    const/16 v5, 0xa

    iget-object v6, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->priceByline:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 496
    :cond_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    if-eqz v5, :cond_15

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_15

    .line 497
    const/4 v0, 0x0

    .line 498
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 499
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_14

    .line 500
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 501
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_13

    .line 502
    add-int/lit8 v0, v0, 0x1

    .line 503
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 499
    :cond_13
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 507
    .end local v2    # "element":Ljava/lang/String;
    :cond_14
    add-int/2addr v4, v1

    .line 508
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 510
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_15
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v5, :cond_17

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v5, v5

    if-lez v5, :cond_17

    .line 511
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v5, v5

    if-ge v3, v5, :cond_17

    .line 512
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v2, v5, v3

    .line 513
    .local v2, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v2, :cond_16

    .line 514
    const/16 v5, 0xc

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 511
    :cond_16
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 519
    .end local v2    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v3    # "i":I
    :cond_17
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    if-eqz v5, :cond_19

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    array-length v5, v5

    if-lez v5, :cond_19

    .line 520
    const/4 v1, 0x0

    .line 521
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    array-length v5, v5

    if-ge v3, v5, :cond_18

    .line 522
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    aget v2, v5, v3

    .line 523
    .local v2, "element":I
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v1, v5

    .line 521
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 526
    .end local v2    # "element":I
    :cond_18
    add-int/2addr v4, v1

    .line 527
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    .line 529
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_19
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    if-eqz v5, :cond_1b

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    array-length v5, v5

    if-lez v5, :cond_1b

    .line 530
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    array-length v5, v5

    if-ge v3, v5, :cond_1b

    .line 531
    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    aget-object v2, v5, v3

    .line 532
    .local v2, "element":Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    if-eqz v2, :cond_1a

    .line 533
    const/16 v5, 0xe

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 530
    :cond_1a
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 538
    .end local v2    # "element":Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    .end local v3    # "i":I
    :cond_1b
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedOriginalPrice:Z

    if-nez v5, :cond_1c

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedOriginalPrice:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1d

    .line 539
    :cond_1c
    const/16 v5, 0xf

    iget-object v6, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedOriginalPrice:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 542
    :cond_1d
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 10
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 550
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 551
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 555
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 556
    :sswitch_0
    return-object p0

    .line 561
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title:Ljava/lang/String;

    .line 562
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasTitle:Z

    goto :goto_0

    .line 566
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedPrice:Ljava/lang/String;

    .line 567
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedPrice:Z

    goto :goto_0

    .line 571
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->purchaseContextToken:Ljava/lang/String;

    .line 572
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPurchaseContextToken:Z

    goto :goto_0

    .line 576
    :sswitch_4
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-nez v8, :cond_1

    .line 577
    new-instance v8, Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;-><init>()V

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 579
    :cond_1
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 583
    :sswitch_5
    const/16 v8, 0x2a

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 585
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    if-nez v8, :cond_3

    move v1, v7

    .line 586
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [Ljava/lang/String;

    .line 587
    .local v4, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 588
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 590
    :cond_2
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_4

    .line 591
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 592
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 590
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 585
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :cond_3
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    array-length v1, v8

    goto :goto_1

    .line 595
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 596
    iput-object v4, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    goto :goto_0

    .line 600
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->footerHtml:Ljava/lang/String;

    .line 601
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFooterHtml:Z

    goto :goto_0

    .line 605
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addInstrumentPromptHtml:Ljava/lang/String;

    .line 606
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasAddInstrumentPromptHtml:Z

    goto :goto_0

    .line 610
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->buttonText:Ljava/lang/String;

    .line 611
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasButtonText:Z

    goto/16 :goto_0

    .line 615
    :sswitch_9
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-nez v8, :cond_5

    .line 616
    new-instance v8, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;-><init>()V

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 618
    :cond_5
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 622
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->priceByline:Ljava/lang/String;

    .line 623
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPriceByline:Z

    goto/16 :goto_0

    .line 627
    :sswitch_b
    const/16 v8, 0x5a

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 629
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    if-nez v8, :cond_7

    move v1, v7

    .line 630
    .restart local v1    # "i":I
    :goto_3
    add-int v8, v1, v0

    new-array v4, v8, [Ljava/lang/String;

    .line 631
    .restart local v4    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_6

    .line 632
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 634
    :cond_6
    :goto_4
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_8

    .line 635
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 636
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 634
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 629
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :cond_7
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    array-length v1, v8

    goto :goto_3

    .line 639
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Ljava/lang/String;
    :cond_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 640
    iput-object v4, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    goto/16 :goto_0

    .line 644
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :sswitch_c
    const/16 v8, 0x62

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 646
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v8, :cond_a

    move v1, v7

    .line 647
    .restart local v1    # "i":I
    :goto_5
    add-int v8, v1, v0

    new-array v4, v8, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 649
    .local v4, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_9

    .line 650
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 652
    :cond_9
    :goto_6
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_b

    .line 653
    new-instance v8, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v8, v4, v1

    .line 654
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 655
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 652
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 646
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_a
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v8

    goto :goto_5

    .line 658
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_b
    new-instance v8, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v8, v4, v1

    .line 659
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 660
    iput-object v4, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 664
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :sswitch_d
    const/16 v8, 0x68

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 666
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    if-nez v8, :cond_d

    move v1, v7

    .line 667
    .restart local v1    # "i":I
    :goto_7
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 668
    .local v4, "newArray":[I
    if-eqz v1, :cond_c

    .line 669
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 671
    :cond_c
    :goto_8
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_e

    .line 672
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 673
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 671
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 666
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_d
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    array-length v1, v8

    goto :goto_7

    .line 676
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 677
    iput-object v4, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    goto/16 :goto_0

    .line 681
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 682
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 684
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 685
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 686
    .local v5, "startPos":I
    :goto_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_f

    .line 687
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    .line 688
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 690
    :cond_f
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 691
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    if-nez v8, :cond_11

    move v1, v7

    .line 692
    .restart local v1    # "i":I
    :goto_a
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 693
    .restart local v4    # "newArray":[I
    if-eqz v1, :cond_10

    .line 694
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 696
    :cond_10
    :goto_b
    array-length v8, v4

    if-ge v1, v8, :cond_12

    .line 697
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 696
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 691
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_11
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    array-length v1, v8

    goto :goto_a

    .line 699
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_12
    iput-object v4, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    .line 700
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 704
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[I
    .end local v5    # "startPos":I
    :sswitch_f
    const/16 v8, 0x72

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 706
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    if-nez v8, :cond_14

    move v1, v7

    .line 707
    .restart local v1    # "i":I
    :goto_c
    add-int v8, v1, v0

    new-array v4, v8, [Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    .line 709
    .local v4, "newArray":[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    if-eqz v1, :cond_13

    .line 710
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 712
    :cond_13
    :goto_d
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_15

    .line 713
    new-instance v8, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;-><init>()V

    aput-object v8, v4, v1

    .line 714
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 715
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 712
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 706
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    :cond_14
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    array-length v1, v8

    goto :goto_c

    .line 718
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    :cond_15
    new-instance v8, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;-><init>()V

    aput-object v8, v4, v1

    .line 719
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 720
    iput-object v4, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    goto/16 :goto_0

    .line 724
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedOriginalPrice:Ljava/lang/String;

    .line 725
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedOriginalPrice:Z

    goto/16 :goto_0

    .line 551
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x6a -> :sswitch_e
        0x72 -> :sswitch_f
        0x7a -> :sswitch_10
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 268
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 373
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasTitle:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 374
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 376
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedPrice:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedPrice:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 377
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedPrice:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 379
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPurchaseContextToken:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->purchaseContextToken:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 380
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->purchaseContextToken:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 382
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-eqz v2, :cond_6

    .line 383
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 385
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 386
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 387
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 388
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_7

    .line 389
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 386
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 393
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFooterHtml:Z

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->footerHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 394
    :cond_9
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->footerHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 396
    :cond_a
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasAddInstrumentPromptHtml:Z

    if-nez v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addInstrumentPromptHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 397
    :cond_b
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addInstrumentPromptHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 399
    :cond_c
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasButtonText:Z

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->buttonText:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 400
    :cond_d
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->buttonText:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 402
    :cond_e
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-eqz v2, :cond_f

    .line 403
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 405
    :cond_f
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPriceByline:Z

    if-nez v2, :cond_10

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->priceByline:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 406
    :cond_10
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->priceByline:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 408
    :cond_11
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_13

    .line 409
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_13

    .line 410
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 411
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_12

    .line 412
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 409
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 416
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_13
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_15

    .line 417
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_15

    .line 418
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 419
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_14

    .line 420
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 417
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 424
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_15
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    array-length v2, v2

    if-lez v2, :cond_16

    .line 425
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    array-length v2, v2

    if-ge v1, v2, :cond_16

    .line 426
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->appliedVoucherIndex:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 425
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 429
    .end local v1    # "i":I
    :cond_16
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    array-length v2, v2

    if-lez v2, :cond_18

    .line 430
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    array-length v2, v2

    if-ge v1, v2, :cond_18

    .line 431
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    aget-object v0, v2, v1

    .line 432
    .local v0, "element":Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    if-eqz v0, :cond_17

    .line 433
    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 430
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 437
    .end local v0    # "element":Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    .end local v1    # "i":I
    :cond_18
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedOriginalPrice:Z

    if-nez v2, :cond_19

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedOriginalPrice:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 438
    :cond_19
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedOriginalPrice:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 440
    :cond_1a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 441
    return-void
.end method
