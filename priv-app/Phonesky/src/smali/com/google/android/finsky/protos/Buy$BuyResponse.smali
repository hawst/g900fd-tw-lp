.class public final Lcom/google/android/finsky/protos/Buy$BuyResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Buy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Buy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BuyResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;
    }
.end annotation


# instance fields
.field public addInstrumentPromptHtml:Ljava/lang/String;

.field public baseCheckoutUrl:Ljava/lang/String;

.field public challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

.field public checkoutInfo:Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

.field public checkoutServiceId:Ljava/lang/String;

.field public checkoutTokenRequired:Z

.field public confirmButtonText:Ljava/lang/String;

.field public continueViaUrl:Ljava/lang/String;

.field public encodedDeliveryToken:Ljava/lang/String;

.field public hasAddInstrumentPromptHtml:Z

.field public hasBaseCheckoutUrl:Z

.field public hasCheckoutServiceId:Z

.field public hasCheckoutTokenRequired:Z

.field public hasConfirmButtonText:Z

.field public hasContinueViaUrl:Z

.field public hasEncodedDeliveryToken:Z

.field public hasPermissionError:Z

.field public hasPermissionErrorMessageText:Z

.field public hasPermissionErrorTitleText:Z

.field public hasPurchaseCookie:Z

.field public hasPurchaseStatusUrl:Z

.field public hasServerLogsCookie:Z

.field public permissionError:I

.field public permissionErrorMessageText:Ljava/lang/String;

.field public permissionErrorTitleText:Ljava/lang/String;

.field public purchaseCookie:Ljava/lang/String;

.field public purchaseResponse:Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

.field public purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

.field public purchaseStatusUrl:Ljava/lang/String;

.field public serverLogsCookie:[B

.field public tosCheckboxHtml:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1226
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1227
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Buy$BuyResponse;->clear()Lcom/google/android/finsky/protos/Buy$BuyResponse;

    .line 1228
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Buy$BuyResponse;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1231
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseResponse:Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    .line 1232
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    .line 1233
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutInfo:Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

    .line 1234
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->continueViaUrl:Ljava/lang/String;

    .line 1235
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasContinueViaUrl:Z

    .line 1236
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutTokenRequired:Z

    .line 1237
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasCheckoutTokenRequired:Z

    .line 1238
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutServiceId:Ljava/lang/String;

    .line 1239
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasCheckoutServiceId:Z

    .line 1240
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->baseCheckoutUrl:Ljava/lang/String;

    .line 1241
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasBaseCheckoutUrl:Z

    .line 1242
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusUrl:Ljava/lang/String;

    .line 1243
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPurchaseStatusUrl:Z

    .line 1244
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    .line 1245
    iput v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionError:I

    .line 1246
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionError:Z

    .line 1247
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseCookie:Ljava/lang/String;

    .line 1248
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPurchaseCookie:Z

    .line 1249
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 1250
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->addInstrumentPromptHtml:Ljava/lang/String;

    .line 1251
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasAddInstrumentPromptHtml:Z

    .line 1252
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->confirmButtonText:Ljava/lang/String;

    .line 1253
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasConfirmButtonText:Z

    .line 1254
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorTitleText:Ljava/lang/String;

    .line 1255
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionErrorTitleText:Z

    .line 1256
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorMessageText:Ljava/lang/String;

    .line 1257
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionErrorMessageText:Z

    .line 1258
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->serverLogsCookie:[B

    .line 1259
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasServerLogsCookie:Z

    .line 1260
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->encodedDeliveryToken:Ljava/lang/String;

    .line 1261
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasEncodedDeliveryToken:Z

    .line 1262
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->cachedSize:I

    .line 1263
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 1333
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 1334
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseResponse:Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    if-eqz v5, :cond_0

    .line 1335
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseResponse:Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1338
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutInfo:Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

    if-eqz v5, :cond_1

    .line 1339
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutInfo:Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1342
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasContinueViaUrl:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->continueViaUrl:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1343
    :cond_2
    const/16 v5, 0x8

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->continueViaUrl:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1346
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPurchaseStatusUrl:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusUrl:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 1347
    :cond_4
    const/16 v5, 0x9

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusUrl:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1350
    :cond_5
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasCheckoutServiceId:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutServiceId:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1351
    :cond_6
    const/16 v5, 0xc

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutServiceId:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1354
    :cond_7
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasCheckoutTokenRequired:Z

    if-nez v5, :cond_8

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutTokenRequired:Z

    if-eqz v5, :cond_9

    .line 1355
    :cond_8
    const/16 v5, 0xd

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutTokenRequired:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 1358
    :cond_9
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasBaseCheckoutUrl:Z

    if-nez v5, :cond_a

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->baseCheckoutUrl:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 1359
    :cond_a
    const/16 v5, 0xe

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->baseCheckoutUrl:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1362
    :cond_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    if-eqz v5, :cond_e

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_e

    .line 1363
    const/4 v0, 0x0

    .line 1364
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 1365
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_d

    .line 1366
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1367
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_c

    .line 1368
    add-int/lit8 v0, v0, 0x1

    .line 1369
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1365
    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1373
    .end local v2    # "element":Ljava/lang/String;
    :cond_d
    add-int/2addr v4, v1

    .line 1374
    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    .line 1376
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_e
    iget v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionError:I

    if-nez v5, :cond_f

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionError:Z

    if-eqz v5, :cond_10

    .line 1377
    :cond_f
    const/16 v5, 0x26

    iget v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionError:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 1380
    :cond_10
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    if-eqz v5, :cond_11

    .line 1381
    const/16 v5, 0x27

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1384
    :cond_11
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPurchaseCookie:Z

    if-nez v5, :cond_12

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseCookie:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_13

    .line 1385
    :cond_12
    const/16 v5, 0x2e

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseCookie:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1388
    :cond_13
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-eqz v5, :cond_14

    .line 1389
    const/16 v5, 0x31

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1392
    :cond_14
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasAddInstrumentPromptHtml:Z

    if-nez v5, :cond_15

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->addInstrumentPromptHtml:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_16

    .line 1393
    :cond_15
    const/16 v5, 0x32

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->addInstrumentPromptHtml:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1396
    :cond_16
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasConfirmButtonText:Z

    if-nez v5, :cond_17

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->confirmButtonText:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 1397
    :cond_17
    const/16 v5, 0x33

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->confirmButtonText:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1400
    :cond_18
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionErrorTitleText:Z

    if-nez v5, :cond_19

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorTitleText:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1a

    .line 1401
    :cond_19
    const/16 v5, 0x34

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorTitleText:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1404
    :cond_1a
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionErrorMessageText:Z

    if-nez v5, :cond_1b

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorMessageText:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1c

    .line 1405
    :cond_1b
    const/16 v5, 0x35

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorMessageText:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1408
    :cond_1c
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasServerLogsCookie:Z

    if-nez v5, :cond_1d

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->serverLogsCookie:[B

    sget-object v6, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v5, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-nez v5, :cond_1e

    .line 1409
    :cond_1d
    const/16 v5, 0x36

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->serverLogsCookie:[B

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v5

    add-int/2addr v4, v5

    .line 1412
    :cond_1e
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasEncodedDeliveryToken:Z

    if-nez v5, :cond_1f

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->encodedDeliveryToken:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_20

    .line 1413
    :cond_1f
    const/16 v5, 0x37

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->encodedDeliveryToken:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1416
    :cond_20
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$BuyResponse;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 1424
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1425
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1429
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1430
    :sswitch_0
    return-object p0

    .line 1435
    :sswitch_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseResponse:Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    if-nez v6, :cond_1

    .line 1436
    new-instance v6, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseResponse:Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    .line 1438
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseResponse:Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1442
    :sswitch_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutInfo:Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

    if-nez v6, :cond_2

    .line 1443
    new-instance v6, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutInfo:Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

    .line 1445
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutInfo:Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

    const/4 v7, 0x2

    invoke-virtual {p1, v6, v7}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    goto :goto_0

    .line 1449
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->continueViaUrl:Ljava/lang/String;

    .line 1450
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasContinueViaUrl:Z

    goto :goto_0

    .line 1454
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusUrl:Ljava/lang/String;

    .line 1455
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPurchaseStatusUrl:Z

    goto :goto_0

    .line 1459
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutServiceId:Ljava/lang/String;

    .line 1460
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasCheckoutServiceId:Z

    goto :goto_0

    .line 1464
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutTokenRequired:Z

    .line 1465
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasCheckoutTokenRequired:Z

    goto :goto_0

    .line 1469
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->baseCheckoutUrl:Ljava/lang/String;

    .line 1470
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasBaseCheckoutUrl:Z

    goto :goto_0

    .line 1474
    :sswitch_8
    const/16 v6, 0x12a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1476
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    if-nez v6, :cond_4

    move v1, v5

    .line 1477
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 1478
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 1479
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1481
    :cond_3
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_5

    .line 1482
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1483
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1481
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1476
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_1

    .line 1486
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1487
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1491
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1492
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    .line 1513
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionError:I

    .line 1514
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionError:Z

    goto/16 :goto_0

    .line 1520
    .end local v4    # "value":I
    :sswitch_a
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    if-nez v6, :cond_6

    .line 1521
    new-instance v6, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    .line 1523
    :cond_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1527
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseCookie:Ljava/lang/String;

    .line 1528
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPurchaseCookie:Z

    goto/16 :goto_0

    .line 1532
    :sswitch_c
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-nez v6, :cond_7

    .line 1533
    new-instance v6, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 1535
    :cond_7
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1539
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->addInstrumentPromptHtml:Ljava/lang/String;

    .line 1540
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasAddInstrumentPromptHtml:Z

    goto/16 :goto_0

    .line 1544
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->confirmButtonText:Ljava/lang/String;

    .line 1545
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasConfirmButtonText:Z

    goto/16 :goto_0

    .line 1549
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorTitleText:Ljava/lang/String;

    .line 1550
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionErrorTitleText:Z

    goto/16 :goto_0

    .line 1554
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorMessageText:Ljava/lang/String;

    .line 1555
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionErrorMessageText:Z

    goto/16 :goto_0

    .line 1559
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->serverLogsCookie:[B

    .line 1560
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasServerLogsCookie:Z

    goto/16 :goto_0

    .line 1564
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->encodedDeliveryToken:Ljava/lang/String;

    .line 1565
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasEncodedDeliveryToken:Z

    goto/16 :goto_0

    .line 1425
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x13 -> :sswitch_2
        0x42 -> :sswitch_3
        0x4a -> :sswitch_4
        0x62 -> :sswitch_5
        0x68 -> :sswitch_6
        0x72 -> :sswitch_7
        0x12a -> :sswitch_8
        0x130 -> :sswitch_9
        0x13a -> :sswitch_a
        0x172 -> :sswitch_b
        0x18a -> :sswitch_c
        0x192 -> :sswitch_d
        0x19a -> :sswitch_e
        0x1a2 -> :sswitch_f
        0x1aa -> :sswitch_10
        0x1b2 -> :sswitch_11
        0x1ba -> :sswitch_12
    .end sparse-switch

    .line 1492
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Buy$BuyResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$BuyResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1269
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseResponse:Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    if-eqz v2, :cond_0

    .line 1270
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseResponse:Lcom/google/android/finsky/protos/Buy$PurchaseNotificationResponse;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1272
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutInfo:Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

    if-eqz v2, :cond_1

    .line 1273
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutInfo:Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1275
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasContinueViaUrl:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->continueViaUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1276
    :cond_2
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->continueViaUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1278
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPurchaseStatusUrl:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1279
    :cond_4
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1281
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasCheckoutServiceId:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutServiceId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1282
    :cond_6
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutServiceId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1284
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasCheckoutTokenRequired:Z

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutTokenRequired:Z

    if-eqz v2, :cond_9

    .line 1285
    :cond_8
    const/16 v2, 0xd

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->checkoutTokenRequired:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1287
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasBaseCheckoutUrl:Z

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->baseCheckoutUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 1288
    :cond_a
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->baseCheckoutUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1290
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_d

    .line 1291
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_d

    .line 1292
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->tosCheckboxHtml:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1293
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_c

    .line 1294
    const/16 v2, 0x25

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1291
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1298
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_d
    iget v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionError:I

    if-nez v2, :cond_e

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionError:Z

    if-eqz v2, :cond_f

    .line 1299
    :cond_e
    const/16 v2, 0x26

    iget v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionError:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1301
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    if-eqz v2, :cond_10

    .line 1302
    const/16 v2, 0x27

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1304
    :cond_10
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPurchaseCookie:Z

    if-nez v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseCookie:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 1305
    :cond_11
    const/16 v2, 0x2e

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->purchaseCookie:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1307
    :cond_12
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-eqz v2, :cond_13

    .line 1308
    const/16 v2, 0x31

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1310
    :cond_13
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasAddInstrumentPromptHtml:Z

    if-nez v2, :cond_14

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->addInstrumentPromptHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 1311
    :cond_14
    const/16 v2, 0x32

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->addInstrumentPromptHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1313
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasConfirmButtonText:Z

    if-nez v2, :cond_16

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->confirmButtonText:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 1314
    :cond_16
    const/16 v2, 0x33

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->confirmButtonText:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1316
    :cond_17
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionErrorTitleText:Z

    if-nez v2, :cond_18

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorTitleText:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    .line 1317
    :cond_18
    const/16 v2, 0x34

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorTitleText:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1319
    :cond_19
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasPermissionErrorMessageText:Z

    if-nez v2, :cond_1a

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorMessageText:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    .line 1320
    :cond_1a
    const/16 v2, 0x35

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->permissionErrorMessageText:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1322
    :cond_1b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasServerLogsCookie:Z

    if-nez v2, :cond_1c

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->serverLogsCookie:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_1d

    .line 1323
    :cond_1c
    const/16 v2, 0x36

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->serverLogsCookie:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 1325
    :cond_1d
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->hasEncodedDeliveryToken:Z

    if-nez v2, :cond_1e

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->encodedDeliveryToken:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    .line 1326
    :cond_1e
    const/16 v2, 0x37

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse;->encodedDeliveryToken:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1328
    :cond_1f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1329
    return-void
.end method
