.class Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;
.super Ljava/lang/Object;
.source "InAppBillingManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/iab/InAppBillingManager;->consumeIabPurchase(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/ConsumePurchaseResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/InAppBillingManager;

.field final synthetic val$responseCode:[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

.field final synthetic val$semaphore:Ljava/util/concurrent/Semaphore;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/iab/InAppBillingManager;[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;Ljava/util/concurrent/Semaphore;)V
    .locals 0

    .prologue
    .line 731
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    iput-object p2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;->val$responseCode:[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    iput-object p3, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/ConsumePurchaseResponse;)V
    .locals 5
    .param p1, "consumeResponse"    # Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    .prologue
    const/4 v4, 0x0

    .line 735
    iget-object v0, p1, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v0, :cond_0

    .line 736
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    # getter for: Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;
    invoke-static {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->access$200(Lcom/google/android/finsky/billing/iab/InAppBillingManager;)Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    # getter for: Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;
    invoke-static {v1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->access$100(Lcom/google/android/finsky/billing/iab/InAppBillingManager;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    const-string v3, "consumePurchase"

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/library/LibraryReplicators;->applyLibraryUpdate(Landroid/accounts/Account;Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;)V

    .line 739
    :cond_0
    iget v0, p1, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->status:I

    packed-switch v0, :pswitch_data_0

    .line 751
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;->val$responseCode:[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    aput-object v1, v0, v4

    .line 754
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 755
    return-void

    .line 741
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;->val$responseCode:[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    aput-object v1, v0, v4

    goto :goto_0

    .line 744
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;->val$responseCode:[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ITEM_NOT_OWNED:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    aput-object v1, v0, v4

    goto :goto_0

    .line 747
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;->val$responseCode:[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    aput-object v1, v0, v4

    goto :goto_0

    .line 739
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 731
    check-cast p1, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;->onResponse(Lcom/google/android/finsky/protos/ConsumePurchaseResponse;)V

    return-void
.end method
