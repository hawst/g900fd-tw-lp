.class Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$2;
.super Ljava/lang/Object;
.source "RestoreAppsSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;->onTokenReceived(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$2;->this$1:Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 5
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v4, 0x0

    .line 76
    const/4 v0, 0x0

    .line 77
    .local v0, "serverMessage":Ljava/lang/String;
    instance-of v1, p1, Lcom/google/android/volley/DisplayMessageError;

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 78
    check-cast v1, Lcom/google/android/volley/DisplayMessageError;

    invoke-virtual {v1}, Lcom/google/android/volley/DisplayMessageError;->getDisplayErrorHtml()Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_0
    const-string v1, "Unable to fetch backup apps: %s (%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    iget-object v1, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$2;->this$1:Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;

    iget-object v1, v1, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;->this$0:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/finsky/setup/RestoreAppsSidecar;->mBackupDocumentInfos:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    invoke-static {v1, v2}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->access$002(Lcom/google/android/finsky/setup/RestoreAppsSidecar;[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;)[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    .line 84
    iget-object v1, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$2;->this$1:Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;

    iget-object v1, v1, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;->this$0:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    const/4 v2, 0x6

    # invokes: Lcom/google/android/finsky/setup/RestoreAppsSidecar;->setState(II)V
    invoke-static {v1, v2, v4}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->access$200(Lcom/google/android/finsky/setup/RestoreAppsSidecar;II)V

    .line 85
    return-void
.end method
