.class public final Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeviceCreateInstrumentFlowState"
.end annotation


# instance fields
.field public handle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

.field public profileForm:Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

.field public usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 633
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 634
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->clear()Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    .line 635
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 638
    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->handle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    .line 639
    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->profileForm:Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

    .line 640
    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    .line 641
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->cachedSize:I

    .line 642
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 662
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 663
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->handle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    if-eqz v1, :cond_0

    .line 664
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->handle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 667
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->profileForm:Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

    if-eqz v1, :cond_1

    .line 668
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->profileForm:Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 671
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    if-eqz v1, :cond_2

    .line 672
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 675
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 683
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 684
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 688
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 689
    :sswitch_0
    return-object p0

    .line 694
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->handle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    if-nez v1, :cond_1

    .line 695
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->handle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    .line 697
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->handle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 701
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->profileForm:Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

    if-nez v1, :cond_2

    .line 702
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->profileForm:Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

    .line 704
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->profileForm:Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 708
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    if-nez v1, :cond_3

    .line 709
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    .line 711
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 684
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 607
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->handle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    if-eqz v0, :cond_0

    .line 649
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->handle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 651
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->profileForm:Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

    if-eqz v0, :cond_1

    .line 652
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->profileForm:Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 654
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    if-eqz v0, :cond_2

    .line 655
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 657
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 658
    return-void
.end method
