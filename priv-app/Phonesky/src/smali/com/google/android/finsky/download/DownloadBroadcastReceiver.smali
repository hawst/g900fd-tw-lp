.class public Lcom/google/android/finsky/download/DownloadBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DownloadBroadcastReceiver.java"


# static fields
.field private static sDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/android/finsky/download/DownloadQueue;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->sDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    return-object v0
.end method

.method public static initialize(Lcom/google/android/finsky/download/DownloadQueue;)V
    .locals 0
    .param p0, "downloadQueue"    # Lcom/google/android/finsky/download/DownloadQueue;

    .prologue
    .line 31
    sput-object p0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->sDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    .line 32
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    .line 36
    const-string v9, "Intent received at DownloadBroadcastReceiver"

    new-array v10, v11, [Ljava/lang/Object;

    invoke-static {v9, v10}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    sget-object v9, Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->sDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v9}, Lcom/google/android/finsky/download/DownloadQueue;->getDownloadManager()Lcom/google/android/finsky/download/DownloadManagerFacade;

    move-result-object v9

    invoke-interface {v9, p2}, Lcom/google/android/finsky/download/DownloadManagerFacade;->getUriFromBroadcast(Landroid/content/Intent;)Landroid/net/Uri;

    move-result-object v1

    .line 39
    .local v1, "contentUri":Landroid/net/Uri;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "action":Ljava/lang/String;
    const/4 v6, 0x0

    .line 41
    .local v6, "isClicked":Z
    const/4 v7, 0x0

    .line 42
    .local v7, "isCompleted":Z
    const-string v9, "android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 43
    const/4 v6, 0x1

    .line 51
    :cond_0
    :goto_0
    sget-object v9, Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->sDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v9, v1}, Lcom/google/android/finsky/download/DownloadQueue;->getDownloadByContentUri(Landroid/net/Uri;)Lcom/google/android/finsky/download/Download;

    move-result-object v3

    .line 52
    .local v3, "download":Lcom/google/android/finsky/download/Download;
    if-nez v3, :cond_4

    .line 53
    const-string v9, "DownloadBroadcastReceiver could not find %s in queue."

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v1, v10, v11

    invoke-static {v9, v10}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    if-eqz v6, :cond_1

    .line 66
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object p1

    .line 68
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v2

    .line 69
    .local v2, "currentAccount":Landroid/accounts/Account;
    if-eqz v2, :cond_1

    .line 70
    invoke-static {p1}, Lcom/google/android/finsky/activities/MainActivity;->getMyDownloadsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v8

    .line 71
    .local v8, "myAppsIntent":Landroid/content/Intent;
    const/high16 v9, 0x10000000

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 72
    invoke-virtual {p1, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 127
    .end local v2    # "currentAccount":Landroid/accounts/Account;
    .end local v8    # "myAppsIntent":Landroid/content/Intent;
    :cond_1
    :goto_1
    return-void

    .line 44
    .end local v3    # "download":Lcom/google/android/finsky/download/Download;
    :cond_2
    const-string v9, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 45
    const/4 v7, 0x1

    goto :goto_0

    .line 46
    :cond_3
    const-string v9, "android.intent.action.DOWNLOAD_COMPLETED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 48
    const/4 v7, 0x1

    goto :goto_0

    .line 78
    .restart local v3    # "download":Lcom/google/android/finsky/download/Download;
    :cond_4
    move v4, v6

    .line 79
    .local v4, "finalIsClicked":Z
    move v5, v7

    .line 82
    .local v5, "finalIsCompleted":Z
    new-instance v9, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;

    invoke-direct {v9, p0, v1, v4, v5}, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;-><init>(Lcom/google/android/finsky/download/DownloadBroadcastReceiver;Landroid/net/Uri;ZZ)V

    new-array v10, v11, [Ljava/lang/Void;

    invoke-virtual {v9, v10}, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method
