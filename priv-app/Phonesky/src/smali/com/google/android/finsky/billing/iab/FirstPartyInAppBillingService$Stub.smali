.class Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;
.super Lcom/android/vending/billing/IFirstPartyInAppBillingService$Stub;
.source "FirstPartyInAppBillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Stub"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    invoke-direct {p0}, Lcom/android/vending/billing/IFirstPartyInAppBillingService$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$1;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;-><init>(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;)V

    return-void
.end method


# virtual methods
.method public consumePurchase(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "apiVersion"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "purchaseToken"    # Ljava/lang/String;

    .prologue
    .line 122
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->checkAccountAndPackageName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v1, p1, p3}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 123
    .local v0, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v0, v1, :cond_0

    .line 124
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    .line 126
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v1

    invoke-virtual {v1, p2, p3, p4}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->consumePurchase(ILjava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public getBuyIntent(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 7
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "apiVersion"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "sku"    # Ljava/lang/String;
    .param p5, "type"    # Ljava/lang/String;
    .param p6, "developerPayload"    # Ljava/lang/String;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->checkAccountAndPackageName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v0, p1, p3}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v6

    .line 101
    .local v6, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v6, v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v0, v6}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$300(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v0, p1}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v0

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getBuyIntentToReplaceSkus(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 8
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "apiVersion"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p5, "newSku"    # Ljava/lang/String;
    .param p6, "type"    # Ljava/lang/String;
    .param p7, "developerPayload"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 143
    .local p4, "oldSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->checkAccountAndPackageName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v0, p1, p3}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v7

    .line 144
    .local v7, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v7, v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v0, v7}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$300(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v0

    .line 147
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v0, p1}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v0

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getBuyIntentToReplaceSkus(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getPurchases(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "apiVersion"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "continuationToken"    # Ljava/lang/String;

    .prologue
    .line 111
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->checkAccountAndPackageName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v1, p1, p3}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 112
    .local v0, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v0, v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v1, v0}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$300(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v1

    .line 115
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v1

    invoke-virtual {v1, p2, p3, p4, p5}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    goto :goto_0
.end method

.method public getSkuDetails(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "apiVersion"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "skusBundle"    # Landroid/os/Bundle;

    .prologue
    .line 90
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->checkAccountAndPackageName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v1, p1, p3}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 91
    .local v0, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v0, v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v1, v0}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$300(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v1

    .line 94
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v1

    invoke-virtual {v1, p2, p3, p4, p5}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    goto :goto_0
.end method

.method public isBillingSupported(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "apiVersion"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;

    .prologue
    .line 80
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->checkAccountAndPackageName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v1, p1, p3}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 81
    .local v0, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v0, v1, :cond_0

    .line 82
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    .line 84
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v1

    invoke-virtual {v1, p2, p3, p4}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public isPromoEligible(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "apiVersion"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 132
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->checkAccountAndPackageName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v1, p1, p3}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 133
    .local v0, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v0, v1, :cond_0

    .line 134
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    .line 136
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v1

    invoke-virtual {v1, p2, p3, p4}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->isPromoEligible(ILjava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method
