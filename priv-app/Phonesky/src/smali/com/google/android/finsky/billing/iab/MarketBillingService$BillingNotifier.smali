.class public Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;
.super Ljava/lang/Object;
.source "MarketBillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/iab/MarketBillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "BillingNotifier"
.end annotation


# instance fields
.field private mService:Lcom/google/android/finsky/billing/iab/MarketBillingService;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/billing/iab/MarketBillingService;)V
    .locals 0
    .param p1, "service"    # Lcom/google/android/finsky/billing/iab/MarketBillingService;

    .prologue
    .line 744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 745
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->mService:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    .line 746
    return-void
.end method


# virtual methods
.method protected sendPurchaseStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .param p3, "signature"    # Ljava/lang/String;

    .prologue
    .line 794
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->mService:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    iget-object v1, v1, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mPackageManager:Landroid/content/pm/PackageManager;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.vending.billing.PURCHASE_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v1, p1, v2}, Lcom/google/android/finsky/utils/IntentUtils;->createIntentForReceiver(Landroid/content/pm/PackageManager;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 796
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 797
    const/4 v1, 0x0

    .line 802
    :goto_0
    return v1

    .line 799
    :cond_0
    const-string v1, "inapp_signed_data"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 800
    const-string v1, "inapp_signature"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 801
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->mService:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->sendBroadcast(Landroid/content/Intent;)V

    .line 802
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected sendResponseCode(Ljava/lang/String;JLcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "requestId"    # J
    .param p4, "responseCode"    # Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .prologue
    .line 807
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->mService:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->sendResponseCode(Landroid/content/Context;Ljava/lang/String;JLcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Z

    move-result v0

    return v0
.end method

.method protected showStatusBarNotifications(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "response"    # Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    .prologue
    .line 762
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->signedResponse:Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

    iget-object v10, v2, Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;->signedData:Ljava/lang/String;

    .line 763
    .local v10, "data":Ljava/lang/String;
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->signedResponse:Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

    iget-object v14, v2, Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;->signature:Ljava/lang/String;

    .line 765
    .local v14, "signature":Ljava/lang/String;
    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    .local v9, "arr$":[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    array-length v12, v9

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    if-ge v11, v12, :cond_0

    aget-object v13, v9, v11

    .line 766
    .local v13, "notification":Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    iget-object v3, v13, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->tickerText:Ljava/lang/String;

    .line 767
    .local v3, "tickerText":Ljava/lang/String;
    iget-object v4, v13, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentTitle:Ljava/lang/String;

    .line 768
    .local v4, "contentTitle":Ljava/lang/String;
    iget-object v5, v13, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->contentText:Ljava/lang/String;

    .line 769
    .local v5, "contentText":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->mService:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    iget-object v2, v2, Lcom/google/android/finsky/billing/iab/MarketBillingService;->mPackageManager:Landroid/content/pm/PackageManager;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 770
    .local v7, "intent":Landroid/content/Intent;
    const-string v2, "inapp_signed_data"

    invoke-virtual {v7, v2, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 771
    const-string v2, "inapp_signature"

    invoke-virtual {v7, v2, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 773
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v1

    .line 774
    .local v1, "notifier":Lcom/google/android/finsky/utils/Notifier;
    const v6, 0x108008a

    const-string v8, "status"

    move-object/from16 v2, p2

    invoke-interface/range {v1 .. v8}, Lcom/google/android/finsky/utils/Notifier;->showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;Ljava/lang/String;)V

    .line 765
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 778
    .end local v1    # "notifier":Lcom/google/android/finsky/utils/Notifier;
    .end local v3    # "tickerText":Ljava/lang/String;
    .end local v4    # "contentTitle":Ljava/lang/String;
    .end local v5    # "contentText":Ljava/lang/String;
    .end local v7    # "intent":Landroid/content/Intent;
    .end local v13    # "notification":Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    :cond_0
    return-void
.end method
