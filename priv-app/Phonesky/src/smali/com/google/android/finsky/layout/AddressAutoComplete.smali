.class public Lcom/google/android/finsky/layout/AddressAutoComplete;
.super Landroid/widget/AutoCompleteTextView;
.source "AddressAutoComplete.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/AddressAutoComplete$1;,
        Lcom/google/android/finsky/layout/AddressAutoComplete$PlacesFilter;,
        Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;

.field private mBlockNextSuggestion:Z

.field private mSuggestionProvider:Lcom/google/android/finsky/layout/AddressSuggestionProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/AddressAutoComplete;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/AddressAutoComplete;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/AddressAutoComplete;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/AddressAutoComplete;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/AddressAutoComplete;->mBlockNextSuggestion:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/finsky/layout/AddressAutoComplete;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/AddressAutoComplete;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/AddressAutoComplete;->mBlockNextSuggestion:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/AddressAutoComplete;)Lcom/google/android/finsky/layout/AddressSuggestionProvider;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/AddressAutoComplete;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressAutoComplete;->mSuggestionProvider:Lcom/google/android/finsky/layout/AddressSuggestionProvider;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/AddressAutoComplete;)Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/AddressAutoComplete;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressAutoComplete;->mAdapter:Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized blockNextSuggestion()V
    .locals 1

    .prologue
    .line 60
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/finsky/layout/AddressAutoComplete;->mBlockNextSuggestion:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    monitor-exit p0

    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 44
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->onFinishInflate()V

    .line 45
    new-instance v0, Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddressAutoComplete;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04001b

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;-><init>(Lcom/google/android/finsky/layout/AddressAutoComplete;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressAutoComplete;->mAdapter:Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;

    .line 46
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressAutoComplete;->mAdapter:Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddressAutoComplete;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 47
    invoke-virtual {p0, p0}, Lcom/google/android/finsky/layout/AddressAutoComplete;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 48
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressAutoComplete;->mSuggestionProvider:Lcom/google/android/finsky/layout/AddressSuggestionProvider;

    if-eqz v0, :cond_0

    .line 72
    iget-object v1, p0, Lcom/google/android/finsky/layout/AddressAutoComplete;->mSuggestionProvider:Lcom/google/android/finsky/layout/AddressSuggestionProvider;

    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressAutoComplete;->mAdapter:Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;

    # getter for: Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;->mPredictions:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;->access$000(Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/placesapi/PlaceAutocompletePrediction;

    invoke-interface {v1, v0}, Lcom/google/android/finsky/layout/AddressSuggestionProvider;->onSuggestionAccepted(Lcom/google/android/finsky/placesapi/PlaceAutocompletePrediction;)V

    .line 74
    :cond_0
    return-void
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 67
    return-void
.end method

.method public declared-synchronized setSuggestionProvider(Lcom/google/android/finsky/layout/AddressSuggestionProvider;)V
    .locals 1
    .param p1, "suggestionProvider"    # Lcom/google/android/finsky/layout/AddressSuggestionProvider;

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/finsky/layout/AddressAutoComplete;->mSuggestionProvider:Lcom/google/android/finsky/layout/AddressSuggestionProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    monitor-exit p0

    return-void

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
