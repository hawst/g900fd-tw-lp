.class public final Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreditCardEntryAction"
.end annotation


# instance fields
.field public expDateEntryType:I

.field public expDateOcrEnabled:Z

.field public expDateRecognizedByOcr:Z

.field public expDateValidationErrorOccurred:Z

.field public hasExpDateEntryType:Z

.field public hasExpDateOcrEnabled:Z

.field public hasExpDateRecognizedByOcr:Z

.field public hasExpDateValidationErrorOccurred:Z

.field public hasNumOcrAttempts:Z

.field public hasOcrExitReason:Z

.field public hasPanEntryType:Z

.field public hasPanOcrEnabled:Z

.field public hasPanRecognizedByOcr:Z

.field public hasPanValidationErrorOccurred:Z

.field public numOcrAttempts:I

.field public ocrExitReason:I

.field public panEntryType:I

.field public panOcrEnabled:Z

.field public panRecognizedByOcr:Z

.field public panValidationErrorOccurred:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3299
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3300
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->clear()Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    .line 3301
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3304
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panOcrEnabled:Z

    .line 3305
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanOcrEnabled:Z

    .line 3306
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panEntryType:I

    .line 3307
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanEntryType:Z

    .line 3308
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panRecognizedByOcr:Z

    .line 3309
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanRecognizedByOcr:Z

    .line 3310
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panValidationErrorOccurred:Z

    .line 3311
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanValidationErrorOccurred:Z

    .line 3312
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateOcrEnabled:Z

    .line 3313
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateOcrEnabled:Z

    .line 3314
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateEntryType:I

    .line 3315
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateEntryType:Z

    .line 3316
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateRecognizedByOcr:Z

    .line 3317
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateRecognizedByOcr:Z

    .line 3318
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    .line 3319
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateValidationErrorOccurred:Z

    .line 3320
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->numOcrAttempts:I

    .line 3321
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasNumOcrAttempts:Z

    .line 3322
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->ocrExitReason:I

    .line 3323
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasOcrExitReason:Z

    .line 3324
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->cachedSize:I

    .line 3325
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3366
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3367
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanOcrEnabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panOcrEnabled:Z

    if-eqz v1, :cond_1

    .line 3368
    :cond_0
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panOcrEnabled:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3371
    :cond_1
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panEntryType:I

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanEntryType:Z

    if-eqz v1, :cond_3

    .line 3372
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panEntryType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3375
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanRecognizedByOcr:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panRecognizedByOcr:Z

    if-eqz v1, :cond_5

    .line 3376
    :cond_4
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panRecognizedByOcr:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3379
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanValidationErrorOccurred:Z

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panValidationErrorOccurred:Z

    if-eqz v1, :cond_7

    .line 3380
    :cond_6
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panValidationErrorOccurred:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3383
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateOcrEnabled:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateOcrEnabled:Z

    if-eqz v1, :cond_9

    .line 3384
    :cond_8
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateOcrEnabled:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3387
    :cond_9
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateEntryType:I

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateEntryType:Z

    if-eqz v1, :cond_b

    .line 3388
    :cond_a
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateEntryType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3391
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateRecognizedByOcr:Z

    if-nez v1, :cond_c

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateRecognizedByOcr:Z

    if-eqz v1, :cond_d

    .line 3392
    :cond_c
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateRecognizedByOcr:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3395
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateValidationErrorOccurred:Z

    if-nez v1, :cond_e

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    if-eqz v1, :cond_f

    .line 3396
    :cond_e
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3399
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasNumOcrAttempts:Z

    if-nez v1, :cond_10

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->numOcrAttempts:I

    if-eqz v1, :cond_11

    .line 3400
    :cond_10
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->numOcrAttempts:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3403
    :cond_11
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->ocrExitReason:I

    if-nez v1, :cond_12

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasOcrExitReason:Z

    if-eqz v1, :cond_13

    .line 3404
    :cond_12
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->ocrExitReason:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3407
    :cond_13
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 3415
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3416
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3420
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3421
    :sswitch_0
    return-object p0

    .line 3426
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panOcrEnabled:Z

    .line 3427
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanOcrEnabled:Z

    goto :goto_0

    .line 3431
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 3432
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 3436
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panEntryType:I

    .line 3437
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanEntryType:Z

    goto :goto_0

    .line 3443
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panRecognizedByOcr:Z

    .line 3444
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanRecognizedByOcr:Z

    goto :goto_0

    .line 3448
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panValidationErrorOccurred:Z

    .line 3449
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanValidationErrorOccurred:Z

    goto :goto_0

    .line 3453
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateOcrEnabled:Z

    .line 3454
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateOcrEnabled:Z

    goto :goto_0

    .line 3458
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 3459
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 3463
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateEntryType:I

    .line 3464
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateEntryType:Z

    goto :goto_0

    .line 3470
    .end local v1    # "value":I
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateRecognizedByOcr:Z

    .line 3471
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateRecognizedByOcr:Z

    goto :goto_0

    .line 3475
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    .line 3476
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateValidationErrorOccurred:Z

    goto :goto_0

    .line 3480
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->numOcrAttempts:I

    .line 3481
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasNumOcrAttempts:Z

    goto :goto_0

    .line 3485
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 3486
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_2

    goto :goto_0

    .line 3492
    :pswitch_2
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->ocrExitReason:I

    .line 3493
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasOcrExitReason:Z

    goto :goto_0

    .line 3416
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch

    .line 3432
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 3459
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 3486
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3230
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3331
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanOcrEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panOcrEnabled:Z

    if-eqz v0, :cond_1

    .line 3332
    :cond_0
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panOcrEnabled:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3334
    :cond_1
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panEntryType:I

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanEntryType:Z

    if-eqz v0, :cond_3

    .line 3335
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panEntryType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3337
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanRecognizedByOcr:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panRecognizedByOcr:Z

    if-eqz v0, :cond_5

    .line 3338
    :cond_4
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panRecognizedByOcr:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3340
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasPanValidationErrorOccurred:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panValidationErrorOccurred:Z

    if-eqz v0, :cond_7

    .line 3341
    :cond_6
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->panValidationErrorOccurred:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3343
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateOcrEnabled:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateOcrEnabled:Z

    if-eqz v0, :cond_9

    .line 3344
    :cond_8
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateOcrEnabled:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3346
    :cond_9
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateEntryType:I

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateEntryType:Z

    if-eqz v0, :cond_b

    .line 3347
    :cond_a
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateEntryType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3349
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateRecognizedByOcr:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateRecognizedByOcr:Z

    if-eqz v0, :cond_d

    .line 3350
    :cond_c
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateRecognizedByOcr:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3352
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasExpDateValidationErrorOccurred:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    if-eqz v0, :cond_f

    .line 3353
    :cond_e
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->expDateValidationErrorOccurred:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3355
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasNumOcrAttempts:Z

    if-nez v0, :cond_10

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->numOcrAttempts:I

    if-eqz v0, :cond_11

    .line 3356
    :cond_10
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->numOcrAttempts:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3358
    :cond_11
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->ocrExitReason:I

    if-nez v0, :cond_12

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->hasOcrExitReason:Z

    if-eqz v0, :cond_13

    .line 3359
    :cond_12
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;->ocrExitReason:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3361
    :cond_13
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3362
    return-void
.end method
