.class public Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;
.source "PlayCardRateAndSuggestClusterViewContent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent$PendingStateHandler;
    }
.end annotation


# instance fields
.field private mState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->mState:I

    .line 41
    return-void
.end method

.method private refreshCards(Z)V
    .locals 7
    .param p1, "animateChanges"    # Z

    .prologue
    const/4 v2, 0x0

    .line 71
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->getCardChildCount()I

    move-result v5

    if-nez v5, :cond_1

    .line 107
    :cond_0
    return-void

    .line 82
    :cond_1
    const/4 v4, 0x0

    .local v4, "tileIndex":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 83
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v1

    .line 84
    .local v1, "child":Lcom/google/android/play/layout/PlayCardViewBase;
    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/PlayCardViewBase;->setVisibility(I)V

    .line 82
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 88
    .end local v1    # "child":Lcom/google/android/play/layout/PlayCardViewBase;
    :cond_2
    iget v5, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->mState:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    const/4 v2, 0x1

    .line 91
    .local v2, "exitPendingStateForOtherCards":Z
    :cond_3
    const/4 v4, 0x1

    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 92
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v0

    .line 97
    .local v0, "card":Lcom/google/android/play/layout/PlayCardViewBase;
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->getData()Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_4

    .line 98
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->bindLoading()V

    :cond_4
    move-object v3, v0

    .line 100
    check-cast v3, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent$PendingStateHandler;

    .line 101
    .local v3, "pendingStateHandler":Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent$PendingStateHandler;
    if-eqz v2, :cond_5

    .line 102
    invoke-interface {v3, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent$PendingStateHandler;->exitPendingStateIfNecessary(Z)V

    .line 91
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 104
    :cond_5
    invoke-interface {v3, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent$PendingStateHandler;->enterPendingStateIfNecessary(Z)V

    goto :goto_2
.end method


# virtual methods
.method public createContent(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 1
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "dismissListener"    # Lcom/google/android/finsky/layout/play/PlayCardDismissListener;
    .param p5, "cardHeap"    # Lcom/google/android/finsky/layout/play/PlayCardHeap;
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 52
    invoke-super/range {p0 .. p6}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->createContent(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->refreshCards(Z)V

    .line 56
    return-void
.end method

.method protected getNumberOfTilesToBind()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->mState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getNumberOfTilesToBind()I

    move-result v0

    goto :goto_0
.end method

.method public setState(IZ)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "isDuringInitialLoading"    # Z

    .prologue
    .line 44
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->mState:I

    .line 45
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterViewContent;->refreshCards(Z)V

    .line 46
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
