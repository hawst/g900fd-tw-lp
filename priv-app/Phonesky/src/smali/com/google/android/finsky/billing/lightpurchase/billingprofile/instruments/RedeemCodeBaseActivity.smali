.class public abstract Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;
.super Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;
.source "RedeemCodeBaseActivity.java"

# interfaces
.implements Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment$Listener;


# instance fields
.field private mRedeemCodeFragment:Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;-><init>()V

    return-void
.end method

.method private getRedeemCodeResult()Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->mRedeemCodeFragment:Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->mRedeemCodeFragment:Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->getRedeemCodeResult()Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    move-result-object v0

    goto :goto_0
.end method

.method protected static putIntentExtras(Landroid/content/Intent;Ljava/lang/String;IILcom/google/android/finsky/protos/Common$Docid;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "redemptionContext"    # I
    .param p3, "backend"    # I
    .param p4, "docid"    # Lcom/google/android/finsky/protos/Common$Docid;
    .param p5, "offerType"    # I
    .param p6, "prefillCode"    # Ljava/lang/String;
    .param p7, "partnerPayload"    # Ljava/lang/String;

    .prologue
    .line 57
    const-string v0, "authAccount"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    const-string v0, "RedeemCodeBaseActivity.redemption_context"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 59
    const-string v0, "RedeemCodeBaseActivity.backend"

    invoke-virtual {p0, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 60
    const-string v0, "RedeemCodeBaseActivity.docid"

    invoke-static {p4}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 61
    const-string v0, "RedeemCodeBaseActivity.offer_type"

    invoke-virtual {p0, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 62
    const-string v0, "RedeemCodeBaseActivity.prefill_code"

    invoke-virtual {p0, v0, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    const-string v0, "RedeemCodeBaseActivity.partner_payload"

    invoke-virtual {p0, v0, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 3

    .prologue
    .line 108
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 109
    .local v1, "result":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->getRedeemCodeResult()Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    move-result-object v0

    .line 110
    .local v0, "redeemCodeResult":Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;
    if-eqz v0, :cond_0

    .line 111
    const-string v2, "RedeemCodeBaseActivity.redeem_code_result"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 115
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->setResult(ILandroid/content/Intent;)V

    .line 116
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->finish()V

    .line 117
    return-void
.end method

.method protected abstract getActivityLayout()I
.end method

.method protected abstract getBillingUiMode()I
.end method

.method protected abstract getPlayStoreUiElementType()I
.end method

.method protected abstract handleMessageAndFinish(Ljava/lang/String;)V
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 68
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->getActivityLayout()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->setContentView(I)V

    .line 70
    if-nez p1, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 72
    .local v8, "intent":Landroid/content/Intent;
    const-string v0, "RedeemCodeBaseActivity.docid"

    invoke-static {v8, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/protos/Common$Docid;

    .line 73
    .local v4, "docid":Lcom/google/android/finsky/protos/Common$Docid;
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->mAccountName:Ljava/lang/String;

    const-string v1, "RedeemCodeBaseActivity.redemption_context"

    invoke-virtual {v8, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "RedeemCodeBaseActivity.backend"

    const/4 v3, -0x1

    invoke-virtual {v8, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->getBillingUiMode()I

    move-result v3

    const-string v5, "RedeemCodeBaseActivity.offer_type"

    invoke-virtual {v8, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v6, "RedeemCodeBaseActivity.prefill_code"

    invoke-virtual {v8, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "RedeemCodeBaseActivity.partner_payload"

    invoke-virtual {v8, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->newInstance(Ljava/lang/String;IIILcom/google/android/finsky/protos/Common$Docid;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    move-result-object v9

    .line 78
    .local v9, "redeemCodeFragment":Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0a00c4

    invoke-virtual {v0, v1, v9}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 81
    .end local v4    # "docid":Lcom/google/android/finsky/protos/Common$Docid;
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v9    # "redeemCodeFragment":Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;
    :cond_0
    return-void
.end method

.method public onFinished()V
    .locals 2

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->getRedeemCodeResult()Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    move-result-object v1

    .line 133
    .local v1, "result":Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 134
    .local v0, "redeemedOfferHtml":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->handleMessageAndFinish(Ljava/lang/String;)V

    .line 135
    return-void

    .line 133
    .end local v0    # "redeemedOfferHtml":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;->getRedeemedOfferHtml()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 92
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onStart()V

    .line 93
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a00c4

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->mRedeemCodeFragment:Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    .line 95
    return-void
.end method
