.class Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SetupWizardRestoreDeviceDialogView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BackupDeviceInfoArrayAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDevices:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "devices"    # [Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    .prologue
    .line 115
    const v0, 0x7f040189

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 116
    iput-object p2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter;->mDevices:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    .line 117
    return-void
.end method

.method private getLastUsageTimeText(Landroid/content/Context;Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;)Ljava/lang/String;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backupDeviceInfo"    # Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    .prologue
    .line 169
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p2, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->lastCheckinTimeMs:J

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v0

    .line 171
    .local v0, "days":J
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 172
    .local v2, "resources":Landroid/content/res/Resources;
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-nez v3, :cond_0

    const v3, 0x7f0c03f1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    const v3, 0x7f100010

    long-to-int v4, v0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter;->mDevices:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 131
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    .line 136
    move-object v4, p2

    .line 137
    .local v4, "rowView":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter;->getItemViewType(I)I

    move-result v7

    .line 138
    .local v7, "viewType":I
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 139
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 141
    .local v3, "inflater":Landroid/view/LayoutInflater;
    if-nez v7, :cond_1

    .line 142
    if-nez v4, :cond_0

    .line 143
    const v8, 0x7f040188

    invoke-virtual {v3, v8, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    move-object v8, v4

    .line 145
    check-cast v8, Landroid/widget/CheckedTextView;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c03ec

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    move-object v5, v4

    .line 165
    .end local v4    # "rowView":Landroid/view/View;
    .local v5, "rowView":Landroid/view/View;
    :goto_0
    return-object v5

    .line 151
    .end local v5    # "rowView":Landroid/view/View;
    .restart local v4    # "rowView":Landroid/view/View;
    :cond_1
    if-nez v4, :cond_2

    .line 152
    const v8, 0x7f040189

    invoke-virtual {v3, v8, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 153
    new-instance v6, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter$ViewHolder;

    invoke-direct {v6}, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter$ViewHolder;-><init>()V

    .line 154
    .local v6, "viewHolder":Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter$ViewHolder;
    const v8, 0x7f0a034b

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckedTextView;

    iput-object v8, v6, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter$ViewHolder;->deviceName:Landroid/widget/CheckedTextView;

    .line 156
    const v8, 0x7f0a034c

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter$ViewHolder;->usageTime:Landroid/widget/TextView;

    .line 158
    invoke-virtual {v4, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 161
    .end local v6    # "viewHolder":Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter$ViewHolder;
    :cond_2
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter$ViewHolder;

    .line 162
    .local v2, "holder":Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter$ViewHolder;
    iget-object v8, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter;->mDevices:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    add-int/lit8 v9, p1, -0x1

    aget-object v0, v8, v9

    .line 163
    .local v0, "backupDeviceInfo":Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    iget-object v8, v2, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter$ViewHolder;->deviceName:Landroid/widget/CheckedTextView;

    iget-object v9, v0, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v8, v2, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter$ViewHolder;->usageTime:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter;->getLastUsageTimeText(Landroid/content/Context;Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v5, v4

    .line 165
    .end local v4    # "rowView":Landroid/view/View;
    .restart local v5    # "rowView":Landroid/view/View;
    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x2

    return v0
.end method
