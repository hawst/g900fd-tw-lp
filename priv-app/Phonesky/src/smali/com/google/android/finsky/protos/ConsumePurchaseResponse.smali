.class public final Lcom/google/android/finsky/protos/ConsumePurchaseResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ConsumePurchaseResponse.java"


# instance fields
.field public hasStatus:Z

.field public libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

.field public status:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->clear()Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    .line 38
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ConsumePurchaseResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 42
    iput v1, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->status:I

    .line 43
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->hasStatus:Z

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->cachedSize:I

    .line 45
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 62
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 63
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v1, :cond_0

    .line 64
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->status:I

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->hasStatus:Z

    if-eqz v1, :cond_2

    .line 68
    :cond_1
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->status:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ConsumePurchaseResponse;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 80
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 84
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 85
    :sswitch_0
    return-object p0

    .line 90
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-nez v2, :cond_1

    .line 91
    new-instance v2, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 93
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 97
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 98
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 103
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->status:I

    .line 104
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->hasStatus:Z

    goto :goto_0

    .line 80
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    .line 98
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v0, :cond_0

    .line 52
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 54
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->status:I

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->hasStatus:Z

    if-eqz v0, :cond_2

    .line 55
    :cond_1
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;->status:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 57
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 58
    return-void
.end method
