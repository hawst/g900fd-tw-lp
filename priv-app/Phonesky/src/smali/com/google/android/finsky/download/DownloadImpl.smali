.class public Lcom/google/android/finsky/download/DownloadImpl;
.super Ljava/lang/Object;
.source "DownloadImpl.java"

# interfaces
.implements Lcom/google/android/finsky/download/Download;


# static fields
.field private static final COMPLETED_STATES:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/finsky/download/Download$DownloadState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActualSize:J

.field private mContentUri:Landroid/net/Uri;

.field private final mCookieName:Ljava/lang/String;

.field private final mCookieValue:Ljava/lang/String;

.field private final mFileUri:Landroid/net/Uri;

.field private mHttpStatus:I

.field private final mInvisible:Z

.field private mLastProgress:Lcom/google/android/finsky/download/DownloadProgress;

.field private final mMaximumSize:J

.field private final mObb:Lcom/google/android/finsky/download/obb/Obb;

.field private final mPackageName:Ljava/lang/String;

.field mState:Lcom/google/android/finsky/download/Download$DownloadState;

.field private final mTitle:Ljava/lang/String;

.field private final mUrl:Ljava/lang/String;

.field private final mWifiOnly:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/finsky/download/Download$DownloadState;->CANCELLED:Lcom/google/android/finsky/download/Download$DownloadState;

    sget-object v1, Lcom/google/android/finsky/download/Download$DownloadState;->ERROR:Lcom/google/android/finsky/download/Download$DownloadState;

    sget-object v2, Lcom/google/android/finsky/download/Download$DownloadState;->SUCCESS:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/download/DownloadImpl;->COMPLETED_STATES:Ljava/util/EnumSet;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/finsky/download/obb/Obb;ZZ)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "cookieName"    # Ljava/lang/String;
    .param p5, "cookieValue"    # Ljava/lang/String;
    .param p6, "fileUri"    # Landroid/net/Uri;
    .param p7, "actualSize"    # J
    .param p9, "maximumSize"    # J
    .param p11, "obb"    # Lcom/google/android/finsky/download/obb/Obb;
    .param p12, "wifiOnly"    # Z
    .param p13, "invisible"    # Z

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadImpl;->mUrl:Ljava/lang/String;

    .line 102
    iput-object p2, p0, Lcom/google/android/finsky/download/DownloadImpl;->mTitle:Ljava/lang/String;

    .line 103
    iput-object p3, p0, Lcom/google/android/finsky/download/DownloadImpl;->mPackageName:Ljava/lang/String;

    .line 104
    iput-object p4, p0, Lcom/google/android/finsky/download/DownloadImpl;->mCookieName:Ljava/lang/String;

    .line 105
    iput-object p5, p0, Lcom/google/android/finsky/download/DownloadImpl;->mCookieValue:Ljava/lang/String;

    .line 106
    iput-object p6, p0, Lcom/google/android/finsky/download/DownloadImpl;->mFileUri:Landroid/net/Uri;

    .line 107
    iput-wide p7, p0, Lcom/google/android/finsky/download/DownloadImpl;->mActualSize:J

    .line 108
    iput-wide p9, p0, Lcom/google/android/finsky/download/DownloadImpl;->mMaximumSize:J

    .line 109
    iput-object p11, p0, Lcom/google/android/finsky/download/DownloadImpl;->mObb:Lcom/google/android/finsky/download/obb/Obb;

    .line 110
    iput-boolean p12, p0, Lcom/google/android/finsky/download/DownloadImpl;->mWifiOnly:Z

    .line 111
    iput-boolean p13, p0, Lcom/google/android/finsky/download/DownloadImpl;->mInvisible:Z

    .line 113
    sget-object v0, Lcom/google/android/finsky/download/Download$DownloadState;->UNQUEUED:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/download/DownloadImpl;->setState(Lcom/google/android/finsky/download/Download$DownloadState;)V

    .line 114
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 257
    if-ne p1, p0, :cond_0

    .line 258
    const/4 v1, 0x1

    .line 267
    :goto_0
    return v1

    .line 261
    :cond_0
    instance-of v1, p1, Lcom/google/android/finsky/download/DownloadImpl;

    if-nez v1, :cond_1

    .line 262
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 265
    check-cast v0, Lcom/google/android/finsky/download/DownloadImpl;

    .line 267
    .local v0, "other":Lcom/google/android/finsky/download/DownloadImpl;
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadImpl;->mUrl:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/finsky/download/DownloadImpl;->mUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 166
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 167
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mContentUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getCookieName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mCookieName:Ljava/lang/String;

    return-object v0
.end method

.method public getCookieValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mCookieValue:Ljava/lang/String;

    return-object v0
.end method

.method public getHttpStatus()I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mHttpStatus:I

    return v0
.end method

.method public getInvisible()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mInvisible:Z

    return v0
.end method

.method public getMaximumSize()J
    .locals 2

    .prologue
    .line 177
    iget-wide v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mMaximumSize:J

    return-wide v0
.end method

.method public getObb()Lcom/google/android/finsky/download/obb/Obb;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mObb:Lcom/google/android/finsky/download/obb/Obb;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 136
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getProgress()Lcom/google/android/finsky/download/DownloadProgress;
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mLastProgress:Lcom/google/android/finsky/download/DownloadProgress;

    return-object v0
.end method

.method public getRequestedDestination()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mFileUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getState()Lcom/google/android/finsky/download/Download$DownloadState;
    .locals 1

    .prologue
    .line 182
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 183
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mState:Lcom/google/android/finsky/download/Download$DownloadState;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 158
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getWifiOnly()Z
    .locals 1

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mWifiOnly:Z

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isCompleted()Z
    .locals 2

    .prologue
    .line 152
    sget-object v0, Lcom/google/android/finsky/download/DownloadImpl;->COMPLETED_STATES:Ljava/util/EnumSet;

    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadImpl;->mState:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isObb()Z
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mObb:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setContentUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadImpl;->mContentUri:Landroid/net/Uri;

    .line 173
    return-void
.end method

.method public setHttpStatus(I)V
    .locals 0
    .param p1, "httpStatus"    # I

    .prologue
    .line 246
    iput p1, p0, Lcom/google/android/finsky/download/DownloadImpl;->mHttpStatus:I

    .line 247
    return-void
.end method

.method public setProgress(Lcom/google/android/finsky/download/DownloadProgress;)V
    .locals 4
    .param p1, "progress"    # Lcom/google/android/finsky/download/DownloadProgress;

    .prologue
    .line 125
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 126
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadImpl;->mLastProgress:Lcom/google/android/finsky/download/DownloadProgress;

    .line 128
    iget-wide v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mActualSize:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-wide v0, p1, Lcom/google/android/finsky/download/DownloadProgress;->bytesTotal:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 129
    iget-wide v0, p1, Lcom/google/android/finsky/download/DownloadProgress;->bytesTotal:J

    iput-wide v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mActualSize:J

    .line 131
    :cond_0
    return-void
.end method

.method public setState(Lcom/google/android/finsky/download/Download$DownloadState;)V
    .locals 5
    .param p1, "state"    # Lcom/google/android/finsky/download/Download$DownloadState;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 228
    invoke-virtual {p0}, Lcom/google/android/finsky/download/DownloadImpl;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received state update when already completed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mState:Lcom/google/android/finsky/download/Download$DownloadState;

    if-ne v0, p1, :cond_1

    .line 232
    const-string v0, "Duplicate state set for \'%s\' (%s). Already in that state"

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p0, v1, v2

    iget-object v2, p0, Lcom/google/android/finsky/download/DownloadImpl;->mState:Lcom/google/android/finsky/download/Download$DownloadState;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 236
    :goto_0
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadImpl;->mState:Lcom/google/android/finsky/download/Download$DownloadState;

    .line 237
    return-void

    .line 234
    :cond_1
    const-string v0, "%s from %s to %s."

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    iget-object v2, p0, Lcom/google/android/finsky/download/DownloadImpl;->mState:Lcom/google/android/finsky/download/Download$DownloadState;

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 275
    const/4 v0, 0x0

    .line 276
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadImpl;->mObb:Lcom/google/android/finsky/download/obb/Obb;

    if-eqz v1, :cond_1

    .line 277
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "obb-for-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/download/DownloadImpl;->mObb:Lcom/google/android/finsky/download/obb/Obb;

    invoke-interface {v2}, Lcom/google/android/finsky/download/obb/Obb;->getPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 283
    :goto_0
    if-nez v0, :cond_0

    const-string v0, "untitled-download"

    .line 284
    :cond_0
    return-object v0

    .line 278
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadImpl;->mPackageName:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 279
    const-string v0, "self-update-download"

    goto :goto_0

    .line 281
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadImpl;->mPackageName:Ljava/lang/String;

    goto :goto_0
.end method
