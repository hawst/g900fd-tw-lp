.class public Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
.super Ljava/lang/Object;
.source "BackgroundEventBuilder.java"


# instance fields
.field private final event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreBackgroundActionEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    .line 37
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type:I

    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType:Z

    .line 39
    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    return-object v0
.end method

.method public setAppData(Lcom/google/android/finsky/analytics/PlayStore$AppData;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 1
    .param p1, "appData"    # Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .line 179
    return-object p0
.end method

.method public setAttempts(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "attempts"    # I

    .prologue
    .line 264
    if-ltz p1, :cond_0

    .line 265
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->attempts:I

    .line 266
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasAttempts:Z

    .line 268
    :cond_0
    return-object p0
.end method

.method public setAuthContext(Lcom/google/android/finsky/analytics/PlayStore$AuthContext;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 1
    .param p1, "authContext"    # Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    .line 330
    return-object p0
.end method

.method public setCallingPackage(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "callingPackage"    # Ljava/lang/String;

    .prologue
    .line 296
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->callingPackage:Ljava/lang/String;

    .line 298
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasCallingPackage:Z

    .line 300
    :cond_0
    return-object p0
.end method

.method public setClientLatencyMs(J)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 3
    .param p1, "clientLatencyMs"    # J

    .prologue
    .line 203
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-wide p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs:J

    .line 205
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasClientLatencyMs:Z

    .line 207
    :cond_0
    return-object p0
.end method

.method public setCreditCardEntryAction(Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 1
    .param p1, "action"    # Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->creditCardEntryAction:Lcom/google/android/finsky/analytics/PlayStore$CreditCardEntryAction;

    .line 354
    return-object p0
.end method

.method public setDocument(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "document"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document:Ljava/lang/String;

    .line 54
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasDocument:Z

    .line 56
    :cond_0
    return-object p0
.end method

.method public setErrorCode(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 79
    if-eqz p1, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode:I

    .line 81
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasErrorCode:Z

    .line 83
    :cond_0
    return-object p0
.end method

.method public setExceptionType(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "exceptionType"    # Ljava/lang/String;

    .prologue
    .line 93
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType:Ljava/lang/String;

    .line 95
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasExceptionType:Z

    .line 97
    :cond_0
    return-object p0
.end method

.method public setExceptionType(Ljava/lang/Throwable;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 1
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 106
    if-eqz p1, :cond_0

    .line 107
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setExceptionType(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    .line 109
    :cond_0
    return-object p0
.end method

.method public setFromSetting(Ljava/lang/Integer;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "fromSetting"    # Ljava/lang/Integer;

    .prologue
    .line 146
    if-eqz p1, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->fromSetting:I

    .line 148
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasFromSetting:Z

    .line 150
    :cond_0
    return-object p0
.end method

.method public setHost(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 233
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->host:Ljava/lang/String;

    .line 235
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasHost:Z

    .line 237
    :cond_0
    return-object p0
.end method

.method public setNlpRepairStatus(Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 1
    .param p1, "nlpRepairStatus"    # Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->nlpRepairStatus:Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    .line 215
    return-object p0
.end method

.method public setOfferCheckoutFlowRequired(Z)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "offerCheckoutFlowRequired"    # Z

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-boolean p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerCheckoutFlowRequired:Z

    .line 276
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferCheckoutFlowRequired:Z

    .line 277
    return-object p0
.end method

.method public setOfferType(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "offerType"    # I

    .prologue
    .line 133
    if-eqz p1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerType:I

    .line 135
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferType:Z

    .line 137
    :cond_0
    return-object p0
.end method

.method public setOperationSuccess(Z)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "operationSuccess"    # Z

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-boolean p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->operationSuccess:Z

    .line 223
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOperationSuccess:Z

    .line 224
    return-object p0
.end method

.method public setReason(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reason:Ljava/lang/String;

    .line 68
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasReason:Z

    .line 70
    :cond_0
    return-object p0
.end method

.method public setReviewData(Lcom/google/android/finsky/analytics/PlayStore$ReviewData;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 1
    .param p1, "reviewData"    # Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reviewData:Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    .line 308
    return-object p0
.end method

.method public setSearchSuggestionReport(Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 1
    .param p1, "searchSuggestionReport"    # Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->searchSuggestionReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    .line 286
    return-object p0
.end method

.method public setServerLatencyMs(J)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 3
    .param p1, "serverLatencyMs"    # J

    .prologue
    .line 189
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-wide p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLatencyMs:J

    .line 191
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLatencyMs:Z

    .line 193
    :cond_0
    return-object p0
.end method

.method public setServerLogsCookie([B)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "serverLogsCookie"    # [B

    .prologue
    .line 118
    if-eqz p1, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLogsCookie:[B

    .line 120
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLogsCookie:Z

    .line 122
    :cond_0
    return-object p0
.end method

.method public setSessionInfo(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 1
    .param p1, "sessionInfo"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSessionData;

    .line 171
    return-object p0
.end method

.method public setToSetting(Ljava/lang/Integer;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 2
    .param p1, "toSetting"    # Ljava/lang/Integer;

    .prologue
    .line 159
    if-eqz p1, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->toSetting:I

    .line 161
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasToSetting:Z

    .line 163
    :cond_0
    return-object p0
.end method

.method public setWidgetEventData(Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 1
    .param p1, "widgetEventData"    # Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->widgetEventData:Lcom/google/android/finsky/analytics/PlayStore$WidgetEventData;

    .line 245
    return-object p0
.end method

.method public setWifiAutoUpdateAttempt(Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .locals 1
    .param p1, "wifiAutoUpdateAttempt"    # Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->event:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    iput-object p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->wifiAutoUpdateAttempt:Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    .line 254
    return-object p0
.end method
