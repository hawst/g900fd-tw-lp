.class public Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;
.super Lcom/google/android/finsky/layout/AddCreditCardFields;
.source "AddCreditCardFieldsIcs.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;
    }
.end annotation


# static fields
.field private static KEY_PARENT_STATE:Ljava/lang/String;

.field private static KEY_STATE:Ljava/lang/String;


# instance fields
.field private mEditNumberButton:Landroid/widget/ImageButton;

.field private mGeneralLogo:Landroid/widget/ImageView;

.field private mNumberConcealed:Landroid/widget/TextView;

.field private mRestorePositionsOnLayout:Z

.field private mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "parent_instance_state"

    sput-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->KEY_PARENT_STATE:Ljava/lang/String;

    .line 31
    const-string v0, "state"

    sput-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->KEY_STATE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/AddCreditCardFields;-><init>(Landroid/content/Context;)V

    .line 33
    sget-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_NUMBER:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/AddCreditCardFields;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    sget-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_NUMBER:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberConcealed:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private animateToTranslationFadeIn(Landroid/view/View;I)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "translationY"    # I

    .prologue
    .line 471
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 472
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 473
    return-void
.end method

.method private animateToTranslationFadeOut(Landroid/view/View;I)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "translationY"    # I

    .prologue
    .line 480
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$5;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 487
    return-void
.end method

.method private concealNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCurrentCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .line 411
    .local v0, "type":Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    if-nez v0, :cond_0

    .line 412
    sget-object v0, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->MC:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .line 414
    :cond_0
    invoke-static {p1}, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->concealNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getBillingAddressTranslationY()I
    .locals 1

    .prologue
    .line 450
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getMonthYearCvcTranslationY()I

    move-result v0

    return v0
.end method

.method private getMonthYearCvcTranslationY()I
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_CONTRACTED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne v0, v1, :cond_0

    .line 435
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 439
    :goto_0
    return v0

    .line 436
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_EXPANDED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne v0, v1, :cond_1

    .line 437
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    goto :goto_0

    .line 439
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNumberTranslationY()I
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ordinal()I

    move-result v0

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_CONTRACTED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 423
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    .line 425
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private indentNumberField()V
    .locals 3

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0132

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 463
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 464
    return-void
.end method

.method private moveToEnteringAddressContractedCardState()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 291
    sget-object v3, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_CONTRACTED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    iput-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    .line 293
    const-wide/16 v4, 0x96

    invoke-direct {p0, v4, v5}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->setAnimationDelay(J)V

    .line 296
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getNumberTranslationY()I

    move-result v2

    .line 297
    .local v2, "numberFieldTranslationY":I
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getMonthYearCvcTranslationY()I

    move-result v1

    .line 298
    .local v1, "monthYearCvcTranslationY":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-direct {p0, v3, v2}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->animateToTranslationFadeOut(Landroid/view/View;I)V

    .line 299
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthField:Landroid/widget/EditText;

    invoke-direct {p0, v3, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->animateToTranslationFadeOut(Landroid/view/View;I)V

    .line 300
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthYearSeparator:Landroid/widget/TextView;

    invoke-direct {p0, v3, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->animateToTranslationFadeOut(Landroid/view/View;I)V

    .line 301
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mYearField:Landroid/widget/EditText;

    invoke-direct {p0, v3, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->animateToTranslationFadeOut(Landroid/view/View;I)V

    .line 302
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcImage:Landroid/widget/ImageView;

    invoke-direct {p0, v3, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->animateToTranslationFadeOut(Landroid/view/View;I)V

    .line 303
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcField:Landroid/widget/EditText;

    invoke-direct {p0, v3, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->animateToTranslationFadeOut(Landroid/view/View;I)V

    .line 306
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberConcealed:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 307
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberConcealed:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->concealNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberConcealed:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setAlpha(F)V

    .line 309
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberConcealed:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 312
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 313
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 314
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 317
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getBillingAddressTranslationY()I

    move-result v0

    .line 318
    .local v0, "billingAddressTranslationY":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v3, v8}, Lcom/google/android/finsky/layout/BillingAddress;->setVisibility(I)V

    .line 319
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v3, v6}, Lcom/google/android/finsky/layout/BillingAddress;->setAlpha(F)V

    .line 320
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/BillingAddress;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 321
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mPrivacyFooter:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 322
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mPrivacyFooter:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setAlpha(F)V

    .line 323
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mPrivacyFooter:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 326
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mImagesAnimator:Lcom/google/android/finsky/layout/CreditCardImagesAnimator;

    check-cast v3, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->switchToOneCardMode()V

    .line 328
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcField:Landroid/widget/EditText;

    const v4, 0x7f0a00bc

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 332
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/BillingAddress;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    new-instance v4, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$2;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$2;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 341
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->onAllFieldsVisible()V

    .line 344
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->requestLayout()V

    .line 345
    return-void
.end method

.method private moveToEnteringAddressExpandedCardState()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 359
    sget-object v2, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_EXPANDED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    iput-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    .line 361
    const-wide/16 v2, 0x96

    invoke-direct {p0, v2, v3}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->setAnimationDelay(J)V

    .line 365
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberConcealed:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$3;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$3;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 372
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->indentNumberField()V

    .line 373
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setVisibility(I)V

    .line 374
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 377
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$4;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$4;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 386
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getMonthYearCvcTranslationY()I

    move-result v1

    .line 387
    .local v1, "monthYearCvcTranslationY":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthField:Landroid/widget/EditText;

    invoke-direct {p0, v2, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->animateToTranslationFadeIn(Landroid/view/View;I)V

    .line 388
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthYearSeparator:Landroid/widget/TextView;

    invoke-direct {p0, v2, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->animateToTranslationFadeIn(Landroid/view/View;I)V

    .line 389
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mYearField:Landroid/widget/EditText;

    invoke-direct {p0, v2, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->animateToTranslationFadeIn(Landroid/view/View;I)V

    .line 390
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcField:Landroid/widget/EditText;

    invoke-direct {p0, v2, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->animateToTranslationFadeIn(Landroid/view/View;I)V

    .line 391
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcImage:Landroid/widget/ImageView;

    invoke-direct {p0, v2, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->animateToTranslationFadeIn(Landroid/view/View;I)V

    .line 394
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getBillingAddressTranslationY()I

    move-result v0

    .line 395
    .local v0, "billingAddressTranslationY":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/BillingAddress;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    int-to-float v3, v0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 396
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mPrivacyFooter:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    int-to-float v3, v0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 399
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->requestLayout()V

    .line 400
    return-void
.end method

.method private moveToEnteringCvcState()V
    .locals 2

    .prologue
    .line 255
    sget-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_CVC:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    .line 256
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 258
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 259
    return-void
.end method

.method private moveToEnteringMonthYearState()V
    .locals 3

    .prologue
    .line 210
    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_MONTH_YEAR:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    iput-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    .line 211
    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthField:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTop()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getTop()I

    move-result v2

    sub-int v0, v1, v2

    .line 212
    .local v0, "deltaY":I
    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthField:Landroid/widget/EditText;

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->setupNumberEnteredAnimation(Landroid/view/View;I)V

    .line 213
    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthYearSeparator:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->setupNumberEnteredAnimation(Landroid/view/View;I)V

    .line 214
    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mYearField:Landroid/widget/EditText;

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->setupNumberEnteredAnimation(Landroid/view/View;I)V

    .line 215
    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcField:Landroid/widget/EditText;

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->setupNumberEnteredAnimation(Landroid/view/View;I)V

    .line 216
    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    const v2, 0x7f0a00e0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setNextFocusDownId(I)V

    .line 217
    return-void
.end method

.method private restorePositions(Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;)V
    .locals 5
    .param p1, "state"    # Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getNumberTranslationY()I

    move-result v2

    .line 172
    .local v2, "numberTranslationY":I
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getMonthYearCvcTranslationY()I

    move-result v1

    .line 173
    .local v1, "monthYearCvcTranslationY":I
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getBillingAddressTranslationY()I

    move-result v0

    .line 174
    .local v0, "billingTranslationY":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setTranslationY(F)V

    .line 175
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthField:Landroid/widget/EditText;

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setTranslationY(F)V

    .line 176
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthYearSeparator:Landroid/widget/TextView;

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 177
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mYearField:Landroid/widget/EditText;

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setTranslationY(F)V

    .line 178
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcImage:Landroid/widget/ImageView;

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 179
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcField:Landroid/widget/EditText;

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setTranslationY(F)V

    .line 180
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberConcealed:Landroid/widget/TextView;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 181
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setTranslationY(F)V

    .line 182
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/layout/BillingAddress;->setTranslationY(F)V

    .line 183
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mPrivacyFooter:Landroid/widget/TextView;

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 186
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ordinal()I

    move-result v3

    sget-object v4, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_CONTRACTED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_0

    .line 187
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mImagesAnimator:Lcom/google/android/finsky/layout/CreditCardImagesAnimator;

    check-cast v3, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->switchToOneCardMode()V

    .line 191
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mImagesAnimator:Lcom/google/android/finsky/layout/CreditCardImagesAnimator;

    check-cast v3, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;

    iget-object v4, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCurrentCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->restoreCardType(Lcom/google/android/finsky/billing/creditcard/CreditCardType;)V

    goto :goto_0
.end method

.method private restoreVisibilities(Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;)V
    .locals 5
    .param p1, "state"    # Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 128
    sget-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_MONTH_YEAR:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_CVC:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_EXPANDED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne p1, v0, :cond_1

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthField:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mYearField:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcField:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthYearSeparator:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 136
    :cond_1
    sget-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_CVC:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_EXPANDED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne p1, v0, :cond_3

    .line 138
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 140
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ordinal()I

    move-result v0

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_CONTRACTED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_4

    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/BillingAddress;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mPrivacyFooter:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    :cond_4
    sget-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_CONTRACTED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne p1, v0, :cond_6

    .line 145
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberConcealed:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberField:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setAlpha(F)V

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthField:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setAlpha(F)V

    .line 152
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mMonthYearSeparator:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mYearField:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setAlpha(F)V

    .line 154
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCvcField:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setAlpha(F)V

    .line 163
    :cond_5
    :goto_0
    return-void

    .line 156
    :cond_6
    sget-object v0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_EXPANDED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne p1, v0, :cond_5

    .line 157
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberConcealed:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->indentNumberField()V

    goto :goto_0
.end method

.method private setAnimationDelay(J)V
    .locals 3
    .param p1, "startDelay"    # J

    .prologue
    .line 223
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 224
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_0
    return-void
.end method

.method private setupNumberEnteredAnimation(Landroid/view/View;I)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "deltaY"    # I

    .prologue
    const/4 v2, 0x0

    .line 236
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 237
    neg-int v0, p2

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 238
    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 241
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 242
    return-void
.end method


# virtual methods
.method protected createCreditCardImagesAnimator()Lcom/google/android/finsky/layout/CreditCardImagesAnimator;
    .locals 4

    .prologue
    .line 195
    new-instance v0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;

    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mCreditCardImages:[Landroid/widget/ImageView;

    sget-object v2, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->CREDIT_CARD_IMAGES_TYPE_ORDER:[Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    iget-object v3, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mGeneralLogo:Landroid/widget/ImageView;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;-><init>([Landroid/widget/ImageView;[Lcom/google/android/finsky/billing/creditcard/CreditCardType;Landroid/widget/ImageView;)V

    return-object v0
.end method

.method public expandFields()Z
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_CONTRACTED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne v0, v1, :cond_0

    .line 280
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->moveToEnteringAddressExpandedCardState()V

    .line 281
    const/4 v0, 0x1

    .line 283
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_CONTRACTED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne v0, v1, :cond_0

    .line 350
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->moveToEnteringAddressExpandedCardState()V

    .line 352
    :cond_0
    return-void
.end method

.method public onCvcEntered()V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_CVC:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne v0, v1, :cond_0

    .line 264
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->moveToEnteringAddressContractedCardState()V

    .line 266
    :cond_0
    return-void
.end method

.method public onCvcFocused()V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_MONTH_YEAR:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne v0, v1, :cond_0

    .line 247
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->moveToEnteringCvcState()V

    .line 249
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 49
    const v0, 0x7f0a00df

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mNumberConcealed:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f0a00e5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    const v0, 0x7f0a00dd

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mGeneralLogo:Landroid/widget/ImageView;

    .line 53
    invoke-super {p0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->onFinishInflate()V

    .line 54
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    new-instance v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$1;-><init>(Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/BillingAddress;->setOnHeightOffsetChangedListener(Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;)V

    .line 64
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 93
    invoke-super/range {p0 .. p5}, Lcom/google/android/finsky/layout/AddCreditCardFields;->onLayout(ZIIII)V

    .line 94
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mRestorePositionsOnLayout:Z

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mRestorePositionsOnLayout:Z

    .line 96
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->restorePositions(Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;)V

    .line 98
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 105
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/layout/AddCreditCardFields;->onMeasure(II)V

    .line 109
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ordinal()I

    move-result v0

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_CVC:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ordinal()I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 112
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/BillingAddress;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mPrivacyFooter:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->setMeasuredDimension(II)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_CONTRACTED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_ADDRESS_EXPANDED_CARD:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne v0, v1, :cond_0

    .line 119
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getMeasuredHeight()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->getBillingAddressTranslationY()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public onNumberEntered()V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ENTERING_NUMBER:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    if-ne v0, v1, :cond_0

    .line 202
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->moveToEnteringMonthYearState()V

    .line 204
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 76
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 77
    check-cast v0, Landroid/os/Bundle;

    .line 78
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->values()[Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->KEY_STATE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    .line 79
    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->restoreVisibilities(Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;)V

    .line 84
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mRestorePositionsOnLayout:Z

    .line 85
    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->KEY_PARENT_STATE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/google/android/finsky/layout/AddCreditCardFields;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 89
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 88
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/finsky/layout/AddCreditCardFields;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 68
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 69
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->KEY_PARENT_STATE:Ljava/lang/String;

    invoke-super {p0}, Lcom/google/android/finsky/layout/AddCreditCardFields;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 70
    sget-object v1, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->KEY_STATE:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mState:Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs$State;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 71
    return-object v0
.end method

.method protected updateIconsFromTheme(Z)V
    .locals 2
    .param p1, "isLightTheme"    # Z

    .prologue
    .line 270
    if-eqz p1, :cond_0

    const v0, 0x7f0200fe

    .line 272
    .local v0, "imageResourceId":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/AddCreditCardFieldsIcs;->mEditNumberButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 273
    return-void

    .line 270
    .end local v0    # "imageResourceId":I
    :cond_0
    const v0, 0x7f0200ff

    goto :goto_0
.end method
